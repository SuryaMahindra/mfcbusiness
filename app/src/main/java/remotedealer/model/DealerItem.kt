package remotedealer.model

import com.google.gson.annotations.SerializedName

class DealerItem {

    @SerializedName("city")
    var city = ""

    @SerializedName("code")
    var code = ""

    @SerializedName("name")
    var name = ""

    @SerializedName("type")
    var type = ""
    @SerializedName("rdr_status")
    var rdrstatus = ""

    @SerializedName("is_operational")
    var isoperational = ""
}