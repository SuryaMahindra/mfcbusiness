package remotedealer.model.sales

import com.google.gson.annotations.SerializedName


class RDMSalesResponse {

    @SerializedName("current_page")
    var currentPage: Int? = null

    @SerializedName("per_page")
    var perPage: Int? = null

    @SerializedName("total")
    var total: Int? = null

    @SerializedName("data")
    var data = ArrayList<RDMSalesItem>()
}