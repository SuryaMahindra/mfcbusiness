package remotedealer.model.sales

import com.google.gson.annotations.SerializedName

class RDMSalesRequest {

    @SerializedName("OrderByReverse")
    var orderByReverse = ""

    @SerializedName("Page")
    var page = ""

    @SerializedName("PageItems")
    var pageItems = ""

    @SerializedName("WhereIn")
    var whereIn: ArrayList<WhereIn>? = null

    @SerializedName("where")
    var where: List<Where>? = null
/*

    @SerializedName("DateFrom")
    var dateFrom = ""

    @SerializedName("DateTo")
    var dateTo = ""

    @SerializedName("DateField")
    var dateField = ""
*/

    @SerializedName("OrderBy")
    var orderBy = ""


    class Where {

        @SerializedName("operator")
        var operator = ""

        @SerializedName("value")
        var value: String? = null

        @SerializedName("column")
        var column = ""
    }


    class WhereIn {

        @SerializedName("column")
        var column = ""

        @SerializedName("values")
        var values = arrayOf<String>()

    }



}