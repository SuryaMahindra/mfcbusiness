package remotedealer.model.sales

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class RDMSalesItem {

    @SerializedName("stock_id")
    var stockId: Int? = null

    @SerializedName("dealer_code")
    var dealerCode = ""

    @SerializedName("make")
    var make = ""

    @SerializedName("model")
    var model = ""

    @SerializedName("variant")
    var variant = ""

    @SerializedName("kilometer")
    var kilometer: Int? = null

    @SerializedName("owner")
    var owner: Int? = null

    @SerializedName("registration_number")
    var registrationNumber = ""

    @SerializedName("selling_price")
    var sellingPrice: Int? = null

    @SerializedName("certified")
    var certified: Any? = null

    @SerializedName("fuel_type")
    var fuelType = ""

    @SerializedName("warranty_type")
    var warrantyType = ""

    @SerializedName("warranty_number")
    var warrantyNumber = ""

    @SerializedName("warranty_amount")
    var warrantyAmount: Double? = null

    @SerializedName("sold_date")
    var soldDate = ""

    @SerializedName("stock_type")
    var stockType = ""

    @SerializedName("model_year")
    var modelYear: Int? = null

    @SerializedName("sold_days")
    var soldDays: Int? = null

    @SerializedName("photo_count")
    var photoCount: Int? = null

    @SerializedName("cover_image")
    var coverImage = ""

    @SerializedName("source")
    var source = ""

    @SerializedName("sold")
    var sold: Int? = null

    @SerializedName("date_of_booking")
    var dateOfBooking = ""
}