package remotedealer.model.sales

import android.content.Context
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import remotedealer.ui.RDMTotalSalesFragment
import remotedealer.ui.RDMTotalWarrantySalesFragment

class TabsPagerAdapter(val mContext: Context, fm: FragmentManager?, var code: String,
                       var filterWhereList: ArrayList<RDMSalesRequest.Where>,
                       var filterWhereInList: ArrayList<RDMSalesRequest.WhereIn>) : FragmentPagerAdapter(fm!!) {

    var TAG = javaClass.simpleName
    val TAB_TITLES = arrayOf("Total Sales", "Warranty")

    override fun getItem(position: Int): Fragment {
        Log.i(TAG, "getItem: $position")
        return when (position) {
            0 -> RDMTotalSalesFragment(code, filterWhereList, filterWhereInList)
            1 -> RDMTotalWarrantySalesFragment(code)
            else -> RDMTotalSalesFragment(code, filterWhereList, filterWhereInList)
        }
    }

    override fun getPageTitle(position: Int): CharSequence {
        Log.i(TAG, "getPageTitle: ")
        return TAB_TITLES[position]
    }

    override fun getCount(): Int {
        Log.i(TAG, "getCount: ")
        return TAB_TITLES.size
    }

}