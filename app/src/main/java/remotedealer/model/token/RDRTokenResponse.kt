package remotedealer.model.token

import com.google.gson.annotations.SerializedName

class RDRTokenResponse {

    @SerializedName("token")
    var token = ""

}