package remotedealer.model.dashboard

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class RAGStatusData {
    @SerializedName("code")
    var code = ""

    @SerializedName("name")
    var name = ""

    @SerializedName("today")
    var today = ""

    @SerializedName("last_week")
    var lastWeek = ""
}