package remotedealer.model.dashboard


import com.google.gson.annotations.SerializedName

data class PhoneNumberResponse(
    @SerializedName("data")
    var `data`: ArrayList<Data>
) {
    data class Data(
        @SerializedName("mobile")
        var mobile: String,
        @SerializedName("name")
        var name: String,
        @SerializedName("type")
        var type: String
    )
}