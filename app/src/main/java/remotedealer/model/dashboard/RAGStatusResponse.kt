package remotedealer.model.dashboard

import com.google.gson.annotations.SerializedName

class RAGStatusResponse {
    @SerializedName("data")
    var data = ArrayList<RAGStatusData>()
}