package remotedealer.model.dashboard


import com.google.gson.annotations.SerializedName

data class DealerNotOperationalRequest(
    @SerializedName("report_for")
    var reportFor: ReportFor
) {
    data class ReportFor(
        @SerializedName("dealerCode")
        var dealerCode: String,
        @SerializedName("isOperational")
        var isOperational: String,
        @SerializedName("videoUrl")
        var videoUrl: String,
        @SerializedName("visitedById")
        var visitedById: Int
    )
}