package remotedealer.model.dashboard


import com.google.gson.annotations.SerializedName
import remotedealer.model.DealerItem

data class DealerListResponse(
        @SerializedName("data")
        var data: ArrayList<DealerItem>,
)