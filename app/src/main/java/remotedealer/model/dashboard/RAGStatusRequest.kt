package remotedealer.model.dashboard

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class RAGStatusRequest {

    @SerializedName("asm_id")
    @Expose
    var asmId: String? = ""

    @SerializedName("date")
    @Expose
     var date: String? = ""
}