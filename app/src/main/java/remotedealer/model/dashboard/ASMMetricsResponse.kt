package remotedealer.model.dashboard


import com.google.gson.annotations.SerializedName

data class ASMMetricsResponse(
    @SerializedName("OMSConversion")
    var oMSConversion: List<OMSConversion>,
    @SerializedName("OverallMetric")
    var overallMetric: List<OverallMetric>,
    @SerializedName("ProcContri")
    var procContri: List<ProcContri>,
    @SerializedName("TotalProc")
    var totalProc: List<TotalProc>,
    @SerializedName("TotalSales")
    var totalSales: List<TotalSale>,
    @SerializedName("ZeroOMSContribution")
    var zeroOMSContribution: List<ZeroOMSContribution>,
    @SerializedName("ZeroSales")
    var zeroSales: List<ZeroSale>,
    @SerializedName("ZeroStock")
    var zeroStock: List<ZeroStock>
) {
    data class OMSConversion(
        @SerializedName("actual")
        var `actual`: String,
        @SerializedName("dealer_code")
        var dealerCode: String,
        @SerializedName("dealer_name")
        var dealerName: String,
        @SerializedName("percentage")
        var percentage: String,
        @SerializedName("status")
        var status: String,
        @SerializedName("target")
        var target: String
    )

    data class OverallMetric(
        @SerializedName("actual")
        var `actual`: String,
        @SerializedName("display")
        var display: String,
        @SerializedName("metric")
        var metric: String,
        @SerializedName("percentage")
        var percentage: String,
        @SerializedName("target")
        var target: String
    )

    data class ProcContri(
        @SerializedName("actual")
        var `actual`: String,
        @SerializedName("dealer_code")
        var dealerCode: String,
        @SerializedName("dealer_name")
        var dealerName: String,
        @SerializedName("percentage")
        var percentage: String,
        @SerializedName("status")
        var status: String,
        @SerializedName("target")
        var target: String
    )

    data class TotalProc(
        @SerializedName("actual")
        var `actual`: String,
        @SerializedName("dealer_code")
        var dealerCode: String,
        @SerializedName("dealer_name")
        var dealerName: String,
        @SerializedName("percentage")
        var percentage: String,
        @SerializedName("status")
        var status: String,
        @SerializedName("target")
        var target: String
    )

    data class TotalSale(
        @SerializedName("actual")
        var `actual`: String,
        @SerializedName("dealer_code")
        var dealerCode: String,
        @SerializedName("dealer_name")
        var dealerName: String,
        @SerializedName("percentage")
        var percentage: String,
        @SerializedName("status")
        var status: String,
        @SerializedName("target")
        var target: String
    )

    data class ZeroOMSContribution(
        @SerializedName("actual")
        var `actual`: String,
        @SerializedName("dealer_code")
        var dealerCode: String,
        @SerializedName("dealer_name")
        var dealerName: String,
        @SerializedName("percentage")
        var percentage: String,
        @SerializedName("status")
        var status: String,
        @SerializedName("target")
        var target: String
    )

    data class ZeroSale(
        @SerializedName("actual")
        var `actual`: String,
        @SerializedName("dealer_code")
        var dealerCode: String,
        @SerializedName("dealer_name")
        var dealerName: String,
        @SerializedName("percentage")
        var percentage: String,
        @SerializedName("status")
        var status: String,
        @SerializedName("target")
        var target: String
    )

    data class ZeroStock(
        @SerializedName("actual")
        var `actual`: String,
        @SerializedName("dealer_code")
        var dealerCode: String,
        @SerializedName("dealer_name")
        var dealerName: String,
        @SerializedName("percentage")
        var percentage: String,
        @SerializedName("status")
        var status: String,
        @SerializedName("target")
        var target: String
    )
}