package remotedealer.model.dashboard


import com.google.gson.annotations.SerializedName

data class RDRStatusResponse(
    @SerializedName("data")
    var `data`: ArrayList<RDRStatusData>
) {
    data class RDRStatusData(
        @SerializedName("code")
        var code: String,
        @SerializedName("is_op")
        var isOp: Boolean,
        @SerializedName("status")
        var status: String
    )
}