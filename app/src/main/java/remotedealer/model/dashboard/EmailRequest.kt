package remotedealer.model.dashboard


import com.google.gson.annotations.SerializedName

data class EmailRequest(
    @SerializedName("dealer_code")
    var dealerCode: String,
    @SerializedName("dealer_type")
    var dealerType: String,
    @SerializedName("from_date")
    var fromDate: String,
    @SerializedName("requested_for_all")
    var requestedForAll: String,
    @SerializedName("to_date")
    var toDate: String
)