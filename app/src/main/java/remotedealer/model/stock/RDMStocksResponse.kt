package remotedealer.model.stock

import com.google.gson.annotations.SerializedName


class RDMStocksResponse {

    @SerializedName("current_page")
    var currentPage: Int? = null

    @SerializedName("per_page")
    var perPage: Int? = null

    @SerializedName("total")
    var total: Int? = null

    @SerializedName("data")
    var data = arrayListOf<RDMStocksItem>()
}