package remotedealer.model.stock

import com.google.gson.annotations.SerializedName


class ImageItem {

    @SerializedName("url")
    var url = ""

    @SerializedName("category_identifier")
    var categoryIdentifier = ""
}