package remotedealer.model.stock

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import java.io.Serializable


class RDMStocksItem : Serializable {

    @SerializedName("stock_id")
     var stockId: Int? = null

    @SerializedName("stock_source")
     var stockSource = ""

    @SerializedName("posted_date")
     var postedDate = ""

    @SerializedName("vehicle_make")
     var vehicleMake = ""

    @SerializedName("vehicle_model")
     var vehicleModel = ""

    @SerializedName("vehicle_variant")
     var vehicleVariant = ""

    @SerializedName("reg_month")
     var regMonth = ""

    @SerializedName("reg_year")
     var regYear = ""

    @SerializedName("registraion_city")
     var registraionCity = ""

    @SerializedName("registration_number")
     var registrationNumber = ""

    @SerializedName("colour")
     var colour = ""

    @SerializedName("kilometer")
     var kilometer: Int? = null

    @SerializedName("owner")
     var owner: Int? = null

    @SerializedName("insurance")
     var insurance = ""

    @SerializedName("insurance_exp_date")
     var insuranceExpDate = ""

    @SerializedName("selling_price")
     var sellingPrice: Int? = null

    @SerializedName("certified_text")
     var certifiedText = ""

    @SerializedName("certification_number")
     var certificationNumber = ""

    @SerializedName("warranty_recommended")
     var warrantyRecommended = ""

    @SerializedName("photo_count")
     var photoCount: Int? = null

    @SerializedName("surveyor_code")
     var surveyorCode = ""

    @SerializedName("surveyor_modified_date")
     var surveyorModifiedDate = ""

    @SerializedName("surveyor_kilometer")
     var surveyorKilometer: Any? = null

    @SerializedName("surveyor_remark")
     var surveyorRemark = ""

    @SerializedName("dealer_code")
     var dealerCode = ""

    @SerializedName("is_certified")
     var isCertified = ""

    @SerializedName("is_featured_car")
     var isFeaturedCar = ""

    @SerializedName("fuel_type")
     var fuelType = ""

    @SerializedName("bought_price")
     var boughtPrice: Int? = null

    @SerializedName("refurbishment_cost")
     var refurbishmentCost: Int? = null

    @SerializedName("procurement_executive_id")
     var procurementExecutiveId: Int? = null

    @SerializedName("procurement_executive_name")
     var procurementExecutiveName = ""

    @SerializedName("cng_kit")
     var cngKit = ""

    @SerializedName("chassis_number")
     var chassisNumber = ""

    @SerializedName("engine_number")
     var engineNumber = ""

    @SerializedName("is_booked")
     var isBooked = ""

    @SerializedName("private_vehicle")
     var privateVehicle = ""

    @SerializedName("comments")
     var comments = ""

    @SerializedName("finance_required")
     var financeRequired = ""

    @SerializedName("manufacture_month")
     var manufactureMonth = ""

    @SerializedName("dealer_price")
     var dealerPrice: Int? = null

    @SerializedName("sales_executive_id")
     var salesExecutiveId: Int? = null

    @SerializedName("sales_executive_name")
     var salesExecutiveName = ""

    @SerializedName("is_offload")
     var isOffload = ""

    @SerializedName("manufacture_year")
     var manufactureYear = ""

    @SerializedName("actual_selling_price")
     var actualSellingPrice = ""

    @SerializedName("images")
     var images: List<ImageItem>? = null

    @SerializedName("cover_image")
     var coverImage = ""

    @SerializedName("video_url")
     var videoUrl: Any? = null

    @SerializedName("lead_import")
     var leadImport: Any? = null

    @SerializedName("is_sold")
     var isSold = ""
}