package remotedealer.model.stock

import com.google.gson.annotations.SerializedName

class RDMStocksRequest {

  /*  @SerializedName("DateField")
    var DateField = ""

    @SerializedName("DateTo")
    var DateTo = ""

    @SerializedName("DateFrom")
    var DateFrom = ""*/

    @SerializedName("OrderBy")
    var orderBy = ""

    @SerializedName("OrderByReverse")
    var orderByReverse = ""

    @SerializedName("Page")
    var page = ""

    @SerializedName("PageItems")
    var pageItem = ""

    @SerializedName("wherein")
    var whereIn = ArrayList<WhereIn>()

    @SerializedName("where")
    var whereList = ArrayList<Where>()

    class WhereIn {
        @SerializedName("column")
        var column = ""

        @SerializedName("values")
        var values = arrayOf<String>()
    }

    class Where {

        @SerializedName("column")
        var column = ""

        @SerializedName("operator")
        var operator = ""

        @SerializedName("value")
        var value = ""

    }
}