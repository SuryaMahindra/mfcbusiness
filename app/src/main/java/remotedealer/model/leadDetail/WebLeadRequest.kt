package remotedealer.model.leadDetail

import com.google.gson.annotations.SerializedName

data class WebLeadRequest(
        @SerializedName("filter_by_fields") val filter_by_fields : String,
        @SerializedName("per_page") val per_page : Int,
        @SerializedName("page") val page : Int,
        @SerializedName("tag") val tag : String,
        @SerializedName("alias_fields") val alias_fields : String,
        @SerializedName("custom_where") val custom_where : List<Custom_where>,
        @SerializedName("access_token") val access_token : String
)
data class Custom_where (

        @SerializedName("column") val column : String,
        @SerializedName("operator") val operator : String,
        @SerializedName("value") val value : String
)