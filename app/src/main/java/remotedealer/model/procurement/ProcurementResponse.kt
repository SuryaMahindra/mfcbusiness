package remotedealer.model.procurement

import com.google.gson.annotations.SerializedName

class ProcurementResponse {

    @SerializedName("data")
    var data = ArrayList<ProcureItem>()

}