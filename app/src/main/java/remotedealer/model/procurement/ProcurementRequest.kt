package remotedealer.model.procurement

import com.google.gson.annotations.SerializedName


/*
 * Created By Udaya(MFCWL) ->  29-11-2020 20:15
 *
 */
class ProcurementRequest {

    @SerializedName("code")
    var code = ""

    @SerializedName("from_date")
    var fromDate = ""

    @SerializedName("to_date")
    var toDate = ""
}