package remotedealer.model.procurement

import com.google.gson.annotations.SerializedName

class ProcureItem {

    @SerializedName("particulars")
    var particulars = ""

    @SerializedName("oms")
    var oms = ""

    @SerializedName("rdr")
    var rdr = ""

    @SerializedName("target")
    var target = ""
}