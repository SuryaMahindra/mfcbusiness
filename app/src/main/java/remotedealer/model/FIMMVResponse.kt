package remotedealer.model

import com.google.gson.annotations.SerializedName

class FIMMVResponse {

    @SerializedName("status")
    var status = ""

    @SerializedName("message")
    var message = ""

    @SerializedName("data")
    var data: List<String>? = null

}