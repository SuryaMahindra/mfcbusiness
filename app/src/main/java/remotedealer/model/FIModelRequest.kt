package remotedealer.model

import com.google.gson.annotations.SerializedName

class FIModelRequest {

    @SerializedName("executiveid")
    var exeId = ""

    @SerializedName("make")
    var make = ""

}