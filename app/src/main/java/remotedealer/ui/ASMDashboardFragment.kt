package remotedealer.ui

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.mfcwl.mfc_dealer.R
import com.mfcwl.mfc_dealer.Utility.AppConstants
import com.mfcwl.mfc_dealer.Utility.AppConstants.DEALER_CODE
import com.mfcwl.mfc_dealer.Utility.CommonMethods
import com.mfcwl.mfc_dealer.Utility.SpinnerManager
import kotlinx.android.synthetic.main.asm_dashboardfragment.*
import kotlinx.android.synthetic.main.asm_dashboardfragment.view.*
import remotedealer.model.DealerItem
import remotedealer.model.dashboard.*
import remotedealer.model.token.RDRTokenRequest
import remotedealer.model.token.RDRTokenResponse
import remotedealer.rdrform.ui.RDMHomeActivity
import remotedealer.retrofit.RetroBase
import remotedealer.retrofit.RetroBase.Companion.URL_END_DASHBOARD_ASMMETRICS
import remotedealer.retrofit.RetroBase.Companion.URL_END_DASHBOARD_DEALERLIST
import remotedealer.retrofit.RetroBase.Companion.URL_END_DASHBOARD_RAGSTATUS
import remotedealer.retrofit.RetroBase.Companion.URL_END_DASHBOARD_RDRSTATUS
import remotedealer.retrofit.RetroBase.Companion.URL_END_GET_TOKEN
import remotedealer.retrofit.RetroBase.Companion.retroInterface
import remotedealer.util.RedirectionConstants.Companion.DEALER_DASHBOARD
import remotedealer.util.RedirectionConstants.Companion.REDIRECT_KEY
import remotedealer.util.RedirectionConstants.Companion.REDIRECT_PROCUREMENT
import remotedealer.util.RedirectionConstants.Companion.REDIRECT_SALES_AND_LEADS
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ASMDashboardFragment : Fragment(), ASMDashboardAdapter.ChangeDealerStatus {
    var TAG = javaClass.simpleName
    lateinit var mView: View
    private var parentLinearLayout: LinearLayout? = null
    private var parentMetricsLayout: LinearLayout? = null
    var redcountstatus = 0
    var greencountstatus = 0
    var ambercountstatus = 0
    lateinit var metricsList: ArrayList<ASMMetricsResponse.OverallMetric>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.i(TAG, "onCreateView: ")

        mView = inflater.inflate(R.layout.asm_dashboardfragment, container, false)
        parentLinearLayout = mView.findViewById(R.id.dynamicData)
        parentMetricsLayout = mView.findViewById(R.id.llmetrics)
        metricsList = ArrayList()

        mView.lldownarrow.setOnClickListener { view ->
            mView.dealerslist.toggleVisibility()
            mView.lldownarrow.toggleVisibility()
        }

        mView.llUpArrow.setOnClickListener { view ->
            mView.dealerslist.toggleVisibility()
            mView.lldownarrow.toggleVisibility()
        }

        return mView
    }

    fun View.toggleVisibility() {
        if (visibility == View.VISIBLE) {
            visibility = View.GONE
        } else {
            visibility = View.VISIBLE
        }
    }

    override fun onResume() {
        super.onResume()
        callGetToken()
    }

    private fun callGetToken() {
        retroInterface.getFromWeb(RDRTokenRequest(), URL_END_GET_TOKEN).enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                try {
                    val strRes = Gson().toJson(response.body())
                    val resModel = Gson().fromJson(strRes, RDRTokenResponse::class.java)
                    CommonMethods.setvalueAgainstKey(context as Activity?, "AMToken", resModel.token)
                    Log.i(TAG, "onResponse: " + resModel.token)
                    CommonMethods.setvalueAgainstKey(context as Activity?, "asmBearer", resModel.token)
                    getASMMetrics(resModel.token)
                    getRAGStatus(resModel.token)
                    getDealersList(resModel.token)
                } catch (e: Exception) {
                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                t.printStackTrace()
            }
        })

    }

    private fun getRAGStatus(token: String) {
        SpinnerManager.showSpinner(activity)
        val req = RAGStatusRequest()
        req.asmId = CommonMethods.getstringvaluefromkey(context as Activity?, "user_id")
        req.date = getTodayDate()
        retroInterface.getFromWeb(req, URL_END_DASHBOARD_RAGSTATUS, "Bearer $token").enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                SpinnerManager.hideSpinner(activity)
                try {
                    val strRes = Gson().toJson(response.body())
                    val modelRes = Gson().fromJson(strRes, RAGStatusResponse::class.java)
                    Log.i(TAG, "onResponse: ${modelRes.data.size}")
                    getStatusCount(modelRes.data)
                    showDealers(modelRes.data)
                } catch (e: Exception) {
                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(activity)
                t.printStackTrace()
            }

        })
    }

    private fun getASMMetrics(token: String) {
        SpinnerManager.showSpinner(activity)
        val req = RAGStatusRequest()
        req.asmId = CommonMethods.getstringvaluefromkey(context as Activity?, "user_id")
        req.date = getTodayDate()
        retroInterface.getFromWeb(req, URL_END_DASHBOARD_ASMMETRICS, "Bearer " + token).enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                SpinnerManager.hideSpinner(activity)
                try {
                    val strRes = Gson().toJson(response.body())
                    val modelRes = Gson().fromJson(strRes, ASMMetricsResponse::class.java)
                    Log.i(TAG, "onResponse: ${modelRes.overallMetric.size}")
                    showMetrics(modelRes)
                } catch (e: Exception) {
                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(activity)
                t.printStackTrace()
            }

        })
    }

    fun getDealersList(token: String) {
        SpinnerManager.showSpinner(activity)
        retroInterface.getFromWeb(URL_END_DASHBOARD_DEALERLIST + "?asmId=" + CommonMethods.getstringvaluefromkey(context as Activity?, "user_id"), "Bearer " + token).enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                SpinnerManager.hideSpinner(activity)
                try {
                    val strRes = Gson().toJson(response.body())

                    val modelRes = Gson().fromJson(strRes, DealerListResponse::class.java)
                    Log.i(TAG, "onResponse: ${modelRes.data.size}")

                    getRDRStatus(token, modelRes.data)


                } catch (e: Exception) {
                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(activity)
                t.printStackTrace()
            }

        })
    }

    fun getRDRStatus(token: String, data: ArrayList<DealerItem>) {
        SpinnerManager.showSpinner(activity)
        retroInterface.getFromWeb(URL_END_DASHBOARD_RDRSTATUS + "?asmId=" + CommonMethods.getstringvaluefromkey(context as Activity?, "user_id"), "Bearer $token").enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                SpinnerManager.hideSpinner(activity)
                try {
                    val strRes = Gson().toJson(response.body())
                    val modelRes = Gson().fromJson(strRes, RDRStatusResponse::class.java)
                    Log.i(TAG, "onResponse: ${modelRes.data.size}")
                    attachDealerListAdapter(data, modelRes.data)
                } catch (e: Exception) {
                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(activity)
                t.printStackTrace()
            }

        })
    }

    private fun getStatusCount(data: ArrayList<RAGStatusData>) {
        redcountstatus = 0
        ambercountstatus = 0
        greencountstatus = 0
        for (item in data) {
            if (item.today.equals("Red", ignoreCase = true)) {
                redcountstatus++
            }
            if (item.today.equals("Amber", ignoreCase = true)) {
                ambercountstatus++
            }

            if (item.today.equals("Green", ignoreCase = true)) {
                greencountstatus++
            }
        }

        greencount.text = "" + greencountstatus
        ambercount.text = "" + ambercountstatus
        redcount.text = "" + redcountstatus


    }

    private fun showMetrics(data: ASMMetricsResponse) {
        try {
            metricsList.clear()
            metricsList = data.overallMetric as ArrayList<ASMMetricsResponse.OverallMetric>
            parentMetricsLayout!!.removeAllViews()
            for (item in metricsList) {
                val mainview: View = layoutInflater.inflate(R.layout.metric_layout, null)
                var txtname = mainview.findViewById<View>(R.id.metricName) as TextView
                var txtpercentage = mainview.findViewById<View>(R.id.percenatge) as TextView
                var txtactual = mainview.findViewById<View>(R.id.actual) as TextView
                var txttarget = mainview.findViewById<View>(R.id.target) as TextView
                var expand = mainview.findViewById<View>(R.id.expand) as ImageView
                var collapse = mainview.findViewById<View>(R.id.collapse) as ImageView
                var innerLayout = mainview.findViewById<View>(R.id.llinnerlayout) as LinearLayout

                if (item.metric.equals("TotalProc", ignoreCase = true)) {
                    for (innerdata in data.totalProc) {
                        val view: View = layoutInflater.inflate(R.layout.asm_metrics_dealer_row, null)
                        var txtname = view.findViewById<View>(R.id.txtdealername) as TextView
                        var txtper = view.findViewById<View>(R.id.txtpercentage) as TextView
                        var txtact = view.findViewById<View>(R.id.txtactual) as TextView
                        var txttg = view.findViewById<View>(R.id.txttarget) as TextView
                        var rightArrow = view.findViewById<View>(R.id.llrightarros) as ImageView
                        rightArrow.setOnClickListener {
                            val intent = Intent(activity, RDMHomeActivity::class.java)
                            intent.putExtra(AppConstants.DEALER_NAME, "" + innerdata.dealerName)
                            intent.putExtra("Status", "" + innerdata.dealerName)
                            intent.putExtra(REDIRECT_KEY, REDIRECT_PROCUREMENT)
                            intent.putExtra(DEALER_CODE, innerdata.dealerCode)

                            Log.i(TAG, "showDealers: " + innerdata.dealerCode)
                            startActivity(intent)
                        }
                        txtname.text = innerdata.dealerName
                        txtper.text = innerdata.percentage
                        setStatuscolor(txtper, innerdata.status)
                        txtact.text = innerdata.actual
                        txttg.text = innerdata.target
                        innerLayout?.addView(view)
                    }
                }

                if (item.metric.equals("OMSConversion", ignoreCase = true)) {
                    for (innerdata in data.oMSConversion) {
                        val view: View = layoutInflater.inflate(R.layout.asm_metrics_dealer_row, null)
                        var txtname = view.findViewById<View>(R.id.txtdealername) as TextView
                        var txtper = view.findViewById<View>(R.id.txtpercentage) as TextView
                        var txtact = view.findViewById<View>(R.id.txtactual) as TextView
                        var txttg = view.findViewById<View>(R.id.txttarget) as TextView
                        var rightArrow = view.findViewById<View>(R.id.llrightarros) as ImageView
                        rightArrow.setOnClickListener {
                            val intent = Intent(activity, RDMHomeActivity::class.java)
                            intent.putExtra(AppConstants.DEALER_NAME, "" + innerdata.dealerName)
                            intent.putExtra("Status", "" + innerdata.dealerName)
                            intent.putExtra(REDIRECT_KEY, REDIRECT_SALES_AND_LEADS)
                            intent.putExtra(DEALER_CODE, innerdata.dealerCode)
                            Log.i(TAG, "showDealers: " + innerdata.dealerCode)
                            startActivity(intent)
                        }
                        txtname.text = innerdata.dealerName
                        txtper.text = innerdata.percentage
                        setStatuscolor(txtper, innerdata.status)
                        txtact.text = innerdata.actual
                        txttg.text = innerdata.target
                        innerLayout?.addView(view)
                    }
                }
                if (item.metric.equals("TotalSales", ignoreCase = true)) {
                    for (innerdata in data.totalSales) {
                        val view: View = layoutInflater.inflate(R.layout.asm_metrics_dealer_row, null)
                        var txtname = view.findViewById<View>(R.id.txtdealername) as TextView
                        var txtper = view.findViewById<View>(R.id.txtpercentage) as TextView
                        var txtact = view.findViewById<View>(R.id.txtactual) as TextView
                        var txttg = view.findViewById<View>(R.id.txttarget) as TextView
                        var rightArrow = view.findViewById<View>(R.id.llrightarros) as ImageView
                        rightArrow.setOnClickListener {
                            val intent = Intent(activity, RDMHomeActivity::class.java)
                            intent.putExtra(AppConstants.DEALER_NAME, "" + innerdata.dealerName)
                            intent.putExtra("Status", "" + innerdata.dealerName)
                            intent.putExtra(REDIRECT_KEY, REDIRECT_SALES_AND_LEADS)
                            intent.putExtra(DEALER_CODE, innerdata.dealerCode)
                            Log.i(TAG, "showDealers: " + innerdata.dealerCode)
                            startActivity(intent)
                        }
                        txtname.text = innerdata.dealerName
                        txtper.text = innerdata.percentage
                        setStatuscolor(txtper, innerdata.status)
                        txtact.text = innerdata.actual
                        txttg.text = innerdata.target
                        innerLayout?.addView(view)
                    }
                }

                if (item.metric.equals("ProcContri", ignoreCase = true)) {
                    for (innerdata in data.procContri) {
                        val view: View = layoutInflater.inflate(R.layout.asm_metrics_dealer_row, null)
                        var txtname = view.findViewById<View>(R.id.txtdealername) as TextView
                        var txtper = view.findViewById<View>(R.id.txtpercentage) as TextView
                        var txtact = view.findViewById<View>(R.id.txtactual) as TextView
                        var txttg = view.findViewById<View>(R.id.txttarget) as TextView
                        var rightArrow = view.findViewById<View>(R.id.llrightarros) as ImageView
                        rightArrow.setOnClickListener {
                            val intent = Intent(activity, RDMHomeActivity::class.java)
                            intent.putExtra(AppConstants.DEALER_NAME, "" + innerdata.dealerName)
                            intent.putExtra("Status", "" + innerdata.dealerName)
                            intent.putExtra(REDIRECT_KEY, REDIRECT_PROCUREMENT)
                            intent.putExtra(DEALER_CODE, innerdata.dealerCode)
                            Log.i(TAG, "showDealers: " + innerdata.dealerCode)
                            startActivity(intent)
                        }
                        txtname.text = innerdata.dealerName
                        txtper.text = innerdata.percentage
                        setStatuscolor(txtper, innerdata.status)
                        txtact.text = innerdata.actual
                        txttg.text = innerdata.target
                        innerLayout?.addView(view)
                    }
                }

                if (item.metric.equals("ZeroStock", ignoreCase = true)) {
                    for (innerdata in data.zeroStock) {
                        val view: View = layoutInflater.inflate(R.layout.asm_metrics_dealer_row, null)
                        var txtname = view.findViewById<View>(R.id.txtdealername) as TextView
                        var txtper = view.findViewById<View>(R.id.txtpercentage) as TextView
                        var txtact = view.findViewById<View>(R.id.txtactual) as TextView
                        var txttg = view.findViewById<View>(R.id.txttarget) as TextView
                        var rightArrow = view.findViewById<View>(R.id.llrightarros) as ImageView
                        rightArrow.setOnClickListener {
                            val intent = Intent(activity, RDMHomeActivity::class.java)
                            intent.putExtra(AppConstants.DEALER_NAME, "" + innerdata.dealerName)
                            intent.putExtra("Status", "" + innerdata.dealerName)
                            intent.putExtra(REDIRECT_KEY, DEALER_DASHBOARD)
                            intent.putExtra(DEALER_CODE, innerdata.dealerCode)
                            Log.i(TAG, "showDealers: " + innerdata.dealerCode)
                            startActivity(intent)
                        }
                        txtname.text = innerdata.dealerName
                        txtper.text = innerdata.percentage
                        setStatuscolor(txtper, innerdata.status)
                        txtact.text = innerdata.actual
                        txttg.text = innerdata.target
                        innerLayout?.addView(view)
                    }
                }

                if (item.metric.equals("ZeroSales", ignoreCase = true)) {
                    for (innerdata in data.zeroSales) {
                        val view: View = layoutInflater.inflate(R.layout.asm_metrics_dealer_row, null)
                        var txtname = view.findViewById<View>(R.id.txtdealername) as TextView
                        var txtper = view.findViewById<View>(R.id.txtpercentage) as TextView
                        var txtact = view.findViewById<View>(R.id.txtactual) as TextView
                        var txttg = view.findViewById<View>(R.id.txttarget) as TextView
                        var rightArrow = view.findViewById<View>(R.id.llrightarros) as ImageView
                        rightArrow.setOnClickListener {
                            val intent = Intent(activity, RDMHomeActivity::class.java)
                            intent.putExtra(AppConstants.DEALER_NAME, "" + innerdata.dealerName)
                            intent.putExtra("Status", "" + innerdata.dealerName)
                            intent.putExtra(REDIRECT_KEY, DEALER_DASHBOARD)
                            intent.putExtra(DEALER_CODE, innerdata.dealerCode)
                            Log.i(TAG, "showDealers: " + innerdata.dealerCode)
                            startActivity(intent)
                        }
                        txtname.text = innerdata.dealerName
                        txtper.text = innerdata.percentage
                        setStatuscolor(txtper, innerdata.status)
                        txtact.text = innerdata.actual
                        txttg.text = innerdata.target
                        innerLayout?.addView(view)
                    }
                }

                if (item.metric.equals("ZeroOMSContribution", ignoreCase = true)) {
                    for (innerdata in data.zeroOMSContribution) {
                        val view: View = layoutInflater.inflate(R.layout.asm_metrics_dealer_row, null)
                        var txtname = view.findViewById<View>(R.id.txtdealername) as TextView
                        var txtper = view.findViewById<View>(R.id.txtpercentage) as TextView
                        var txtact = view.findViewById<View>(R.id.txtactual) as TextView
                        var txttg = view.findViewById<View>(R.id.txttarget) as TextView
                        var rightArrow = view.findViewById<View>(R.id.llrightarros) as ImageView
                        rightArrow.setOnClickListener {
                            val intent = Intent(activity, RDMHomeActivity::class.java)
                            intent.putExtra(AppConstants.DEALER_NAME, "" + innerdata.dealerName)
                            intent.putExtra("Status", "" + innerdata.dealerName)
                            intent.putExtra(REDIRECT_KEY, DEALER_DASHBOARD)
                            intent.putExtra(DEALER_CODE, innerdata.dealerCode)
                            Log.i(TAG, "showDealers: " + innerdata.dealerCode)
                            startActivity(intent)
                        }
                        txtname.text = innerdata.dealerName
                        txtper.text = innerdata.percentage
                        setStatuscolor(txtper, innerdata.status)
                        txtact.text = innerdata.actual
                        txttg.text = innerdata.target
                        innerLayout?.addView(view)
                    }
                }

                txtname.text = item.display.trim()
                txtpercentage.text = item.percentage
                txtactual.text = item.actual
                txttarget.text = item.target

                expand.setOnClickListener { view ->
                    innerLayout.toggleVisibility()
                    collapse.toggleVisibility()
                    expand.toggleVisibility()
                    mainview?.setBackgroundDrawable(resources.getDrawable(R.drawable.metrics_header))
                    mainview?.setMargin(5, 5, 5, 10)
                    innerLayout.setBackgroundColor(resources.getColor(R.color.white))


                }
                collapse.setOnClickListener { view ->
                    innerLayout.toggleVisibility()
                    collapse.toggleVisibility()
                    expand.toggleVisibility()
                    mainview?.setBackgroundColor(resources.getColor(R.color.white))
                    innerLayout.setBackgroundColor(resources.getColor(R.color.white))

                }

                parentMetricsLayout?.addView(mainview)
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun View.setMargin(left: Int? = null, top: Int? = null, right: Int? = null, bottom: Int? = null) {
        val params = (layoutParams as? ViewGroup.MarginLayoutParams)
        params?.setMargins(
                left ?: params.leftMargin,
                top ?: params.topMargin,
                right ?: params.rightMargin,
                bottom ?: params.bottomMargin)
        layoutParams = params
    }


    private fun showDealers(data: ArrayList<RAGStatusData>) {
        parentLinearLayout!!.removeAllViews()
        for (item in data) {
            val view: View = layoutInflater.inflate(R.layout.asm_dealer_row, null)
            var name = view.findViewById<View>(R.id.txtdealername) as TextView
            var todaystatus = view.findViewById<View>(R.id.statustoday) as TextView
            var lastweekstatus = view.findViewById<View>(R.id.statuslastweek) as TextView
            var rightarrow = view.findViewById<View>(R.id.llrightarros) as LinearLayout
            rightarrow.setOnClickListener {
                val intent = Intent(activity, RDMHomeActivity::class.java)
                intent.putExtra(AppConstants.DEALER_NAME, "" + name.text)
                intent.putExtra("Status", "" + todaystatus.text)
                intent.putExtra(REDIRECT_KEY, DEALER_DASHBOARD)
                intent.putExtra(DEALER_CODE, item.code)
                Log.i(TAG, "showDealers: " + item.code)
                startActivity(intent)
            }
            Log.i(TAG, "showDealers: " + item.code)
            name.text = item.name
            if (item.today.equals("Red", ignoreCase = true)) {
                todaystatus.background = resources.getDrawable(R.drawable.circle_red)
            }
            if (item.today.equals("Green", ignoreCase = true)) {
                todaystatus.background = resources.getDrawable(R.drawable.circle_green)

            }
            if (item.today.equals("Amber", ignoreCase = true)) {
                todaystatus.background = resources.getDrawable(R.drawable.circle_amber)

            }
           // setStatus(todaystatus, item.today)

            if (item.lastWeek.equals("Red", ignoreCase = true)) {
                lastweekstatus.background = resources.getDrawable(R.drawable.circle_red)
            }
            if (item.lastWeek.equals("Green", ignoreCase = true)) {
                lastweekstatus.background = resources.getDrawable(R.drawable.circle_green)

            }
            if (item.lastWeek.equals("Amber", ignoreCase = true)) {
                lastweekstatus.background = resources.getDrawable(R.drawable.circle_amber)

            }
            //setStatus(lastweekstatus, item.lastWeek)
            parentLinearLayout?.addView(view)
        }
    }

    @SuppressLint("WrongConstant")
    fun attachDealerListAdapter(data: ArrayList<DealerItem>, statusList: ArrayList<RDRStatusResponse.RDRStatusData>) {
        val adapter = ASMDashboardAdapter(data, statusList, this)
        mView.dealertableview.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
        mView.dealertableview.adapter = adapter
        mView.dealertableview.isNestedScrollingEnabled = false
        adapter.notifyDataSetChanged()
    }

    private fun setStatus(view: TextView, status: String) {
        if (status.equals("Red", ignoreCase = true)) {
            view.background = resources.getDrawable(R.drawable.circle_red)
        }
        if (status.equals("Green", ignoreCase = true)) {
            view.background = resources.getDrawable(R.drawable.circle_green)

        }
        if (status.equals("Amber", ignoreCase = true)) {
            view.background = resources.getDrawable(R.drawable.circle_amber)

        }

    }


    private fun setStatuscolor(view: TextView, status: String) {
        if (status.equals("Red", ignoreCase = true)) {
            view.setTextColor(resources.getColor(R.color.red))
        }
        if (status.equals("Green", ignoreCase = true)) {
            view.setTextColor(resources.getColor(R.color.completedcolor))
        }
        if (status.equals("Amber", ignoreCase = true)) {
            view.setTextColor(resources.getColor(R.color.graph_yellow))
        }

    }

    fun getTodayDate(): String? {
        val today = Calendar.getInstance().time
        val format = SimpleDateFormat("yyyy-MM-dd")
        val dateToStr = format.format(today)
        return dateToStr
    }

    override fun onStatusChange() {
        callGetToken()
    }

}
