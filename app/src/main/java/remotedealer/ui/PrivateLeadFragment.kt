//package remotedealer.ui
//
//import android.app.Activity
//import android.content.Context
//import android.content.SharedPreferences
//import android.graphics.Color
//import android.os.Bundle
//import android.util.Log
//import android.view.Gravity
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import android.widget.Button
//import android.widget.Toast
//import androidx.fragment.app.Fragment
//import androidx.recyclerview.widget.LinearLayoutManager
//import androidx.recyclerview.widget.RecyclerView
//import com.mfcwl.mfc_dealer.Activity.Leads.LeadConstants
//import com.mfcwl.mfc_dealer.Activity.Leads.LeadsInstance
//import com.mfcwl.mfc_dealer.Activity.MainActivity
//import com.mfcwl.mfc_dealer.Activity.SplashActivity
//import com.mfcwl.mfc_dealer.Controller.Application
//import com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragmentNew
//import com.mfcwl.mfc_dealer.Fragment.Leads.LeadSortCustomDialogClass
//import com.mfcwl.mfc_dealer.Fragment.Leads.NormalLeadsFragment
//import com.mfcwl.mfc_dealer.Model.Leads.PrivateLeadModel
//import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall
//import com.mfcwl.mfc_dealer.R
//import com.mfcwl.mfc_dealer.Utility.CommonMethods
//import com.mfcwl.mfc_dealer.Utility.GlobalText
//import org.json.JSONArray
//import org.json.JSONException
//import org.json.JSONObject
//import java.text.SimpleDateFormat
//import java.util.*
//import kotlin.math.ceil
//
//class PrivateLeadFragment : Fragment(){
//
//    lateinit var recyclerView: RecyclerView
//    lateinit var btnAllLeads:Button
//    lateinit var btnFollowUpLeads:Button
//    var mPrivateLeadsAdapter:PrivateLeadAdapter? = null
//
//    var countsPrivate = 1
//    var negotiationLeadCountPrivate = 0
//    var pageItemPrivate = 100
//    var page_noPrivate = "1"
//    var linearLayoutManager: LinearLayoutManager? = null
//    var lastVisibleItemPosition = 0
//    var totalItemCount:Int = 0
//    var myPriList: MutableList<PrivateLeadModel>? = null
//    lateinit var mSharedPreferences:SharedPreferences
//    private var whichLeadsPrivate = "allleads"
//    private var mPrivate:Boolean = false
//    private var flagRes:Boolean = false
//    var Flag1 = false
//
//
//    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        return inflater.inflate(R.layout.sales_web_and_private_leads_fragment, container, false)
//    }
//
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        btnAllLeads = view.findViewById(R.id.btnAllLeads)
//        btnFollowUpLeads = view.findViewById(R.id.btnFollowUpLeads)
//        recyclerView = view.findViewById(R.id.leads_recyclerview)
//        mSharedPreferences = requireActivity().getSharedPreferences("MFC", Context.MODE_PRIVATE)
//
//        mPrivateLeadsAdapter = PrivateLeadAdapter(requireActivity().parent, null)
//        recyclerView.setHasFixedSize(false)
//        val layoutManger = LinearLayoutManager(activity)
//        recyclerView.layoutManager = layoutManger
//        recyclerView.adapter = mPrivateLeadsAdapter
//
//        btnAllLeads.setOnClickListener { //NotificationInstance.getInstance().setTodayfollowdate("");
//            SplashActivity.progress = true
//            if (mPrivateLeadsAdapter != null) {
//                recyclerView.recycledViewPool.clear()
//                mPrivateLeadsAdapter!!.notifyDataSetChanged()
//            }
//            btnAllLeads.setTextColor(Color.BLACK)
//            btnAllLeads.setBackgroundResource(R.drawable.my_button)
//            btnFollowUpLeads.setBackgroundResource(R.drawable.my_buttom_gray_light)
//            btnFollowUpLeads.setTextColor(Color.parseColor("#b9b9b9"))
//            ShowAllLeads()
//        }
//
//        btnFollowUpLeads.setOnClickListener { //NotificationInstance.getInstance().setTodayfollowdate("");
//            SplashActivity.progress = true
//            if (mPrivateLeadsAdapter != null) {
//                recyclerView.recycledViewPool.clear()
//                mPrivateLeadsAdapter!!.notifyDataSetChanged()
//            }
//            btnFollowUpLeads.setTextColor(Color.BLACK)
//            btnFollowUpLeads.setBackgroundResource(R.drawable.my_button)
//            btnAllLeads.setBackgroundResource(R.drawable.my_buttom_gray_light)
//            btnAllLeads.setTextColor(Color.parseColor("#b9b9b9"))
//            ShowFollowUpLeads()
//        }
//
//
//        val nRecyclerViewOnScrollListener: RecyclerView.OnScrollListener = object : RecyclerView.OnScrollListener() {
//            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
//                super.onScrollStateChanged(recyclerView, newState)
//                if (newState == 2) {
//                    try {
//                        if (lastVisibleItemPosition + 1 == totalItemCount) {
//                            //Write you code here
//                            if (MainActivity.searchVal != "") {
//                                countsPrivate += 1
//                                val maxPageNumber = Math.ceil(negotiationLeadCountPrivate.toDouble() / pageItemPrivate.toDouble()) // d = 3.0
//                                if (countsPrivate > maxPageNumber) {
//                                    return
//                                }
//                                page_noPrivate = Integer.toString(countsPrivate)
//                                SearchingByPrivateLeads()
//                            } else {
//                            }
//                        }
//                    } catch (e: Exception) {
//                        e.printStackTrace()
//                    }
//                }
//            }
//
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                super.onScrolled(recyclerView, dx, dy)
//                totalItemCount = linearLayoutManager!!.itemCount
//                lastVisibleItemPosition = (recyclerView.layoutManager as LinearLayoutManager?)!!.findLastCompletelyVisibleItemPosition()
//            }
//        }
//
//        recyclerView.addOnScrollListener(nRecyclerViewOnScrollListener)
//
//
//        // We need to detect scrolling changes in the RecyclerView
//        recyclerView.setOnScrollListener(object : RecyclerView.OnScrollListener() {
//            // Keeps track of the overall vertical offset in the list
//            var verticalOffset = 0
//
//            // Determines the scroll UP/DOWN direction
//            var scrollingUp = false
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                verticalOffset += dy
//                scrollingUp = dy > 0
//                if (scrollingUp) {
//                    if (Flag1) {
//                        MainActivity.bottom_topAnimation()
//                        Flag1 = false
//                    }
//                } else {
//                    if (!Flag1) {
//                        MainActivity.top_bottomAnimation()
//                        Flag1 = true
//                    }
//                }
//            }
//        })
//
//    }
//
//
//    fun loadmore(count: Int) {
//
//        // LoginActivity.progress = false;
//        countsPrivate += 1
//        val maxPageNumber = ceil(negotiationLeadCountPrivate.toDouble() / pageItemPrivate.toDouble()) // d = 3.0
//        if (countsPrivate > maxPageNumber) {
//            // PrivateLeadsAdapter.progress.setVisibility(View.GONE);
//            /*Snackbar.make(viewsprivateLeads, "End of results", Snackbar.LENGTH_SHORT)
//                    .setAction("Action", null).show();*/
//            //ShowSnackBar("End of results");
//            return
//        }
//        // PrivateLeadsAdapter.progress.setVisibility(View.GONE);
//        page_noPrivate = countsPrivate.toString()
//        CommonMethods.setvalueAgainstKey(activity, "loadPri", "true")
//        if (CommonMethods.isInternetWorking(activity)) {
//            if (CommonMethods.getstringvaluefromkey(activity, "moredata").equals("moredata", ignoreCase = true)) {
//                /*Snackbar.make(viewsprivateLeads, "Scroll down to see more...", Snackbar.LENGTH_SHORT)
//                        .setAction("Action", null).show();*/
//                //ShowSnackBar("Scroll down to see more...");
//                WebServicesCall.webCall(activity, activity, jsonMake(), "PrivateLeadStore", GlobalText.POST)
//            } else {
//                val toast = Toast.makeText(activity, "No More Data.", Toast.LENGTH_LONG)
//                toast.setGravity(Gravity.CENTER, 0, 0)
//                toast.show()
//            }
//        }
//    }
//    private fun ShowAllLeads() {
//        myPriList?.clear()
//        countsPrivate = 1
//        negotiationLeadCountPrivate = 0
//        pageItemPrivate = 100
//        page_noPrivate = "1"
//        whichLeadsPrivate = "allleads"
//        SortByPrivateLeads()
//    }
//
//    private fun ShowFollowUpLeads() {
//        myPriList?.clear()
//        countsPrivate = 1
//        negotiationLeadCountPrivate = 0
//        pageItemPrivate = 100
//        page_noPrivate = "1"
//        whichLeadsPrivate = "followleads"
//        SortByPrivateLeads()
//    }
//
//    fun jsonMake(): JSONObject {
//        val jObj = JSONObject()
//        val jsonArray = JSONArray()
//        val jsondateArray = JSONArray()
//        val jObj1 = JSONObject()
//        val jObj2 = JSONObject()
//        val jObj3 = JSONObject()
//        val jObj4 = JSONObject()
//        val jObj5 = JSONObject()
//        val jObj6 = JSONObject()
//        val jObj7 = JSONObject()
//        val jObj8 = JSONObject()
//        val jObj9 = JSONObject()
//        val jObj10 = JSONObject()
//        val jObjleads = JSONObject()
//        val jObjcode = JSONObject()
//        val jObj11 = JSONObject()
//        val jObj12 = JSONObject()
//        val jObj13 = JSONObject()
//        val jObjmake = JSONObject()
//        val jObjmodel = JSONObject()
//        val jObjvariant = JSONObject()
//        val jObjname = JSONObject()
//        val jObjmobile = JSONObject()
//        val jsonSearchVal = JSONArray()
//        val statusArray = JSONArray()
//        val ltypeArray = JSONArray()
//        val codeArray = JSONArray()
//        statusArray.put("")
//        statusArray.put("HOT")
//        statusArray.put("WARM")
//        statusArray.put("OPEN")
//        statusArray.put("COLD")
//
//        var postdateinfo: String? = ""
//        try {
//            if (mSharedPreferences.getString(LeadConstants.LEAD_POST_TODAY, "") !== "") {
//                postdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_POST_TODAY, "")
//            }
//            if (mSharedPreferences.getString(LeadConstants.LEAD_POST_YESTER, "") !== "") {
//                postdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_POST_YESTER, "")
//            }
//            if (mSharedPreferences.getString(LeadConstants.LEAD_POST_7DAYS, "") !== "") {
//                postdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_POST_7DAYS, "")
//            }
//            if (mSharedPreferences.getString(LeadConstants.LEAD_POST_15DAYS, "") !== "") {
//                postdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_POST_15DAYS, "")
//            }
//            if (mSharedPreferences.getString(LeadConstants.LEAD_POST_CUSTDATE, "") !== "") {
//                postdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_POST_CUSTDATE, "")
//            }
//        } catch (e: java.lang.Exception) {
//            e.printStackTrace()
//        }
//        if (postdateinfo != null && postdateinfo.contains("@")) {
//            //split the date and atach to query
//            val createddates = postdateinfo.split("@").toTypedArray()
//            try {
//                if (mSharedPreferences.getString(LeadConstants.LEAD_POST_YESTER, "") !== "") {
//                    jObj2.put("column", "created_at")
//                    jObj2.put("operator", ">=")
//                    jObj3.put("operator", "<")
//                } else {
//                    jObj2.put("column", "created_at")
//                    jObj2.put("operator", ">=")
//                    jObj3.put("operator", "<=")
//                }
//                jObj2.put("value", createddates[0])
//                jObj3.put("column", "created_at")
//                jObj3.put("value", createddates[1])
//                jsondateArray.put(jObj2)
//                jsondateArray.put(jObj3)
//            } catch (e: java.lang.Exception) {
//                e.printStackTrace()
//            }
//        }
//        var followdateinfo: String? = ""
//        try {
//            if (mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_TMRW, "") !== "") {
//                followdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_TMRW, "")
//            }
//            if (mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_TODAY, "") !== "") {
//                followdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_TODAY, "")
//            }
//            if (mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_YESTER, "") !== "") {
//                followdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_YESTER, "")
//            }
//            if (mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_7DAYS, "") !== "") {
//                followdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_7DAYS, "")
//            }
//            if (mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_CUSTDATE, "") !== "") {
//                followdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_CUSTDATE, "")
//            }
//        } catch (e: java.lang.Exception) {
//            e.printStackTrace()
//        }
//        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equals("dealer", ignoreCase = true)) {
//            if (followdateinfo != null && followdateinfo.contains("@")) {
//                val follow = followdateinfo.split("@").toTypedArray()
//                try {
//                    if (mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_TMRW, "") !== "" || mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_TODAY, "") !== "" || mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_YESTER, "") !== "") {
//                        jObj4.put("column", "follow_date")
//                        jObj4.put("operator", "=")
//                        jObj4.put("value", follow[0])
//                        jsondateArray.put(jObj4)
//                    } else {
//                        jObj4.put("column", "follow_date")
//                        jObj4.put("operator", ">=")
//                        jObj5.put("operator", "<=")
//                        jObj4.put("value", follow[0])
//                        jObj5.put("column", "follow_date")
//                        jObj5.put("value", follow[1])
//                        jsondateArray.put(jObj4)
//                        jsondateArray.put(jObj5)
//                    }
//                } catch (e: java.lang.Exception) {
//                    e.printStackTrace()
//                }
//            }
//        } else {
//            try {
//                val startDate = CommonMethods.getstringvaluefromkey(activity, "startDate")
//                val endDate = CommonMethods.getstringvaluefromkey(activity, "endDate")
//                if (!startDate.equals("", ignoreCase = true)
//                        && !endDate.equals("", ignoreCase = true)) {
//                    jObj4.put("column", "created_at")
//                    jObj4.put("operator", ">=")
//                    jObj5.put("operator", "<=")
//                    jObj4.put("value", startDate)
//                    jObj5.put("column", "created_at")
//                    jObj5.put("value", endDate)
//                    jsondateArray.put(jObj4)
//                    jsondateArray.put(jObj5)
//                    jObj.put("where", jsondateArray)
//                }
//            } catch (e: java.lang.Exception) {
//                e.printStackTrace()
//            }
//        }
//        try {
//            jObj.put("PageItems", Integer.toString(pageItemPrivate))
//            jObj.put("Page", page_noPrivate)
//            if (whichLeadsPrivate == "followleads") {
//                if (CommonMethods.getstringvaluefromkey(activity, "user_type").equals("dealer", ignoreCase = true)) {
//                    jObj1.put("column", "lead_status")
//                    if (LeadFilterFragmentNew.leadstatusJsonArray.length() != 0) {
//                        jObj1.put("values", LeadFilterFragmentNew.leadstatusJsonArray)
//                    } else {
//                        jObj1.put("values", statusArray)
//                    }
//                    jsonArray.put(jObj1)
//                    jObj.put("wherein", jsonArray)
//                } else {
//                    if (CommonMethods.getstringvaluefromkey(activity, "leadStatus").equals("true", ignoreCase = true)) {
//                        //remove if already presents in json arry
//                        if (leadstatusJsonArray.length() != 0) {
//                            try {
//                                for (i in 0 until leadstatusJsonArray.length()) {
//                                    leadstatusJsonArray.remove(i)
//                                }
//                            } catch (e: java.lang.Exception) {
//                                e.printStackTrace()
//                            }
//                        }
//                        LeadFilterFragmentNew.leadstatusJsonArray.put(CommonMethods.getstringvaluefromkey(activity, "Lvalue"))
//                        jObj1.put("column", "lead_status")
//                        if (LeadFilterFragmentNew.leadstatusJsonArray.length() != 0) {
//                            jObj1.put("values", LeadFilterFragmentNew.leadstatusJsonArray)
//                        } else {
//                            jObj1.put("values", statusArray)
//                        }
//                        jsonArray.put(jObj1)
//                        jObj.put("wherein", jsonArray)
//                    } else {
//                        jObj1.put("column", "lead_status")
//                        if (LeadFilterFragmentNew.leadstatusJsonArray.length() != 0) {
//                            jObj1.put("values", LeadFilterFragmentNew.leadstatusJsonArray)
//                        } else {
//                            jObj1.put("values", statusArray)
//                        }
//                        jsonArray.put(jObj1)
//                        jObj.put("wherein", jsonArray)
//                    }
//                }
//            } else {
//                if (CommonMethods.getstringvaluefromkey(activity, "user_type").equals("dealer", ignoreCase = true)) {
//                    jObj10.put("column", "lead_status")
//                    if (LeadFilterFragmentNew.leadstatusJsonArray.length() != 0) {
//                        jObj10.put("values", LeadFilterFragmentNew.leadstatusJsonArray)
//                        jsonArray.put(jObj10)
//                        jObj.put("wherein", jsonArray)
//                    } else {
//                        jObj.put("wherein", jsonArray)
//                    }
//                } else {
//                    if (LeadFilterFragmentNew.leadstatusJsonArray.length() != 0) {
//                        try {
//                            for (i in 0 until LeadFilterFragmentNew.leadstatusJsonArray.length()) {
//                                LeadFilterFragmentNew.leadstatusJsonArray.remove(i)
//                            }
//                        } catch (e: java.lang.Exception) {
//                            e.printStackTrace()
//                        }
//                    }
//                    LeadFilterFragmentNew.leadstatusJsonArray.put(CommonMethods.getstringvaluefromkey(activity, "Lvalue"))
//                    if (!CommonMethods.getstringvaluefromkey(activity, "Lvalue").equals("", ignoreCase = true)) {
//                        jObj1.put("column", "lead_status")
//                        if (LeadFilterFragmentNew.leadstatusJsonArray.length() != 0) {
//                            jObj1.put("values", LeadFilterFragmentNew.leadstatusJsonArray)
//                        } else {
//                            jObj1.put("values", statusArray)
//                        }
//                        jsonArray.put(jObj1)
//                    }
//                    jObj.put("wherein", jsonArray)
//                }
//
//
//                //
//
//                // lead types
//                ltypeArray.put("SALES")
//                jObjleads.put("column", "lead_type")
//                jObjleads.put("values", ltypeArray)
//                jsonArray.put(jObjleads)
//                jObj.put("wherein", jsonArray)
//
//                // lead types
//                codeArray.put(CommonMethods.getstringvaluefromkey(activity, "dealer_code"))
//                jObjcode.put("column", "dealer_id")
//                jObjcode.put("values", codeArray)
//                jsonArray.put(jObjcode)
//                jObj.put("wherein", jsonArray)
//            }
//            if (LeadSortCustomDialogClass.leadsortbystatus.equals("posted_date_l_o", ignoreCase = true)) {
//                jObj.put("OrderBy", "created_at")
//                jObj.put("OrderByReverse", "true")
//            } else if (LeadSortCustomDialogClass.leadsortbystatus.equals("posted_date_o_l", ignoreCase = true)) {
//                jObj.put("OrderBy", "created_at")
//                jObj.put("OrderByReverse", "false")
//            } else if (LeadSortCustomDialogClass.leadsortbystatus.equals("folowup_date_l_o", ignoreCase = true)) {
//                jObj.put("OrderBy", "follow_date")
//                jObj.put("OrderByReverse", "true")
//            } else if (LeadSortCustomDialogClass.leadsortbystatus.equals("folowup_date_o_l", ignoreCase = true)) {
//                jObj.put("OrderBy", "follow_date")
//                jObj.put("OrderByReverse", "false")
//            } else {
//                jObj.put("OrderBy", "created_at")
//                jObj.put("OrderByReverse", "true")
//            }
//            if (whichLeadsPrivate == "followleads") {
//                val d = Calendar.getInstance().time
//                val df = SimpleDateFormat("yyyy-MM-dd")
//                val formattedDate = df.format(d)
//                jObj11.put("column", "follow_date")
//                jObj11.put("operator", "<=")
//                jObj11.put("value", formattedDate)
//                jsondateArray.put(jObj11)
//            }
//            if (MainActivity.searchVal != "") {
//                jObjmake.put("column", "make")
//                jObjmake.put("operator", "like")
//                jObjmake.put("value", MainActivity.searchVal.trim { it <= ' ' })
//                jsonSearchVal.put(jObjmake)
//                jObjmodel.put("column", "model")
//                jObjmodel.put("operator", "like")
//                jObjmodel.put("value", MainActivity.searchVal.trim { it <= ' ' })
//                jsonSearchVal.put(jObjmodel)
//                jObjvariant.put("column", "variant")
//                jObjvariant.put("operator", "like")
//                jObjvariant.put("value", MainActivity.searchVal.trim { it <= ' ' })
//                jsonSearchVal.put(jObjvariant)
//                jObjname.put("column", "customer_name")
//                jObjname.put("operator", "like")
//                jObjname.put("value", MainActivity.searchVal.trim { it <= ' ' })
//                jsonSearchVal.put(jObjname)
//                jObjmobile.put("column", "customer_mobile")
//                jObjmobile.put("operator", "like")
//                jObjmobile.put("value", MainActivity.searchVal.trim { it <= ' ' })
//                jsonSearchVal.put(jObjmobile)
//            }
//            jObj.put("where_or", jsonSearchVal)
//            jObj.put("where", jsondateArray)
//        } catch (e: JSONException) {
//            e.printStackTrace()
//        }
//        // Log.e("ParseleadstorePrivate ", "Request " + jObj.toString());
//        return jObj
//    }
//
//
//    fun ParsePrivateleadstore(jObj: JSONObject, activity: Activity?) {
//        Log.e("ParseleadstorePrivate ", "Response $jObj")
//        if (CommonMethods.getstringvaluefromkey(activity, "loadPri").equals("true", ignoreCase = true)) {
//            myPriList?.size?.minus(1)?.let { mPrivateLeadsAdapter?.notifyItemInserted(it) }
//        }
//        if (CommonMethods.getstringvaluefromkey(activity, "loadPri").equals("false", ignoreCase = true)) {
//            myPriList = ArrayList()
//        } else {
//            if (myPriList?.size?.minus(1) ?: 0 > 0) {
//                // myJobList.remove(myJobList.size()-1);
//            }
//        }
//        try {
//            val arrayList = jObj.getJSONArray("data")
//            for (i in 0 until arrayList.length()) {
//                val jsonobject = JSONObject(arrayList.getString(i))
//                if (whichLeadsPrivate == "allleads") {
//                    val normalLeadModel = PrivateLeadModel(jsonobject.getString("id"),
//                            jsonobject.getString("lead_type"),
//                            jsonobject.getString("lead_title"),
//                            jsonobject.getString("lead_date"),
//                            jsonobject.getString("lead_category"),
//                            jsonobject.getString("lead_tags"),
//                            jsonobject.getString("lead_flag"),
//                            jsonobject.getString("customer_id"),
//                            jsonobject.getString("customer_prefix"),
//                            jsonobject.getString("customer_name"),
//                            jsonobject.getString("customer_mobile"),
//                            jsonobject.getString("customer_email"),
//                            jsonobject.getString("customer_comments"),
//                            jsonobject.getString("customer_xtras"),
//                            jsonobject.getString("customer_prefs_communication"),
//                            jsonobject.getString("area_code"),
//                            jsonobject.getString("city"),
//                            jsonobject.getString("state"),
//                            jsonobject.getString("area_mfc_branch"),
//                            jsonobject.getString("area_pincode"),
//                            jsonobject.getString("stock_id"),
//                            jsonobject.getString("stock_type"),
//                            jsonobject.getString("make"),
//                            jsonobject.getString("model"),
//                            jsonobject.getString("variant"),
//                            jsonobject.getString("stock_model_year"),
//                            jsonobject.getString("register_month"),
//                            jsonobject.getString("register_year"),
//                            jsonobject.getString("color"),
//                            jsonobject.getString("kms"),
//                            jsonobject.getString("owner"),
//                            jsonobject.getString("register_no"),
//                            jsonobject.getString("register_city"),
//                            jsonobject.getString("stock_vinno"),
//                            jsonobject.getString("insurance_type"),
//                            jsonobject.getString("stock_insurance"),
//                            jsonobject.getString("stock_expected_price"),
//                            jsonobject.getString("dealer_id"),
//                            jsonobject.getString("demand_for_dealer"),
//                            jsonobject.getString("dealer_quote"),
//                            jsonobject.getString("dealer_comments"),
//                            jsonobject.getString("lead_stage"),
//                            jsonobject.getString("lead_priority"),
//                            jsonobject.getString("lead_status"),
//                            jsonobject.getString("lead_source"),
//                            jsonobject.getString("marketting_source"),
//                            jsonobject.getString("marketting_campaign"),
//                            jsonobject.getString("marketting_device"),
//                            jsonobject.getString("client_ip"),
//                            jsonobject.getString("follow_date"),
//                            jsonobject.getString("remark"),
//                            jsonobject.getString("associated_lead"),
//                            jsonobject.getString("ref_id"),
//                            jsonobject.getString("ref_src"),
//                            jsonobject.getString("status"),
//                            jsonobject.getString("comments"),
//                            jsonobject.getString("created_by"),
//                            jsonobject.getString("updated_by"),
//                            jsonobject.getString("deleted_at"),
//                            jsonobject.getString("created_at"),
//                            jsonobject.getString("updated_at"),
//                            jsonobject.getString("manufacturing_month"),
//                            jsonobject.getString("executive_name"),
//                            jsonobject.getString("executive_id"),
//                            jsonobject.getString("Vehicle_Type"),
//                            jsonobject.getString("lead_date2"),
//                            jsonobject.getString("created_by_device"),
//                            jsonobject.getString("updated_by_device"))
//                    myPriList?.add(normalLeadModel)
//                } else {
//                    val normalLeadModel = PrivateLeadModel(jsonobject.getString("id"),
//                            jsonobject.getString("lead_type"),
//                            jsonobject.getString("lead_title"),
//                            jsonobject.getString("lead_date"),
//                            jsonobject.getString("lead_category"),
//                            jsonobject.getString("lead_tags"),
//                            jsonobject.getString("lead_flag"),
//                            jsonobject.getString("customer_id"),
//                            jsonobject.getString("customer_prefix"),
//                            jsonobject.getString("customer_name"),
//                            jsonobject.getString("customer_mobile"),
//                            jsonobject.getString("customer_email"),
//                            jsonobject.getString("customer_comments"),
//                            jsonobject.getString("customer_xtras"),
//                            jsonobject.getString("customer_prefs_communication"),
//                            jsonobject.getString("area_code"),
//                            jsonobject.getString("city"),
//                            jsonobject.getString("state"),
//                            jsonobject.getString("area_mfc_branch"),
//                            jsonobject.getString("area_pincode"),
//                            jsonobject.getString("stock_id"),
//                            jsonobject.getString("stock_type"),
//                            jsonobject.getString("make"),
//                            jsonobject.getString("model"),
//                            jsonobject.getString("variant"),
//                            jsonobject.getString("stock_model_year"),
//                            jsonobject.getString("register_month"),
//                            jsonobject.getString("register_year"),
//                            jsonobject.getString("color"),
//                            jsonobject.getString("kms"),
//                            jsonobject.getString("owner"),
//                            jsonobject.getString("register_no"),
//                            jsonobject.getString("register_city"),
//                            jsonobject.getString("stock_vinno"),
//                            jsonobject.getString("insurance_type"),
//                            jsonobject.getString("stock_insurance"),
//                            jsonobject.getString("stock_expected_price"),
//                            jsonobject.getString("dealer_id"),
//                            jsonobject.getString("demand_for_dealer"),
//                            jsonobject.getString("dealer_quote"),
//                            jsonobject.getString("dealer_comments"),
//                            jsonobject.getString("lead_stage"),
//                            jsonobject.getString("lead_priority"),
//                            jsonobject.getString("lead_status"),
//                            jsonobject.getString("lead_source"),
//                            jsonobject.getString("marketting_source"),
//                            jsonobject.getString("marketting_campaign"),
//                            jsonobject.getString("marketting_device"),
//                            jsonobject.getString("client_ip"),
//                            jsonobject.getString("follow_date"),
//                            jsonobject.getString("remark"),
//                            jsonobject.getString("associated_lead"),
//                            jsonobject.getString("ref_id"),
//                            jsonobject.getString("ref_src"),
//                            jsonobject.getString("status"),
//                            jsonobject.getString("comments"),
//                            jsonobject.getString("created_by"),
//                            jsonobject.getString("updated_by"),
//                            jsonobject.getString("deleted_at"),
//                            jsonobject.getString("created_at"),
//                            jsonobject.getString("updated_at"),
//                            jsonobject.getString("manufacturing_month"),
//                            jsonobject.getString("executive_name"),
//                            jsonobject.getString("executive_id"),
//                            jsonobject.getString("Vehicle_Type"),
//                            jsonobject.getString("lead_date2"),
//                            jsonobject.getString("created_by_device"),
//                            jsonobject.getString("updated_by_device"))
//                    myPriList?.add(normalLeadModel)
//                }
//            }
//            negotiationLeadCountPrivate = jObj.getString("total").toString().toInt()
//        } catch (e: JSONException) {
//            e.printStackTrace()
//            Log.e("ParseleadstorePrivate ", "JSONException $e")
//        }
//        CommonMethods.setvalueAgainstKey(activity, "moredata", "moredata")
//        if (CommonMethods.getstringvaluefromkey(activity, "loadPri").equals("true", ignoreCase = true)) {
//            mPrivateLeadsAdapter?.notifyDataSetChanged()
//        } else {
//            CommonMethods.setvalueAgainstKey(activity, "loadPri", "true")
//            reloadData()
//        }
//
//    }
//
//    fun reloadData() {
//        mPrivateLeadsAdapter = PrivateLeadAdapter(requireActivity().parent, myPriList)
//        recyclerView.setHasFixedSize(false)
//        linearLayoutManager = LinearLayoutManager(activity)
//        recyclerView.layoutManager = linearLayoutManager
//        recyclerView.adapter = mPrivateLeadsAdapter
//    }
//
//    override fun onResume() {
//        super.onResume()
//        Application.getInstance().trackScreenView(activity, GlobalText.PrivateLead_Fragment)
//        if (whichLeadsPrivate == "allleads") {
//            btnAllLeads.setTextColor(Color.BLACK)
//            btnAllLeads.setBackgroundResource(R.drawable.my_button)
//            btnFollowUpLeads.setBackgroundResource(R.drawable.my_buttom_gray_light)
//            btnFollowUpLeads.setTextColor(Color.parseColor("#b9b9b9"))
//        } else {
//            btnFollowUpLeads.setTextColor(Color.BLACK)
//            btnFollowUpLeads.setBackgroundResource(R.drawable.my_button)
//            btnAllLeads.setBackgroundResource(R.drawable.my_buttom_gray_light)
//            btnAllLeads.setTextColor(Color.parseColor("#b9b9b9"))
//        }
//        if (mPrivateLeadsAdapter != null) {
//            recyclerView.recycledViewPool.clear()
//            recyclerView.stopScroll()
//            mPrivateLeadsAdapter!!.notifyDataSetChanged()
//        }
//        if (flagRes == true) {
//            if (!mPrivate) {
//                if (LeadsInstance.getInstance().whichleads == "PrivateLeads") {
//                    countsPrivate = 1
//                    negotiationLeadCountPrivate = 0
//                    pageItemPrivate = 100
//                    page_noPrivate = "1"
//                    WebServicesCall.webCall(activity, activity, jsonMake(), "PrivateLeadStore", GlobalText.POST)
//                }
//            } else {
//                mPrivate = false
//                //reloadData();
//                if (mPrivateLeadsAdapter != null) {
//                    mPrivateLeadsAdapter!!.notifyDataSetChanged()
//                }
//            }
//            flagRes = false
//        }
//    }
//
//
//    fun SortByPrivateLeads() {
//        if (myPriList != null) {
//            myPriList.orEmpty()
//        }
//        countsPrivate = 1
//        negotiationLeadCountPrivate = 0
//        pageItemPrivate = 100
//        page_noPrivate = "1"
//        WebServicesCall.webCall(activity, activity, jsonMake(), "PrivateLeadStore", GlobalText.POST)
//    }
//
//
//    class WrapContentLinearLayoutManager(activity: Activity?, horizontal: Int, b: Boolean) : LinearLayoutManager(activity) {
//        //... constructor
//        override fun onLayoutChildren(recycler: RecyclerView.Recycler, state: RecyclerView.State) {
//            try {
//                super.onLayoutChildren(recycler, state)
//            } catch (e: IndexOutOfBoundsException) {
//            }
//        }
//    }
//
//
//
//
//    fun SearchingByPrivateLeads() {
//        if (myPriList != null) {
//            myPriList.orEmpty()
//        }
//        countsPrivate = 1
//        negotiationLeadCountPrivate = 0
//        if (MainActivity.searchVal != "") {
//            pageItemPrivate = pageItemPrivate + 10
//        } else {
//            pageItemPrivate = 100
//        }
//        page_noPrivate = "1"
//        WebServicesCall.webCall(activity, activity, jsonMake(), "PrivateLeadStore", GlobalText.POST)
//    }
//}
