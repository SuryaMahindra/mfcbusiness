package remotedealer.ui

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import com.mfcwl.mfc_dealer.Activity.Leads.LeadDetailsActivity
import com.mfcwl.mfc_dealer.Activity.Leads.LeadsInstance
import com.mfcwl.mfc_dealer.Controller.Application
import com.mfcwl.mfc_dealer.Fragment.Leads.FollowupHistoryFragment
import com.mfcwl.mfc_dealer.R
import com.mfcwl.mfc_dealer.ResponseModel.PrivateLeadResponse
import com.mfcwl.mfc_dealer.Utility.CommonMethods
import com.mfcwl.mfc_dealer.Utility.GlobalText
import com.mfcwl.mfc_dealer.Utility.MonthUtility
import de.hdodenhof.circleimageview.CircleImageView
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

@SuppressLint("ValidFragment")
class LeadDetailFragment : Fragment {
    var lname: String
    var lmobile: String? = null
    var lemail: String? = null
    var lstatus: String? = null
    var lposteddate: String? = null
    var lmake: String? = null
    var lmodel: String? = null
    var lprice: String? = null
    var lregno: String? = null
    var lfollupdate: String? = null
    var lexename: String? = null
    var lregcity: String? = null
    var lowner: String? = null
    var lregyear: String? = null
    var lcolor: String? = null
    var lkms: String? = null
    var ldid: String? = null
    var remarks: String? = null
    var ldays: String? = null
    var Lvariant: String? = null
    var Lmfyear: String? = null

    lateinit var leads_details_next_followup: TextView
    lateinit var leads_details_exe_name: TextView
    lateinit var leads_details_name: TextView
    lateinit var leads_details_num: TextView
    lateinit var leads_details_email: TextView
    lateinit var leads_details_days: TextView
    lateinit var leads_details_hot: TextView
    lateinit var leads_details_model: TextView
    lateinit var leads_details_model_name: TextView
    lateinit var leads_details_price: TextView
    lateinit var leads_details_model_num: TextView
    lateinit var leads_details_message: TextView
    lateinit var leads_details_register_city_name: TextView
    lateinit var leads_details_color_which: TextView
    lateinit var leads_details_owner_name: TextView
    lateinit var leads_details_kms_which: TextView
    lateinit var leads_details_year_which: TextView
    lateinit var leads_details_fuel_which: TextView
    lateinit var leads_details_phonecall: CircleImageView
    lateinit var leads_details_messages: CircleImageView
    lateinit var leads_details_whatsapp: CircleImageView
    lateinit var cardview2: CardView
    lateinit var badgeid:LinearLayout
    var lost = false


    constructor(lname: String, lmobile: String?, lemail: String?, lstatus: String?, lposteddate: String?, lmake: String?, lmodel: String?, lprice: String?, lregno: String?, lfollupdate: String?, lexename: String?, lregcity: String?, lowner: String?, lregyear: String?, lcolor: String?, lkms: String?, ldid: String?, remarks: String?, ldays: String?, Lvariant: String?) {
        this.lname = lname
        this.lmobile = lmobile
        this.lemail = lemail
        this.lstatus = lstatus
        this.lposteddate = lposteddate
        this.lmake = lmake
        this.lmodel = lmodel
        this.lprice = lprice
        this.lregno = lregno
        this.lfollupdate = lfollupdate
        this.lexename = lexename
        this.lregcity = lregcity
        this.lowner = lowner
        this.lregyear = lregyear
        this.lcolor = lcolor
        this.lkms = lkms
        this.ldid = ldid
        this.remarks = remarks
        this.ldays = ldays
        this.Lvariant = Lvariant
    }

    constructor(lname: String, lmobile: String?, lemail: String?, lstatus: String?, lposteddate: String?, lmake: String?, lmodel: String?, lprice: String?, lregno: String?, lfollupdate: String?, lexename: String?, lregcity: String?, lowner: String?, lregyear: String?, lcolor: String?, lkms: String?, ldid: String?, remarks: String?, ldays: String?, Lvariant: String?, Lmfyear: String?) {
        this.lname = lname
        this.lmobile = lmobile
        this.lemail = lemail
        this.lstatus = lstatus
        this.lposteddate = lposteddate
        this.lmake = lmake
        this.lmodel = lmodel
        this.lprice = lprice
        this.lregno = lregno
        this.lfollupdate = lfollupdate
        this.lexename = lexename
        this.lregcity = lregcity
        this.lowner = lowner
        this.lregyear = lregyear
        this.lcolor = lcolor
        this.lkms = lkms
        this.ldid = ldid
        this.remarks = remarks
        this.ldays = ldays
        this.Lvariant = Lvariant
        this.Lmfyear = Lmfyear
    }

    @SuppressLint("ValidFragment")
    constructor(lname: String) {
        // Required empty public constructor
        this.lname = lname
    }

    var name = ""
    var TAG = javaClass.simpleName
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /*if (getArguments() != null) {
            name = getArguments().getString("paramsname");
            Log.e("getArguments ","getArguments "+name);
        }
        else
        {
            Log.e("getArguments ","getArguments null ");
        }*/
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Log.i(TAG, "onCreateView: ")
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.layout_leads_details, container, false)
        cardview2 = view.findViewById(R.id.cardview2)
        leads_details_next_followup = view.findViewById<TextView>(R.id.leads_details_next_followup)
        leads_details_exe_name = view.findViewById<TextView>(R.id.leads_details_exe_name)
        leads_details_name = view.findViewById<TextView>(R.id.leads_details_name)
        leads_details_num = view.findViewById<TextView>(R.id.leads_details_num)
        leads_details_email = view.findViewById<TextView>(R.id.leads_details_email)
        leads_details_days = view.findViewById<TextView>(R.id.leads_details_days)
        leads_details_hot = view.findViewById<TextView>(R.id.leads_details_hot)
        leads_details_model = view.findViewById<TextView>(R.id.leads_details_model)
        leads_details_model_name = view.findViewById<TextView>(R.id.leads_details_model_name)
        leads_details_price = view.findViewById<TextView>(R.id.leads_details_price)
        leads_details_model_num = view.findViewById<TextView>(R.id.leads_details_model_num)
        leads_details_message = view.findViewById<TextView>(R.id.leads_details_message)
        leads_details_register_city_name = view.findViewById<TextView>(R.id.leads_details_register_city_name)
        leads_details_color_which = view.findViewById<TextView>(R.id.leads_details_color_which)
        leads_details_owner_name = view.findViewById<TextView>(R.id.leads_details_owner_name)
        leads_details_kms_which = view.findViewById<TextView>(R.id.leads_details_kms_which)
        leads_details_year_which = view.findViewById<TextView>(R.id.leads_details_year_which)
        leads_details_fuel_which = view.findViewById<TextView>(R.id.leads_details_fuel_which)
        leads_details_phonecall = view.findViewById(R.id.leads_details_phonecall)
        leads_details_messages = view.findViewById(R.id.leads_details_messages)
        leads_details_whatsapp = view.findViewById(R.id.leads_details_whatsapp)
        badgeid = view.findViewById<LinearLayout>(R.id.badgeid)
        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equals("dealer", ignoreCase = true)) {
        } else {
            badgeid.setVisibility(View.INVISIBLE)
        }

        //name = getActivity().getIntent().getExtras().getString("NM");
        leads_details_name.setText(capitalize(lname))
        leads_details_num.setText(lmobile)
        leads_details_email.setText(lemail)
        leads_details_hot.setText(capitalize(lstatus))
        leads_details_model.setText(lmake)
        if (Lvariant == "null") {
            Lvariant = ""
        }
        leads_details_model_name.setText("$lmodel $Lvariant")
        leads_details_price.setText(lprice)
        leads_details_message.setText(remarks)
        if (remarks!!.trim { it <= ' ' } == "" || remarks!!.trim { it <= ' ' } == "NA") {
            leads_details_message.setText("No comments available")
        }
        leads_details_days.setText(getRequiredDateFormat(ldays))
        if (lstatus.equals("Open", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.open_28)
            //leads_details_hot.setVisibility(View.INVISIBLE);
        }
        if (lstatus.equals("", ignoreCase = true) || lstatus.equals("null", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.open_28)
            leads_details_hot.setText(" Open")
            //leads_details_hot.setVisibility(View.INVISIBLE);
        }
        if (lstatus.equals("Hot", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.hot_28)
            leads_details_hot.setVisibility(View.VISIBLE)
        }
        if (lstatus.equals("Warm", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.warm_28)
            leads_details_hot.setVisibility(View.VISIBLE)
        }
        if (lstatus.equals("Cold", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.cold_28)
            leads_details_hot.setVisibility(View.VISIBLE)
        }
        if (lstatus.equals("Lost", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.lost_28)
            leads_details_hot.setVisibility(View.VISIBLE)
            LeadDetailsActivity.Invisible()
        }
        if (lstatus.equals("Sold", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.sold_28)
            leads_details_hot.setVisibility(View.VISIBLE)
        }
        if (lstatus.equals("Visiting", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_visiting)
            leads_details_hot.setVisibility(View.VISIBLE)
        }
        if (lstatus.equals("Search", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_search_lead)
            leads_details_hot.setVisibility(View.VISIBLE)
        }
        if (lstatus.equals("Not-Interested", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_not_intrested)
            leads_details_hot.setVisibility(View.VISIBLE)
            LeadDetailsActivity.Invisible()
        }
        if (lstatus.equals("Finalised", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_finalized)
            leads_details_hot.setVisibility(View.VISIBLE)
            LeadDetailsActivity.Invisible()
        }
        if (lstatus.equals("No-Response", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_no_responce)
            leads_details_hot.setVisibility(View.VISIBLE)
        }
        try {
            lfollupdate = if (LeadsInstance.getInstance().whichleads == "WebLeads") {
                val splitdatespace = lfollupdate!!.split(" ").toTypedArray()
                val splitdate1 = splitdatespace[0].split("-").toTypedArray()
                splitdate1[2] + " " + MonthUtility.Month[splitdate1[1].toInt()] + " " + splitdate1[0]
            } else {
                val splitdatespace = lfollupdate!!.split(" ").toTypedArray()
                val splitdate1 = splitdatespace[0].split("-").toTypedArray()
                splitdate1[2] + " " + MonthUtility.Month[splitdate1[1].toInt()] + " " + splitdate1[0]
            }
            Log.e("leadfollow up date", lfollupdate!!)
        } catch (e: Exception) {
        }
        if (lexename?.trim { it <= ' ' }.equals("null", ignoreCase = true) || lexename?.trim { it <= ' ' }.equals("", ignoreCase = true)) {
            lexename = "NA"
        }
        if (lfollupdate != null) {
            if (lfollupdate!!.startsWith("00") || lfollupdate!!.startsWith("null") || lfollupdate == "") {
                lfollupdate = "NA"
            }
        } else {
            lfollupdate = "NA"
        }
        val sourceString1 = "Next Follow-up <br><font color=\"black\">$lfollupdate</font></br> "
        val sourceString2 = "Executive Name <br><font color=\"black\">$lexename</font></br> "
        leads_details_next_followup.setText(Html.fromHtml(sourceString1))
        leads_details_exe_name.setText(Html.fromHtml(sourceString2))
        if (lregcity == null || lregcity.equals("null", ignoreCase = true) || lregcity.equals("", ignoreCase = true)) {
            lregcity = "NA"
        }
        leads_details_register_city_name.setText(lregcity)
        if (lowner == null || lowner.equals("null", ignoreCase = true) || lowner.equals("", ignoreCase = true)) {
            lowner = "NA"
        }
        leads_details_owner_name.setText(lowner)
        //leads_details_year_which.setText(lregyear);
        if (lcolor == null || lcolor.equals("null", ignoreCase = true) || lcolor.equals("", ignoreCase = true)) {
            lcolor = "NA"
        }
        leads_details_color_which.setText(lcolor)
        if (lkms == null || lkms.equals("null", ignoreCase = true) || lkms.equals("", ignoreCase = true)) {
            lkms = "NA"
        }
        leads_details_kms_which.setText(lkms)
        Log.e("LDID ", "Val $ldid")
        if (LeadsInstance.getInstance().whichleads == "WebLeads") {
            if (lregno == null || lregno.equals("null", ignoreCase = true) || lregno.equals("", ignoreCase = true)) {
                lregno = "NA"
            }
            leads_details_model_num.setText(lregno)
            if (Lmfyear == null || Lmfyear.equals("null", ignoreCase = true) || Lmfyear.equals("", ignoreCase = true)) {
                Lmfyear = "NA"
            }
            leads_details_year_which.setText(Lmfyear)
            cardview2.setVisibility(View.GONE)
        } else {
            if (lregno == null || lregno.equals("null", ignoreCase = true) || lregno.equals("", ignoreCase = true)) {
                lregno = "NA"
            }
            leads_details_model_num.setText(lregno)
            cardview2.setVisibility(View.GONE)
        }
        leads_details_model_num.setVisibility(View.VISIBLE)
        leads_details_phonecall.setOnClickListener(View.OnClickListener {
            Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.leads_details_call, GlobalText.android)
            val callIntent = Intent(Intent.ACTION_VIEW)
            callIntent.data = Uri.parse("tel:$lmobile") //change the number
            startActivity(callIntent)
        })
        leads_details_messages.setOnClickListener(View.OnClickListener {
            Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.leads_details_message, GlobalText.android)
            val sendIntent = Intent(Intent.ACTION_VIEW)
            sendIntent.data = Uri.parse("sms:$lmobile")
            sendIntent.putExtra("sms_body", "")
            startActivity(sendIntent)
        })
        leads_details_whatsapp.setOnClickListener(View.OnClickListener {
            Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.leads_details_whatsapp, GlobalText.android)
            openWhatsApp(lmobile)
        })
        return view
    }

    private fun openWhatsApp(number: String?) {
        val smsNumber = "91$number" //without '+'
        try {
            /*Intent sendIntent = new Intent("android.intent.action.MAIN");
            //sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.setType("stock_fragment/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my stock_fragment to send.");
            sendIntent.putExtra("jid", smsNumber + "@s.whatsapp.net"); //phone number without "+" prefix
            sendIntent.setPackage("com.whatsapp");
            context.startActivity(sendIntent);*/
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_VIEW
            val url = "https://api.whatsapp.com/send?phone=$smsNumber&stock_fragment="
            sendIntent.data = Uri.parse(url)
            startActivity(sendIntent)
        } catch (e: Exception) {
            Toast.makeText(activity, "Error/n$e", Toast.LENGTH_SHORT).show()
        }
    }

    private fun capitalize(capString: String?): String {
        val capBuffer = StringBuffer()
        val capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString)
        while (capMatcher.find()) {
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase())
        }
        return capMatcher.appendTail(capBuffer).toString()
    }

    override fun onResume() {
        super.onResume()
        // put your code here...
        Application.getInstance().trackScreenView(activity, GlobalText.NormalLeadDetails_Fragment)
    }


    fun updateLeadState(upleadstatus: String, upnextfollowdate: String, upremark: String?) {
        var upnextfollowdate = upnextfollowdate
        var upremark = upremark
        leads_details_hot.setText(upleadstatus)
        if (upleadstatus.equals("Open", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.hot_28)
            leads_details_hot.setVisibility(View.INVISIBLE)
        }
        if (upleadstatus.equals("Hot", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.hot_28)
            leads_details_hot.setVisibility(View.VISIBLE)
        }
        if (upleadstatus.equals("Warm", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.warm_28)
            leads_details_hot.setVisibility(View.VISIBLE)
        }
        if (upleadstatus.equals("Cold", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.cold_28)
            leads_details_hot.setVisibility(View.VISIBLE)
        }
        if (upleadstatus.equals("Lost", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.lost_28)
            leads_details_hot.setVisibility(View.VISIBLE)
        }
        if (upleadstatus.equals("Sold", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.sold_28)
            leads_details_hot.setVisibility(View.VISIBLE)
        }
        if (upnextfollowdate == "") {
            upnextfollowdate = "NA"
        }
        val updatefollowdate = "Next Follow-up <br><font color=\"black\">$upnextfollowdate</font></br> "
        leads_details_next_followup.setText(Html.fromHtml(updatefollowdate))
        if (leads_details_message.getText().toString() == "NA" || leads_details_message.getText().toString() == "") {
            upremark = "No comments available"
            leads_details_message.setText(upremark)
        } else {
            leads_details_message.setText(upremark)
        }
    }

    fun Parsewebleadstore(jObj: JSONObject, activity: Activity?) {
        var updateFollowup = ""
        var remarkweb = ""
        try {
            if (jObj.getString("status") == "failure") {
            } else {
                Log.e("NewCheck123 ", "Response $jObj")
                val arrayList = jObj.getJSONArray("data")
                val jsonobject = JSONObject(arrayList.getString(0))
                leads_details_hot.setText(jsonobject.getString("status"))
                if (jsonobject.getString("status").equals("Open", ignoreCase = true)) {
                    leads_details_hot.setBackgroundResource(R.drawable.hot_28)
                    leads_details_hot.setVisibility(View.INVISIBLE)
                }
                if (jsonobject.getString("status").equals("Hot", ignoreCase = true)) {
                    leads_details_hot.setBackgroundResource(R.drawable.hot_28)
                    leads_details_hot.setVisibility(View.VISIBLE)
                }
                if (jsonobject.getString("status").equals("Warm", ignoreCase = true)) {
                    leads_details_hot.setBackgroundResource(R.drawable.warm_28)
                    leads_details_hot.setVisibility(View.VISIBLE)
                }
                if (jsonobject.getString("status").equals("Cold", ignoreCase = true)) {
                    leads_details_hot.setBackgroundResource(R.drawable.cold_28)
                    leads_details_hot.setVisibility(View.VISIBLE)
                }
                if (jsonobject.getString("status").equals("Lost", ignoreCase = true)) {
                    leads_details_hot.setBackgroundResource(R.drawable.lost_28)
                    leads_details_hot.setVisibility(View.VISIBLE)
                    lost = true
                    LeadDetailsActivity.Invisible()
                }
                if (jsonobject.getString("status").equals("Sold", ignoreCase = true)) {
                    leads_details_hot.setBackgroundResource(R.drawable.sold_28)
                    leads_details_hot.setVisibility(View.VISIBLE)
                }
                if (jsonobject.getString("status").equals("Visiting", ignoreCase = true)) {
                    leads_details_hot.setBackgroundResource(R.drawable.ic_visiting)
                    leads_details_hot.setVisibility(View.VISIBLE)
                }
                if (jsonobject.getString("status").equals("Search", ignoreCase = true)) {
                    leads_details_hot.setBackgroundResource(R.drawable.ic_search_lead)
                    leads_details_hot.setVisibility(View.VISIBLE)
                }
                if (jsonobject.getString("status").equals("Not-Interested", ignoreCase = true)) {
                    leads_details_hot.setBackgroundResource(R.drawable.ic_not_intrested)
                    leads_details_hot.setVisibility(View.VISIBLE)
                    lost = true
                    LeadDetailsActivity.Invisible()
                }
                if (jsonobject.getString("status").equals("Finalised", ignoreCase = true)) {
                    leads_details_hot.setBackgroundResource(R.drawable.ic_finalized)
                    leads_details_hot.setVisibility(View.VISIBLE)
                    lost = true
                    LeadDetailsActivity.Invisible()
                }
                if (jsonobject.getString("status").equals("No-Response", ignoreCase = true)) {
                    leads_details_hot.setBackgroundResource(R.drawable.ic_no_responce)
                    leads_details_hot.setVisibility(View.VISIBLE)
                }
                updateFollowup = jsonobject.getString("FollowUpDate")
                if (updateFollowup == "" || updateFollowup.trim { it <= ' ' } == "000000" || updateFollowup == "00 0000" || updateFollowup == "00  0000") {
                    updateFollowup = "NA"
                }
                try {
                    val splitdatespace = updateFollowup.split(" ").toTypedArray()
                    val splitdate1 = splitdatespace[0].split("-").toTypedArray()
                    updateFollowup = splitdate1[2] + " " + MonthUtility.Month[splitdate1[1].toInt()] + " " + splitdate1[0]
                    Log.e("updateFollowup ", "updateFollowup $updateFollowup")
                    if (updateFollowup == "" || updateFollowup.trim { it <= ' ' } == "000000" || updateFollowup == "00 0000" || updateFollowup == "00  0000") {
                        updateFollowup = "NA"
                    }
                    updateFollowup = "Next Follow-up <br><font color=\"black\">$updateFollowup</font></br> "
                    leads_details_next_followup.setText(Html.fromHtml(updateFollowup))
                } catch (e: Exception) {
                    updateFollowup = "Next Follow-up <br><font color=\"black\">$updateFollowup</font></br> "
                    leads_details_next_followup.setText(Html.fromHtml(updateFollowup))
                }
                remarkweb = jsonobject.getString("remark")
                if (remarkweb == "NA" || remarkweb == "") {
                    remarkweb = "No comments available"
                    leads_details_message.setText(remarkweb)
                } else {
                    leads_details_message.setText(remarkweb)
                }
                FollowupHistoryFragment.reloadAPI()
            }
        } catch (e: JSONException) {
        }
    }

    fun ParsePrivateUpdateLeadStore(jObj: JSONObject, activity: Activity?) {
        var updateFollowup = ""
        var remarkpri = ""
        Log.e("PrivateUpdateLeadStore ", "jobj $jObj")
        try {
            val arrayList = jObj.getJSONArray("data")
            val jsonobject = JSONObject(arrayList.getString(0))
            leads_details_hot.setText(jsonobject.getString("lead_status"))
            if (jsonobject.getString("lead_status").equals("Open", ignoreCase = true)) {
                leads_details_hot.setBackgroundResource(R.drawable.hot_28)
                leads_details_hot.setVisibility(View.INVISIBLE)
            }
            if (jsonobject.getString("lead_status").equals("Hot", ignoreCase = true)) {
                leads_details_hot.setBackgroundResource(R.drawable.hot_28)
                leads_details_hot.setVisibility(View.VISIBLE)
            }
            if (jsonobject.getString("lead_status").equals("Warm", ignoreCase = true)) {
                leads_details_hot.setBackgroundResource(R.drawable.warm_28)
                leads_details_hot.setVisibility(View.VISIBLE)
            }
            if (jsonobject.getString("lead_status").equals("Cold", ignoreCase = true)) {
                leads_details_hot.setBackgroundResource(R.drawable.cold_28)
                leads_details_hot.setVisibility(View.VISIBLE)
            }
            if (jsonobject.getString("lead_status").equals("Lost", ignoreCase = true)) {
                leads_details_hot.setBackgroundResource(R.drawable.lost_28)
                leads_details_hot.setVisibility(View.VISIBLE)
                lost = true
                LeadDetailsActivity.Invisible()
            }
            if (jsonobject.getString("lead_status").equals("Sold", ignoreCase = true)) {
                leads_details_hot.setBackgroundResource(R.drawable.sold_28)
                leads_details_hot.setVisibility(View.VISIBLE)
            }
            if (jsonobject.getString("lead_status").equals("Visiting", ignoreCase = true)) {
                leads_details_hot.setBackgroundResource(R.drawable.ic_visiting)
                leads_details_hot.setVisibility(View.VISIBLE)
            }
            if (jsonobject.getString("lead_status").equals("Search", ignoreCase = true)) {
                leads_details_hot.setBackgroundResource(R.drawable.ic_search_lead)
                leads_details_hot.setVisibility(View.VISIBLE)
            }
            if (jsonobject.getString("lead_status").equals("Not-Interested", ignoreCase = true)) {
                leads_details_hot.setBackgroundResource(R.drawable.ic_not_intrested)
                leads_details_hot.setVisibility(View.VISIBLE)
                lost = true
                LeadDetailsActivity.Invisible()
            }
            if (jsonobject.getString("lead_status").equals("Finalised", ignoreCase = true)) {
                leads_details_hot.setBackgroundResource(R.drawable.ic_finalized)
                leads_details_hot.setVisibility(View.VISIBLE)
                lost = true
                LeadDetailsActivity.Invisible()
            }
            if (jsonobject.getString("lead_status").equals("No-Response", ignoreCase = true)) {
                leads_details_hot.setBackgroundResource(R.drawable.ic_no_responce)
                leads_details_hot.setVisibility(View.VISIBLE)
            }
            updateFollowup = jsonobject.getString("follow_date")
            if (updateFollowup == "" || updateFollowup.trim { it <= ' ' } == "000000" || updateFollowup == "00 0000" || updateFollowup == "00  0000") {
                updateFollowup = "NA"
            }
            try {
                val splitdatespace = updateFollowup.split(" ").toTypedArray()
                val splitdate1 = splitdatespace[0].split("-").toTypedArray()
                updateFollowup = splitdate1[2] + " " + MonthUtility.Month[splitdate1[1].toInt()] + " " + splitdate1[0]
                Log.e("updateFollowup ", "updateFollowup $updateFollowup")
                updateFollowup = "Next Follow-up <br><font color=\"black\">$updateFollowup</font></br> "
                leads_details_next_followup.setText(Html.fromHtml(updateFollowup))
            } catch (e: Exception) {
                updateFollowup = "Next Follow-up <br><font color=\"black\">$updateFollowup</font></br> "
                leads_details_next_followup.setText(Html.fromHtml(updateFollowup))
            }
            remarkpri = jsonobject.getString("remark")
            if (remarkpri == "NA" || remarkpri == "") {
                remarkpri = "No comments available"
                leads_details_message.setText(remarkpri)
            } else {
                leads_details_message.setText(remarkpri)
            }
            FollowupHistoryFragment.reloadAPI()
        } catch (e: JSONException) {
        }
    }

    fun RetroParsePrivateLeadStore(mPrivateLeadResponse: PrivateLeadResponse) {
        var updateFollowup = ""
        var remarkpri = ""
        val mlist = mPrivateLeadResponse.data
        leads_details_hot.setText(mlist[0].leadStatus)
        if (mlist[0].leadStatus.equals("Open", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.hot_28)
            leads_details_hot.setVisibility(View.INVISIBLE)
        }
        if (mlist[0].leadStatus.equals("Hot", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.hot_28)
            leads_details_hot.setVisibility(View.VISIBLE)
        }
        if (mlist[0].leadStatus.equals("Warm", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.warm_28)
            leads_details_hot.setVisibility(View.VISIBLE)
        }
        if (mlist[0].leadStatus.equals("Cold", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.cold_28)
            leads_details_hot.setVisibility(View.VISIBLE)
        }
        if (mlist[0].leadStatus.equals("Lost", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.lost_28)
            leads_details_hot.setVisibility(View.VISIBLE)
            lost = true
            LeadDetailsActivity.Invisible()
        }
        if (mlist[0].leadStatus.equals("Sold", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.sold_28)
            leads_details_hot.setVisibility(View.VISIBLE)
        }
        if (mlist[0].leadStatus.equals("Visiting", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_visiting)
            leads_details_hot.setVisibility(View.VISIBLE)
        }
        if (mlist[0].leadStatus.equals("Search", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_search_lead)
            leads_details_hot.setVisibility(View.VISIBLE)
        }
        if (mlist[0].leadStatus.equals("Not-Interested", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_not_intrested)
            leads_details_hot.setVisibility(View.VISIBLE)
            lost = true
            LeadDetailsActivity.Invisible()
        }
        if (mlist[0].leadStatus.equals("Finalised", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_finalized)
            leads_details_hot.setVisibility(View.VISIBLE)
            lost = true
            LeadDetailsActivity.Invisible()
        }
        if (mlist[0].leadStatus.equals("No-Response", ignoreCase = true)) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_no_responce)
            leads_details_hot.setVisibility(View.VISIBLE)
        }
        updateFollowup = mlist[0].followDate
        if (updateFollowup == "" || updateFollowup.trim { it <= ' ' } == "000000" || updateFollowup == "00 0000" || updateFollowup == "00  0000") {
            updateFollowup = "NA"
        }
        try {
            val splitdatespace = updateFollowup.split(" ").toTypedArray()
            val splitdate1 = splitdatespace[0].split("-").toTypedArray()
            updateFollowup = splitdate1[2] + " " + MonthUtility.Month[splitdate1[1].toInt()] + " " + splitdate1[0]
            Log.e("updateFollowup ", "updateFollowup $updateFollowup")
            updateFollowup = "Next Follow-up <br><font color=\"black\">$updateFollowup</font></br> "
            leads_details_next_followup.setText(Html.fromHtml(updateFollowup))
        } catch (e: Exception) {
            updateFollowup = "Next Follow-up <br><font color=\"black\">$updateFollowup</font></br> "
            leads_details_next_followup.setText(Html.fromHtml(updateFollowup))
        }
        remarkpri = mlist[0].remark
        if (remarkpri == "NA" || remarkpri == "") {
            remarkpri = "No comments available"
            leads_details_message.text = remarkpri
        } else {
            leads_details_message.setText(remarkpri)
        }
        FollowupHistoryFragment.reloadAPI()
    }
    private fun getRequiredDateFormat(date: String?):String{
        val inputFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
        val outputFormat = SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH)
        var outDate = "NO Date"
        try {
            outDate = outputFormat.format(inputFormat.parse(date))
        }catch (e: Exception){
            e.printStackTrace()
        }

        return outDate
    }
}