package remotedealer.ui;

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mfcwl.mfc_dealer.R
import com.squareup.picasso.Picasso
import gallerylib.utils.PicassoImageLoader
import kotlinx.android.synthetic.main.rdm_stock_details_fragment.*
import kotlinx.android.synthetic.main.rdm_stock_details_fragment.view.*
import okhttp3.internal.platform.Platform.Companion.get
import remotedealer.model.stock.RDMStocksItem

import remotedealer.rdrform.ui.RDMFormFragment

private const val ARG_PARAM1 = "param1"

/*
 * Created By Uday(MFCWL) ->  17-12-2020 17:59
 *
 */
public class RDMStockDetailsFragment : Fragment() {

    var TAG = javaClass.simpleName
    lateinit var vi: View

    private lateinit var stockitem: RDMStocksItem
    private lateinit var picasso: Picasso


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        Log.i(TAG, "onCreateView: ")

        vi = inflater.inflate(R.layout.rdm_stock_details_fragment, container, false)

        arguments.let {
            stockitem = it?.getSerializable(ARG_PARAM1) as RDMStocksItem
            showStockData(stockitem)
        }


        return vi
    }

    private fun showStockData(data: RDMStocksItem) {

        if(data.images!!.size >0){
            var imageURL = data.images!!.get(0).url
            if(imageURL!= null &&imageURL != ""){
                Picasso.get().load(imageURL).resize(90,90).placeholder(R.drawable.no_car_small).into(vi.carimage)
            }
        }

        vi.vmake.text = "" + data.vehicleMake.trim()
        vi.dateType.text = ""+ getOnlyDate(data.postedDate)
        vi.vmodelvariant.text = "" + data.vehicleModel.trim() + "-" + data.vehicleVariant.trim()
        vi.moreinfo.text = "" + data.colour.trim() + " | " + data.kilometer+ "km" + " " + data.fuelType.trim() + " | " + data.owner + " " + "owner" + " | " + data.registrationNumber.trim()
        vi.vsellingprice.text = "₹ " + data.sellingPrice
        vi.certifiedornot.text = "" + data.certifiedText.trim()
        vi.vregcity.text = "" + data.registraionCity.trim()
        vi.vboughtprice.text = "₹ " + data.boughtPrice
        vi.vregmonthandyear.text = "" + data.regMonth.trim() + " " + data.regYear.trim()
        vi.vboughtprice.text = "₹ " + data.boughtPrice
        vi.vinsurance.text = "" + data.insurance.trim()
        vi.vcertificationnumber.text = "" + data.certificationNumber.trim()
        vi.procName.text = "" + data.procurementExecutiveName.trim()
        vi.vinsuranceexpdate.text = "" + data.insuranceExpDate.trim()
        vi.vcngkit.text = "" + data.cngKit.trim()
        vi.vchasisno.text = "" + data.chassisNumber.trim()
        vi.vengineno.text = "" + data.engineNumber.trim()
        vi.vsource.text = "" + data.stockSource.trim()
        vi.vtype.text = "" + data.privateVehicle.trim()
        vi.recowarrant.text = "" + data.warrantyRecommended.trim()
        vi.scatogory.text = "" + data.stockSource.trim()
        vi.comments.text = "" + data.comments.trim()
       // vi.dateType.text = "" + data.postedDate
        if (data.isCertified != "" && data.isCertified.equals("true", ignoreCase = true)) {
            vi.imgcertified.toggleVisibility()
        }

    }

    companion object {
        @JvmStatic
        fun newInstance(param1: RDMStocksItem) =
                RDMStockDetailsFragment().apply {
                    arguments = Bundle().apply {
                        putSerializable(ARG_PARAM1, param1)

                    }
                }
    }


    private fun getOnlyDate(postedDate: String): String {
        var date = ""
        try {
            date = postedDate.split(" ")[0]
        } catch (e: Exception) {
            Log.e(TAG, "getOnlyDate: " + e.message)
            return postedDate
        }
        return date
    }

    fun View.toggleVisibility() {
        if (visibility == View.VISIBLE) {
            visibility = View.GONE
        } else {
            visibility = View.VISIBLE
        }
    }

}