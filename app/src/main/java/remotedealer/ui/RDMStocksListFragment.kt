package remotedealer.ui;

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.mfcwl.mfc_dealer.R
import com.mfcwl.mfc_dealer.Utility.CommonMethods
import com.mfcwl.mfc_dealer.Utility.SpinnerManager
import kotlinx.android.synthetic.main.rdm_stocks_list_fragment.view.*
import remotedealer.adapter.RDMStocksAdapter
import remotedealer.model.stock.RDMStocksItem
import remotedealer.model.stock.RDMStocksRequest
import remotedealer.model.stock.RDMStocksResponse
import remotedealer.retrofit.RetroBase.Companion.URL_END_RDM_STOCKS
import remotedealer.retrofit.RetroBase.Companion.mfcRetroInterface
import remotedealer.util.RedirectionConstants.Companion.KEY_BOOKSTOCK
import remotedealer.util.RedirectionConstants.Companion.KEY_CERTIFIED
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/*
 * Created By Uday(MFCWL) ->  16-12-2020 18:10
 *
 */
public class RDMStocksListFragment(
        var token: String,
        var code: String,
        private var filterWhereList: ArrayList<RDMStocksRequest.Where>,
        private var filterWhereInList: ArrayList<RDMStocksRequest.WhereIn>,
        var identifier: String,
        var clickedItem: String,
) : Fragment() {
    var TAG = javaClass.simpleName
    lateinit var vi: View

    lateinit var stockData: ArrayList<RDMStocksItem>
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?,
    ): View {
        Log.i(TAG, "onCreateView: ")

        vi = inflater.inflate(R.layout.rdm_stocks_list_fragment, container, false)

        initViews()


        return vi
    }

    private fun initViews() {

        stockData = ArrayList()

        vi.btnTotalStock.setOnClickListener {

            clearCheckBoxes()
            hideCheckBoxLayout(false)

            vi.btnTotalStock.setTextColor(resources.getColor(R.color.white))
            vi.btnTotalStock.setBackgroundResource(R.drawable.rounded_bg_for_btn)

            vi.btnBookStock.setTextColor(resources.getColor(R.color.black))
            vi.btnBookStock.setBackgroundResource(R.drawable.rounded_bg_for_btn_not_selected)

            vi.btnLorem.setTextColor(resources.getColor(R.color.black))
            vi.btnLorem.setBackgroundResource(R.drawable.rounded_bg_for_btn_not_selected)
            identifier = ""

            filterWhereInList = getWhereInList("all")
            filterWhereList = ArrayList()
            callRDMStocks()
        }

        vi.btnBookStock.setOnClickListener {

            clearCheckBoxes()
            hideCheckBoxLayout(true)

            highlightBookStockBtn()

            vi.btnLorem.setTextColor(resources.getColor(R.color.black))
            vi.btnLorem.setBackgroundResource(R.drawable.rounded_bg_for_btn_not_selected)
            filterWhereInList = getWhereInList("is_sold")
            filterWhereList = getWhereList("is_booked")
            identifier = "totalbook"
            callRDMStocks()
        }

        vi.btnLorem.setOnClickListener {
            vi.btnTotalStock.setTextColor(resources.getColor(R.color.black))
            vi.btnTotalStock.setBackgroundResource(R.drawable.rounded_bg_for_btn_not_selected)

            vi.btnBookStock.setTextColor(resources.getColor(R.color.black))
            vi.btnBookStock.setBackgroundResource(R.drawable.rounded_bg_for_btn_not_selected)

            vi.btnLorem.setTextColor(resources.getColor(R.color.white))
            vi.btnLorem.setBackgroundResource(R.drawable.rounded_bg_for_btn)

        }

        vi.ivStockFilter.setOnClickListener { Log.i(TAG, "initViews: ") }


        vi.cbCertifiedCar.setOnCheckedChangeListener { buttonView, isChecked ->

            clearButtons()

            if (isChecked) {
                vi.cbNonCertifiedCar.isChecked = false

                filterWhereInList = getWhereInList("certified")
                filterWhereList = addCertifiedCond(true)
                identifier = ""
                callRDMStocks()

            } else {

                if (!vi.cbNonCertifiedCar.isChecked)
                    vi.btnTotalStock.performClick()

            }


        }


        vi.cbNonCertifiedCar.setOnCheckedChangeListener { buttonView, isChecked ->

            clearButtons()

            if (isChecked) {
                vi.cbCertifiedCar.isChecked = false

                filterWhereInList = getWhereInList("non_certified")
                filterWhereList = ArrayList() //addCertifiedCond(false)
                identifier = ""
                callRDMStocks()
            } else {

                if (!vi.cbCertifiedCar.isChecked)
                    vi.btnTotalStock.performClick()

            }
        }

        callRDMStocks()

        if (clickedItem == KEY_BOOKSTOCK) {
            highlightBookStockBtn()
            hideCheckBoxLayout(true)
        } else if (clickedItem == KEY_CERTIFIED) {
            clearButtons()
            vi.cbCertifiedCar.isChecked = true
        }

    }

    private fun highlightBookStockBtn() {
        vi.btnTotalStock.setTextColor(resources.getColor(R.color.black))
        vi.btnTotalStock.setBackgroundResource(R.drawable.rounded_bg_for_btn_not_selected)

        vi.btnBookStock.setTextColor(resources.getColor(R.color.white))
        vi.btnBookStock.setBackgroundResource(R.drawable.rounded_bg_for_btn)
    }


    private fun addCertifiedCond(condition: Boolean): ArrayList<RDMStocksRequest.Where> {
        val certifiedWhereList = ArrayList<RDMStocksRequest.Where>()

        val where = RDMStocksRequest.Where()
        if (condition) {
            where.column = "is_certified"
            where.operator = "="
            where.value = "true"
            certifiedWhereList.add(where)
        } else {
            where.column = "is_certified"
            where.operator = "="
            where.value = "false"
            certifiedWhereList.add(where)
        }
        return certifiedWhereList
    }

    private fun bookStockWhereInList(key: String): ArrayList<RDMStocksRequest.WhereIn> {
        val list = ArrayList<RDMStocksRequest.WhereIn>()

        val whereIn1 = RDMStocksRequest.WhereIn()
        whereIn1.column = "dealer_code"
        whereIn1.values = arrayOf("$code")

        val whereIn2 = RDMStocksRequest.WhereIn()
        whereIn2.column = "stock_source"
        whereIn2.values = arrayOf("mfc", "p&s")

        val whereIn3 = RDMStocksRequest.WhereIn()
        whereIn3.column = "is_sold"
        whereIn3.values = arrayOf("1", "0")

        list.add(whereIn1)
        list.add(whereIn2)
        list.add(whereIn3)

        return list
    }

    private fun totalStockWhereInList(): ArrayList<RDMStocksRequest.WhereIn> {
        val list = ArrayList<RDMStocksRequest.WhereIn>()

        val whereIn1 = RDMStocksRequest.WhereIn()
        whereIn1.column = "dealer_code"
        whereIn1.values = arrayOf("$code")

        val whereIn2 = RDMStocksRequest.WhereIn()
        whereIn2.column = "stock_source"
        whereIn2.values = arrayOf("mfc", "p&s")

        list.add(whereIn1)
        list.add(whereIn2)

        return list
    }

    private fun hideCheckBoxLayout(hide: Boolean) {
        if (hide)
            vi.certifiedLayout.visibility = View.GONE
        else
            vi.certifiedLayout.visibility = View.VISIBLE
    }

    private fun clearCheckBoxes() {
        vi.cbCertifiedCar.isChecked = false
        vi.cbNonCertifiedCar.isChecked = false
    }

    private fun clearButtons() {
        vi.btnTotalStock.setTextColor(resources.getColor(R.color.black))
        vi.btnTotalStock.setBackgroundResource(R.drawable.rounded_bg_for_btn_not_selected)

        vi.btnBookStock.setTextColor(resources.getColor(R.color.black))
        vi.btnBookStock.setBackgroundResource(R.drawable.rounded_bg_for_btn_not_selected)
    }

    fun getFirstDay(d: Date?): String {
        var formatedDate = ""
        try {
            val calendar = Calendar.getInstance()
            calendar.time = d
            calendar[Calendar.DAY_OF_MONTH] = 1
            val mdate = calendar.time
            val sdf = SimpleDateFormat("yyyy-MM-dd")
            formatedDate = sdf.format(mdate)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return formatedDate
    }

    private fun getWhereList(str: String): ArrayList<RDMStocksRequest.Where> {
        val whereList = ArrayList<RDMStocksRequest.Where>()

        if (str == "certified") {
            val where = RDMStocksRequest.Where()
            where.column = "is_certified"
            where.operator = "="
            where.value = "true"
            whereList.add(where)


        } else if (str == "non-certified") {
            val where = RDMStocksRequest.Where()
            where.column = "is_certified"
            where.operator = "!="
            where.value = "true"
            whereList.add(where)
        } else if (str == "above30") {
            val where = RDMStocksRequest.Where()
            where.column = "stock_age"
            where.operator = ">="
            where.value = "30"
            whereList.add(where)
        } else if (str == "above60") {
            val where = RDMStocksRequest.Where()
            where.column = "stock_age"
            where.operator = ">="
            where.value = "60"
            whereList.add(where)
        } else if (str == "visibleOnMFC") {
            val where = RDMStocksRequest.Where()
            where.column = "photo_count"
            where.operator = ">"
            where.value = "0"
            whereList.add(where)

            val where2 = RDMStocksRequest.Where()
            where2.column = "private_vehicle"
            where2.operator = "="
            where2.value = "true"
            whereList.add(where2)
        } else if (str.equals("is_booked", ignoreCase = true)) {
            val where = RDMStocksRequest.Where()
            where.column = "is_booked"
            where.operator = "="
            where.value = "true"
            whereList.add(where)

            /*val where1 = RDMStocksRequest.Where()
            where1.column = "book_date"
            where1.operator = ">="
            where1.value = getFirstDay(Date())
            whereList.add(where1)*/
        }

        return whereList
    }


    private fun getWhereInList(str: String): ArrayList<RDMStocksRequest.WhereIn> {
        val whereInList = ArrayList<RDMStocksRequest.WhereIn>()
        val whereIn1 = RDMStocksRequest.WhereIn()
        whereIn1.column = "dealer_code"
        whereIn1.values = arrayOf("$code")
        whereInList.add(whereIn1)

        if (str == "paidup") {
            val whereIn2 = RDMStocksRequest.WhereIn()
            whereIn2.column = "stock_source"
            whereIn2.values = arrayOf("mfc")
            whereInList.add(whereIn2)
        } else if (str == "parkAndSell") {
            val whereIn2 = RDMStocksRequest.WhereIn()
            whereIn2.column = "stock_source"
            whereIn2.values = arrayOf("p&s")
            whereInList.add(whereIn2)
        } else if (str == "withoutImage") {
            val whereIn2 = RDMStocksRequest.WhereIn()
            whereIn2.column = "photo_count"
            whereIn2.values = arrayOf("0")
            whereInList.add(whereIn2)
        } else if (str.equals("is_sold")) {

            val whereIn3 = RDMStocksRequest.WhereIn()
            whereIn3.column = "is_sold"
            whereIn3.values = arrayOf("1", "0")
            whereInList.add(whereIn3)

            val whereIn4 = RDMStocksRequest.WhereIn()
            whereIn4.column = "stock_source"
            whereIn4.values = arrayOf("mfc", "p&s")
            whereInList.add(whereIn4)

        } else if (str.equals("stock_source")) {
            val whereIn3 = RDMStocksRequest.WhereIn()
            whereIn3.column = "stock_source"
            whereIn3.values = arrayOf("mfc", "p&s")
            whereInList.add(whereIn3)

        } else if (str == "all") {
            val whereIn3 = RDMStocksRequest.WhereIn()
            whereIn3.column = "stock_source"
            whereIn3.values = arrayOf("mfc", "p&s")
            whereInList.add(whereIn3)
        } else if (str == "certified") {
            val whereIn3 = RDMStocksRequest.WhereIn()
            whereIn3.column = "stock_source"
            whereIn3.values = arrayOf("mfc", "p&s")
            whereInList.add(whereIn3)
        } else if (str == "non_certified") {
            val whereIn3 = RDMStocksRequest.WhereIn()
            whereIn3.column = "is_certified"
            whereIn3.values = arrayOf("", "false")
            whereInList.add(whereIn3)

            val whereIn4 = RDMStocksRequest.WhereIn()
            whereIn4.column = "stock_source"
            whereIn4.values = arrayOf("mfc", "p&s")
            whereInList.add(whereIn4)
        }

        return whereInList
    }


    fun getTodayDate(): String {
        val c = Calendar.getInstance().time
        println("Current time => $c")
        val df = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        return df.format(c)
    }


    private fun callRDMStocks() {

        val req = RDMStocksRequest()
        val whereIn = RDMStocksRequest.WhereIn()
        //val where = RDMStocksRequest.Where()
        //val whereInList = ArrayList<RDMStocksRequest.WhereIn>()
        //val whereList = ArrayList<RDMStocksRequest.Where>()

        /*whereIn.column = "dealer_code"
        whereIn.values = arrayOf(code)
        whereInList.add(whereIn)*/

        //whereList.add(filterWhere)

        req.orderBy = "posted_date"
        req.orderByReverse = "true"
        req.page = "1"
        req.pageItem = "100"

        /*  if(identifier.equals("totalbook",ignoreCase = true)){
              req.DateField = "book_date"
              req.DateFrom = getFirstDay(Date())
              req.DateTo = getTodayDate()
          }
  */

        req.whereIn = filterWhereInList
        req.whereList = filterWhereList

        /*if (filterWhereList.size == 1) {

            if (filterWhereList[0].column != ""
                    && filterWhereList[0].operator != ""
                    && filterWhereList[0].value != "") {
                req.whereList = filterWhereList
            }

        }*/


        SpinnerManager.showSpinner(context)
        mfcRetroInterface.getFromWebUsingToken(req, URL_END_RDM_STOCKS, CommonMethods.getToken("token")).enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                SpinnerManager.hideSpinner(context)

                stockData.clear()
                try {
                    val strRes = Gson().toJson(response.body())
                    val strModel = Gson().fromJson(strRes, RDMStocksResponse::class.java)

                    Log.i(TAG, "onResponse: " + strModel.data.size)

                    stockData = strModel.data
                    if (strModel.data.size == 0) {
                        Toast.makeText(context, "Stocks not available", Toast.LENGTH_LONG).show()
                    }

                    loadToAdapter(stockData)

                } catch (e: Exception) {
                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(context)
            }
        })

    }

    private fun loadToAdapter(stockData: java.util.ArrayList<RDMStocksItem>) {

        val adapter = RDMStocksAdapter(requireContext(), requireActivity(), stockData)

        vi.recyclerViewStocks.layoutManager = LinearLayoutManager(context)
        vi.recyclerViewStocks.adapter = adapter
        adapter.notifyDataSetChanged()

    }


    private fun getDummyData(): ArrayList<RDMStocksItem> {
        val list = ArrayList<RDMStocksItem>()
        list.add(RDMStocksItem())
        list.add(RDMStocksItem())
        list.add(RDMStocksItem())

        return list
    }

}