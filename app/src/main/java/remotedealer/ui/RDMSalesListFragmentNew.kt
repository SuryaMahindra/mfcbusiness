package remotedealer.ui;

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.mfcwl.mfc_dealer.R
import com.mfcwl.mfc_dealer.Utility.CommonMethods
import com.mfcwl.mfc_dealer.Utility.SpinnerManager
import kotlinx.android.synthetic.main.rdm_sales_list_fragment_new.view.*
import kotlinx.android.synthetic.main.rdm_total_sales_fragment.view.recyclerViewTotalSales
import remotedealer.adapter.RDMSalesListAdapter
import remotedealer.model.sales.RDMSalesRequest
import remotedealer.model.sales.RDMSalesResponse
import remotedealer.retrofit.RetroBase.Companion.URL_END_RDM_SALES
import remotedealer.retrofit.RetroBase.Companion.mfcRetroInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

/*
 * Created By Uday(MFCWL) ->  17-12-2020 17:02
 *
 */
class RDMSalesListFragmentNew(
        var token: String,
        var code: String,
        var key: String,
        var filterWhereList: ArrayList<RDMSalesRequest.Where>,
        var filterWhereInList: ArrayList<RDMSalesRequest.WhereIn>,
) : Fragment() {

    var TAG = javaClass.simpleName
    lateinit var vi: View

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?,
    ): View {
        Log.i(TAG, "onCreateView: ")

        vi = inflater.inflate(R.layout.rdm_sales_list_fragment_new, container, false)

        initViews()

        return vi
    }

    fun initViews() {


        vi.tvTotalSales.setOnClickListener {
            vi.layoutWarrantyItems.visibility = View.GONE
            vi.tvTotalSalesBottomColor.setBackgroundColor(resources.getColor(R.color.web_leads_text_blue))
            vi.tvWarrantyBottomColor.setBackgroundColor(resources.getColor(R.color.white))

            filterWhereInList = getWhereInList("total_sales")
            filterWhereList = getWhereList("total_sales")

            callRDMSales()
        }
        vi.tvTotalWarranty.setOnClickListener {
            warrantyHighlight()
            warrantySalesClickHighlight()

            filterWhereInList = getWhereInList("")
            filterWhereList = getWhereList("warranty")

            callRDMSales()
        }


        vi.btnWarrantySales.setOnClickListener {

            warrantySalesClickHighlight()

            filterWhereInList = getWhereInList("warranty")
            filterWhereList = getWhereList("warranty")


            callRDMSales()
        }

        vi.btnNonWarrantySales.setOnClickListener {

            nonWarrantySalesClickHighlight()

            filterWhereList = getWhereList("non-warranty")
            filterWhereInList = getWhereInList("non-warranty")

            callRDMSales()
        }


        when (key) {
            "total_sales" -> {
                vi.layoutWarrantyItems.visibility = View.GONE
            }
            "warranty" -> {
                warrantyHighlight()
                warrantySalesClickHighlight()
                /*vi.tvTotalWarrantySales.performClick()
                vi.btnWarrantySales.performClick()*/
            }
            "non-warranty" -> {
                warrantyHighlight()
                nonWarrantySalesClickHighlight()
                /*vi.tvTotalWarrantySales.performClick()
                vi.btnNonWarrantySales.performClick()*/
            }
        }

        callRDMSales()
    }

    private fun warrantyHighlight() {
        vi.layoutWarrantyItems.visibility = View.VISIBLE
        vi.tvTotalSalesBottomColor.setBackgroundColor(resources.getColor(R.color.white))
        vi.tvWarrantyBottomColor.setBackgroundColor(resources.getColor(R.color.web_leads_text_blue))
    }

    private fun warrantySalesClickHighlight() {
        vi.btnWarrantySales.setTextColor(resources.getColor(R.color.white))
        vi.btnWarrantySales.setBackgroundResource(R.drawable.rounded_bg_for_btn)

        vi.btnNonWarrantySales.setTextColor(resources.getColor(R.color.black))
        vi.btnNonWarrantySales.setBackgroundResource(R.drawable.rounded_bg_for_btn_not_selected)
    }

    private fun nonWarrantySalesClickHighlight() {
        vi.btnNonWarrantySales.setTextColor(resources.getColor(R.color.white))
        vi.btnNonWarrantySales.setBackgroundResource(R.drawable.rounded_bg_for_btn)

        vi.btnWarrantySales.setTextColor(resources.getColor(R.color.black))
        vi.btnWarrantySales.setBackgroundResource(R.drawable.rounded_bg_for_btn_not_selected)
    }

    private fun callRDMSales() {

        val req = RDMSalesRequest()

        /*val whereIn = RDMSalesRequest.WhereIn()
        whereIn.column = "dealer_code"
        whereIn.values = arrayOf(code)

        val whereInList = ArrayList<RDMSalesRequest.WhereIn>()
        whereInList.add(whereIn)*/

        req.orderBy = "sold_date"
        req.orderByReverse = "true"
        req.page = "1"
        req.pageItems = "100"
        req.where = filterWhereList
        req.whereIn = filterWhereInList

        SpinnerManager.showSpinner(context)
        mfcRetroInterface.getFromWebUsingToken(req, URL_END_RDM_SALES, CommonMethods.getToken("token")).enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                SpinnerManager.hideSpinner(context)

                try {
                    val strRes = Gson().toJson(response.body())
                    val strModel = Gson().fromJson(strRes, RDMSalesResponse::class.java)

                    Log.i(TAG, "onResponse: " + strModel.data.size)

                    if (strModel.data.size == 0) {
                        Toast.makeText(context, "Leads not available", Toast.LENGTH_LONG).show()
                        val adapter = RDMSalesListAdapter(requireContext(), requireActivity(), strModel.data)

                        vi.recyclerViewTotalSales.layoutManager = LinearLayoutManager(context)
                        vi.recyclerViewTotalSales.adapter = adapter
                        adapter.notifyDataSetChanged()
                    } else {
                        val adapter = RDMSalesListAdapter(requireContext(), requireActivity(), strModel.data)

                        vi.recyclerViewTotalSales.layoutManager = LinearLayoutManager(context)
                        vi.recyclerViewTotalSales.adapter = adapter
                        adapter.notifyDataSetChanged()
                    }
                } catch (e: Exception) {
                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(context)
            }
        })

    }


    private fun getWhereList(str: String): java.util.ArrayList<RDMSalesRequest.Where> {
        val whereList = ArrayList<RDMSalesRequest.Where>()

        if (str == "warranty") {
            val warrantyNumber = RDMSalesRequest.Where()
            warrantyNumber.column = "warranty_number"
            warrantyNumber.operator = "!="
            warrantyNumber.value = null
            whereList.add(warrantyNumber)

            val where = RDMSalesRequest.Where()
            where.column = "sold"
            where.operator = "="
            where.value = "1"
            whereList.add(where)

            val stockType = RDMSalesRequest.Where()
            stockType.column = "stock_type"
            stockType.operator = "="
            stockType.value = "4W"
            whereList.add(stockType)
        } else if (str == "non-warranty") {
            val warrantyNumber = RDMSalesRequest.Where()
            warrantyNumber.column = "warranty_number"
            warrantyNumber.operator = "="
            warrantyNumber.value = null
            whereList.add(warrantyNumber)

            val where = RDMSalesRequest.Where()
            where.column = "sold"
            where.operator = "="
            where.value = "1"
            whereList.add(where)

            val stockType = RDMSalesRequest.Where()
            stockType.column = "stock_type"
            stockType.operator = "="
            stockType.value = "4W"
            whereList.add(stockType)

        } else if (str == "total_sales") {

            val where = RDMSalesRequest.Where()
            where.column = "sold"
            where.operator = "="
            where.value = "1"
            whereList.add(where)

           /* val warrantyNumber = RDMSalesRequest.Where()
            warrantyNumber.column = "warranty_number"
            warrantyNumber.operator = "!="
            warrantyNumber.value = null
            whereList.add(warrantyNumber)*/

            val stockType = RDMSalesRequest.Where()
            stockType.column = "stock_type"
            stockType.operator = "="
            stockType.value = "4W"
            whereList.add(stockType)

           /* val isOffload = RDMSalesRequest.Where()
            isOffload.column = "isOffload"
            isOffload.operator = "="
            isOffload.value = "NO"
            whereList.add(isOffload)*/

            /*val soldDate = RDMSalesRequest.Where()
            soldDate.column = "sold_date"
            soldDate.operator = ">="
            soldDate.value = CommonMethods.getFirstDay(Date())
            whereList.add(soldDate)*/

        }
        /*else if (str == "above30") {
            val where = RDMSalesRequest.Where()
            where.column = "stock_age"
            where.operator = ">="
            where.value = "30"
            whereList.add(where)
        } else if (str == "above60") {
            val where = RDMSalesRequest.Where()
            where.column = "stock_age"
            where.operator = ">="
            where.value = "60"
            whereList.add(where)
        } else if (str == "visibleOnMFC") {
            val where = RDMSalesRequest.Where()
            where.column = "photo_count"
            where.operator = ">"
            where.value = "0"
            whereList.add(where)

            val where2 = RDMSalesRequest.Where()
            where2.column = "private_vehicle"
            where2.operator = "="
            where2.value = "true"
            whereList.add(where2)
        }*/

        return whereList
    }


    private fun getWhereInList(str: String): java.util.ArrayList<RDMSalesRequest.WhereIn> {
        val whereInList = java.util.ArrayList<RDMSalesRequest.WhereIn>()
        val whereIn1 = RDMSalesRequest.WhereIn()
        whereIn1.column = "dealer_code"
        whereIn1.values = arrayOf("$code")
        whereInList.add(whereIn1)
        if (str == "total_sales") {
            val whereIn2 = RDMSalesRequest.WhereIn()
            whereIn2.column = "source"
            whereIn2.values = arrayOf("mfc", "p&s")
            whereInList.add(whereIn2)
        }

        if (str == "non-warranty") {
            val whereIn2 = RDMSalesRequest.WhereIn()
            whereIn2.column = "source"
            whereIn2.values = arrayOf("mfc", "p&s")
            whereInList.add(whereIn2)
        }

        if (str == "warranty") {
            val whereIn2 = RDMSalesRequest.WhereIn()
            whereIn2.column = "source"
            whereIn2.values = arrayOf("mfc", "p&s")
            whereInList.add(whereIn2)
        }
        /*if (str == "parkAndSell") {
           val whereIn2 = RDMSalesRequest.WhereIn()
           whereIn2.column = "stock_source"
           whereIn2.values = arrayOf("p&s")
           whereInList.add(whereIn2)
       } else if (str == "withoutImage") {
           val whereIn2 = RDMSalesRequest.WhereIn()
           whereIn2.column = "photo_count"
           whereIn2.values = arrayOf("0")
           whereInList.add(whereIn2)
       }*/

        return whereInList
    }
}