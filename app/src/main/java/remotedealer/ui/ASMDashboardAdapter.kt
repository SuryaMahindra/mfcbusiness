package remotedealer.ui

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.*
import android.widget.*
import androidx.annotation.ColorRes
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.mfcwl.mfc_dealer.R
import com.mfcwl.mfc_dealer.Utility.AppConstants
import com.mfcwl.mfc_dealer.Utility.CommonMethods.getToken
import com.mfcwl.mfc_dealer.Utility.CommonMethods.getstringvaluefromkey
import com.mfcwl.mfc_dealer.Utility.SpinnerManager
import remotedealer.model.DealerItem
import remotedealer.model.dashboard.DealerNotOperationalRequest
import remotedealer.model.dashboard.EmailRequest
import remotedealer.model.dashboard.PhoneNumberResponse
import remotedealer.model.dashboard.RDRStatusResponse
import remotedealer.rdrform.ui.RDMHomeActivity
import remotedealer.retrofit.RetroBase
import remotedealer.util.RedirectionConstants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.reflect.Method


class ASMDashboardAdapter(val userList: ArrayList<DealerItem>, val statusList: ArrayList<RDRStatusResponse.RDRStatusData>, val statusListner: ChangeDealerStatus) : RecyclerView.Adapter<ASMDashboardAdapter.ViewHolder>() {

    interface ChangeDealerStatus {
        fun onStatusChange()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.asm_dealer_list_row, parent, false)
        Log.i("ASMDashboardAdapter", "onCreateViewHolder: ")
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(userList[position], statusList[position], statusListner)
    }


    override fun getItemCount(): Int {
        return userList.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        fun bindItems(user: DealerItem, mstatus: RDRStatusResponse.RDRStatusData, listner: ChangeDealerStatus) {
            val textViewName = itemView.findViewById(R.id.txtdealername) as TextView
            val textViewStatus = itemView.findViewById(R.id.txtdealerstatus) as TextView
            val textViewType = itemView.findViewById(R.id.txttype) as TextView
            val textViewDeatils = itemView.findViewById(R.id.txtviewdetails) as TextView
            val buttonfillRDR = itemView.findViewById(R.id.fillRDR) as Button
            val imageMore = itemView.findViewById(R.id.moreoptions) as ImageView
            val llimageMore = itemView.findViewById(R.id.llmoreoptions) as LinearLayout

            Log.i("ASMDashboardAdapter", "bindItems: " + user.code)
            textViewDeatils.setOnClickListener { view ->
                val intent = Intent(buttonfillRDR.context, RDMHomeActivity::class.java)
                intent.putExtra(AppConstants.DEALER_CODE, user.code)
                intent.putExtra(AppConstants.DEALER_NAME, user.name)
                intent.putExtra(AppConstants.MEETING_TYPE, user.type)
                intent.putExtra(AppConstants.RDR_STATUS, mstatus.status)
                intent.putExtra(AppConstants.IS_OP, mstatus.isOp)
                intent.putExtra(RedirectionConstants.REDIRECT_KEY, RedirectionConstants.VIEWDETAILS)
                buttonfillRDR.context.startActivity(intent)

            }

            llimageMore.setOnClickListener { view ->

                showPopup(imageMore, user, listner, mstatus)
            }

            buttonfillRDR.setOnClickListener { view ->
                val intent = Intent(buttonfillRDR.context, RDMHomeActivity::class.java)
                intent.putExtra(AppConstants.DEALER_CODE, "" + user.code)
                intent.putExtra(AppConstants.DEALER_NAME, "" + user.name)
                intent.putExtra(AppConstants.MEETING_TYPE, "" + user.type)
                intent.putExtra(AppConstants.RDR_STATUS, mstatus.status)
                intent.putExtra(AppConstants.IS_OP, mstatus.isOp)
                var label = buttonfillRDR.text.toString()
                if (label.equals("View", ignoreCase = true)) {
                    intent.putExtra(RedirectionConstants.REDIRECT_KEY, RedirectionConstants.RDRVIEWFORMS)
                } else {
                    intent.putExtra(RedirectionConstants.REDIRECT_KEY, RedirectionConstants.RDRFORMS)
                }
                buttonfillRDR.context.startActivity(intent)
            }

            textViewName.text = user.name
            if (mstatus.status.equals("pending", ignoreCase = true)) {

                textViewStatus.setTextColorRes(R.color.asm_red)
            }
            if (mstatus.status.equals("completed", ignoreCase = true)) {
                buttonfillRDR.text = "View"
                textViewStatus.setTextColorRes(R.color.asm_green)
            } else {
                buttonfillRDR.text = "Fill RDR"
            }
            textViewStatus.text = "" + mstatus.status
            textViewType.text = user.type

        }


        fun TextView.setTextColorRes(@ColorRes colorRes: Int) {
            val color = ContextCompat.getColor(context, colorRes)
            setTextColor(color)
        }


        fun showPopup(view: View, user: DealerItem, listner: ChangeDealerStatus, mstatus: RDRStatusResponse.RDRStatusData) {
            val popup = PopupMenu(view.context, view)
            popup.inflate(R.menu.asm_popup_menu)
            try {

                if (mstatus.status.equals("Completed", ignoreCase = true) && mstatus.isOp == false) {
                    popup.menu.findItem(R.id.makeoperational).setVisible(true)
                    popup.menu.findItem(R.id.nonoperational).setVisible(false)
                } else if (mstatus.status.equals("Completed", ignoreCase = true) && mstatus.isOp == true) {
                    popup.menu.findItem(R.id.makeoperational).setVisible(false)
                    popup.menu.findItem(R.id.nonoperational).setVisible(false)
                } else if (mstatus.status.equals("Pending", ignoreCase = true) && mstatus.isOp == false) {
                    popup.menu.findItem(R.id.makeoperational).setVisible(false)
                    popup.menu.findItem(R.id.nonoperational).setVisible(true)
                }
                val method: Method = popup.menu.javaClass.getDeclaredMethod("setOptionalIconsVisible", Boolean::class.javaPrimitiveType)
                method.setAccessible(true)
                method.invoke(popup.menu, true)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            popup.setOnMenuItemClickListener { item: MenuItem? ->


                when (item!!.itemId) {
                    R.id.nonoperational -> {
                        var token = getstringvaluefromkey(view.context as Activity?, "asmBearer")
                        Log.e("name and code ", "name and code" + user.name + "  " + user.code)
                        updateDealerNotOperataional(view.context, token, user, listner)
                    }

                    R.id.makeoperational -> {
                        var token = getstringvaluefromkey(view.context as Activity?, "asmBearer")
                        Log.e("name and code ", "name and code" + user.name + "  " + user.code)
                        updateDealerOperataional(view.context, token, user, listner)
                    }

                    R.id.emailreport -> {
                        sendEmailReport(view.context, user)
                    }

                    R.id.call -> {
                        var token = getstringvaluefromkey(view.context as Activity?, "asmBearer")
                        getPhoneNumber(view.context, token, user)
                    }
                    R.id.schedule -> {
                        val intent = Intent(view.context, RDMHomeActivity::class.java)
                        intent.putExtra(RedirectionConstants.REDIRECT_KEY, RedirectionConstants.DEALER_EVENTS)
                        intent.putExtra(AppConstants.DEALER_CODE, "" + user.code)
                        intent.putExtra(AppConstants.DEALER_NAME, "" + user.name)
                        intent.putExtra(AppConstants.MEETING_TYPE, "" + user.type)
                        intent.putExtra(AppConstants.RDR_STATUS, mstatus.status)
                        intent.putExtra(AppConstants.IS_OP, mstatus.isOp)

                        view.context.startActivity(intent)
                    }
                }

                true
            }

            popup.show()
        }

        fun updateDealerNotOperataional(ctx: Context, token: String, user: DealerItem, listner: ChangeDealerStatus) {
            var reqData = DealerNotOperationalRequest.ReportFor(user.code, "No", "NA", getstringvaluefromkey(ctx as Activity?, "user_id").toInt())
            var req = DealerNotOperationalRequest(reqData)

            RetroBase.retroInterface.getFromWeb(req, RetroBase.URL_END_DEALER_NON_OPERATIONAL, "Bearer " + token).enqueue(object : Callback<Any> {
                override fun onResponse(call: Call<Any>, response: Response<Any>) {
                    //  Toast.makeText(ctx, "Your request sent", Toast.LENGTH_LONG).show()
                    if (listner != null) {
                        listner.onStatusChange()
                    }

                }

                override fun onFailure(call: Call<Any>, t: Throwable) {
                    t.printStackTrace()
                    Toast.makeText(ctx, "Your request failed, please try again", Toast.LENGTH_LONG).show()
                }

            })

        }

        fun updateDealerOperataional(ctx: Context, token: String, user: DealerItem, listner: ChangeDealerStatus) {


            RetroBase.retroInterface.makeDealerOperational(RetroBase.URL_END_DEALER_OPERATIONAL + "?DealerCode=" + user.code, "Bearer " + token).enqueue(object : Callback<Any> {
                override fun onResponse(call: Call<Any>, response: Response<Any>) {
                    //  Toast.makeText(ctx, "Your request sent", Toast.LENGTH_LONG).show()
                    if (listner != null) {
                        listner.onStatusChange()
                    }

                }

                override fun onFailure(call: Call<Any>, t: Throwable) {
                    t.printStackTrace()
                    Toast.makeText(ctx, "Your request failed, please try again", Toast.LENGTH_LONG).show()
                }

            })

        }

        fun sendEmailReport(ctx: Context, user: DealerItem) {
            var reqData = EmailRequest(user.code, user.type, "2020-07-01", "true", "2020-07-30")
            RetroBase.retroInterface.sendEmail(reqData, RetroBase.URL_END_DEALER_SEND_EMAIL_REPORT, getToken("access_token_from_header")).enqueue(object : Callback<Any> {
                override fun onResponse(call: Call<Any>, response: Response<Any>) {
                    Toast.makeText(ctx, "Your request sent", Toast.LENGTH_LONG).show()
                }

                override fun onFailure(call: Call<Any>, t: Throwable) {
                    t.printStackTrace()
                    Toast.makeText(ctx, "Your request failed, please try again", Toast.LENGTH_LONG).show()
                }

            })

        }


        fun getPhoneNumber(ctx: Context, token: String, user: DealerItem) {

            SpinnerManager.showSpinner(ctx)
            RetroBase.retroInterface.getFromWeb(RetroBase.URL_END_DEALER_GETPHONE_NUMBERS + "?" + "DealerCode=" + user.code, "Bearer " + token).enqueue(object : Callback<Any> {
                override fun onResponse(call: Call<Any>, response: Response<Any>) {
                    SpinnerManager.hideSpinner(ctx)

                    try {
                        val strRes = Gson().toJson(response.body())
                        val modelRes = Gson().fromJson(strRes, PhoneNumberResponse::class.java)

                        if (modelRes.data.isNotEmpty())
                            showDialog(ctx, modelRes)
                        else
                            Toast.makeText(ctx,"No Phone number found",Toast.LENGTH_SHORT).show()

                    } catch (exc: Exception) {
                        exc.printStackTrace()
                    }

                }

                override fun onFailure(call: Call<Any>, t: Throwable) {
                    SpinnerManager.hideSpinner(ctx)

                    t.printStackTrace()

                }

            })

        }

        private fun showDialog(mContext: Context, response: PhoneNumberResponse) {
            val dialog = Dialog(mContext, R.style.Theme_Dialog)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.call_dialog)
            val closedialog = dialog.findViewById(R.id.closedialog) as LinearLayout
            var datalayout = dialog.findViewById(R.id.datalayout) as LinearLayout
            datalayout.removeAllViews()
            var distinctRes = response.data.distinctBy { it.mobile }
            closedialog.setOnClickListener {
                dialog.dismiss()
            }

            for (item in distinctRes) {
                val view: View = LayoutInflater.from(mContext).inflate(R.layout.phone_number_row, null)
                var txtname = view.findViewById<View>(R.id.calldata) as TextView
                var phone = view.findViewById<View>(R.id.pohnenumber) as TextView
                var callImage = view.findViewById<View>(R.id.buttoncall) as LinearLayout

                callImage.setOnClickListener {
                    dialog.dismiss()
                    if (item.mobile.equals("") || item.mobile == null) {
                        Toast.makeText(mContext, "No mobile number", Toast.LENGTH_LONG).show()
                        return@setOnClickListener
                    }
                    val intent = Intent(Intent.ACTION_CALL)
                    intent.data = Uri.parse("tel:" + item.mobile.trim())
                    mContext.startActivity(intent)
                }
                if (item.mobile != "") {
                    txtname.text = "" + item.name + " | " + item.type
                    phone.text = "" + item.mobile
                    datalayout?.addView(view, datalayout!!.childCount - 1)

                }

            }
            dialog.show()

        }


    }


}