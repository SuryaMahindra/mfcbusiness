package remotedealer.ui;

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mfcwl.mfc_dealer.R
import kotlinx.android.synthetic.main.rdm_sales_list_fragment.view.*
import remotedealer.model.sales.RDMSalesRequest
import remotedealer.model.sales.TabsPagerAdapter

/*
 * Created By Uday(MFCWL) ->  17-12-2020 17:02
 *
 */
public class RDMSalesListFragment(
        var token: String,
        var code: String,
        var filterWhereList: ArrayList<RDMSalesRequest.Where>,
        var filterWhereInList: ArrayList<RDMSalesRequest.WhereIn>,
) : Fragment() {

    var TAG = javaClass.simpleName
    lateinit var vi: View

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?,
    ): View {
        Log.i(TAG, "onCreateView: ")

        vi = inflater.inflate(R.layout.rdm_sales_list_fragment, container, false)

        initViews()

        return vi
    }

    fun initViews() {
        val tabsPagerAdapter = TabsPagerAdapter(requireContext(), requireActivity().supportFragmentManager,
                code, filterWhereList, filterWhereInList)
        vi.viewPagerSalesList.adapter = tabsPagerAdapter
        vi.tabLayoutSalesList.setupWithViewPager(vi.viewPagerSalesList)

        vi.ivSalesFilter.setOnClickListener { Log.i(TAG, "initViews: ") }
        vi.ivSalesFilter.visibility = View.INVISIBLE
    }

}