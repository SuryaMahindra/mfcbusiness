package remotedealer.ui;

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.mfcwl.mfc_dealer.R
import com.mfcwl.mfc_dealer.Utility.CommonMethods
import com.mfcwl.mfc_dealer.Utility.SpinnerManager
import kotlinx.android.synthetic.main.rdm_total_sales_fragment.view.recyclerViewTotalSales
import kotlinx.android.synthetic.main.rdm_total_warranty_sales_fragment.view.*
import remotedealer.adapter.RDMSalesListAdapter
import remotedealer.model.sales.RDMSalesRequest
import remotedealer.model.sales.RDMSalesResponse
import remotedealer.retrofit.RetroBase
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/*
 * Created By Uday(MFCWL) ->  18-12-2020 18:09
 *
 */
public class RDMTotalWarrantySalesFragment(var code: String) : Fragment() {
    var TAG = javaClass.simpleName
    lateinit var vi: View

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?,
    ): View {
        Log.i(TAG, "onCreateView: ")

        vi = inflater.inflate(R.layout.rdm_total_warranty_sales_fragment, container, false)

        initViews()

        return vi
    }

    fun initViews() {

        vi.btnWarrantySales.setOnClickListener {

            vi.btnWarrantySales.setTextColor(resources.getColor(R.color.white))
            vi.btnWarrantySales.setBackgroundResource(R.drawable.rounded_bg_for_btn)

            vi.btnNonWarrantySales.setTextColor(resources.getColor(R.color.black))
            vi.btnNonWarrantySales.setBackgroundResource(R.drawable.rounded_bg_for_btn_not_selected)
        }

        vi.btnNonWarrantySales.setOnClickListener {

            vi.btnNonWarrantySales.setTextColor(resources.getColor(R.color.white))
            vi.btnNonWarrantySales.setBackgroundResource(R.drawable.rounded_bg_for_btn)

            vi.btnWarrantySales.setTextColor(resources.getColor(R.color.black))
            vi.btnWarrantySales.setBackgroundResource(R.drawable.rounded_bg_for_btn_not_selected)
        }

        callRDMSales()
    }

    private fun callRDMSales() {

        val req = RDMSalesRequest()

        val whereIn = RDMSalesRequest.WhereIn()
        whereIn.column = "dealer_code"
        whereIn.values = arrayOf(code)

        val whereInList = ArrayList<RDMSalesRequest.WhereIn>()
        whereInList.add(whereIn)

        req.orderBy = "sold_date"
        req.orderByReverse = "true"
        req.page = "1"
        req.pageItems = "100"
        req.whereIn = whereInList

        SpinnerManager.showSpinner(context)
        RetroBase.mfcRetroInterface.getFromWebUsingToken(req, RetroBase.URL_END_RDM_SALES, CommonMethods.getToken("token")).enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                SpinnerManager.hideSpinner(context)

                try {
                    val strRes = Gson().toJson(response.body())
                    val strModel = Gson().fromJson(strRes, RDMSalesResponse::class.java)

                    Log.i(TAG, "onResponse: " + strModel.data.size)


                    val adapter = RDMSalesListAdapter(requireContext(), requireActivity(), strModel.data)

                    vi.recyclerViewTotalSales.layoutManager = LinearLayoutManager(context)
                    vi.recyclerViewTotalSales.adapter = adapter

                } catch (e: Exception) {
                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(context)
            }
        })

    }
}