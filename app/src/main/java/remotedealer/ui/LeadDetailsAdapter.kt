package remotedealer.ui

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.common.data.DataHolder
import com.mfcwl.mfc_dealer.Model.LeadSection.PrivateLeadDatum
import com.mfcwl.mfc_dealer.Model.LeadSection.WebLeadsDatum
import com.mfcwl.mfc_dealer.Model.Leads.PrivateLeadModel
import com.mfcwl.mfc_dealer.R
import com.microsoft.graph.models.extensions.Contact
import remotedealer.util.NavigationUtility
import java.lang.IndexOutOfBoundsException
import java.lang.NullPointerException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class LeadDetailsAdapter(val activity: FragmentActivity, var list: List<WebLeadsDatum>?) : RecyclerView.Adapter<LeadDetailsAdapter.ViewHolder>() {

    var leadList = list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.sales_lead_card, parent, false)
        return ViewHolder(view, activity)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        try {
            val data = list?.get(position) ?: return

            holder.leads_name.text = data.name
            holder.leads_num.text = data.mobile
            holder.leads_comp_name.text = data.primaryMake
            holder.leads_car_model.text = data.primaryModel
//        holder.leads_days.text = data.leaddate
//        holder.leads_date.text = data.followUpDate
            if (data.followUpDate.equals("0000-00-00 00:00:00")) {
                holder.leads_date.text = "NA"
            } else {
                holder.leads_date.text = getRequiredDateFormat(data.followUpDate)
            }
            holder.leads_followup_history.text = data.status

            if (data.status == "open" || data.status == "Open" || data.status == "OPEN") {
                holder.leads_followup_history.setBackgroundResource(R.drawable.open_lead_status_bg)
            } else if (data.status == "hot" || data.status == "Hot" || data.status == "HOT") {
                holder.leads_followup_history.setBackgroundResource(R.drawable.hot_lead_status_bg)
            } else if (data.status == "warm" || data.status == "Warm" || data.status == "WARM") {
                holder.leads_followup_history.setBackgroundResource(R.drawable.warm_lead_status_bg)
            } else if (data.status == "cold" || data.status == "Cold" || data.status == "COLD") {
                holder.leads_followup_history.setBackgroundResource(R.drawable.cold_lead_status)
            } else if (data.status == "Walk-in" || data.status == "walk-in" || data.status == "WALK-IN" || data.status == "Walk-In" || data.status == "Test Drive Taken" || data.status == "test drive taken" || data.status == "TEST DRIVEN TAKEN") {
                holder.leads_followup_history.setBackgroundResource(R.drawable.walk_in_test_drive_lead_status_bg)
            } else if (data.status == "") {
                holder.leads_followup_history.setBackgroundResource(0)
            } else {
                holder.leads_followup_history.setBackgroundResource(R.drawable.lead_status_grey_bg)
            }

            val createdDate: String = data.leadDate
            val splitDateTime = createdDate.split(" ").toTypedArray()
            val splitDate = splitDateTime[0].split("-").toTypedArray()
            val finalDate = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0]

            val simpleDateFormat = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
            val c = Calendar.getInstance()
            val formattedDate = simpleDateFormat.format(c.time)
            val splitSpace = formattedDate.split(" ").toTypedArray()

            try {
                val date1 = simpleDateFormat.parse("$finalDate 00:00:00")
                val date2 = simpleDateFormat.parse(splitSpace[0] + " 00:00:00")
                when {
                    printDifference(date1, date2) == 0L -> {
                        holder.leads_days.text = "Today"
                    }
                    printDifference(date1, date2) == 1L -> {
                        holder.leads_days.text = "Yesterday"
                    }
                    printDifference(date1, date2) > 364 -> {
                        holder.leads_days.text = "" + printDifference(date1, date2) / 365 + " Y ago"
                    }
                    else -> {
                        holder.leads_days.text = "" + printDifference(date1, date2) + " D ago"
                    }
                }
            } catch (e: ParseException) {
                e.printStackTrace()
            }

        } catch (exc: IndexOutOfBoundsException) {
            exc.printStackTrace()
            clearFilter()
        }

        /*holder.itemView.setOnClickListener{
            val fragment  = LeadDetailFragment(
                    data.name,
                    data.email,
                    data.mobile,
                    data.status,
                    data.postedDate,
                    data.primaryMake,
                    data.primaryModel,
                    data.price,
                    data.regno,
                    data.followUpDate,
                    data.executiveName,
                    data.registerCity,
                    data.owners.toString(),
                    data.regyear,
                    data.color,
                    data.kms.toString(),
                    data.dispatchid.toString(),
                    data.remark,
                    data.leadDate,
                    data.primaryVariant
            )
            setCurrentFragment(fragment)


        }*/
    }

    private fun setCurrentFragment(fragment: Fragment) = NavigationUtility.addFragment(fragment, activity, fragment.tag)
//            activity.supportFragmentManager.beginTransaction().apply {
//                replace(R.id.flFragment, fragment)
//                commit()
//            }

    private fun printDifference(startDate: Date, endDate: Date): Long {
        //milliseconds
        var different = endDate.time - startDate.time

        /* System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);*/
        val secondsInMilli: Long = 1000
        val minutesInMilli = secondsInMilli * 60
        val hoursInMilli = minutesInMilli * 60
        val daysInMilli = hoursInMilli * 24
        val elapsedDays = different / daysInMilli
        different = different % daysInMilli
        val elapsedHours = different / hoursInMilli
        different = different % hoursInMilli
        val elapsedMinutes = different / minutesInMilli
        different = different % minutesInMilli
        val elapsedSeconds = different / secondsInMilli
        return elapsedDays
    }

    override fun getItemCount(): Int {
        return list?.size ?: 0
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    class ViewHolder(itemView: View, activity: Activity) : RecyclerView.ViewHolder(itemView) {
        var leads_name: TextView
        var leads_num: TextView
        var leads_comp_name: TextView
        var leads_car_model: TextView
        var leads_date: TextView
        var leads_days: TextView
        var leads_followup_history: TextView
//        var leads_Call: CircleImageView
//        var leads_Messege: CircleImageView
//        var leads_Whatsapp: CircleImageView

        init {
            leads_name = itemView.findViewById(R.id.name)
            leads_num = itemView.findViewById(R.id.lead_phone_number)
            leads_comp_name = itemView.findViewById(R.id.veh_make)
            leads_car_model = itemView.findViewById(R.id.veh_model)
            leads_date = itemView.findViewById(R.id.follow_up_date_leads)
            leads_days = itemView.findViewById(R.id.posted)
            leads_followup_history = itemView.findViewById(R.id.textView12)
//            leads_Call = itemView.findViewById(R.id.leads_Call)
//            leads_Messege = itemView.findViewById(R.id.leads_Messege)
//            leads_Whatsapp = itemView.findViewById(R.id.leads_Whatsapp)
//            if (CommonMethods.getstringvaluefromkey(activity, "user_type").equals("dealer", ignoreCase = true)) {
//            } else {
//                leads_Call.visibility = View.INVISIBLE
//                leads_Messege.visibility = View.INVISIBLE
//                leads_Whatsapp.visibility = View.INVISIBLE
//            }
        }
    }

    private fun getRequiredDateFormat(date: String): String {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
        val outputFormat = SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH)
        var outDate = "NO Date"
        try {
            outDate = outputFormat.format(inputFormat.parse(date))
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return outDate
    }

    fun getFilter(): Filter? {
        return object : Filter() {
            protected override fun performFiltering(charSequence: CharSequence): FilterResults? {
                val charString = charSequence.toString()
                val filteredList: MutableList<WebLeadsDatum> = ArrayList()
                if (charString.isEmpty()) {
                    list = leadList
                    notifyDataSetChanged()
                } else {
                    for (row in leadList!!) {
                        if (row.name.contains(charString, true)
                                || row.primaryMake.contains(charString, true)
                                || row.primaryModel.contains(charString, true)
                                || row.primaryVariant.contains(charString, true)) {
                            filteredList.add(row)
                        }
                    }
                }
                val filterResults = FilterResults()
                filterResults.values = filteredList
                return filterResults
            }

            protected override fun publishResults(charSequence: CharSequence?, filterResults: FilterResults) {
                if (filterResults.values != null) {
                    try {
                        (filterResults.values as? ArrayList<WebLeadsDatum>).also {
                            it?.let {
                                list = it
                                if ((list as ArrayList<WebLeadsDatum>).isEmpty()) {
                                    Toast.makeText(activity, "No leads found", Toast.LENGTH_SHORT).show()
                                }

                            }
                        }
                        notifyDataSetChanged()

                    } catch (exc: NullPointerException) {
                        exc.printStackTrace()

                    }
                }

            }
        }
    }

    fun clearFilter() {
        list = leadList
        notifyDataSetChanged()
    }

}