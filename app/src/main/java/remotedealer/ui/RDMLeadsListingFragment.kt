package remotedealer.ui

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.opengl.Visibility
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.mfcwl.mfc_dealer.Activity.LeadSection.LeadConstantsSection
import com.mfcwl.mfc_dealer.InstanceCreate.LeadSection.LeadFilterSaveInstance
import com.mfcwl.mfc_dealer.Model.LeadSection.*
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall
import com.mfcwl.mfc_dealer.R
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse
import com.mfcwl.mfc_dealer.RetrofitServicesLeadSection.PrivateLeadService
import com.mfcwl.mfc_dealer.RetrofitServicesLeadSection.WebLeadService
import com.mfcwl.mfc_dealer.Utility.AppConstants
import com.mfcwl.mfc_dealer.Utility.CommonMethods
import com.mfcwl.mfc_dealer.Utility.SpinnerManager
import kotlinx.android.synthetic.main.addstock_fragment.*
import kotlinx.android.synthetic.main.sales_web_and_private_leads_fragment.*
import rdm.sort_dialogs.LeadSortDialog
import rdm.sort_dialogs.LeadSortDialog.LeadSortCallBack
import remotedealer.activity.RDMFilterActivity
import remotedealer.activity.RDMFilterActivity.ApplyFilterInf
import remotedealer.retrofit.RetroBase
import remotedealer.retrofit.RetroBase.Companion.URL_END_RDM_LEAD_ACCESS_TOKEN
import remotedealer.retrofit.RetroBase.Companion.lmsRetroInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


class RDMLeadsListingFragment() : Fragment(), ApplyFilterInf, LeadSortCallBack {

    private var filterCategory: String = ""
    private var fromDate: String = ""
    private var toDate: String = ""
    private var leadStatusFilter = ArrayList<String>()

    private var mOrderByReverse: Boolean = true
    private var isFollowUpLeadOnClick = false
    private var isAllLeadsOnClick = false
    lateinit var recyclerView: RecyclerView
    lateinit var btnAllLeads: Button
    lateinit var btnFollowUpLeads: Button
    lateinit var imageViewFilter: ImageView
    var mPrivateLeadsAdapter: PrivateLeadAdapter? = null
    var leadDetailsAdapter: LeadDetailsAdapter? = null
    var searchQueryFilter: String = ""
    var page_no = "1"
    var page = 1
    var isLoadmore = false
    private val searchQuery = ""
    private var mLeadsDatumList: MutableList<WebLeadsDatum>? = null
    private var mFollowuplist: MutableList<WebLeadsDatum>? = null
    private var mLeadsPrivateList: MutableList<PrivateLeadDatum>? = null
    private var mFollowupPrivatelist: MutableList<PrivateLeadDatum>? = null
    private var web_closed_lead_tag: String = ""
    private var leadType = "webleads"
    private var innerType = "all"
    lateinit var weblead: TextView
    lateinit var privateLead: TextView
    lateinit var ivASMLeadsSortBy: ImageView
    private var mwhereinList: List<PrivateLeadWhereIn>? = null
    private var mwhereList: MutableList<PrivateLeadWhere>? = null
    private var mOrderBy = "leads.created_at"
    private lateinit var mWebLeadsWherenotin: List<WhereNotIn>
    private lateinit var mWhereorList: MutableList<WebLeadsWhereor>
    private lateinit var mWherein: MutableList<WebleadsWhereIn>
    private var webLeadsCustomWheresList: MutableList<WebLeadsCustomWhere>? = null
    private var leadTypeTag: String = ""
    private var sortLeadBy: String = ""
    private var walkInOrderBy: String = "lead_date2"
    private var walkInOrderReverse: Boolean = true
    private lateinit var llLeadSortIV: LinearLayout
    private lateinit var llLeadFilterIV: LinearLayout
    private lateinit var llLeadSearchIV: LinearLayout


    private lateinit var mWebLeadsCustomWheres: MutableList<WebLeadsCustomWhere>
    val TAG = javaClass.simpleName
    private var token: String = ""
    private val pageItem = 100

    constructor(webLeadsCustomWheresList: MutableList<WebLeadsCustomWhere>?, mWherein: MutableList<WebleadsWhereIn>, web_closed_lead_tag: String, mwhereinList: MutableList<PrivateLeadWhereIn>?, mwhereList: MutableList<PrivateLeadWhere>?, innerType: String, leadTypeTag: String) : this() {
        this.webLeadsCustomWheresList = webLeadsCustomWheresList
        this.mWherein = mWherein
        this.web_closed_lead_tag = web_closed_lead_tag
        this.mwhereinList = mwhereinList
        this.mwhereList = mwhereList
        this.innerType = innerType
        this.leadTypeTag = leadTypeTag

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.i(TAG, "onCreateView: ")
        return inflater.inflate(R.layout.sales_web_and_private_leads_fragment, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.i(TAG, "onAttach: " + (context is Activity))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.i(TAG, "onViewCreated: ")
        initView(view)
    }


    fun initView(view: View) {
        btnAllLeads = view.findViewById(R.id.all_leads)
        btnFollowUpLeads = view.findViewById(R.id.follow_up)
        recyclerView = view.findViewById(R.id.leads_recyclerview)
        weblead = view.findViewById(R.id.web_leads)
        privateLead = view.findViewById(R.id.private_leads)
        ivASMLeadsSortBy = view.findViewById(R.id.ivASMLeadsSortBy)
        imageViewFilter = view.findViewById(R.id.imageViewFilter)
        val mSharedPreferences = activity?.getSharedPreferences("MFC", Context.MODE_PRIVATE)
        imageViewFilter.setBackgroundResource(R.drawable.ic_new_filter_enabled)
        llLeadSortIV = view.findViewById(R.id.llLeadSortIV)
        llLeadSearchIV = view.findViewById(R.id.llLeadSearchIV)
        llLeadFilterIV = view.findViewById(R.id.llLeadFilterIV)
        CommonMethods.setvalueAgainstKey(activity, AppConstants.ASM_LEAD_SORT, "")
        try {
            val filterReceiver = object : BroadcastReceiver() {
                override fun onReceive(context: Context?, intent: Intent?) {
                    val bundle = intent?.extras
                    val message = bundle?.get("filterCategory")
                    filterCategory = message.toString()
                    fromDate = bundle?.get("fromDate").toString()
                    toDate = bundle?.get("toDate").toString()
                    leadStatusFilter = bundle?.getStringArrayList("LeadStatusFilter")!!


                    if (filterCategory == "" && fromDate == "" && toDate == "" && leadStatusFilter.isEmpty()) {
                        isAllLeadsOnClick = true
                        imageViewFilter.setBackgroundResource(R.drawable.ic_new_filter)
                        mWherein.clear()
                        mwhereinList = getWalkInWhereIn("")
                        handleRequest()
                    } else {
                        imageViewFilter.setBackgroundResource(R.drawable.ic_new_filter_enabled)
                        isAllLeadsOnClick = false
                        isFollowUpLeadOnClick = false
                        webLeadsCustomWheresList = null
                        webLeadsCustomWheresList = applyFilter()
                        mwhereList = applyPrivateFilter()
                        if (leadStatusFilter.isNotEmpty()) {
                            mWherein = getLeadStatusFilter()
                            mwhereinList = getPrivateWhereIn()
                        }
                        handleRequest()
                    }


                }
            }
            activity?.let { LocalBroadcastManager.getInstance(it).registerReceiver(filterReceiver, IntentFilter("Filter_Tag")) }

        } catch (exception: Exception) {
            exception.printStackTrace()
        }
        llLeadFilterIV.setOnClickListener {
            moveToFilter()

            /* val bottomSheet = FilterWebLeadsBottomSheetFragment.newInstance("", "")
                    bottomSheet.show(requireActivity().supportFragmentManager, bottomSheet.tag)*/
        }

        llLeadSearchIV.setOnClickListener(View.OnClickListener {

            llLeadSearchView.visibility = View.VISIBLE
            llLeadTitle.visibility = View.GONE
        })


        ivNewLeadFilterSearch.setOnClickListener(View.OnClickListener {
            if (!etLeadSearchQ.text.equals("")) {
                CommonMethods.setvalueAgainstKey(activity, "LEAD_FILTER_SEARCH", etLeadSearchQ.text.toString())

                try {
                    if (it.windowToken != null) {
                        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(it!!.getWindowToken(), 0)
                    }

                } catch (ex: Exception) {
                    ex.printStackTrace()
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
                        applySearchResults(etLeadSearchQ.text.toString())
                    }
                }

            } else {
                Toast.makeText(activity, "Please enter value to Search", Toast.LENGTH_LONG)
            }
        })


        tvSearchQuery.setOnClickListener(View.OnClickListener {
            llLeadSearResult.visibility = View.GONE
            leadDetailsAdapter?.clearFilter()
            mPrivateLeadsAdapter?.clearFilter()
        })


        /* etLeadSearchQ.addTextChangedListener(object : TextWatcher {
             override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

             }

             override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
             }

             override fun afterTextChanged(s: Editable) {
                 CommonMethods.setvalueAgainstKey(activity, "LEAD_FILTER_SEARCH", etLeadSearchQ.text.toString())
                 applySearchResults(s)
             }
         })*/
        /*    weblead.setOnClickListener(View.OnClickListener { leadTypeTag=AppConstants.WEB_LEADS })
             .setOnClickListener(View.OnClickListener { leadTypeTag=AppConstants.PRIVATE_LEADS })*/
        ivNewLeadFilterSearchClose.setOnClickListener(View.OnClickListener {
            if (etLeadSearchQ.text.isNotEmpty()) {
                etLeadSearchQ.text.clear()
            } else {
                llLeadSearchView.visibility = View.GONE
                llLeadTitle.visibility = View.VISIBLE
                try {
                    if (it.windowToken != null) {
                        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(it.getWindowToken(), 0)
                    }

                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
            /* llLeadSearchView.visibility = View.GONE
             etLeadSearchQ.setText("")
             llLeadSearResult.visibility = View.GONE
             option_ll.visibility = View.VISIBLE
 */
        })

        etLeadSearchQ.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                /*  if (leadType == "private") {
                      leadDetailsAdapter?.clearFilter()
                      mPrivateLeadsAdapter?.getFilter()?.filter(s)
                  } else if (leadType == "webleads") {
                      mPrivateLeadsAdapter?.clearFilter()
                      leadDetailsAdapter?.getFilter()?.filter(s)
                  }*/
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                /*if (leadType == "private") {
                    leadDetailsAdapter?.clearFilter()
                    mPrivateLeadsAdapter?.getFilter()?.filter(s)
                } else if (leadType == "webleads") {
                    mPrivateLeadsAdapter?.clearFilter()
                    leadDetailsAdapter?.getFilter()?.filter(s)
                }*/
            }

            override fun afterTextChanged(s: Editable?) {

                if (etLeadSearchQ.visibility == View.VISIBLE && etLeadSearchQ.text.isNotEmpty()) {
                    if (leadType == "private") {
                        leadDetailsAdapter?.clearFilter()
                        mPrivateLeadsAdapter?.getFilter()?.filter(s)
                    } else if (leadType == "webleads") {
                        mPrivateLeadsAdapter?.clearFilter()
                        leadDetailsAdapter?.getFilter()?.filter(s)
                    }

                }

            }

        })

        etLeadSearchQ.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    try {
                        if (!etLeadSearchQ.text.equals("")) {
                            CommonMethods.setvalueAgainstKey(activity, "LEAD_FILTER_SEARCH", etLeadSearchQ.text.toString())
                            applySearchResults(etLeadSearchQ.text.toString())
                            if (v!!.windowToken != null) {
                                val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                                imm.hideSoftInputFromWindow(v!!.getWindowToken(), 0)
                            }
                        } else {
                            Toast.makeText(activity, "Please enter value to Search", Toast.LENGTH_LONG)
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
                            applySearchResults(etLeadSearchQ.text.toString())
                        }

                    }
                    return true
                }
                return false
            }
        })

        llLeadSortIV.setOnClickListener(View.OnClickListener {

            val leadSortDialog = LeadSortDialog(activity, this)
            leadSortDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            leadSortDialog.show()
            leadSortDialog.setCancelable(false)
        })

        if (leadTypeTag == AppConstants.WEB_LEADS) {
            makeWebLeadsActive()
            if (innerType == "all") {
                btnAllLeads.setTextColor(resources.getColor(R.color.white))
                btnAllLeads.setBackgroundResource(R.drawable.rounded_bg_for_btn)

                btnFollowUpLeads.setTextColor(resources.getColor(R.color.black))
                btnFollowUpLeads.setBackgroundResource(R.drawable.rounded_bg_for_btn_not_selected)

            } else if (innerType == "followup") {
                btnFollowUpLeads.setTextColor(resources.getColor(R.color.white))
                btnFollowUpLeads.setBackgroundResource(R.drawable.rounded_bg_for_btn)

                btnAllLeads.setTextColor(resources.getColor(R.color.black))
                btnAllLeads.setBackgroundResource(R.drawable.rounded_bg_for_btn_not_selected)
            }

        } else if (leadTypeTag == AppConstants.PRIVATE_LEADS) {
            makePrivateLeadsActive()

            if (innerType == "all") {
                btnAllLeads.setTextColor(resources.getColor(R.color.white))
                btnAllLeads.setBackgroundResource(R.drawable.rounded_bg_for_btn)

                btnFollowUpLeads.setTextColor(resources.getColor(R.color.black))
                btnFollowUpLeads.setBackgroundResource(R.drawable.rounded_bg_for_btn_not_selected)
            } else if (innerType == "followup") {
                btnFollowUpLeads.setTextColor(resources.getColor(R.color.white))
                btnFollowUpLeads.setBackgroundResource(R.drawable.rounded_bg_for_btn)

                btnAllLeads.setTextColor(resources.getColor(R.color.black))
                btnAllLeads.setBackgroundResource(R.drawable.rounded_bg_for_btn_not_selected)

            }
        }

        weblead.setOnClickListener {
            leadTypeTag = AppConstants.WEB_LEADS
            makeWebLeadsActive()
            highLightAllLeads()
            try {
                if (filterCategory == "" && leadStatusFilter.isEmpty()) {
                    isAllLeadsOnClick=true
                    clearLeadsFilter()
                    imageViewFilter.setBackgroundResource(R.drawable.ic_new_filter)
                } else {
                    imageViewFilter.setBackgroundResource(R.drawable.ic_new_filter_enabled)
                }
                if (llLeadSearchView.visibility == View.VISIBLE) {
                    llLeadSearchView.visibility = View.GONE
                    llLeadTitle.visibility = View.VISIBLE
                    etLeadSearchQ.setText("")

                }
                if (llLeadSearResult.visibility == View.VISIBLE) {
                    tvSearchQuery.text = ""
                    llLeadSearResult.visibility = View.GONE
                }
            } catch (exc: java.lang.Exception) {
                exc.printStackTrace()
            }

            getWebLeadToken()
        }

        privateLead.setOnClickListener {

            leadTypeTag = AppConstants.PRIVATE_LEADS
            makePrivateLeadsActive()
            highLightAllLeads()
            try {
                if (filterCategory == "" && leadStatusFilter.isEmpty()) {
                    isAllLeadsOnClick = true
                    clearLeadsFilter()
                    imageViewFilter.setBackgroundResource(R.drawable.ic_new_filter)
                } else {
                    imageViewFilter.setBackgroundResource(R.drawable.ic_new_filter_enabled)
                }
                if (llLeadSearchView.visibility == View.VISIBLE) {
                    llLeadSearchView.visibility = View.GONE
                    llLeadTitle.visibility = View.VISIBLE
                    etLeadSearchQ.setText("")

                }
                if (llLeadSearResult.visibility == View.VISIBLE) {
                    tvSearchQuery.text = ""
                    llLeadSearResult.visibility = View.GONE
                }
            } catch (exc: java.lang.Exception) {
                exc.printStackTrace()
            }


            getWebLeadToken()
        }

        btnAllLeads.setOnClickListener {
            isAllLeadsOnClick = true

            try {
                if (filterCategory == "" && leadStatusFilter.isEmpty()) {
                    clearLeadsFilter()
                    imageViewFilter.setBackgroundResource(R.drawable.ic_new_filter)
                } else {
                    imageViewFilter.setBackgroundResource(R.drawable.ic_new_filter_enabled)
                }

                if (llLeadSearchView.visibility == View.VISIBLE) {
                    llLeadSearchView.visibility = View.GONE
                    llLeadTitle.visibility = View.VISIBLE
                    etLeadSearchQ.setText("")

                }
                if (llLeadSearResult.visibility == View.VISIBLE) {
                    tvSearchQuery.text = ""
                    llLeadSearResult.visibility = View.GONE
                }
            } catch (exc: java.lang.Exception) {
                exc.printStackTrace()
            }

            highLightAllLeads()
            handleRequest()
        }

        btnFollowUpLeads.setOnClickListener {
            isFollowUpLeadOnClick = true

            try {
                if (filterCategory == "" && leadStatusFilter.isEmpty()) {
                    clearLeadsFilter()
                    imageViewFilter.setBackgroundResource(R.drawable.ic_new_filter)
                } else {
                    imageViewFilter.setBackgroundResource(R.drawable.ic_new_filter_enabled)
                }
                if (llLeadSearchView.visibility == View.VISIBLE) {
                    llLeadSearchView.visibility = View.GONE
                    llLeadTitle.visibility = View.VISIBLE
                    etLeadSearchQ.setText("")

                }
                if (llLeadSearResult.visibility == View.VISIBLE) {
                    tvSearchQuery.text = ""
                    llLeadSearResult.visibility = View.GONE
                }
            } catch (exc: java.lang.Exception) {
                exc.printStackTrace()
            }

            highLightFollowUpLeads()
            handleRequest()

        }


        getWebLeadToken()
    }

    private fun clearLeadsFilter() {
        CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_FILTER_CATEGORY, "")
        CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_FILTER_CATEGORY_VALUE, "");
        try {
            if (CommonMethods.getstringvaluefromkey(activity, AppConstants.LEAD_STATUS_COUNT).isNotEmpty()) {
                CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_STATUS_COUNT, "");
                CommonMethods.clearLeadStatusMap(activity);
            }

        } catch (exception: java.lang.Exception) {
            exception.printStackTrace()
        }

    }

    private fun applySearchResults(s: String) {

        if (leadType == "webleads") {
            mPrivateLeadsAdapter?.clearFilter()
            leadDetailsAdapter?.getFilter()?.filter(s)
        } else if (leadType == "private") {
            leadDetailsAdapter?.clearFilter()
            mPrivateLeadsAdapter?.getFilter()?.filter(s)
        }
        llLeadSearResult.visibility = View.VISIBLE
        tvSearchQuery.text = s
        llLeadSearchView.visibility = View.GONE
        llLeadTitle.visibility = View.VISIBLE
        etLeadSearchQ.setText("")


    }


    private fun getPrivateWhereIn(): MutableList<PrivateLeadWhereIn> {
        val list = mutableListOf<PrivateLeadWhereIn>()

        val whereIn = PrivateLeadWhereIn()
        whereIn.column = "dealer_id"
        val valuesList = ArrayList<String>()
        valuesList.add(CommonMethods.getstringvaluefromkey(activity, "dealer_code"))
        whereIn.values = valuesList

        val leadStatus = PrivateLeadWhereIn()
        leadStatus.column = "lead_status"
        leadStatus.values = leadStatusFilter

        list.add(whereIn)
        list.add(leadStatus)


        return list
    }

    private fun applyPrivateFilter(): MutableList<PrivateLeadWhere>? {
        val list = mutableListOf<PrivateLeadWhere>()
        val leadType = PrivateLeadWhere()
        leadType.column = "lead_type"
        leadType.operator = "="
        leadType.value = "sales"

        val leadTag = PrivateLeadWhere()
        leadTag.column = "lead_tags"
        leadTag.operator = "="
        leadTag.value = "usedcar"
        list.add(leadType)
        list.add(leadTag)

        if (filterCategory.equals(AppConstants.POSTED_DATE)) {

            val followDateFrom = PrivateLeadWhere()
            followDateFrom.column = "lead_date2"
            followDateFrom.operator = ">="
            followDateFrom.value = fromDate

            val dateTo = PrivateLeadWhere()
            dateTo.column = "lead_date2"
            dateTo.operator = "<="
            dateTo.value = toDate

            list.add(followDateFrom)
            list.add(dateTo)

        } else if (filterCategory.equals(AppConstants.FOLLOW_DATE)) {
            val followDateFrom = PrivateLeadWhere()
            followDateFrom.column = "follow_date"
            followDateFrom.operator = ">="
            followDateFrom.value = fromDate

            val fDateTo = PrivateLeadWhere()
            fDateTo.column = "follow_date"
            fDateTo.operator = "<="
            fDateTo.value = toDate

            list.add(followDateFrom)
            list.add(fDateTo)

        }

        return list
    }

    private fun getLeadStatusFilter(): MutableList<WebleadsWhereIn> {
        val list = mutableListOf<WebleadsWhereIn>()
        val whereIn = WebleadsWhereIn()
        whereIn.column = "dispatched.status"
        val valuesList = leadStatusFilter
        whereIn.value = valuesList
        list.add(whereIn)
        return list
    }


    private fun applyFilter(): MutableList<WebLeadsCustomWhere> {

        val list = mutableListOf<WebLeadsCustomWhere>()

        val leadType1 = WebLeadsCustomWhere()
        leadType1.column = "leads.lead_type"
        leadType1.operator = "="
        leadType1.value = "USEDCARSALES"


        val targetId = WebLeadsCustomWhere()
        targetId.column = "dispatched.target_id"
        targetId.operator = "="
        targetId.value = CommonMethods.getstringvaluefromkey(activity, "dealer_code")

        if (filterCategory.equals(AppConstants.POSTED_DATE)) {
            val from = WebLeadsCustomWhere()
            from.column = "leads.created_at"
            from.operator = ">="
            from.value = fromDate + " 00:00:00"

            val to = WebLeadsCustomWhere()
            to.column = "leads.created_at"
            to.operator = "<="
            to.value = toDate + " 23:59:59"
            list.add(leadType1)
            list.add(targetId)
            list.add(from)
            list.add(to)

        } else if (filterCategory.equals(AppConstants.FOLLOW_DATE)) {
            val from = WebLeadsCustomWhere()
            from.column = "dispatched.followup_date"
            from.operator = ">="
            from.value = fromDate + " 00:00:00"

            val to = WebLeadsCustomWhere()
            to.column = "dispatched.followup_date"
            to.operator = "<="
            to.value = toDate + " 23:59:59"
            list.add(leadType1)
            list.add(targetId)
            list.add(from)
            list.add(to)

        }

        return list
    }

    private fun moveToFilter() {
        try {

            val intent = Intent(activity, RDMFilterActivity::class.java)
            activity?.startActivity(intent)
        } catch (exception: Exception) {
            exception.printStackTrace()
        }

    }

    private fun highLightAllLeads() {
        innerType = "all"
        isFollowUpLeadOnClick = false

        recyclerView.recycledViewPool.clear()

        btnAllLeads.setTextColor(resources.getColor(R.color.white))
        btnAllLeads.setBackgroundResource(R.drawable.rounded_bg_for_btn)

        btnFollowUpLeads.setTextColor(resources.getColor(R.color.black))
        btnFollowUpLeads.setBackgroundResource(R.drawable.rounded_bg_for_btn_not_selected)

    }

    private fun highLightFollowUpLeads() {
        innerType = "followup"
        isAllLeadsOnClick = false
        recyclerView.recycledViewPool.clear()
        btnFollowUpLeads.setTextColor(resources.getColor(R.color.white))
        btnFollowUpLeads.setBackgroundResource(R.drawable.rounded_bg_for_btn)

        btnAllLeads.setTextColor(resources.getColor(R.color.black))
        btnAllLeads.setBackgroundResource(R.drawable.rounded_bg_for_btn_not_selected)

    }

    private fun makePrivateLeadsActive() {
        tvWebLeadBottomColor.visibility = View.GONE
        privateLeadBottomColor.visibility = View.VISIBLE
        isLoadmore = false
        leadType = "private"
    }

    private fun makeWebLeadsActive() {
        tvWebLeadBottomColor.visibility = View.VISIBLE
        privateLeadBottomColor.visibility = View.GONE
        isLoadmore = false
        leadType = "webleads"

    }

    var webLeadsAccessToken = ""

    private fun getWebLeadToken() {

        lmsRetroInterface.getFromWeb(AccessTokenRequest(), URL_END_RDM_LEAD_ACCESS_TOKEN)
                .enqueue(object : Callback<Any> {
                    override fun onResponse(call: Call<Any>, response: Response<Any>) {

                        try {
                            val strRes = Gson().toJson(response.body())
                            val resModel = Gson().fromJson(strRes, AccessTokenResponse::class.java)

                            Log.i(TAG, "onResponse: " + resModel.accessToken)

                            webLeadsAccessToken = resModel.accessToken

                            handleRequest()
                        } catch (e: Exception) {
                            Log.e(TAG, "onResponse " + e.message)
                        }
                    }

                    override fun onFailure(call: Call<Any>, t: Throwable) {

                    }

                })

    }

    class AccessTokenRequest {

        @SerializedName("username")
        var username = "mfcwapp@lms.com"

        @SerializedName("password")
        var password = "p@ssword"

        @SerializedName("tag")
        var tag = "android"

    }

    class AccessTokenResponse {

        @SerializedName("status")
        var status = ""

        @SerializedName("status_code")
        var statusCode = ""

        @SerializedName("access_token")
        var accessToken = ""

        @SerializedName("expires_at")
        var expiryAt = ""
    }

    private fun handleRequest() {
        clearRecyclerview()
        page = 1
        page_no = "1"
        if (leadType == "webleads") {
            if (innerType == "all") {
                prepareWebLeadRequest(webLeadsAccessToken)
            } else {
                preparefollowupWebLeadRequest(webLeadsAccessToken)
            }
        } else {
            if (innerType == "all") {
                preparePrivateLeadRequest()
            } else {
                preparefollowupPrivateLeadRequest()
            }
        }
    }

    private fun clearRecyclerview() {
        recyclerView.adapter = null
        recyclerView.recycledViewPool.clear()
        mFollowupPrivatelist = mutableListOf()
        mFollowuplist = mutableListOf()
        mLeadsDatumList = mutableListOf()
        mLeadsPrivateList = mutableListOf()
        mPrivateLeadsAdapter?.notifyDataSetChanged()
        leadDetailsAdapter?.notifyDataSetChanged()
    }

    fun loadMoreItems() {
        page += 1
        page_no = page.toString()
        isLoadmore = true
        if (leadType == "webleads") {
            if (innerType == "all") {
                prepareWebLeadRequest(webLeadsAccessToken)
            } else {
                preparefollowupWebLeadRequest(webLeadsAccessToken)
            }
        } else {
            if (innerType == "all") {
                preparePrivateLeadRequest()
            } else {
                preparefollowupPrivateLeadRequest()
            }
        }
    }

    private fun prepareWhereNotInList(): List<WhereNotIn> {
        val list = WhereNotIn()
        list.column = "dispatched.status"
        if (web_closed_lead_tag == "web_leads_no_closed") {
            list.value = listOf("sold",
                    "bought",
                    "lost",
                    "notInterested",
                    "finalised")

        } else {
            list.value = listOf("sold",
                    "bought",
                    "lost",
                    "closed",
                    "notInterested",
                    "finalised")

        }
        return listOf(list)
    }

    private fun prepareWebLeadRequest(token: String) {

        mWhereorList = ArrayList()
        mWebLeadsWherenotin = listOf()
        mWebLeadsCustomWheres = ArrayList()

        val webLeadsModelRequest = WebLeadsModelRequest()
/*
        webLeadsModelRequest.filterByFields = ""
*/
        webLeadsModelRequest.orderBy = mOrderBy
        webLeadsModelRequest.orderByReverse = mOrderByReverse.toString()

        webLeadsModelRequest.perPage = pageItem
        webLeadsModelRequest.page = Integer.parseInt(page_no)
        /* webLeadsModelRequest.perPage = pageItem.toString()
         webLeadsModelRequest.page = page_no*/
        webLeadsModelRequest.accessToken = token

        //Search input Values
        searchbyWebLeads(mWhereorList)

/*
        webLeadsModelRequest.whereOr = mWhereorList
*/
        webLeadsModelRequest.aliasFields = "leads.created_at as PostedDate,leads.created_at as LeadDate,leads.id as id,dispatched.id as dispatchid,leads.customer_name as Name,leads.customer_mobile as Mobile,leads.customer_email as Email,leads.primary_make,leads.primary_model,leads.primary_variant,leads.secondary_make,leads.secondary_model,leads.secondary_variant,leads.register_no as regno,leads.register_month as regmonth,leads.register_year as regyear,leads.color,leads.kms,mfg_year,leads.owner as owners,leads.register_city,leads.stock_listed_price as price,dispatched.status,dispatched.followup_comments as remark,dispatched.followup_date as FollowUpDate,dispatched.employee_name as ExecutiveName,leads.lead_type"

        //sortbyWebLeads(webLeadsModelRequest)
        webLeadsModelRequest.wherenotin = prepareWhereNotInList()
/*
        webLeadsModelRequest.reportPrefix = "Follow up Sales Lead Report"
*/
        webLeadsModelRequest.tag = "android"

        //new changes
        val webLeadsCustomWhere = WebLeadsCustomWhere()
        setCustomWhereWebLeads(webLeadsCustomWhere)

        //Update Lead Status
        val webleadsWhereIn = WebleadsWhereIn()
        //setLeadStatus(webleadsWhereIn)
        webLeadsModelRequest.wherenotin = prepareWhereNotInList()
        webLeadsModelRequest.whereIn = mWherein

        if (isAllLeadsOnClick)
            webLeadsModelRequest.customWhere = getWebLeadWhereList() // All Leads
        else
            webLeadsModelRequest.customWhere = webLeadsCustomWheresList

        context?.let { sendWebLeadDetails(it, webLeadsModelRequest) }
    }

    private fun getWebLeadWhereList(): MutableList<WebLeadsCustomWhere>? {
        val list = mutableListOf<WebLeadsCustomWhere>()

        when {
            filterCategory.equals(AppConstants.POSTED_DATE) -> {
                val leadType1 = WebLeadsCustomWhere()
                leadType1.column = "leads.lead_type"
                leadType1.operator = "="
                leadType1.value = "USEDCARSALES"


                val targetId = WebLeadsCustomWhere()
                targetId.column = "dispatched.target_id"
                targetId.operator = "="
                targetId.value = CommonMethods.getstringvaluefromkey(activity, "dealer_code")

                val from = WebLeadsCustomWhere()
                from.column = "leads.created_at"
                from.operator = ">="
                from.value = fromDate + " 00:00:00"

                val to = WebLeadsCustomWhere()
                to.column = "leads.created_at"
                to.operator = "<="
                to.value = toDate + " 23:59:59"
                list.add(leadType1)
                list.add(targetId)
                list.add(from)
                list.add(to)

            }
            filterCategory.equals(AppConstants.FOLLOW_DATE) -> {
                val leadType1 = WebLeadsCustomWhere()
                leadType1.column = "leads.lead_type"
                leadType1.operator = "="
                leadType1.value = "USEDCARSALES"


                val targetId = WebLeadsCustomWhere()
                targetId.column = "dispatched.target_id"
                targetId.operator = "="
                targetId.value = CommonMethods.getstringvaluefromkey(activity, "dealer_code")

                val from = WebLeadsCustomWhere()
                from.column = "dispatched.followup_date"
                from.operator = ">="
                from.value = fromDate + " 00:00:00"

                val to = WebLeadsCustomWhere()
                to.column = "dispatched.followup_date"
                to.operator = "<="
                to.value = toDate + " 23:59:59"
                list.add(leadType1)
                list.add(targetId)
                list.add(from)
                list.add(to)

            }
            filterCategory.isEmpty()-> {
                val leadType = WebLeadsCustomWhere()
                leadType.column = "leads.lead_type"
                leadType.operator = "="
                leadType.value = "USEDCARSALES"

                val targetId = WebLeadsCustomWhere()
                targetId.column = "dispatched.target_id"
                targetId.operator = "="
                targetId.value = CommonMethods.getstringvaluefromkey(activity, "dealer_code")

                list.add(leadType)
                list.add(targetId)
            }
        }

        return list
    }

    private fun setCustomWhereWebLeads(webLeadsCustomWhere: WebLeadsCustomWhere) {

        var webLeadsCustomWhere = webLeadsCustomWhere
        webLeadsCustomWhere.column = "dispatched.target_id"
        webLeadsCustomWhere.operator = "="
        webLeadsCustomWhere.value = CommonMethods.getstringvaluefromkey(activity, "dealer_code")

        webLeadsCustomWhere = WebLeadsCustomWhere()
        webLeadsCustomWhere.column = "dispatched.target_source"
        webLeadsCustomWhere.operator = "="
        webLeadsCustomWhere.value = "MFC"

        webLeadsCustomWhere = WebLeadsCustomWhere()
        webLeadsCustomWhere.column = "leads.lead_type"
        webLeadsCustomWhere.operator = "="
        webLeadsCustomWhere.value = "USEDCARSALES"

    }

    private fun getRequestBody(param: String): WebLeadsCustomWhere {

        var webLeadsCustomWhere = WebLeadsCustomWhere()
        if (param == "lead_type") {
            webLeadsCustomWhere.column = "leads.lead_type"
            webLeadsCustomWhere.operator = "="
            webLeadsCustomWhere.value = "USEDCARSALES"
        } else if (param == "target_id") {
            webLeadsCustomWhere.column = "dispatched.target_id"
            webLeadsCustomWhere.operator = "="
            webLeadsCustomWhere.value = CommonMethods.getstringvaluefromkey(activity, "dealer_code")
        } else if (param == "target_source") {
            webLeadsCustomWhere.column = "dispatched.target_source"
            webLeadsCustomWhere.operator = "="
            webLeadsCustomWhere.value = "MFC"
        } else if (param == "follow_up_future") {
            webLeadsCustomWhere.column = mOrderBy
            webLeadsCustomWhere.operator = ">="
            webLeadsCustomWhere.value = CommonMethods.getTodayDate()
        }

        return webLeadsCustomWhere
    }

    private fun searchbyWebLeads(mWhereorList: MutableList<WebLeadsWhereor>) {
        if (searchQuery != "") {
            //Make
            val vehicle_make = WebLeadsWhereor()
            vehicle_make.column = "leads.primary_make"
            vehicle_make.operator = "like"
            vehicle_make.value = "%" + searchQuery.trim { it <= ' ' } + "%"
            mWhereorList.add(vehicle_make)

            //Model
            val vehicle_model = WebLeadsWhereor()
            vehicle_model.column = "leads.primary_model"
            vehicle_model.operator = "like"
            vehicle_model.value = "%" + searchQuery.trim { it <= ' ' } + "%"
            mWhereorList.add(vehicle_model)

            //Variant
            val vehicle_variant = WebLeadsWhereor()
            vehicle_variant.column = "leads.primary_variant"
            vehicle_variant.operator = "like"
            vehicle_variant.value = "%" + searchQuery.trim { it <= ' ' } + "%"
            mWhereorList.add(vehicle_variant)

            //Name
            val customer_name = WebLeadsWhereor()
            customer_name.column = "leads.customer_name"
            customer_name.operator = "like"
            customer_name.value = "%" + searchQuery.trim { it <= ' ' } + "%"
            mWhereorList.add(customer_name)

            //Mobile
            val customer_mobile = WebLeadsWhereor()
            customer_mobile.column = "leads.customer_mobile"
            customer_mobile.operator = "like"
            customer_mobile.value = "%" + searchQuery.trim { it <= ' ' } + "%"
            mWhereorList.add(customer_mobile)
        }
    }

    private fun setLeadStatus(privateLeadWhereIn: WebleadsWhereIn) {
        privateLeadWhereIn.column = "lead_type";
        privateLeadWhereIn.value = Collections.singletonList("Sales");
        mwhereinList?.plus(privateLeadWhereIn);

        val listStatus = ArrayList<String>();
        val dealercode = ArrayList<String>();
        val dealertype = ArrayList<String>();

        val leadstatusJsonArray = LeadFilterSaveInstance.getInstance().statusarray;

        // New ASM
        if (CommonMethods.getstringvaluefromkey(activity, "user_type") == "dealer") {
            if (leadstatusJsonArray.length() != 0) {
                privateLeadWhereIn.column = "lead_status";
                privateLeadWhereIn.value = listStatus;
                mwhereinList?.plus(privateLeadWhereIn);
            }
        } else {
            if (CommonMethods.getstringvaluefromkey(activity, "leadStatus") == "true") {
                //remove if already presents in json array
                if (CommonMethods.getstringvaluefromkey(activity, "Lvalue") != "") {
                    // remove
                    if (leadstatusJsonArray.length() != 0) {

                    }

                    listStatus.add(CommonMethods.getstringvaluefromkey(activity, "Lvalue"));
                    leadstatusJsonArray.put(CommonMethods.getstringvaluefromkey(activity, "Lvalue"));
                    privateLeadWhereIn.column = "lead_status";
                    privateLeadWhereIn.value = listStatus;
                    mwhereinList?.plus(privateLeadWhereIn);
                }

                val codeWhereIn = PrivateLeadWhereIn();
                val typeWhereIn = PrivateLeadWhereIn();
                dealertype.add("SALES");
                codeWhereIn.column = "lead_type";
                codeWhereIn.values = dealertype;
                mwhereinList?.plus(codeWhereIn);

                dealercode.add(CommonMethods.getstringvaluefromkey(activity, "dealer_code"));
                typeWhereIn.column = "dealer_id";
                typeWhereIn.values = dealercode;
                mwhereinList?.plus(typeWhereIn);
            } else {
                if (leadstatusJsonArray.length() != 0) {
                    privateLeadWhereIn.column = "lead_status";
                    privateLeadWhereIn.value = listStatus;
                    mwhereinList?.plus(privateLeadWhereIn);
                }

                //ASM
                val codeWhereIn = PrivateLeadWhereIn();
                val typeWhereIn = PrivateLeadWhereIn();

                dealertype.add("sales");
                codeWhereIn.column = "lead_type";
                codeWhereIn.values = dealertype;
                mwhereinList?.plus(codeWhereIn);

                dealercode.add(CommonMethods.getstringvaluefromkey(activity, "dealer_code"));
                typeWhereIn.column = "dealer_id";
                typeWhereIn.values = dealercode;
                mwhereinList?.plus(typeWhereIn);
            }
        }
    }

    private fun sendWebLeadDetails(mContext: Context, webLeadsModelRequest: WebLeadsModelRequest) {
        SpinnerManager.showSpinner(mContext)
        WebLeadService.postWebLead(webLeadsModelRequest, object : HttpCallResponse {
            override fun OnSuccess(obj: Any) {
                SpinnerManager.hideSpinner(mContext)

                val mRes = obj as Response<WebLeadsModelResponse>
                val mData = Gson().toJson(mRes.body())
                val str = Gson().fromJson(mData, WebLeadsModelResponse::class.java)
                if (str!!.status != "failure") {
                    val mlist = str.data
                    Log.i(TAG, "OnSuccess: " + mlist.size)
                    if (isLoadmore) {
                        mPrivateLeadsAdapter?.notifyItemInserted((mLeadsDatumList?.size ?: 0) - 1)
                        mLeadsDatumList?.addAll(mlist)
                        mFollowuplist = ArrayList()
                        mFollowuplist?.addAll(mlist)

                        mPrivateLeadsAdapter?.notifyDataSetChanged()
                        attachtoWebAdapter()

                    } else {

                        mLeadsDatumList = ArrayList()
                        mLeadsDatumList?.addAll(mlist)

                        mFollowuplist = ArrayList()
                        mFollowuplist?.addAll(mlist)

                        attachtoWebAdapter()
                    }

                } else {
                    WebServicesCall.error_popup2(activity, str.statusCode.toString().toInt(), "")
                }
            }

            override fun OnFailure(t: Throwable) {
                SpinnerManager.hideSpinner(mContext)
                t.printStackTrace()
            }
        })
    }

    private fun sendWebLeadFollowupDetails(mContext: Context, webLeadsModelRequest: WebLeadsModelRequest) {
        SpinnerManager.showSpinner(mContext)
        WebLeadService.postWebLead(webLeadsModelRequest, object : HttpCallResponse {
            override fun OnSuccess(obj: Any) {
                SpinnerManager.hideSpinner(mContext)
                val mRes = obj as Response<WebLeadsModelResponse>
                val mData = Gson().toJson(mRes.body())
                val str = Gson().fromJson(mData, WebLeadsModelResponse::class.java)
                if (str!!.status != "failure") {
                    val mlist = str.data
                    Log.i(TAG, "OnSuccess: " + mlist.size)
                    var mWebLeadsDatum: WebLeadsDatum? = null
                    for (i in mlist.indices) {
                        mWebLeadsDatum = mlist[i]
                        if (mWebLeadsDatum.status != null && !mWebLeadsDatum.status.equals("Sold", ignoreCase = true) &&
                                !mWebLeadsDatum.status.equals("Lost", ignoreCase = true) &&
                                !mWebLeadsDatum.status.equals("Closed", ignoreCase = true) && !mWebLeadsDatum.status.equals("Not-Interested", ignoreCase = true)
                                && !mWebLeadsDatum.status.equals("Finalised", ignoreCase = true)) {
                            mFollowuplist?.add(mWebLeadsDatum)
                        }
                    }
                    if (isLoadmore) {
                        mLeadsDatumList?.size?.minus(1)?.let { mPrivateLeadsAdapter?.notifyItemInserted(it) }
                        if (mFollowuplist != null) {
                            mLeadsDatumList?.addAll(mFollowuplist!!)
                        }
                        mPrivateLeadsAdapter?.notifyDataSetChanged()
                        attachtoWebAdapter()

                    } else {
                        mLeadsDatumList = ArrayList<WebLeadsDatum>()
                        if (mFollowuplist != null) {
                            mLeadsDatumList?.addAll(mFollowuplist!!)
                        }
                        attachtoWebAdapter()
                    }
                } else {
                    WebServicesCall.error_popup2(activity, str.statusCode.toString().toInt(), "")
                }
            }

            override fun OnFailure(t: Throwable) {
                SpinnerManager.hideSpinner(mContext)
                t.printStackTrace()
            }
        })
    }

    private fun attachtoWebAdapter() {
        if (mFollowuplist?.size == 0) {
            Toast.makeText(context, "No Leads found", Toast.LENGTH_LONG).show()
        }
        leadDetailsAdapter = activity?.let { LeadDetailsAdapter(it, mFollowuplist) }
        recyclerView.setHasFixedSize(false)
        val layoutManger = LinearLayoutManager(activity)
        recyclerView.layoutManager = layoutManger
        recyclerView.adapter = leadDetailsAdapter
    }

    private fun attachtoPrivateAdapter() {
        if (mFollowupPrivatelist?.size == 0) {
            Toast.makeText(context, "No Leads found", Toast.LENGTH_LONG).show()
        }
        mPrivateLeadsAdapter = activity?.let { PrivateLeadAdapter(it, mFollowupPrivatelist) }
        recyclerView.setHasFixedSize(false)
        val layoutManger = LinearLayoutManager(activity)
        recyclerView.layoutManager = layoutManger
        recyclerView.adapter = mPrivateLeadsAdapter
    }


    private fun preparefollowupWebLeadRequest(token: String) {
        mWhereorList = ArrayList()

        mWebLeadsWherenotin = listOf()
        mWebLeadsCustomWheres = ArrayList()
        val webLeadsModelRequest = WebLeadsModelRequest()
/*
        webLeadsModelRequest.filterByFields = ""
*/
        webLeadsModelRequest.perPage = pageItem
        webLeadsModelRequest.page = Integer.parseInt(page_no)
        webLeadsModelRequest.accessToken = token

        //Search input Values
        searchbyWebLeads(mWhereorList)
/*
        webLeadsModelRequest.whereOr = mWhereorList
*/
        webLeadsModelRequest.orderBy = mOrderBy
        webLeadsModelRequest.orderByReverse = mOrderByReverse.toString()
        webLeadsModelRequest.aliasFields = "leads.created_at as PostedDate,leads.created_at as LeadDate,leads.id as id,dispatched.id as dispatchid,leads.customer_name as Name,leads.customer_mobile as Mobile,leads.customer_email as Email,leads.primary_make,leads.primary_model,leads.primary_variant,leads.secondary_make,leads.secondary_model,leads.secondary_variant,leads.register_no as regno,leads.register_month as regmonth,leads.register_year as regyear,leads.color,leads.kms,mfg_year,leads.owner as owners,leads.register_city,leads.stock_listed_price as price,dispatched.status,dispatched.followup_comments as remark,dispatched.followup_date as FollowUpDate,dispatched.employee_name as ExecutiveName,leads.lead_type"
        webLeadsModelRequest.wherenotin = prepareWhereNotInList()
/*
        webLeadsModelRequest.reportPrefix = "Follow up Sales Lead Report"
*/
        webLeadsModelRequest.tag = "android"

        /*  //new changes
          val webLeadsCustomWhere = WebLeadsCustomWhere()
          setCustomWhereWebLeads(webLeadsCustomWhere)
  */
        //Posted Followup Date Filter
        // setPostedFollowupDateInfo()

        if (isFollowUpLeadOnClick)
            webLeadsModelRequest.customWhere = getWebFollowUpLeadsWhere()
        else
            webLeadsModelRequest.customWhere = webLeadsCustomWheresList

        //Update Lead Status
        val webleadsWhereIn = WebleadsWhereIn()
        //setLeadStatus(webleadsWhereIn)
        webLeadsModelRequest.wherenotin = prepareWhereNotInList()
        webLeadsModelRequest.whereIn = mWherein

        context?.let { sendWebLeadFollowupDetails(it, webLeadsModelRequest) }
    }

    private fun getWebFollowUpLeadsWhere(): MutableList<WebLeadsCustomWhere>? {

        val list = mutableListOf<WebLeadsCustomWhere>()
        val leadType = WebLeadsCustomWhere()
        leadType.column = "leads.lead_type"
        leadType.operator = "="
        leadType.value = "USEDCARSALES"

        val targetId = WebLeadsCustomWhere()
        targetId.column = "dispatched.target_id"
        targetId.operator = "="
        targetId.value = CommonMethods.getstringvaluefromkey(activity, "dealer_code")
        list.add(leadType)
        list.add(targetId)


        when {
            filterCategory.equals(AppConstants.POSTED_DATE) -> {
                val leadType1 = WebLeadsCustomWhere()
                leadType1.column = "leads.lead_type"
                leadType1.operator = "="
                leadType1.value = "USEDCARSALES"


                val targetId = WebLeadsCustomWhere()
                targetId.column = "dispatched.target_id"
                targetId.operator = "="
                targetId.value = CommonMethods.getstringvaluefromkey(activity, "dealer_code")

                val from = WebLeadsCustomWhere()
                from.column = "leads.created_at"
                from.operator = ">="
                from.value = fromDate + " 00:00:00"

                val to = WebLeadsCustomWhere()
                to.column = "leads.created_at"
                to.operator = "<="
                to.value = toDate + " 23:59:59"
                list.add(leadType1)
                list.add(targetId)
                list.add(from)
                list.add(to)

            }
            filterCategory.equals(AppConstants.FOLLOW_DATE) -> {
                val leadType1 = WebLeadsCustomWhere()
                leadType1.column = "leads.lead_type"
                leadType1.operator = "="
                leadType1.value = "USEDCARSALES"


                val targetId = WebLeadsCustomWhere()
                targetId.column = "dispatched.target_id"
                targetId.operator = "="
                targetId.value = CommonMethods.getstringvaluefromkey(activity, "dealer_code")

                val from = WebLeadsCustomWhere()
                from.column = "dispatched.followup_date"
                from.operator = ">="
                from.value = fromDate + " 00:00:00"

                val to = WebLeadsCustomWhere()
                to.column = "dispatched.followup_date"
                to.operator = "<="
                to.value = toDate + " 23:59:59"
                list.add(leadType1)
                list.add(targetId)
                list.add(from)
                list.add(to)

            }
            else -> {
                val followupDate = WebLeadsCustomWhere()
                followupDate.column = "dispatched.followup_date"
                followupDate.operator = "<="
                followupDate.value = CommonMethods.getTodayDate()
                list.add(followupDate)

            }
        }



        return list

    }

    private fun preparePrivateLeadRequest() {

        val mwhereorList = ArrayList<PrivateLeadsWhereor>()


        //new changes

        val privateLeadsModelRequest = PrivateLeadsModelRequest()
        privateLeadsModelRequest.orderBy = walkInOrderBy
        privateLeadsModelRequest.orderByReverse = walkInOrderReverse.toString()

        privateLeadsModelRequest.pageItems = "100"
        privateLeadsModelRequest.page = page_no

        privateLeadsModelRequest.setWhereOr(mwhereorList)

        if (isAllLeadsOnClick && filterCategory == "" && leadStatusFilter.isEmpty()) {
            privateLeadsModelRequest.wherein = getWalkInWhereIn("")
            privateLeadsModelRequest.where = getWalkInWhereList()
        } else if (isAllLeadsOnClick && filterCategory == "" && leadStatusFilter.isNotEmpty()) {
            privateLeadsModelRequest.where = getWalkInWhereList()
            privateLeadsModelRequest.wherein = mwhereinList
        } else {
            privateLeadsModelRequest.where = mwhereList
            privateLeadsModelRequest.wherein = mwhereinList
        }

        /* if (CommonMethods.getstringvaluefromkey(MainActivity.activity, "user_type").equals(GlobalText.USER_TYPE_SALES, ignoreCase = true) ||
                 CommonMethods.getstringvaluefromkey(MainActivity.activity, "user_type").equals(GlobalText.USER_TYPE_PROCUREMENT, ignoreCase = true)) {
             val where = PrivateLeadWhere()
             where.column = "executive_id"
             where.operator = "="
             where.value = CommonMethods.getstringvaluefromkey(MainActivity.activity, "user_id")
             mWhereListAllPrivateLeads?.add(where)
             privateLeadsModelRequest.where = mWhereListAllPrivateLeads
         }*/
        activity?.let { sendPrivateLeadDetails(it, privateLeadsModelRequest) }
    }

    private fun getWalkInWhereIn(tag: String): MutableList<PrivateLeadWhereIn>? {
        val list = mutableListOf<PrivateLeadWhereIn>()
        val whereIn = PrivateLeadWhereIn()
        whereIn.column = "dealer_id"
        val valuesList = ArrayList<String>()
        valuesList.add(CommonMethods.getstringvaluefromkey(activity, "dealer_code"))
        whereIn.values = valuesList
        list.add(whereIn)
        if (tag.equals("LeadStatus")) {
            val leadStatusF = PrivateLeadWhereIn()
            leadStatusF.column = "dealer_id"
            leadStatusF.values = leadStatusFilter
            list.add(leadStatusF)

        }
        return list

    }

    private fun getWalkInWhereList(): MutableList<PrivateLeadWhere>? {

        val list = mutableListOf<PrivateLeadWhere>()
        val leadType = PrivateLeadWhere()
        leadType.column = "lead_type"
        leadType.operator = "="
        leadType.value = "sales"

        val leadTag = PrivateLeadWhere()
        leadTag.column = "lead_tags"
        leadTag.operator = "="
        leadTag.value = "usedcar"

        list.add(leadType)
        list.add(leadTag)

        if (filterCategory.equals(AppConstants.POSTED_DATE)) {
            val from = PrivateLeadWhere()
            from.column = "lead_date2"
            from.operator = ">="
            from.value = fromDate

            val to = PrivateLeadWhere()
            to.column = "lead_date2"
            to.operator = "<="
            to.value = fromDate

            list.add(from)
            list.add(to)

        } else if (filterCategory.equals(AppConstants.POSTED_DATE)) {
            val from = PrivateLeadWhere()
            from.column = "follow_date"
            from.operator = ">="
            from.value = fromDate

            val to = PrivateLeadWhere()
            to.column = "follow_date"
            to.operator = "<="
            to.value = fromDate

            list.add(from)
            list.add(to)

        }

        return list

    }


    private fun sendPrivateLeadDetails(mActivity: Activity, privateLeadsModelRequest: PrivateLeadsModelRequest) {
        SpinnerManager.showSpinner(mActivity)
        RetroBase.mfcRetroInterface.postRequestWeb(privateLeadsModelRequest, "v3/api/dealer/private-leads/getlead", CommonMethods.getToken("access_token_from_header")).enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                SpinnerManager.hideSpinner(mActivity)

                val strRes = Gson().toJson(response.body())
                val resModel = Gson().fromJson(strRes, PrivateLeadsModelResponse::class.java)

                val mlist = resModel?.data
                mlist?.let {
                    if (isLoadmore) {
                        mPrivateLeadsAdapter?.notifyItemInserted(mLeadsDatumList!!.size - 1)
                        mFollowupPrivatelist?.addAll(mlist)
                        mPrivateLeadsAdapter?.notifyDataSetChanged()
                        attachtoPrivateAdapter()
                    } else {
                        mLeadsPrivateList = mutableListOf()
                        mFollowupPrivatelist?.addAll(mlist)
                        attachtoPrivateAdapter()
                    }
                }

            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(mActivity)
                t.printStackTrace()
            }

        })
    }


    private fun preparefollowupPrivateLeadRequest() {
        mFollowupPrivatelist = ArrayList<PrivateLeadDatum>()
        val mwhereList = mwhereList
        val mwhereinPrivateList = mwhereinList
        val mwhereorList = ArrayList<PrivateLeadsWhereor>()
        val privateLeadsModelRequest = PrivateLeadsModelRequest()
        privateLeadsModelRequest.orderBy = walkInOrderBy
        privateLeadsModelRequest.orderByReverse = walkInOrderReverse.toString()
        privateLeadsModelRequest.pageItems = "100"
        privateLeadsModelRequest.page = page_no




        if (isFollowUpLeadOnClick && filterCategory == "" && leadStatusFilter.isEmpty()) {
            privateLeadsModelRequest.where = getWalkInFollowUpWhere()
            privateLeadsModelRequest.wherein = getWalkInWhereIn("")
        } else if (isFollowUpLeadOnClick && filterCategory == "" && leadStatusFilter.isNotEmpty()) {
            privateLeadsModelRequest.where = getWalkInFollowUpWhere()
            privateLeadsModelRequest.wherein = mwhereinPrivateList
        } else {
            privateLeadsModelRequest.where = mwhereList
            privateLeadsModelRequest.wherein = mwhereinPrivateList

        }
        privateLeadsModelRequest.whereOr = mwhereorList

        /* val usertype = CommonMethods.getstringvaluefromkey(MainActivity.activity, "user_type")
         if (usertype.equals(GlobalText.USER_TYPE_SALES, ignoreCase = true) ||
                 usertype.equals(GlobalText.USER_TYPE_PROCUREMENT, ignoreCase = true)) {
             val where2 = PrivateLeadWhere()
             where2.column = "executive_id"
             where2.operator = "="
             where2.value = CommonMethods.getstringvaluefromkey(MainActivity.activity, "user_id")
             //mwhereList.add(where2)
         }*/
        activity?.let { sendfollowupPrivateLeadDetails(it, privateLeadsModelRequest) }
    }

    private fun getWalkInFollowUpWhere(): MutableList<PrivateLeadWhere>? {
        val list = mutableListOf<PrivateLeadWhere>()
        val leadType = PrivateLeadWhere()
        leadType.column = "lead_type"
        leadType.operator = "="
        leadType.value = "sales"

        val leadTag = PrivateLeadWhere()
        leadTag.column = "lead_tags"
        leadTag.operator = "="
        leadTag.value = "usedcar"

        list.add(leadType)
        list.add(leadTag)

        if (filterCategory.equals(AppConstants.POSTED_DATE)) {
            val from = PrivateLeadWhere()
            from.column = "lead_date2"
            from.operator = ">="
            from.value = fromDate

            val to = PrivateLeadWhere()
            to.column = "lead_date2"
            to.operator = "<="
            to.value = fromDate

            list.add(from)
            list.add(to)

        } else if (filterCategory.equals(AppConstants.FOLLOW_DATE)) {
            val from = PrivateLeadWhere()
            from.column = "follow_date"
            from.operator = ">="
            from.value = fromDate

            val to = PrivateLeadWhere()
            to.column = "follow_date"
            to.operator = "<="
            to.value = fromDate

            list.add(from)
            list.add(to)
        } else {
            val followDate = PrivateLeadWhere()
            followDate.column = "follow_date"
            followDate.operator = "<="
            followDate.value = CommonMethods.getTodayDate()

            list.add(followDate)
        }

        return list
    }


    private fun sendfollowupPrivateLeadDetails(mActivity: Activity, privateLeadsModelRequest: PrivateLeadsModelRequest) {
        SpinnerManager.showSpinner(mActivity)
        PrivateLeadService.postPrivateLead(privateLeadsModelRequest, object : HttpCallResponse {
            override fun OnSuccess(obj: Any) {
                SpinnerManager.hideSpinner(mActivity)
                val mRes = obj as Response<PrivateLeadsModelResponse>
                val mData = Gson().toJson(mRes.body())
                val str = Gson().fromJson(mData, PrivateLeadsModelResponse::class.java)
                val mlist = str.data
                mFollowupPrivatelist = mlist
                if (isLoadmore) {
                    mPrivateLeadsAdapter?.notifyItemInserted(mLeadsDatumList!!.size - 1)
                    mLeadsDatumList!!.addAll(mFollowuplist!!)
                    mPrivateLeadsAdapter?.notifyDataSetChanged()
                    attachtoPrivateAdapter()

                } else {
                    mLeadsPrivateList = mutableListOf()
                    attachtoPrivateAdapter()
                }
            }

            override fun OnFailure(t: Throwable) {
                SpinnerManager.hideSpinner(mActivity)
                t.printStackTrace()
            }
        })
    }

    private fun setPostedFollowupDateInfo() {
        val postedfollDateHashMap = LeadFilterSaveInstance.getInstance().savedatahashmap
        var postdateinfo: String? = ""

        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_TODAY) != null) {
            postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_TODAY);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_YESTER) != null) {
            postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_YESTER);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_7DAYS) != null) {
            postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_7DAYS);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_15DAYS) != null) {
            postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_15DAYS);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE) != null) {
            postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE);
        }

        val postDateCustomWhereStart = PrivateLeadWhere();
        val postDateCustomWhereEnd = PrivateLeadWhere();
        if (postdateinfo != null && postdateinfo.contains("@")) {
            //split the date and atach to query
            val createddates = postdateinfo.split("@")
            try {

                if (CommonMethods.getstringvaluefromkey(activity, "user_type").equals("dealer")) {
                    if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE) != null) {

                        postDateCustomWhereStart.setColumn("created_at");
                        postDateCustomWhereEnd.setColumn("created_at");

                        postDateCustomWhereStart.setOperator(">=");
                        postDateCustomWhereEnd.setOperator("<=");

                        postDateCustomWhereStart.setValue(createddates[0]);
                        postDateCustomWhereEnd.setValue(createddates[1]);

                        mwhereList?.add(postDateCustomWhereStart);
                        mwhereList?.add(postDateCustomWhereEnd);

                    } else {
                        postDateCustomWhereStart.setColumn("created_at");
                        postDateCustomWhereEnd.setColumn("created_at");

                        postDateCustomWhereStart.setOperator(">=");
                        postDateCustomWhereEnd.setOperator("<");

                        postDateCustomWhereStart.setValue(createddates[0]);
                        postDateCustomWhereEnd.setValue(createddates[1]);

                        mwhereList?.add(postDateCustomWhereStart);
                        mwhereList?.add(postDateCustomWhereEnd);
                    }
                } else {
                    if (CommonMethods.getstringvaluefromkey(activity, "leadStatus").equals("true")) {
                        try {
                            // remove
                            if (!CommonMethods.getstringvaluefromkey(activity, "leads_status")
                                            .equals("", ignoreCase = true)) {
                            } else {
                                LeadFilterSaveInstance.getInstance().setSavedatahashmap(HashMap<String, String>());
                            }
                        } catch (e: Exception) {
                        }


                        postDateCustomWhereStart.setColumn("created_at");
                        postDateCustomWhereEnd.setColumn("created_at");

                        val startDate = CommonMethods.getstringvaluefromkey(activity, "startDate");
                        val endDate = CommonMethods.getstringvaluefromkey(activity, "endDate");

                        if (!startDate.equals("", ignoreCase = true)) {
                            postDateCustomWhereStart.setOperator(">=");
                            postDateCustomWhereEnd.setOperator("<=");

                            postDateCustomWhereStart.setValue(startDate);
                            postDateCustomWhereEnd.setValue(endDate);

                            mwhereList?.add(postDateCustomWhereStart);
                            mwhereList?.add(postDateCustomWhereEnd);
                        }
                    } else {
                        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE) != null) {
                            postDateCustomWhereStart.setColumn("created_at");
                            postDateCustomWhereEnd.setColumn("created_at");

                            postDateCustomWhereStart.setOperator(">=");
                            postDateCustomWhereEnd.setOperator("<=");

                            postDateCustomWhereStart.setValue(createddates[0]);
                            postDateCustomWhereEnd.setValue(createddates[1]);

                            mwhereList?.add(postDateCustomWhereStart);
                            mwhereList?.add(postDateCustomWhereEnd);

                        } else {

                            postDateCustomWhereStart.setColumn("created_at");
                            postDateCustomWhereEnd.setColumn("created_at");

                            postDateCustomWhereStart.setOperator(">=");
                            postDateCustomWhereEnd.setOperator("<");

                            postDateCustomWhereStart.setValue(createddates[0]);
                            postDateCustomWhereEnd.setValue(createddates[1]);

                            mwhereList?.add(postDateCustomWhereStart);
                            mwhereList?.add(postDateCustomWhereEnd);
                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace();
            }
        } else {
            if (!CommonMethods.getstringvaluefromkey(activity, "user_type").equals("dealer", ignoreCase = true)) {

                postDateCustomWhereStart.setColumn("created_at");
                postDateCustomWhereEnd.column = "created_at";

                val startDate = CommonMethods.getstringvaluefromkey(activity, "startDate");
                val endDate = CommonMethods.getstringvaluefromkey(activity, "endDate");

                if (!startDate.equals("", ignoreCase = true)) {
                    postDateCustomWhereStart.setOperator(">=");
                    postDateCustomWhereEnd.setOperator("<=");

                    postDateCustomWhereStart.setValue(startDate);
                    postDateCustomWhereEnd.setValue(endDate);

                    mwhereList?.add(postDateCustomWhereStart);
                    mwhereList?.add(postDateCustomWhereEnd);
                }
            }
        }

        var followdateinfo = "";

        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TMRW) != null) {
            followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TMRW)!!;
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TODAY) != null) {
            followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TODAY)!!;
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_YESTER) != null) {
            followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_YESTER)!!;
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_7DAYS) != null) {
            followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_7DAYS)!!;
        }
        if (postedfollDateHashMap[LeadConstantsSection.LEAD_FOLUP_CUSTDATE] != null) {
            followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_CUSTDATE)!!;
        }

        val followDateCustomWhereStart = PrivateLeadWhere();
        val followDateCustomWhereEnd = PrivateLeadWhere();
        if (followdateinfo.contains("@")) {

            val follow = followdateinfo.split("@");
            try {
                followDateCustomWhereStart.setColumn("follow_date");
                followDateCustomWhereEnd.setColumn("follow_date");

                if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TMRW) != null) {
                    followDateCustomWhereStart.setOperator(">");
                    followDateCustomWhereEnd.setOperator("<=");
                    followDateCustomWhereStart.setValue(follow[1]);
                    followDateCustomWhereEnd.setValue(follow[0]);

                }
                if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TODAY) != null || postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_YESTER) != null
                        || postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_7DAYS) != null) {
                    followDateCustomWhereStart.setOperator(">=");
                    followDateCustomWhereEnd.setOperator("<");
                    followDateCustomWhereStart.setValue(follow[0]);
                    followDateCustomWhereEnd.setValue(follow[1]);
                }
                if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_CUSTDATE) != null) {
                    followDateCustomWhereStart.setOperator(">=");
                    followDateCustomWhereEnd.setOperator("<=");
                    followDateCustomWhereStart.setValue(follow[0]);
                    followDateCustomWhereEnd.setValue(follow[1]);
                }
                mwhereList?.add(followDateCustomWhereStart);
                mwhereList?.add(followDateCustomWhereEnd);
            } catch (e: Exception) {
                e.printStackTrace();
            }
        } else {
            if (CommonMethods.getstringvaluefromkey(activity, "leadStatus").equals("true")
                    && !CommonMethods.getstringvaluefromkey(activity, "value2").equals("")
                    && !CommonMethods.getstringvaluefromkey(activity, "messageCenter").equals("true")) {

                followDateCustomWhereStart.setColumn("follow_date");
                followDateCustomWhereEnd.setColumn("follow_date");

                followDateCustomWhereStart.setOperator(CommonMethods.getstringvaluefromkey(getActivity(), "operator"));
                followDateCustomWhereEnd.setOperator(CommonMethods.getstringvaluefromkey(getActivity(), "operator2"));

                followDateCustomWhereStart.setValue(CommonMethods.getstringvaluefromkey(getActivity(), "value"));
                followDateCustomWhereEnd.setValue(CommonMethods.getstringvaluefromkey(getActivity(), "value2"));

                mwhereList?.add(followDateCustomWhereStart);
                mwhereList?.add(followDateCustomWhereEnd);
            }
        }
    }

    override fun showToast(message: String?) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
    }

    override fun sortLeadBy(selectedSort: String?) {

        sortLeadBy = selectedSort!!
        when {
            sortLeadBy.equals("") -> {
                mOrderBy = "leads.created_at"
                mOrderByReverse = false
                walkInOrderBy = "lead_date2"
                walkInOrderReverse = false
                ivASMLeadsSortBy.setBackgroundResource(R.drawable.ic_new_sort_by)
            }
            sortLeadBy.equals(AppConstants.POSTED_OTL) -> {
                mOrderBy = "leads.created_at"
                mOrderByReverse = false
                walkInOrderBy = "lead_date2"
                walkInOrderReverse = false
                ivASMLeadsSortBy.setBackgroundResource(R.drawable.ic_sort_enabled)
            }
            sortLeadBy.equals(AppConstants.POSTED_LTO) -> {
                mOrderBy = "leads.created_at"
                mOrderByReverse = true
                walkInOrderBy = "lead_date2"
                walkInOrderReverse = true
                ivASMLeadsSortBy.setBackgroundResource(R.drawable.ic_sort_enabled)
            }
            sortLeadBy.equals(AppConstants.FOLLOW_OTL) -> {
                mOrderBy = "dispatched.followup_date"
                mOrderByReverse = false
                walkInOrderBy = "follow_date"
                walkInOrderReverse = false
                ivASMLeadsSortBy.setBackgroundResource(R.drawable.ic_sort_enabled)
            }
            sortLeadBy.equals(AppConstants.FOLLOW_LTO) -> {
                mOrderBy = "dispatched.followup_date"
                mOrderByReverse = true
                walkInOrderBy = "follow_date"
                walkInOrderReverse = true
                ivASMLeadsSortBy.setBackgroundResource(R.drawable.ic_sort_enabled)
            }
        }
        handleRequest()

    }

    protected fun onPostExecute(result: Void?) {
        if (isAdded) {
            val filterReceiver = object : BroadcastReceiver() {
                override fun onReceive(context: Context?, intent: Intent?) {
                    val bundle = intent?.extras
                    val message = bundle?.get("filterCategory")
                    filterCategory = message.toString()
                    fromDate = bundle?.get("fromDate").toString()
                    toDate = bundle?.get("toDate").toString()
                    leadStatusFilter = bundle?.getStringArrayList("LeadStatusFilter")!!
                    webLeadsCustomWheresList = null
                    webLeadsCustomWheresList = applyFilter()
                    handleRequest()
                }
            }

            LocalBroadcastManager.getInstance(requireContext()).registerReceiver(filterReceiver, IntentFilter("Filter_Tag"))
        }
    }


}
/*
|| s.primaryMake.contains(toString.toLowerCase()) ||
s.primaryModel.contains(toString.toLowerCase()) || s.primaryVariant.contains(toString.toLowerCase())*/
//        PrivateLeadService.postPrivateLead(privateLeadsModelRequest, object : HttpCallResponse {
//            override fun OnSuccess(obj: Any) {
//                SpinnerManager.hideSpinner(mActivity)
//                val mRes = obj as Response<PrivateLeadsModelResponse>
//                val mData = mRes.body()
//                val str = Gson().toJson(mData, PrivateLeadsModelResponse::class.java)
////                Log.i(TAG, "OnSuccess: $str")
////                negotiationLeadCount = mData!!.total
//                val mlist = mData?.data
////                Log.i(TAG, "OnSuccess: " + mlist.size)
//                if (isLoadmore) {
//                    mPrivateLeadsAdapter?.notifyItemInserted(mLeadsDatumList!!.size - 1)
//                    mFollowupPrivatelist?.addAll(mlist!!)
//                    mPrivateLeadsAdapter?.notifyDataSetChanged()
//                } else {
//                    mLeadsPrivateList = mutableListOf()
//                    mFollowupPrivatelist?.addAll(mlist!!)
//                    attachtoPrivateAdapter()
//                }
////                isLeadsAvailable(negotiationLeadCount)
//            }
//
//            override fun OnFailure(t: Throwable) {
//                SpinnerManager.hideSpinner(mActivity)
////                mSwipeRefreshLayout.setRefreshing(false)
//                t.printStackTrace()
//            }
//        })

//    private fun prepareWebLeadRequest() {
//        // webLeadsCustomWheresList = ArrayList()
//        mWherein = ArrayList()
//        mWhereorList = ArrayList()
//        mWebLeadsWherenotin = ArrayList()
//        mWebLeadsCustomWheres = ArrayList()
//        val webLeadsModelRequest = WebLeadsModelRequest()
//        webLeadsModelRequest.filterByFields = ""
//        webLeadsModelRequest.perPage = Integer.toString(pageItem)
//        webLeadsModelRequest.page = page_no
//        webLeadsModelRequest.accessToken = CommonMethods.getstringvaluefromkey(requireActivity(), "access_token")
//
//        //Search input Values
//        searchbyWebLeads(mWhereorList)
//        webLeadsModelRequest.whereOr = mWhereorList
//        webLeadsModelRequest.aliasFields = "leads.created_at as PostedDate,leads.created_at as LeadDate,leads.id as id,dispatched.id as dispatchid,leads.customer_name as Name,leads.customer_mobile as Mobile,leads.customer_email as Email,leads.primary_make,leads.primary_model,leads.primary_variant,leads.secondary_make,leads.secondary_model,leads.secondary_variant,leads.register_no as regno,leads.register_month as regmonth,leads.register_year as regyear,leads.color,leads.kms,mfg_year,leads.owner as owners,leads.register_city,leads.stock_listed_price as price,dispatched.status,dispatched.followup_comments as remark,dispatched.followup_date as FollowUpDate,dispatched.employee_name as ExecutiveName,leads.lead_type"
//        //sortbyWebLeads(webLeadsModelRequest)
//        webLeadsModelRequest.customWhere = mWebLeadsCustomWheres
//        webLeadsModelRequest.wherenotin = prepareWhereNotInList()
//        webLeadsModelRequest.reportPrefix = "Follow up Sales Lead Report"
//        webLeadsModelRequest.tag = "android"
//
//        //new changes
//        val webLeadsCustomWhere = WebLeadsCustomWhere()
//        setCustomWhereWebLeads(webLeadsCustomWhere)
//
//        //Posted Followup Date Filter
//        setPostedFollowupDateInfo()
//
//        //Update Lead Status
//        val webleadsWhereIn = WebleadsWhereIn()
//        setLeadStatus(webleadsWhereIn)
//        webLeadsModelRequest.whereIn = mWherein
//        if (webLeadsCustomWheresList != null)
//            webLeadsModelRequest.customWhere = webLeadsCustomWheresList
//        else
//            webLeadsModelRequest.customWhere = listOf()
//
//        context?.let { sendWebLeadDetails(it, webLeadsModelRequest) }
//    }
/* val usertype = CommonMethods.getstringvaluefromkey(MainActivity.activity, "user_type")
        if (usertype.equals(GlobalText.USER_TYPE_SALES, ignoreCase = true) ||
                usertype.equals(GlobalText.USER_TYPE_PROCUREMENT, ignoreCase = true)) {
            webLeadsCustomWhere = WebLeadsCustomWhere()
            webLeadsCustomWhere.column = "dispatched.employee_id"
            webLeadsCustomWhere.operator = "="
            webLeadsCustomWhere.value = CommonMethods.getstringvaluefromkey(activity, "user_id")
            webLeadsCustomWheresList?.add(webLeadsCustomWhere)
        }*/
/*
private fun setCustomWhereWebLeadsFollwup(webLeadsCustomWhere: WebLeadsCustomWhere) {
        //Add Dealer Id into DashboardCustomWhere
        var webLeadsCustomWhere = webLeadsCustomWhere
        webLeadsCustomWhere.column = "dispatched.target_id"
        webLeadsCustomWhere.operator = "="
        webLeadsCustomWhere.value = CommonMethods.getstringvaluefromkey(activity, "dealer_code")
        webLeadsCustomWheresList?.add(webLeadsCustomWhere)
        val d = Calendar.getInstance().time
        val df = SimpleDateFormat("yyyy-MM-dd")
        val formattedDate = df.format(d)
        webLeadsCustomWhere = WebLeadsCustomWhere()
        webLeadsCustomWhere.column = "dispatched.followup_date"
        webLeadsCustomWhere.operator = "<="
        webLeadsCustomWhere.value = "$formattedDate 23:59:59"
        webLeadsCustomWheresList?.add(webLeadsCustomWhere)

        //Add Target Source into DashboardCustomWhere
        webLeadsCustomWhere = WebLeadsCustomWhere()
        webLeadsCustomWhere.column = "dispatched.target_source"
        webLeadsCustomWhere.operator = "="
        webLeadsCustomWhere.value = "MFC"
        webLeadsCustomWheresList?.add(webLeadsCustomWhere)
        //End

        //Add USEDCARSALES
        webLeadsCustomWhere = WebLeadsCustomWhere()
        webLeadsCustomWhere.column = "leads.lead_type"
        webLeadsCustomWhere.operator = "="
        webLeadsCustomWhere.value = "USEDCARSALES"
        webLeadsCustomWheresList?.add(webLeadsCustomWhere)
        val usertype = CommonMethods.getstringvaluefromkey(activity, "user_type")
        if (usertype.equals(GlobalText.USER_TYPE_SALES, ignoreCase = true) ||
                usertype.equals(GlobalText.USER_TYPE_PROCUREMENT, ignoreCase = true)) {
            webLeadsCustomWhere = WebLeadsCustomWhere()
            webLeadsCustomWhere.column = "dispatched.employee_id"
            webLeadsCustomWhere.operator = "="
            webLeadsCustomWhere.value = CommonMethods.getstringvaluefromkey(activity, "user_id")
            webLeadsCustomWheresList?.add(webLeadsCustomWhere)
        }
    }
*/

