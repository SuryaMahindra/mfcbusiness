package remotedealer.ui.filter

import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.RelativeLayout
import androidx.annotation.NonNull
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.mfcwl.mfc_dealer.R
import com.mfcwl.mfc_dealer.SalesStocks.Instances.SalesCalendarFilterDialog
import kotlinx.android.synthetic.main.fragment_bottom_sheet_filter_web_leads.*
import kotlinx.android.synthetic.main.layout_web_lead_filter_by_date.view.*
import sidekicklpr.PreferenceManager

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 *
 */
class FilterWebLeadsBottomSheetFragment : BottomSheetDialogFragment() {

    private var param1: String? = null
    private var param2: String? = null
    private var fragmentView: View? = null
    lateinit var preferenceManager: PreferenceManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        preferenceManager = PreferenceManager(requireActivity())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_bottom_sheet_filter_web_leads, container, false)
        return fragmentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val rootLayout = fragmentView?.findViewById<RelativeLayout>(R.id.relLayMainContainerBottomSheet)
        val params = rootLayout?.layoutParams
        params?.height = getScreenHeight()
        rootLayout?.layoutParams = params

        val dialog = dialog as BottomSheetDialog
        val bottomSheet = dialog.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
        val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        bottomSheetBehavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(@NonNull bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                }
            }

            override fun onSlide(@NonNull bottomSheet: View, slideOffset: Float) {}
        })

        initView()
    }

    private fun initView() {
        buttonApply.setOnClickListener {
            handleApplyButtonClick()
            this.dismiss()
        }

        imageViewClose.setOnClickListener {
            this.dismiss()
        }

        textViewCancel.setOnClickListener {
            this.dismiss()
        }

        textViewClear.setOnClickListener {
            handleClearButtonClick()
        }

        buttonPostedDate.setOnClickListener {
            handlePostedDateClick()
        }

        buttonFollowUpDate.setOnClickListener {
            handleFollowUpDateClick()
        }

        buttonLeadStatus.setOnClickListener {
            handleLeadStatusClick()
        }

        updateUIIfDataAvailable()
    }

    private fun updateUIIfDataAvailable() {
        layoutWebLeadsFilterPostedDate.editTextCustomDate.setText(preferenceManager.readString(POST_DATED_CUSTOM_VALUE, ""))
        layoutWebLeadsFilterFollowUpDate.editTextCustomDate.setText(preferenceManager.readString(FOLLOW_UP_DATE_CUSTOM_VALUE, ""))

        when {
            preferenceManager.readString(POST_DATED_VALUE, "").equals(TODAY, ignoreCase = true) -> layoutWebLeadsFilterPostedDate.checkBoxToday.isChecked = true
            preferenceManager.readString(POST_DATED_VALUE, "").equals(TOMORROW, ignoreCase = true) -> layoutWebLeadsFilterPostedDate.checkBoxTomorrow.isChecked = true
            preferenceManager.readString(POST_DATED_VALUE, "").equals(YESTERDAY, ignoreCase = true) -> layoutWebLeadsFilterPostedDate.checkBoxYesterday.isChecked = true
            preferenceManager.readString(POST_DATED_VALUE, "").equals(LAST7DAYS, ignoreCase = true) -> layoutWebLeadsFilterPostedDate.checkBoxLastSevenDays.isChecked = true
            preferenceManager.readString(POST_DATED_VALUE, "").equals(LAST15DAYS, ignoreCase = true) -> layoutWebLeadsFilterPostedDate.checkBoxLastFifteenDays.isChecked = true
        }

        when {
            preferenceManager.readString(FOLLOW_UP_DATE_VALUE, "").equals(TODAY, ignoreCase = true) -> layoutWebLeadsFilterFollowUpDate.checkBoxToday.isChecked = true
            preferenceManager.readString(FOLLOW_UP_DATE_VALUE, "").equals(TOMORROW, ignoreCase = true) -> layoutWebLeadsFilterFollowUpDate.checkBoxTomorrow.isChecked = true
            preferenceManager.readString(FOLLOW_UP_DATE_VALUE, "").equals(YESTERDAY, ignoreCase = true) -> layoutWebLeadsFilterFollowUpDate.checkBoxYesterday.isChecked = true
            preferenceManager.readString(FOLLOW_UP_DATE_VALUE, "").equals(LAST7DAYS, ignoreCase = true) -> layoutWebLeadsFilterFollowUpDate.checkBoxLastSevenDays.isChecked = true
            preferenceManager.readString(FOLLOW_UP_DATE_VALUE, "").equals(LAST15DAYS, ignoreCase = true) -> layoutWebLeadsFilterFollowUpDate.checkBoxLastFifteenDays.isChecked = true
        }

        layoutWebLeadsStatus.checkBoxHot.isChecked = preferenceManager.readBoolean(LEAD_STATUS_SELECTED_HOT, false)
        layoutWebLeadsStatus.checkBoxOpen.isChecked = preferenceManager.readBoolean(LEAD_STATUS_SELECTED_OPEN, false)
        layoutWebLeadsStatus.checkBoxWarm.isChecked = preferenceManager.readBoolean(LEAD_STATUS_SELECTED_WARM, false)
        layoutWebLeadsStatus.checkBoxCold.isChecked = preferenceManager.readBoolean(LEAD_STATUS_SELECTED_COLD, false)
        layoutWebLeadsStatus.checkBoxLost.isChecked = preferenceManager.readBoolean(LEAD_STATUS_SELECTED_LOST, false)
        layoutWebLeadsStatus.checkBoxClosed.isChecked = preferenceManager.readBoolean(LEAD_STATUS_SELECTED_CLOSED, false)
        layoutWebLeadsStatus.checkBoxNoResponse.isChecked = preferenceManager.readBoolean(LEAD_STATUS_SELECTED_NO_RESPONSE, false)
        layoutWebLeadsStatus.checkBoxNotInterested.isChecked = preferenceManager.readBoolean(LEAD_STATUS_SELECTED_NOT_INTERESTED, false)
        layoutWebLeadsStatus.checkBoxOutstationLeads.isChecked = preferenceManager.readBoolean(LEAD_STATUS_SELECTED_OUTSTATION_LEAD, false)
        layoutWebLeadsStatus.checkBoxWalkIns.isChecked = preferenceManager.readBoolean(LEAD_STATUS_SELECTED_WALK_INS, false)
        layoutWebLeadsStatus.checkBoxTestDriveTaken.isChecked = preferenceManager.readBoolean(LEAD_STATUS_SELECTED_TEST_DRIVEN_TAKEN, false)
    }

    private fun handlePostedDateClick() {
        layoutWebLeadsFilterPostedDate.visibility = View.VISIBLE
        layoutWebLeadsFilterFollowUpDate.visibility = View.GONE
        layoutWebLeadsStatus.visibility = View.GONE
        layoutWebLeadsFilterPostedDate.editTextCustomDate.setOnClickListener {
            val postCalendarDialog = SalesCalendarFilterDialog(
                    activity,
                    "CustomPostDate"
            ) { startDateString, endDateString, dateType ->
                //webLeadsStartDate = startDateString
                //webLeadsEndDate = endDateString
                //callCPT()
                Log.e(TAG, "startDateString ::$startDateString")
                Log.e(TAG, "endDateString ::$endDateString")
                Log.e(TAG, "dateType ::$dateType")
                layoutWebLeadsFilterPostedDate.editTextCustomDate.setText(endDateString)
            }
            postCalendarDialog.setCancelable(false)
            postCalendarDialog.show()
        }
    }

    private fun handleFollowUpDateClick() {
        layoutWebLeadsFilterPostedDate.visibility = View.GONE
        layoutWebLeadsFilterFollowUpDate.visibility = View.VISIBLE
        layoutWebLeadsStatus.visibility = View.GONE
        layoutWebLeadsFilterFollowUpDate.linLayDates.checkBoxTomorrow.visibility = View.VISIBLE
        layoutWebLeadsFilterFollowUpDate.editTextCustomDate.setOnClickListener {
            val postCalendarDialog = SalesCalendarFilterDialog(
                    activity,
                    "CustomFollowDate"
            ) { startDateString, endDateString, dateType ->
                //webLeadsStartDate = startDateString
                //webLeadsEndDate = endDateString
                //callCPT()
                layoutWebLeadsFilterFollowUpDate.editTextCustomDate.setText(startDateString)
            }
            postCalendarDialog.setCancelable(false)
            postCalendarDialog.show()
        }
    }

    private fun handleLeadStatusClick() {
        layoutWebLeadsFilterPostedDate.visibility = View.GONE
        layoutWebLeadsFilterFollowUpDate.visibility = View.GONE
        layoutWebLeadsStatus.visibility = View.VISIBLE
        layoutWebLeadsStatus.linLayDates.visibility = View.GONE
        layoutWebLeadsStatus.linLayLeadStatus.visibility = View.VISIBLE
    }

    private fun handleApplyButtonClick() {
        var postDateSelectedValue = ""
        when {
            layoutWebLeadsFilterPostedDate.checkBoxToday.isChecked -> postDateSelectedValue = TODAY
            layoutWebLeadsFilterPostedDate.checkBoxTomorrow.isChecked -> postDateSelectedValue = TOMORROW
            layoutWebLeadsFilterPostedDate.checkBoxYesterday.isChecked -> postDateSelectedValue = YESTERDAY
            layoutWebLeadsFilterPostedDate.checkBoxLastSevenDays.isChecked -> postDateSelectedValue = LAST7DAYS
            layoutWebLeadsFilterPostedDate.checkBoxLastFifteenDays.isChecked -> postDateSelectedValue = LAST15DAYS
        }

        preferenceManager.writeString(POST_DATED_VALUE, postDateSelectedValue)
        preferenceManager.writeString(POST_DATED_CUSTOM_VALUE, layoutWebLeadsFilterPostedDate.editTextCustomDate.text.toString().trim())

        var followUpDateSelectedValue = ""
        when {
            layoutWebLeadsFilterFollowUpDate.checkBoxToday.isChecked -> followUpDateSelectedValue = TODAY
            layoutWebLeadsFilterFollowUpDate.checkBoxTomorrow.isChecked -> followUpDateSelectedValue = TOMORROW
            layoutWebLeadsFilterFollowUpDate.checkBoxYesterday.isChecked -> followUpDateSelectedValue = YESTERDAY
            layoutWebLeadsFilterFollowUpDate.checkBoxLastSevenDays.isChecked -> followUpDateSelectedValue = LAST7DAYS
            layoutWebLeadsFilterFollowUpDate.checkBoxLastFifteenDays.isChecked -> followUpDateSelectedValue = LAST15DAYS
        }

        preferenceManager.writeString(FOLLOW_UP_DATE_VALUE, followUpDateSelectedValue)
        preferenceManager.writeString(FOLLOW_UP_DATE_CUSTOM_VALUE, layoutWebLeadsFilterFollowUpDate.editTextCustomDate.text.toString().trim())
        preferenceManager.writeBoolean(LEAD_STATUS_SELECTED_HOT, layoutWebLeadsStatus.checkBoxHot.isChecked)
        preferenceManager.writeBoolean(LEAD_STATUS_SELECTED_OPEN, layoutWebLeadsStatus.checkBoxOpen.isChecked)
        preferenceManager.writeBoolean(LEAD_STATUS_SELECTED_WARM, layoutWebLeadsStatus.checkBoxWarm.isChecked)
        preferenceManager.writeBoolean(LEAD_STATUS_SELECTED_COLD, layoutWebLeadsStatus.checkBoxCold.isChecked)
        preferenceManager.writeBoolean(LEAD_STATUS_SELECTED_LOST, layoutWebLeadsStatus.checkBoxLost.isChecked)
        preferenceManager.writeBoolean(LEAD_STATUS_SELECTED_CLOSED, layoutWebLeadsStatus.checkBoxClosed.isChecked)
        preferenceManager.writeBoolean(LEAD_STATUS_SELECTED_NO_RESPONSE, layoutWebLeadsStatus.checkBoxNoResponse.isChecked)
        preferenceManager.writeBoolean(LEAD_STATUS_SELECTED_NOT_INTERESTED, layoutWebLeadsStatus.checkBoxNotInterested.isChecked)
        preferenceManager.writeBoolean(LEAD_STATUS_SELECTED_OUTSTATION_LEAD, layoutWebLeadsStatus.checkBoxOutstationLeads.isChecked)
        preferenceManager.writeBoolean(LEAD_STATUS_SELECTED_WALK_INS, layoutWebLeadsStatus.checkBoxWalkIns.isChecked)
        preferenceManager.writeBoolean(LEAD_STATUS_SELECTED_TEST_DRIVEN_TAKEN, layoutWebLeadsStatus.checkBoxTestDriveTaken.isChecked)
    }

    private fun handleClearButtonClick() {
        layoutWebLeadsFilterPostedDate.editTextCustomDate.setText("")
        layoutWebLeadsFilterPostedDate.checkBoxTomorrow.isChecked = false
        layoutWebLeadsFilterPostedDate.checkBoxToday.isChecked = false
        layoutWebLeadsFilterPostedDate.checkBoxYesterday.isChecked = false
        layoutWebLeadsFilterPostedDate.checkBoxLastFifteenDays.isChecked = false
        layoutWebLeadsFilterPostedDate.checkBoxLastSevenDays.isChecked = false

        layoutWebLeadsFilterFollowUpDate.editTextCustomDate.setText("")
        layoutWebLeadsFilterFollowUpDate.checkBoxTomorrow.isChecked = false
        layoutWebLeadsFilterFollowUpDate.checkBoxToday.isChecked = false
        layoutWebLeadsFilterFollowUpDate.checkBoxYesterday.isChecked = false
        layoutWebLeadsFilterFollowUpDate.checkBoxLastFifteenDays.isChecked = false
        layoutWebLeadsFilterFollowUpDate.checkBoxLastSevenDays.isChecked = false

        layoutWebLeadsStatus.checkBoxHot.isChecked = false
        layoutWebLeadsStatus.checkBoxOpen.isChecked = false
        layoutWebLeadsStatus.checkBoxWarm.isChecked = false
        layoutWebLeadsStatus.checkBoxCold.isChecked = false
        layoutWebLeadsStatus.checkBoxLost.isChecked = false
        layoutWebLeadsStatus.checkBoxClosed.isChecked = false
        layoutWebLeadsStatus.checkBoxNoResponse.isChecked = false
        layoutWebLeadsStatus.checkBoxNotInterested.isChecked = false
        layoutWebLeadsStatus.checkBoxOutstationLeads.isChecked = false
        layoutWebLeadsStatus.checkBoxWalkIns.isChecked = false
        layoutWebLeadsStatus.checkBoxTestDriveTaken.isChecked = false
    }

    private fun getScreenHeight(): Int {
        return Resources.getSystem().displayMetrics.heightPixels
    }

    companion object {
        const val POST_DATED_VALUE = "POST_DATED_VALUE"
        const val POST_DATED_CUSTOM_VALUE = "POST_DATED_CUSTOM_VALUE"
        const val FOLLOW_UP_DATE_VALUE = "FOLLOW_UP_DATE_VALUE"
        const val FOLLOW_UP_DATE_CUSTOM_VALUE = "FOLLOW_UP_DATE_CUSTOM_VALUE"
        const val LEAD_STATUS_SELECTED_HOT = "LEAD_STATUS_SELECTED_HOT"
        const val LEAD_STATUS_SELECTED_OPEN = "LEAD_STATUS_SELECTED_OPEN"
        const val LEAD_STATUS_SELECTED_WARM = "LEAD_STATUS_SELECTED_WARM"
        const val LEAD_STATUS_SELECTED_COLD = "LEAD_STATUS_SELECTED_COLD"
        const val LEAD_STATUS_SELECTED_LOST = "LEAD_STATUS_SELECTED_LOST"
        const val LEAD_STATUS_SELECTED_CLOSED = "LEAD_STATUS_SELECTED_CLOSED"
        const val LEAD_STATUS_SELECTED_NO_RESPONSE = "LEAD_STATUS_SELECTED_NO_RESPONSE"
        const val LEAD_STATUS_SELECTED_NOT_INTERESTED = "LEAD_STATUS_SELECTED_NOT_INTERESTED"
        const val LEAD_STATUS_SELECTED_OUTSTATION_LEAD = "LEAD_STATUS_SELECTED_OUTSTATION_LEAD"
        const val LEAD_STATUS_SELECTED_WALK_INS = "LEAD_STATUS_SELECTED_WALK_INS"
        const val LEAD_STATUS_SELECTED_TEST_DRIVEN_TAKEN = "LEAD_STATUS_SELECTED_TEST_DRIVEN_TAKEN"

        const val TODAY = "TODAY"
        const val TOMORROW = "TOMORROW"
        const val YESTERDAY = "YESTERDAY"
        const val LAST7DAYS = "LAST7DAYS"
        const val LAST15DAYS = "LAST15DAYS"

        val TAG: String = FilterWebLeadsBottomSheetFragment.javaClass.simpleName

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                FilterWebLeadsBottomSheetFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}