package remotedealer.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.mfcwl.mfc_dealer.R
import com.squareup.picasso.Picasso
import gallerylib.utils.PicassoImageLoader
import remotedealer.model.stock.RDMStocksItem
import remotedealer.ui.RDMStockDetailsFragment
import remotedealer.util.NavigationUtility

/*
 * Created By Uday(MFCWL) ->  16-12-2020 17:54
 *
 */
class RDMStocksAdapter(var c: Context, var a: FragmentActivity, var list: ArrayList<RDMStocksItem>)
    : RecyclerView.Adapter<RDMStocksAdapter.MyViewHolder>() {

    var TAG = javaClass.simpleName
    lateinit var vi: View

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        Log.i(TAG, "onCreateViewHolder: ")


        vi = LayoutInflater.from(c).inflate(R.layout.rdm_stock_row_item, parent, false)

        return MyViewHolder(vi)
    }


    private fun loadFragment(frag: Fragment) {
       /* a.supportFragmentManager.beginTransaction()
               .replace(R.id.flFragment, frag)
                .commit()*/
       // NavigationUtility.removeAllFragments(c as FragmentActivity)
        NavigationUtility.addFragment(frag, a, frag.tag)
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = list[position]

        if (item.images!=null && item.images?.isNotEmpty() == true) {

            val imageURL = item.images?.get(0)?.url

            if(imageURL!= null &&imageURL != ""){
                Log.i(TAG, "onBindViewHolder: $position ${item.vehicleMake}: ${item.images?.size}")
                Picasso.get().load(imageURL).resize(90,90).placeholder(R.drawable.no_car_small).into(holder.ivCar)
            }else{
                Picasso.get().load(R.drawable.no_car_small).resize(90,90).placeholder(R.drawable.no_car_small).into(holder.ivCar)
            }

        }else{
            Picasso.get().load(R.drawable.no_car_small).resize(90,90).placeholder(R.drawable.no_car_small).into(holder.ivCar)

        }

        holder.tvDateTime.text = getOnlyDate(item.postedDate)
        holder.tvMake.text = item.vehicleMake
        holder.tvModelVariant.text = "${item.vehicleModel} ${item.vehicleVariant}"
        holder.tvVehicleColor.text = item.colour + "  |  "
        holder.tvVehicleKms.text = "" + item.kilometer + "Km  |  "
        holder.tvVehicleOwners.text = "${item.owner} owner  |  "
        holder.tvNo.text = item.privateVehicle
        holder.tvVehicleCost.text = "\u20B9" + item.boughtPrice
        holder.tvTotalSalesLeads.text = "Total Sales Leads " + item.leadImport
        holder.tvPS.text = item.stockSource
        holder.tvRetail.text = "Retail " //+list[position]
        holder.tvCertified.text = item.certifiedText
        holder.tvWarranty.text = item.warrantyRecommended


        holder.itemView.setOnClickListener {
            var stockDetailed = RDMStockDetailsFragment.newInstance(item)
            loadFragment(stockDetailed)
        }
    }

    private fun getOnlyDate(postedDate: String): String {
        var date = ""
        try {
            date = postedDate.split(" ")[0]
        } catch (e: Exception) {
            Log.e(TAG, "getOnlyDate: " + e.message)
            return postedDate
        }
        return date
    }

    override fun getItemCount() = list.size


    class MyViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var ivCar: ImageView = v.findViewById(R.id.ivCar)
        var tvDateTime: TextView = v.findViewById(R.id.tvDateTime)
        var tvMake: TextView = v.findViewById(R.id.tvMake)
        var tvModelVariant: TextView = v.findViewById(R.id.tvModelVariant)
        var tvVehicleColor: TextView = v.findViewById(R.id.tvVehicleColor)
        var tvVehicleKms: TextView = v.findViewById(R.id.tvVehicleKms)
        var tvVehicleOwners: TextView = v.findViewById(R.id.tvVehicleOwners)
        var tvNo: TextView = v.findViewById(R.id.tvNo)
        var tvVehicleCost: TextView = v.findViewById(R.id.tvVehicleCost)
        var tvTotalSalesLeads: TextView = v.findViewById(R.id.tvTotalSalesLeads)
        var tvPS: TextView = v.findViewById(R.id.tvPS)
        var tvRetail: TextView = v.findViewById(R.id.tvRetail)
        var tvCertified: TextView = v.findViewById(R.id.tvCertified)
        var tvWarranty: TextView = v.findViewById(R.id.tvWarranty)
    }
}