package remotedealer.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mfcwl.mfc_dealer.Activity.RDR.DiscussionPoint1;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.AppConstants;
import com.mfcwl.mfc_dealer.Utility.CustomFonts;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import rdm.ScheduleEventAdapter;
import rdm.model.EventData;

public class ParticipantAdapter extends RecyclerView.Adapter<ParticipantAdapter.ViewHolder> {


    Activity activity;
    String TAG = ParticipantAdapter.class.getSimpleName();
    ArrayList<String> participantList = new ArrayList<>();
    ParticipantCallBack callBack;

    public ParticipantAdapter(List<String> participantList, Activity activity,ParticipantCallBack callBack) {
        this.participantList.addAll(participantList);
        this.activity = activity;
        this.callBack=callBack;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.add_participant_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        Log.i(TAG, "onCreateViewHolder: ");
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (!participantList.get(position).isEmpty() && participantList.get(position) != null) {
            holder.tvParticipantsValue.setText(participantList.get(position));
        }

        holder.iv_clearParticipants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                participantList.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, participantList.size());
                callBack.updateParticipantList(participantList);
            }
        });
    }

    @Override
    public int getItemCount() {
        return participantList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvParticipantsValue;
        ImageView iv_clearParticipants;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvParticipantsValue = itemView.findViewById(R.id.tvParticipantsValue);
            iv_clearParticipants = itemView.findViewById(R.id.iv_clearParticipants);
        }
    }

    public interface ParticipantCallBack {
        void updateParticipantList(ArrayList<String> eventParticipants);
    }

}
