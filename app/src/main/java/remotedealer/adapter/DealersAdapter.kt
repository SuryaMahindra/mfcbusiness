package remotedealer.adapter

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mfcwl.mfc_dealer.R
import remotedealer.listener.DealerListener
import remotedealer.model.DealerItem

/*
 * Created By Uday(MFCWL) ->  14-12-2020 14:22
 *
 */
class DealersAdapter(var c: Context,
                     var a: Activity,
                     var dealerCode:String,
                     var list: ArrayList<DealerItem>,
                     var dealerListener: DealerListener) :
        RecyclerView.Adapter<DealersAdapter.MyViewHolder>() {

    var TAG = javaClass.simpleName
    lateinit var vi: View

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        Log.i(TAG, "onCreateViewHolder: ")

        vi = LayoutInflater.from(c).inflate(R.layout.rdm_dealers_row_item, parent, false)

        return MyViewHolder(vi)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = list[position]
        holder.dealerName.text = item.name
        holder.dealerLocation.text = item.city
        if(item.code==dealerCode){
            holder.dealerName.setTextColor(a.resources.getColor(R.color.rdr_blue))
            holder.dealerLocation.setTextColor(a.resources.getColor(R.color.rdr_blue))
        }
        vi.setOnClickListener {
            dealerListener.onDealerClicked(list[position])
            dealerCode = item.code
        }
    }

    override fun getItemCount() = list.size


    class MyViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var dealerName = v.findViewById(R.id.tvDealerName) as TextView
        var dealerLocation = v.findViewById(R.id.tvDealerLocation) as TextView
    }
}