package remotedealer.adapter

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mfcwl.mfc_dealer.R
import remotedealer.model.sales.RDMSalesItem

/*
 * Created By Uday(MFCWL) ->  18-12-2020 11:40
 *
 */
class RDMSalesListAdapter(var c: Context, var a: Activity, var list: ArrayList<RDMSalesItem>) :
        RecyclerView.Adapter<RDMSalesListAdapter.MyViewHolder>() {

    var TAG = javaClass.simpleName
    lateinit var v: View

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        Log.i(TAG, "onCreateViewHolder: ")


        v = LayoutInflater.from(c).inflate(R.layout.rdm_sales_list_row_item, parent, false)

        return MyViewHolder(v)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        /*PicassoImageLoader(list[position].coverImage, 90, 90)
                .loadMedia(c, holder.ivCar) {
                    Log.i(TAG, "onBindViewHolder: image loaded")
                }*/
        holder.tvDateTime.text = getOnlyDate(list[position].soldDate)
        holder.tvMake.text = list[position].make
        holder.tvModelVariant.text = list[position].model + " " + list[position].variant
        holder.tvNo.text = "${list[position].registrationNumber}  |  "
        holder.tvVehicleYear.text =  "${list[position].modelYear}  |  "
        holder.tvVehicleKms.text =  "${list[position].kilometer} Km  |  "
        if(list[position].owner !=  null){
            holder.tvVehicleOwners.text = "${list[position].owner} owner   "
        }else{
            holder.tvVehicleOwners.text = "1 owner   "
        }
        holder.tvVehicleCost.text = "\u20B9" + list[position].sellingPrice
        holder.tvAssistFirst.text = list[position].warrantyType
        //holder.tvTotalSalesLeads.text = "Total Sales Leads " + list[position].ins
    }

    override fun getItemCount() = list.size


    class MyViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var ivCar: ImageView = v.findViewById(R.id.ivCar)
        var tvDateTime: TextView = v.findViewById(R.id.tvDateTime)
        var tvMake: TextView = v.findViewById(R.id.tvMake)
        var tvModelVariant: TextView = v.findViewById(R.id.tvModelVariant)

        var tvNo: TextView = v.findViewById(R.id.tvNo)
        var tvVehicleYear: TextView = v.findViewById(R.id.tvVehicleYear)
        var tvVehicleKms: TextView = v.findViewById(R.id.tvVehicleKms)
        var tvVehicleOwners: TextView = v.findViewById(R.id.tvVehicleOwners)

        var tvVehicleCost: TextView = v.findViewById(R.id.tvVehicleCost)
        var tvAssistFirst: TextView = v.findViewById(R.id.tvAssistFirst)
    }

    private fun getOnlyDate(postedDate: String): String {
        var date = ""
        try {
            date = postedDate.split(" ")[0]
        } catch (e: Exception) {
            Log.e(TAG, "getOnlyDate: " + e.message)
            return postedDate
        }
        return date
    }
}