package remotedealer.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.mfcwl.mfc_dealer.R
import remotedealer.model.DealerItem

class DealerAdapter2(val context: Context, var dataSource: List<DealerItem>) : BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val vh: ItemHolder
        if (convertView == null) {
            view = inflater.inflate(R.layout.rdm_dealers_row_item, parent, false)
            vh = ItemHolder(view)
            view?.tag = vh
        } else {
            view = convertView
            vh = view.tag as ItemHolder
        }

        vh.dealerName.text = dataSource.get(position).name
        vh.dealerLocation.text = dataSource.get(position).city

        return view
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getCount() = dataSource.size

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    class ItemHolder(row: View) {
        val dealerName: TextView = row.findViewById(R.id.tvDealerName)
        val dealerLocation: TextView = row.findViewById(R.id.tvDealerLocation)
    }

}