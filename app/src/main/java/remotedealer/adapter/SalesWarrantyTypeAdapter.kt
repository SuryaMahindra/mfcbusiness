package remotedealer.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.mfcwl.mfc_dealer.R
import kotlinx.android.synthetic.main.sales_warranty_type_row.view.*
import remotedealer.model.procurement.ProcureItem
import remotedealer.model.sales.RDMSalesRequest
import remotedealer.ui.RDMSalesListFragmentNew

/*
 * Created By Uday(MFCWL) ->  12-12-2020 16:36
 *
 */
class SalesWarrantyTypeAdapter(var c: Context, var a: FragmentActivity, var code: String, var token: String, var list: ArrayList<ProcureItem>) :
        RecyclerView.Adapter<SalesWarrantyTypeAdapter.WarrantyTypeViewHolder>() {

    var TAG = javaClass.simpleName
    lateinit var vi: View

    var salesWarrantyStartDate = ""
    var salesWarrantyEndDate = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WarrantyTypeViewHolder {
        Log.i(TAG, "onCreateViewHolder: " + list.size)

        vi = LayoutInflater.from(c).inflate(R.layout.sales_warranty_type_row, parent, false)

        return WarrantyTypeViewHolder(vi)
    }

    override fun onBindViewHolder(holder: WarrantyTypeViewHolder, position: Int) {
        holder.bind(list[position], position)
    }

    override fun getItemCount() = list.size

    inner class WarrantyTypeViewHolder(var v: View) : RecyclerView.ViewHolder(v) {

        fun bind(procureItem: ProcureItem, itemPosition: Int) {
            v.tvParticular.text = procureItem.particulars
            v.tvOMS.text = procureItem.oms
            v.tvRDR.text = procureItem.rdr
            v.tvOMS.setOnClickListener {

                val fragment: Fragment = if (itemPosition == 0)
                    RDMSalesListFragmentNew(token, code, "warranty", getWhereList("warranty_sales"), getWhereInList())
                else
                    RDMSalesListFragmentNew(token, code, "warranty", getWhereList(procureItem.particulars), getWhereInList())

                a.supportFragmentManager.beginTransaction().replace(R.id.flFragment, fragment).commit()
            }
        }

    }

    private fun getWhereList(str: String): ArrayList<RDMSalesRequest.Where> {
        val whereList = ArrayList<RDMSalesRequest.Where>()



        val stockType = RDMSalesRequest.Where()
        stockType.column = "stock_type"
        stockType.operator = "="
        stockType.value = "4W"
        whereList.add(stockType)

        val where = RDMSalesRequest.Where()
        where.column = "warranty_punched_date"
        where.operator = ">="
        where.value = salesWarrantyStartDate
        whereList.add(where)

        val where2 = RDMSalesRequest.Where()
        where2.column = "warranty_punched_date"
        where2.operator = "<="
        where2.value = salesWarrantyEndDate
        whereList.add(where2)

        val where3 = RDMSalesRequest.Where()
        where3.column = "sold"
        where3.operator = "="
        where3.value = "1"
        whereList.add(where3)

        if (str == "warranty_sales") {
            val where1 = RDMSalesRequest.Where()
            where1.column = "warranty_number"
            where1.operator = "!="
            where1.value = null
            whereList.add(where1)

        } else {
            val where4 = RDMSalesRequest.Where()
            where4.column = "warranty_type"
            where4.operator = "="
            where4.value = str
            whereList.add(where4)
        }

        return whereList
    }


    private fun getWhereInList(): ArrayList<RDMSalesRequest.WhereIn> {
        val whereInList = ArrayList<RDMSalesRequest.WhereIn>()
        val whereIn1 = RDMSalesRequest.WhereIn()
        whereIn1.column = "dealer_code"
        whereIn1.values = arrayOf("$code")
        whereInList.add(whereIn1)

        val whereIn3 = RDMSalesRequest.WhereIn()
        whereIn3.column = "source"
        whereIn3.values = arrayOf("p&s", "mfc")
        whereInList.add(whereIn3)

        return whereInList
    }

}