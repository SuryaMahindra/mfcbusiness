package remotedealer.listener

import remotedealer.model.DealerItem

interface DealerListener {
    fun onDealerClicked(dealer: DealerItem)
}