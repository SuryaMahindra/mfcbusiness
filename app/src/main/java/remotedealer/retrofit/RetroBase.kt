package remotedealer.retrofit

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetroBase {

    companion object {


           //Stage
      /*  var RDR_BASE_URL = "http://rdm-services.stagemfc.com/api/v2/"
        var MFC_BASE_URL = "https://newadmin.stagemfc.com/mfc_apis/"
        var LMS_BASE_URL = "http://lms.stageibb.com/leads_microservice/public/api/"*/



        //Production
        var RDR_BASE_URL = "https://rdm-services.mahindrafirstchoice.com/api/v2/"
        var MFC_BASE_URL = "https://newadmin.emfcwl.com/mfc_apis/"
        var LMS_BASE_URL = "https://lms-api.prodibb.com/public/api/"


        const val URL_END_GET_TOKEN = "Auth"
        const val URL_END_INSERT_RDR_FORM = "RDRForm/SetRDRForm"
        const val URL_END_SAVE_RDR_FORM = "RDRForm/StoreRDRForm"

        const val URL_END_GET_RDR_FORM = "RDRForm/GetRDRForm"
        const val URL_END_GET_RDR_ACTON_ITEM = "RDRForm/GetRDRActionItem?dealerCode="
        const val URL_END_STOCKS = "RDRForm/GetRDRStackItem?dealerCode="
        const val URL_END_SALES = "RDRForm/GetRDRSalesItem?dealerCode="
        const val URL_END_PROCUREMENT = "RDRForm/GetRDRProcItem?dealerCode="
        const val URL_END_DASHBOARD_RAGSTATUS = "Dashboard/GetTodaysRAGStatus"
        const val URL_END_DASHBOARD_ASMMETRICS = "Dashboard/GetMetricsWiseStatus"
        const val URL_END_DEALER_NON_OPERATIONAL = "RDRForm/SetRDRForm"
        const val URL_END_DEALER_OPERATIONAL = "ThreeDot/SetDealerOperational"
        const val URL_END_DEALER_GETPHONE_NUMBERS = "ThreeDot/GetDealerPhone"
        const val URL_END_DASHBOARD_DEALERLIST = "Dashboard/GetDealerList"
        const val URL_END_DEALER_SEND_EMAIL_REPORT = "v3/api/franschisereport/sendemailrepor"
        const val URL_END_DASHBOARD_RDRSTATUS = "Dashboard/GetRDRStatus"
        const val URL_END_DEALER_MATRIX = "Dealer/GetDealerMetrics?dealerCode="
        const val URL_END_DEALER_INITIATIVE = "Dealer/GetDealerInitiative?dealerCode="
        const val URL_FETCH_EVENTS = "v2/api/franchise/event/fetch?"
        const val URL_DELETE_EVENTS = "Dealer/CloseActionItem"

        const val URL_END_PROCUREMENT_CPT = "PandSDashboard/GetCPT"
        const val URL_END_PROCUREMENT_IEP = "PandSDashboard/GetIEP"
        const val URL_END_PROCUREMENT_SELF = "PandSDashboard/GetSelf"

        const val URL_END_STORE_STOCKS = "PandSDashboard/GetStoreStock"
        const val URL_END_BOOKED_STOCKS = "PandSDashboard/GetBookedStock"

        const val URL_END_WEB_LEADS = "Leads/GetWebLeads"
        const val URL_END_WEB_LEADS_FOLLOW_UP = "Leads/GetFollowUpPendingWebLeads"
        const val URL_END_WEB_LEADS_FOLLOW_UP_TODAY = "Leads/GetFollowUpPendingTodayWebLeads"

        const val URL_END_WALK_IN_LEADS = "Leads/GetWalkInLeads"
        const val URL_END_WALK_IN_LEADS_FOLLOW_UP = "Leads/GetFollowUpPendingWalkInLeads"
        const val URL_END_WALK_IN_LEADS_FOLLOW_UP_TODAY = "Leads/GetFollowUpPendingTodayWalkInLeads"

        const val URL_END_SALES_LEADS = "Leads/GetSalesDetails"
        const val URL_END_SALES_LEADS_WARRANTY_REVENUE = "Leads/GetWarrantyRevenue"
        const val URL_END_SALES_LEADS_WARRANTY_TYPES = "Leads/GetWarrantyTypes"
        const val URL_COUNT = "Dealer/GetTotalCounts?dealerCode="
       //const val URL_END_DEALER_SEND_EMAIL_REPORT = "v3/api/franschisereport/sendemailreport"

        const val URL_GET_RDR_FORM = "RDRForm/GetRDRForm"
        /****************************************************************************/
        private var gson: Gson = GsonBuilder().setDateFormat("dd-MM-yyyy'T'HH:mm:ssZ").create()

        private var httpLoggingInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

        private var okHttpClient = OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor).build()

        private var retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(RDR_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build()
        var retroInterface: RetroInterface = retrofit.create(RetroInterface::class.java)


        /*****************************************************************************/

        //const val URL_END_RDM_STOCKS = "v2/api/dealer/stocks/store"
       const val URL_END_RDM_STOCKS = "v3/api/dealer/stocks/store"//prod


        private var mfcRetrofit: Retrofit = Retrofit.Builder()
                .baseUrl(MFC_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build()
        var mfcRetroInterface: RetroInterface = mfcRetrofit.create(RetroInterface::class.java)


        /*****************************************************************************/
        const val URL_END_RDM_SALES = "v3/api/franchise/WSRTSR"
        const val URL_END_RDM_LEAD_ACCESS_TOKEN = "mfcwapp/generateAccessToken"

        private var lmsRetrofit: Retrofit = Retrofit.Builder()
                .baseUrl(LMS_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build()
        var lmsRetroInterface: RetroInterface = lmsRetrofit.create(RetroInterface::class.java)
        /****************************************************************************/
    }
}