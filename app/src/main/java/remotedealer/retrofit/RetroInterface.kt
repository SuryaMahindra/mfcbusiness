package remotedealer.retrofit

import retrofit2.Call
import retrofit2.http.*

interface RetroInterface {

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST
    fun getFromWeb(@Body request: Any, @Url url: String): Call<Any>

    @Headers("content-Type: application/json; charset=utf-8")
    @GET
    fun getFromWeb(@Url url: String, @Header("Authorization") token: String): Call<Any>

    @Headers("content-Type: application/json; charset=utf-8")
    @POST
    fun getFromWebUsingToken(@Body request: Any, @Url url: String, @Header("token") token: String): Call<Any>


    @Headers("content-Type: application/json; charset=utf-8")
    @GET
    fun getEvents(@Url url: String, @Header("token") token: String): Call<Any>

    @Headers("content-Type: application/json; charset=utf-8")
    @POST
    fun getFromWeb(@Body request: Any, @Url url: String, @Header("Authorization") token: String): Call<Any>

    @Headers("content-Type: application/json; charset=utf-8")
    @PUT
    fun deleteFromWeb(@Body request: Any, @Url url: String, @Header("Authorization") token: String): Call<Any>

    @Headers("content-Type: application/json; charset=utf-8")
    @POST
    fun sendEmail(@Body request: Any, @Url url: String, @Header("token") token: String): Call<Any>

    @Headers("content-Type: application/json; charset=utf-8")
    @POST
    fun postRequestWeb(@Body request: Any, @Url url: String, @Header("token") token: String): Call<Any>
    @Headers("content-Type: application/json; charset=utf-8")
    @POST
    fun makeDealerOperational(@Url url: String, @Header("Authorization") token: String): Call<Any>

}