package remotedealer

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.mfcwl.mfc_dealer.R
import com.mfcwl.mfc_dealer.Utility.SpinnerManager
import remotedealer.model.token.RDRTokenRequest
import remotedealer.model.token.RDRTokenResponse
import remotedealer.rdrform.model.*
import remotedealer.retrofit.RetroBase.Companion.URL_END_GET_TOKEN
import remotedealer.retrofit.RetroBase.Companion.URL_END_INSERT_RDR_FORM
import remotedealer.retrofit.RetroBase.Companion.retroInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RDRDashboardActivity : AppCompatActivity() {

    var TAG = javaClass.simpleName

    /*
     * 1.get token web api integrated
     * 2.insert RDR Form api is integrated
     *
     */

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.rdm_dashboard_activity)


        callGetToken()


    }

    private fun callRDRForm(rdrToken: String) {

        val req = RDRFormRequest()

        val reportFor = ReportFor()
        reportFor.dealerCode = "1452"
        reportFor.visitedById = 469
        reportFor.videoUrl = "2"
        reportFor.isOperational = "YES"


        val procurement = ProcurementRDMFormInfoModel()
        procurement.liveAuctionsIep = "2"
        procurement.liveAuctionsCpt = "2"
        procurement.vehicleNotPerRequirementIep = "2"
        procurement.vehicleNotPerRequirementCpt = "2"
        procurement.auctionsBidIep = "2"
        procurement.auctionsBidCpt = "2"
        procurement.auctionsWonIep = "2"
        procurement.auctionsWonCpt = "2"
        procurement.negotiationsIep = "2"
        procurement.negotiationsCpt = "2"
        procurement.procuredActualIep = "2"
        procurement.procuredActualCpt = "2"
        procurement.procuredAmSupport = "2"
        procurement.totalProcurementTarget = "2"
        procurement.totalProcurementActual = "2"
        procurement.iepAndCptProcuredTarget = "2"
        procurement.iepAndCptProcuredActual = "2"
        procurement.selfProcurement = "2"
        procurement.totalProcurementToday = "2"

        val stock = StackRDMFormInfoModel()
        stock.warrantyUnits = "2"
        stock.warrantyValue = "2"
        stock.financeNo = "2"
        stock.aggregatorNo = "2"
        stock.financeIntervention = "2"
        stock.pendingRc90Days = "2"
        stock.miblNo = "2"
        stock.royaltyMoneyActual = "2"
        stock.financePenetrationTarget = "2"
        stock.financePenetrationActual = "2"
        stock.warrantyPenetrationTarget = "2"
        stock.warrantyPenetrationActual = "2"
        stock.pendingRcTarget = "2"
        stock.pendingRcActual = "2"
        stock.royaltyCollected = "yes"
        stock.royaltyMoneyTarget = "2"

        val sales = SalesRDMFormInfoModel()
        sales.omsStocks = "2"
        sales.certifiedStocks = "2"
        sales.walkins = "2"
        sales.totalSales = "2"
        sales.retailSales = "2"
        sales.bookingsInHand = "2"
        sales.omsLeads = "2"
        sales.convOms = "2"
        sales.bookingThruOms = "2"
        sales.qualLeadsFromCre = "2"
        sales.convLeadsFromCre = "2"
        sales.totalStockTarget = "2"
        //sales.totalStockActual = "2"
        sales.totalSalesTarget = "2"
        sales.totalSalesActual = "2"
        sales.certifiedStockTarget = "2"
        sales.certifiedStockActual = "2"
        sales.omsContributionTarget = "2"
        sales.omsContributionActual = "2"
        sales.actualStock = "2"
        sales.repeatLeads = "2"
        sales.parkSellStock = "2"
        sales.stockNumber = "2"

        val actionItems = ArrayList<ActionItemInfoModel>()

        val item1 = ActionItemInfoModel()
        item1.category = "Stack"
        item1.actionItem = "Royalty Payment pending"
        item1.remarks = "MFC franchiss  is  not for free,  pay royalty man"

        actionItems.add(item1)

        req.reportFor = reportFor
        req.procurementRDMFormInfoModel = procurement
        req.stackRDMFormInfoModel = stock
        req.salesRDMFormInfoModel = sales
        req.actionItemsList = actionItems

        retroInterface.getFromWeb(req, URL_END_INSERT_RDR_FORM, "Bearer $rdrToken")
                .enqueue(object : Callback<Any> {
                    override fun onResponse(call: Call<Any>, response: Response<Any>) {

                        val strRes = Gson().toJson(response.body())
                        val modelRes = Gson().fromJson(strRes, RDRFormResponse::class.java)

                        Log.i(TAG, "onResponse: $strRes")
                    }

                    override fun onFailure(call: Call<Any>, t: Throwable) {
                          t.printStackTrace()
                    }
                })
    }

    private fun callGetToken() {


        SpinnerManager.showSpinner(this)
        retroInterface.getFromWeb(RDRTokenRequest(), URL_END_GET_TOKEN).enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                SpinnerManager.hideSpinner(this@RDRDashboardActivity)

                val strRes = Gson().toJson(response.body())
                val resModel = Gson().fromJson(strRes, RDRTokenResponse::class.java)

                Log.i(TAG, "onResponse: " + resModel.token)
                callRDRForm(resModel.token)
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(this@RDRDashboardActivity)
            }

        })

    }

    /*fun callMakes() {

        val req = FIMakeRequest()
        req.exeId = "1086"

        SpinnerManager.showSpinner(this)
        retroInterface.getFromWeb(req, URL_END_FI_MAKE).enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                SpinnerManager.hideSpinner(this@RDRDashboardActivity)

                val strRes = Gson().toJson(response.body())
                val resModel = Gson().fromJson(strRes, FIMMVResponse::class.java)

                Log.i(TAG, "onResponse: " + resModel.data?.size)
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(this@RDRDashboardActivity)
            }

        })

    }*/

}