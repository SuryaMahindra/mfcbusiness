package remotedealer.interfaces

import remotedealer.model.DealerItem

interface DealerInterface {
    fun onUpdate(dealer: DealerItem)
}