package remotedealer.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mfcwl.mfc_dealer.R
import kotlinx.android.synthetic.main.tutorial_two.*

class TutorialTwo : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tutorial_two)

        tutonext.setOnClickListener {
            val tutoTwo = Intent(this,TutorialThree::class.java)
            startActivity(tutoTwo)
            finish()
        }

        tutoback.setOnClickListener{
            val tutoTwo = Intent(this,TutorialOne::class.java)
            startActivity(tutoTwo)
            finish()
        }


    }

    override fun onBackPressed() {

    }
}