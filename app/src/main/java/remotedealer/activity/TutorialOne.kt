package remotedealer.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mfcwl.mfc_dealer.R
import kotlinx.android.synthetic.main.tutorial_one.*

class TutorialOne : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tutorial_one)

        tutonext.setOnClickListener{
            val tutoTwo = Intent(this,TutorialTwo::class.java)
            startActivity(tutoTwo)
            finish()
        }
    }

    override fun onBackPressed() {

    }
}