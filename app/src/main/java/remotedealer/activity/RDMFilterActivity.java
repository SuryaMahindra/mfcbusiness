package remotedealer.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.SalesStocks.Instances.SalesCalendarFilterDialog;
import com.mfcwl.mfc_dealer.Utility.AppConstants;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.sun.jersey.core.impl.provider.entity.XMLRootObjectProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import imageeditor.base.BaseActivity;

public class RDMFilterActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnCancelFilter, btnApplyFilter;
    boolean isClearAllEnabled = false;
    String strMessage = "";
    LinearLayout llPostedOrFollowUpDate, lLayoutFollowUpDate, llNewFilterLeadStatus, llLeadStatusAndCount, llFollowUpdate, llPostedDate, llFilterClosed, llFilterHot, llFilterWarm, llFilterCold, llFilterLost, llFilterNotInterested, llFilterNoResponse, llFilterTestDriveTaken, llFilterWalkIn, llFilterOpenNew, llFilterOutstation;
    Button btnSelectCustomDate;
    private String filterCategory = "", fromDate = "", toDate = "", leadStatus = "", customDate = "";
    TextView tvNewLeadFilterToday, tvNewLeadFilterLast7D, tvNewLeadFilterLast15D, tvNewLeadFilterLast30D, tvLFPostedDate,
            tvLFPostedDateCount, tvFollowUpDate, tvFollowUpDateCount, tvLeadStatus, tvLeadStatusCount, tvDeleteSelectedDate;

    boolean isLeadStatusOnClick = false;
    // Follow Up date filter TextView
    TextView tvSelectFCustDateLbl, tvFDeleteSelectedDate, tvFQuickSelectLbl, tvFNewLeadFilterToday, tvFNewLeadFilterLast7D, tvFNewLeadFilterLast15D, tvFNewLeadFilterLast30D;
    ImageView ivFLFPostedToday, ivFLFPostedLast7D, ivFLFPostedLast15D, ivFLFPostedLast30D;
    Button btnSelectFCustomDate;
    TextView tvNewFilterClosed, tvNewFilterHot, tvNewFilterWarm, tvNewFilterCold, tvNewFilterLost, tvNewFilterNotInterested, tvNewFilterNoResponse,
            tvNewFilterTestDriveTaken, tvNewFilterWalkIn, tvNewFilterOpen, tvNewFilterOutStation, tvNewClearAllLbl;
    ImageView ivLFPostedToday, ivLFPostedLast7D, ivLFPostedLast15D, ivLFPostedLast30D, ivLFilterClosed, ivLFilterHot, ivLFilterWarm, ivLFilterCold, ivLFilterLost, ivLFilterNotInterest, ivLFilterNoResponse, ivLFilterTestDrivenTaken, ivLFilterWalkIn, ivLFilterNewOpen, ivLFilterOutStation;
    private HashMap<String, Integer> postedfollDateHashMap = new HashMap<String, Integer>();
    ;
    private String filterCategoryVal = "";
    ArrayList<String> selectedLeadStatus = new ArrayList<>();
    ApplyFilterInf applyFilterInf;
    SharedPreferences sharedPreferences;
    int leadStatusCount = 0;

    /* public RDMFilterActivity(ApplyFilterInf applyFilterInf) {
         this.applyFilterInf = applyFilterInf;
     }
 */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_r_d_m_filter);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            window.setStatusBarColor(Color.WHITE);
        }
        initViews();
    }

    private void initViews() {


        btnApplyFilter = findViewById(R.id.btnApplyFilter);
        btnCancelFilter = findViewById(R.id.btnCancelFilter);

        llPostedOrFollowUpDate = findViewById(R.id.lLayoutPostedDate);
        llNewFilterLeadStatus = findViewById(R.id.llNewFilterLeadStatus);
        lLayoutFollowUpDate = findViewById(R.id.lLayoutFollowUpDate);
        llLeadStatusAndCount = findViewById(R.id.llLeadStatusAndCount);
        llFollowUpdate = findViewById(R.id.llFollowUpdate);
        llPostedDate = findViewById(R.id.llPostedDate);

        btnSelectCustomDate = findViewById(R.id.btnSelectCustomDate);
        tvNewLeadFilterToday = findViewById(R.id.tvNewLeadFilterToday);
        tvNewLeadFilterLast7D = findViewById(R.id.tvNewLeadFilterLast7D);
        tvNewLeadFilterLast15D = findViewById(R.id.tvNewLeadFilterLast15D);
        tvNewLeadFilterLast30D = findViewById(R.id.tvNewLeadFilterLast30D);
        tvLFPostedDate = findViewById(R.id.tvLFPostedDate);
        tvLFPostedDateCount = findViewById(R.id.tvLFPostedDateCount);
        tvFollowUpDate = findViewById(R.id.tvFollowUpDate);
        tvFollowUpDateCount = findViewById(R.id.tvFollowUpDateCount);
        tvLeadStatus = findViewById(R.id.tvNewLeadsFilterLeadStatus);
        tvLeadStatusCount = findViewById(R.id.tvLeadStatusCount);
        tvDeleteSelectedDate = findViewById(R.id.tvDeleteSelectedDate);
        tvNewClearAllLbl = findViewById(R.id.tvNewClearAllLbl);

        tvSelectFCustDateLbl = findViewById(R.id.tvSelectFCustDateLbl);

        btnSelectFCustomDate = findViewById(R.id.btnSelectFCustomDate);

        tvFDeleteSelectedDate = findViewById(R.id.tvFDeleteSelectedDate);
        tvFQuickSelectLbl = findViewById(R.id.tvFQuickSelectLbl);
        tvFNewLeadFilterToday = findViewById(R.id.tvFNewLeadFilterToday);
        tvFNewLeadFilterLast7D = findViewById(R.id.tvFNewLeadFilterLast7D);
        tvFNewLeadFilterLast15D = findViewById(R.id.tvFNewLeadFilterLast15D);
        tvFNewLeadFilterLast30D = findViewById(R.id.tvFNewLeadFilterLast30D);

        ivFLFPostedToday = findViewById(R.id.ivFLFPostedToday);
        ivFLFPostedLast7D = findViewById(R.id.ivFLFPostedLast7D);
        ivFLFPostedLast15D = findViewById(R.id.ivFLFPostedLast15D);
        ivFLFPostedLast30D = findViewById(R.id.ivFLFPostedLast30D);


        ivLFPostedToday = findViewById(R.id.ivLFPostedToday);
        ivLFPostedLast7D = findViewById(R.id.ivLFPostedLast7D);
        ivLFPostedLast15D = findViewById(R.id.ivLFPostedLast15D);
        ivLFPostedLast30D = findViewById(R.id.ivLFPostedLast30D);

        tvNewFilterClosed = findViewById(R.id.tvNewFilterClosed);
        tvNewFilterHot = findViewById(R.id.tvNewFilterHot);
        tvNewFilterWarm = findViewById(R.id.tvNewFilterWarm);
        tvNewFilterCold = findViewById(R.id.tvNewFilterCold);
        tvNewFilterLost = findViewById(R.id.tvNewFilterLost);
        tvNewFilterNotInterested = findViewById(R.id.tvNewFilterNotInterested);
        tvNewFilterNoResponse = findViewById(R.id.tvNewFilterNoResponse);
        tvNewFilterTestDriveTaken = findViewById(R.id.tvNewFilterTestDriveTaken);
        tvNewFilterWalkIn = findViewById(R.id.tvNewFilterWalkIn);
        tvNewFilterOpen = findViewById(R.id.tvNewFilterOpen);
        tvNewFilterOutStation = findViewById(R.id.tvNewFilterOutStation);

        ivLFilterClosed = findViewById(R.id.ivLFilterClosed);
        ivLFilterHot = findViewById(R.id.ivLFilterHot);
        ivLFilterWarm = findViewById(R.id.ivLFilterWarm);
        ivLFilterCold = findViewById(R.id.ivLFilterCold);
        ivLFilterLost = findViewById(R.id.ivLFilterLost);
        ivLFilterNotInterest = findViewById(R.id.ivLFilterNotInterest);
        ivLFilterNoResponse = findViewById(R.id.ivLFilterNoResponse);
        ivLFilterTestDrivenTaken = findViewById(R.id.ivLFilterTestDrivenTaken);
        ivLFilterWalkIn = findViewById(R.id.ivLFilterWalkIn);
        ivLFilterNewOpen = findViewById(R.id.ivLFilterNewOpen);
        ivLFilterOutStation = findViewById(R.id.ivLFilterOutStation);

        llFilterClosed = findViewById(R.id.llFilterClosed);
        llFilterHot = findViewById(R.id.llFilterHot);
        llFilterWarm = findViewById(R.id.llFilterWarm);
        llFilterCold = findViewById(R.id.llFilterCold);
        llFilterLost = findViewById(R.id.llFilterLost);
        llFilterNotInterested = findViewById(R.id.llFilterNotInterested);
        llFilterNoResponse = findViewById(R.id.llFilterNoResponse);
        llFilterTestDriveTaken = findViewById(R.id.llFilterTestDriveTaken);
        llFilterWalkIn = findViewById(R.id.llFilterWalkIn);
        llFilterOpenNew = findViewById(R.id.llFilterOpenNew);
        llFilterOutstation = findViewById(R.id.llFilterOutstation);


        llPostedDate.setOnClickListener(this);
        llFollowUpdate.setOnClickListener(this);
        llLeadStatusAndCount.setOnClickListener(this);

        llPostedDate.setOnClickListener(this);
        llFollowUpdate.setOnClickListener(this);
        llLeadStatusAndCount.setOnClickListener(this);

        btnSelectCustomDate.setOnClickListener(this);
        btnSelectFCustomDate.setOnClickListener(this);

        tvNewLeadFilterToday.setOnClickListener(this);
        tvNewLeadFilterLast7D.setOnClickListener(this);
        tvNewLeadFilterLast15D.setOnClickListener(this);
        tvNewLeadFilterLast30D.setOnClickListener(this);

        tvFNewLeadFilterToday.setOnClickListener(this);
        tvFNewLeadFilterLast7D.setOnClickListener(this);
        tvFNewLeadFilterLast15D.setOnClickListener(this);
        tvFNewLeadFilterLast30D.setOnClickListener(this);


        tvDeleteSelectedDate.setOnClickListener(this);
        tvFDeleteSelectedDate.setOnClickListener(this);
        btnApplyFilter.setOnClickListener(this);

        llFilterClosed.setOnClickListener(this);
        llFilterHot.setOnClickListener(this);
        llFilterWarm.setOnClickListener(this);
        llFilterCold.setOnClickListener(this);
        llFilterLost.setOnClickListener(this);
        llFilterNotInterested.setOnClickListener(this);
        llFilterNoResponse.setOnClickListener(this);
        llFilterTestDriveTaken.setOnClickListener(this);
        llFilterWalkIn.setOnClickListener(this);
        llFilterOpenNew.setOnClickListener(this);
        llFilterOutstation.setOnClickListener(this);
        tvNewClearAllLbl.setOnClickListener(this);


        filterCategory = CommonMethods.getstringvaluefromkey(this, AppConstants.LEAD_FILTER_CATEGORY);
        filterCategoryVal = CommonMethods.getstringvaluefromkey(RDMFilterActivity.this, AppConstants.LEAD_FILTER_CATEGORY_VALUE);

        try {

            if (!CommonMethods.getstringvaluefromkey(this, AppConstants.LEAD_STATUS_COUNT).isEmpty())
                leadStatusCount = Integer.parseInt(CommonMethods.getstringvaluefromkey(this, AppConstants.LEAD_STATUS_COUNT));
            else
                leadStatusCount = 0;

            if (filterCategory.isEmpty()) {
                if (leadStatusCount > 0) {
                    isLeadStatusOnClick = true;
                    selectCategory();
                    postedfollDateHashMap = CommonMethods.readFromSP(this);
                    selectedLeadStatus.clear();
                    for (Map.Entry me : postedfollDateHashMap.entrySet()) {
                        highLightLeadStatus((Integer) me.getValue());
                        selectedLeadStatus.add(String.valueOf(me.getKey()));
                        Log.i("FilterActivity", "initViews: " + (Integer) me.getValue());
                    }
                    if (postedfollDateHashMap.containsKey("open")) {
                        int leadStatusCount = postedfollDateHashMap.size() - 1;
                        tvLeadStatusCount.setText(leadStatusCount);
                    } else {
                        tvLeadStatusCount.setText(Integer.toString(postedfollDateHashMap.size()));
                    }
                } else {
                    filterCategory = AppConstants.POSTED_DATE;
                    filterCategoryVal = AppConstants.TODAY;
                    highlightSelectedItem(filterCategoryVal);
                    Toast.makeText(this, "No filter Applied", Toast.LENGTH_SHORT).show();
                }
            } else {
                //highlight given field category & field value
                selectCategory();
                highlightSelectedItem(filterCategoryVal);
                if (leadStatusCount > 0) {
                    tvLeadStatusCount.setVisibility(View.VISIBLE);
                    tvLeadStatusCount.setBackgroundColor(getResources().getColor(R.color.transparent));
                    tvLeadStatusCount.setTextColor(getResources().getColor(R.color.black));
                    tvLeadStatusCount.setText(CommonMethods.getstringvaluefromkey(RDMFilterActivity.this, AppConstants.LEAD_STATUS_COUNT));
                    postedfollDateHashMap = CommonMethods.readFromSP(this);
                    selectedLeadStatus.clear();

                    postedfollDateHashMap = CommonMethods.readFromSP(this);
                    selectedLeadStatus.clear();
                    for (Map.Entry me : postedfollDateHashMap.entrySet()) {
                        highLightLeadStatus((Integer) me.getValue());
                        selectedLeadStatus.add(String.valueOf(me.getKey()));
                        Log.i("FilterActivity", "initViews: " + (Integer) me.getValue());
                    }
                    if (postedfollDateHashMap.containsKey("open")) {
                        int leadStatusCount = postedfollDateHashMap.size() - 1;
                        tvLeadStatusCount.setText(leadStatusCount);
                    } else {
                        tvLeadStatusCount.setText(Integer.toString(postedfollDateHashMap.size()));
                    }
                }
            }
        } catch (
                Exception exception) {
            exception.printStackTrace();
        }


        btnCancelFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strMessage = "";
                finish();
            }
        });


    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.llPostedDate:
                    filterCategory = AppConstants.POSTED_DATE;
                    isLeadStatusOnClick = false;
                    selectCategory();
                    break;

                case R.id.llFollowUpdate:
                    filterCategory = AppConstants.FOLLOW_DATE;
                    isLeadStatusOnClick = false;
                    selectCategory();
                    break;

                case R.id.llLeadStatusAndCount:
                    isLeadStatusOnClick = true;
                    selectCategory();
                    break;

                case R.id.tvNewLeadFilterToday:
                    filterCategory = AppConstants.POSTED_DATE;
                    if (tvNewLeadFilterToday.getCurrentTextColor() == getColor(R.color.edit_text_blue)) {
                        clearFollowUpData();
                        clearPostedDateData();
                        tvFollowUpDateCount.setVisibility(View.GONE);
                    } else {
                        highlightSelectedItem("TODAY");
                    }
                    break;
                case R.id.tvFNewLeadFilterToday:
                    filterCategory = AppConstants.FOLLOW_DATE;
                    if (tvFNewLeadFilterToday.getCurrentTextColor() == getColor(R.color.edit_text_blue)) {
                        clearPostedDateData();
                        clearFollowUpData();
                        tvLFPostedDateCount.setVisibility(View.GONE);
                    } else {
                        highlightSelectedItem(AppConstants.TODAY);
                    }
                    break;
                case R.id.tvNewLeadFilterLast7D:
                    filterCategory = AppConstants.POSTED_DATE;
                    if (tvNewLeadFilterLast7D.getCurrentTextColor() == getColor(R.color.edit_text_blue)) {
                        clearPostedDateData();
                        clearFollowUpData();
                        tvLFPostedDateCount.setVisibility(View.GONE);
                        tvFollowUpDateCount.setVisibility(View.GONE);
                    } else {
                        highlightSelectedItem("LAST_SEVEN_DAYS");
                    }
                    break;
                case R.id.tvFNewLeadFilterLast7D:
                    filterCategory = AppConstants.FOLLOW_DATE;
                    if (tvFNewLeadFilterLast7D.getCurrentTextColor() == getColor(R.color.edit_text_blue)) {
                        clearPostedDateData();
                        clearFollowUpData();
                        tvLFPostedDateCount.setVisibility(View.GONE);
                        tvFollowUpDateCount.setVisibility(View.GONE);
                    } else {
                        highlightSelectedItem("LAST_SEVEN_DAYS");
                    }
                    break;
                case R.id.tvNewLeadFilterLast15D:
                    filterCategory = AppConstants.POSTED_DATE;
                    if (tvNewLeadFilterLast15D.getCurrentTextColor() == getColor(R.color.edit_text_blue)) {
                        clearPostedDateData();
                        clearFollowUpData();
                        tvLFPostedDateCount.setVisibility(View.GONE);
                        tvFollowUpDateCount.setVisibility(View.GONE);
                    } else {
                        highlightSelectedItem("LAST_FIFTEEN_DAYS");
                    }
                    break;
                case R.id.tvFNewLeadFilterLast15D:
                    filterCategory = AppConstants.FOLLOW_DATE;
                    if (tvFNewLeadFilterLast15D.getCurrentTextColor() == getColor(R.color.edit_text_blue)) {
                        clearPostedDateData();
                        clearFollowUpData();
                        tvLFPostedDateCount.setVisibility(View.GONE);
                        tvFollowUpDateCount.setVisibility(View.GONE);
                    } else {
                        highlightSelectedItem("LAST_FIFTEEN_DAYS");
                    }
                    break;
                case R.id.tvNewLeadFilterLast30D:
                    filterCategory = AppConstants.POSTED_DATE;
                    if (tvNewLeadFilterLast30D.getCurrentTextColor() == getColor(R.color.edit_text_blue)) {
                        clearPostedDateData();
                        clearFollowUpData();
                        tvLFPostedDateCount.setVisibility(View.GONE);
                        tvFollowUpDateCount.setVisibility(View.GONE);
                    } else {
                        highlightSelectedItem("LAST_THIRTY_DAYS");
                    }
                    break;
                case R.id.tvFNewLeadFilterLast30D:
                    filterCategory = AppConstants.FOLLOW_DATE;
                    if (tvFNewLeadFilterLast30D.getCurrentTextColor() == getColor(R.color.edit_text_blue)) {
                        clearPostedDateData();
                        clearFollowUpData();
                        tvLFPostedDateCount.setVisibility(View.GONE);
                        tvFollowUpDateCount.setVisibility(View.GONE);
                    } else {
                        highlightSelectedItem("LAST_THIRTY_DAYS");
                    }
                    break;
                case R.id.llFilterClosed:
                    if (tvNewFilterClosed.getCurrentTextColor() == getColor(R.color.edit_text_blue)) {
                        postedfollDateHashMap.remove("closed");
                        selectedLeadStatus.remove("closed");
                        tvNewFilterClosed.setTextColor(getResources().getColor(R.color.detailed_dark));
                        ivLFilterClosed.setImageResource(R.drawable.ic_new_filter_rounded_check);
                        enableFilterCount(AppConstants.LEAD_STATUS_COUNT);
                    } else {
                        postedfollDateHashMap.put("closed", R.id.llFilterClosed);
                        selectedLeadStatus.add("closed");
                        highLightLeadStatus(R.id.llFilterClosed);
                    }
                    break;
                case R.id.llFilterHot:
                    if (tvNewFilterHot.getCurrentTextColor() == getColor(R.color.edit_text_blue)) {
                        postedfollDateHashMap.remove("hot");
                        selectedLeadStatus.remove("hot");
                        tvNewFilterHot.setTextColor(getResources().getColor(R.color.detailed_dark));
                        ivLFilterHot.setImageResource(R.drawable.ic_new_filter_rounded_check);
                        enableFilterCount(AppConstants.LEAD_STATUS_COUNT);
                    } else {
                        postedfollDateHashMap.put("hot", R.id.llFilterHot);
                        selectedLeadStatus.add("hot");
                        highLightLeadStatus(R.id.llFilterHot);
                    }
                    break;
                case R.id.llFilterWarm:
                    if (tvNewFilterWarm.getCurrentTextColor() == getColor(R.color.edit_text_blue)) {
                        postedfollDateHashMap.remove("warm");
                        selectedLeadStatus.remove("warm");
                        tvNewFilterWarm.setTextColor(getResources().getColor(R.color.detailed_dark));
                        ivLFilterWarm.setImageResource(R.drawable.ic_new_filter_rounded_check);
                        enableFilterCount(AppConstants.LEAD_STATUS_COUNT);
                    } else {
                        postedfollDateHashMap.put("warm", R.id.llFilterWarm);
                        selectedLeadStatus.add("warm");
                        highLightLeadStatus(R.id.llFilterWarm);
                    }
                    break;
                case R.id.llFilterCold:
                    if (tvNewFilterCold.getCurrentTextColor() == getColor(R.color.edit_text_blue)) {
                        postedfollDateHashMap.remove("cold");
                        selectedLeadStatus.remove("cold");
                        tvNewFilterCold.setTextColor(getResources().getColor(R.color.detailed_dark));
                        ivLFilterCold.setImageResource(R.drawable.ic_new_filter_rounded_check);
                        enableFilterCount(AppConstants.LEAD_STATUS_COUNT);
                    } else {
                        postedfollDateHashMap.put("cold", R.id.llFilterCold);
                        selectedLeadStatus.add("cold");
                        highLightLeadStatus(R.id.llFilterCold);
                    }
                    break;
                case R.id.llFilterLost:
                    if (tvNewFilterLost.getCurrentTextColor() == getColor(R.color.edit_text_blue)) {
                        postedfollDateHashMap.remove("lost");
                        selectedLeadStatus.remove("lost");
                        tvNewFilterLost.setTextColor(getResources().getColor(R.color.detailed_dark));
                        ivLFilterLost.setImageResource(R.drawable.ic_new_filter_rounded_check);
                        enableFilterCount(AppConstants.LEAD_STATUS_COUNT);
                    } else {
                        postedfollDateHashMap.put("lost", R.id.llFilterLost);
                        selectedLeadStatus.add("lost");
                        highLightLeadStatus(R.id.llFilterLost);
                    }
                    break;
                case R.id.llFilterNotInterested:
                    if (tvNewFilterNotInterested.getCurrentTextColor() == getColor(R.color.edit_text_blue)) {
                        postedfollDateHashMap.remove("notinterested");
                        selectedLeadStatus.remove("notinterested");
                        tvNewFilterNotInterested.setTextColor(getResources().getColor(R.color.detailed_dark));
                        ivLFilterNotInterest.setImageResource(R.drawable.ic_new_filter_rounded_check);
                        enableFilterCount(AppConstants.LEAD_STATUS_COUNT);
                    } else {
                        postedfollDateHashMap.put("notinterested", R.id.llFilterNotInterested);
                        selectedLeadStatus.add("notinterested");
                        highLightLeadStatus(R.id.llFilterNotInterested);

                    }
                    break;
                case R.id.llFilterNoResponse:

                    if (tvNewFilterNoResponse.getCurrentTextColor() == getColor(R.color.edit_text_blue)) {
                        postedfollDateHashMap.remove("noresponse");
                        selectedLeadStatus.remove("noresponse");
                        tvNewFilterNoResponse.setTextColor(getResources().getColor(R.color.detailed_dark));
                        ivLFilterNoResponse.setImageResource(R.drawable.ic_new_filter_rounded_check);
                        enableFilterCount(AppConstants.LEAD_STATUS_COUNT);
                    } else {
                        postedfollDateHashMap.put("noresponse", R.id.llFilterNoResponse);
                        selectedLeadStatus.add("noresponse");
                        highLightLeadStatus(R.id.llFilterNoResponse);
                    }
                    break;
                case R.id.llFilterTestDriveTaken:
                    if (tvNewFilterTestDriveTaken.getCurrentTextColor() == getColor(R.color.edit_text_blue)) {
                        postedfollDateHashMap.remove("testdriventaken");
                        selectedLeadStatus.remove("testdriventaken");
                        tvNewFilterTestDriveTaken.setTextColor(getResources().getColor(R.color.detailed_dark));
                        ivLFilterTestDrivenTaken.setImageResource(R.drawable.ic_new_filter_rounded_check);
                        enableFilterCount(AppConstants.LEAD_STATUS_COUNT);
                    } else {
                        postedfollDateHashMap.put("testdriventaken", R.id.llFilterTestDriveTaken);
                        selectedLeadStatus.add("testdriventaken");
                        highLightLeadStatus(R.id.llFilterTestDriveTaken);

                    }
                    break;
                case R.id.llFilterWalkIn:
                    if (tvNewFilterWalkIn.getCurrentTextColor() == getColor(R.color.edit_text_blue)) {
                        postedfollDateHashMap.remove("walkin");
                        selectedLeadStatus.remove("walkin");
                        tvNewFilterWalkIn.setTextColor(getResources().getColor(R.color.detailed_dark));
                        ivLFilterWalkIn.setImageResource(R.drawable.ic_new_filter_rounded_check);
                        enableFilterCount(AppConstants.LEAD_STATUS_COUNT);
                    } else {
                        postedfollDateHashMap.put("walkin", R.id.llFilterWalkIn);
                        selectedLeadStatus.add("walkin");
                        highLightLeadStatus(R.id.llFilterWalkIn);

                    }
                    break;
                case R.id.llFilterOpenNew:
                    if (tvNewFilterOpen.getCurrentTextColor() == getColor(R.color.edit_text_blue)) {
                        postedfollDateHashMap.remove("open");
                        postedfollDateHashMap.remove("new");
                        selectedLeadStatus.remove("open");
                        selectedLeadStatus.remove("new");
                        tvNewFilterOpen.setTextColor(getResources().getColor(R.color.detailed_dark));
                        ivLFilterNewOpen.setImageResource(R.drawable.ic_new_filter_rounded_check);
                        enableFilterCount(AppConstants.LEAD_STATUS_COUNT);
                    } else {
                        postedfollDateHashMap.put("open", R.id.llFilterOpenNew);
                        postedfollDateHashMap.put("new", R.id.llFilterOpenNew);
                        selectedLeadStatus.add("open");
                        selectedLeadStatus.add("new");
                        highLightLeadStatus(R.id.llFilterOpenNew);

                    }
                    break;
                case R.id.llFilterOutstation:
                    if (tvNewFilterOutStation.getCurrentTextColor() == getColor(R.color.edit_text_blue)) {
                        postedfollDateHashMap.remove("outstation");
                        selectedLeadStatus.remove("outstation");
                        tvNewFilterOutStation.setTextColor(getResources().getColor(R.color.detailed_dark));
                        ivLFilterOutStation.setImageResource(R.drawable.ic_new_filter_rounded_check);
                        enableFilterCount(AppConstants.LEAD_STATUS_COUNT);
                    } else {
                        postedfollDateHashMap.put("outstation", R.id.llFilterOutstation);
                        selectedLeadStatus.add("outstation");
                        highLightLeadStatus(R.id.llFilterOutstation);

                    }
                    break;
                case R.id.tvDeleteSelectedDate:
                    tvLFPostedDateCount.setVisibility(View.GONE);
                    clearCustomDate(btnSelectCustomDate, tvDeleteSelectedDate);
                    break;
                case R.id.tvFDeleteSelectedDate:
                    clearCustomDate(btnSelectFCustomDate, tvFDeleteSelectedDate);
                    tvFollowUpDateCount.setVisibility(View.GONE);
                    break;
                case R.id.btnSelectCustomDate:
                case R.id.btnSelectFCustomDate:
                    try {
                        setSelectedCustomDate();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                    break;
                case R.id.tvNewClearAllLbl:
                    isClearAllEnabled = true;
                    clearUI();
                    break;
                case R.id.btnApplyFilter:
                    try {
                        btnApplyFilter.setBackground(getDrawable(R.drawable.rounded_button_background_blue));

                        if (isClearAllEnabled) {
                            clearValues();
                            CommonMethods.setvalueAgainstKey(RDMFilterActivity.this, AppConstants.LEAD_FILTER_CATEGORY, filterCategory);
                            CommonMethods.setvalueAgainstKey(RDMFilterActivity.this, AppConstants.LEAD_FILTER_CATEGORY_VALUE, filterCategoryVal);
                            CommonMethods.clearLeadStatusMap(this);
                            CommonMethods.insertToSP(this, postedfollDateHashMap);
                            CommonMethods.setvalueAgainstKey(RDMFilterActivity.this, AppConstants.LEAD_STATUS_COUNT, "");

                            Intent pushFilterString = new Intent("Filter_Tag");
                            pushFilterString.putExtra("filterCategory", filterCategory);
                            pushFilterString.putExtra("fromDate", fromDate);
                            pushFilterString.putExtra("toDate", toDate);
                            pushFilterString.putExtra("LeadStatusFilter", selectedLeadStatus);

                            LocalBroadcastManager.getInstance(RDMFilterActivity.this).sendBroadcast(pushFilterString);
                        } else {

                            if (filterCategory.isEmpty() && filterCategoryVal.isEmpty() && postedfollDateHashMap.isEmpty()) {
                                Toast.makeText(RDMFilterActivity.this, "Please select any filter to apply", Toast.LENGTH_SHORT);
                            } else {
                                CommonMethods.setvalueAgainstKey(RDMFilterActivity.this, AppConstants.LEAD_FILTER_CATEGORY, filterCategory);
                                CommonMethods.setvalueAgainstKey(RDMFilterActivity.this, AppConstants.LEAD_FILTER_CATEGORY_VALUE, filterCategoryVal);
                                CommonMethods.clearLeadStatusMap(this);
                                CommonMethods.insertToSP(RDMFilterActivity.this,postedfollDateHashMap);
                                CommonMethods.setvalueAgainstKey(RDMFilterActivity.this, AppConstants.LEAD_STATUS_COUNT, String.valueOf(selectedLeadStatus.size()));

                                Intent pushFilterString = new Intent("Filter_Tag");
                                pushFilterString.putExtra("filterCategory", filterCategory);
                                pushFilterString.putExtra("fromDate", fromDate);
                                pushFilterString.putExtra("toDate", toDate);
                                pushFilterString.putExtra("LeadStatusFilter", selectedLeadStatus);

                                LocalBroadcastManager.getInstance(RDMFilterActivity.this).sendBroadcast(pushFilterString);

                            }
                        }

                        finish();

                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                    break;

            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    private void applyFilterCatChanges() {
        filterCategoryVal = AppConstants.LEAD_FILTER_CUSTOM_DATE;
        CommonMethods.setvalueAgainstKey(this, AppConstants.LEAD_FILTER_CUSTOM_DATE, fromDate + "to" + toDate);
        if (filterCategory.equalsIgnoreCase(AppConstants.POSTED_DATE)) {
            btnSelectCustomDate.setText(fromDate + " to " + toDate);
            btnSelectCustomDate.setTextColor(getResources().getColor(R.color.edit_text_blue));
            btnSelectCustomDate.setBackground(getResources().getDrawable(R.drawable.navy_blue_outline));
            tvDeleteSelectedDate.setVisibility(View.VISIBLE);
            tvDeleteSelectedDate.setClickable(true);
            tvDeleteSelectedDate.setFocusable(true);
            enableFilterCount(AppConstants.POSTED_DATE);                   // To set Posted date count and hide follow up date count
            clearCustomDate(btnSelectFCustomDate, tvFDeleteSelectedDate);  // To clear follow-up date custom date data
            clearFollowUpData();                                           // To clear follow up date Quick option selection
            clearPostedDateData();
            btnApplyFilter.setBackground(getDrawable(R.drawable.rounded_button_background_blue));
        } else if (filterCategory.equalsIgnoreCase(AppConstants.FOLLOW_DATE)) {
            btnSelectFCustomDate.setText(fromDate + " to " + toDate);
            btnSelectFCustomDate.setTextColor(getResources().getColor(R.color.edit_text_blue));
            btnSelectFCustomDate.setBackground(getResources().getDrawable(R.drawable.navy_blue_outline));
            tvFDeleteSelectedDate.setVisibility(View.VISIBLE);
            tvFDeleteSelectedDate.setClickable(true);
            tvFDeleteSelectedDate.setFocusable(true);
            enableFilterCount(AppConstants.FOLLOW_DATE);                   // To set Posted date count and hide follow up date count
            clearCustomDate(btnSelectCustomDate, tvDeleteSelectedDate);  // To clear follow-up date custom date data
            clearFollowUpData();                                           // To clear follow up date Quick option selection
            clearPostedDateData();
            btnApplyFilter.setBackground(getDrawable(R.drawable.rounded_button_background_blue));
        }
    }


    private void clearCustomDate(Button button, TextView textView) {

        button.setTextColor(getResources().getColor(R.color.cust_grey));
        button.setBackground(getResources().getDrawable(R.drawable.grey_box_1dp));
        button.setText(getText(R.string.tap_here_to_select_custom_date));
        textView.setVisibility(View.GONE);
        textView.setFocusable(false);
        textView.setClickable(false);
        if (selectedLeadStatus.isEmpty()) {
            btnApplyFilter.setBackground(getDrawable(R.drawable.rounded_button_background_dark_grey_disabled));
        }
    }

    private void highlightSelectedItem(String field) {

        if (field.equalsIgnoreCase(AppConstants.TODAY)) {
            if (filterCategory.equalsIgnoreCase(AppConstants.POSTED_DATE)) {

                clearPostedDateData();
                clearFollowUpData();
                clearCustomDate(btnSelectCustomDate, tvDeleteSelectedDate);
                clearCustomDate(btnSelectFCustomDate, tvFDeleteSelectedDate);
                tvNewLeadFilterToday.setTextColor(getResources().getColor(R.color.edit_text_blue));
                ivLFPostedToday.setImageResource(R.drawable.ic_new_active_rounded_cb);
                enableFilterCount(AppConstants.POSTED_DATE);

            } else if (filterCategory.equalsIgnoreCase(AppConstants.FOLLOW_DATE)) {
                clearPostedDateData();
                clearFollowUpData();
                clearCustomDate(btnSelectCustomDate, tvDeleteSelectedDate);
                clearCustomDate(btnSelectFCustomDate, tvFDeleteSelectedDate);
                tvFNewLeadFilterToday.setTextColor(getResources().getColor(R.color.edit_text_blue));
                ivFLFPostedToday.setImageResource(R.drawable.ic_new_active_rounded_cb);
                enableFilterCount(AppConstants.FOLLOW_DATE);

            }
            filterCategoryVal = "";
            filterCategoryVal = AppConstants.TODAY;
            fromDate = CommonMethods.getTodayDate();
            toDate = fromDate;
            btnApplyFilter.setBackground(getDrawable(R.drawable.rounded_button_background_blue));


        } else if (field.equalsIgnoreCase("LAST_SEVEN_DAYS")) {

            if (filterCategory.equalsIgnoreCase(AppConstants.POSTED_DATE)) {
                clearPostedDateData();
                clearFollowUpData();
                clearCustomDate(btnSelectCustomDate, tvDeleteSelectedDate);
                clearCustomDate(btnSelectFCustomDate, tvFDeleteSelectedDate);

                tvNewLeadFilterLast7D.setTextColor(getResources().getColor(R.color.edit_text_blue));

                ivLFPostedLast7D.setImageResource(R.drawable.ic_new_active_rounded_cb);


                enableFilterCount(AppConstants.POSTED_DATE);

            } else if (filterCategory.equalsIgnoreCase(AppConstants.FOLLOW_DATE)) {

                clearPostedDateData();
                clearFollowUpData();
                clearCustomDate(btnSelectCustomDate, tvDeleteSelectedDate);
                clearCustomDate(btnSelectFCustomDate, tvFDeleteSelectedDate);

                tvFNewLeadFilterLast7D.setTextColor(getResources().getColor(R.color.edit_text_blue));

                ivFLFPostedLast7D.setImageResource(R.drawable.ic_new_active_rounded_cb);

                enableFilterCount(AppConstants.FOLLOW_DATE);

            }

            filterCategoryVal = AppConstants.LAST_SEVEN_DAYS;
            fromDate = CommonMethods.getLastSevenDays().substring(0, 10);
            toDate = CommonMethods.getLastSevenDays().substring(11);
            btnApplyFilter.setBackground(getDrawable(R.drawable.rounded_button_background_blue));

        } else if (field.equalsIgnoreCase("LAST_FIFTEEN_DAYS")) {

            if (filterCategory.equalsIgnoreCase(AppConstants.POSTED_DATE)) {

                clearPostedDateData();
                clearFollowUpData();
                clearCustomDate(btnSelectCustomDate, tvDeleteSelectedDate);
                clearCustomDate(btnSelectFCustomDate, tvFDeleteSelectedDate);

                tvNewLeadFilterLast15D.setTextColor(getResources().getColor(R.color.edit_text_blue));

                ivLFPostedLast15D.setImageResource(R.drawable.ic_new_active_rounded_cb);

                enableFilterCount(AppConstants.POSTED_DATE);


            } else if (filterCategory.equalsIgnoreCase(AppConstants.FOLLOW_DATE)) {

                clearPostedDateData();
                clearFollowUpData();
                clearCustomDate(btnSelectCustomDate, tvDeleteSelectedDate);
                clearCustomDate(btnSelectFCustomDate, tvFDeleteSelectedDate);

                tvFNewLeadFilterLast15D.setTextColor(getResources().getColor(R.color.edit_text_blue));

                ivFLFPostedLast15D.setImageResource(R.drawable.ic_new_active_rounded_cb);

                enableFilterCount(AppConstants.FOLLOW_DATE);

            }
            filterCategoryVal = AppConstants.LAST_FIFTEEN_DAYS;
            fromDate = CommonMethods.getLastFifteenDays().substring(0, 10);
            toDate = CommonMethods.getLastFifteenDays().substring(11);
            btnApplyFilter.setBackground(getDrawable(R.drawable.rounded_button_background_blue));


        } else if (field.equalsIgnoreCase("LAST_THIRTY_DAYS")) {

            if (filterCategory.equalsIgnoreCase(AppConstants.POSTED_DATE)) {

                clearPostedDateData();
                clearFollowUpData();
                clearCustomDate(btnSelectCustomDate, tvDeleteSelectedDate);
                clearCustomDate(btnSelectFCustomDate, tvFDeleteSelectedDate);

                tvNewLeadFilterLast30D.setTextColor(getResources().getColor(R.color.edit_text_blue));

                ivLFPostedLast30D.setImageResource(R.drawable.ic_new_active_rounded_cb);

                enableFilterCount(AppConstants.POSTED_DATE);

            } else if (filterCategory.equalsIgnoreCase(AppConstants.FOLLOW_DATE)) {
                clearPostedDateData();
                clearFollowUpData();
                clearCustomDate(btnSelectCustomDate, tvDeleteSelectedDate);
                clearCustomDate(btnSelectFCustomDate, tvFDeleteSelectedDate);

                tvFNewLeadFilterLast30D.setTextColor(getResources().getColor(R.color.edit_text_blue));

                ivFLFPostedLast30D.setImageResource(R.drawable.ic_new_active_rounded_cb);

                enableFilterCount(AppConstants.FOLLOW_DATE);

            }

            filterCategoryVal = AppConstants.LAST_THIRTY_DAYS;
            fromDate = CommonMethods.getLastThirtyDays().substring(0, 10);
            toDate = CommonMethods.getLastThirtyDays().substring(11);
            btnApplyFilter.setBackground(getDrawable(R.drawable.rounded_button_background_blue));

        } else if (field.equalsIgnoreCase(AppConstants.LEAD_FILTER_CUSTOM_DATE)) {
            customDate = CommonMethods.getstringvaluefromkey(this, AppConstants.LEAD_FILTER_CUSTOM_DATE);
            fromDate = customDate.substring(0, 10);
            toDate = customDate.substring(12);
            applyFilterCatChanges();
        }

    }

    private void clearFollowUpData() {

        tvFNewLeadFilterToday.setTextColor(getResources().getColor(R.color.detailed_dark));
        tvFNewLeadFilterLast7D.setTextColor(getResources().getColor(R.color.detailed_dark));
        tvFNewLeadFilterLast15D.setTextColor(getResources().getColor(R.color.detailed_dark));
        tvFNewLeadFilterLast30D.setTextColor(getResources().getColor(R.color.detailed_dark));

        ivFLFPostedToday.setImageResource(R.drawable.ic_new_filter_rounded_check);
        ivFLFPostedLast7D.setImageResource(R.drawable.ic_new_filter_rounded_check);
        ivFLFPostedLast15D.setImageResource(R.drawable.ic_new_filter_rounded_check);
        ivFLFPostedLast30D.setImageResource(R.drawable.ic_new_filter_rounded_check);

    }

    private void clearPostedDateData() {
        tvNewLeadFilterToday.setTextColor(getResources().getColor(R.color.detailed_dark));
        tvNewLeadFilterLast7D.setTextColor(getResources().getColor(R.color.detailed_dark));
        tvNewLeadFilterLast15D.setTextColor(getResources().getColor(R.color.detailed_dark));
        tvNewLeadFilterLast30D.setTextColor(getResources().getColor(R.color.detailed_dark));

        ivLFPostedToday.setImageResource(R.drawable.ic_new_filter_rounded_check);
        ivLFPostedLast7D.setImageResource(R.drawable.ic_new_filter_rounded_check);
        ivLFPostedLast15D.setImageResource(R.drawable.ic_new_filter_rounded_check);
        ivLFPostedLast30D.setImageResource(R.drawable.ic_new_filter_rounded_check);

    }


    private void enableFilterCount(String tag) {
        if (tag.equalsIgnoreCase(AppConstants.POSTED_DATE)) {

            tvLFPostedDateCount.setVisibility(View.VISIBLE);
            tvLFPostedDateCount.setTextColor(getResources().getColor(R.color.white));
            tvLFPostedDateCount.setText("1");
            tvFollowUpDateCount.setVisibility(View.GONE);
            tvLeadStatusCount.setTextColor(getResources().getColor(R.color.black));

        } else if (tag.equalsIgnoreCase(AppConstants.FOLLOW_DATE)) {
            tvLFPostedDateCount.setVisibility(View.GONE);
            tvFollowUpDateCount.setVisibility(View.VISIBLE);
            tvFollowUpDateCount.setText("1");
            tvFollowUpDateCount.setTextColor(getResources().getColor(R.color.white));
            tvLeadStatusCount.setTextColor(getResources().getColor(R.color.black));
        }
        if (tag.equalsIgnoreCase(AppConstants.LEAD_STATUS_COUNT)) {
            if (isLeadStatusOnClick) {
                if (postedfollDateHashMap.size() > 0) {
                    tvLeadStatusCount.setVisibility(View.VISIBLE);
                    tvLFPostedDateCount.setTextColor(getResources().getColor(R.color.black));
                    tvFollowUpDateCount.setTextColor(getResources().getColor(R.color.black));
                    tvLeadStatusCount.setTextColor(getResources().getColor(R.color.white));
                    int leadCount = postedfollDateHashMap.size();
                    if (selectedLeadStatus.contains("open")) {
                        leadCount = leadCount - 1;
                        tvLeadStatusCount.setText("" + leadCount);

                    } else {
                        tvLeadStatusCount.setText("" + leadCount);
                    }
                } else {
                    tvLeadStatusCount.setVisibility(View.GONE);
                    tvLFPostedDateCount.setTextColor(getResources().getColor(R.color.black));
                    tvFollowUpDateCount.setTextColor(getResources().getColor(R.color.black));
                }

            } else {

                tvLeadStatusCount.setVisibility(View.VISIBLE);
                tvLeadStatusCount.setText("" + postedfollDateHashMap.size());
                tvLeadStatusCount.setTextColor(getResources().getColor(R.color.black));
            }

        }
    }

    private void isOpenIncluded() {


    }

    private void highLightLeadStatus(int linearLayout) {

        enableFilterCount(AppConstants.LEAD_STATUS_COUNT);
        btnApplyFilter.setBackground(getDrawable(R.drawable.rounded_button_background_blue));

        if (linearLayout == R.id.llFilterClosed) {
            tvNewFilterClosed.setTextColor(getResources().getColor(R.color.edit_text_blue));
            ivLFilterClosed.setImageResource(R.drawable.ic_new_active_rounded_cb);
            enableFilterCount(AppConstants.LEAD_STATUS_COUNT);
        } else if (linearLayout == R.id.llFilterHot) {
            tvNewFilterHot.setTextColor(getResources().getColor(R.color.edit_text_blue));
            ivLFilterHot.setImageResource(R.drawable.ic_new_active_rounded_cb);
        } else if (linearLayout == R.id.llFilterWarm) {
            tvNewFilterWarm.setTextColor(getResources().getColor(R.color.edit_text_blue));
            ivLFilterWarm.setImageResource(R.drawable.ic_new_active_rounded_cb);
        } else if (linearLayout == R.id.llFilterCold) {
            tvNewFilterCold.setTextColor(getResources().getColor(R.color.edit_text_blue));
            ivLFilterCold.setImageResource(R.drawable.ic_new_active_rounded_cb);
        } else if (linearLayout == R.id.llFilterLost) {
            tvNewFilterLost.setTextColor(getResources().getColor(R.color.edit_text_blue));
            ivLFilterLost.setImageResource(R.drawable.ic_new_active_rounded_cb);
        } else if (linearLayout == R.id.llFilterNotInterested) {
            tvNewFilterNotInterested.setTextColor(getResources().getColor(R.color.edit_text_blue));
            ivLFilterNotInterest.setImageResource(R.drawable.ic_new_active_rounded_cb);
        } else if (linearLayout == R.id.llFilterNoResponse) {
            tvNewFilterNoResponse.setTextColor(getResources().getColor(R.color.edit_text_blue));
            ivLFilterNoResponse.setImageResource(R.drawable.ic_new_active_rounded_cb);
        } else if (linearLayout == R.id.llFilterTestDriveTaken) {
            tvNewFilterTestDriveTaken.setTextColor(getResources().getColor(R.color.edit_text_blue));
            ivLFilterTestDrivenTaken.setImageResource(R.drawable.ic_new_active_rounded_cb);
        } else if (linearLayout == R.id.llFilterWalkIn) {
            tvNewFilterWalkIn.setTextColor(getResources().getColor(R.color.edit_text_blue));
            ivLFilterWalkIn.setImageResource(R.drawable.ic_new_active_rounded_cb);
        } else if (linearLayout == R.id.llFilterOpenNew) {
            tvNewFilterOpen.setTextColor(getResources().getColor(R.color.edit_text_blue));
            ivLFilterNewOpen.setImageResource(R.drawable.ic_new_active_rounded_cb);
        } else if (linearLayout == R.id.llFilterOutstation) {
            tvNewFilterOutStation.setTextColor(getResources().getColor(R.color.edit_text_blue));
            ivLFilterOutStation.setImageResource(R.drawable.ic_new_active_rounded_cb);
        }

    }


    private void clearValues() {
        customDate = "";
        fromDate = "";
        toDate = "";
        filterCategory = "";
        filterCategoryVal = "";
        postedfollDateHashMap.clear();
        selectedLeadStatus.clear();
    }

    private void clearUI() {

        // Custom Date Layout
        btnSelectCustomDate.setText(getString(R.string.tap_here_to_select_custom_date));
        llNewFilterLeadStatus.setVisibility(View.GONE);

        // Posted Date quick link buttons
        tvNewLeadFilterToday.setTextColor(getColor(R.color.detailed_dark));
        tvNewLeadFilterLast7D.setTextColor(getColor(R.color.detailed_dark));
        tvNewLeadFilterLast15D.setTextColor(getColor(R.color.detailed_dark));
        tvNewLeadFilterLast30D.setTextColor(getColor(R.color.detailed_dark));

        tvFNewLeadFilterToday.setTextColor(getColor(R.color.detailed_dark));
        tvFNewLeadFilterLast7D.setTextColor(getColor(R.color.detailed_dark));
        tvFNewLeadFilterLast15D.setTextColor(getColor(R.color.detailed_dark));
        tvFNewLeadFilterLast30D.setTextColor(getColor(R.color.detailed_dark));


        //Posted Date quick link ImageViews
        ivLFPostedToday.setImageResource(R.drawable.ic_new_filter_rounded_check);
        ivLFPostedLast7D.setImageResource(R.drawable.ic_new_filter_rounded_check);
        ivLFPostedLast15D.setImageResource(R.drawable.ic_new_filter_rounded_check);
        ivLFPostedLast30D.setImageResource(R.drawable.ic_new_filter_rounded_check);

        ivFLFPostedToday.setImageResource(R.drawable.ic_new_filter_rounded_check);
        ivFLFPostedLast7D.setImageResource(R.drawable.ic_new_filter_rounded_check);
        ivFLFPostedLast15D.setImageResource(R.drawable.ic_new_filter_rounded_check);
        ivFLFPostedLast30D.setImageResource(R.drawable.ic_new_filter_rounded_check);

        //Lead Status TextViews

        tvNewFilterClosed.setTextColor(getColor(R.color.detailed_dark));
        tvNewFilterHot.setTextColor(getColor(R.color.detailed_dark));
        tvNewFilterWarm.setTextColor(getColor(R.color.detailed_dark));
        tvNewFilterCold.setTextColor(getColor(R.color.detailed_dark));
        tvNewFilterLost.setTextColor(getColor(R.color.detailed_dark));
        tvNewFilterNotInterested.setTextColor(getColor(R.color.detailed_dark));
        tvNewFilterNoResponse.setTextColor(getColor(R.color.detailed_dark));
        tvNewFilterTestDriveTaken.setTextColor(getColor(R.color.detailed_dark));
        tvNewFilterWalkIn.setTextColor(getColor(R.color.detailed_dark));
        tvNewFilterOpen.setTextColor(getColor(R.color.detailed_dark));
        tvNewFilterOutStation.setTextColor(getColor(R.color.detailed_dark));

        // Lead Status ImageViews

        ivLFilterClosed.setImageResource(R.drawable.ic_new_filter_rounded_check);
        ivLFilterHot.setImageResource(R.drawable.ic_new_filter_rounded_check);
        ivLFilterWarm.setImageResource(R.drawable.ic_new_filter_rounded_check);
        ivLFilterCold.setImageResource(R.drawable.ic_new_filter_rounded_check);
        ivLFilterLost.setImageResource(R.drawable.ic_new_filter_rounded_check);
        ivLFilterNotInterest.setImageResource(R.drawable.ic_new_filter_rounded_check);
        ivLFilterNoResponse.setImageResource(R.drawable.ic_new_filter_rounded_check);
        ivLFilterTestDrivenTaken.setImageResource(R.drawable.ic_new_filter_rounded_check);
        ivLFilterWalkIn.setImageResource(R.drawable.ic_new_filter_rounded_check);
        ivLFilterNewOpen.setImageResource(R.drawable.ic_new_filter_rounded_check);
        ivLFilterOutStation.setImageResource(R.drawable.ic_new_filter_rounded_check);

        // Filter Apply button

        btnApplyFilter.setBackground(getDrawable(R.drawable.rounded_button_background_dark_grey_disabled));
        btnApplyFilter.setTextColor(getColor(R.color.white));

        // Posted date count
        llPostedDate.setBackgroundColor(getColor(R.color.edit_text_blue));


        tvLFPostedDateCount.setVisibility(View.GONE);
        tvFollowUpDateCount.setVisibility(View.GONE);
        tvLeadStatusCount.setVisibility(View.GONE);

        tvLFPostedDate.setTextColor(getColor(R.color.white));

        // Follow Up Date
        llFollowUpdate.setBackgroundColor(getColor(R.color.new_lead_filter_grey));
        tvFollowUpDate.setTextColor(getColor(R.color.black));

        llLeadStatusAndCount.setBackgroundColor(getColor(R.color.new_lead_filter_grey));
        tvLeadStatus.setTextColor(getColor(R.color.black));

        clearCustomDate(btnSelectCustomDate, tvDeleteSelectedDate);
        clearCustomDate(btnSelectFCustomDate, tvFDeleteSelectedDate);

        llNewFilterLeadStatus.setVisibility(View.GONE);
        llPostedOrFollowUpDate.setVisibility(View.VISIBLE);
        lLayoutFollowUpDate.setVisibility(View.GONE);

        tvDeleteSelectedDate.setVisibility(View.INVISIBLE);
        tvFDeleteSelectedDate.setVisibility(View.INVISIBLE);



        /*View postedLayout = getLayoutInflater().inflate(R.layout.lead_filter_posted_follow_date_layout, llPostedOrFollowUpDate,false);
        llPostedOrFollowUpDate.addView(postedLayout);*/
    }

    public interface ApplyFilterInf {
        void showToast(String message);

    }

    private void selectCategory() {
        if (isClearAllEnabled) {
            isClearAllEnabled = false;
            clearValues();
        }
        if (filterCategory.equalsIgnoreCase(AppConstants.POSTED_DATE)) {


            llPostedDate.setBackgroundColor(getColor(R.color.edit_text_blue));
            llFollowUpdate.setBackgroundColor(getColor(R.color.new_lead_filter_grey));
            llLeadStatusAndCount.setBackgroundColor(getColor(R.color.new_lead_filter_grey));
            inflateCustomLayout();
            tvLFPostedDate.setTextColor(getColor(R.color.white));
            tvLFPostedDateCount.setTextColor(getColor(R.color.white));
            tvFollowUpDate.setTextColor(getColor(R.color.black));
            tvFollowUpDateCount.setTextColor(getColor(R.color.black));
            tvLeadStatus.setTextColor(getColor(R.color.black));
            tvLeadStatusCount.setTextColor(getColor(R.color.black));

        } else if (filterCategory.equalsIgnoreCase(AppConstants.FOLLOW_DATE)) {
            llPostedDate.setBackgroundColor(getColor(R.color.new_lead_filter_grey));
            llFollowUpdate.setBackgroundColor(getColor(R.color.edit_text_blue));
            llLeadStatusAndCount.setBackgroundColor(getColor(R.color.new_lead_filter_grey));
            inflateCustomLayout();
            tvLFPostedDate.setTextColor(getColor(R.color.black));
            tvLFPostedDateCount.setTextColor(getColor(R.color.black));
            tvFollowUpDate.setTextColor(getColor(R.color.white));
            tvFollowUpDateCount.setTextColor(getColor(R.color.white));
            tvLeadStatus.setTextColor(getColor(R.color.black));
            tvLeadStatusCount.setTextColor(getColor(R.color.black));

        }
        if (isLeadStatusOnClick) {
            llPostedDate.setBackgroundColor(getColor(R.color.new_lead_filter_grey));
            llFollowUpdate.setBackgroundColor(getColor(R.color.new_lead_filter_grey));
            llLeadStatusAndCount.setBackgroundColor(getColor(R.color.edit_text_blue));
            inflateCustomLayout();
            tvLFPostedDate.setTextColor(getColor(R.color.black));
            tvLFPostedDateCount.setTextColor(getColor(R.color.black));
            tvFollowUpDate.setTextColor(getColor(R.color.black));
            tvFollowUpDateCount.setTextColor(getColor(R.color.black));
            tvLeadStatus.setTextColor(getColor(R.color.white));
            tvLeadStatusCount.setTextColor(getColor(R.color.white));
        }
    }

    private void inflateCustomLayout() {
        if (filterCategory.equals("") || filterCategory.equalsIgnoreCase(AppConstants.POSTED_DATE)) {
            /*View postedLayout = getLayoutInflater().inflate(R.layout.lead_filter_posted_follow_date_layout, null);
            llPostedOrFollowUpDate.addView(postedLayout);*/
            llPostedOrFollowUpDate.setVisibility(View.VISIBLE);
            lLayoutFollowUpDate.setVisibility(View.GONE);
            llNewFilterLeadStatus.setVisibility(View.GONE);
        } else if (filterCategory == AppConstants.FOLLOW_DATE) {
            llPostedOrFollowUpDate.setVisibility(View.GONE);
            lLayoutFollowUpDate.setVisibility(View.VISIBLE);
            llNewFilterLeadStatus.setVisibility(View.GONE);
        }
        if (isLeadStatusOnClick) {
            llPostedOrFollowUpDate.setVisibility(View.GONE);
            lLayoutFollowUpDate.setVisibility(View.GONE);
            llNewFilterLeadStatus.setVisibility(View.VISIBLE);
        }
    }

    private void setSelectedCustomDate() {
        try {

            SalesCalendarFilterDialog postCalendarDialog = new SalesCalendarFilterDialog(
                    this, this::onClick,
                    "",
                    (startDateString, endDateString, dateType) -> {

                        if (!startDateString.equals(endDateString)) {
                            //button.setText(startDateString + " To " + endDateString);
                            fromDate = startDateString;
                            toDate = endDateString;
                        } else {
                            /*button.setText(startDateString + " ");*/
                            fromDate = startDateString;
                            toDate = fromDate;
                            Log.i("StartDate", "onClick: " + startDateString);
                        }
                        applyFilterCatChanges();

                    });
            postCalendarDialog.setCancelable(false);
            postCalendarDialog.show();

        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }


}

   /*  tvLeadStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llPostedOrFollowUpDate.setVisibility(View.GONE);
                lLayoutFollowUpDate.setVisibility(View.GONE);
                llNewFilterLeadStatus.setVisibility(View.VISIBLE);

                highlightSelectedItem("LEAD_STATUS");

                if (!CommonMethods.getstringvaluefromkey(RDMFilterActivity.this, AppConstants.LEAD_STATUS_COUNT).isEmpty()) {
                    try {
                        if (CommonMethods.readFromSP(RDMFilterActivity.this).size() > 0) {
                            postedfollDateHashMap = CommonMethods.readFromSP(RDMFilterActivity.this);
                            enableFilterCount(AppConstants.LEAD_STATUS_COUNT);
                            for (Map.Entry me : postedfollDateHashMap.entrySet()) {
                                highLightLeadStatus((Integer) me.getValue());
                                Log.i("FilterActivity", "initViews: " + (Integer) me.getValue());
                            }
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }

                }

            }
        });

*/