package remotedealer.activity

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.google.android.material.internal.NavigationMenuView
import com.google.android.material.navigation.NavigationView
import com.google.firebase.iid.FirebaseInstanceId
import com.mfcwl.mfc_dealer.Activity.SplashActivity
import com.mfcwl.mfc_dealer.BuildConfig
import com.mfcwl.mfc_dealer.R
import com.mfcwl.mfc_dealer.Utility.CommonMethods
import kotlinx.android.synthetic.main.rdm_landingpage.*
import rdm.NotificationActivity
import remotedealer.ui.ASMDashboardFragment
import java.io.IOException


class RDMLandingPage : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.rdm_landingpage)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility =View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            window.statusBarColor = Color.WHITE
        }
        Log.i("RDMLandingPage", "onCreate: ")
        setSupportActionBar(asmtoolbar)
        val drawerToggle = ActionBarDrawerToggle(this, drawerLayout, asmtoolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawerLayout.addDrawerListener(drawerToggle)
        drawerToggle.syncState()
        asmtoolbar.setNavigationIcon(R.drawable.ic_rdm_burger)//this must be after s
        navigationView.setNavigationItemSelectedListener(this)
        loadFragment(ASMDashboardFragment())
        val menu = navigationView.menu
        val prfileName = menu.findItem(R.id.asm_profile)
        prfileName.title = ""+ CommonMethods.getstringvaluefromkey(this, "user_name")
        val headerView: View? = nav_view_footer.getHeaderView(0) // Index of the added headerView
        val titleTextView: TextView? = headerView?.findViewById(R.id.appversiontxt)
        titleTextView!!.text = "App version : "+ BuildConfig.VERSION_NAME

        nav_view_footer.isVerticalScrollBarEnabled = false
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.asm_notification, menu);
        return super.onCreateOptionsMenu(menu)
    }


    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.notificationicon -> {
                val notifications = Intent(this, NotificationActivity::class.java)
                startActivity(notifications)
                return true
            }
        }
        return true
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.asm_profile -> {

            }

            R.id.rdr_menu_logout -> {
                logoutDialog()
            }

        }

        item.isCheckable = false
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    fun loadFragment(fragment: Fragment) {
        if (fragment != null) {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.content_frame, fragment)
            transaction.commit()
        }
    }


    fun logoutDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.asm_logout)
        val Message: TextView
        val yes: Button
        val cancel: Button
        Message = dialog.findViewById(R.id.Message)
        yes = dialog.findViewById(R.id.yes)
        cancel = dialog.findViewById(R.id.cancel)
        dialog.setCancelable(false)

        yes.setOnClickListener {
             logOut()
            dialog.dismiss()
        }

        cancel.setOnClickListener {

            dialog.dismiss()
        }
        dialog.show()
    }

    fun logOut() {


        try {
            val preference = PreferenceManager.getDefaultSharedPreferences(this)
            val mSharedPreferences =getSharedPreferences("MFC", MODE_PRIVATE)
            val editor = preference.edit()
            val e = mSharedPreferences.edit()
            e.clear()
            e.commit()
            editor.clear().commit()
            CommonMethods.deleteCache(this)
            CommonMethods.setvalueAgainstKey(this, "token", "")
            CommonMethods.MemoryClears()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            FirebaseInstanceId.getInstance().deleteInstanceId()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        CommonMethods.setvalueAgainstKey(this, "appstatus", "0")
        val notifications = Intent(this, SplashActivity::class.java)
        startActivity(notifications)
        finish()

    }


}