package remotedealer.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mfcwl.mfc_dealer.R
import kotlinx.android.synthetic.main.tutorial_four.*

class TutorialFour : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tutorial_four)

        tutonext.setOnClickListener {
            val tutoTwo = Intent(this,TutorialFive::class.java)
            startActivity(tutoTwo)
            finish()
        }

        tutoback.setOnClickListener {
            val tutoTwo = Intent(this,TutorialThree::class.java)
            startActivity(tutoTwo)
            finish()
        }
    }

    override fun onBackPressed() {


    }
}