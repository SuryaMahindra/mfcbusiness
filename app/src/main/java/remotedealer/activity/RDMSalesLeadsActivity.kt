package remotedealer.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.mfcwl.mfc_dealer.R
import com.mfcwl.mfc_dealer.Utility.SpinnerManager
import kotlinx.android.synthetic.main.rdm_sales_leads_fragment.*
import remotedealer.model.procurement.ProcureItem
import remotedealer.model.procurement.ProcurementRequest
import remotedealer.model.procurement.ProcurementResponse
import remotedealer.model.token.RDRTokenRequest
import remotedealer.model.token.RDRTokenResponse
import remotedealer.retrofit.RetroBase
import remotedealer.retrofit.RetroBase.Companion.URL_END_PROCUREMENT_CPT
import remotedealer.retrofit.RetroBase.Companion.retroInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

/*
 * Created By Udaya(MFCWL) ->  02-12-2020 17:13
 *
 */
class RDMSalesLeadsActivity : AppCompatActivity() {

    var TAG = javaClass.simpleName
    var token = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.rdm_sales_leads_fragment)
        Log.i(TAG, "onCreate: ")


        callGetToken()

        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH)
        val strToDate = simpleDateFormat.format(Calendar.getInstance().time)

        val cal = Calendar.getInstance()
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH))

        val strFromDate = SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH).format(cal.time)



        Log.i(TAG, "onCreate: $strFromDate")
        Log.i(TAG, "onCreate: $strToDate")

    }

    private fun callGetToken() {

        SpinnerManager.showSpinner(this)
        retroInterface.getFromWeb(RDRTokenRequest(), RetroBase.URL_END_GET_TOKEN).enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                SpinnerManager.hideSpinner(this@RDMSalesLeadsActivity)

                val strRes = Gson().toJson(response.body())
                val resModel = Gson().fromJson(strRes, RDRTokenResponse::class.java)

                token = resModel.token

                Log.i(TAG, "onResponse: " + resModel.token)

                callWebLeads(View(this@RDMSalesLeadsActivity))
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(this@RDMSalesLeadsActivity)
            }

        })

    }

    fun callWebLeads(view: View) {

        web_leads_details.visibility = View.VISIBLE
        walkin_leads_details.visibility = View.GONE


        val req = ProcurementRequest()
        req.code = "123"
        req.fromDate = "2020-11-10"
        req.toDate = "2020-11-13"

        SpinnerManager.showSpinner(this)
        retroInterface.getFromWeb(req, URL_END_PROCUREMENT_CPT, "Bearer $token").enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                SpinnerManager.hideSpinner(this@RDMSalesLeadsActivity)

                val strRes = Gson().toJson(response.body())
                val strModel = Gson().fromJson(strRes, ProcurementResponse::class.java)

                Log.i(TAG, "onResponse: " + strModel.data.size)

                parseWebLeadsData(strModel.data)

            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(this@RDMSalesLeadsActivity)
            }
        })

    }

    private fun parseWebLeadsData(data: ArrayList<ProcureItem>) {

        try {

            //tvWebLeadsOMS
        } catch (e: Exception){
            Log.i(TAG, "parseWebLeadsData: "+e.message)
        }

    }

    fun callWalkInLeads(view: View) {
        web_leads_details.visibility = View.GONE
        walkin_leads_details.visibility = View.VISIBLE
    }
}