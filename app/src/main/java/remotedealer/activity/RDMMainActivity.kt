package remotedealer.activity

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.mfcwl.mfc_dealer.R
import remotedealer.rdrform.ui.WebLeadsFragment

/*
 * Created By Uday(MFCWL) ->  03-12-2020 13:02
 *
 */
class RDMMainActivity : AppCompatActivity() {

    var TAG = javaClass.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_rdm_main_actvity)

        Log.i(TAG, "onCreate: ")

        loadFragment(WebLeadsFragment())
    }

    private fun loadFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.rdm_fragment_container, fragment).commit()
    }

}