package remotedealer.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mfcwl.mfc_dealer.R
import com.mfcwl.mfc_dealer.Utility.CommonMethods
import kotlinx.android.synthetic.main.tutorial_five.*

class TutorialFive : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tutorial_five)

        tutonext.setOnClickListener {
            CommonMethods.setvalueAgainstKey(this,"isTutorialSaw","True")
            val tutoTwo = Intent(this,RDMLandingPage::class.java)
            startActivity(tutoTwo)
            finish()
        }

        tutoback.setOnClickListener {
            val tutoTwo = Intent(this,TutorialFour::class.java)
            startActivity(tutoTwo)
            finish()
        }
    }

    override fun onBackPressed() {


    }
}