package remotedealer.dashboard.interfaces

interface DeleteCallBack {
    fun onSuccess(message:String?)

    fun onFailure(message:String?)
}