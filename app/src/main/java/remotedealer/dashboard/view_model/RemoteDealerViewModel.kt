package remotedealer.dashboard.view_model

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.mfcwl.mfc_dealer.Activity.MainActivity.activity
import com.mfcwl.mfc_dealer.Utility.CommonMethods
import org.json.JSONObject
import rdm.model.EventWhere
import rdm.model.SearchEventReq
import rdm.retrofit_services.RetroBaseService
import remotedealer.dashboard.interfaces.DeleteCallBack
import remotedealer.dashboard.model.*
import remotedealer.model.token.RDRTokenRequest
import remotedealer.model.token.RDRTokenResponse
import remotedealer.retrofit.RetroBase
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RemoteDealerViewModel : ViewModel() {
    var TAG = javaClass.simpleName
    private val _userMatrices = MutableLiveData<ParameterResponse>()
    val userMatrices: LiveData<ParameterResponse> = _userMatrices

    private val _initiative = MutableLiveData<InitiativeResponse>()
    val initiative: LiveData<InitiativeResponse> = _initiative

    private val _meeting = MutableLiveData<MeetingResponse>()
    val meeting: LiveData<MeetingResponse> = _meeting

    private val _token = MutableLiveData<String>()

    init {
        callApis()
    }

    fun callApis(){
        callGetToken()
    }

    private fun callGetToken() {
        RetroBase.retroInterface.getFromWeb(RDRTokenRequest(), RetroBase.URL_END_GET_TOKEN).enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                val strRes = Gson().toJson(response.body())
                val resModel = Gson().fromJson(strRes, RDRTokenResponse::class.java)
                if(resModel is RDRTokenResponse) {
                    _token.value = resModel.token
                    callDealerMatrices(resModel.token)
                    callDealerInitiativeAPI(resModel.token)
                    callMeetingApi()
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    private fun callDealerMatrices(token: String) {
        val id = CommonMethods.getToken("dealer_code")
        RetroBase.retroInterface.getFromWeb(RetroBase.URL_END_DEALER_MATRIX+id, "Bearer $token").enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                val strRes = Gson().toJson(response.body())
                val resModel = Gson().fromJson(strRes, ParameterResponse::class.java)
                _userMatrices.value = resModel
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    private fun callMeetingApi() {

        RetroBaseService.retrofitInterface.getFromWeb(getEventSearchRequest(),RetroBaseService.URL_END_SEARCH_EVENT).enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                val strRes = Gson().toJson(response.body())
                val resModel = Gson().fromJson(strRes, MeetingResponse::class.java)
                _meeting.value = resModel
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    private fun getEventSearchRequest(): SearchEventReq? {

        var searchEvent = SearchEventReq()
        searchEvent.page = "1"
        searchEvent.pageItems = "2000"
        searchEvent.orderBy = ""
        searchEvent.orderByReverse = false.toString()
        searchEvent.filterByFields = ""
        searchEvent.where = getEventWhere()
        return searchEvent

    }

    private fun getEventWhere(): MutableList<EventWhere>? {

        var eventWhere = mutableListOf<EventWhere>()

        val dealerCode = EventWhere()
        dealerCode.column = "dealer_code"
        dealerCode.operator = "="
        dealerCode.value = CommonMethods.getstringvaluefromkey(activity,"dealer_code")

        val startEventDate = EventWhere()
        startEventDate.column = "event_start_date_time"
        startEventDate.operator = ">="
        startEventDate.value = CommonMethods.getTodayDate()+" 00:00:00"

        val eventEndDate = EventWhere()
        eventEndDate.column = "event_start_date_time"
        eventEndDate.operator = "<="
        eventEndDate.value = CommonMethods.getTomorrowDateAndTime(activity)

        eventWhere.add(dealerCode)
        eventWhere.add(startEventDate)
        eventWhere.add(eventEndDate)

        return eventWhere
    }

    private fun callDealerInitiativeAPI(token: String) {
        val id = CommonMethods.getToken("dealer_code")
        RetroBase.retroInterface.getFromWeb(RetroBase.URL_END_DEALER_INITIATIVE+id, "Bearer $token").enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                val strRes = Gson().toJson(response.body())
                Log.i(TAG, "onResponse: "+strRes)
                val resModel = Gson().fromJson(strRes, InitiativeResponse::class.java)
                _initiative.value = resModel
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    fun deleteInitiative(id:Int,callback:DeleteCallBack?){
        val request = DeleteRequest(id.toString())
        RetroBase.retroInterface.deleteFromWeb(request,RetroBase.URL_DELETE_EVENTS, "Bearer ${_token.value}").enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                callback?.onSuccess(response.body().toString())
                callDealerInitiativeAPI(_token.value!!)
//                val strRes = Gson().toJson(response.body())
//                val resModel = Gson().fromJson(strRes, DeleteEventResponse::class.java)
//                if(resModel.isSuccess){
//                    callDealerInitiativeAPI(_token.value!!)
//                    callback?.onSuccess(resModel.message)
//                }else{
//                    callback?.onFailure(resModel.message)
//                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                t.printStackTrace()
                callback?.onFailure(t.message)
            }
        })
    }
}