package remotedealer.dashboard.view_model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.mfcwl.mfc_dealer.Utility.CommonMethods
import rdm.retrofit_services.RetroBaseService
import remotedealer.dashboard.model.CountResponse
import remotedealer.dashboard.model.ParameterResponse
import remotedealer.model.token.RDRTokenRequest
import remotedealer.model.token.RDRTokenResponse
import remotedealer.retrofit.RetroBase
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CountViewModel : ViewModel() {
    private val _countData = MutableLiveData<CountResponse>()
    val countData: LiveData<CountResponse> = _countData

    init {
        callGetToken()
    }

    fun callGetToken() {
        RetroBase.retroInterface.getFromWeb(RDRTokenRequest(), RetroBase.URL_END_GET_TOKEN).enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                val strRes = Gson().toJson(response.body())
                val resModel = Gson().fromJson(strRes, RDRTokenResponse::class.java)
                if(resModel is RDRTokenResponse) {
                    getCountAPI(resModel.token)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    private fun getCountAPI(token:String){
        val id = CommonMethods.getToken("dealer_code")
        RetroBase.retroInterface.getFromWeb(RetroBase.URL_COUNT+id,"Bearer $token").enqueue(object : Callback<Any> {
                override fun onResponse(call: Call<Any>, response: Response<Any>) {
                    val strRes = Gson().toJson(response.body())
                    val resModel = Gson().fromJson(strRes, CountResponse::class.java)
                    _countData.value = resModel
                }

                override fun onFailure(call: Call<Any>, t: Throwable) {
                    t.printStackTrace()
                }
            })
    }
}