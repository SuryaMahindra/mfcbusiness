package remotedealer.dashboard.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.mfcwl.mfc_dealer.Model.LeadSection.WebLeadsCustomWhere
import com.mfcwl.mfc_dealer.R
import com.mfcwl.mfc_dealer.Utility.AppConstants
import com.mfcwl.mfc_dealer.Utility.CommonMethods
import remotedealer.dashboard.interfaces.DeleteCallBack
import remotedealer.dashboard.model.*
import remotedealer.dashboard.view_model.CountViewModel
import remotedealer.dashboard.view_model.RemoteDealerViewModel
import remotedealer.model.sales.RDMSalesRequest
import remotedealer.model.stock.RDMStocksRequest
import remotedealer.ui.RDMLeadsListingFragment
import remotedealer.ui.RDMSalesListFragmentNew
import remotedealer.ui.RDMStocksListFragment
import remotedealer.util.NavigationUtility
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class RemoteDealerDashboard(var code: String, var token: String) : Fragment() {

    private lateinit var viewModel: RemoteDealerViewModel
    private lateinit var parameterLayout: LinearLayout
    private lateinit var parameterTable: RelativeLayout
    private lateinit var parametervaluecontainer: LinearLayout

    private lateinit var initiativeToggleLayout: RelativeLayout
    private lateinit var initiativeviewholder: LinearLayout
    private lateinit var initiativeLayout: LinearLayout
    private lateinit var meetingToggleLayout: RelativeLayout
    private lateinit var meetingviewholder: LinearLayout
    private lateinit var meetingLayout: LinearLayout
    private lateinit var initiativeCount: TextView
    private lateinit var parameterCount: TextView

    private lateinit var totalStocks: LinearLayout
    private lateinit var webLeads: LinearLayout
    private lateinit var totalSales: LinearLayout

    private lateinit var stocksCount: TextView
    private lateinit var webCount: TextView
    private lateinit var salesCount: TextView

    val TAG = javaClass.simpleName

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        Log.i(TAG, "onCreateView: ")
        return inflater.inflate(R.layout.rdm_dashboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
        initViewModel()
    }

    private fun initView(view: View) {

        totalSales = view.findViewById(R.id.totalSales)
        totalStocks = view.findViewById(R.id.stocks)
        webLeads = view.findViewById(R.id.webleads)
        val parameterImage = view.findViewById<ImageView>(R.id.parameter_image)
        val initiativeImage = view.findViewById<ImageView>(R.id.initiative_image)
        val meetingImage = view.findViewById<ImageView>(R.id.meeting_image)
        initiativeCount = view.findViewById<TextView>(R.id.initiative_count)
        parameterCount = view.findViewById(R.id.parameter_count)
        salesCount = view.findViewById(R.id.sales_count)
        webCount = view.findViewById(R.id.web_count)
        stocksCount = view.findViewById(R.id.stockCount)
        /**
         * handling parameter click
         */
        parameterLayout = view.findViewById(R.id.parameter_layout)
        parameterTable = view.findViewById(R.id.table_view)
        parametervaluecontainer = view.findViewById(R.id.table_body)
        parameterLayout.setOnClickListener {
            if (parameterTable.visibility == View.VISIBLE) {
                parameterImage.setImageResource(R.drawable.ic_expand)
                parameterTable.visibility = View.GONE
            } else {
                parameterImage.setImageResource(R.drawable.ic_collapse)
                parameterTable.visibility = View.VISIBLE
            }
            if (initiativeToggleLayout.visibility == View.VISIBLE) {
                initiativeImage.setImageResource(R.drawable.ic_expand)
                initiativeToggleLayout.visibility = View.GONE
            }
            if (meetingToggleLayout.visibility == View.VISIBLE) {
                meetingImage.setImageResource(R.drawable.ic_expand)
                meetingToggleLayout.visibility = View.GONE
            }
        }

        /**
         * handling initiative click
         */
        initiativeLayout = view.findViewById(R.id.initiative_layout)
        initiativeToggleLayout = view.findViewById(R.id.initiativeToggleLayout)
        initiativeviewholder = view.findViewById(R.id.initiativeHolder)
        initiativeLayout.setOnClickListener {
            if (initiativeToggleLayout.visibility == View.VISIBLE) {
                initiativeImage.setImageResource(R.drawable.ic_expand)
                initiativeToggleLayout.visibility = View.GONE
            } else {
                initiativeImage.setImageResource(R.drawable.ic_collapse)
                initiativeToggleLayout.visibility = View.VISIBLE
            }
            if (parameterTable.visibility == View.VISIBLE) {
                parameterImage.setImageResource(R.drawable.ic_expand)
                parameterTable.visibility = View.GONE
            }
            if (meetingToggleLayout.visibility == View.VISIBLE) {
                meetingImage.setImageResource(R.drawable.ic_expand)
                meetingToggleLayout.visibility = View.GONE
            }
        }

        /**
         * handling meeting click
         */
        meetingLayout = view.findViewById(R.id.meeting)
        meetingToggleLayout = view.findViewById(R.id.meetingToggleLayout)
        meetingviewholder = view.findViewById(R.id.meetingHolder)
        meetingLayout.setOnClickListener {
            if (meetingToggleLayout.visibility == View.VISIBLE) {
                meetingImage.setImageResource(R.drawable.ic_expand)
                meetingToggleLayout.visibility = View.GONE
            } else {
                meetingImage.setImageResource(R.drawable.ic_collapse)
                meetingToggleLayout.visibility = View.VISIBLE
            }
            if (parameterTable.visibility == View.VISIBLE) {
                parameterImage.setImageResource(R.drawable.ic_collapse)
                parameterTable.visibility = View.GONE
            }
            if (initiativeToggleLayout.visibility == View.VISIBLE) {
                initiativeImage.setImageResource(R.drawable.ic_collapse)
                initiativeToggleLayout.visibility = View.GONE
            }
        }


        totalSales.setOnClickListener {
            setCurrentFragment(RDMSalesListFragmentNew(token, code, "total_sales", getWhereListSales("sold_date"), getWhereInListSales("")))
        }
        totalStocks.setOnClickListener {
            setCurrentFragment(RDMStocksListFragment(token, code, getWhereList(""), getWhereInList("stock_source"), "", ""))
        }
        webLeads.setOnClickListener {
            setCurrentFragment(RDMLeadsListingFragment(getOMSPending(), mutableListOf(), "web_leads_no_closed",mutableListOf(), mutableListOf(), "all", AppConstants.WEB_LEADS))
        }

//        Handler(Looper.getMainLooper()).postDelayed({
//            parameterLayout.performClick()
//        },1000)

    }

    private fun getWhereListSales(str: String): ArrayList<RDMSalesRequest.Where> {
        val whereList = ArrayList<RDMSalesRequest.Where>()
        val where = RDMSalesRequest.Where()
        if (str == "hot") {
            where.column = "dispatched.status"
            where.operator = "="
            where.value = str
            whereList.add(where)
        } else if (str == "warm") {
            where.column = "dispatched.status"
            where.operator = "="
            where.value = str
            whereList.add(where)
        } else if(str == "sold_date"){

            val stockType = RDMSalesRequest.Where()
            stockType.column = "stock_type"
            stockType.operator = "="
            stockType.value = "4W"
            whereList.add(stockType)

            val sold = RDMSalesRequest.Where()
            sold.column = "sold"
            sold.operator = "="
            sold.value = "1"
            whereList.add(sold)

            val where = RDMSalesRequest.Where()
            where.column = "sold_date"
            where.operator = ">="
            where.value = CommonMethods.getFirstDay(Date())
            whereList.add(where)
        }



        return whereList
    }


    private fun getWhereInListSales(str: String): ArrayList<RDMSalesRequest.WhereIn> {
        val whereInList = ArrayList<RDMSalesRequest.WhereIn>()
        val whereIn1 = RDMSalesRequest.WhereIn()
        whereIn1.column = "dealer_code"
        whereIn1.values = arrayOf("$code")
        whereInList.add(whereIn1)

        val whereIn2 = RDMSalesRequest.WhereIn()
        whereIn2.column = "source"
        whereIn2.values = arrayOf("mfc", "p&s")
        whereInList.add(whereIn2)

        return whereInList
    }

    private fun getWhereList(str: String): ArrayList<RDMStocksRequest.Where> {
        val whereList = ArrayList<RDMStocksRequest.Where>()
        return whereList
    }


    private fun getWhereInList(str: String): ArrayList<RDMStocksRequest.WhereIn> {
        val whereInList = ArrayList<RDMStocksRequest.WhereIn>()
        val whereIn1 = RDMStocksRequest.WhereIn()
        whereIn1.column = "dealer_code"
        whereIn1.values = arrayOf("$code")
        whereInList.add(whereIn1)


        if (str == "stock_source") {
            val whereIn2 = RDMStocksRequest.WhereIn()
            whereIn2.column = "stock_source"
            whereIn2.values = arrayOf("mfc", "p&s")
            whereInList.add(whereIn2)
        }

        return whereInList
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider.NewInstanceFactory().create(RemoteDealerViewModel::class.java)


        viewModel.userMatrices.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            if (it == null) return@Observer
            populateParameterUI(it)
        })

        viewModel.initiative.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            if (it == null) return@Observer
            populateInitiative(it)
        })
        viewModel.meeting.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            try {
                populateMeeting(it)
            } catch (e: Exception) {

            }
        })
        val countViewModel = ViewModelProvider.NewInstanceFactory().create(CountViewModel::class.java)
        countViewModel.countData.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            loadCount(it)
        })
    }

    private fun loadCount(count: CountResponse) {
        salesCount.text = "(" + count.totalSalesCount + ")"
        webCount.text = "(" + count.totalWebLeadsCount + ")"
        Log.i(TAG, "loadCount: " + count.totalStocksCount)
        stocksCount.text = "(" + count.totalStocksCount + ")"
    }

    private fun loadUnderDevelopmentFragment() {
        setCurrentFragment(UnderDevelopmentFragment())
    }

    private fun populateParameterUI(response: ParameterResponse) {
        var count = 0
        val data = response.data
        parametervaluecontainer.removeAllViews()
        data.forEach {
            val tableItem = LayoutInflater.from(activity).inflate(R.layout.table_item, parametervaluecontainer, false)
            val parameter = tableItem.findViewById<TextView>(R.id.parameter)
            val percent = tableItem.findViewById<TextView>(R.id.percent)
            val expected = tableItem.findViewById<TextView>(R.id.expected)
            val actual = tableItem.findViewById<TextView>(R.id.actual)

            val status = it.status
            when {
                status.equals("green",ignoreCase = true )-> {
                    percent.setTextColor(resources.getColor(R.color.text_view_green))
                }
                status.equals("Red",ignoreCase = true ) -> {
                    percent.setTextColor(resources.getColor(R.color.red))
                    count ++
                }
                else -> {
                    percent.setTextColor(resources.getColor(R.color.table_yellow))
                }
            }

            parameter.text = it.metric
            percent.text = "${it.percentage}%"
            expected.text = it.target
            actual.text = it.actual
            parametervaluecontainer.addView(tableItem)
        }
        if(count!=0)
            parameterCount.visibility = View.VISIBLE
        parameterCount.text = "$count"
    }

    private fun populateInitiative(response: InitiativeResponse) {

        var data = response.data
        initiativeviewholder.removeAllViews()
        initiativeCount.text = data.size.toString()
        if (data == null || data.isEmpty()) {
            val tableItem = LayoutInflater.from(activity).inflate(R.layout.no_data_layout, meetingviewholder, false)
            val noData = tableItem.findViewById<TextView>(R.id.no_data_message)
            noData.text = "No Open Initiatives"
            initiativeviewholder.addView(tableItem)
            return
        }
        data.forEach { eachData ->
            val tableItem = LayoutInflater.from(activity).inflate(R.layout.initiative_layout, initiativeviewholder, false)
            val initiativeHeading = tableItem.findViewById<TextView>(R.id.initiative_heading)
            val initiativeSubHeading = tableItem.findViewById<TextView>(R.id.initiative_subHeading)
            val initiativeBody = tableItem.findViewById<TextView>(R.id.initiative_body)
            val deleteButton = tableItem.findViewById<LinearLayout>(R.id.delete_initiative)

            initiativeHeading.text = eachData.category
            initiativeSubHeading.text = eachData.actionItem
            initiativeBody.text = eachData.remark

            deleteButton.setOnClickListener {
                viewModel.deleteInitiative(eachData.actionId, object : DeleteCallBack {
                    override fun onSuccess(message: String?) {
                    }

                    override fun onFailure(message: String?) {
                        CommonMethods.alertMessage(activity, message)
                    }

                })
            }

            initiativeviewholder.addView(tableItem)
        }
    }

    private fun populateMeeting(response: MeetingResponse) {
        val data = response.data.sortedWith(compareBy({ it.event_start_date_time }))
        var isToday = false
        var isTomorrow = false
        meetingviewholder.removeAllViews()
        if (data == null || data.isEmpty()) {
            val tableItem = LayoutInflater.from(activity).inflate(R.layout.no_data_layout, meetingviewholder, false)
            val noData = tableItem.findViewById<TextView>(R.id.no_data_message)
            noData.text = "No meetings"
            meetingviewholder.addView(tableItem)
            return
        }


        data.forEach {
            val tableItem = LayoutInflater.from(activity).inflate(R.layout.meeting_card, meetingviewholder, false)

            val title = tableItem.findViewById<TextView>(R.id.meeting_title)
            val date = tableItem.findViewById<TextView>(R.id.meeting_date)
            if(isToday){
                title.visibility = View.GONE
            }

            if(getTodaysOrTomorrow(it.event_start_date_time).equals("Today Meeting",ignoreCase = true)){
                title.text = getTodaysOrTomorrow(it.event_start_date_time)
                isToday = true
            } else if(getTodaysOrTomorrow(it.event_start_date_time).equals("Tomorrow Meeting",ignoreCase = true)){
                if(isTomorrow){
                    title.visibility = View.GONE
                }else{
                    title.visibility = View.VISIBLE
                }
                title.text = getTodaysOrTomorrow(it.event_start_date_time)
                isTomorrow = true
            }
            date.text = "${getMeetingDateFormat(it.event_start_date_time)} to ${getOnlyTime(it.event_end_date_time)}"

            meetingviewholder.addView(tableItem)
        }

        if(!isTomorrow){
            val tableItem = LayoutInflater.from(activity).inflate(R.layout.meeting_card, meetingviewholder, false)
            val title = tableItem.findViewById<TextView>(R.id.meeting_title)
            val date = tableItem.findViewById<TextView>(R.id.meeting_date)
            title.text = "Tomorrow Meeting"
            date.text = "No meetings"
            meetingviewholder.addView(tableItem)
        }

    }

    private fun getMeetingDateFormat(date: String): String {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
        val outputFormat = SimpleDateFormat("dd E | HH:mm", Locale.ENGLISH)
        var outDate = "NO meeting"
        try {
            outDate = outputFormat.format(inputFormat.parse(date))
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return outDate
    }

    private fun getOnlyTime(date: String): String {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
        val outputFormat = SimpleDateFormat("HH:mm", Locale.ENGLISH)
        var outDate = "NO meeting"
        try {
            outDate = outputFormat.format(inputFormat.parse(date))
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return outDate
    }

    private fun getOMSPending(): MutableList<WebLeadsCustomWhere> {

        val list = mutableListOf<WebLeadsCustomWhere>()
        val leadType = WebLeadsCustomWhere()
        leadType.column = "leads.lead_type"
        leadType.operator = "="
        leadType.value = "USEDCARSALES"

        val targetId = WebLeadsCustomWhere()
        targetId.column = "dispatched.target_id"
        targetId.operator = "="
        targetId.value = code

        list.add(leadType)
        list.add(targetId)

            val createdStartDate = WebLeadsCustomWhere()
            createdStartDate.column = "leads.created_at"
            createdStartDate.operator = ">="
            createdStartDate.value = CommonMethods.getFirstDay(Date())
            list.add(createdStartDate)

        return list

    }


    private fun getTodaysDateFormat(date: String): String {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
        val outputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        var outDate: String = ""
        try {
            outDate = outputFormat.format(inputFormat.parse(date))
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return outDate
    }

    private fun getTodaysOrTomorrow(date: String): String {
        val str = getTodaysDateFormat(date)
        if (isTodaysDate(str))
            return "Today Meeting"
        return "Tomorrow Meeting"
    }

    private fun isTodaysDate(date: String): Boolean {
        if (date == CommonMethods.getTodayDate())
            return true
        return false
    }

    private fun setCurrentFragment(fragment: Fragment) =
//            activity?.supportFragmentManager?.beginTransaction()?.apply {
//                replace(R.id.flFragment, fragment)
//                commit()
//            }

            NavigationUtility.addFragment(fragment, activity, fragment.tag)
}