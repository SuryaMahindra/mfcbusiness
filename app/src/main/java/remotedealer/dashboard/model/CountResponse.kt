package remotedealer.dashboard.model

import com.google.gson.annotations.SerializedName

data class CountResponse(
        @SerializedName("totalStocksCount") val totalStocksCount : Int,
        @SerializedName("certifiedStocksCount") val certifiedStocksCount : Int,
        @SerializedName("bookedStocksCount") val bookedStocksCount : Int,
        @SerializedName("totalWebLeadsCount") val totalWebLeadsCount : Int,
        @SerializedName("totalPrivateLeadsCount") val totalPrivateLeadsCount : Int,
        @SerializedName("followUpWebLeadsCount") val followUpWebLeadsCount : Int,
        @SerializedName("followUpWalkinLeadsCount") val followUpWalkinLeadsCount : Int,
        @SerializedName("totalSalesCount") val totalSalesCount : Int,
        @SerializedName("warrantySalesCount") val warrantySalesCount : Int,
        @SerializedName("nonWarrantySalesCount") val nonWarrantySalesCount : Int
)

