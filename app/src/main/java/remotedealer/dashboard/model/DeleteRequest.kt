package remotedealer.dashboard.model

import com.google.gson.annotations.SerializedName

data class DeleteRequest(
        @SerializedName("id") val id:String
)
