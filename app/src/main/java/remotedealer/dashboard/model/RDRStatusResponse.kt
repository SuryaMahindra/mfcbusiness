package remotedealer.dashboard.model


import com.google.gson.annotations.SerializedName

data class RDRStatusResponse(
    @SerializedName("data")
    var `data`: List<Data>
) {
    data class Data(
        @SerializedName("code")
        var code: String,
        @SerializedName("is_op")
        var isOp: Boolean,
        @SerializedName("status")
        var status: String
    )
}