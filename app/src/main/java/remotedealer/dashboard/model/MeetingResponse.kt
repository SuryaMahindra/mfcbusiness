package remotedealer.dashboard.model

import com.google.gson.annotations.SerializedName

data class MeetingResponse(@SerializedName("current_page") val current_page: Integer,
                           @SerializedName("per_page") val per_page: Integer,
                           @SerializedName("total") val total: Integer,
                           @SerializedName("data") val data: List<MeetingData>)


data class MeetingData(
        @SerializedName("event_title") val event_title: String,
        @SerializedName("event_description") val event_description: String,
        @SerializedName("event_location") val event_location: String,
        @SerializedName("event_id") val event_id: Int,
        @SerializedName("is_all_day_event") val is_all_day_event: Boolean,
        @SerializedName("event_start_date_time") val event_start_date_time: String,
        @SerializedName("event_end_date_time") val event_end_date_time: String,
        @SerializedName("event_type") val event_type: String,
        @SerializedName("dealer_code") val dealer_code: Int,
        @SerializedName("dealer_name") val dealer_name: String,
        @SerializedName("mteams_event_id") val mteams_event_id: String,
        @SerializedName("participants") val participants: List<Participants>
)

data class Participants(

        @SerializedName("user_code") val user_code: Int,
        @SerializedName("name") val name: String,
        @SerializedName("email") val email: String,
        @SerializedName("type") val type: String
)