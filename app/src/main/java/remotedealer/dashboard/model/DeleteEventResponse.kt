package remotedealer.dashboard.model

import com.google.gson.annotations.SerializedName

data class DeleteEventResponse(
        @SerializedName("is_success") val isSuccess : Boolean,
        @SerializedName("message") val message : String,
        @SerializedName("data") val data : String,
)