package remotedealer.dashboard.model

import com.google.gson.annotations.SerializedName

data class ParameterResponse(@SerializedName("data") val data : List<ParameterData>)

data class ParameterData(
        @SerializedName("metric") val metric : String,
        @SerializedName("target") val target : String,
        @SerializedName("actual") val actual : String,
        @SerializedName("percentage") val percentage : String,
        @SerializedName("status") val status : String
)