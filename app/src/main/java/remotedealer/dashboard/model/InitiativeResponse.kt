package remotedealer.dashboard.model

import com.google.gson.annotations.SerializedName

class InitiativeResponse {
    @SerializedName("data")
    var data = ArrayList<InitiativeData>()
}

class InitiativeData {
    @SerializedName("category")
    var category: String = ""

    @SerializedName("actionitem")
    var actionItem: String = ""

    @SerializedName("remarks")
    var remark: String = ""

    @SerializedName("action_id")
    var actionId: Int = 0
}