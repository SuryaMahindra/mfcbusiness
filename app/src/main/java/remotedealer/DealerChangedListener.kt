package remotedealer.listener

import remotedealer.model.DealerItem

interface DealerChangedListener {
    fun dealerChanged(dealer: DealerItem)
}