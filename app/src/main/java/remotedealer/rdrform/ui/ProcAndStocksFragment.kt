package remotedealer.rdrform.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import com.mfcwl.mfc_dealer.R
import com.mfcwl.mfc_dealer.SalesStocks.Instances.SalesCalendarFilterDialog
import com.mfcwl.mfc_dealer.Utility.SpinnerManager
import kotlinx.android.synthetic.main.booked_stoks.*
import kotlinx.android.synthetic.main.booked_stoks.layoutStoreStocks
import kotlinx.android.synthetic.main.booked_stoks.view.*
import kotlinx.android.synthetic.main.fragment_proc_and_stocks.*
import kotlinx.android.synthetic.main.fragment_proc_and_stocks.view.*
import remotedealer.dashboard.view_model.CountViewModel
import remotedealer.model.procurement.ProcureItem
import remotedealer.model.procurement.ProcurementRequest
import remotedealer.model.procurement.ProcurementResponse
import remotedealer.model.stock.RDMStocksRequest
import remotedealer.model.token.RDRTokenRequest
import remotedealer.model.token.RDRTokenResponse
import remotedealer.retrofit.RetroBase
import remotedealer.retrofit.RetroBase.Companion.URL_END_GET_TOKEN
import remotedealer.retrofit.RetroBase.Companion.URL_END_PROCUREMENT_CPT
import remotedealer.retrofit.RetroBase.Companion.URL_END_PROCUREMENT_IEP
import remotedealer.retrofit.RetroBase.Companion.URL_END_STORE_STOCKS
import remotedealer.retrofit.RetroBase.Companion.retroInterface
import remotedealer.ui.RDMStocksListFragment
import remotedealer.util.NavigationUtility
import remotedealer.util.RedirectionConstants.Companion.KEY_BOOKSTOCK
import remotedealer.util.RedirectionConstants.Companion.KEY_CERTIFIED
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class ProcAndSalesFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null

    var code = ""

    var TAG = javaClass.simpleName
    var token = ""
    lateinit var vi: View

    var cptFromDate = "2020-11-10"
    var cptToDate = "2020-11-13"

    var iepFromDate = "2020-11-10"
    var iepToDate = "2020-11-13"

    var selfFromDate = "2020-11-10"
    var selfToDate = "2020-11-13"

    var bookedStockFromDate = ""
    var bookedStockToDate = ""

    var storeStockFromDate = ""
    var storeStockToDate = ""

    private fun getFirstDateOfCurrentMonth(): Date {
        val cal = Calendar.getInstance()
        cal[Calendar.DAY_OF_MONTH] = cal.getActualMinimum(Calendar.DAY_OF_MONTH)
        return cal.time
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?,
    ): View {
        Log.i(TAG, "onCreateView: ")

//        currentFragment = javaClass.simpleName

        callGetToken()

        vi = inflater.inflate(R.layout.fragment_proc_and_stocks, container, false)

        initViews()


        return vi

    }

    private fun initViews() {

        setDefaultDateToProcurement()
        setDefaultDateToStocks()

        vi.btnProcurement.setOnClickListener {
            setDefaultDateToProcurement()
            callProcurement()
        }

        vi.btnStocks.setOnClickListener {
            setDefaultDateToStocks()
            callStocks()
        }

        vi.ivCPTCalendar.setOnClickListener {
            val postCalendarDialog = SalesCalendarFilterDialog(
                    activity,
                    "CustomPostDate"
            ) { startDateString, endDateString, dateType ->
                cptFromDate = startDateString
                cptToDate = endDateString
                callCPT()
            }
            postCalendarDialog.setCancelable(false)
            postCalendarDialog.show()
        }

        vi.ivIEPCalendar.setOnClickListener {
            val postCalendarDialog = SalesCalendarFilterDialog(
                    activity,
                    "CustomPostDate"
            ) { startDateString, endDateString, dateType ->
                iepFromDate = startDateString
                iepToDate = endDateString
                callIEP()
            }
            postCalendarDialog.setCancelable(false)
            postCalendarDialog.show()
        }

        vi.ivSelfCalendar.setOnClickListener {
            val postCalendarDialog = SalesCalendarFilterDialog(
                    activity,
                    "CustomPostDate"
            ) { startDateString, endDateString, dateType ->
                selfFromDate = startDateString
                selfToDate = endDateString
                callSELF()
            }
            postCalendarDialog.setCancelable(false)
            postCalendarDialog.show()
        }

        vi.ivCalendarStoreStock.setOnClickListener {
            val postCalendarDialog = SalesCalendarFilterDialog(
                    activity,
                    "CustomPostDate"
            ) { startDateString, endDateString, dateType ->
                storeStockFromDate = startDateString
                storeStockToDate = endDateString
                callStoreStocks()
            }
            postCalendarDialog.setCancelable(false)
            postCalendarDialog.show()
        }
        vi.ivCalendarBookedStock.setOnClickListener {
            val postCalendarDialog = SalesCalendarFilterDialog(
                    activity,
                    "CustomPostDate"
            ) { startDateString, endDateString, dateType ->
                bookedStockFromDate = startDateString
                bookedStockToDate = endDateString
                callBookedStocks()
            }
            postCalendarDialog.setCancelable(false)
            postCalendarDialog.show()
        }


        vi.tvStoreStockOMS.setOnClickListener { loadFragment(RDMStocksListFragment(token, code, getWhereList("commercial_vehicle"), getWhereInList("stock_source"), "", "")) }
        vi.tvPaidUpStockOMS.setOnClickListener { loadFragment(RDMStocksListFragment(token, code, getWhereList("commercial_vehicle"), getWhereInList("paidup"), "", "")) }
        vi.tvParkAndSellStockOMS.setOnClickListener { loadFragment(RDMStocksListFragment(token, code, getWhereList("private_vehicle"), getWhereInList("parkAndSell"), "", "")) }
        vi.tvStockCertifiedOMS.setOnClickListener { loadFragment(RDMStocksListFragment(token, code, getWhereList("certified_private"), getWhereInList("stock_source"), "", KEY_CERTIFIED)) }
        vi.tvStocksWithoutImageOMS.setOnClickListener { loadFragment(RDMStocksListFragment(token, code, getWhereList("commercial_vehicle"), getWhereInList("withoutImage"), "", "")) }
        vi.tvStocksAbove30DaysOMS.setOnClickListener { loadFragment(RDMStocksListFragment(token, code, getWhereList("above30"), getWhereInList("stock_source"), "", "")) }
        vi.tvStocksAbove60DaysOMS.setOnClickListener { loadFragment(RDMStocksListFragment(token, code, getWhereList("above60"), getWhereInList("stock_source"), "", "")) }
        vi.tvStocksOnMFCWebsiteOMS.setOnClickListener { loadFragment(RDMStocksListFragment(token, code, getWhereList("visibleOnMFC"), getWhereInList("stock_source"), "", "")) }

        vi.tvBookingThroughOMSOMS.setOnClickListener { } //no

        vi.tvTotalStocks.setOnClickListener { loadFragment(RDMStocksListFragment(token, code, getWhereList(""), getWhereInList("stock_source"), "", "")) }
        vi.tvTotalCertifiedStocks.setOnClickListener { loadFragment(RDMStocksListFragment(token, code, getWhereList("certified"), getWhereInList("stock_source"), "", KEY_CERTIFIED)) }
        vi.tvTotalBookedStocks.setOnClickListener { loadFragment(RDMStocksListFragment(token, code, getWhereList("is_booked"), getWhereInList("is_sold"), "", KEY_BOOKSTOCK)) }

        val countViewModel = ViewModelProvider.NewInstanceFactory().create(CountViewModel::class.java)
        countViewModel.countData.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            loadCount(it)
        })
    }

    override fun onResume() {
        super.onResume()
        SpinnerManager.hideSpinner(context)
    }

    fun getFirstDay(d: Date?): String {
        var formatedDate = ""
        try {
            val calendar = Calendar.getInstance()
            calendar.time = d
            calendar[Calendar.DAY_OF_MONTH] = 1
            val mdate = calendar.time
            val sdf = SimpleDateFormat("yyyy-MM-dd")
            formatedDate = sdf.format(mdate)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return formatedDate
    }

    private fun getWhereList(str: String): ArrayList<RDMStocksRequest.Where> {
        val whereList = ArrayList<RDMStocksRequest.Where>()

        when (str) {


            "certified" -> {
                val where = RDMStocksRequest.Where()
                where.column = "is_certified"
                where.operator = "="
                where.value = "true"
                whereList.add(where)

                /*val where1 = RDMStocksRequest.Where()
                where1.column = "commercial_vehicle"
                where1.operator = "="
                where1.value = "false"
                whereList.add(where1)*/
            }
            "certified_private" -> {
                val where = RDMStocksRequest.Where()
                where.column = "is_certified"
                where.operator = "="
                where.value = "true"
                whereList.add(where)

                val where1 = RDMStocksRequest.Where()
                where1.column = "commercial_vehicle"
                where1.operator = "="
                where1.value = "false"
                whereList.add(where1)
            }
            "above30" -> {
                val where = RDMStocksRequest.Where()
                where.column = "stock_age"
                where.operator = ">="
                where.value = "30"
                whereList.add(where)

                val where1 = RDMStocksRequest.Where()
                where1.column = "commercial_vehicle"
                where1.operator = "="
                where1.value = "false"
                whereList.add(where1)
            }
            "above60" -> {
                val where = RDMStocksRequest.Where()
                where.column = "stock_age"
                where.operator = ">="
                where.value = "60"
                whereList.add(where)

                val where1 = RDMStocksRequest.Where()
                where1.column = "commercial_vehicle"
                where1.operator = "="
                where1.value = "false"
                whereList.add(where1)
            }
            "visibleOnMFC" -> {
                val where = RDMStocksRequest.Where()
                where.column = "photo_count"
                where.operator = ">"
                where.value = "0"
                whereList.add(where)

                val where2 = RDMStocksRequest.Where()
                where2.column = "commercial_vehicle"
                where2.operator = "="
                where2.value = "false"
                whereList.add(where2)
            }
            "is_booked" -> {
                val where = RDMStocksRequest.Where()
                where.column = "is_booked"
                where.operator = "="
                where.value = "true"
                whereList.add(where)

                val where1 = RDMStocksRequest.Where()
                where1.column = "book_date"
                where1.operator = ">="
                where1.value = getFirstDay(Date())
                whereList.add(where1)
            }
            "private_vehicle" -> {
                val where2 = RDMStocksRequest.Where()
                where2.column = "private_vehicle"
                where2.operator = "="
                where2.value = "true"
                whereList.add(where2)
            }

            "commercial_vehicle" -> {
                val where2 = RDMStocksRequest.Where()
                where2.column = "commercial_vehicle"
                where2.operator = "="
                where2.value = "false"
                whereList.add(where2)
            }
        }

        return whereList
    }


    private fun getWhereInList(str: String): ArrayList<RDMStocksRequest.WhereIn> {
        val whereInList = ArrayList<RDMStocksRequest.WhereIn>()
        val whereIn1 = RDMStocksRequest.WhereIn()
        whereIn1.column = "dealer_code"
        whereIn1.values = arrayOf("$code")
        whereInList.add(whereIn1)

        when (str) {
            "paidup" -> {
                val whereIn2 = RDMStocksRequest.WhereIn()
                whereIn2.column = "stock_source"
                whereIn2.values = arrayOf("mfc")
                whereInList.add(whereIn2)
            }
            "parkAndSell" -> {
                val whereIn2 = RDMStocksRequest.WhereIn()
                whereIn2.column = "stock_source"
                whereIn2.values = arrayOf("p&s")
                whereInList.add(whereIn2)
            }
            "withoutImage" -> {
                val whereIn2 = RDMStocksRequest.WhereIn()
                whereIn2.column = "photo_count"
                whereIn2.values = arrayOf("0")
                whereInList.add(whereIn2)

                val whereIn3 = RDMStocksRequest.WhereIn()
                whereIn3.column = "stock_source"
                whereIn3.values = arrayOf("mfc", "p&s")
                whereInList.add(whereIn3)
            }

            "stock_source" -> {
                val whereIn3 = RDMStocksRequest.WhereIn()
                whereIn3.column = "stock_source"
                whereIn3.values = arrayOf("mfc", "p&s")
                whereInList.add(whereIn3)
            }

            "is_sold" -> {
                val whereIn3 = RDMStocksRequest.WhereIn()
                whereIn3.column = "is_sold"
                whereIn3.values = arrayOf("1", "0")
                whereInList.add(whereIn3)

                val whereIn4 = RDMStocksRequest.WhereIn()
                whereIn4.column = "stock_source"
                whereIn4.values = arrayOf("mfc", "p&s")
                whereInList.add(whereIn4)
            }
        }

        return whereInList
    }

    private fun loadCount(count: remotedealer.dashboard.model.CountResponse) {
        Log.i(TAG, "loadCount: " + count.totalPrivateLeadsCount)
        tvTotalStocks.text= "Total Stocks (${count.totalStocksCount})"
        tvTotalCertifiedStocks.text = "Certified Stocks(${count.certifiedStocksCount})"
        tvTotalBookedStocks.text = "Booked Stocks(${count.bookedStocksCount})"
    }

    private fun loadFragment(fragment: Fragment) = NavigationUtility.addFragment(fragment, activity, fragment.tag)

//            activity?.supportFragmentManager?.beginTransaction()?.apply {
//                replace(R.id.flFragment, fragment)
//                commit()
//            }


    private fun callProcurement() {

        btnProcurement.setTextColor(resources.getColor(R.color.white))
        btnProcurement.setBackgroundDrawable(resources.getDrawable(R.drawable.rounded_event_blue_bg))
        btnStocks.setTextColor(resources.getColor(R.color.edit_text_light_black_color))
        btnStocks.setBackgroundDrawable(resources.getDrawable(R.drawable.rounded_bg_for_btn_not_selected))

        layoutCPT.visibility = View.VISIBLE
        layoutIEP.visibility = View.VISIBLE
        layoutSELF.visibility = View.VISIBLE
        border1.visibility = View.VISIBLE
        border2.visibility = View.VISIBLE

        layoutStoreStocks.visibility = View.GONE
        layoutBookedStocks.visibility = View.GONE

        callCPT()
        callIEP()
        callSELF()
    }

    private fun callCPT() {
        val req = ProcurementRequest()
        req.code = code
        req.fromDate = cptFromDate
        req.toDate = cptToDate

        SpinnerManager.showSpinner(context)
        retroInterface.getFromWeb(req, URL_END_PROCUREMENT_CPT, "Bearer $token").enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                SpinnerManager.hideSpinner(context)

                try {
                    val strRes = Gson().toJson(response.body())
                    val strModel = Gson().fromJson(strRes, ProcurementResponse::class.java)

                    Log.i(TAG, "onResponse: " + strModel.data.size)

                    parseCPTData(strModel.data)
                } catch (e: Exception) {
                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(context)
            }
        })
    }

    private fun callIEP() {

        val req = ProcurementRequest()
        req.code = code
        req.fromDate = iepFromDate
        req.toDate = iepToDate

        SpinnerManager.showSpinner(context)
        retroInterface.getFromWeb(req, URL_END_PROCUREMENT_IEP, "Bearer $token").enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                SpinnerManager.hideSpinner(context)

                try {
                    val strRes = Gson().toJson(response.body())
                    val strModel = Gson().fromJson(strRes, ProcurementResponse::class.java)

                    Log.i(TAG, "onResponse: " + strModel.data.size)

                    parseIEPData(strModel.data)
                } catch (e: Exception) {
                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(context)
            }
        })

    }

    private fun callSELF() {
        val req = ProcurementRequest()
        req.code = code
        req.fromDate = selfFromDate
        req.toDate = selfToDate

        SpinnerManager.showSpinner(context)
        RetroBase.retroInterface.getFromWeb(req, RetroBase.URL_END_PROCUREMENT_SELF, "Bearer $token").enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                SpinnerManager.hideSpinner(context)

                try {
                    val strRes = Gson().toJson(response.body())
                    val strModel = Gson().fromJson(strRes, ProcurementResponse::class.java)

                    Log.i(TAG, "onResponse: " + strModel.data.size)

                    parseSelfData(strModel.data)
                } catch (e: Exception) {
                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(context)
            }
        })
    }

    private fun parseSelfData(data: ArrayList<ProcureItem>) {

        try {

            if (data[0].oms == "NA" &&
                    data[0].rdr == "NA" &&
                    data[0].particulars == "NA" &&
                    data[0].target == "NA") {
                return
            }

            for (item in data) {
                if (item.particulars.equals("SelfProcurement", ignoreCase = true)) {
                    tvSelfProcurementOMS.text = item.oms
                    tvSelfProcurementRDR.text = item.rdr
                }

                if (item.particulars.equals("TotalProcurement", ignoreCase = true)) {
                    tvTotalProcurementOMS.text = item.oms
                    tvTotalProcurementRDR.text = item.rdr
                }

                if (item.particulars.equals("ProcurementthroughAMsupport", ignoreCase = true)) {
                    tvProcuredThroughAMSupportOMS.text = item.oms
                    tvProcuredThroughAMSupportRDR.text = item.rdr
                }
            }


        } catch (e: Exception) {
            Log.e(TAG, "parseSelfData: " + e.message)
        }
    }


    private fun callStocks() {

        btnProcurement.setTextColor(resources.getColor(R.color.edit_text_light_black_color))
        btnProcurement.setBackgroundDrawable(resources.getDrawable(R.drawable.rounded_bg_for_btn_not_selected))
        btnStocks.setTextColor(resources.getColor(R.color.white))
        btnStocks.setBackgroundDrawable(resources.getDrawable(R.drawable.rounded_event_blue_bg))

        layoutCPT.visibility = View.GONE
        layoutIEP.visibility = View.GONE
        layoutSELF.visibility = View.GONE
        border1.visibility = View.GONE
        border2.visibility = View.GONE

        layoutStoreStocks.visibility = View.VISIBLE
        layoutBookedStocks.visibility = View.VISIBLE

        callStoreStocks()
        callBookedStocks()

    }

    private fun callBookedStocks() {
        val req = ProcurementRequest()
        req.code = code
        req.fromDate = bookedStockFromDate
        req.toDate = bookedStockToDate

        SpinnerManager.showSpinner(context)
        retroInterface.getFromWeb(req, RetroBase.URL_END_BOOKED_STOCKS, "Bearer $token").enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                SpinnerManager.hideSpinner(context)

                try {
                    val strRes = Gson().toJson(response.body())
                    val strModel = Gson().fromJson(strRes, ProcurementResponse::class.java)

                    Log.i(TAG, "onResponse: " + strModel.data.size)

                    parseBookedStocks(strModel.data)
                } catch (e: Exception) {
                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(context)
            }
        })
    }

    private fun parseBookedStocks(data: ArrayList<ProcureItem>) {

        try {

            if (data[1].oms == "NA" &&
                    data[1].rdr == "NA" &&
                    data[1].particulars == "NA" &&
                    data[1].target == "NA") {
                return
            }


            val bookTOMS = data[1].oms
            val bookTRDR = data[1].rdr

            if (bookTOMS.contains('.')) {
                val bookOMS = bookTOMS.split(".")
                tvBookingThroughOMSOMS.text = bookOMS[1]
            } else {
                tvBookingThroughOMSOMS.text = bookTOMS
            }

            if (bookTRDR.contains('.')) {
                val bookRMD = bookTRDR.split(".")
                tvBookingThroughOMSRDR.text = bookRMD[1]
            } else {
                tvBookingThroughOMSRDR.text = bookTRDR
            }

        } catch (e: Exception) {
            Log.e(TAG, "parseBookedStocks: " + e.message)
        }
    }

    private fun callStoreStocks() {

        val req = ProcurementRequest()
        req.code = code
        req.fromDate = storeStockFromDate
        req.toDate = storeStockToDate

        SpinnerManager.showSpinner(context)
        retroInterface.getFromWeb(req, URL_END_STORE_STOCKS, "Bearer $token").enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                SpinnerManager.hideSpinner(context)

                try {
                    val strRes = Gson().toJson(response.body())
                    val strModel = Gson().fromJson(strRes, ProcurementResponse::class.java)

                    Log.i(TAG, "onResponse: " + strModel.data.size)

                    parseStoreStocks(strModel.data)

                } catch (e: Exception) {
                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(context)
            }
        })

    }

    private fun parseStoreStocks(data: ArrayList<ProcureItem>) {

        try {

            if (data[0].oms == "NA" &&
                    data[0].rdr == "NA" &&
                    data[0].particulars == "NA" &&
                    data[0].target == "NA") {
                return
            }


            tvStoreStockOMS.text = data[0].oms
            tvStoreStockRDR.text = data[0].rdr

            tvPaidUpStockOMS.text = data[1].oms
            tvPaidUpStockRDR.text = data[1].rdr

            tvParkAndSellStockOMS.text = data[2].oms
            tvParkAndSellStockRDR.text = data[2].rdr

            tvStockCertifiedOMS.text = data[3].oms
            tvStockCertifiedRDR.text = data[3].rdr

            tvStocksWithoutImageOMS.text = data[4].oms
            tvStocksWithoutImageRDR.text = data[4].rdr

            tvStocksAbove30DaysOMS.text = data[5].oms
            tvStocksAbove30DaysRDR.text = data[5].rdr

            tvStocksAbove60DaysOMS.text = data[6].oms
            tvStocksAbove60DaysRDR.text = data[6].rdr

            tvStocksOnMFCWebsiteOMS.text = data[7].oms
            tvStocksOnMFCWebsiteRDR.text = data[7].rdr

        } catch (e: Exception) {
            Log.e(TAG, "parseStoreStocks: " + e.message)
        }

    }


    private fun parseCPTData(data: ArrayList<ProcureItem>) {

        try {

            if (data[0].oms == "NA" &&
                    data[0].rdr == "NA" &&
                    data[0].particulars == "NA" &&
                    data[0].target == "NA") {
                return
            }

            tvProcuredThroughCPTOMS.text = data[0].oms
            tvProcuredThroughCPTRDR.text = data[0].rdr

            tvNoOfActionsOMS.text = data[1].oms
            tvNoOfActionsRDR.text = data[1].rdr

            tvVehicleNotAsperRequirementOMS.text = data[2].oms
            tvVehicleNotAsperRequirementRDR.text = data[2].rdr

            tvNoOfBidsOMS.text = data[3].oms
            tvNoOfBidsRDR.text = data[3].rdr


            tvNoOfHighestBidsOMS.text = data[4].oms
            tvNoOfHighestBidsRDR.text = data[4].rdr

            tvNoOfVehiclesUnderNegotiationOMS.text = data[5].oms
            tvNoOfVehiclesUnderNegotiationRDR.text = data[5].rdr

        } catch (e: Exception) {
            Log.i(TAG, "parseCPTData: " + e.message)
        }
    }


    private fun parseIEPData(data: ArrayList<ProcureItem>) {

        try {

            if (data[0].oms == "NA" &&
                    data[0].rdr == "NA" &&
                    data[0].particulars == "NA" &&
                    data[0].target == "NA") {
                return
            }

            tvProcuredThroughIEPOMS.text = data[0].oms
            tvProcuredThroughIEPRDR.text = data[0].rdr

            tvNoOfActionsIEPOMS.text = data[1].oms
            tvNoOfActionsIEPRDR.text = data[1].rdr

            tvVehicleNotAsperRequirementIEPOMS.text = data[2].oms
            tvVehicleNotAsperRequirementIEPRDR.text = data[2].rdr

            tvNoOfBidsIEPOMS.text = data[3].oms
            tvNoOfBidsIEPRDR.text = data[3].rdr

            tvNoOfHighestBidsIEPOMS.text = data[4].oms
            tvNoOfHighestBidsIEPRDR.text = data[4].rdr

            tvNoOfVehiclesUnderNegotiationIEPOMS.text = data[5].oms
            tvNoOfVehiclesUnderNegotiationIEPRDR.text = data[5].rdr

        } catch (e: Exception) {
            Log.i(TAG, "parseIEPData: " + e.message)
        }
    }

    private fun callGetToken() {

        SpinnerManager.showSpinner(context)
        retroInterface.getFromWeb(RDRTokenRequest(), URL_END_GET_TOKEN).enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                SpinnerManager.hideSpinner(context)

                try {
                    val strRes = Gson().toJson(response.body())
                    val resModel = Gson().fromJson(strRes, RDRTokenResponse::class.java)

                    token = resModel.token

                    Log.i(TAG, "onResponse: " + resModel.token)

                    callProcurement()
                } catch (e: Exception) {
                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(context)
            }

        })

    }


    fun setDefaultDateToProcurement() {

        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")

        cptFromDate = simpleDateFormat.format(getFirstDateOfCurrentMonth())
        cptToDate = simpleDateFormat.format(Date())

        iepFromDate = simpleDateFormat.format(getFirstDateOfCurrentMonth())
        iepToDate = simpleDateFormat.format(Date())

        selfFromDate = simpleDateFormat.format(getFirstDateOfCurrentMonth())
        selfToDate = simpleDateFormat.format(Date())

    }

    fun setDefaultDateToStocks() {

        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")

        bookedStockFromDate = simpleDateFormat.format(getFirstDateOfCurrentMonth())
        bookedStockToDate = simpleDateFormat.format(Date())

        storeStockFromDate = simpleDateFormat.format(getFirstDateOfCurrentMonth())
        storeStockToDate = simpleDateFormat.format(Date())
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                ProcAndSalesFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}