package remotedealer.rdrform.ui

/**
 * Created by Shitalkumar on 8/12/20
 */
interface AddRemarksClickListener {
    fun onRemarkAdded(remark: String, actionItem : String, category : String)
}