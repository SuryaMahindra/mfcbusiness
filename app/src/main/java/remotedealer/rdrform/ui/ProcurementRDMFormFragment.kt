package remotedealer.rdrform.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.mfcwl.mfc_dealer.R
import com.mfcwl.mfc_dealer.Utility.SpinnerManager
import kotlinx.android.synthetic.main.activity_rdm_home.*
import kotlinx.android.synthetic.main.fragment_procurement_rdm_form.*
import kotlinx.android.synthetic.main.rdm_form_element_custom_view.view.*
import remotedealer.model.token.RDRTokenRequest
import remotedealer.model.token.RDRTokenResponse
import remotedealer.rdrform.model.ProcurementRDMFormInfoModel
import remotedealer.retrofit.RetroBase
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.math.ceil


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * Created by Shitalkumar on 01/12/2020
 */
class ProcurementRDMFormFragment : BaseRDMFormFragment() {

    private var param2: String? = null
    private var procurementIep = 0
    private var procurementCPT = 0
    private var procurementThruAmSupport = 0
    private var selfProcurement = 0
    private var procurementRDMFormResponse = ProcurementRDMFormInfoModel()
    private var oldTotalProcurementActual = 0
    private var oldIepCptCountActual = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            isViewDetails = it.getBoolean(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        Log.i(TAG, "onCreateView: ")
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_procurement_rdm_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()

        try {
            if (!isViewDetails) {
                callGetToken()
            } else {
                val savedData = preferenceManager.readString(PROCUREMENT_RDM_INFO, "")
                if (savedData.trim().isNotEmpty()) {
                    procurementRDMFormResponse = Gson().fromJson(savedData, ProcurementRDMFormInfoModel::class.java)
                    updateUI()
                }
            }
        } catch (e: Exception) {
            Log.e(TAG, e.message.toString())
        }
    }

    private fun initView() {
        // No of auctions
        layoutNoOfAuction.textViewTitle.text = getString(R.string.title_no_of_auctions)
        setEditTextEditable(layoutNoOfAuction.editTextFirstField, false, null)
        setEditTextEditable(layoutNoOfAuction.editTextSecondField, false, null)

        // Vehicle not as per requirement
        layoutVehicleNotAsPerRequirement.textViewTitle.text = getString(R.string.title_vehicle_not_as_per_requirement)
        setEditTextEditable(layoutVehicleNotAsPerRequirement.editTextFirstField, true, layoutVehicleNotAsPerRequirement.textViewFirstField)
        setEditTextEditable(layoutVehicleNotAsPerRequirement.editTextSecondField, true, layoutVehicleNotAsPerRequirement.textViewSecondField)

        // No of bids
        layoutNoOfBids.textViewTitle.text = getString(R.string.title_no_of_bids)
        setEditTextEditable(layoutNoOfBids.editTextFirstField, false, null)
        setEditTextEditable(layoutNoOfBids.editTextSecondField, false, null)

        // No of highest bids
        layoutNoOfHighestBids.textViewTitle.text = getString(R.string.title_no_of_highest_bids)
        setEditTextEditable(layoutNoOfHighestBids.editTextFirstField, false, null)
        setEditTextEditable(layoutNoOfHighestBids.editTextSecondField, false, null)

        // No of vehicle under negotiation
        layoutNoOfVehicleUnderNegotiation.textViewTitle.text = getString(R.string.title_no_of_vehicle_under_negotiation)
        setEditTextEditable(layoutNoOfVehicleUnderNegotiation.editTextFirstField, true, layoutNoOfVehicleUnderNegotiation.textViewFirstField)
        setEditTextEditable(layoutNoOfVehicleUnderNegotiation.editTextSecondField, true, layoutNoOfVehicleUnderNegotiation.textViewSecondField)

        // Procurement
        layoutProcurement.textViewTitle.text = getString(R.string.title_procurement)
        setEditTextEditable(layoutProcurement.editTextFirstField, true, layoutProcurement.textViewFirstField)
        setEditTextEditable(layoutProcurement.editTextSecondField, true, layoutProcurement.textViewSecondField)

        // Procurement Thru AM Support
        layoutProcurementThruAMSupport.textViewTitle.text = getString(R.string.title_procurement_through_am_support)
        layoutProcurementThruAMSupport.linLayDouble.visibility = View.GONE
        layoutProcurementThruAMSupport.linLayFull.visibility = View.VISIBLE
        setEditTextEditable(layoutProcurementThruAMSupport.editTextFullLayout, true, layoutProcurementThruAMSupport.textViewFullLayout)

        // Self Procurement
        layoutSelfProcurement.textViewTitle.text = getString(R.string.title_self_procurement)
        layoutSelfProcurement.linLayDouble.visibility = View.GONE
        layoutSelfProcurement.linLayFull.visibility = View.VISIBLE
        setEditTextEditable(layoutSelfProcurement.editTextFullLayout, true, layoutSelfProcurement.textViewFullLayout)

        // Total Procurement
        layoutTotalProcurement.textViewTitle.text = getString(R.string.title_total_procurement)
        layoutTotalProcurement.linLayDouble.visibility = View.GONE
        layoutTotalProcurement.linLayFull.visibility = View.VISIBLE
        setEditTextEditable(layoutTotalProcurement.editTextFullLayout, false, null)

        // Total Procurement MTD
        layoutTotalProcurementMTD.textViewTitle.text = getString(R.string.title_total_procurement)
        layoutTotalProcurementMTD.textViewFirstField.text = getString(R.string.title_number)
        layoutTotalProcurementMTD.textViewSecondField.text = getString(R.string.title_mtd)
        layoutTotalProcurementMTD.textViewAchieved.visibility = View.VISIBLE
        layoutTotalProcurementMTD.linLayTargetNo.visibility = View.VISIBLE
        setEditTextEditable(layoutTotalProcurementMTD.editTextFirstField, false, null)
        setEditTextEditable(layoutTotalProcurementMTD.editTextSecondField, false, null)

        // IEP = CPT count
        layoutIEPCPTCounts.textViewTitle.text = getString(R.string.title_iep_cpt_count)
        layoutIEPCPTCounts.textViewFirstField.text = getString(R.string.title_number)
        layoutIEPCPTCounts.textViewSecondField.text = getString(R.string.title_mtd)
        layoutIEPCPTCounts.textViewAchieved.visibility = View.VISIBLE
        layoutIEPCPTCounts.linLayTargetNo.visibility = View.VISIBLE
        setEditTextEditable(layoutIEPCPTCounts.editTextFirstField, false, null)
        setEditTextEditable(layoutIEPCPTCounts.editTextSecondField, false, null)

        if (isViewDetails)
            buttonNext.visibility = View.GONE

        buttonNext.setOnClickListener {
            RDMHomeActivity.tvSavedLbl.visibility =  View.VISIBLE
            rdmFormClicksListener.onNextButtonClick(getFilledData())
        }

        layoutProcurement.editTextFirstField.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                //if (s.toString().isNotEmpty()) {
                setValuesToCalculationVariables()
                updateFieldsOnDataChange()
                //}
            }
        })

        layoutProcurement.editTextSecondField.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                //if (s.toString().isNotEmpty()) {
                setValuesToCalculationVariables()
                updateFieldsOnDataChange()
                //}
            }
        })

        layoutProcurementThruAMSupport.editTextFullLayout.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                //if (s.toString().isNotEmpty()) {
                setValuesToCalculationVariables()
                updateFieldsOnDataChange()
                //}
            }
        })

        layoutSelfProcurement.editTextFullLayout.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                //if (s.toString().isNotEmpty()) {
                setValuesToCalculationVariables()
                updateFieldsOnDataChange()
                //}
            }
        })

        layoutSelfProcurement.editTextFullLayout.setOnKeyListener(onKeyListener)
        layoutVehicleNotAsPerRequirement.editTextFirstField.setOnKeyListener(onKeyListener)
        layoutVehicleNotAsPerRequirement.editTextSecondField.setOnKeyListener(onKeyListener)
        layoutNoOfVehicleUnderNegotiation.editTextFirstField.setOnKeyListener(onKeyListener)
        layoutNoOfVehicleUnderNegotiation.editTextSecondField.setOnKeyListener(onKeyListener)
        layoutProcurement.editTextFirstField.setOnKeyListener(onKeyListener)
        layoutProcurement.editTextSecondField.setOnKeyListener(onKeyListener)
        layoutProcurementThruAMSupport.editTextFullLayout.setOnKeyListener(onKeyListener)
    }

    private val onKeyListener = View.OnKeyListener { _, _, event ->
        if (event.action == KeyEvent.ACTION_UP) {
            processButtonByTextLength()
        }

        false
    }

    private fun processButtonByTextLength() {
        val vehiclesNotAsPerRequirementIEP = layoutVehicleNotAsPerRequirement.editTextFirstField.text.toString().trim().isEmpty()
        val vehiclesNotAsPerRequirementCEP = layoutVehicleNotAsPerRequirement.editTextSecondField.text.toString().trim().isEmpty()
        val noOfVehicleUnderNegotiationIEP = layoutNoOfVehicleUnderNegotiation.editTextFirstField.text.toString().trim().isEmpty()
        val noOfVehicleUnderNegotiationCPT = layoutNoOfVehicleUnderNegotiation.editTextSecondField.text.toString().trim().isEmpty()
        val procurementIEP = layoutProcurement.editTextFirstField.text.toString().trim().isEmpty()
        val procurementCPT = layoutProcurement.editTextSecondField.text.toString().trim().isEmpty()
        val procurementThroughAMSupport = layoutProcurementThruAMSupport.editTextFullLayout.text.toString().trim().isEmpty()
        val selfProcurement = layoutSelfProcurement.editTextFullLayout.text.toString().trim().isEmpty()

        if (vehiclesNotAsPerRequirementCEP || vehiclesNotAsPerRequirementIEP || noOfVehicleUnderNegotiationCPT || noOfVehicleUnderNegotiationIEP
                || procurementCPT || procurementIEP || procurementThroughAMSupport || selfProcurement) {
            buttonNext.isEnabled = false
            buttonNext.setBackgroundDrawable(resources.getDrawable(R.drawable.rounded_button_background_dark_grey_disabled))
        } else {
            buttonNext.isEnabled = true
            buttonNext.setBackgroundDrawable(resources.getDrawable(R.drawable.rounded_button_background_blue_enabled))
        }

        var count = 0;
        if (!vehiclesNotAsPerRequirementCEP)
            count++
        if (!vehiclesNotAsPerRequirementIEP)
            count++
        if (!noOfVehicleUnderNegotiationCPT)
            count++
        if (!noOfVehicleUnderNegotiationIEP)
            count++
        if (!procurementCPT)
            count++
        if (!procurementIEP)
            count++
        if (!procurementThroughAMSupport)
            count++
        if (!selfProcurement)
            count++

        val formCompletionPercentage = (100 * count) / 8
        rdmFormClicksListener.onFormFillListener("Procurement", formCompletionPercentage.toString())
        preferenceManager.writeString(PROCUREMENT_RDM_INFO, getFilledData())
    }

    private fun updateUI() {
        try {
            oldTotalProcurementActual = procurementRDMFormResponse.totalProcurementActual.toInt()
            oldIepCptCountActual = procurementRDMFormResponse.iepAndCptProcuredActual.toInt()

            // No of auctions
            layoutNoOfAuction.editTextFirstField.setText(procurementRDMFormResponse.liveAuctionsIep)
            layoutNoOfAuction.editTextSecondField.setText(procurementRDMFormResponse.liveAuctionsCpt)

            // No of bids
            layoutNoOfBids.editTextFirstField.setText(procurementRDMFormResponse.auctionsBidIep)
            layoutNoOfBids.editTextSecondField.setText(procurementRDMFormResponse.auctionsBidCpt)

            // No of highest bids
            layoutNoOfHighestBids.editTextFirstField.setText(procurementRDMFormResponse.auctionsWonIep)
            layoutNoOfHighestBids.editTextSecondField.setText(procurementRDMFormResponse.auctionsWonCpt)

            // No of vehicle under negotiation
            layoutNoOfVehicleUnderNegotiation.editTextFirstField.setText(procurementRDMFormResponse.negotiationsIep)
            layoutNoOfVehicleUnderNegotiation.editTextSecondField.setText(procurementRDMFormResponse.negotiationsCpt)

            // Procurement
            layoutProcurement.editTextFirstField.setText(procurementRDMFormResponse.procuredActualIep)
            layoutProcurement.editTextSecondField.setText(procurementRDMFormResponse.procuredActualCpt)

            layoutVehicleNotAsPerRequirement.editTextFirstField.setText(procurementRDMFormResponse.vehicleNotPerRequirementIep)
            layoutVehicleNotAsPerRequirement.editTextSecondField.setText(procurementRDMFormResponse.vehicleNotPerRequirementCpt)

            layoutProcurementThruAMSupport.editTextFullLayout.setText(procurementRDMFormResponse.procuredAmSupport)
            layoutSelfProcurement.editTextFullLayout.setText(procurementRDMFormResponse.selfProcurement)

            setValuesToCalculationVariables()
            updateFieldsOnDataChange()
            processButtonByTextLength()
        } catch (e: Exception) {
            Log.e(TAG, e.message.toString())
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updateFieldsOnDataChange() {
        // Total Procurement
        layoutTotalProcurement.editTextFullLayout.setText(calculateTotalProcurement())

        // Total Procurement MTD
        if (procurementRDMFormResponse.totalProcurementTarget.isNotEmpty() && procurementRDMFormResponse.totalProcurementTarget != "0") {
            val percentageTotalProcurementMTD = (100 * procurementRDMFormResponse.totalProcurementActual.toDouble()) / procurementRDMFormResponse.totalProcurementTarget.toDouble()
            setAchievedPercentageColor(layoutTotalProcurementMTD.textViewAchieved, percentageTotalProcurementMTD)
        }
        layoutTotalProcurementMTD.editTextFirstField.setText(calculateTotalProcurement())
        if(procurementRDMFormResponse.totalProcurementActual != ""){
            layoutTotalProcurementMTD.editTextSecondField.setText("${ceil(procurementRDMFormResponse.totalProcurementActual.toDouble()).toInt()}")

        }

        if(procurementRDMFormResponse.totalProcurementTarget != ""){
            layoutTotalProcurementMTD.textViewTargetNo.text = "Target : ${ceil(procurementRDMFormResponse.totalProcurementTarget.toDouble()).toInt()}"

        }

        // IEP = CPT count
        if (procurementRDMFormResponse.iepAndCptProcuredTarget.isNotEmpty() && procurementRDMFormResponse.iepAndCptProcuredTarget != "0") {
            val percentageIEPCPTCounts = (100 * procurementRDMFormResponse.iepAndCptProcuredActual.toDouble()) / procurementRDMFormResponse.iepAndCptProcuredTarget.toDouble()
            setAchievedPercentageColor(layoutIEPCPTCounts.textViewAchieved, percentageIEPCPTCounts)
        }
        layoutIEPCPTCounts.editTextFirstField.setText("${procurementCPT + procurementIep}")
        if(procurementRDMFormResponse.iepAndCptProcuredActual != ""){
            layoutIEPCPTCounts.editTextSecondField.setText("" + ceil(procurementRDMFormResponse.iepAndCptProcuredActual.toDouble()).toInt())

        }
        if(procurementRDMFormResponse.iepAndCptProcuredTarget != ""){
            layoutIEPCPTCounts.textViewTargetNo.text = "Target : ${ceil(procurementRDMFormResponse.iepAndCptProcuredTarget.toDouble()).toInt()}"

        }
    }

    private fun getFilledData(): String {
        procurementRDMFormResponse.vehicleNotPerRequirementIep = layoutVehicleNotAsPerRequirement.editTextFirstField.text.toString()
        procurementRDMFormResponse.vehicleNotPerRequirementCpt = layoutVehicleNotAsPerRequirement.editTextSecondField.text.toString()

        procurementRDMFormResponse.negotiationsIep = layoutNoOfVehicleUnderNegotiation.editTextFirstField.text.toString()
        procurementRDMFormResponse.negotiationsCpt = layoutNoOfVehicleUnderNegotiation.editTextSecondField.text.toString()

        procurementRDMFormResponse.procuredActualIep = layoutProcurement.editTextFirstField.text.toString()
        procurementRDMFormResponse.procuredActualCpt = layoutProcurement.editTextSecondField.text.toString()

        procurementRDMFormResponse.procuredAmSupport = layoutProcurementThruAMSupport.editTextFullLayout.text.toString()
        procurementRDMFormResponse.selfProcurement = layoutSelfProcurement.editTextFullLayout.text.toString()

        procurementRDMFormResponse.iepAndCptProcuredToday = layoutIEPCPTCounts.editTextFirstField.text.toString()
        procurementRDMFormResponse.totalProcurementToday = calculateTotalProcurement()
        return Gson().toJson(procurementRDMFormResponse)
    }

    private fun getProcurementInfo(token: String) {
        val finalURL = RetroBase.URL_END_PROCUREMENT + preferenceManager.readString(SELECTED_DEALER_CODE, "")
        RetroBase.retroInterface.getFromWeb(finalURL, "Bearer $token").enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                try {
                    SpinnerManager.hideSpinner(requireActivity())
                    val strRes = Gson().toJson(response.body())
                    procurementRDMFormResponse = Gson().fromJson(strRes, ProcurementRDMFormInfoModel::class.java)
                    Log.d(TAG, "onResponse: $procurementRDMFormResponse")
                    updateUI()
                } catch (e: Exception) {
                    SpinnerManager.hideSpinner(requireActivity())
                    Log.e(TAG, e.message.toString())
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(requireActivity())
            }
        })
    }

    private fun callGetToken() {
        SpinnerManager.showSpinner(requireActivity())
        RetroBase.retroInterface.getFromWeb(RDRTokenRequest(), RetroBase.URL_END_GET_TOKEN).enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                try {
                    val strRes = Gson().toJson(response.body())
                    val resModel = Gson().fromJson(strRes, RDRTokenResponse::class.java)
                    Log.i(TAG, "onResponse: " + resModel.token)
                    getProcurementInfo(resModel.token)
                } catch (e: Exception) {
                    SpinnerManager.hideSpinner(requireActivity())
                    Log.e(TAG, e.message.toString())
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(requireActivity())
            }
        })
    }

    private fun setValuesToCalculationVariables() {
        procurementIep = if (layoutProcurement.editTextFirstField.text.toString().trim().isNotEmpty())
            layoutProcurement.editTextFirstField.text.toString().trim().toInt()
        else
            0

        procurementCPT = if (layoutProcurement.editTextSecondField.text.toString().trim().isNotEmpty())
            layoutProcurement.editTextSecondField.text.toString().trim().toInt()
        else
            0

        procurementThruAmSupport = if (layoutProcurementThruAMSupport.editTextFullLayout.text.toString().trim().isNotEmpty())
            layoutProcurementThruAMSupport.editTextFullLayout.text.toString().trim().toInt()
        else
            0

        selfProcurement = if (layoutSelfProcurement.editTextFullLayout.text.toString().trim().isNotEmpty())
            layoutSelfProcurement.editTextFullLayout.text.toString().trim().toInt()
        else
            0

        if (!isViewDetails) {
            procurementRDMFormResponse.totalProcurementActual = (calculateTotalProcurement().toInt() + oldTotalProcurementActual).toString()
            procurementRDMFormResponse.iepAndCptProcuredActual = (procurementCPT + procurementIep + oldIepCptCountActual).toString()
        } else {
            procurementRDMFormResponse.totalProcurementActual = oldTotalProcurementActual.toString()
            procurementRDMFormResponse.iepAndCptProcuredActual = oldIepCptCountActual.toString()
        }
    }

    private fun calculateTotalProcurement(): String {

        return (procurementCPT + procurementIep + procurementThruAmSupport + selfProcurement).toString()
    }

    companion object {
        val TAG: String = ProcurementRDMFormFragment.javaClass.simpleName

        @JvmStatic
        fun newInstance(param1: Boolean, param2: String?, listener: RDMFormClicksListener) =
                ProcurementRDMFormFragment().apply {
                    arguments = Bundle().apply {
                        putBoolean(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                    rdmFormClicksListener = listener
                }
    }

    override fun saveData() {
        Log.e(TAG, "saveData() called")
        rdmFormClicksListener.onNextButtonClick(getFilledData())
    }
}