package remotedealer.rdrform.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.mfcwl.mfc_dealer.R
import com.mfcwl.mfc_dealer.Utility.AppConstants
import com.mfcwl.mfc_dealer.Utility.CommonMethods
import mteams.MTeamsLogin
import rdm.ScheduleEventAdapter
import rdm.model.EventData
import rdm.model.EventListRes
import rdm.model.EventWhere
import rdm.model.SearchEventReq
import rdm.retrofit_services.RetroBaseService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import sidekicklpr.PreferenceManager
import java.text.SimpleDateFormat
import java.util.*

public class EventListFragment() : Fragment(), Callback<Any> {
    var TAG = javaClass.simpleName

    var startDate = ""
    var endDate: String? = ""
    private val format = "yyyy-MM-dd hh:mm:ss"
    private val formatExpected = "dd MMM"
    val cal = Calendar.getInstance()
    var todayDate = cal.time

    private val sdf = SimpleDateFormat(format)
    lateinit var ll_no_events: LinearLayout
    lateinit var rv_schedule_events_list: RecyclerView
    lateinit var scheduleEventAdapter: ScheduleEventAdapter
    lateinit var fab_add_event: Button
    var code = ""
    var meetingType = ""
    var name = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_r_d_m_schedule, container, false)
        setHasOptionsMenu(true)
        Log.i(TAG, "onCreateView: ")

        startDate = CommonMethods.getTodayDate()+" 00:00:00"
        Log.i(TAG, "onCreateView: " + startDate)
        preferenceEManager = PreferenceManager(context)

        endDate = CommonMethods.getTomorrowDateAndTime(activity)
        Log.i(TAG, "onCreateView: " + startDate + " " + endDate);
        arguments?.let {
            Log.i(TAG, "onCreateView: " + it.getString("DCODE").toString())
            code = it.getString("DCODE").toString()
            name = it.getString("DNAME").toString()
            meetingType = it.getString("MEETING_TYPE").toString()
        }

       // SpinnerManager.showSpinner(getActivity())
        return view
    }

    private fun getSearchEventReq(): SearchEventReq? {

        var searchEvent = SearchEventReq()
        searchEvent.page = "1"
        searchEvent.pageItems = "2000"
        searchEvent.orderBy = ""
        searchEvent.orderByReverse = false.toString()
        searchEvent.filterByFields = ""
        searchEvent.where = getEventWhere()
        return searchEvent
    }

    private fun getEventWhere(): MutableList<EventWhere>? {
        var eventWhere = mutableListOf<EventWhere>()

        val dealerCode = EventWhere()
        dealerCode.column = "dealer_code"
        dealerCode.operator = "="
        dealerCode.value = code

        val startEventDate = EventWhere()
        startEventDate.column = "event_start_date_time"
        startEventDate.operator = ">="
        startEventDate.value = startDate

        val eventEndDate = EventWhere()
        eventEndDate.column = "event_start_date_time"
        eventEndDate.operator = "<="
        eventEndDate.value = endDate

        eventWhere.add(dealerCode)
        eventWhere.add(startEventDate)
        eventWhere.add(eventEndDate)

        return eventWhere
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
    }

    private fun initView(view: View) {
        ll_no_events = view.findViewById(R.id.ll_no_events)
        rv_schedule_events_list = view.findViewById(R.id.rv_schedule_events_list)
        fab_add_event = view.findViewById(R.id.fab_add_event)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this.getActivity())
        rv_schedule_events_list.setLayoutManager(layoutManager)
        fab_add_event.setOnClickListener { view ->

            val mTeamsLogin = Intent(context, MTeamsLogin::class.java)
            mTeamsLogin.putExtra(AppConstants.DEALER_NAME, name)
            mTeamsLogin.putExtra(AppConstants.DEALER_CODE, code)
            Log.i(TAG, "initView: " + code)
            mTeamsLogin.putExtra(AppConstants.MEETING_TYPE, meetingType)
            startActivity(mTeamsLogin)

        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            arguments?.let {
                Log.i(TAG, "onCreateView: " + it.getString("DCODE").toString())
                code = it.getString("DCODE").toString()
                name = it.getString("DNAME").toString()
                meetingType = it.getString("MEETING_TYPE").toString()
            }

        }
    }

    fun getLayoutId(): Int {
        return 0
    }


    override fun onResponse(call: Call<Any?>?, response: Response<Any?>) {
     //  SpinnerManager.hideSpinner(getActivity())
        try {

            val strRes = Gson().toJson(response.body())
            Log.i("RDMScheduleFragment", "onResponse: $strRes")
            val eventResponse = Gson().fromJson(strRes, EventListRes::class.java)
            if (eventResponse.total>=1) {
                Log.i(TAG, "onResponse: " + eventResponse.total)
                if (!eventResponse.data.isEmpty()) {
                    ll_no_events.visibility = View.GONE
                    setToScheduleAdapter(eventResponse.data)
                } else
                    ll_no_events.visibility = View.VISIBLE
            }
            else
            {
                Toast.makeText(activity,"No Event for Today and Tomorrow",Toast.LENGTH_LONG).show()
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun onFailure(call: Call<Any?>?, t: Throwable?) {
        t!!.printStackTrace()
        //SpinnerManager.hideSpinner(getActivity())
    }


    fun setToScheduleAdapter(eventDataList: List<EventData?>?) {
        try {
            if (eventDataList != null) {
                scheduleEventAdapter = ScheduleEventAdapter(eventDataList.reversed(), getActivity())
            }
            rv_schedule_events_list!!.adapter = scheduleEventAdapter
        } catch (nullPointerExc: NullPointerException) {
            CommonMethods.alertMessage(activity, resources.getString(R.string.no_data_found))
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }

    companion object {
        lateinit var preferenceEManager: PreferenceManager

        @JvmField
        val TAG: String = EventListFragment.javaClass.simpleName
        fun newInstance(dealerCode: String, name: String, meetingType: String) =
                EventListFragment().apply {
                    arguments = Bundle().apply {
                        putString("DCODE", dealerCode)
                        Log.i(TAG, "newInstance: " + dealerCode)
                        putString("DNAME", name)
                        putString("MEETING_TYPE", meetingType)
                    }
                }
    }

    override fun onResume() {
        super.onResume()
        RetroBaseService.retrofitInterface.getFromWeb(getSearchEventReq(), RetroBaseService.URL_END_SEARCH_EVENT).enqueue(this)


    }


}

