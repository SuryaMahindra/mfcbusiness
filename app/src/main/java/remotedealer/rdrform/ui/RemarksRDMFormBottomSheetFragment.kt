package remotedealer.rdrform.ui

import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.Gson
import com.mfcwl.mfc_dealer.R
import com.mfcwl.mfc_dealer.Utility.SpinnerManager
import kotlinx.android.synthetic.main.fragment_bottom_sheet_remarks.*
import remotedealer.model.token.RDRTokenRequest
import remotedealer.model.token.RDRTokenResponse
import remotedealer.rdrform.model.*
import remotedealer.rdrform.ui.BaseRDMFormFragment.Companion.ACTION_ITEMS_INFO
import remotedealer.rdrform.ui.BaseRDMFormFragment.Companion.ACTION_ITEMS_UPDATED_DATA
import remotedealer.retrofit.RetroBase
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import sidekicklpr.PreferenceManager


/**
 * Created by Shitalkumar on 7/12/20
 */
class RemarksRDMFormBottomSheetFragment : BottomSheetDialogFragment(), AddRemarksClickListener {

    private var dealerCode: String? = null
    private var isViewDetails: Boolean = false
    private var fragmentView: View? = null
    private lateinit var preferenceManager: PreferenceManager
    private lateinit var remarksInfoModel: RemarksInfoModel
    private var previousRemarksListViewAdapter = PreviousRemarksListViewAdapter(listOf())
    private lateinit var addRemarksListViewAdapterProcurement: AddRemarksListViewAdapter
    private lateinit var addRemarksListViewAdapterSales: AddRemarksListViewAdapter
    private lateinit var addRemarksListViewAdapterStack: AddRemarksListViewAdapter
    private var storedInitiativesTemp: ArrayList<StoredInitiative> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            dealerCode = it.getString(ARG_PARAM1)
            isViewDetails = it.getBoolean(ARG_PARAM2)
        }
        preferenceManager = PreferenceManager(requireActivity())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.fragment_bottom_sheet_remarks, container, false)
        return fragmentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val rootLayout = fragmentView?.findViewById<LinearLayout>(R.id.constraintLayoutBottomSheet)
        val params = rootLayout?.layoutParams
        params?.height = getScreenHeight()
        rootLayout?.layoutParams = params

        val dialog = dialog as BottomSheetDialog
        val bottomSheet = dialog.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
        val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        bottomSheetBehavior.addBottomSheetCallback(object : BottomSheetCallback() {
            override fun onStateChanged(@NonNull bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                }
            }

            override fun onSlide(@NonNull bottomSheet: View, slideOffset: Float) {}
        })

        initView()
        callGetToken()
    }

    private fun initView() {
        recyclerViewPreviousRemarks.layoutManager = LinearLayoutManager(context)
        recyclerViewPreviousRemarks.addItemDecoration(
                DividerItemDecoration(
                        activity,
                        LinearLayoutManager.VERTICAL
                )
        )

        recyclerViewPreviousRemarks.itemAnimator = DefaultItemAnimator()
        recyclerViewPreviousRemarks.adapter = previousRemarksListViewAdapter
        recyclerViewPreviousRemarks.isNestedScrollingEnabled = false

        recyclerViewProcurement.layoutManager = LinearLayoutManager(context)
        recyclerViewProcurement.itemAnimator = DefaultItemAnimator()
        recyclerViewProcurement.isNestedScrollingEnabled = false

        recyclerViewSales.layoutManager = LinearLayoutManager(context)
        recyclerViewSales.itemAnimator = DefaultItemAnimator()
        recyclerViewSales.isNestedScrollingEnabled = false

        recyclerViewStack.layoutManager = LinearLayoutManager(context)
        recyclerViewStack.itemAnimator = DefaultItemAnimator()
        recyclerViewStack.isNestedScrollingEnabled = false

        imageViewClose.setOnClickListener {
            this.dismiss()
        }

        imageViewPreviousRemarksExpand.setOnClickListener {
            handlePreviousRemarksSection()
        }

        imageViewProcurementExpand.setOnClickListener {
            handleProcurementSection()
        }

        imageViewSalesExpand.setOnClickListener {
            handleSalesSection()
        }

        imageViewStackExpand.setOnClickListener {
            handleStackSection()
        }

        if (isViewDetails) {
            linLayProcurementRemarks.visibility = View.GONE
            linLaySalesRemarks.visibility = View.GONE
            linLayStackRemarks.visibility = View.GONE
        }
    }

    private fun updateUI() {

        var updateddata = preferenceManager.readString(ACTION_ITEMS_UPDATED_DATA, "")
        var updatedList = Gson().fromJson(updateddata, StoredInitiativelist::class.java)
        if (updatedList != null) {
            storedInitiativesTemp.clear()
            for (data in updatedList) {
                var tempdata = StoredInitiative()
                tempdata.remarks = data.remarks
                tempdata.category = data.category
                tempdata.actionitem = data.actionitem
                tempdata.actionId = data.actionId
                storedInitiativesTemp.add(tempdata)
            }
        }
        if (remarksInfoModel.initiative.size > 0) {
            linLayPreviousRemarks.visibility = View.VISIBLE
            previousRemarksListViewAdapter = PreviousRemarksListViewAdapter(remarksInfoModel.initiative)
            recyclerViewPreviousRemarks.adapter = previousRemarksListViewAdapter
        }

        if (remarksInfoModel.procurement.size > 0) {

            if (updatedList != null) {

                addRemarksListViewAdapterProcurement = AddRemarksListViewAdapter(resources.getString(R.string.title_procurement), storedInitiativesTemp, remarksInfoModel.procurement, this)

            } else {
                addRemarksListViewAdapterProcurement = AddRemarksListViewAdapter(resources.getString(R.string.title_procurement), remarksInfoModel.storedInitiatives, remarksInfoModel.procurement, this)

            }
            recyclerViewProcurement.adapter = addRemarksListViewAdapterProcurement
        }

        if (remarksInfoModel.sales.size > 0) {
            if (updatedList != null) {
                addRemarksListViewAdapterSales = AddRemarksListViewAdapter(resources.getString(R.string.title_sales), storedInitiativesTemp, remarksInfoModel.sales, this)

            } else {
                addRemarksListViewAdapterSales = AddRemarksListViewAdapter(resources.getString(R.string.title_sales), remarksInfoModel.storedInitiatives, remarksInfoModel.sales, this)

            }
            recyclerViewSales.adapter = addRemarksListViewAdapterSales
        }

        if (remarksInfoModel.stack.size > 0) {
            if (updatedList != null) {
                addRemarksListViewAdapterStack = AddRemarksListViewAdapter(resources.getString(R.string.title_stack), storedInitiativesTemp, remarksInfoModel.stack, this)

            } else {
                addRemarksListViewAdapterStack = AddRemarksListViewAdapter(resources.getString(R.string.title_stack), remarksInfoModel.storedInitiatives, remarksInfoModel.stack, this)

            }
            recyclerViewStack.adapter = addRemarksListViewAdapterStack
        }
    }

    private fun handlePreviousRemarksSection() {
        if (recyclerViewPreviousRemarks.visibility == View.VISIBLE) {
            recyclerViewPreviousRemarks.visibility = View.GONE
            imageViewPreviousRemarksExpand.setImageDrawable(resources.getDrawable(R.drawable.ic_expand))
        } else {
            recyclerViewPreviousRemarks.visibility = View.VISIBLE
            imageViewPreviousRemarksExpand.setImageDrawable(resources.getDrawable(R.drawable.ic_collapse))
        }
    }

    private fun handleProcurementSection() {
        if (recyclerViewProcurement.visibility == View.VISIBLE) {
            recyclerViewProcurement.visibility = View.GONE
            imageViewProcurementExpand.setImageDrawable(resources.getDrawable(R.drawable.ic_expand))
        } else {
            recyclerViewProcurement.visibility = View.VISIBLE
            imageViewProcurementExpand.setImageDrawable(resources.getDrawable(R.drawable.ic_collapse))
        }
    }

    private fun handleSalesSection() {
        if (recyclerViewSales.visibility == View.VISIBLE) {
            recyclerViewSales.visibility = View.GONE
            imageViewSalesExpand.setImageDrawable(resources.getDrawable(R.drawable.ic_expand))
        } else {
            recyclerViewSales.visibility = View.VISIBLE
            imageViewSalesExpand.setImageDrawable(resources.getDrawable(R.drawable.ic_collapse))
        }
    }

    private fun handleStackSection() {
        if (recyclerViewStack.visibility == View.VISIBLE) {
            recyclerViewStack.visibility = View.GONE
            imageViewStackExpand.setImageDrawable(resources.getDrawable(R.drawable.ic_expand))
        } else {
            recyclerViewStack.visibility = View.VISIBLE
            imageViewStackExpand.setImageDrawable(resources.getDrawable(R.drawable.ic_collapse))
        }
    }

    private fun getRDRActionItems(token: String) {
        val finalURL = RetroBase.URL_END_GET_RDR_ACTON_ITEM + dealerCode
        RetroBase.retroInterface.getFromWeb(finalURL, "Bearer $token").enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                try {
                    val strRes = Gson().toJson(response.body())
                    remarksInfoModel = Gson().fromJson(strRes, RemarksInfoModel::class.java)
                    Log.d(TAG, "URL_END_GET_RDR_ACTON_ITEM - onResponse: $remarksInfoModel")
                    Log.d(TAG, "stored items:" + remarksInfoModel.storedInitiatives.size)
                    storedInitiativesTemp.addAll(remarksInfoModel.storedInitiatives)

                    if (remarksInfoModel.storedInitiatives.size > 0) {
                        var arrayListActionItemInfoModel = ActionItemsList()
                        for (data in storedInitiativesTemp) {
                            val actionItemInfoModel = ActionItemInfoModel()
                            actionItemInfoModel.actionItem = data.actionitem
                            actionItemInfoModel.category = data.category
                            actionItemInfoModel.remarks = data.remarks
                            arrayListActionItemInfoModel.actionItemInfoModels.add(actionItemInfoModel)
                        }
                        preferenceManager.writeString(ACTION_ITEMS_INFO, Gson().toJson(arrayListActionItemInfoModel))
                    }
                    updateUI()
                } catch (e: Exception) {
                    Log.e(TAG, e.message.toString())
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
            }
        })
    }

    private fun callGetToken() {
        SpinnerManager.showSpinner(requireActivity())
        RetroBase.retroInterface.getFromWeb(RDRTokenRequest(), RetroBase.URL_END_GET_TOKEN).enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                try {
                    SpinnerManager.hideSpinner(requireActivity())
                    val strRes = Gson().toJson(response.body())
                    val resModel = Gson().fromJson(strRes, RDRTokenResponse::class.java)
                    Log.i(TAG, "onResponse: " + resModel.token)
                    getRDRActionItems(resModel.token)
                } catch (e: Exception) {
                    Log.e(TAG, e.message.toString())
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(requireActivity())
            }
        })
    }

    private fun getScreenHeight(): Int {
        return Resources.getSystem().displayMetrics.heightPixels
    }

    companion object {
        val TAG: String = RemarksRDMFormBottomSheetFragment.javaClass.simpleName

        private const val ARG_PARAM1 = "param1"
        private const val ARG_PARAM2 = "param2"

        @JvmStatic
        fun newInstance(param1: String, param2: Boolean) =
                RemarksRDMFormBottomSheetFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putBoolean(ARG_PARAM2, param2)
                    }
                }
    }

    override fun onRemarkAdded(remarks: String, actionItem: String, category: String) {
        try {
            Log.d(TAG, "Remark Added :: $remarks for actionItem :: $actionItem  and category :: $category")
            val actionItemInfoModel = ActionItemInfoModel()
            actionItemInfoModel.remarks = remarks
            actionItemInfoModel.actionItem = actionItem
            actionItemInfoModel.category = category

            if (remarks != "") {
             var useradded =    StoredInitiative()
                useradded.actionitem = actionItem
                useradded.category = category
                useradded.remarks = remarks
                useradded.actionId = "0.0"
                storedInitiativesTemp?.removeAll() { it.category.equals(category) && it.actionitem.equals(actionItem) }
                storedInitiativesTemp?.add(useradded)

            } else {
                storedInitiativesTemp?.removeAll() { it.category.equals(category) && it.actionitem.equals(actionItem) }
            }
            //update the list if remarks empty remarks we no need to show them again
            if (storedInitiativesTemp.size > 0) {
                var arrayListActionItemInfoModel = ActionItemsList()
                for (data in storedInitiativesTemp) {
                    val actionItemInfoModel = ActionItemInfoModel()
                    actionItemInfoModel.actionItem = data.actionitem
                    actionItemInfoModel.category = data.category
                    actionItemInfoModel.remarks = data.remarks
                    arrayListActionItemInfoModel.actionItemInfoModels.add(actionItemInfoModel)
                }
                preferenceManager.writeString(ACTION_ITEMS_INFO, Gson().toJson(arrayListActionItemInfoModel))
            }else{
                preferenceManager.writeString(ACTION_ITEMS_INFO, "")
            }
            preferenceManager.writeString(ACTION_ITEMS_UPDATED_DATA, Gson().toJson(storedInitiativesTemp))
            val actionItemsJson = preferenceManager.readString(ACTION_ITEMS_INFO, "")
            var arrayListActionItemInfoModel = Gson().fromJson(actionItemsJson, ActionItemsList::class.java)
            if (!remarks.equals("")) {

                if (arrayListActionItemInfoModel == null)
                    arrayListActionItemInfoModel = ActionItemsList()
                arrayListActionItemInfoModel.actionItemInfoModels.add(actionItemInfoModel)
                preferenceManager.writeString(ACTION_ITEMS_INFO, Gson().toJson(arrayListActionItemInfoModel))
                Toast.makeText(requireContext(), "Remark added", Toast.LENGTH_SHORT).show()
            }
            this.dismiss()
        } catch (e: java.lang.Exception) {
            Log.e(TAG, e.message.toString())
        }
    }
}