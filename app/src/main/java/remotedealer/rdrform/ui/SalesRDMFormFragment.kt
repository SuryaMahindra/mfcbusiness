 package remotedealer.rdrform.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.gson.Gson
import com.mfcwl.mfc_dealer.R
import com.mfcwl.mfc_dealer.Utility.SpinnerManager
import kotlinx.android.synthetic.main.fragment_sales_rdm_form.*
import kotlinx.android.synthetic.main.rdm_form_element_custom_view.view.*
import remotedealer.model.token.RDRTokenRequest
import remotedealer.model.token.RDRTokenResponse
import remotedealer.rdrform.model.SalesRDMFormInfoModel
import remotedealer.retrofit.RetroBase
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * Created by Shitalkumar on 01/12/2020
 */
class SalesRDMFormFragment : BaseRDMFormFragment() {

    private var param1: Boolean? = null
    private var param2: String? = null
    private var salesRDMFormResponse = SalesRDMFormInfoModel()
    private var oldTotalStocksActual = 0
    private var oldTotalSalesActual = 0
    private var oldCertifiedStockActual = 0
    private var oldOmsContributionActual = 0
    private var certifiedStocks = 0.0
    private var conversionThruOMS = 0.0
    private var retailSales = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            isViewDetails = it.getBoolean(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Log.i(TAG, "onCreateView: ")
        return inflater.inflate(R.layout.fragment_sales_rdm_form, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()

        try {
            if (!isViewDetails) {
                callGetToken()
            } else {
                val savedData = preferenceManager.readString(SALES_RDM_INFO, "")
                if (savedData.trim().isNotEmpty()) {
                    salesRDMFormResponse = Gson().fromJson(savedData, SalesRDMFormInfoModel::class.java)
                    updateUI()
                }
            }
        } catch (e: Exception) {
            Log.e(ProcurementRDMFormFragment.TAG, e.message.toString())
        }
    }

    private fun initView() {
        layoutStocksAndCertifiedOMS.linLayHeader.visibility = View.GONE
        layoutStocksAndCertifiedOMS.textViewFirstField.text = "Stocks on OMS"
        layoutStocksAndCertifiedOMS.textViewSecondField.text = "Certified Stocks"
        setEditTextEditable(layoutStocksAndCertifiedOMS.editTextFirstField, false, null)
        setEditTextEditable(layoutStocksAndCertifiedOMS.editTextSecondField, false, null)

        layoutWalkInAndSales.linLayHeader.visibility = View.GONE
        layoutWalkInAndSales.textViewFirstField.text = "Walk-ins"
        layoutWalkInAndSales.textViewSecondField.text = "Sales"
        setEditTextEditable(layoutWalkInAndSales.editTextFirstField, true, layoutWalkInAndSales.textViewFirstField)
        setEditTextEditable(layoutWalkInAndSales.editTextSecondField, true, layoutWalkInAndSales.textViewSecondField)

        layoutRetailAndBooking.linLayHeader.visibility = View.GONE
        layoutRetailAndBooking.textViewFirstField.text = "Retail"
        layoutRetailAndBooking.textViewSecondField.text = "Bookings"
        setEditTextEditable(layoutRetailAndBooking.editTextFirstField, true, layoutRetailAndBooking.textViewFirstField)
        setEditTextEditable(layoutRetailAndBooking.editTextSecondField, true, layoutRetailAndBooking.textViewSecondField)

        layoutOMSLeadAndRepeatLeads.linLayHeader.visibility = View.GONE
        layoutOMSLeadAndRepeatLeads.textViewFirstField.text = "OMS Leads"
        layoutOMSLeadAndRepeatLeads.textViewSecondField.text = "Repeat Leads"
        setEditTextEditable(layoutOMSLeadAndRepeatLeads.editTextFirstField, false, null)
        setEditTextEditable(layoutOMSLeadAndRepeatLeads.editTextSecondField, true, layoutOMSLeadAndRepeatLeads.textViewSecondField)

        layoutConversionAndBookingThroughOMS.linLayHeader.visibility = View.GONE
        layoutConversionAndBookingThroughOMS.textViewFirstField.text = "Conversion through \nOMS"
        layoutConversionAndBookingThroughOMS.textViewSecondField.text = "Booking through \nOMS"
        setEditTextEditable(layoutConversionAndBookingThroughOMS.editTextFirstField, true, layoutConversionAndBookingThroughOMS.textViewFirstField)
        setEditTextEditable(layoutConversionAndBookingThroughOMS.editTextSecondField, true, layoutConversionAndBookingThroughOMS.textViewSecondField)

        layoutQualifiedAndConversionThroughCRE.linLayHeader.visibility = View.GONE
        layoutQualifiedAndConversionThroughCRE.textViewFirstField.text = "Qualified Leads \nfrom CRE"
        layoutQualifiedAndConversionThroughCRE.textViewSecondField.text = "Conversion out of \nCRE leads"
        setEditTextEditable(layoutQualifiedAndConversionThroughCRE.editTextFirstField, true, layoutQualifiedAndConversionThroughCRE.textViewFirstField)
        setEditTextEditable(layoutQualifiedAndConversionThroughCRE.editTextSecondField, true, layoutQualifiedAndConversionThroughCRE.textViewSecondField)

        layoutActualAndParkAndSellStock.linLayHeader.visibility = View.GONE
        layoutActualAndParkAndSellStock.textViewFirstField.text = "Actual Stock"
        layoutActualAndParkAndSellStock.textViewSecondField.text = "Park & Sell Stock"
        setEditTextEditable(layoutActualAndParkAndSellStock.editTextFirstField, true, layoutActualAndParkAndSellStock.textViewFirstField)
        setEditTextEditable(layoutActualAndParkAndSellStock.editTextSecondField, true, layoutActualAndParkAndSellStock.textViewSecondField)

        layoutTotalStock.textViewTitle.text = "Stocks"
        layoutTotalStock.textViewFirstField.text = getString(R.string.title_number)
        layoutTotalStock.textViewSecondField.text = getString(R.string.title_mtd)
        layoutTotalStock.textViewAchieved.visibility = View.VISIBLE
        layoutTotalStock.linLayTargetNo.visibility = View.VISIBLE
        setEditTextEditable(layoutTotalStock.editTextFirstField, false, null)
        setEditTextEditable(layoutTotalStock.editTextSecondField, false, null)

        layoutTotalSale.textViewTitle.text = "Sales"
        layoutTotalSale.textViewFirstField.text = getString(R.string.title_number)
        layoutTotalSale.textViewSecondField.text = getString(R.string.title_mtd)
        layoutTotalSale.textViewAchieved.visibility = View.VISIBLE
        layoutTotalSale.linLayTargetNo.visibility = View.VISIBLE
        setEditTextEditable(layoutTotalSale.editTextFirstField, false, null)
        setEditTextEditable(layoutTotalSale.editTextSecondField, false, null)

        layoutCertifiedStockPercentage.textViewTitle.text = "Certified Stocks % total Stock"
        layoutCertifiedStockPercentage.textViewFirstField.text = getString(R.string.title_number)
        layoutCertifiedStockPercentage.textViewSecondField.text = getString(R.string.title_mtd)
        layoutCertifiedStockPercentage.textViewAchieved.visibility = View.VISIBLE
        layoutCertifiedStockPercentage.linLayTargetNo.visibility = View.VISIBLE
        setEditTextEditable(layoutCertifiedStockPercentage.editTextFirstField, false, null)
        setEditTextEditable(layoutCertifiedStockPercentage.editTextSecondField, false, null)

        layoutOMSContribution.textViewTitle.text = "OMS Contribution"
        layoutOMSContribution.textViewFirstField.text = getString(R.string.title_number)
        layoutOMSContribution.textViewSecondField.text = getString(R.string.title_mtd)
        layoutOMSContribution.textViewAchieved.visibility = View.VISIBLE
        layoutOMSContribution.linLayTargetNo.visibility = View.VISIBLE
        setEditTextEditable(layoutOMSContribution.editTextFirstField, false, null)
        setEditTextEditable(layoutOMSContribution.editTextSecondField, false, null)

        if (isViewDetails)
            buttonSalesNext.visibility = View.GONE

        buttonSalesNext.setOnClickListener {
            rdmFormClicksListener.onNextButtonClick(getFilledData())
        }

        layoutActualAndParkAndSellStock.editTextFirstField.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                //if (s.toString().isNotEmpty()) {
                setValuesToCalculationVariables()
                updateFieldsOnDataChange()
                //}
            }
        })

        layoutWalkInAndSales.editTextSecondField.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                //if (s.toString().isNotEmpty()) {
                setValuesToCalculationVariables()
                updateFieldsOnDataChange()
                //}
            }
        })

        layoutConversionAndBookingThroughOMS.editTextFirstField.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                //if (s.toString().isNotEmpty()) {
                setValuesToCalculationVariables()
                updateFieldsOnDataChange()
                //}
            }
        })

        layoutStocksAndCertifiedOMS.editTextSecondField.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                //if (s.toString().isNotEmpty()) {
                setValuesToCalculationVariables()
                updateFieldsOnDataChange()
                //}
            }
        })

        layoutRetailAndBooking.editTextFirstField.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if (s.toString().isNotEmpty()) {
                    setValuesToCalculationVariables()
                    updateFieldsOnDataChange()
                    salesRDMFormResponse.retailSales = s.toString()
                    if (salesRDMFormResponse.retailSales != "" && salesRDMFormResponse.retailSales != null) {
                        val intent = Intent(INTENT_FILTER_DATACHANGED)
                        context?.let { LocalBroadcastManager.getInstance(it).sendBroadcast(intent) }
                    }

                }
            }
        })

        layoutWalkInAndSales.editTextFirstField.setOnKeyListener(onKeyListener)
        layoutWalkInAndSales.editTextSecondField.setOnKeyListener(onKeyListener)
        layoutRetailAndBooking.editTextFirstField.setOnKeyListener(onKeyListener)
        layoutRetailAndBooking.editTextSecondField.setOnKeyListener(onKeyListener)
        layoutOMSLeadAndRepeatLeads.editTextSecondField.setOnKeyListener(onKeyListener)
        layoutConversionAndBookingThroughOMS.editTextFirstField.setOnKeyListener(onKeyListener)
        layoutConversionAndBookingThroughOMS.editTextSecondField.setOnKeyListener(onKeyListener)
        layoutQualifiedAndConversionThroughCRE.editTextFirstField.setOnKeyListener(onKeyListener)
        layoutQualifiedAndConversionThroughCRE.editTextSecondField.setOnKeyListener(onKeyListener)
        layoutActualAndParkAndSellStock.editTextFirstField.setOnKeyListener(onKeyListener)
        layoutActualAndParkAndSellStock.editTextSecondField.setOnKeyListener(onKeyListener)
    }

    private val onKeyListener = View.OnKeyListener { _, _, event ->
        if (event.action == KeyEvent.ACTION_UP) {
            processButtonByTextLength()
        }

        false
    }

    private fun processButtonByTextLength() {
        val walkIns = layoutWalkInAndSales.editTextFirstField.text.toString().trim().isEmpty()
        val sales = layoutWalkInAndSales.editTextSecondField.text.toString().trim().isEmpty()
        val retails = layoutRetailAndBooking.editTextFirstField.text.toString().trim().isEmpty()
        val bookings = layoutRetailAndBooking.editTextSecondField.text.toString().trim().isEmpty()
        val repeatLeads = layoutOMSLeadAndRepeatLeads.editTextSecondField.text.toString().trim().isEmpty()
        val convThruOMS = layoutConversionAndBookingThroughOMS.editTextFirstField.text.toString().trim().isEmpty()
        val bookingsThruOms = layoutConversionAndBookingThroughOMS.editTextSecondField.text.toString().trim().isEmpty()
        val qualifiedLeadsCRE = layoutQualifiedAndConversionThroughCRE.editTextFirstField.text.toString().trim().isEmpty()
        val conversionOutCRE = layoutQualifiedAndConversionThroughCRE.editTextSecondField.text.toString().trim().isEmpty()
        val actualStock = layoutActualAndParkAndSellStock.editTextFirstField.text.toString().trim().isEmpty()
        val parkAndSellStock = layoutActualAndParkAndSellStock.editTextSecondField.text.toString().trim().isEmpty()

        if (walkIns || sales || retails || bookings || repeatLeads || bookingsThruOms || convThruOMS || qualifiedLeadsCRE
                || conversionOutCRE || actualStock || parkAndSellStock) {
            buttonSalesNext.isEnabled = false
            buttonSalesNext.setBackgroundDrawable(resources.getDrawable(R.drawable.rounded_button_background_dark_grey_disabled))
        } else {
            buttonSalesNext.isEnabled = true
            buttonSalesNext.setBackgroundDrawable(resources.getDrawable(R.drawable.rounded_button_background_blue_enabled))
        }

        var count = 0;
        if (!walkIns)
            count++
        if (!sales)
            count++
        if (!repeatLeads)
            count++
        if (!retails)
            count++
        if (!bookings)
            count++
        if (!bookingsThruOms)
            count++
        if (!convThruOMS)
            count++
        if (!qualifiedLeadsCRE)
            count++
        if (!conversionOutCRE)
            count++
        if (!actualStock)
            count++
        if (!parkAndSellStock)
            count++

        val formCompletionPercentage = (100 * count) / 11
        rdmFormClicksListener.onFormFillListener("Sales", formCompletionPercentage.toString())
        preferenceManager.writeString(SALES_RDM_INFO, getFilledData())

    }

    private fun updateUI() {
        try {
            oldTotalStocksActual = salesRDMFormResponse.actualStock.toInt()
            oldTotalSalesActual = salesRDMFormResponse.totalSalesActual.toInt()
            oldCertifiedStockActual = salesRDMFormResponse.certifiedStockActual.toInt()
            oldOmsContributionActual = salesRDMFormResponse.omsContributionActual.toInt()

            layoutStocksAndCertifiedOMS.editTextFirstField.setText(salesRDMFormResponse.omsStocks)
            layoutStocksAndCertifiedOMS.editTextSecondField.setText(salesRDMFormResponse.certifiedStocks)

            layoutWalkInAndSales.editTextFirstField.setText(salesRDMFormResponse.walkins)
            layoutWalkInAndSales.editTextSecondField.setText(salesRDMFormResponse.totalSales)

            layoutRetailAndBooking.editTextFirstField.setText(salesRDMFormResponse.retailSales)
            layoutRetailAndBooking.editTextSecondField.setText(salesRDMFormResponse.bookingsInHand)

            layoutOMSLeadAndRepeatLeads.editTextFirstField.setText(salesRDMFormResponse.omsLeads)
            layoutOMSLeadAndRepeatLeads.editTextSecondField.setText(salesRDMFormResponse.repeatLeads)

            layoutConversionAndBookingThroughOMS.editTextFirstField.setText(salesRDMFormResponse.convOms)
            layoutConversionAndBookingThroughOMS.editTextSecondField.setText(salesRDMFormResponse.bookingThruOms)

            layoutActualAndParkAndSellStock.editTextFirstField.setText(salesRDMFormResponse.actualStock)
            layoutActualAndParkAndSellStock.editTextSecondField.setText(salesRDMFormResponse.parkSellStock)

            layoutQualifiedAndConversionThroughCRE.editTextFirstField.setText(salesRDMFormResponse.qualLeadsFromCre)
            layoutQualifiedAndConversionThroughCRE.editTextSecondField.setText(salesRDMFormResponse.convLeadsFromCre)

            if(isViewDetails){
                layoutOMSContribution.editTextFirstField.setText(salesRDMFormResponse.omsContributionActual)
                layoutOMSContribution.editTextSecondField.setText(salesRDMFormResponse.omsContributionToday)
            }

            if(!isViewDetails){
                setValuesToCalculationVariables()
                updateFieldsOnDataChange()
            }
            processButtonByTextLength()

        } catch (e: Exception) {
            Log.e(TAG, e.message.toString())
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updateFieldsOnDataChange() {
        // Total Procurement MTD
        if (salesRDMFormResponse.totalStockTarget.isNotEmpty() && salesRDMFormResponse.totalStockTarget != "0") {
            val totalStocks = (100 * salesRDMFormResponse.actualStock.toDouble()) / salesRDMFormResponse.totalStockTarget.toDouble()
            setAchievedPercentageColor(layoutTotalStock.textViewAchieved, totalStocks)
        }
        if (layoutActualAndParkAndSellStock.editTextFirstField.text.toString().trim().isNotEmpty())
            layoutTotalStock.editTextFirstField.setText("${layoutActualAndParkAndSellStock.editTextFirstField.text.toString().trim().toInt()}")
        else
            layoutTotalStock.editTextFirstField.setText("0")

        layoutTotalStock.editTextSecondField.setText("${salesRDMFormResponse.actualStock.toInt()}")
        if(salesRDMFormResponse.totalStockTarget != ""){
            layoutTotalStock.textViewTargetNo.text = "Target : ${salesRDMFormResponse.totalStockTarget.toInt()}"

        }

        if (salesRDMFormResponse.totalSalesTarget.isNotEmpty() && salesRDMFormResponse.totalSalesTarget != "0") {
            val totalSales = (100 * salesRDMFormResponse.totalSalesActual.toDouble()) / salesRDMFormResponse.totalSalesTarget.toDouble()
            setAchievedPercentageColor(layoutTotalSale.textViewAchieved, totalSales)
        }

        if (layoutWalkInAndSales.editTextSecondField.text.toString().trim().isNotEmpty())
            layoutTotalSale.editTextFirstField.setText("${layoutWalkInAndSales.editTextSecondField.text.toString().trim().toInt()}")
        else
            layoutTotalSale.editTextFirstField.setText("0")

        layoutTotalSale.editTextSecondField.setText("${salesRDMFormResponse.totalSalesActual.toInt()}")
        layoutTotalSale.textViewTargetNo.text = "Target : ${salesRDMFormResponse.totalSalesTarget.toInt()}"

        if (salesRDMFormResponse.certifiedStockTarget.isNotEmpty() && salesRDMFormResponse.certifiedStockTarget != "0") {
            val certifiedStocks = (100 * salesRDMFormResponse.certifiedStockActual.toDouble()) / salesRDMFormResponse.certifiedStockTarget.toInt()
            setAchievedPercentageColor(layoutCertifiedStockPercentage.textViewAchieved, certifiedStocks)
        }
        layoutCertifiedStockPercentage.editTextFirstField.setText("${certifiedStocks.toInt()}")
        layoutCertifiedStockPercentage.editTextSecondField.setText("${salesRDMFormResponse.certifiedStockActual.toInt()}")
        layoutCertifiedStockPercentage.textViewTargetNo.text = "Target : ${salesRDMFormResponse.omsStocks.toInt()}"

        if (salesRDMFormResponse.omsContributionTarget.isNotEmpty() && salesRDMFormResponse.omsContributionTarget != "0") {
            val omsContribution = (100 * salesRDMFormResponse.omsContributionActual.toDouble()) / salesRDMFormResponse.omsContributionTarget.toDouble()
            setAchievedPercentageColor(layoutOMSContribution.textViewAchieved, omsContribution)
        }
        var calculation = 0;
        if (retailSales != 0.0) {
            calculation = (conversionThruOMS / retailSales).toInt()
            layoutOMSContribution.editTextFirstField.setText("" + calculation)

        } else {
                layoutOMSContribution.editTextFirstField.setText("0")
        }
        layoutOMSContribution.editTextSecondField.setText(""+salesRDMFormResponse.omsContributionActual.toInt())
        layoutOMSContribution.textViewTargetNo.text = "Target : ${salesRDMFormResponse.omsContributionTarget.toInt()}"

    }


    private fun getFilledData(): String {
        salesRDMFormResponse.walkins = layoutWalkInAndSales.editTextFirstField.text.toString()
        salesRDMFormResponse.totalSales = layoutWalkInAndSales.editTextSecondField.text.toString()

        salesRDMFormResponse.retailSales = layoutRetailAndBooking.editTextFirstField.text.toString()
        salesRDMFormResponse.bookingsInHand = layoutRetailAndBooking.editTextSecondField.text.toString()

        salesRDMFormResponse.retailSales = layoutRetailAndBooking.editTextFirstField.text.toString()
        salesRDMFormResponse.bookingsInHand = layoutRetailAndBooking.editTextSecondField.text.toString()

        salesRDMFormResponse.repeatLeads = layoutRetailAndBooking.editTextSecondField.text.toString()

        salesRDMFormResponse.convOms = layoutConversionAndBookingThroughOMS.editTextFirstField.text.toString()
        salesRDMFormResponse.bookingThruOms = layoutConversionAndBookingThroughOMS.editTextSecondField.text.toString()

        salesRDMFormResponse.qualLeadsFromCre = layoutQualifiedAndConversionThroughCRE.editTextFirstField.text.toString()
        salesRDMFormResponse.convLeadsFromCre = layoutQualifiedAndConversionThroughCRE.editTextSecondField.text.toString()

        salesRDMFormResponse.actualStock = layoutActualAndParkAndSellStock.editTextFirstField.text.toString()
        salesRDMFormResponse.parkSellStock = layoutActualAndParkAndSellStock.editTextSecondField.text.toString()
        salesRDMFormResponse.stockNumber = salesRDMFormResponse.actualStock

        salesRDMFormResponse.omsContributionActual = layoutOMSContribution.editTextSecondField.text.toString()
        salesRDMFormResponse.omsContributionToday = layoutOMSContribution.editTextFirstField.text.toString()
        return Gson().toJson(salesRDMFormResponse)
    }

    private fun setValuesToCalculationVariables() {
        if (layoutActualAndParkAndSellStock.editTextFirstField.text.toString().trim().isNotEmpty() && !isViewDetails)
            salesRDMFormResponse.actualStock = (layoutActualAndParkAndSellStock.editTextFirstField.text.toString().trim().toInt()).toString()
        else
            salesRDMFormResponse.actualStock = oldTotalStocksActual.toString()

        if (layoutWalkInAndSales.editTextSecondField.text.toString().trim().isNotEmpty() && !isViewDetails)
            salesRDMFormResponse.totalSalesActual = (layoutWalkInAndSales.editTextSecondField.text.toString().trim().toInt() + oldTotalSalesActual).toString()
        else
            salesRDMFormResponse.totalSalesActual = oldTotalSalesActual.toString()

        if (layoutStocksAndCertifiedOMS.editTextSecondField.text.toString().trim().isNotEmpty() && !isViewDetails) {
            certifiedStocks = layoutStocksAndCertifiedOMS.editTextSecondField.text.toString().toDouble()
            salesRDMFormResponse.certifiedStockActual = (certifiedStocks.toInt() + oldCertifiedStockActual).toString()
        } else {
            certifiedStocks = 0.0
            salesRDMFormResponse.certifiedStockActual = oldCertifiedStockActual.toString()
        }

        if (layoutConversionAndBookingThroughOMS.editTextFirstField.text.toString().trim().isNotEmpty()
                && layoutRetailAndBooking.editTextFirstField.text.toString().trim().isNotEmpty() && !isViewDetails) {
            conversionThruOMS = layoutConversionAndBookingThroughOMS.editTextFirstField.text.toString().toDouble()
            retailSales = layoutRetailAndBooking.editTextFirstField.text.toString().toDouble()
            if (retailSales != 0.0){
                salesRDMFormResponse.omsContributionActual = ((conversionThruOMS / retailSales).toInt() + oldOmsContributionActual).toString()
            }else{
                salesRDMFormResponse.omsContributionActual = "0"
            }

        } else {
            conversionThruOMS = 0.0
            //salesRDMFormResponse.omsContributionActual = "0"
        }
    }

    private fun getSalesInfo(token: String) {
        val finalURL = RetroBase.URL_END_SALES + preferenceManager.readString(SELECTED_DEALER_CODE, "")
        RetroBase.retroInterface.getFromWeb(finalURL, "Bearer $token").enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                try {
                    SpinnerManager.hideSpinner(requireActivity())
                    val strRes = Gson().toJson(response.body())
                    salesRDMFormResponse = Gson().fromJson(strRes, SalesRDMFormInfoModel::class.java)
                    Log.d(TAG, "onResponse: $salesRDMFormResponse")
                    updateUI()
                } catch (e: Exception) {
                    SpinnerManager.hideSpinner(requireActivity())
                    Log.e(TAG, e.message.toString())
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(requireActivity())
            }
        })
    }

    private fun callGetToken() {
        SpinnerManager.showSpinner(requireActivity())
        RetroBase.retroInterface.getFromWeb(RDRTokenRequest(), RetroBase.URL_END_GET_TOKEN).enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                try {
                    val strRes = Gson().toJson(response.body())
                    val resModel = Gson().fromJson(strRes, RDRTokenResponse::class.java)
                    Log.i(TAG, "onResponse: " + resModel.token)
                    getSalesInfo(resModel.token)
                } catch (e: Exception) {
                    SpinnerManager.hideSpinner(requireActivity())
                    Log.e(TAG, e.message.toString())
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(requireActivity())
            }
        })
    }

    companion object {
        val TAG: String = SalesRDMFormFragment.javaClass.simpleName

        @JvmStatic
        fun newInstance(param1: Boolean, param2: String?, listener: RDMFormClicksListener) =
                SalesRDMFormFragment().apply {
                    arguments = Bundle().apply {
                        putBoolean(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                    rdmFormClicksListener = listener
                }

    }
}