package remotedealer.rdrform.ui

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.text.*
import android.text.method.DigitsKeyListener
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.gson.Gson
import com.mfcwl.mfc_dealer.R
import com.mfcwl.mfc_dealer.Utility.SpinnerManager
import kotlinx.android.synthetic.main.fragment_stack_rdm_form.*
import kotlinx.android.synthetic.main.rayoalty_collection.view.*
import kotlinx.android.synthetic.main.rdm_form_element_custom_view.view.*
import kotlinx.android.synthetic.main.rdm_form_element_custom_view.view.editTextFirstField
import kotlinx.android.synthetic.main.rdm_form_element_custom_view.view.editTextFullLayout
import kotlinx.android.synthetic.main.rdm_form_element_custom_view.view.linLayDouble
import kotlinx.android.synthetic.main.rdm_form_element_custom_view.view.linLayFull
import kotlinx.android.synthetic.main.rdm_form_element_custom_view.view.linLayHeader
import kotlinx.android.synthetic.main.rdm_form_element_custom_view.view.linLayTargetNo
import kotlinx.android.synthetic.main.rdm_form_element_custom_view.view.textViewAchieved
import kotlinx.android.synthetic.main.rdm_form_element_custom_view.view.textViewFirstField
import kotlinx.android.synthetic.main.rdm_form_element_custom_view.view.textViewFullLayout
import kotlinx.android.synthetic.main.rdm_form_element_custom_view.view.textViewSecondField
import kotlinx.android.synthetic.main.rdm_form_element_custom_view.view.textViewTargetNo
import kotlinx.android.synthetic.main.rdm_form_element_custom_view.view.textViewTitle
import remotedealer.model.token.RDRTokenRequest
import remotedealer.model.token.RDRTokenResponse
import remotedealer.rdrform.model.SalesRDMFormInfoModel
import remotedealer.rdrform.model.StackRDMFormInfoModel
import remotedealer.retrofit.RetroBase
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * Created by Shitalkumar on 01/12/2020
 */
class StackRDMFormFragment : BaseRDMFormFragment() {

    private var param1: Boolean? = null
    private var param2: String? = null
    private var stackRDMFormResponse = StackRDMFormInfoModel()
    private var oldFinancePenetrationActual = 0
    private var oldWarrantyPenetrationActual = 0
    private var oldPendingRCs = 0
    private var financePenetrationCalculation = 0.0
    private var warrantyPenetrationCalculation = 0.0
    private lateinit var mView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            isViewDetails = it.getBoolean(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Log.i(TAG, "onCreateView: ")
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_stack_rdm_form, container, false)
        return mView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()

        try {
            if (!isViewDetails) {
                callGetToken()
            } else {
                val savedData = preferenceManager.readString(STACK_RDM_INFO, "")
                if (savedData.trim().isNotEmpty()) {
                    stackRDMFormResponse = Gson().fromJson(savedData, StackRDMFormInfoModel::class.java)
                    updateUI()
                }
            }
        } catch (e: Exception) {
            Log.e(ProcurementRDMFormFragment.TAG, e.message.toString())
        }
        context?.let { LocalBroadcastManager.getInstance(it).registerReceiver(myBroadcastReceiver, IntentFilter(INTENT_FILTER_DATACHANGED)) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        context?.let { LocalBroadcastManager.getInstance(it).unregisterReceiver(myBroadcastReceiver) }
    }

    private fun initView() {
        layoutWarrantyUnitsAndValue.linLayHeader.visibility = View.GONE
        layoutWarrantyUnitsAndValue.textViewFirstField.text = "Warranty Units"
        layoutWarrantyUnitsAndValue.textViewSecondField.text = "Warranty Value"
        setEditTextEditable(layoutWarrantyUnitsAndValue.editTextFirstField, true, layoutWarrantyUnitsAndValue.textViewFirstField)
        setEditTextEditable(layoutWarrantyUnitsAndValue.editTextSecondField, true, layoutWarrantyUnitsAndValue.textViewSecondField)

        layoutFinanceAndAggregatorNo.linLayHeader.visibility = View.GONE
        layoutFinanceAndAggregatorNo.textViewFirstField.text = "Finance No"
        layoutFinanceAndAggregatorNo.textViewSecondField.text = "Aggregator No"
        setEditTextEditable(layoutFinanceAndAggregatorNo.editTextFirstField, true, layoutFinanceAndAggregatorNo.textViewFirstField)
        setEditTextEditable(layoutFinanceAndAggregatorNo.editTextSecondField, true, layoutFinanceAndAggregatorNo.textViewSecondField)

        layoutFinanceCasesAndPendingRCs.linLayHeader.visibility = View.GONE
        layoutFinanceCasesAndPendingRCs.textViewFirstField.text = "Finance cases need intervention"
        layoutFinanceCasesAndPendingRCs.textViewSecondField.text = "Pending \nRCs"
        setEditTextEditable(layoutFinanceCasesAndPendingRCs.editTextFirstField, true, layoutFinanceCasesAndPendingRCs.textViewFirstField)
        setEditTextEditable(layoutFinanceCasesAndPendingRCs.editTextSecondField, true, layoutFinanceCasesAndPendingRCs.textViewSecondField)

        layoutMIBLAndRoyaltyCollected.linLayHeader.visibility = View.GONE
        layoutMIBLAndRoyaltyCollected.textViewFirstField.text = "MIBL no"
        layoutMIBLAndRoyaltyCollected.textViewSecondField.text = "Royalty Collected"
      // layoutMIBLAndRoyaltyCollected.editTextSecondField1.inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS

        setEditTextEditable(layoutMIBLAndRoyaltyCollected.editTextFirstField, true, layoutMIBLAndRoyaltyCollected.textViewFirstField)
        setEditTextEditable(layoutMIBLAndRoyaltyCollected.editTextSecondField1, true, layoutMIBLAndRoyaltyCollected.textViewSecondField)

        layoutRoyaltyRs.linLayHeader.visibility = View.GONE
        layoutRoyaltyRs.linLayDouble.visibility = View.GONE
        layoutRoyaltyRs.linLayFull.visibility = View.VISIBLE
        layoutRoyaltyRs.textViewFullLayout.visibility = View.VISIBLE
        layoutRoyaltyRs.textViewFullLayout.text = "Royalty Rs"
        setEditTextEditable(layoutRoyaltyRs.editTextFullLayout, true, layoutRoyaltyRs.textViewFullLayout)

        layoutFinancePenetration.textViewTitle.text = "Finance Penetration"
        layoutFinancePenetration.textViewFirstField.text = getString(R.string.title_number)
        layoutFinancePenetration.textViewSecondField.text = getString(R.string.title_mtd)
        layoutFinancePenetration.textViewAchieved.visibility = View.VISIBLE
        layoutFinancePenetration.linLayTargetNo.visibility = View.VISIBLE
        setEditTextEditable(layoutFinancePenetration.editTextFirstField, false, null)
        setEditTextEditable(layoutFinancePenetration.editTextSecondField, false, null)

        layoutWarrantyPenetration.textViewTitle.text = "Warranty Penetration"
        layoutWarrantyPenetration.textViewFirstField.text = getString(R.string.title_number)
        layoutWarrantyPenetration.textViewSecondField.text = getString(R.string.title_mtd)
        layoutWarrantyPenetration.textViewAchieved.visibility = View.VISIBLE
        layoutWarrantyPenetration.linLayTargetNo.visibility = View.VISIBLE
        setEditTextEditable(layoutWarrantyPenetration.editTextFirstField, false, null)
        setEditTextEditable(layoutWarrantyPenetration.editTextSecondField, false, null)

        layoutPendingRCs.textViewTitle.text = "Pending RCs"
        layoutPendingRCs.textViewFirstField.text = getString(R.string.title_number)
        layoutPendingRCs.textViewSecondField.text = getString(R.string.title_mtd)
        layoutPendingRCs.textViewAchieved.visibility = View.VISIBLE
        layoutPendingRCs.linLayTargetNo.visibility = View.VISIBLE
        setEditTextEditable(layoutPendingRCs.editTextFirstField, false, null)
        setEditTextEditable(layoutPendingRCs.editTextSecondField, true, layoutPendingRCs.textViewSecondField)

        layoutRoyaltyCollected.textViewTitle.text = "Royalty Collected"
        layoutRoyaltyCollected.textViewFirstField.text = getString(R.string.title_status)
        layoutRoyaltyCollected.textViewSecondField.text = getString(R.string.title_status)
        layoutRoyaltyCollected.textViewAchieved.visibility = View.VISIBLE
        layoutRoyaltyCollected.linLayTargetNo.visibility = View.VISIBLE
        layoutRoyaltyCollected.textViewSecondField.inputType = InputType.TYPE_CLASS_TEXT
        setEditTextEditable(layoutRoyaltyCollected.editTextFirstField, false, null)
        setEditTextEditable(layoutRoyaltyCollected.editTextSecondField, false, null)

        if (isViewDetails)
            buttonStackNext.visibility = View.GONE

        buttonStackNext.setOnClickListener {
            rdmFormClicksListener.onNextButtonClick(getFilledData())
        }

        layoutMIBLAndRoyaltyCollected.editTextSecondField1.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                setValuesToCalculationVariables()
                updateFieldsOnDataChange()
                processButtonByTextLength()
            }
        })

        layoutFinanceAndAggregatorNo.editTextFirstField.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                setValuesToCalculationVariables()
                updateFieldsOnDataChange()
            }
        })

        layoutWarrantyUnitsAndValue.editTextSecondField.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                setValuesToCalculationVariables()
                updateFieldsOnDataChange()
            }
        })

        layoutWarrantyUnitsAndValue.editTextFirstField.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if (s.toString().isNotEmpty()) {
                    setValuesToCalculationVariables()
                    updateFieldsOnDataChange()
                }
            }
        })

        layoutFinanceCasesAndPendingRCs.editTextSecondField.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if (s.toString().isNotEmpty()) {
                    setValuesToCalculationVariables()
                    updateFieldsOnDataChange()
                }
            }
        })



        layoutWarrantyUnitsAndValue.editTextFirstField.setOnKeyListener(onKeyListener)
        layoutWarrantyUnitsAndValue.editTextSecondField.setOnKeyListener(onKeyListener)
        layoutFinanceAndAggregatorNo.editTextFirstField.setOnKeyListener(onKeyListener)
        layoutFinanceAndAggregatorNo.editTextSecondField.setOnKeyListener(onKeyListener)
        layoutFinanceCasesAndPendingRCs.editTextFirstField.setOnKeyListener(onKeyListener)
        layoutFinanceCasesAndPendingRCs.editTextSecondField.setOnKeyListener(onKeyListener)
        layoutMIBLAndRoyaltyCollected.editTextFirstField.setOnKeyListener(onKeyListener)
        layoutMIBLAndRoyaltyCollected.editTextSecondField1.setOnKeyListener(onKeyListener)
        layoutRoyaltyRs.editTextFullLayout.setOnKeyListener(onKeyListener)
    }

    private val onKeyListener = View.OnKeyListener { _, _, event ->
        if (event.action == KeyEvent.ACTION_UP) {
            processButtonByTextLength()
        }

        false
    }


    override fun onResume() {
        super.onResume()
        saveFormData()

    }

    private fun processButtonByTextLength() {
        val warrantyUnit = layoutWarrantyUnitsAndValue.editTextFirstField.text.toString().trim().isEmpty()
        val warrantyValue = layoutWarrantyUnitsAndValue.editTextSecondField.text.toString().trim().isEmpty()
        val financeNo = layoutFinanceAndAggregatorNo.editTextFirstField.text.toString().trim().isEmpty()
        val aggregatorNo = layoutFinanceAndAggregatorNo.editTextSecondField.text.toString().trim().isEmpty()
        val financeCases = layoutFinanceCasesAndPendingRCs.editTextFirstField.text.toString().trim().isEmpty()
        val pendingRCs = layoutFinanceCasesAndPendingRCs.editTextSecondField.text.toString().trim().isEmpty()
        val miblNo = layoutMIBLAndRoyaltyCollected.editTextFirstField.text.toString().trim().isEmpty()
        val royaltyCollected = layoutMIBLAndRoyaltyCollected.editTextSecondField1.text.toString().trim().isEmpty()
        val royaltyRs = layoutRoyaltyRs.editTextFullLayout.text.toString().trim().isEmpty()
        val pendingRCActual = layoutPendingRCs.editTextSecondField.text.toString().trim().isEmpty()

        if (warrantyUnit || warrantyValue || financeNo || aggregatorNo || royaltyRs || pendingRCActual
                || financeCases || pendingRCs || miblNo || royaltyCollected) {
            buttonStackNext.isEnabled = false
            buttonStackNext.setBackgroundDrawable(resources.getDrawable(R.drawable.rounded_button_background_dark_grey_disabled))
        } else {
            buttonStackNext.isEnabled = true
            buttonStackNext.setBackgroundDrawable(resources.getDrawable(R.drawable.rounded_button_background_blue_enabled))
        }

        var count = 0;
        if (!warrantyUnit)
            count++
        if (!warrantyValue)
            count++
        if (!financeNo)
            count++
        if (!aggregatorNo)
            count++
        if (!financeCases)
            count++
        if (!royaltyRs)
            count++
        if (!pendingRCs)
            count++
        if (!miblNo)
            count++
        if (!royaltyCollected)
            count++
        if (!pendingRCActual)
            count++

        val formCompletionPercentage = (100 * count) / 10
        rdmFormClicksListener.onFormFillListener("Stack", formCompletionPercentage.toString())
        if (formCompletionPercentage > 50) {
            saveFormData()
        }

    }

    private fun saveFormData() {
        preferenceManager.writeString(STACK_RDM_INFO, getFilledData())
    }


    private fun updateUI() {
        try {
            oldFinancePenetrationActual = stackRDMFormResponse.financePenetrationActual.toInt()
            oldWarrantyPenetrationActual = stackRDMFormResponse.warrantyPenetrationActual.toInt()
            oldPendingRCs = stackRDMFormResponse.pendingRcActual.toInt()

            // Note - Do not change the sequence of this
            layoutFinanceCasesAndPendingRCs.editTextSecondField.setText(stackRDMFormResponse.pendingRc90Days)

            layoutMIBLAndRoyaltyCollected.editTextSecondField1.setText(stackRDMFormResponse.royaltyCollected)

            layoutFinanceCasesAndPendingRCs.editTextFirstField.setText(stackRDMFormResponse.financeIntervention)

            layoutRoyaltyRs.editTextFullLayout.setText(stackRDMFormResponse.royaltyMoneyActual)

            layoutRoyaltyCollected.editTextSecondField.setText(stackRDMFormResponse.royaltyCollected)


            layoutMIBLAndRoyaltyCollected.editTextFirstField.setText(stackRDMFormResponse.miblNo)

            layoutWarrantyUnitsAndValue.editTextFirstField.setText(stackRDMFormResponse.warrantyUnits)
            layoutWarrantyUnitsAndValue.editTextSecondField.setText(stackRDMFormResponse.warrantyValue)

            layoutFinanceAndAggregatorNo.editTextFirstField.setText(stackRDMFormResponse.financeNo)
            layoutFinanceAndAggregatorNo.editTextSecondField.setText(stackRDMFormResponse.aggregatorNo)

            if (isViewDetails) {
                layoutFinancePenetration.editTextFirstField.setText(stackRDMFormResponse.financePenetrationToday)
                layoutWarrantyPenetration.editTextFirstField.setText(stackRDMFormResponse.warrantyPenetrationToday)
            }

            if (!isViewDetails) {
                setValuesToCalculationVariables()
                updateFieldsOnDataChange()
            }
            processButtonByTextLength()
        } catch (e: Exception) {
            Log.e(TAG, e.message.toString())
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updateFieldsOnDataChange() {
        try {

            if (stackRDMFormResponse.financePenetrationTarget.isNotEmpty() && stackRDMFormResponse.financePenetrationTarget != "0") {
                val financePenetration = (100 * stackRDMFormResponse.financePenetrationActual.toDouble()) / stackRDMFormResponse.financePenetrationTarget.toDouble()
                setAchievedPercentageColor(layoutFinancePenetration.textViewAchieved, financePenetration)
            }


            layoutFinancePenetration.editTextFirstField.setText("${financePenetrationCalculation.toInt()}")

            layoutFinancePenetration.editTextSecondField.setText("${stackRDMFormResponse.financePenetrationActual.toDouble().toInt()}")
            layoutFinancePenetration.textViewTargetNo.text = "Target : ${stackRDMFormResponse.financePenetrationTarget.toDouble().toInt()}"

            if (stackRDMFormResponse.warrantyPenetrationTarget.isNotEmpty() && stackRDMFormResponse.warrantyPenetrationTarget != "0") {
                val warrantyPenetration = (100 * stackRDMFormResponse.warrantyPenetrationActual.toDouble()) / stackRDMFormResponse.warrantyPenetrationTarget.toDouble()
                setAchievedPercentageColor(layoutWarrantyPenetration.textViewAchieved, warrantyPenetration)
            }

            layoutWarrantyPenetration.editTextFirstField.setText("${warrantyPenetrationCalculation.toInt()}")

            layoutWarrantyPenetration.editTextSecondField.setText("${stackRDMFormResponse.warrantyPenetrationActual.toDouble().toInt()}")
            layoutWarrantyPenetration.textViewTargetNo.text = "Target : ${stackRDMFormResponse.warrantyPenetrationTarget.toDouble().toInt()}"

            if (stackRDMFormResponse.pendingRcTarget.isNotEmpty()) {
                val pendingRCs = (100 * stackRDMFormResponse.pendingRcActual.toDouble()) / stackRDMFormResponse.pendingRcTarget.toDouble()
                setAchievedPercentageColorForPendingRCs(layoutPendingRCs.textViewAchieved, pendingRCs, stackRDMFormResponse.pendingRcActual)
            }
            layoutPendingRCs.textViewTargetNo.text = "Target : ${stackRDMFormResponse.pendingRcTarget}"

            setAchievedPercentageColorForRoyalty(layoutRoyaltyCollected.textViewAchieved, stackRDMFormResponse.royaltyCollected)
            layoutRoyaltyCollected.editTextFirstField.setText(stackRDMFormResponse.royaltyCollected)
            layoutRoyaltyCollected.editTextSecondField.setText(stackRDMFormResponse.royaltyCollected)
            layoutRoyaltyCollected.textViewTargetNo.text = "Target : ${stackRDMFormResponse.royaltyMoneyTarget}"

            if (layoutFinanceCasesAndPendingRCs.editTextSecondField.text.toString().trim().isNotEmpty())
                layoutPendingRCs.editTextFirstField.setText("${layoutFinanceCasesAndPendingRCs.editTextSecondField.text.toString().trim().toInt()}")
            else
                layoutPendingRCs.editTextFirstField.setText("0")

            layoutPendingRCs.editTextSecondField.setText(stackRDMFormResponse.pendingRcActual)
        } catch (e: Exception) {
            Log.e(TAG, e.message.toString())
        }
    }

    private fun setValuesToCalculationVariables() {

        try {
            stackRDMFormResponse.pendingRc90Days = layoutFinanceCasesAndPendingRCs.editTextSecondField.text.toString()

            if (layoutMIBLAndRoyaltyCollected.editTextSecondField1.text.toString() != "") {
                stackRDMFormResponse.royaltyCollected = layoutMIBLAndRoyaltyCollected.editTextSecondField1.text.toString()
            }

            if (layoutFinanceCasesAndPendingRCs.editTextSecondField.text.toString().trim().isNotEmpty() && !isViewDetails)
                stackRDMFormResponse.pendingRcActual = (layoutFinanceCasesAndPendingRCs.editTextSecondField.text.toString().trim().toInt() + oldPendingRCs).toString()
            else
                stackRDMFormResponse.pendingRcActual = oldPendingRCs.toString()

            val saleRDMFormResponseString = preferenceManager.readString(SALES_RDM_INFO, "")
            val saleRDMFormResponse = Gson().fromJson(saleRDMFormResponseString, SalesRDMFormInfoModel::class.java)
                    ?: return

            if (layoutFinanceAndAggregatorNo.editTextFirstField.text.toString().trim().isNotEmpty() && saleRDMFormResponse.retailSales != "0" && saleRDMFormResponse.retailSales != "00" && !isViewDetails) {
                val financeNo = layoutFinanceAndAggregatorNo.editTextFirstField.text.toString().trim().toDouble()

                if (saleRDMFormResponse.retailSales.toDouble() > 0) {
                    financePenetrationCalculation = ((financeNo / saleRDMFormResponse.retailSales.toDouble()) * 100)
                    stackRDMFormResponse.financePenetrationActual = (financePenetrationCalculation + oldFinancePenetrationActual).toInt().toString()
                }

            } else {
                financePenetrationCalculation = 0.0
                stackRDMFormResponse.financePenetrationActual = oldFinancePenetrationActual.toString()
            }

            if (layoutWarrantyUnitsAndValue.editTextFirstField.text.toString().trim().isNotEmpty() && saleRDMFormResponse.retailSales != "0" && saleRDMFormResponse.retailSales != "00" && !isViewDetails) {
                val warrantyUnit = layoutWarrantyUnitsAndValue.editTextFirstField.text.toString().trim().toDouble()
                if (saleRDMFormResponse.retailSales.toDouble() > 0) {
                    warrantyPenetrationCalculation = ((warrantyUnit / saleRDMFormResponse.retailSales.toDouble()) * 100)
                    stackRDMFormResponse.warrantyPenetrationActual = (warrantyPenetrationCalculation + oldWarrantyPenetrationActual).toInt().toString()
                }

            } else {
                warrantyPenetrationCalculation = 0.0
                stackRDMFormResponse.warrantyPenetrationActual = oldWarrantyPenetrationActual.toString()
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun getFilledData(): String {
        stackRDMFormResponse.warrantyUnits = layoutWarrantyUnitsAndValue.editTextFirstField.text.toString()
        stackRDMFormResponse.warrantyValue = layoutWarrantyUnitsAndValue.editTextSecondField.text.toString()

        stackRDMFormResponse.financeNo = layoutFinanceAndAggregatorNo.editTextFirstField.text.toString()
        stackRDMFormResponse.aggregatorNo = layoutFinanceAndAggregatorNo.editTextSecondField.text.toString()

        stackRDMFormResponse.financeIntervention = layoutFinanceCasesAndPendingRCs.editTextFirstField.text.toString()

        stackRDMFormResponse.miblNo = layoutMIBLAndRoyaltyCollected.editTextFirstField.text.toString()
        stackRDMFormResponse.royaltyCollected = layoutMIBLAndRoyaltyCollected.editTextSecondField1.text.toString()

        stackRDMFormResponse.royaltyMoneyActual = layoutRoyaltyRs.editTextFullLayout.text.toString()
        stackRDMFormResponse.pendingRc90Days = layoutFinanceCasesAndPendingRCs.editTextSecondField.text.toString()

        stackRDMFormResponse.financePenetrationToday = layoutFinancePenetration.editTextFirstField.text.toString()
        stackRDMFormResponse.warrantyPenetrationToday = layoutWarrantyPenetration.editTextFirstField.text.toString()

        return Gson().toJson(stackRDMFormResponse)
    }

    private fun setAchievedPercentageColorForPendingRCs(textView: TextView, percentage: Double, pendingRcActual: String) {
        when {
            pendingRcActual.toInt() == 0 -> {
                textView.text = "100% Achieved"
                textView.setTextColor(resources.getColor(R.color.text_view_green))
            }
            pendingRcActual.toInt() > 0 -> {
                textView.text = "0% Achieved"
                textView.setTextColor(resources.getColor(R.color.text_view_red))
            }
        }
    }

    private fun setAchievedPercentageColorForRoyalty(textView: TextView, percentage: String) {
        when {
            percentage.contains("yes", ignoreCase = true) -> {
                textView.text = "100% Achieved"
                textView.setTextColor(resources.getColor(R.color.text_view_green))
            }
            percentage.contains("no", ignoreCase = true) -> {
                textView.text = "0% Achieved"
                textView.setTextColor(resources.getColor(R.color.text_view_red))
            }
        }
    }

    private fun getStackInfo(token: String) {
        val finalURL = RetroBase.URL_END_STOCKS + preferenceManager.readString(SELECTED_DEALER_CODE, "")
        RetroBase.retroInterface.getFromWeb(finalURL, "Bearer $token").enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                try {
                    SpinnerManager.hideSpinner(requireActivity())
                    val strRes = Gson().toJson(response.body())
                    stackRDMFormResponse = Gson().fromJson(strRes, StackRDMFormInfoModel::class.java)
                    Log.d(TAG, "onResponse: $stackRDMFormResponse")
                    updateUI()
                } catch (e: Exception) {
                    SpinnerManager.hideSpinner(requireActivity())
                    Log.e(TAG, e.message.toString())
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(requireActivity())
            }
        })
    }

    private fun callGetToken() {
        SpinnerManager.showSpinner(requireActivity())
        RetroBase.retroInterface.getFromWeb(RDRTokenRequest(), RetroBase.URL_END_GET_TOKEN).enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                try {
                    val strRes = Gson().toJson(response.body())
                    val resModel = Gson().fromJson(strRes, RDRTokenResponse::class.java)
                    Log.i(TAG, "onResponse: " + resModel.token)
                    getStackInfo(resModel.token)
                } catch (e: Exception) {
                    SpinnerManager.hideSpinner(requireActivity())
                    Log.e(TAG, e.message.toString())
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(requireActivity())
            }
        })
    }

    companion object {
        val TAG: String = StackRDMFormFragment.javaClass.simpleName

        @JvmStatic
        fun newInstance(param1: Boolean, param2: String?, listener: RDMFormClicksListener) =
                StackRDMFormFragment().apply {
                    arguments = Bundle().apply {
                        putBoolean(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                    rdmFormClicksListener = listener
                }
    }


    private val myBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {

            setValuesToCalculationVariables()
            updateFieldsOnDataChange()
        }
    }
}