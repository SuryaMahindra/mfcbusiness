package remotedealer.rdrform.ui;

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mfcwl.mfc_dealer.R

/*
 * Created By Udaya(MFCWL) ->  03-12-2020 13:10
 *
 */
public class WebLeadsFragment : Fragment() {

    var TAG = javaClass.simpleName
    lateinit var vi: View

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?,
    ): View {
        Log.i(TAG, "onCreateView: ")

        vi = inflater.inflate(R.layout.rdm_walkin_leads, container, false)

        return vi
    }
}