package remotedealer.rdrform.ui

import android.content.Context
import android.graphics.Paint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mfcwl.mfc_dealer.R
import remotedealer.rdrform.model.StoredInitiative

class AddRemarksListViewAdapter(
        private var category: String,
        private var storedInitiatives: ArrayList<StoredInitiative>,
        private var stringArrayData: List<String>,
        private var addRemarksClickListener: AddRemarksClickListener
) : RecyclerView.Adapter<AddRemarksListViewAdapter.AddRemarksViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddRemarksViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return AddRemarksViewHolder(inflater, parent, addRemarksClickListener, category)
    }

    override fun getItemCount(): Int {
        return stringArrayData.size
    }

    override fun onBindViewHolder(holder: AddRemarksViewHolder, position: Int) {
        val initiative = stringArrayData[position]
        holder.bind(initiative)
        for (item in storedInitiatives) {
            if (item.category.equals(category, ignoreCase = true) && holder.textViewHeading.text.equals(item.actionitem)) {
                    if(!item.remarks.equals("")){
                        holder.textViewHeading.performClick()
                        holder.editTextAddRemark.setText(item.remarks)
                    }
            }

        }
    }

    class AddRemarksViewHolder(inflater: LayoutInflater, parent: ViewGroup, addRemarksClickListener: AddRemarksClickListener, category: String) :
            RecyclerView.ViewHolder(inflater.inflate(R.layout.fragment_add_remarks_list_item_view, parent, false)) {

        var context: Context = parent.context
        var textViewHeading: TextView = itemView.findViewById(R.id.textViewHeading)
        var buttonAddRemark: Button = itemView.findViewById(R.id.buttonAddRemark)
        var editTextAddRemark: EditText = itemView.findViewById(R.id.editTextAddRemark)
        var linLayAddRemarks: LinearLayout = itemView.findViewById(R.id.linLayAddRemarks)
        var viewDivider: View = itemView.findViewById(R.id.viewDivider)



        init {
            buttonAddRemark.setOnClickListener {
                val remark = editTextAddRemark.text.toString()
                addRemarksClickListener.onRemarkAdded(remark, textViewHeading.text.toString(), category)
            }

            textViewHeading.setOnClickListener {
                handleRemarksSection()
            }

        }

        fun bind(data: String) {
            textViewHeading.text = data
        }

        private fun handleRemarksSection() {
            if (linLayAddRemarks.visibility == View.VISIBLE) {
                linLayAddRemarks.visibility = View.GONE
                viewDivider.visibility = View.GONE
                textViewHeading.setTextColor(context.resources.getColor(R.color.text_heading_remarks_color))
                textViewHeading.paintFlags = textViewHeading.paintFlags and Paint.UNDERLINE_TEXT_FLAG.inv()
            } else {
                linLayAddRemarks.visibility = View.VISIBLE
                viewDivider.visibility = View.VISIBLE
                textViewHeading.setTextColor(context.resources.getColor(R.color.edit_text_blue))
                textViewHeading.paintFlags = textViewHeading.paintFlags or Paint.UNDERLINE_TEXT_FLAG
            }
        }
    }

    companion object {
        val TAG: String = AddRemarksListViewAdapter.javaClass.simpleName
    }
}