package remotedealer.rdrform.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mfcwl.mfc_dealer.R
import remotedealer.rdrform.model.Initiative

class PreviousRemarksListViewAdapter(
        private var initiativesData: List<Initiative>
) : RecyclerView.Adapter<PreviousRemarksListViewAdapter.PreviousRemarksViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PreviousRemarksViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return PreviousRemarksViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int {
        return initiativesData.size
    }

    override fun onBindViewHolder(holder: PreviousRemarksViewHolder, position: Int) {
        val initiative: Initiative = initiativesData[position]
        holder.bind(initiative)
    }

    class PreviousRemarksViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
            RecyclerView.ViewHolder(inflater.inflate(R.layout.fragment_previous_remarks_list_item_view, parent, false)) {

        private var textViewPreviousRemarksHeading: TextView? = null
        private var textViewPreviousRemarksMessage: TextView? = null

        init {
            textViewPreviousRemarksHeading = itemView.findViewById(R.id.textViewPreviousRemarksHeading)
            textViewPreviousRemarksMessage = itemView.findViewById(R.id.textViewPreviousRemarksMessage)
        }

        fun bind(initiative: Initiative) {
            textViewPreviousRemarksHeading?.text = initiative.actionItem
            textViewPreviousRemarksMessage?.text = "Remarks: " + initiative.remark
        }
    }

    companion object {
        val TAG: String = PreviousRemarksListViewAdapter.javaClass.simpleName
    }
}