package remotedealer.rdrform.ui;

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mfcwl.mfc_dealer.R
import remotedealer.rdrform.ui.RDMHomeActivity.Companion.currentFragment

/*
 * Created By Uday(MFCWL) ->  04-12-2020 17:19
 *
 */
class RDMMeetingFragment : Fragment() {
    var TAG = javaClass.simpleName
    lateinit var vi: View

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        Log.i(TAG, "onCreateView: ")
        currentFragment = TAG
        vi = inflater.inflate(R.layout.rdr_meeting_fragment, container, false)

        return vi
    }
}