package remotedealer.rdrform.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.HorizontalScrollView
import android.widget.Toast
import com.google.gson.Gson
import com.mfcwl.mfc_dealer.R
import com.mfcwl.mfc_dealer.Utility.CommonMethods
import com.mfcwl.mfc_dealer.Utility.SpinnerManager
import kotlinx.android.synthetic.main.fragment_r_d_m_form.*
import remotedealer.model.token.RDRTokenRequest
import remotedealer.model.token.RDRTokenResponse
import remotedealer.rdrform.model.*
import remotedealer.retrofit.RetroBase
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private const val ARG_PARAM3 = "param3"

/**
 * Created by Shitalkumar on 30/11/2020
 */
class RDMFormFragment : BaseRDMFormFragment(), RDMFormClicksListener {

    private var bIsProcurementComplete = false
    private var bIsSalesComplete = false
    private var bIsStackComplete = false
    private var IsRDRSubmited = false
    private lateinit var remarksInfoModel: RemarksInfoModel
    private var storedInitiativesTemp: ArrayList<StoredInitiative> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            dealerCode = it.getString(ARG_PARAM1)
            token = it.getString(ARG_PARAM2)
            isViewDetails = it.getBoolean(ARG_PARAM3)

            preferenceManager.writeString(SELECTED_DEALER_CODE, dealerCode)
            preferenceManager.writeString(PROCUREMENT_RDM_INFO, "")
            preferenceManager.writeString(SALES_RDM_INFO, "")
            preferenceManager.writeString(STACK_RDM_INFO, "")
            preferenceManager.writeString(ACTION_ITEMS_INFO, "")
            preferenceManager.writeString(ACTION_ITEMS_UPDATED_DATA, "")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Log.i(TAG, "onCreateView: ")
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_r_d_m_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        token?.let { getRDRActionItems(it) }
    }

    fun initView() {
        if (isViewDetails)
            callGetToken(false)
        else
            setViewPagerAdapter()

        buttonProc.isSelected = true
        handleButtonSelection()

        floatingButton.setOnClickListener {
            val bottomSheet = RemarksRDMFormBottomSheetFragment.newInstance(dealerCode.toString(), isViewDetails)
            bottomSheet.show(requireActivity().supportFragmentManager, bottomSheet.tag)
        }


    }

    private fun setViewPagerAdapter() {
        viewPager.adapter = createViewPagerAdapter()
        viewPager.isUserInputEnabled = false
        //Added by surya it we have to  prevent one crash raised by QA team
        viewPager.isSaveEnabled = false
    }

    private fun handleButtonSelection() {
        buttonProc.setOnClickListener {
            buttonProc.isSelected = true
            buttonSales.isSelected = false;
            buttonStack.isSelected = false;
            viewPager.currentItem = 0
            horizontalScrollView.fullScroll(HorizontalScrollView.FOCUS_LEFT)
        }

        buttonSales.setOnClickListener {
            buttonSales.isSelected = true
            buttonProc.isSelected = false;
            buttonStack.isSelected = false;
            viewPager.currentItem = 1
            autoSmoothScrollToRight()
            RDMHomeActivity.tvSavedLbl.visibility =  View.VISIBLE
        }

        buttonStack.setOnClickListener {
            buttonProc.isSelected = false;
            buttonSales.isSelected = false;
            buttonStack.isSelected = true
            viewPager.currentItem = 2
            RDMHomeActivity.tvSavedLbl.visibility =  View.VISIBLE
            autoSmoothScrollToRight()

        }
    }

    private fun autoSmoothScrollToRight() {
        horizontalScrollView.postDelayed({
            horizontalScrollView.smoothScrollBy(700, 500)
        }, 50)
    }

    private fun createViewPagerAdapter(): RDMFormViewPagerAdapter {
        rdmFormClicksListener = this
        return RDMFormViewPagerAdapter(requireActivity(), this, isViewDetails)
    }

    override fun onNextButtonClick(data: String) {
        when (viewPager.currentItem) {
            0 -> {
                buttonSales.callOnClick()
                preferenceManager.writeString(PROCUREMENT_RDM_INFO, data)
            }
            1 -> {
                buttonStack.callOnClick()
                preferenceManager.writeString(SALES_RDM_INFO, data)
            }
            2 -> {
                preferenceManager.writeString(STACK_RDM_INFO, data)
                if (bIsProcurementComplete && bIsSalesComplete && bIsStackComplete){
                    IsRDRSubmited = true
                    callGetToken(true)
                } else
                    Toast.makeText(requireContext(), "One of the form(s) is incomplete. kindly fill all forms data and click on submit", Toast.LENGTH_SHORT).show()
            }
        }
    }


    @SuppressLint("SetTextI18n")
    override fun onFormFillListener(category: String, percentage: String) {
        if (category.equals("Procurement", ignoreCase = true)) {
            buttonProc.text = "Procurement ($percentage%)"
            if (percentage.toInt() > 99)
                bIsProcurementComplete = true
        }

        if (category.equals("Sales", ignoreCase = true)) {
            buttonSales.text = "Sales ($percentage%)"
            if (percentage.toInt() > 99)
                bIsSalesComplete = true
        }

        if (category.equals("Stack", ignoreCase = true)) {
            buttonStack.text = "Stack ($percentage%)"
            if (percentage.toInt() > 99)
                bIsStackComplete = true
        }
    }



    private fun getRDRForm() {
        val viewRDRFormRequest = ViewRDRFormRequest()
        viewRDRFormRequest.dealerCode = preferenceManager.readString(SELECTED_DEALER_CODE,"")
        viewRDRFormRequest.createdOn = getTodayDate()
        SpinnerManager.showSpinner(requireActivity())

        RetroBase.retroInterface.getFromWeb(viewRDRFormRequest, RetroBase.URL_GET_RDR_FORM, "Bearer $token").enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                try {
                    SpinnerManager.hideSpinner(requireActivity())
                    val strRes = Gson().toJson(response.body())
                    val resModel = Gson().fromJson(strRes, ViewRDRFormResponse::class.java)
                    Log.d(TAG, "URL_GET_RDR_FORM - onResponse: $resModel")
                    preferenceManager.writeString(PROCUREMENT_RDM_INFO, Gson().toJson(resModel.procurementRDMFormInfoModel))
                    preferenceManager.writeString(SALES_RDM_INFO, Gson().toJson(resModel.salesRDMFormInfoModel))
                    preferenceManager.writeString(STACK_RDM_INFO, Gson().toJson(resModel.stackRDMFormInfoModel))
                    preferenceManager.writeString(ACTION_ITEMS_INFO, Gson().toJson(resModel.actionItemsList))
                    setViewPagerAdapter()
                } catch (e: Exception) {
                    SpinnerManager.hideSpinner(activity)
                    Log.e(TAG, e.message.toString())
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(activity)
            }
        })
    }

    private fun callGetToken(isSubmitClick: Boolean) {
       // SpinnerManager.showSpinner(requireActivity()) // commented by surya, its giving exception will discuss further
        RetroBase.retroInterface.getFromWeb(RDRTokenRequest(), RetroBase.URL_END_GET_TOKEN).enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                try {
                   // SpinnerManager.hideSpinner(requireActivity())// commented by surya, its giving exception will discuss further
                    val strRes = Gson().toJson(response.body())
                    val resModel = Gson().fromJson(strRes, RDRTokenResponse::class.java)
                    token = resModel.token
                    Log.i(TAG, "URL_END_GET_TOKEN - onResponse: " + resModel.token)
                    if (isSubmitClick)
                        submitRDRFormData(resModel.token)
                    else
                        getRDRForm()
                } catch (e: Exception) {
                   // SpinnerManager.hideSpinner(requireActivity())// commented by surya, its giving exception will discuss further
                    Log.e(TAG, e.message.toString())
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(requireActivity())
            }
        })
    }

    private fun submitRDRFormData(token: String) {
        val reportFor = ReportFor()
        reportFor.dealerCode = preferenceManager.readString(SELECTED_DEALER_CODE,"")
        reportFor.isOperational = "YES"
        reportFor.videoUrl = "2"
        reportFor.visitedById = CommonMethods.getstringvaluefromkey(requireActivity(), "user_id").toInt()

        val rdrFormRequest = RDRFormRequest()
        rdrFormRequest.reportFor = reportFor
        rdrFormRequest.procurementRDMFormInfoModel = Gson().fromJson(preferenceManager.readString(PROCUREMENT_RDM_INFO, ""), ProcurementRDMFormInfoModel::class.java)
        rdrFormRequest.salesRDMFormInfoModel = Gson().fromJson(preferenceManager.readString(SALES_RDM_INFO, ""), SalesRDMFormInfoModel::class.java)
        rdrFormRequest.stackRDMFormInfoModel = Gson().fromJson(preferenceManager.readString(STACK_RDM_INFO, ""), StackRDMFormInfoModel::class.java)
        val actionItemsList = Gson().fromJson(preferenceManager.readString(ACTION_ITEMS_INFO, ""), ActionItemsList::class.java)
        if (actionItemsList != null) {
            rdrFormRequest.actionItemsList = actionItemsList.actionItemInfoModels.distinctBy { it.category to  it.actionItem } as ArrayList<ActionItemInfoModel>
        }

        RetroBase.retroInterface.getFromWeb(rdrFormRequest, RetroBase.URL_END_INSERT_RDR_FORM, "Bearer $token").enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                try {
                    SpinnerManager.hideSpinner(requireActivity())
                    val strRes = Gson().toJson(response.body())
                    val resModel = Gson().fromJson(strRes, RDRFormResponse::class.java)
                    Log.d(TAG, "URL_END_INSERT_RDR_FORM - onResponse: $resModel")
                    updateUIOnSubmitResponse(resModel)
                } catch (e: Exception) {
                   // SpinnerManager.hideSpinner(requireActivity())
                    Log.e(TAG, e.message.toString())
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(requireActivity())
            }
        })
    }

    private fun updateUIOnSubmitResponse(resModel: RDRFormResponse) {
        try {
            Toast.makeText(requireContext(), resModel.message, Toast.LENGTH_SHORT).show()
            Log.i(TAG, resModel.message)
            requireActivity().finish()
        } catch (e: Exception) {
            Log.e(TAG, e.message.toString())
        }
    }

    fun getTodayDate(): String {
        val today = Calendar.getInstance().time
        val format = SimpleDateFormat("yyyy-MM-dd")
        return format.format(today)
    }

    companion object {
        val TAG: String = RDMFormFragment.javaClass.simpleName

        @JvmStatic
        fun newInstance(param1: String, param2: String, isViewDetails: Boolean) =
                RDMFormFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                        putBoolean(ARG_PARAM3, isViewDetails)
                    }
                }
    }

    override fun onPause() {
        super.onPause()
        Log.e(TAG, "onPause Called ()")
        //saveData()
    }

    override fun onStop() {
        super.onStop()
        Log.e(TAG, "onStop Called ()")
    }



    override fun onDestroy() {
        super.onDestroy()

        Log.e(TAG, "onDestroy Called ()")
        var procdata = preferenceManager.readString(PROCUREMENT_RDM_INFO, "")

        if(!isViewDetails && !IsRDRSubmited && procdata != ""){
            token?.let { submitRDRDraftFormData(it) }
        }

    }



    private fun submitRDRDraftFormData(token: String) {
     try {
         val reportFor = ReportFor()
         reportFor.dealerCode = preferenceManager.readString(SELECTED_DEALER_CODE,"")
         reportFor.isOperational = "YES"
         reportFor.videoUrl = "2"
         reportFor.visitedById = CommonMethods.getstringvaluefromkey(requireActivity(), "user_id").toInt()
         val rdrFormRequest = RDRFormRequest()
         rdrFormRequest.reportFor = reportFor
         var procdata = preferenceManager.readString(PROCUREMENT_RDM_INFO, "")
         var salesdata = preferenceManager.readString(SALES_RDM_INFO, "")
         var stockdata = preferenceManager.readString(STACK_RDM_INFO, "")

         if(!procdata.equals("")){
             rdrFormRequest.procurementRDMFormInfoModel = Gson().fromJson(procdata, ProcurementRDMFormInfoModel::class.java)
         }
         if(!salesdata.equals("")){
             rdrFormRequest.salesRDMFormInfoModel = Gson().fromJson(salesdata, SalesRDMFormInfoModel::class.java)
         }

         if(!stockdata.equals("")){
             rdrFormRequest.stackRDMFormInfoModel = Gson().fromJson(stockdata, StackRDMFormInfoModel::class.java)
         }

         val actionItemsList = Gson().fromJson(preferenceManager.readString(ACTION_ITEMS_INFO, ""), ActionItemsList::class.java)
         if (actionItemsList != null) {
             rdrFormRequest.actionItemsList = actionItemsList.actionItemInfoModels.distinctBy { it.actionItem to it.category } as ArrayList<ActionItemInfoModel>
         }

         RetroBase.retroInterface.getFromWeb(rdrFormRequest, RetroBase.URL_END_SAVE_RDR_FORM, "Bearer $token").enqueue(object : Callback<Any> {
             override fun onResponse(call: Call<Any>, response: Response<Any>) {
                 try {

                     val strRes = Gson().toJson(response.body())
                     val resModel = Gson().fromJson(strRes, RDRFormResponse::class.java)
                     Log.d(TAG, "URL_END_INSERT_RDR_FORM - onResponse: $resModel")

                 } catch (e: Exception) {

                     Log.e(TAG, e.message.toString())
                 }
             }

             override fun onFailure(call: Call<Any>, t: Throwable) {
                t.printStackTrace()
             }
         })

     }catch (e:java.lang.Exception){
         e.printStackTrace()
     }
    }


    private fun getRDRActionItems(token: String) {
        val finalURL = RetroBase.URL_END_GET_RDR_ACTON_ITEM + dealerCode
        RetroBase.retroInterface.getFromWeb(finalURL, "Bearer $token").enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                try {
                    val strRes = Gson().toJson(response.body())
                    remarksInfoModel = Gson().fromJson(strRes, RemarksInfoModel::class.java)
                    Log.d(RemarksRDMFormBottomSheetFragment.TAG, "URL_END_GET_RDR_ACTON_ITEM - onResponse: $remarksInfoModel")
                    Log.d(RemarksRDMFormBottomSheetFragment.TAG, "stored items:" + remarksInfoModel.storedInitiatives.size)
                    storedInitiativesTemp.addAll(remarksInfoModel.storedInitiatives)

                    if (remarksInfoModel.storedInitiatives.size > 0) {
                        var arrayListActionItemInfoModel = ActionItemsList()
                        for (data in storedInitiativesTemp) {
                            val actionItemInfoModel = ActionItemInfoModel()
                            actionItemInfoModel.actionItem = data.actionitem
                            actionItemInfoModel.category = data.category
                            actionItemInfoModel.remarks = data.remarks
                            arrayListActionItemInfoModel.actionItemInfoModels.add(actionItemInfoModel)
                        }
                        preferenceManager.writeString(ACTION_ITEMS_INFO, Gson().toJson(arrayListActionItemInfoModel))
                    }

                } catch (e: Exception) {
                    Log.e(RemarksRDMFormBottomSheetFragment.TAG, e.message.toString())
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
            }
        })
    }
}