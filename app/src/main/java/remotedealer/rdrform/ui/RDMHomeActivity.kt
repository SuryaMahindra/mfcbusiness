package remotedealer.rdrform.ui

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.mfcwl.mfc_dealer.ASMFragments.ASMDealerFragment
import com.mfcwl.mfc_dealer.R
import com.mfcwl.mfc_dealer.Utility.AppConstants
import com.mfcwl.mfc_dealer.Utility.AppConstants.DEALER_CODE
import com.mfcwl.mfc_dealer.Utility.CommonMethods
import com.mfcwl.mfc_dealer.Utility.CommonMethods.setvalueAgainstKey
import com.mfcwl.mfc_dealer.Utility.SpinnerManager
import kotlinx.android.synthetic.main.activity_rdm_home.*
import kotlinx.android.synthetic.main.rdm_dealer_list_dialog.*
import remotedealer.dashboard.model.RDRStatusResponse
import remotedealer.dashboard.ui.RemoteDealerDashboard
import remotedealer.interfaces.DealerInterface
import remotedealer.model.DealerItem
import remotedealer.model.token.RDRTokenRequest
import remotedealer.model.token.RDRTokenResponse
import remotedealer.retrofit.RetroBase
import remotedealer.retrofit.RetroBase.Companion.URL_END_GET_TOKEN
import remotedealer.retrofit.RetroBase.Companion.retroInterface
import remotedealer.util.NavigationUtility
import remotedealer.util.RedirectionConstants
import remotedealer.util.RedirectionConstants.Companion.REDIRECT_PROCUREMENT
import remotedealer.util.RedirectionConstants.Companion.REDIRECT_SALES_AND_LEADS
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import sidekicklpr.PreferenceManager

/**
 * Created by Shitalkumar on 27/11/2020
 */
class RDMHomeActivity : AppCompatActivity() {

    var TAG = javaClass.simpleName
    var code = ""
    var token = ""
    var redirectionString = ""
    var leadsClickedItem = ""
    var name = ""
    var meetingType = ""
    private lateinit var rdmFormFragment: RDMFormFragment
    private lateinit var bottomNavigationView: com.ismaeldivita.chipnavigation.ChipNavigationBar
    private lateinit var eventListFragment: EventListFragment
    private lateinit var RDRStatus: String
    val fragmentStack = mutableListOf<String>()
    lateinit var preferenceManager: PreferenceManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rdm_home)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            window.statusBarColor = Color.WHITE
        }
        RDMHomeActivity.tvSavedLbl = findViewById(R.id.tvSavedLbl)
        bottomNavigationView = findViewById(R.id.bottomNavigationView)
        tvSavedLbl.visibility = View.GONE
        Log.i(TAG, "onCreate: ")

        try {
            redirectionString = intent.getStringExtra(RedirectionConstants.REDIRECT_KEY).toString()
            tvRdrHeaderTitle.text = intent.getStringExtra(AppConstants.DEALER_NAME)
            Log.i(TAG, "onCreate: " + intent.getStringExtra(AppConstants.DEALER_NAME))

            code = intent.getStringExtra(DEALER_CODE).toString()
            Log.i(TAG, "onCreate: $code")
            name = intent.getStringExtra(AppConstants.DEALER_NAME).toString()
            RDRStatus = intent.getStringExtra(AppConstants.RDR_STATUS).toString()
            meetingType = intent.getStringExtra(AppConstants.MEETING_TYPE).toString()


        } catch (e: Exception) {
            Log.e(TAG, "onCreate: " + e.message)
        }

        setvalueAgainstKey(this@RDMHomeActivity, "dealer_code", intent.getStringExtra(DEALER_CODE).toString())



        bottomNavigationView.setOnItemSelectedListener {
            when (it) {
                R.id.nav_dealer -> {
                    tvSavedLbl.visibility = View.GONE
                    setCurrentFragment(RemoteDealerDashboard(code, token), DASHBOARD)
                }
                R.id.nav_proc_and_stock -> {
                    tvSavedLbl.visibility = View.GONE
                    val frag = ProcAndSalesFragment()
                    frag.code = code
                    setCurrentFragment(frag, PROCANDSALES)
                }
                R.id.nav_sales_and_leads -> {
                    tvSavedLbl.visibility = View.GONE
                    val frag = RDMSalesAndLeadsFragment(code, token, leadsClickedItem)
                    setCurrentFragment(frag, SALESANDLEADS)
                    leadsClickedItem = ""
                }
                R.id.nav_meetings -> {
                    tvSavedLbl.visibility = View.GONE
                    val frag = EventListFragment()
                    frag.code = code
                    frag.name = name
                    frag.meetingType = meetingType
                    setCurrentFragment(frag, EVENTS)
                }
                R.id.nav_rdm_form -> {
                    tvSavedLbl.visibility = View.GONE
                    if (RDRStatus.equals("Completed", ignoreCase = true)) {
                        rdmFormFragment = RDMFormFragment.newInstance(code, token, true)
                    } /*else {
                        rdmFormFragment = RDMFormFragment.newInstance(code, token, false)
                    }*/
                    setCurrentFragment(rdmFormFragment, RDRFORM)
                }
            }
            true

        }


        backtodahsboard.setOnClickListener { view ->
            fragmentStack.clear()
            finish()
        }
        backarrow.setOnClickListener { view ->
            onBackPressed()
        }
        var openStatus = false

        arrow.setOnClickListener {
            tvRdrHeaderCompletedStatus.performClick()
        }

        tvRdrHeaderCompletedStatus.setOnClickListener {
            if (!openStatus) {
                openStatus = true
                try {
                    tvRdrHeaderCompletedStatus.setCompoundDrawables(null, null, getDrawable(R.drawable.ic_baseline_keyboard_arrow_up_24), null)
                    arrow.setImageResource(R.drawable.ic_baseline_keyboard_arrow_up_24)
                    setCurrentFragmentWithoutTag(ASMDealerFragment(code, token, object : DealerInterface {
                        override fun onUpdate(dealer: DealerItem) {
                            tvRdrHeaderCompletedStatus.setCompoundDrawables(null, null, getDrawable(R.drawable.ic_rdm_app_bar_dd), null)
                            tvRdrHeaderTitle.text = dealer.name
                            tvRdrHeaderCompletedStatus.text = "RDR 75% completed"
                            arrow.setImageResource(R.drawable.ic_rdm_app_bar_dd)

                            code = dealer.code
                            openStatus = false
                            RDRStatus = dealer.rdrstatus
                            if (dealer.rdrstatus.equals("Completed", ignoreCase = true)) {
                                tvRdrHeaderCompletedStatus.text = "RDR 100% completed"
                            } else {
                                tvRdrHeaderCompletedStatus.text = "RDR 75% completed"
                            }
                            if (RDRStatus.equals("Completed", ignoreCase = true)) {
                                bottomNavigationView.setMenuResource(R.menu.asm_bottom_navigation_green)

                            } else {
                                bottomNavigationView.setMenuResource(R.menu.asm_bottom_navigation)
                            }

                            rdmFormFragment = if (RDRStatus.equals("Completed", ignoreCase = true)) {
                                RDMFormFragment.newInstance(code, token, true)
                            } else {
                                RDMFormFragment.newInstance(code, token, false)
                            }

                            NavigationUtility.removeAllFragments(this@RDMHomeActivity)

                            when (currentFragment) {
                                DASHBOARD -> {
                                    val view: View = bottomNavigationView.findViewById(R.id.nav_dealer)
                                    view.performClick()
                                }

                                PROCANDSALES -> {
                                    val view: View = bottomNavigationView.findViewById(R.id.nav_proc_and_stock)
                                    view.performClick()
                                }
                                SALESANDLEADS -> {
                                    val view: View = bottomNavigationView.findViewById(R.id.nav_sales_and_leads)
                                    view.performClick()
                                }
                                EVENTS -> {
                                    val view: View = bottomNavigationView.findViewById(R.id.nav_meetings)
                                    view.performClick()
                                }
                                RDRFORM -> {
                                    val view: View = bottomNavigationView.findViewById(R.id.nav_rdm_form)
                                    view.performClick()
                                }
                            }


                        }

                    }))

                } catch (exce: Exception) {
                    exce.printStackTrace()
                }
            } else {
                openStatus = false
                arrow.setImageResource(R.drawable.ic_rdm_app_bar_dd)
                when (currentFragment) {
                    DASHBOARD -> {
                        setCurrentFragment(RemoteDealerDashboard(code, token), currentFragment)
                    }
                    PROCANDSALES -> {
                        val frag = ProcAndSalesFragment()
                        frag.code = code
                        setCurrentFragment(frag, currentFragment)
                    }
                    SALESANDLEADS -> {
                        val frag = RDMSalesAndLeadsFragment(code, token, "")
                        setCurrentFragment(frag, currentFragment)
                    }
                    EVENTS -> {
                        val frag = EventListFragment()
                        frag.code = code
                        frag.name = name
                        frag.meetingType = meetingType
                        setCurrentFragment(frag, currentFragment)
                    }
                    RDRFORM -> {
                        // val rdmFormFragment = RDMFormFragment()
                        setCurrentFragment(rdmFormFragment, currentFragment)
                    }
                    else -> {
                        Toast.makeText(this, "Else case ", Toast.LENGTH_LONG).show()
                    }
                }
            }

        }


        callGetToken()


    }

    private fun callGetToken() {

        SpinnerManager.showSpinner(this)
        retroInterface.getFromWeb(RDRTokenRequest(), URL_END_GET_TOKEN).enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                SpinnerManager.hideSpinner(this@RDMHomeActivity)

                try {
                    val strRes = Gson().toJson(response.body())
                    val resModel = Gson().fromJson(strRes, RDRTokenResponse::class.java)

                    token = resModel.token

                    Log.i(TAG, "onResponse: " + resModel.token)
                    getDealersList(resModel.token)


                } catch (e: Exception) {
                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(this@RDMHomeActivity)
            }

        })

    }


    private fun redirectIfRequired(option: String) {
        when (option) {
            RedirectionConstants.DEALER_DASHBOARD -> {
                setCurrentFragment(RemoteDealerDashboard(code, token), DASHBOARD)
                val view: View = bottomNavigationView.findViewById(R.id.nav_dealer)
                view.performClick()
            }

            RedirectionConstants.DEALER_EVENTS -> {
                Log.i(TAG, "redirectIfRequired: $code")
                eventListFragment = EventListFragment.newInstance(code, name, meetingType)
                setCurrentFragment(eventListFragment, EVENTS)
                val view: View = bottomNavigationView.findViewById(R.id.nav_meetings)
                view.performClick()
            }

            RedirectionConstants.RDRFORMS -> {
                rdmFormFragment = RDMFormFragment.newInstance(code, token, false)
                setCurrentFragment(rdmFormFragment, RDRFORM)
                val view: View = bottomNavigationView.findViewById(R.id.nav_rdm_form)
                view.performClick()
            }

            RedirectionConstants.RDRVIEWFORMS -> {
                rdmFormFragment = RDMFormFragment.newInstance(code, token, true)
                setCurrentFragment(rdmFormFragment, RDRFORM)
                val view: View = bottomNavigationView.findViewById(R.id.nav_rdm_form)
                view.performClick()
            }

            RedirectionConstants.VIEWDETAILS -> {
                setCurrentFragment(RemoteDealerDashboard(code, token), DASHBOARD)
                val view: View = bottomNavigationView.findViewById(R.id.nav_dealer)
                view.performClick()
            }
            REDIRECT_SALES_AND_LEADS -> {
                Log.i(TAG, "redirectIfRequired:REDIRECT_SALES_AND_LEADS RDMSalesAndLeadsFragment")
                leadsClickedItem = "sales"
                //setCurrentFragment(RDMSalesAndLeadsFragment(code, token, "sales"), REDIRECT_SALES_AND_LEADS)
                val view: View = bottomNavigationView.findViewById(R.id.nav_sales_and_leads)
                view.performClick()
            }
            REDIRECT_PROCUREMENT -> {
                val view: View = bottomNavigationView.findViewById(R.id.nav_proc_and_stock)
                view.performClick()
            }

        }
    }

    override fun onBackPressed() {
        val isLastFragment = NavigationUtility.isLastFragment(this)
        if (isLastFragment) {
            loadFragment()
        } else
            super.onBackPressed()
    }


    // don't forget click listener for back button
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun loadFromBackStack(fragment: Fragment, tag: String) = supportFragmentManager.beginTransaction().apply {
        replace(R.id.flFragment, supportFragmentManager.fragments[supportFragmentManager.fragments.size - 1])
        commit()
        currentFragment = tag
    }

    private fun setCurrentFragment(fragment: Fragment, tag: String) =
//            NavigationUtility.addFragment(fragment, this, tag)
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.flFragment, fragment)
                commit()
                findAndRemoveFragmentFromBackStack(tag)
                fragmentStack.add(tag)
                currentFragment = tag

            }

    private fun setCurrentFragmentWithoutTag(fragment: Fragment) =
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.flFragment, fragment)
                commit()
            }


    private fun findAndRemoveFragmentFromBackStack(tag: String) {
        var index: Int? = null
        if (fragmentStack.size == 0) return
        for (i in 0 until fragmentStack.size) {
            if (fragmentStack[i] == tag) {
                index = i
                break
            }
        }
        if (index != null) {
            fragmentStack.removeAt(index)
        }
    }

    private fun loadFragment() {

        //dont know why this is written , we need to to check.
        /* val initialsize = fragmentStack.size-1
         if(initialsize > 1)
         fragmentStack.removeAt(fragmentStack.size-1)*/
        val size = fragmentStack.size
        /*
         this is modified by surya, if we have zero or 1 fragment we should close the dealer specific dashboard since user visited only one page so no need to show this again.
         */
        if (size <= 1) {
            finish()
            return
        }

        when (fragmentStack[size - 2]) {
            DASHBOARD -> {
                loadFromBackStack(RemoteDealerDashboard(code, token), currentFragment)
                val view: View = bottomNavigationView.findViewById(R.id.nav_dealer)
                view.performClick()
                fragmentStack.removeAt(size - 1)
            }
            PROCANDSALES -> {
                val frag = ProcAndSalesFragment()
                frag.code = code
                loadFromBackStack(frag, currentFragment)
                val view: View = bottomNavigationView.findViewById(R.id.nav_proc_and_stock)
                view.performClick()
                fragmentStack.removeAt(size - 1)
            }
            SALESANDLEADS -> {
                Log.i(TAG, "loadFragment:SALESANDLEADS RDMSalesAndLeadsFragment")
                val frag = RDMSalesAndLeadsFragment(code, token, "")
                loadFromBackStack(frag, currentFragment)
                val view: View = bottomNavigationView.findViewById(R.id.nav_sales_and_leads)
                view.performClick()
                fragmentStack.removeAt(size - 1)
            }
            EVENTS -> {
                val frag = EventListFragment()
                frag.code = code
                frag.name = name
                frag.meetingType = meetingType
                loadFromBackStack(frag, currentFragment)
                val view: View = bottomNavigationView.findViewById(R.id.nav_meetings)
                view.performClick()
                fragmentStack.removeAt(size - 1)
            }
            RDRFORM -> {
                // val rdmFormFragment = RDMFormFragment()
                loadFromBackStack(rdmFormFragment, currentFragment)
                val view: View = bottomNavigationView.findViewById(R.id.nav_rdm_form)
                view.performClick()
                fragmentStack.removeAt(size - 1)
            }
        }
    }


    private fun getDummyList(): ArrayList<DealerItem> {
        val item = DealerItem()
        item.city = "Banglore"
        item.name = "Automax"
        val item2 = DealerItem()
        item2.city = "Hosur"
        item2.name = "Auto"
        val list = ArrayList<DealerItem>()
        list.add(item)
        list.add(item2)
        return list
    }

    companion object {
        var openStatus = false
        var currentFragment = ""
        val DASHBOARD = "dashboard"
        val SALESANDLEADS = "salesandleads"
        val PROCANDSALES = "procSales"
        val EVENTS = "events"
        val RDRFORM = "rdrform"
        val MEETING = "Meeting"
        lateinit var tvSavedLbl: TextView
    }

    fun getDealersList(token: String) {
        SpinnerManager.showSpinner(this)
        retroInterface.getFromWeb(RetroBase.URL_END_DASHBOARD_RDRSTATUS + "?asmId=" + CommonMethods.getstringvaluefromkey(this, "user_id") + "&dealerCode=" + code, "Bearer " + token).enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                SpinnerManager.hideSpinner(this@RDMHomeActivity)
                val strRes = Gson().toJson(response.body())
                val modelRes = Gson().fromJson(strRes, RDRStatusResponse::class.java)
                var data = modelRes.data.get(0)
                Log.i(TAG, "onResponse: ${modelRes.data.size}")
                RDRStatus = data.status

                if (RDRStatus.equals("Completed", ignoreCase = true)) {
                    tvRdrHeaderCompletedStatus.text = "RDR 100% completed"
                } else {
                    tvRdrHeaderCompletedStatus.text = "RDR 75% completed"
                }
                rdmFormFragment = if (RDRStatus.equals("Completed", ignoreCase = true)) {
                    RDMFormFragment.newInstance(code, token, true)
                } else {
                    RDMFormFragment.newInstance(code, token, false)
                }
                if (RDRStatus.equals("Completed", ignoreCase = true)) {
                    bottomNavigationView.setMenuResource(R.menu.asm_bottom_navigation_green)

                } else {
                    bottomNavigationView.setMenuResource(R.menu.asm_bottom_navigation)
                }

                redirectIfRequired(redirectionString)

            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(this@RDMHomeActivity)
                t.printStackTrace()
            }

        })
    }
}