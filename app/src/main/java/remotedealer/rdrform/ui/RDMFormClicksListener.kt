package remotedealer.rdrform.ui

/**
 * Created by Shitalkumar on 8/12/20
 */
interface RDMFormClicksListener {
    fun onNextButtonClick(data: String)
    fun onFormFillListener(category: String, percentage : String)
}