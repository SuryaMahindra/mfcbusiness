package remotedealer.rdrform.ui

import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter


/**
 * Created by Shitalkumar on 1/12/20
 */
class RDMFormViewPagerAdapter(@NonNull fragmentActivity: FragmentActivity, val listener: RDMFormClicksListener, private val isViewDetails: Boolean) : FragmentStateAdapter(fragmentActivity) {

    @NonNull
    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> return ProcurementRDMFormFragment.newInstance(isViewDetails, null, listener)
            1 -> return SalesRDMFormFragment.newInstance(isViewDetails, null, listener)
            2 -> return StackRDMFormFragment.newInstance(isViewDetails, null, listener)
        }

        return ProcurementRDMFormFragment.newInstance(isViewDetails, null, listener)
    }

    override fun getItemCount(): Int {
        return 3
    }




}