package remotedealer.rdrform.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.mfcwl.mfc_dealer.R
import sidekicklpr.PreferenceManager

/**
 * Created by Shitalkumar on 7/12/20
 */
abstract class BaseRDMFormFragment : Fragment() {

    var dealerCode: String? = null
    var token: String? = null
    var isViewDetails: Boolean = false
    lateinit var preferenceManager: PreferenceManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preferenceManager = PreferenceManager(requireActivity())
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    fun setEditTextEditable(editText: EditText, isEnabled: Boolean, textView: TextView?) {
        if (isEnabled && !isViewDetails) {
            editText.background = resources.getDrawable(R.drawable.rounded_edit_text_background_selected)
            editText.setTextColor(resources.getColor(R.color.blue))
            textView?.setTextColor(resources.getColor(R.color.edit_text_blue))
        } else {
            editText.isClickable = false
            editText.isFocusable = false
            editText.isCursorVisible = false
        }
    }

    @SuppressLint("SetTextI18n")
    fun setAchievedPercentageColor(textView: TextView, percentage: Double) {
        textView.text = "${kotlin.math.ceil(percentage).toInt()}% Achieved"
        when {
            percentage >= 90.0 -> {
                textView.setTextColor(resources.getColor(R.color.text_view_green))
            }
            percentage in 70.0..89.0 -> {
                textView.setTextColor(resources.getColor(R.color.edit_text_amber))
            }
            percentage < 70.0 -> {
                textView.setTextColor(resources.getColor(R.color.text_view_red))
            }
        }
    }

    companion object {
        const val PROCUREMENT_RDM_INFO = "PROCUREMENT_RDM_INFO"
        const val SALES_RDM_INFO = "SALES_RDM_INFO"
        const val STACK_RDM_INFO = "STACK_RDM_INFO"
        const val STACK_RDM_INFO_TEMP = "STACK_RDM_INFO_TEMP"
        const val ACTION_ITEMS_INFO = "ACTION_ITEMS_INFO"
        const val ACTION_ITEMS_UPDATED_DATA = "ACTION_ITEMS_UPDATED"
        const val REPORT_FOR_INFO = "REPORT_FOR_INFO"
        const val SELECTED_DEALER_CODE = "SELECTED_DEALER_CODE"
        const val INTENT_FILTER_DATACHANGED ="calculate"

        lateinit var rdmFormClicksListener: RDMFormClicksListener
    }

    open fun saveData() {
    }
}