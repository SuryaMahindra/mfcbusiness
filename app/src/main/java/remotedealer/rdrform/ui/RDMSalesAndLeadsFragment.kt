package remotedealer.rdrform.ui

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.HorizontalScrollView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.mfcwl.mfc_dealer.Model.LeadSection.PrivateLeadWhere
import com.mfcwl.mfc_dealer.Model.LeadSection.PrivateLeadWhereIn
import com.mfcwl.mfc_dealer.Model.LeadSection.WebLeadsCustomWhere
import com.mfcwl.mfc_dealer.Model.LeadSection.WebleadsWhereIn
import com.mfcwl.mfc_dealer.R
import com.mfcwl.mfc_dealer.R.id.id
import com.mfcwl.mfc_dealer.R.id.llFilterHot
import com.mfcwl.mfc_dealer.SalesStocks.Instances.SalesCalendarFilterDialog
import com.mfcwl.mfc_dealer.Utility.AppConstants
import com.mfcwl.mfc_dealer.Utility.CommonMethods
import com.mfcwl.mfc_dealer.Utility.SpinnerManager
import kotlinx.android.synthetic.main.rdm_sales_leads.*
import kotlinx.android.synthetic.main.rdm_sales_leads.view.*
import kotlinx.android.synthetic.main.rdm_sales_leads_fragment.view.*
import kotlinx.android.synthetic.main.rdm_walkin_leads.*
import kotlinx.android.synthetic.main.rdm_walkin_leads.view.*
import kotlinx.android.synthetic.main.rdm_web_leads.*
import kotlinx.android.synthetic.main.rdm_web_leads.view.*
import remotedealer.adapter.SalesWarrantyTypeAdapter
import remotedealer.dashboard.model.CountResponse
import remotedealer.dashboard.view_model.CountViewModel
import remotedealer.model.procurement.ProcureItem
import remotedealer.model.procurement.ProcurementRequest
import remotedealer.model.procurement.ProcurementResponse
import remotedealer.model.sales.RDMSalesRequest
import remotedealer.model.token.RDRTokenRequest
import remotedealer.model.token.RDRTokenResponse
import remotedealer.retrofit.RetroBase.Companion.URL_END_GET_TOKEN
import remotedealer.retrofit.RetroBase.Companion.URL_END_SALES_LEADS
import remotedealer.retrofit.RetroBase.Companion.URL_END_SALES_LEADS_WARRANTY_REVENUE
import remotedealer.retrofit.RetroBase.Companion.URL_END_SALES_LEADS_WARRANTY_TYPES
import remotedealer.retrofit.RetroBase.Companion.URL_END_WALK_IN_LEADS
import remotedealer.retrofit.RetroBase.Companion.URL_END_WALK_IN_LEADS_FOLLOW_UP
import remotedealer.retrofit.RetroBase.Companion.URL_END_WALK_IN_LEADS_FOLLOW_UP_TODAY
import remotedealer.retrofit.RetroBase.Companion.URL_END_WEB_LEADS
import remotedealer.retrofit.RetroBase.Companion.URL_END_WEB_LEADS_FOLLOW_UP
import remotedealer.retrofit.RetroBase.Companion.URL_END_WEB_LEADS_FOLLOW_UP_TODAY
import remotedealer.retrofit.RetroBase.Companion.retroInterface
import remotedealer.ui.RDMLeadsListingFragment
import remotedealer.ui.RDMSalesListFragmentNew
import remotedealer.util.NavigationUtility
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.security.Key
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/*
 * Created By Udaya(MFCWL) ->  02-12-2020 17:13
 *
 */
class RDMSalesAndLeadsFragment(var code: String, var token: String, var clickedItem: String) : Fragment() {

    var TAG = javaClass.simpleName

    lateinit var vi: View


    var webLeadsStartDate = "2020-12-10"
    var webLeadsEndDate = "2020-12-10"
    var webLeadsPendingStartDate = ""
    var webLeadsPendingEndDate = ""
    var webLeadsPendingTodayStartDate = ""
    var webLeadsPendingTodayEndDate = ""


    var walkinLeadsStartDate = "2020-12-10"
    var walkinLeadsEndDate = "2020-12-10"
    var walkinLeadsPendingStartDate = ""
    var walkinLeadsPendingEndDate = ""
    var walkinLeadsPendingTodayStartDate = ""
    var walkinLeadsPendingTodayEndDate = ""

    var salesLeads1StartDate = "2020-12-10"
    var salesLeads1EndDate = "2020-12-10"
    var salesLeads2StartDate = ""
    var salesLeads2EndDate = ""
    var salesLeads3StartDate = ""
    var salesLeads3EndDate = ""

    var leadTypeTag1 = "MTD"
    var leadTypeTag2 = "MTD"
    var leadTypeTag3 = "MTD"
    var leadTypeTag4 = "MTD"


    private fun getFirstDateOfCurrentMonth(): Date {
        val cal = Calendar.getInstance()
        cal[Calendar.DAY_OF_MONTH] = cal.getActualMinimum(Calendar.DAY_OF_MONTH)
        return cal.time
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        Log.i(TAG, "onCreateView: ")

//        currentFragment = TAG

        vi = inflater.inflate(R.layout.rdm_sales_leads_fragment, container, false)

        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")

        CommonMethods.setvalueAgainstKey(activity, "dealer_code", code)
        webLeadsStartDate = simpleDateFormat.format(getFirstDateOfCurrentMonth())
        webLeadsEndDate = simpleDateFormat.format(Date())
        webLeadsPendingStartDate = simpleDateFormat.format(getFirstDateOfCurrentMonth())
        webLeadsPendingEndDate = simpleDateFormat.format(Date())
        webLeadsPendingTodayStartDate = CommonMethods.getTodayDate()
        webLeadsPendingTodayEndDate = CommonMethods.getTodayDate()

        walkinLeadsStartDate = simpleDateFormat.format(getFirstDateOfCurrentMonth())
        walkinLeadsEndDate = simpleDateFormat.format(Date())
        walkinLeadsPendingStartDate = simpleDateFormat.format(getFirstDateOfCurrentMonth())
        walkinLeadsPendingEndDate = simpleDateFormat.format(Date())
        walkinLeadsPendingTodayStartDate = CommonMethods.getTodayDate()
        walkinLeadsPendingTodayEndDate = CommonMethods.getTodayDate()

        salesLeads1StartDate = simpleDateFormat.format(getFirstDateOfCurrentMonth())
        salesLeads1EndDate = simpleDateFormat.format(Date())
        salesLeads2StartDate = simpleDateFormat.format(getFirstDateOfCurrentMonth())
        salesLeads2EndDate = simpleDateFormat.format(Date())
        salesLeads3StartDate = simpleDateFormat.format(getFirstDateOfCurrentMonth())
        salesLeads3EndDate = simpleDateFormat.format(Date())


        //callGetToken()

        /*callWebLeads1()
        callWebLeadsFollowUp()
        callWebLeadsFollowUpToday()*/
        initViews()

        if (clickedItem == "sales")
            vi.btnSalesLeads.performClick()
        else if (clickedItem == "walkin")
            vi.btnWalkinLeads.performClick()
        else
            vi.btnWebLeads.performClick()

        return vi
    }

    private fun initViews() {

        clearLeadsFilter()
        vi.btnWebLeads.setOnClickListener {
            vi.web_leads_details.visibility = View.VISIBLE
            vi.walkin_leads_details.visibility = View.GONE
            vi.sales_leads_details.visibility = View.GONE

            vi.leadsHorizontalView.fullScroll(HorizontalScrollView.FOCUS_LEFT)

            vi.btnWebLeads.setTextColor(Color.WHITE)
            vi.btnWebLeads.setBackgroundDrawable(resources.getDrawable(R.drawable.rounded_event_blue_bg))
            vi.btnWalkinLeads.setTextColor(Color.BLACK)
            vi.btnWalkinLeads.setBackgroundDrawable(resources.getDrawable(R.drawable.rounded_bg_for_btn_not_selected))
            vi.btnSalesLeads.setTextColor(Color.BLACK)
            vi.btnSalesLeads.setBackgroundDrawable(resources.getDrawable(R.drawable.rounded_bg_for_btn_not_selected))

            callWebLeads1()
            callWebLeadsFollowUpToday()
            callWebLeadsFollowUp()

        }
        vi.btnWalkinLeads.setOnClickListener {

            vi.web_leads_details.visibility = View.GONE
            vi.walkin_leads_details.visibility = View.VISIBLE
            vi.sales_leads_details.visibility = View.GONE

            vi.btnWebLeads.setTextColor(Color.BLACK)
            vi.btnWebLeads.setBackgroundDrawable(resources.getDrawable(R.drawable.rounded_bg_for_btn_not_selected))
            vi.btnWalkinLeads.setTextColor(Color.WHITE)
            vi.btnWalkinLeads.setBackgroundDrawable(resources.getDrawable(R.drawable.rounded_event_blue_bg))
            vi.btnSalesLeads.setTextColor(Color.BLACK)
            vi.btnSalesLeads.setBackgroundDrawable(resources.getDrawable(R.drawable.rounded_bg_for_btn_not_selected))

            autoSmoothScrollToRight()

            callWalkInLeads()
            callWalkInLeadsFollowUpToday()
            callWalkInLeadsFollowUp()
        }


        vi.btnSalesLeads.setOnClickListener {

            vi.web_leads_details.visibility = View.GONE
            vi.walkin_leads_details.visibility = View.GONE
            vi.sales_leads_details.visibility = View.VISIBLE

            vi.btnWebLeads.setTextColor(Color.BLACK)
            vi.btnWebLeads.setBackgroundDrawable(resources.getDrawable(R.drawable.rounded_bg_for_btn_not_selected))
            vi.btnWalkinLeads.setTextColor(Color.BLACK)
            vi.btnWalkinLeads.setBackgroundDrawable(resources.getDrawable(R.drawable.rounded_bg_for_btn_not_selected))
            vi.btnSalesLeads.setTextColor(Color.WHITE)
            vi.btnSalesLeads.setBackgroundDrawable(resources.getDrawable(R.drawable.rounded_event_blue_bg))

            autoSmoothScrollToRight()

            callSalesLeads1()
            callSalesLeadsWarrantyType()
            callSalesLeadsWarrantyRevenue()

        }

        vi.ivCalendarWebLeads.setOnClickListener {
            val postCalendarDialog = SalesCalendarFilterDialog(
                    activity,
                    "CustomPostDate"
            ) { startDateString, endDateString, dateType ->
                webLeadsStartDate = startDateString
                webLeadsEndDate = endDateString
                leadTypeTag1 = "FILTER"
                CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_FILTER_CATEGORY_VALUE, AppConstants.LEAD_FILTER_CUSTOM_DATE);
                CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_FILTER_CUSTOM_DATE, startDateString + "To" + endDateString)
                callWebLeads1()
            }
            postCalendarDialog.setCancelable(false)
            postCalendarDialog.show()
        }

        /* vi.ivCalendarWebLeadsPendingToday.setOnClickListener {
             val postCalendarDialog = SalesCalendarFilterDialog(
                     activity,
                     "CustomPostDate"
             ) { startDateString, endDateString, dateType ->
                 webLeadsPendingTodayStartDate = startDateString
                 webLeadsPendingTodayEndDate = endDateString
                 leadTypeTag2 = "FILTER"
                 callWebLeadsFollowUpToday()
             }
             postCalendarDialog.setCancelable(false)
             postCalendarDialog.show()
         }
 */  // Commented by Sangeetha ---> for Follow-up pending today in Web Leads and Walking in section always the date should be current date.
        vi.ivCalendarWebLeadsPending.setOnClickListener {
            val postCalendarDialog = SalesCalendarFilterDialog(
                    activity,
                    "CustomPostDate"
            ) { startDateString, endDateString, dateType ->
                webLeadsPendingStartDate = startDateString
                webLeadsPendingEndDate = endDateString
                leadTypeTag2 = "FILTER"
                CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_FILTER_CATEGORY_VALUE, AppConstants.LEAD_FILTER_CUSTOM_DATE);
                CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_FILTER_CUSTOM_DATE, startDateString + "To" + endDateString)
                callWebLeadsFollowUp()
            }
            postCalendarDialog.setCancelable(false)
            postCalendarDialog.show()
        }

        vi.ivWalkInCalendar.setOnClickListener {
            val postCalendarDialog = SalesCalendarFilterDialog(
                    activity,
                    "CustomPostDate"
            ) { startDateString, endDateString, dateType ->
                walkinLeadsStartDate = startDateString
                walkinLeadsEndDate = endDateString
                leadTypeTag3 = "FILTER"
                CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_FILTER_CATEGORY_VALUE, AppConstants.LEAD_FILTER_CUSTOM_DATE);
                CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_FILTER_CUSTOM_DATE, startDateString + "To" + endDateString)
                callWalkInLeads()
            }
            postCalendarDialog.setCancelable(false)
            postCalendarDialog.show()
        }
        /*vi.ivCalendarWalkInPendingToday.setOnClickListener {
            val postCalendarDialog = SalesCalendarFilterDialog(
                    activity,
                    "CustomPostDate"
            ) { startDateString, endDateString, dateType ->
                walkinLeadsPendingTodayStartDate = startDateString
                walkinLeadsPendingTodayEndDate = endDateString
                callWalkInLeadsFollowUpToday()
            }
            postCalendarDialog.setCancelable(false)
            postCalendarDialog.show()
        }*/ // Commented by Sangeetha ---> for Follow-up pending today in Web Leads and Walking in section always the date should be current date.
        vi.ivCalendarWalkinPending.setOnClickListener {
            val postCalendarDialog = SalesCalendarFilterDialog(
                    activity,
                    "CustomPostDate"
            ) { startDateString, endDateString, dateType ->
                walkinLeadsPendingStartDate = startDateString
                walkinLeadsPendingEndDate = endDateString
                leadTypeTag4 = "FILTER"
                CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_FILTER_CATEGORY_VALUE, AppConstants.LEAD_FILTER_CUSTOM_DATE);
                CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_FILTER_CUSTOM_DATE, startDateString + "To" + endDateString)
                callWalkInLeadsFollowUp()
            }
            postCalendarDialog.setCancelable(false)
            postCalendarDialog.show()
        }

        vi.ivCalendar1SalesLeads.setOnClickListener {
            val postCalendarDialog = SalesCalendarFilterDialog(
                    activity,
                    "CustomPostDate"
            ) { startDateString, endDateString, dateType ->
                salesLeads1StartDate = startDateString
                salesLeads1EndDate = endDateString
                callSalesLeads1()
            }
            postCalendarDialog.setCancelable(false)
            postCalendarDialog.show()
        }

        vi.ivCalendar2SalesLeads.setOnClickListener {
            val postCalendarDialog = SalesCalendarFilterDialog(
                    activity,
                    "CustomPostDate"
            ) { startDateString, endDateString, dateType ->
                salesLeads2StartDate = startDateString
                salesLeads2EndDate = endDateString
                callSalesLeadsWarrantyType()
            }
            postCalendarDialog.setCancelable(false)
            postCalendarDialog.show()
        }


        vi.ivCalendar3SalesLeads.setOnClickListener {
            val postCalendarDialog = SalesCalendarFilterDialog(
                    activity,
                    "CustomPostDate"
            ) { startDateString, endDateString, dateType ->
                salesLeads3StartDate = startDateString
                salesLeads3EndDate = endDateString
                callSalesLeadsWarrantyRevenue()
            }
            postCalendarDialog.setCancelable(false)
            postCalendarDialog.show()
        }

        vi.tvTotalWebLeads.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(getOMSPending("MTD"), mutableListOf(), "web_leads_no_closed", mutableListOf(), mutableListOf(), "all", AppConstants.WEB_LEADS)) }
        vi.tvWebWalkinLeads.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(mutableListOf(), mutableListOf(), "web_leads_closed", getWalkInWhereIn(), getWalkInWhere("total_walk_in"), "all", AppConstants.PRIVATE_LEADS)) }
        vi.tvWebFollowUpLeads.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(getFollowUpPending("total_follow_up"), mutableListOf(), "web_leads_closed", mutableListOf(), mutableListOf(), "followup", AppConstants.WEB_LEADS)) }

        vi.tvTotalWalkinLeads.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(getOMSPending("MTD"), mutableListOf(), "web_leads_closed", mutableListOf(), mutableListOf(), "all", AppConstants.WEB_LEADS)) }
        vi.tvWalkIn.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(mutableListOf(), mutableListOf(), "", getWalkInWhereIn(), getWalkInWhere("total_walk_in"), "all", AppConstants.PRIVATE_LEADS)) }
        vi.tvWalkInFollowUpLeads.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(mutableListOf(), mutableListOf(), "", getWalkInWhereIn(), getWalkInWhere("walk_in_follow_up_pending"), "followup", AppConstants.PRIVATE_LEADS)) }

        vi.tvTotalSales.setOnClickListener {
            loadFragment(RDMSalesListFragmentNew(token, code, "total_sales", getWhereList("sold_date"), getWhereInList("bottom_warranty_sales"))) }
        vi.tvTotalWarranty.setOnClickListener {
            loadFragment(RDMSalesListFragmentNew(token, code, "warranty", getWhereList("total_warranty_sales"), getWhereInList("bottom_warranty_sales"))) }
        vi.tvNonWarrantySales.setOnClickListener {
            loadFragment(RDMSalesListFragmentNew(token, code, "non-warranty", getWhereList("non_warranty_sales"), getWhereInList("bottom_warranty_sales"))) }


        //Web

        vi.tvWebLeadsOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(getOMSPending("MTD"), mutableListOf(), "web_leads_no_closed", mutableListOf(), mutableListOf(), "all", AppConstants.WEB_LEADS)) }
        vi.tvWebLeadsFollowUpPendingOMS.setOnClickListener {        clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(getFollowUpPending("follow_up_future"), mutableListOf(), "", mutableListOf(), mutableListOf(), "followup", AppConstants.WEB_LEADS)) }
        vi.tvWebLeadsNewOMS.setOnClickListener {         clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(getNewOrOpen(), getWebleadsWhereIn(), "web_leads_no_closed", mutableListOf(), mutableListOf(), "all", AppConstants.WEB_LEADS)) }
        /* vi.tvWebLeadsRepeatLeadsOMS.setOnClickListener { loadFragment(RDMLeadsListingFragment()) } */ // There is no OnClick event specified for Repeat leads in Prod.doc
     /*   vi.tvWebLeadsQualifiedLeads.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment()) }
     */   vi.tvWebLeadsClosedOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(getClosed(), mutableListOf(), "", mutableListOf(), mutableListOf(), "all", AppConstants.WEB_LEADS)) }

        vi.tvWebLeadsFollowUpPendingFollowUpPendingOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(getFollowUpPending("follow_up"), mutableListOf(), "", mutableListOf(), mutableListOf(), "followup", AppConstants.WEB_LEADS)) }
        vi.tvWebLeadsHotOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(getFollowUpPending("hot"), mutableListOf(), "", mutableListOf(), mutableListOf(), "followup", AppConstants.WEB_LEADS)) }
        vi.tvWebLeadsWarmOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(getFollowUpPending("warm"), mutableListOf(), "", mutableListOf(), mutableListOf(), "followup", AppConstants.WEB_LEADS)) }
        vi.tvWebLeadsWalkInOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(getFollowUpPending("walk_in"), mutableListOf(), "", mutableListOf(), mutableListOf(), "followup", AppConstants.WEB_LEADS)) }
        vi.tvWebLeadsTestDriveOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(getFollowUpPending("test_driven"), mutableListOf(), "", mutableListOf(), mutableListOf(), "followup", AppConstants.WEB_LEADS)) }
        vi.tvWebLeadsColdOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(getFollowUpPending("cold"), mutableListOf(), "", mutableListOf(), mutableListOf(), "followup", AppConstants.WEB_LEADS)) }

        vi.tvWebLeadsFollowUpPendingTodayFollowUpPendingOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(getFollowUpPendingToday(), mutableListOf(), "", mutableListOf(), mutableListOf(), "followup", AppConstants.WEB_LEADS)) }
        vi.tvWebLeadsHotTodayOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(getFollowUpPendingTodayHot(), mutableListOf(), "", mutableListOf(), mutableListOf(), "followup", AppConstants.WEB_LEADS)) }
        vi.tvWebLeadsWarmTodayOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(getFollowUpPendingTodayWarm(), mutableListOf(), "", mutableListOf(), mutableListOf(), "followup", AppConstants.WEB_LEADS)) }
        vi.tvWebLeadsWalkInTodayOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(getFollowUpPendingTodayWalkIN(), mutableListOf(), "", mutableListOf(), mutableListOf(), "followup", AppConstants.WEB_LEADS)) }
        vi.tvWebLeadsTestDriveTodayOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(getFollowUpPendingTodaytestDrive(), mutableListOf(), "", mutableListOf(), mutableListOf(), "followup", AppConstants.WEB_LEADS)) }
        vi.tvWebLeadsColdTodayOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(getFollowUpPendingTodayCold(), mutableListOf(), "", mutableListOf(), mutableListOf(), "followup", AppConstants.WEB_LEADS)) }
        //vi.tvWebLeadsClosedTodayOMS.setOnClickListener { loadFragment(RDMLeadsListingFragment()) }


        //WalkIns

        vi.tvWalkInLeadsOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(mutableListOf(), mutableListOf(), "", getWalkInWhereIn(), getWalkInTargetWhere(), "all", AppConstants.PRIVATE_LEADS)) }

        vi.tvWalkInWalkInOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(mutableListOf(), mutableListOf(), "", getWalkInWhereIn(), getWalkInWhere("oms"), "all", AppConstants.PRIVATE_LEADS)) }

        vi.tvWalkinFollowupPendingTodayOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(mutableListOf(), mutableListOf(), "", getWalkInWhereIn(), getWalkInWhere("walk_in_follow_up_pending_today"), "followup", AppConstants.PRIVATE_LEADS)) }
        vi.tvWalkinHotTodayOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(mutableListOf(), mutableListOf(), "", getWalkInWhereIn(), getWalkInWhere("hot_pending_today"), "followup", AppConstants.PRIVATE_LEADS)) }
        vi.tvWalkInWarmTodayOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(mutableListOf(), mutableListOf(), "", getWalkInWhereIn(), getWalkInWhere("warm_pending_today"), "followup", AppConstants.PRIVATE_LEADS)) }
        vi.tvWalkInWalkInTodayOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(mutableListOf(), mutableListOf(), "", getWalkInWhereIn(), getWalkInWhere("walk_in_pending_today"), "followup", AppConstants.PRIVATE_LEADS)) }
        vi.tvWalkInTestDriveTodayOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(mutableListOf(), mutableListOf(), "", getWalkInWhereIn(), getWalkInWhere("test_drive_pending_today"), "followup", AppConstants.PRIVATE_LEADS)) }
        vi.tvWalkInColdTodayOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(mutableListOf(), mutableListOf(), "", getWalkInWhereIn(), getWalkInWhere("cold_pending_today"), "followup", AppConstants.PRIVATE_LEADS)) }

        vi.tvWalkInFollowUpPendingOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(mutableListOf(), mutableListOf(), "", getWalkInWhereIn(), getWalkInWhere("walk_in_follow_up_pending"), "followup", AppConstants.PRIVATE_LEADS)) }
        vi.tvWalkInHotOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(mutableListOf(), mutableListOf(), "", getWalkInWhereIn(), getWalkInWhere("hot_pending"), "followup", AppConstants.PRIVATE_LEADS)) }
        vi.tvWalkInWarmOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(mutableListOf(), mutableListOf(), "", getWalkInWhereIn(), getWalkInWhere("warm_pending"), "followup", AppConstants.PRIVATE_LEADS)) }
        vi.tvWalkInWalkInOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(mutableListOf(), mutableListOf(), "", getWalkInWhereIn(), getWalkInWhere("walk_in_pending"), "followup", AppConstants.PRIVATE_LEADS)) }
        vi.tvWalkInTestDriveOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(mutableListOf(), mutableListOf(), "", getWalkInWhereIn(), getWalkInWhere("test_drive_pending"), "followup", AppConstants.PRIVATE_LEADS)) }
        vi.tvWalkInColdOMS.setOnClickListener {
            clearLeadsFilter()
            loadFragment(RDMLeadsListingFragment(mutableListOf(), mutableListOf(), "", getWalkInWhereIn(), getWalkInWhere("cold_pending"), "followup", AppConstants.PRIVATE_LEADS)) }


        //Sales
        vi.tvSalesOMS.setOnClickListener { loadFragment(RDMSalesListFragmentNew(token, code, "total_sales", getWhereList("sold_date"), getWhereInList("bottom_warranty_sales"))) }
        vi.tvRetailOMS.setOnClickListener { loadFragment(RDMSalesListFragmentNew(token, code, "total_sales", getWhereList("retail"), getWhereInList("bottom_warranty_sales"))) }
        vi.tvConversionThroughOMS.setOnClickListener { } //no
        vi.tvConversionOutOfCREOMS.setOnClickListener { } //no

        vi.tvWarrantyRevenueOMS.setOnClickListener { } //not required

        val countViewModel = ViewModelProvider.NewInstanceFactory().create(CountViewModel::class.java)
        countViewModel.countData.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            loadCount(it)
        })
    }

    private fun getWalkInTargetWhere(): MutableList<PrivateLeadWhere>? {
        setLeadFilterValues(AppConstants.POSTED_DATE)


        val list = mutableListOf<PrivateLeadWhere>()
        val leadType = PrivateLeadWhere()
        leadType.column = "lead_type"
        leadType.operator = "="
        leadType.value = "sales"

        val leadTag = PrivateLeadWhere()
        leadTag.column = "lead_tags"
        leadTag.operator = "="
        leadTag.value = "usedcar"

        list.add(leadType)
        list.add(leadTag)
        if (leadTypeTag3 == "MTD") {
            val followDateFrom = PrivateLeadWhere()
            followDateFrom.column = "lead_date2"
            followDateFrom.operator = ">="
            followDateFrom.value = CommonMethods.getFirstDay(Date())

            val followDateTo = PrivateLeadWhere()
            followDateTo.column = "lead_date2"
            followDateTo.operator = "<="
            followDateTo.value = CommonMethods.getTodayDate()

            list.add(followDateFrom)
            list.add(followDateTo)

            setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)


        } else if (leadTypeTag3 == "FILTER") {
            val followDateFrom = PrivateLeadWhere()
            followDateFrom.column = "lead_date2"
            followDateFrom.operator = ">="
            followDateFrom.value = walkinLeadsStartDate

            val followDateTo = PrivateLeadWhere()
            followDateTo.column = "lead_date2"
            followDateTo.operator = "<="
            followDateTo.value = walkinLeadsEndDate

            list.add(followDateFrom)
            list.add(followDateTo)

            setLeadFilterValues(AppConstants.LEAD_FILTER_CUSTOM_DATE)


        }


        return list
    }

    private fun getWebleadsWhereIn(): MutableList<WebleadsWhereIn> {
        val list = mutableListOf<WebleadsWhereIn>()
        val whereIn = WebleadsWhereIn()
        whereIn.column = "dispatched.status"
        val valuesList = ArrayList<String>()
        valuesList.add("new")
        valuesList.add("open")
        whereIn.value = valuesList
        list.add(whereIn)
        return list
    }

    private fun getWalkInWhere(tag: String): MutableList<PrivateLeadWhere>? {


        val list = mutableListOf<PrivateLeadWhere>()
        val leadType = PrivateLeadWhere()
        leadType.column = "lead_type"
        leadType.operator = "="
        leadType.value = "sales"

        val leadTag = PrivateLeadWhere()
        leadTag.column = "lead_tags"
        leadTag.operator = "="
        leadTag.value = "usedcar"
        list.add(leadType)
        list.add(leadTag)

        if (tag == "walk_in") {
            if (leadTypeTag3 == "MTD") {

                val followDateFrom = PrivateLeadWhere()
                followDateFrom.column = "lead_date2"
                followDateFrom.operator = ">="
                followDateFrom.value = CommonMethods.getFirstDay(Date())

                val followDateTo = PrivateLeadWhere()
                followDateTo.column = "lead_date2"
                followDateTo.operator = "<="
                followDateTo.value = CommonMethods.getTodayDate()

                list.add(followDateFrom)
                list.add(followDateTo)
                setLeadFilterValues(AppConstants.POSTED_DATE)
                setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)
                setLeadFilterValues("walkin")

            } else if (leadTypeTag3 == "FILTER") {
                val followDateFrom = PrivateLeadWhere()
                followDateFrom.column = "lead_date2"
                followDateFrom.operator = ">="
                followDateFrom.value = walkinLeadsPendingStartDate

                val followDateTo = PrivateLeadWhere()
                followDateTo.column = "lead_date2"
                followDateTo.operator = "<="
                followDateTo.value = walkinLeadsPendingEndDate

                list.add(followDateFrom)
                list.add(followDateTo)
                setLeadFilterValues(AppConstants.POSTED_DATE)

            }


        } else if (tag.equals("total_walk_in")) {
            val followDateFrom = PrivateLeadWhere()
            followDateFrom.column = "lead_date2"
            followDateFrom.operator = ">="
            followDateFrom.value = CommonMethods.getFirstDay(Date())
            list.add(followDateFrom)
            setLeadFilterValues(AppConstants.POSTED_DATE)
            setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)

        } else if (tag.equals("oms")) {

            if (leadTypeTag2 == "MTD") {

                val followDateFrom = PrivateLeadWhere()
                followDateFrom.column = "lead_date2"
                followDateFrom.operator = ">="
                followDateFrom.value = CommonMethods.getTodayDate()

                val followDateTo = PrivateLeadWhere()
                followDateTo.column = "lead_date2"
                followDateTo.operator = "<="
                followDateTo.value = CommonMethods.getTodayDate()
                list.add(followDateFrom)
                list.add(followDateTo)
                setLeadFilterValues(AppConstants.POSTED_DATE)
                setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)


            } else if (leadTypeTag2 == "FILTER") {
                val followDateFrom = PrivateLeadWhere()
                followDateFrom.column = "lead_date2"
                followDateFrom.operator = ">="
                followDateFrom.value = walkinLeadsPendingStartDate

                val followDateTo = PrivateLeadWhere()
                followDateTo.column = "lead_date2"
                followDateTo.operator = "<="
                followDateTo.value = walkinLeadsPendingEndDate

                list.add(followDateFrom)
                list.add(followDateTo)

                setLeadFilterValues(AppConstants.POSTED_DATE)
                setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)

            }
        } else if (tag.equals("walk_in_follow_up_pending_today")) {
            val followDate = PrivateLeadWhere()
            followDate.column = "follow_date"
            followDate.operator = "="
            followDate.value = CommonMethods.getTodayDate()
            list.add(followDate)
            setLeadFilterValues(AppConstants.TODAY)
            setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)


        } else if (tag.equals("walk_in_follow_up_pending")) {


            setLeadFilterValues(AppConstants.FOLLOW_DATE)

            if (leadTypeTag4 == "FILTER") {
                val followDate = PrivateLeadWhere()
                followDate.column = "follow_date"
                followDate.operator = "<"
                followDate.value = walkinLeadsPendingEndDate
                list.add(followDate)
                setLeadFilterValues(AppConstants.LEAD_FILTER_CUSTOM_DATE)
            } else {
                val followDate = PrivateLeadWhere()
                followDate.column = "follow_date"
                followDate.operator = "<"
                followDate.value = CommonMethods.getTodayDate()
                list.add(followDate)
                setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)
            }

        } else if (tag.equals("hot_pending_today")) {

            setLeadFilterValues(AppConstants.TODAY)
            setLeadFilterValues(AppConstants.FOLLOW_DATE)
            setLeadFilterValues("hot")


            val followDate = PrivateLeadWhere()
            followDate.column = "follow_date"
            followDate.operator = "="
            followDate.value = CommonMethods.getTodayDate()

            val leadStatus = PrivateLeadWhere()
            leadStatus.column = "lead_status"
            leadStatus.operator = "="
            leadStatus.value = "hot"
            list.add(followDate)
            list.add(leadStatus)
        } else if (tag.equals("hot_pending")) {

            setLeadFilterValues(AppConstants.FOLLOW_DATE)
            setLeadFilterValues("hot")


            val leadStatus = PrivateLeadWhere()
            leadStatus.column = "lead_status"
            leadStatus.operator = "="
            leadStatus.value = "hot"
            list.add(leadStatus)


            if (leadTypeTag4 == "MTD") {


                val followDate = PrivateLeadWhere()
                followDate.column = "follow_date"
                followDate.operator = "<"
                followDate.value = CommonMethods.getTodayDate()

                list.add(followDate)

                setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)
            } else if (leadTypeTag4 == "FILTER") {

                val followDateTo = PrivateLeadWhere()
                followDateTo.column = "follow_date"
                followDateTo.operator = "<="
                followDateTo.value = walkinLeadsPendingEndDate

                list.add(followDateTo)
                setLeadFilterValues(AppConstants.LEAD_FILTER_CUSTOM_DATE)
            }

        } else if (tag.equals("warm_pending_today")) {

            setLeadFilterValues(AppConstants.TODAY)
            setLeadFilterValues(AppConstants.FOLLOW_DATE)
            setLeadFilterValues("warm")


            val followDate = PrivateLeadWhere()
            followDate.column = "follow_date"
            followDate.operator = "="
            followDate.value = CommonMethods.getTodayDate()

            val leadStatus = PrivateLeadWhere()
            leadStatus.column = "lead_status"
            leadStatus.operator = "="
            leadStatus.value = "warm"
            list.add(followDate)
            list.add(leadStatus)
        } else if (tag.equals("warm_pending")) {

            setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)
            setLeadFilterValues(AppConstants.FOLLOW_DATE)
            setLeadFilterValues("warm")

            val leadStatus = PrivateLeadWhere()
            leadStatus.column = "lead_status"
            leadStatus.operator = "="
            leadStatus.value = "warm"
            list.add(leadStatus)

            if (leadTypeTag4 == "MTD") {

                val followDate = PrivateLeadWhere()
                followDate.column = "follow_date"
                followDate.operator = "<"
                followDate.value = CommonMethods.getTodayDate()

                list.add(followDate)
                setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)
            } else if (leadTypeTag4 == "FILTER") {

                val followDateTo = PrivateLeadWhere()
                followDateTo.column = "follow_date"
                followDateTo.operator = "<"
                followDateTo.value = walkinLeadsPendingEndDate
                list.add(followDateTo)
                setLeadFilterValues(AppConstants.LEAD_FILTER_CUSTOM_DATE)
            }

        } else if (tag.equals("walk_in_pending_today")) {


            setLeadFilterValues(AppConstants.TODAY)
            setLeadFilterValues(AppConstants.FOLLOW_DATE)
            setLeadFilterValues("walkin")

            val followDate = PrivateLeadWhere()
            followDate.column = "follow_date"
            followDate.operator = "="
            followDate.value = CommonMethods.getTodayDate()

            val leadStatus = PrivateLeadWhere()
            leadStatus.column = "lead_status"
            leadStatus.operator = "="
            leadStatus.value = "Walk-In"
            list.add(followDate)
            list.add(leadStatus)
        } else if (tag.equals("walk_in_pending")) {

            setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)
            setLeadFilterValues(AppConstants.FOLLOW_DATE)
            setLeadFilterValues("walkin")

            val leadStatus = PrivateLeadWhere()
            leadStatus.column = "lead_status"
            leadStatus.operator = "="
            leadStatus.value = "Walk-In"
            list.add(leadStatus)

            if (leadTypeTag4 == "MTD") {

                val followDate = PrivateLeadWhere()
                followDate.column = "follow_date"
                followDate.operator = "<"
                followDate.value = CommonMethods.getTodayDate()

                list.add(followDate)
                setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)
            } else if (leadTypeTag4 == "FILTER") {

                val followDateTo = PrivateLeadWhere()
                followDateTo.column = "follow_date"
                followDateTo.operator = "<"
                followDateTo.value = walkinLeadsPendingEndDate

                list.add(followDateTo)
                setLeadFilterValues(AppConstants.LEAD_FILTER_CUSTOM_DATE)
            }


        } else if (tag.equals("test_drive_pending_today")) {


            setLeadFilterValues(AppConstants.TODAY)
            setLeadFilterValues(AppConstants.FOLLOW_DATE)
            setLeadFilterValues("testdrivetaken")

            val followDate = PrivateLeadWhere()
            followDate.column = "follow_date"
            followDate.operator = "="
            followDate.value = CommonMethods.getTodayDate()

            val leadStatus = PrivateLeadWhere()
            leadStatus.column = "lead_status"
            leadStatus.operator = "="
            leadStatus.value = "Test Drive Taken"
            list.add(followDate)
            list.add(leadStatus)
        } else if (tag.equals("test_drive_pending")) {

            setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)
            setLeadFilterValues(AppConstants.FOLLOW_DATE)
            setLeadFilterValues("testdrivetaken")

            val leadStatus = PrivateLeadWhere()
            leadStatus.column = "lead_status"
            leadStatus.operator = "="
            leadStatus.value = "Test Drive Taken"
            list.add(leadStatus)

            if (leadTypeTag4 == "MTD") {

                val followDate = PrivateLeadWhere()
                followDate.column = "follow_date"
                followDate.operator = "<"
                followDate.value = CommonMethods.getTodayDate()
                list.add(followDate)
                setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)
            } else if (leadTypeTag4 == "FILTER") {

                val followDateTo = PrivateLeadWhere()
                followDateTo.column = "follow_date"
                followDateTo.operator = "<"
                followDateTo.value = walkinLeadsPendingEndDate
                list.add(followDateTo)
                setLeadFilterValues(AppConstants.LEAD_FILTER_CUSTOM_DATE)
            }
        } else if (tag.equals("cold_pending_today")) {

            setLeadFilterValues(AppConstants.TODAY)
            setLeadFilterValues(AppConstants.FOLLOW_DATE)
            setLeadFilterValues("cold")

            val followDate = PrivateLeadWhere()
            followDate.column = "follow_date"
            followDate.operator = "="
            followDate.value = CommonMethods.getTodayDate()

            val leadStatus = PrivateLeadWhere()
            leadStatus.column = "lead_status"
            leadStatus.operator = "="
            leadStatus.value = "cold"
            list.add(followDate)
            list.add(leadStatus)
        } else if (tag.equals("cold_pending")) {
            val leadStatus = PrivateLeadWhere()
            leadStatus.column = "lead_status"
            leadStatus.operator = "="
            leadStatus.value = "cold"
            list.add(leadStatus)
            setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)
            setLeadFilterValues(AppConstants.FOLLOW_DATE)
            setLeadFilterValues("testdrivetaken")

            if (leadTypeTag4 == "MTD") {

                val followDate = PrivateLeadWhere()
                followDate.column = "follow_date"
                followDate.operator = "<"
                followDate.value = CommonMethods.getTodayDate()
                list.add(followDate)
                setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)
            } else if (leadTypeTag4 == "FILTER") {

                val followDateTo = PrivateLeadWhere()
                followDateTo.column = "follow_date"
                followDateTo.operator = "<"
                followDateTo.value = walkinLeadsPendingEndDate
                list.add(followDateTo)
                setLeadFilterValues(AppConstants.LEAD_FILTER_CUSTOM_DATE)
            }


        }

        return list
    }

    private fun getWalkInWhereIn(): MutableList<PrivateLeadWhereIn> {
        val list = mutableListOf<PrivateLeadWhereIn>()
        val whereIn = PrivateLeadWhereIn()
        whereIn.column = "dealer_id"
        val valuesList = ArrayList<String>()
        valuesList.add(code)
        whereIn.values = valuesList
        list.add(whereIn)
        return list
    }

    /*private fun getWhereListSales(str: String): ArrayList<RDMSalesRequest.Where> {
        val whereList = ArrayList<RDMSalesRequest.Where>()
        val where = RDMSalesRequest.Where()
        if (str == "hot") {
            where.column = "dispatched.status"
            where.operator = "="
            where.value = str
        } else if (str == "warm") {
            where.column = "dispatched.status"
            where.operator = "="
            where.value = str
        }

        whereList.add(where)

        return whereList
    }
    private fun getWhereInListSales(str: String): ArrayList<RDMSalesRequest.WhereIn> {
        val whereInList = ArrayList<RDMSalesRequest.WhereIn>()
        val whereIn1 = RDMSalesRequest.WhereIn()
        whereIn1.column = "dealer_code"
        whereIn1.values = arrayOf("$code")
        whereInList.add(whereIn1)
        return whereInList
    }*/

    private fun getOMSPending(tag: String): MutableList<WebLeadsCustomWhere> {

        setLeadFilterValues(AppConstants.POSTED_DATE)

        val list = mutableListOf<WebLeadsCustomWhere>()
        val leadType = WebLeadsCustomWhere()
        leadType.column = "leads.lead_type"
        leadType.operator = "="
        leadType.value = "USEDCARSALES"

        val targetId = WebLeadsCustomWhere()
        targetId.column = "dispatched.target_id"
        targetId.operator = "="
        targetId.value = code

        list.add(leadType)
        list.add(targetId)

        if (leadTypeTag1.equals("MTD")) {
            val createdStartDate = WebLeadsCustomWhere()
            createdStartDate.column = "leads.created_at"
            createdStartDate.operator = ">="
            createdStartDate.value = CommonMethods.getFirstDay(Date()) + " 00:00:00"
            list.add(createdStartDate)
            setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)

        } else if (leadTypeTag1.equals("FILTER")) {
            val createdStartDate = WebLeadsCustomWhere()
            createdStartDate.column = "leads.created_at"
            createdStartDate.operator = ">="
            createdStartDate.value = webLeadsStartDate + " 00:00:00"

            val createdToDate = WebLeadsCustomWhere()
            createdToDate.column = "leads.created_at"
            createdToDate.operator = "<="
            createdToDate.value = webLeadsEndDate + " 23:59:59"

            setLeadFilterValues(AppConstants.LEAD_FILTER_CUSTOM_DATE)


            list.add(createdStartDate)
            list.add(createdToDate)

        }

        return list

    }

    private fun getFollowUpPending(tag: String): MutableList<WebLeadsCustomWhere> {

        setLeadFilterValues(AppConstants.FOLLOW_DATE)

        val list = mutableListOf<WebLeadsCustomWhere>()
        val leadType = WebLeadsCustomWhere()
        leadType.column = "leads.lead_type"
        leadType.operator = "="
        leadType.value = "USEDCARSALES"

        val targetId = WebLeadsCustomWhere()
        targetId.column = "dispatched.target_id"
        targetId.operator = "="
        targetId.value = code
        list.add(leadType)
        list.add(targetId)

        if (tag == "follow_up_future") {
            if (leadTypeTag1 == "MTD") {
                val followupDate = WebLeadsCustomWhere()
                followupDate.column = "dispatched.followup_date"
                followupDate.operator = ">="
                followupDate.value = CommonMethods.getTodayDate() + " 00:00:00"
                list.add(followupDate)

                setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)

            } else if (leadTypeTag1 == "FILTER") {

                val followupToDate = WebLeadsCustomWhere()
                followupToDate.column = "dispatched.followup_date"
                followupToDate.operator = ">="
                followupToDate.value = CommonMethods.getTodayDate() + " 00:00:00"
                list.add(followupToDate)
                setLeadFilterValues(AppConstants.LEAD_FILTER_CUSTOM_DATE)

            }

        } else if (tag == "follow_up") {

            if (leadTypeTag2 == "MTD") {
                val followupDate = WebLeadsCustomWhere()
                followupDate.column = "dispatched.followup_date"
                followupDate.operator = "<"
                followupDate.value = CommonMethods.getTodayDate()
                list.add(followupDate)
                setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)

            } else if (leadTypeTag2 == "FILTER") {

                val followupToDate = WebLeadsCustomWhere()
                followupToDate.column = "dispatched.followup_date"
                followupToDate.operator = "<"
                followupToDate.value = webLeadsEndDate
                list.add(followupToDate)

                setLeadFilterValues(AppConstants.LEAD_FILTER_CUSTOM_DATE)

            }

        } else if (tag == "total_follow_up") {

            /* val followupDate1 = WebLeadsCustomWhere()
             followupDate1.column = "dispatched.followup_date"
             followupDate1.operator = ">="
             followupDate1.value = CommonMethods.getFirstDay(Date())
                         list.add(followupDate1)

 */

            val followupDate = WebLeadsCustomWhere()
            followupDate.column = "dispatched.followup_date"
            followupDate.operator = "<="
            followupDate.value = CommonMethods.getTodayDate()
            list.add(followupDate)
            setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)


        } else if (tag == "hot") {
            setLeadFilterValues("hot")

            val status = WebLeadsCustomWhere()
            status.column = "dispatched.status"
            status.operator = "="
            status.value = "hot"

            list.add(status)

            if (leadTypeTag2 == "FILTER") {

                val followupToDate = WebLeadsCustomWhere()
                followupToDate.column = "dispatched.followup_date"
                followupToDate.operator = "<"
                followupToDate.value = webLeadsPendingEndDate

                list.add(followupToDate)
                setLeadFilterValues(AppConstants.LEAD_FILTER_CUSTOM_DATE)

            } else {
                val followupDate = WebLeadsCustomWhere()
                followupDate.column = "dispatched.followup_date"
                followupDate.operator = "<"
                followupDate.value = CommonMethods.getTodayDate()
                list.add(followupDate)
                setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)

            }

        } else if (tag == "cold") {
            setLeadFilterValues("cold")

            val status = WebLeadsCustomWhere()
            status.column = "dispatched.status"
            status.operator = "="
            status.value = "cold"
            list.add(status)

            if (leadTypeTag2 == "FILTER") {

                val followupToDate = WebLeadsCustomWhere()
                followupToDate.column = "dispatched.followup_date"
                followupToDate.operator = "<"
                followupToDate.value = webLeadsPendingEndDate

                list.add(followupToDate)
                setLeadFilterValues(AppConstants.LEAD_FILTER_CUSTOM_DATE)


            } else {
                val followupDate = WebLeadsCustomWhere()
                followupDate.column = "dispatched.followup_date"
                followupDate.operator = "<"
                followupDate.value = CommonMethods.getTodayDate()
                list.add(followupDate)
                setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)

            }

        } else if (tag == "warm") {
            setLeadFilterValues("warm")

            val status = WebLeadsCustomWhere()
            status.column = "dispatched.status"
            status.operator = "="
            status.value = "warm"

            list.add(status)

            if (leadTypeTag2 == "FILTER") {


                val followupToDate = WebLeadsCustomWhere()
                followupToDate.column = "dispatched.followup_date"
                followupToDate.operator = "<"
                followupToDate.value = webLeadsPendingEndDate

                list.add(followupToDate)

                setLeadFilterValues(AppConstants.LEAD_FILTER_CUSTOM_DATE)


            } else {
                val followupDate = WebLeadsCustomWhere()
                followupDate.column = "dispatched.followup_date"
                followupDate.operator = "<"
                followupDate.value = CommonMethods.getTodayDate()
                list.add(followupDate)

                setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)

            }


        } else if (tag == "walk_in") {
            setLeadFilterValues("walkin")

            val status = WebLeadsCustomWhere()
            status.column = "dispatched.status"
            status.operator = "="
            status.value = "Walk-in"

            list.add(status)

            if (leadTypeTag2 == "FILTER") {


                val followupToDate = WebLeadsCustomWhere()
                followupToDate.column = "dispatched.followup_date"
                followupToDate.operator = "<"
                followupToDate.value = webLeadsPendingEndDate

                list.add(followupToDate)

                setLeadFilterValues(AppConstants.LEAD_FILTER_CUSTOM_DATE)


            } else {
                val followupDate = WebLeadsCustomWhere()
                followupDate.column = "dispatched.followup_date"
                followupDate.operator = "<"
                followupDate.value = CommonMethods.getTodayDate()

                list.add(followupDate)

                setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)

            }


        } else if (tag == "test_driven") {
            setLeadFilterValues("testdrivetaken")

            val status = WebLeadsCustomWhere()
            status.column = "dispatched.status"
            status.operator = "="
            status.value = "Test Drive Taken"
            list.add(status)

            if (leadTypeTag2 == "FILTER") {


                val followupToDate = WebLeadsCustomWhere()
                followupToDate.column = "dispatched.followup_date"
                followupToDate.operator = "<"
                followupToDate.value = webLeadsPendingEndDate

                list.add(followupToDate)

                setLeadFilterValues(AppConstants.LEAD_FILTER_CUSTOM_DATE)


            } else {
                val followupDate = WebLeadsCustomWhere()
                followupDate.column = "dispatched.followup_date"
                followupDate.operator = "<"
                followupDate.value = CommonMethods.getTodayDate()
                list.add(followupDate)
                setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)

            }


        }

        return list

    }

    private fun getWalkInFollowUpPending(): MutableList<PrivateLeadWhere> {
        val list = mutableListOf<PrivateLeadWhere>()
        val leadType = PrivateLeadWhere()
        leadType.column = "lead_type"
        leadType.operator = "="
        leadType.value = "sales"

        val leadTag = PrivateLeadWhere()
        leadTag.column = "lead_tags"
        leadTag.operator = "="
        leadTag.value = "usedcar"

        val followDate = PrivateLeadWhere()
        followDate.column = "follow_date"
        followDate.operator = "="
        followDate.value = CommonMethods.getTodayDate()

        list.add(leadType)
        list.add(leadTag)
        list.add(followDate)

        return list

    }

    private fun getNewOrOpen(): MutableList<WebLeadsCustomWhere> {

        setLeadFilterValues(AppConstants.POSTED_DATE)
        setLeadFilterValues("new")
        val list = mutableListOf<WebLeadsCustomWhere>()
        val leadType = WebLeadsCustomWhere()
        leadType.column = "leads.lead_type"
        leadType.operator = "="
        leadType.value = "USEDCARSALES"

        val targetId = WebLeadsCustomWhere()
        targetId.column = "dispatched.target_id"
        targetId.operator = "="
        targetId.value = code
        list.add(leadType)
        list.add(targetId)

        if (leadTypeTag1 == "MTD") {
            val new = WebLeadsCustomWhere()
            new.column = "leads.created_at"
            new.operator = ">="
            new.value = CommonMethods.getFirstDay(Date()) + " 00:00:00"
            list.add(new)

            setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)


        } else if (leadTypeTag1 == "FILTER") {
            val newFrom = WebLeadsCustomWhere()
            newFrom.column = "leads.created_at"
            newFrom.operator = ">="
            newFrom.value = webLeadsStartDate + " 00:00:00"

            val newTo = WebLeadsCustomWhere()
            newTo.column = "leads.created_at"
            newTo.operator = "<="
            newTo.value = webLeadsEndDate + " 23:59:59"

            list.add(newFrom)
            list.add(newTo)

            setLeadFilterValues(AppConstants.LEAD_FILTER_CUSTOM_DATE)

        }

        /*val new = WebLeadsCustomWhere()
        new.column = "dispatched.status"
        new.operator = "="
        new.value = "new"

        val open = WebLeadsCustomWhere()
        open.column = "dispatched.status"
        open.operator = "="
        open.value = "open"*/

        return list


    }

    private fun getClosed(): MutableList<WebLeadsCustomWhere> {
        setLeadFilterValues(AppConstants.POSTED_DATE)
        setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)
        setLeadFilterValues("closed")
        val list = mutableListOf<WebLeadsCustomWhere>()
        val leadType = WebLeadsCustomWhere()
        leadType.column = "leads.lead_type"
        leadType.operator = "="
        leadType.value = "USEDCARSALES"

        val targetId = WebLeadsCustomWhere()
        targetId.column = "dispatched.target_id"
        targetId.operator = "="
        targetId.value = code

        val status = WebLeadsCustomWhere()
        status.column = "dispatched.status"
        status.operator = "="
        status.value = "closed"

        list.add(leadType)
        list.add(status)
        list.add(targetId)

        if (leadTypeTag1 == "MTD") {
            val fromDate = WebLeadsCustomWhere()
            fromDate.column = "leads.created_at"
            fromDate.operator = ">="
            fromDate.value = CommonMethods.getFirstDay(Date()) + " 00:00:00"

            list.add(fromDate)
            setLeadFilterValues(AppConstants.LAST_THIRTY_DAYS)


        } else if (leadTypeTag1 == "FILTER") {
            val fromDate = WebLeadsCustomWhere()
            fromDate.column = "leads.created_at"
            fromDate.operator = ">="
            fromDate.value = webLeadsStartDate + " 00:00:00"

            val toDate = WebLeadsCustomWhere()
            toDate.column = "leads.created_at"
            toDate.operator = "<="
            toDate.value = webLeadsEndDate + " 23:59:59"

            list.add(fromDate)
            list.add(toDate)

            setLeadFilterValues(AppConstants.LEAD_FILTER_CUSTOM_DATE)


        }

        return list
    }

    private fun getFollowUpPendingToday(): MutableList<WebLeadsCustomWhere> {

        setLeadFilterValues(AppConstants.FOLLOW_DATE)
        setLeadFilterValues(AppConstants.TODAY)
        val list = mutableListOf<WebLeadsCustomWhere>()
        val leadType = WebLeadsCustomWhere()
        leadType.column = "leads.lead_type"
        leadType.operator = "="
        leadType.value = "USEDCARSALES"

        val targetId = WebLeadsCustomWhere()
        targetId.column = "dispatched.target_id"
        targetId.operator = "="
        targetId.value = code

        val from = WebLeadsCustomWhere()
        from.column = "dispatched.followup_date"
        from.operator = ">="
        from.value = CommonMethods.getTodayDate() + " 00:00:00"

        val to = WebLeadsCustomWhere()
        to.column = "dispatched.followup_date"
        to.operator = "<="
        to.value = CommonMethods.getTodayDate() + " 23:59:59"

        list.add(leadType)
        list.add(from)
        list.add(to)
        list.add(targetId)
        return list


    }

    private fun getFollowUpPendingTodayHot(): MutableList<WebLeadsCustomWhere> {

        setLeadFilterValues(AppConstants.FOLLOW_DATE)
        setLeadFilterValues(AppConstants.TODAY)
        setLeadFilterValues("hot")

        val list = mutableListOf<WebLeadsCustomWhere>()
        val leadType = WebLeadsCustomWhere()
        leadType.column = "leads.lead_type"
        leadType.operator = "="
        leadType.value = "USEDCARSALES"

        val targetId = WebLeadsCustomWhere()
        targetId.column = "dispatched.target_id"
        targetId.operator = "="
        targetId.value = code


        val hot = WebLeadsCustomWhere()
        hot.column = "dispatched.status"
        hot.operator = "="
        hot.value = "hot"

        list.add(leadType)
        list.add(targetId)
        list.add(hot)

        val from = WebLeadsCustomWhere()
        from.column = "dispatched.followup_date"
        from.operator = ">="
        from.value = CommonMethods.getTodayDate() + " 00:00:00"

        val to = WebLeadsCustomWhere()
        to.column = "dispatched.followup_date"
        to.operator = "<="
        to.value = CommonMethods.getTodayDate() + " 23:59:59"

        list.add(from)
        list.add(to)


        return list

    }

    private fun getFollowUpPendingTodayWarm(): MutableList<WebLeadsCustomWhere> {

        setLeadFilterValues(AppConstants.FOLLOW_DATE)
        setLeadFilterValues(AppConstants.TODAY)
        setLeadFilterValues("warm")


        val list = mutableListOf<WebLeadsCustomWhere>()
        val leadType = WebLeadsCustomWhere()
        leadType.column = "leads.lead_type"
        leadType.operator = "="
        leadType.value = "USEDCARSALES"

        val targetId = WebLeadsCustomWhere()
        targetId.column = "dispatched.target_id"
        targetId.operator = "="
        targetId.value = code


        val warm = WebLeadsCustomWhere()
        warm.column = "dispatched.status"
        warm.operator = "="
        warm.value = "warm"

        list.add(leadType)
        list.add(targetId)
        list.add(warm)


        val from = WebLeadsCustomWhere()
        from.column = "dispatched.followup_date"
        from.operator = ">="
        from.value = CommonMethods.getTodayDate() + " 00:00:00"

        val to = WebLeadsCustomWhere()
        to.column = "dispatched.followup_date"
        to.operator = "<="
        to.value = CommonMethods.getTodayDate() + " 23:59:59"

        list.add(from)
        list.add(to)


        return list

    }

    private fun getFollowUpPendingTodayWalkIN(): MutableList<WebLeadsCustomWhere> {

        setLeadFilterValues(AppConstants.FOLLOW_DATE)
        setLeadFilterValues(AppConstants.TODAY)
        setLeadFilterValues("walkin")

        val list = mutableListOf<WebLeadsCustomWhere>()
        val leadType = WebLeadsCustomWhere()
        leadType.column = "leads.lead_type"
        leadType.operator = "="
        leadType.value = "USEDCARSALES"

        val targetId = WebLeadsCustomWhere()
        targetId.column = "dispatched.target_id"
        targetId.operator = "="
        targetId.value = code

        val walkin = WebLeadsCustomWhere()
        walkin.column = "dispatched.status"
        walkin.operator = "="
        walkin.value = "Walk-in"

        list.add(leadType)
        list.add(targetId)
        list.add(walkin)


        val from = WebLeadsCustomWhere()
        from.column = "dispatched.followup_date"
        from.operator = ">="
        from.value = CommonMethods.getTodayDate() + " 00:00:00"

        val to = WebLeadsCustomWhere()
        to.column = "dispatched.followup_date"
        to.operator = "<="
        to.value = CommonMethods.getTodayDate() + " 23:59:59"

        list.add(from)
        list.add(to)



        return list
    }

    private fun getFollowUpPendingTodaytestDrive(): MutableList<WebLeadsCustomWhere> {
        setLeadFilterValues(AppConstants.FOLLOW_DATE)
        setLeadFilterValues(AppConstants.TODAY)
        setLeadFilterValues("testdrivetaken")


        val list = mutableListOf<WebLeadsCustomWhere>()
        val leadType = WebLeadsCustomWhere()
        leadType.column = "leads.lead_type"
        leadType.operator = "="
        leadType.value = "USEDCARSALES"

        val targetId = WebLeadsCustomWhere()
        targetId.column = "dispatched.target_id"
        targetId.operator = "="
        targetId.value = code

        val walkin = WebLeadsCustomWhere()
        walkin.column = "dispatched.status"
        walkin.operator = "="
        walkin.value = "Test Drive Taken"

        list.add(leadType)
        list.add(targetId)
        list.add(walkin)


        val from = WebLeadsCustomWhere()
        from.column = "dispatched.followup_date"
        from.operator = ">="
        from.value = CommonMethods.getTodayDate() + " 00:00:00"

        val to = WebLeadsCustomWhere()
        to.column = "dispatched.followup_date"
        to.operator = "<="
        to.value = CommonMethods.getTodayDate() + " 23:59:59"

        list.add(from)
        list.add(to)

        return list
    }

    private fun getFollowUpPendingTodayCold(): MutableList<WebLeadsCustomWhere> {
        setLeadFilterValues(AppConstants.FOLLOW_DATE)
        setLeadFilterValues(AppConstants.TODAY)
        setLeadFilterValues("cold")


        val list = mutableListOf<WebLeadsCustomWhere>()
        val leadType = WebLeadsCustomWhere()
        leadType.column = "leads.lead_type"
        leadType.operator = "="
        leadType.value = "USEDCARSALES"

        val targetId = WebLeadsCustomWhere()
        targetId.column = "dispatched.target_id"
        targetId.operator = "="
        targetId.value = code


        val walkin = WebLeadsCustomWhere()
        walkin.column = "dispatched.status"
        walkin.operator = "="
        walkin.value = "cold"

        list.add(leadType)
        list.add(targetId)
        list.add(walkin)


        val from = WebLeadsCustomWhere()
        from.column = "dispatched.followup_date"
        from.operator = ">="
        from.value = CommonMethods.getTodayDate() + " 00:00:00"

        val to = WebLeadsCustomWhere()
        to.column = "dispatched.followup_date"
        to.operator = "<="
        to.value = CommonMethods.getTodayDate() + " 23:59:59"

        list.add(from)
        list.add(to)

        return list
    }


    private fun getWhereList(str: String): ArrayList<RDMSalesRequest.Where> {
        val whereList = ArrayList<RDMSalesRequest.Where>()

        val stockType = RDMSalesRequest.Where()
        stockType.column = "stock_type"
        stockType.operator = "="
        stockType.value = "4W"
        whereList.add(stockType)

        if (str == "retail") {
            val where = RDMSalesRequest.Where()
            where.column = "isOffload"
            where.operator = "="
            where.value = "NO"
            whereList.add(where)

            /*val soldDate = RDMSalesRequest.Where()
            soldDate.column = "sold_date"
            soldDate.operator = ">="
            soldDate.value = CommonMethods.getFirstDay(Date())
            whereList.add(soldDate)*/

            val sold = RDMSalesRequest.Where()
            sold.column = "sold"
            sold.operator = "="
            sold.value = "1"
            whereList.add(sold)

            val where2 = RDMSalesRequest.Where()
            where2.column = "sold_date"
            where2.operator = ">="
            where2.value = salesLeads1StartDate
            whereList.add(where2)

            val where1 = RDMSalesRequest.Where()
            where1.column = "sold_date"
            where1.operator = "<="
            where1.value = salesLeads1EndDate
            whereList.add(where1)

        } else if (str == "sold_date") {

            val sold = RDMSalesRequest.Where()
            sold.column = "sold"
            sold.operator = "="
            sold.value = "1"
            whereList.add(sold)

            val where2 = RDMSalesRequest.Where()
            where2.column = "sold_date"
            where2.operator = ">="
            where2.value = salesLeads1StartDate
            whereList.add(where2)

            val where1 = RDMSalesRequest.Where()
            where1.column = "sold_date"
            where1.operator = "<="
            where1.value = salesLeads1EndDate
            whereList.add(where1)

        } else if (str == "total_warranty_sales") {

            val where = RDMSalesRequest.Where()
            where.column = "sold"
            where.operator = "="
            where.value = "1"
            whereList.add(where)

            val warrantyNumber = RDMSalesRequest.Where()
            warrantyNumber.column = "warranty_number"
            warrantyNumber.operator = "!="
            warrantyNumber.value = null
            whereList.add(warrantyNumber)

            /*val isOffload = RDMSalesRequest.Where()
            isOffload.column = "isOffload"
            isOffload.operator = "="
            isOffload.value = "NO"
            whereList.add(isOffload)*/

            val soldDate = RDMSalesRequest.Where()
            soldDate.column = "warranty_punched_date"
            soldDate.operator = ">="
            soldDate.value = CommonMethods.getFirstDay(Date())
            whereList.add(soldDate)

        } else if (str == "non_warranty_sales") {

            val where = RDMSalesRequest.Where()
            where.column = "sold"
            where.operator = "="
            where.value = "1"
            whereList.add(where)

            val warrantyNumber = RDMSalesRequest.Where()
            warrantyNumber.column = "warranty_number"
            warrantyNumber.operator = "="
            warrantyNumber.value = null
            whereList.add(warrantyNumber)

            /*val isOffload = RDMSalesRequest.Where()
            isOffload.column = "isOffload"
            isOffload.operator = "="
            isOffload.value = "NO"
            whereList.add(isOffload)*/

            val soldDate = RDMSalesRequest.Where()
            soldDate.column = "warranty_punched_date"
            soldDate.operator = ">="
            soldDate.value = CommonMethods.getFirstDay(Date())
            whereList.add(soldDate)

        }
        /*else if (str == "above30") {
            val where = RDMSalesRequest.Where()
            where.column = "stock_age"
            where.operator = ">="
            where.value = "30"
            whereList.add(where)
        } else if (str == "above60") {
            val where = RDMSalesRequest.Where()
            where.column = "stock_age"
            where.operator = ">="
            where.value = "60"
            whereList.add(where)
        } else if (str == "visibleOnMFC") {
            val where = RDMSalesRequest.Where()
            where.column = "photo_count"
            where.operator = ">"
            where.value = "0"
            whereList.add(where)

            val where2 = RDMSalesRequest.Where()
            where2.column = "private_vehicle"
            where2.operator = "="
            where2.value = "true"
            whereList.add(where2)
        }*/

        return whereList
    }


    private fun getWhereInList(str: String): ArrayList<RDMSalesRequest.WhereIn> {
        val whereInList = ArrayList<RDMSalesRequest.WhereIn>()
        val whereIn1 = RDMSalesRequest.WhereIn()
        whereIn1.column = "dealer_code"
        whereIn1.values = arrayOf("$code")
        whereInList.add(whereIn1)

        if (str == "bottom_warranty_sales") { // for bottom fields this field is common
            val whereIn2 = RDMSalesRequest.WhereIn()
            whereIn2.column = "source"
            whereIn2.values = arrayOf("mfc", "p&s")
            whereInList.add(whereIn2)
        }
        /*if (str == "parkAndSell") {
           val whereIn2 = RDMSalesRequest.WhereIn()
           whereIn2.column = "stock_source"
           whereIn2.values = arrayOf("p&s")
           whereInList.add(whereIn2)
       } else if (str == "withoutImage") {
           val whereIn2 = RDMSalesRequest.WhereIn()
           whereIn2.column = "photo_count"
           whereIn2.values = arrayOf("0")
           whereInList.add(whereIn2)
       }*/

        return whereInList
    }

    private fun loadCount(count: CountResponse) {

        vi.tvTotalWebLeads.text = "Total Leads (${count.totalWebLeadsCount})"
        vi.tvWebWalkinLeads.text = "Walk-In (${count.totalPrivateLeadsCount})" // Web Leads Walk-in Count
        vi.tvWebFollowUpLeads.text = "Follow-Up Leads (${count.followUpWebLeadsCount})" // Web

        vi.tvTotalWalkinLeads.text = "Total Leads (${count.totalWebLeadsCount})"
        vi.tvWalkIn.text = "Walk-In  (${count.totalPrivateLeadsCount})"
        vi.tvWalkInFollowUpLeads.text = "Follow-Up Leads  (${count.followUpWalkinLeadsCount})"

        vi.tvTotalSales.text = "Total Sales (${count.totalSalesCount})"
        vi.tvTotalWarranty.text = "Warranty Sales (${count.warrantySalesCount})"
        vi.tvNonWarrantySales.text = "Non-Warranty Sales (${count.nonWarrantySalesCount})"

    }

    private fun callWebLeadsFollowUpToday() {


        val req = ProcurementRequest()
        req.code = code
        req.fromDate = webLeadsPendingTodayStartDate
        req.toDate = webLeadsPendingTodayEndDate

        //SpinnerManager.showSpinner(context)
        retroInterface.getFromWeb(req, URL_END_WEB_LEADS_FOLLOW_UP_TODAY, "Bearer $token").enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                //SpinnerManager.hideSpinner(context)

                try {
                    val strRes = Gson().toJson(response.body())
                    val strModel = Gson().fromJson(strRes, ProcurementResponse::class.java)

                    //Log.i(TAG, "onResponse: " + strModel.data.size)

                    parseWebLeadsFollowUpTodayData(strModel.data)
                } catch (e: Exception) {
                    Log.i(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                //SpinnerManager.hideSpinner(context)
            }
        })


    }


    private fun callGetToken() {

        //SpinnerManager.showSpinner(context)
        retroInterface.getFromWeb(RDRTokenRequest(), URL_END_GET_TOKEN).enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                //SpinnerManager.hideSpinner(context)

                try {
                    val strRes = Gson().toJson(response.body())
                    val resModel = Gson().fromJson(strRes, RDRTokenResponse::class.java)

                    token = resModel.token

                    //Log.i(TAG, "onResponse: " + resModel.token)

                    callWebLeads1()
                    callWebLeadsFollowUp()
                    callWebLeadsFollowUpToday()
                } catch (e: Exception) {
                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                //SpinnerManager.hideSpinner(context)
            }

        })

    }

    fun callWebLeads1() {

        val req = ProcurementRequest()
        req.code = code
        req.fromDate = webLeadsStartDate
        req.toDate = webLeadsEndDate

        //SpinnerManager.showSpinner(context)
        retroInterface.getFromWeb(req, URL_END_WEB_LEADS, "Bearer $token").enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                //SpinnerManager.hideSpinner(context)

                try {
                    val strRes = Gson().toJson(response.body())
                    val strModel = Gson().fromJson(strRes, ProcurementResponse::class.java)

                    //Log.i(TAG, "onResponse: " + strModel.data.size)

                    parseWebLeads1Data(strModel.data)
                } catch (e: Exception) {
                    Log.i(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                //SpinnerManager.hideSpinner(context)
            }
        })

    }

    fun callWebLeadsFollowUp() {

        val req = ProcurementRequest()
        req.code = code
        req.fromDate = webLeadsPendingStartDate
        req.toDate = webLeadsPendingEndDate

        //SpinnerManager.showSpinner(context)
        retroInterface.getFromWeb(req, URL_END_WEB_LEADS_FOLLOW_UP, "Bearer $token").enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                //SpinnerManager.hideSpinner(context)

                try {
                    val strRes = Gson().toJson(response.body())
                    val strModel = Gson().fromJson(strRes, ProcurementResponse::class.java)

                    //Log.i(TAG, "onResponse: " + strModel.data.size)

                    parseWebLeadsFollowUpData(strModel.data)
                } catch (e: Exception) {
                    Log.i(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                //SpinnerManager.hideSpinner(context)
            }
        })

    }

    private fun parseWebLeadsFollowUpData(data: ArrayList<ProcureItem>) {

        try {
            if (data[0].oms == "NA" &&
                    data[0].rdr == "NA" &&
                    data[0].particulars == "NA" &&
                    data[0].target == "NA") {
                return
            }

            tvWebLeadsFollowUpPendingFollowUpPendingOMS.text = data[0].oms
            tvWebLeadsFollowUpPendingFollowUpPendingRDR.text = data[0].rdr

            tvWebLeadsHotOMS.text = data[1].oms
            tvWebLeadsHotRDR.text = data[1].rdr

            tvWebLeadsWarmOMS.text = data[2].oms
            tvWebLeadsWarmRDR.text = data[2].rdr

            tvWebLeadsWalkInOMS.text = data[3].oms
            tvWebLeadsWalkInRDR.text = data[3].rdr

            tvWebLeadsTestDriveOMS.text = data[4].oms
            tvWebLeadsTestDriveRDR.text = data[4].rdr

            tvWebLeadsColdOMS.text = data[5].oms
            tvWebLeadsColdRDR.text = data[5].rdr

            /*tvWebLeadsClosedOMS.text = data[6].oms
            tvWebLeadsClosedRDR.text = data[6].rdr*/


        } catch (e: Exception) {
            Log.i(TAG, "parseWebLeadsData: " + e.message)
        }


    }

    private fun parseWebLeadsFollowUpTodayData(data: ArrayList<ProcureItem>) {

        try {
            if (data[0].oms == "NA" &&
                    data[0].rdr == "NA" &&
                    data[0].particulars == "NA" &&
                    data[0].target == "NA") {
                return
            }

            tvWebLeadsFollowUpPendingTodayFollowUpPendingOMS.text = data[0].oms
            tvWebLeadsFollowUpPendingTodayFollowUpPendingRDR.text = data[0].rdr

            tvWebLeadsHotTodayOMS.text = data[1].oms
            tvWebHotTodayRDR.text = data[1].rdr

            tvWebLeadsWarmTodayOMS.text = data[2].oms
            tvWebLeadsWarmTodayRDR.text = data[2].rdr

            tvWebLeadsWalkInTodayOMS.text = data[3].oms
            tvWebWalkInTodayRDR.text = data[3].rdr

            tvWebLeadsTestDriveTodayOMS.text = data[4].oms
            tvWebLeadsTestDriveTodayRDR.text = data[4].rdr

            tvWebLeadsColdTodayOMS.text = data[5].oms
            tvWebLeadsColdTodayRDR.text = data[5].rdr

            /*tvWebLeadsClosedTodayOMS.text = data[6].oms
            tvWebLeadsClosedTodayRDR.text = data[6].rdr*/


            /*tvFollowUpPendingOMS.text = data[1].oms
            tvFollowUpPendingRDR.text = data[1].rdr


            tvNewOMS.text = data[2].oms
            tvNewRDR.text = data[2].rdr

            tvRepeatLeadsOMS.text = data[3].oms
            tvRepeatLeadsRDR.text = data[3].rdr

            tvQualifiedLeadsOMS.text = data[3].oms
            tvQualifiedLeadsRDR.text = data[3].rdr

            tvWebLeadsClosedOMS.text = data[4].oms
            tvWebLeadsClosedRDR.text = data[4].rdr*/

        } catch (e: Exception) {
            Log.i(TAG, "parseWebLeadsData: " + e.message)
        }

    }

    private fun parseWebLeads1Data(data: ArrayList<ProcureItem>) {

        try {

            if (data[0].oms == "NA" &&
                    data[0].rdr == "NA" &&
                    data[0].particulars == "NA" &&
                    data[0].target == "NA") {
                return
            }

            tvWebLeadsOMS.text = data[0].oms
            tvWebLeadsRDR.text = data[0].rdr

            tvWebLeadsFollowUpPendingOMS.text = data[1].oms
            tvWebLeadsFollowUpPendingRDR.text = data[1].rdr

            tvWebLeadsNewOMS.text = data[2].oms
            tvNewRDR.text = data[2].rdr

            tvWebLeadsRepeatLeadsOMS.text = data[5].oms
            tvRepeatLeadsRDR.text = data[5].rdr

            tvQualifiedLeadsOMS.text = data[3].oms
            tvQualifiedLeadsRDR.text = data[3].rdr

            tvWebLeadsClosedOMS.text = data[4].oms
            tvWebLeadsClosedRDR.text = data[4].rdr

        } catch (e: Exception) {
            Log.i(TAG, "parseWebLeadsData: " + e.message)
        }

    }

    private fun callWalkInLeads() {

        val req = ProcurementRequest()
        req.code = code
        req.fromDate = walkinLeadsStartDate
        req.toDate = walkinLeadsEndDate

        //SpinnerManager.showSpinner(context)
        retroInterface.getFromWeb(req, URL_END_WALK_IN_LEADS, "Bearer $token").enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                //SpinnerManager.hideSpinner(context)

                try {
                    val strRes = Gson().toJson(response.body())
                    val strModel = Gson().fromJson(strRes, ProcurementResponse::class.java)

                    //Log.i(TAG, "onResponse: " + strModel.data.size)

                    parseWalkInLeadsData(strModel.data)
                } catch (e: Exception) {
                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                //SpinnerManager.hideSpinner(context)
            }
        })

    }

    private fun callWalkInLeadsFollowUpToday() {

        val req = ProcurementRequest()
        req.code = code
        req.fromDate = walkinLeadsPendingTodayStartDate
        req.toDate = walkinLeadsPendingTodayEndDate

        //SpinnerManager.showSpinner(context)
        retroInterface.getFromWeb(req, URL_END_WALK_IN_LEADS_FOLLOW_UP_TODAY, "Bearer $token").enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                //SpinnerManager.hideSpinner(context)

                try {
                    val strRes = Gson().toJson(response.body())
                    val strModel = Gson().fromJson(strRes, ProcurementResponse::class.java)

                    //Log.i(TAG, "onResponse: " + strModel.data.size)

                    parseWalkInLeadsFollowUpTodayData(strModel.data)
                } catch (e: Exception) {
                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                //SpinnerManager.hideSpinner(context)
            }
        })

    }

    private fun callWalkInLeadsFollowUp() {

        val req = ProcurementRequest()
        req.code = code
        req.fromDate = walkinLeadsPendingStartDate
        req.toDate = walkinLeadsPendingEndDate

        //SpinnerManager.showSpinner(context)
        retroInterface.getFromWeb(req, URL_END_WALK_IN_LEADS_FOLLOW_UP, "Bearer $token").enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                //SpinnerManager.hideSpinner(context)

                try {
                    val strRes = Gson().toJson(response.body())
                    val strModel = Gson().fromJson(strRes, ProcurementResponse::class.java)

                    //Log.i(TAG, "onResponse: " + strModel.data.size)

                    parseWalkInFollowUpData(strModel.data)
                } catch (e: Exception) {
                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                //SpinnerManager.hideSpinner(context)
            }
        })


    }

    fun parseWalkInFollowUpData(data: ArrayList<ProcureItem>) {

        try {

            if (data[0].oms == "NA" &&
                    data[0].rdr == "NA" &&
                    data[0].particulars == "NA" &&
                    data[0].target == "NA") {
                return
            }

            tvWalkInFollowUpPendingOMS.text = data[0].oms
            tvWalkInFollowUpPendingRDR.text = data[0].rdr

            tvWalkInHotOMS.text = data[1].oms
            tvWalkInHotRDR.text = data[1].rdr

            tvWalkInWarmOMS.text = data[2].oms
            tvWalkInWarmRDR.text = data[2].rdr

            tvWalkInWalkInOMS.text = data[3].oms
            tvWalkInWalkInRDR.text = data[3].rdr

            tvWalkInTestDriveOMS.text = data[4].oms
            tvWalkInTestDriveRDR.text = data[4].rdr

            tvWalkInColdOMS.text = data[5].oms
            tvWalkInColdRDR.text = data[5].rdr

            /*tvWalkInClosedOMS.text = data[5].oms
            tvClosedRDR.text = data[5].rdr*/

        } catch (e: Exception) {
            Log.i(TAG, "parseWalkinLeadsData: ")
        }
    }

    private fun callWalkInLeadsToday() {

        val req = ProcurementRequest()
        req.code = code
        req.fromDate = walkinLeadsStartDate
        req.toDate = walkinLeadsEndDate

        //SpinnerManager.showSpinner(context)
        retroInterface.getFromWeb(req, URL_END_WALK_IN_LEADS, "Bearer $token").enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                //SpinnerManager.hideSpinner(context)

                try {
                    val strRes = Gson().toJson(response.body())
                    val strModel = Gson().fromJson(strRes, ProcurementResponse::class.java)

                    //Log.i(TAG, "onResponse: " + strModel.data.size)

                    parseWalkInLeadsData(strModel.data)
                } catch (e: Exception) {
                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                //SpinnerManager.hideSpinner(context)
            }
        })


    }

    private fun parseWalkInLeadsData(data: ArrayList<ProcureItem>) {

        try {

            if (data[0].oms == "NA" &&
                    data[0].rdr == "NA" &&
                    data[0].particulars == "NA" &&
                    data[0].target == "NA") {
                return
            }

            tvWalkInLeadsOMS.text = data[0].oms
            tvWalkInLeadsRDR.text = data[0].rdr

        } catch (e: Exception) {
            Log.i(TAG, "parseWalkinLeadsData: " + e.message)
        }
    }


    private fun parseWalkInLeadsFollowUpTodayData(data: ArrayList<ProcureItem>) {

        try {

            if (data[0].oms == "NA" &&
                    data[0].rdr == "NA" &&
                    data[0].particulars == "NA" &&
                    data[0].target == "NA") {
                return
            }

            tvWalkinFollowupPendingTodayOMS.text = data[0].oms
            tvFollowupPendingTodayRDR.text = data[0].rdr

            tvWalkinHotTodayOMS.text = data[1].oms
            tvWalkInHotTodayRDR.text = data[1].rdr

            tvWalkInWarmTodayOMS.text = data[2].oms
            tvWalkInWarmTodayRDR.text = data[2].rdr

            tvWalkInWalkInTodayOMS.text = data[3].oms
            tvWalkInWalkInTodayRDR.text = data[3].rdr

            tvWalkInTestDriveTodayOMS.text = data[4].oms
            tvWalkInTestDriveTodayRDR.text = data[4].rdr

            tvWalkInColdTodayOMS.text = data[5].oms
            tvWalkInColdRDRToday.text = data[5].rdr

            /*tvClosedTodayOMS.text = data[6].oms
            tvClosedTodayRDR.text = data[6].rdr*/

        } catch (e: Exception) {
            Log.i(TAG, "parseWalkinLeadsDataToday: " + e.message)
        }
    }


    private fun callSalesLeads1() {

        val req = ProcurementRequest()
        req.code = code
        req.fromDate = salesLeads1StartDate
        req.toDate = salesLeads1EndDate

        //SpinnerManager.showSpinner(context)
        retroInterface.getFromWeb(req, URL_END_SALES_LEADS, "Bearer $token").enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                //SpinnerManager.hideSpinner(context)

                try {
                    val strRes = Gson().toJson(response.body())
                    val strModel = Gson().fromJson(strRes, ProcurementResponse::class.java)

                    Log.i(TAG, "onResponse: " + strModel.data.size)

                    parseSalesLeadsData(strModel.data)
                } catch (e: Exception) {
                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                //SpinnerManager.hideSpinner(context)
            }
        })

    }

    private fun callSalesLeadsWarrantyRevenue() {

        val req = ProcurementRequest()
        req.code = code
        req.fromDate = salesLeads3StartDate
        req.toDate = salesLeads3EndDate

        //SpinnerManager.showSpinner(context)
        retroInterface.getFromWeb(req, URL_END_SALES_LEADS_WARRANTY_REVENUE, "Bearer $token").enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                //SpinnerManager.hideSpinner(context)

                try {
                    val strRes = Gson().toJson(response.body())
                    val strModel = Gson().fromJson(strRes, ProcurementResponse::class.java)

                    Log.i(TAG, "onResponse: " + strModel.data.size)

                    parseSalesLeadsWarrantyRevenueData(strModel.data)
                } catch (e: Exception) {
                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                //SpinnerManager.hideSpinner(context)
            }
        })


    }

    private fun callSalesLeadsWarrantyType() {

        val req = ProcurementRequest()
        req.code = code
        req.fromDate = salesLeads2StartDate
        req.toDate = salesLeads2EndDate

        //SpinnerManager.showSpinner(context)
        retroInterface.getFromWeb(req, URL_END_SALES_LEADS_WARRANTY_TYPES, "Bearer $token").enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                //SpinnerManager.hideSpinner(context)

                try {
                    val strRes = Gson().toJson(response.body())
                    val strModel = Gson().fromJson(strRes, ProcurementResponse::class.java)

                    Log.i(TAG, "onResponse: " + strModel.data.size)

                    parseSalesLeadsWarrantyTypeData(strModel.data)
                } catch (e: Exception) {
                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                //SpinnerManager.hideSpinner(context)
            }
        })

    }

    private fun parseSalesLeadsData(data: ArrayList<ProcureItem>) {

        try {

            if (data[0].oms == "NA" &&
                    data[0].rdr == "NA" &&
                    data[0].particulars == "NA" &&
                    data[0].target == "NA") {
                return
            }

            tvSalesOMS.text = data[0].oms
            tvSalesRDR.text = data[0].rdr

            tvRetailOMS.text = data[1].oms
            tvRetailRDR.text = data[1].rdr

            tvConversionThroughOMS.text = data[2].oms
            tvConversionThroughRDR.text = data[2].rdr

            tvConversionOutOfCREOMS.text = data[3].oms
            tvConversionOutOfCRERDR.text = data[3].rdr

        } catch (e: Exception) {
            Log.i(TAG, "parseSalesLeadsData: " + e.message)
        }

    }

    private fun parseSalesLeadsWarrantyRevenueData(data: ArrayList<ProcureItem>) {

        try {

            if (data[0].oms == "NA" &&
                    data[0].rdr == "NA" &&
                    data[0].particulars == "NA" &&
                    data[0].target == "NA") {
                return
            }

            tvWarrantyRevenueOMS.text = data[0].oms
            tvWarrantyRevenueRDR.text = data[0].rdr


        } catch (e: Exception) {
            Log.i(TAG, "parseSalesLeadsWarrantyRevenueData: " + e.message)
        }

    }


    private fun parseSalesLeadsWarrantyTypeData(data: ArrayList<ProcureItem>) {

        try {
            /*if (data[0].oms == "NA" &&
                    data[0].rdr == "NA" &&
                    data[0].particulars == "NA" &&
                    data[0].target == "NA") {
                return
            }*/

            recyclerViewWarrantyType.layoutManager = LinearLayoutManager(activity)
            val adapter = SalesWarrantyTypeAdapter(requireContext(), requireActivity(), code, token, data)
            adapter.salesWarrantyStartDate = salesLeads2StartDate
            adapter.salesWarrantyEndDate = salesLeads2EndDate
            recyclerViewWarrantyType.adapter = adapter

        } catch (e: Exception) {
            Log.i(TAG, "parseWalkinLeadsDataToday: " + e.message)
        }

    }

    private fun loadFragment(fragment: Fragment) {
        NavigationUtility.addFragment(fragment, activity, fragment.tag)
        RDMHomeActivity.currentFragment = fragment.tag.toString()
    }

    private fun autoSmoothScrollToRight() {
        vi.leadsHorizontalView.postDelayed({
            vi.leadsHorizontalView.smoothScrollBy(700, 0)
        }, 50)
    }

    private fun setLeadFilterValues(tag: String) {
        var hashMap: HashMap<String, Int> = HashMap<String, Int>()
        if (tag == AppConstants.POSTED_DATE) {
            CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_FILTER_CATEGORY, AppConstants.POSTED_DATE)
        } else if (tag == AppConstants.FOLLOW_DATE) {
            CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_FILTER_CATEGORY, AppConstants.FOLLOW_DATE)
        } else if (tag == AppConstants.TODAY) {
            CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_FILTER_CATEGORY_VALUE, AppConstants.TODAY)
        } else if (tag == AppConstants.LEAD_FILTER_CUSTOM_DATE) {
            CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_FILTER_CATEGORY_VALUE, AppConstants.LEAD_FILTER_CUSTOM_DATE)
        } else if (tag == AppConstants.LAST_THIRTY_DAYS) {
            CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_FILTER_CATEGORY_VALUE, AppConstants.LAST_THIRTY_DAYS)
        } else if (tag == "hot") {
            CommonMethods.clearLeadStatusMap(activity);
            hashMap.put(tag, R.id.llFilterHot)
            var leadStatusMapSize = CommonMethods.getstringvaluefromkey(activity, AppConstants.LEAD_STATUS_COUNT)
            CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_STATUS_COUNT, leadStatusMapSize + 1)
            CommonMethods.insertToSP(activity, hashMap)
        } else if (tag == "new" || tag == "open") {
            CommonMethods.clearLeadStatusMap(activity);
            hashMap.put(tag, R.id.llFilterOpenNew)
            var leadStatusMapSize = CommonMethods.getstringvaluefromkey(activity, AppConstants.LEAD_STATUS_COUNT)
            CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_STATUS_COUNT, leadStatusMapSize + 1)
            CommonMethods.insertToSP(activity, hashMap)
        } else if (tag == "closed") {
            CommonMethods.clearLeadStatusMap(activity);
            CommonMethods.clearLeadStatusMap(activity);
            hashMap.put(tag, R.id.llFilterClosed)
            var leadStatusMapSize = CommonMethods.getstringvaluefromkey(activity, AppConstants.LEAD_STATUS_COUNT)
            CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_STATUS_COUNT, leadStatusMapSize + 1)
            CommonMethods.insertToSP(activity, hashMap)
        } else if (tag == "warm") {
            CommonMethods.clearLeadStatusMap(activity);
            hashMap.put(tag, R.id.llFilterWarm)
            var leadStatusMapSize = CommonMethods.getstringvaluefromkey(activity, AppConstants.LEAD_STATUS_COUNT)
            CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_STATUS_COUNT, leadStatusMapSize + 1)
            CommonMethods.insertToSP(activity, hashMap)
        } else if (tag == "walkin") {
            CommonMethods.clearLeadStatusMap(activity);
            hashMap.put(tag, R.id.llFilterWalkIn)
            var leadStatusMapSize = CommonMethods.getstringvaluefromkey(activity, AppConstants.LEAD_STATUS_COUNT)
            CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_STATUS_COUNT, leadStatusMapSize + 1)
            CommonMethods.insertToSP(activity, hashMap)
        } else if (tag == "testdrivetaken") {
            CommonMethods.clearLeadStatusMap(activity);
            hashMap.put(tag, R.id.llFilterTestDriveTaken)
            var leadStatusMapSize = CommonMethods.getstringvaluefromkey(activity, AppConstants.LEAD_STATUS_COUNT)
            CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_STATUS_COUNT, leadStatusMapSize + 1)
            CommonMethods.insertToSP(activity, hashMap)
        } else if (tag == "cold") {
            CommonMethods.clearLeadStatusMap(activity);
            hashMap.put(tag, R.id.llFilterCold)
            var leadStatusMapSize = CommonMethods.getstringvaluefromkey(activity, AppConstants.LEAD_STATUS_COUNT)
            CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_STATUS_COUNT, leadStatusMapSize + 1)
            CommonMethods.insertToSP(activity, hashMap)
        }


    }

    private fun clearLeadsFilter() {
        CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_FILTER_CATEGORY, "")
        CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_FILTER_CATEGORY_VALUE, "");
        if (!CommonMethods.getstringvaluefromkey(activity, AppConstants.LEAD_STATUS_COUNT).isEmpty()) {
            CommonMethods.setvalueAgainstKey(activity, AppConstants.LEAD_STATUS_COUNT, "");
            CommonMethods.clearLeadStatusMap(activity);
        }

    }

    override fun onResume() {
        super.onResume()
        SpinnerManager.hideSpinner(context)
        //clearLeadsFilter()
    }
}