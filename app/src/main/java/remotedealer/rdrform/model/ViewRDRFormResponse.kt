package remotedealer.rdrform.model

import com.google.gson.annotations.SerializedName

class ViewRDRFormResponse {

    @SerializedName("report_for")
    var reportFor: ReportFor? = null

    @SerializedName("procurement")
    var procurementRDMFormInfoModel: ProcurementRDMFormInfoModel? = null

    @SerializedName("stack")
    var stackRDMFormInfoModel: StackRDMFormInfoModel? = null

    @SerializedName("sales")
    var salesRDMFormInfoModel: SalesRDMFormInfoModel? = null

    @SerializedName("action_item")
    var actionItemsList: ArrayList<ActionItemInfoModel>? = null
}