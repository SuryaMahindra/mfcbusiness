package remotedealer.rdrform.model

import com.google.gson.annotations.SerializedName


class SalesRDMFormInfoModel {

    @SerializedName("omsStocks")
    var omsStocks = ""

    @SerializedName("certifiedStocks")
    var certifiedStocks = ""

    @SerializedName("walkins")
    var walkins = ""

    @SerializedName("totalSales")
    var totalSales = ""

    @SerializedName("retailSales")
    var retailSales = ""

    @SerializedName("bookingsInHand")
    var bookingsInHand = ""

    @SerializedName("omsLeads")
    var omsLeads = ""

    @SerializedName("convOms")
    var convOms = ""

    @SerializedName("bookingThruOms")
    var bookingThruOms = ""

    @SerializedName("qualLeadsFromCre")
    var qualLeadsFromCre = ""

    @SerializedName("convLeadsFromCre")
    var convLeadsFromCre = ""

    @SerializedName("totalStockTarget")
    var totalStockTarget = ""

    @SerializedName("totalSalesTarget")
    var totalSalesTarget = ""

    @SerializedName("totalSalesActual")
    var totalSalesActual = "0"

    @SerializedName("certifiedStockTarget")
    var certifiedStockTarget = ""

    @SerializedName("certifiedStockActual")
    var certifiedStockActual = "0"

    @SerializedName("omsContributionTarget")
    var omsContributionTarget = ""

    @SerializedName("omsContributionActual")
    var omsContributionActual = "0"

    @SerializedName("actualStock")
    var actualStock = ""

    @SerializedName("repeatLeads")
    var repeatLeads = ""

    @SerializedName("parkSellStock")
    var parkSellStock = ""

    @SerializedName("stockNumber")
    var stockNumber = ""

    @SerializedName("omsContributionToday")
    var omsContributionToday = ""
}