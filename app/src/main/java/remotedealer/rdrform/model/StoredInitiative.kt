package remotedealer.rdrform.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
class StoredInitiative {

    @SerializedName("category")
    public var category: String = ""

    @SerializedName("actionitem")
    public var actionitem: String = ""

    @SerializedName("remarks")
    public var remarks: String = ""

    @SerializedName("action_id")
    public var actionId: String? = ""
}