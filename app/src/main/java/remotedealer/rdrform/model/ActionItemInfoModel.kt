package remotedealer.rdrform.model

import com.google.gson.annotations.SerializedName


class ActionItemsList {
    var actionItemInfoModels: ArrayList<ActionItemInfoModel> = ArrayList()
}

class ActionItemInfoModel {

    @SerializedName("category")
    var category = ""

    @SerializedName("actionItem")
    var actionItem = ""

    @SerializedName("remarks")
    var remarks = ""

}