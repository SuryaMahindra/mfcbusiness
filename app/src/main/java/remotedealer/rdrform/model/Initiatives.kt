package remotedealer.rdrform.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Shitalkumar on 12/12/20
 */
class Initiative {
    @SerializedName("category")
    val category: String = ""

    @SerializedName("actionitem")
    val actionItem: String = ""

    @SerializedName("remarks")
    val remark: String = ""

    @SerializedName("action_id")
    val actionId: Int = 0
}