package remotedealer.rdrform.model


import com.google.gson.annotations.SerializedName


class RemarksInfoModel {

    @SerializedName("initiatives")
    var initiative : ArrayList<Initiative> = ArrayList()

    @SerializedName("procurement")
    var procurement : ArrayList<String> = ArrayList()

    @SerializedName("sales")
    var sales : ArrayList<String> = ArrayList()

    @SerializedName("stack")
    var stack : ArrayList<String> = ArrayList()

    @SerializedName("storedInitiatives")
    var storedInitiatives : ArrayList<StoredInitiative> = ArrayList()
}
