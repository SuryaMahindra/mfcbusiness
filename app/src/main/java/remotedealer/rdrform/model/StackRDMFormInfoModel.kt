package remotedealer.rdrform.model

import com.google.gson.annotations.SerializedName


class StackRDMFormInfoModel {

    @SerializedName("warrantyUnits")
    var warrantyUnits = ""

    @SerializedName("warrantyValue")
    var warrantyValue = ""

    @SerializedName("financeNo")
    var financeNo = ""

    @SerializedName("aggregatorNo")
    var aggregatorNo = ""

    @SerializedName("financeIntervention")
    var financeIntervention = ""

    @SerializedName("pendingRc90Days")
    var pendingRc90Days = ""

    @SerializedName("miblNo")
    var miblNo = ""

    @SerializedName("royaltyMoneyActual")
    var royaltyMoneyActual = ""

    @SerializedName("financePenetrationTarget")
    var financePenetrationTarget = ""

    @SerializedName("financePenetrationActual")
    var financePenetrationActual = "0"

    @SerializedName("warrantyPenetrationTarget")
    var warrantyPenetrationTarget = ""

    @SerializedName("warrantyPenetrationActual")
    var warrantyPenetrationActual = "0"

    @SerializedName("pendingRcTarget")
    var pendingRcTarget = "0"

    @SerializedName("pendingRcActual")
    var pendingRcActual = "0"

    @SerializedName("royaltyCollected")
    var royaltyCollected = ""

    @SerializedName("royaltyMoneyTarget")
    var royaltyMoneyTarget = ""

    @SerializedName("financePenetrationToday")
    var financePenetrationToday = ""

    @SerializedName("warrantyPenetrationToday")
    var warrantyPenetrationToday = ""
}