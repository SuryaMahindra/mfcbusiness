package remotedealer.rdrform.model

import com.google.gson.annotations.SerializedName


class ReportFor {
    
    @SerializedName("dealerCode")
    var dealerCode = ""

    @SerializedName("visitedById")
    var visitedById : Int = 0

    @SerializedName("videoUrl")
    var videoUrl = ""

    @SerializedName("isOperational")
    var isOperational = ""
}