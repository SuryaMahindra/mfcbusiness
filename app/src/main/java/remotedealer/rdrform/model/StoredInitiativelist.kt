package remotedealer.rdrform.model


import com.google.gson.annotations.SerializedName

class StoredInitiativelist : ArrayList<StoredInitiativelist.StoredInitiativelistItem>(){
    data class StoredInitiativelistItem(
        @SerializedName("action_id")
        var actionId: String,
        @SerializedName("actionitem")
        var actionitem: String,
        @SerializedName("category")
        var category: String,
        @SerializedName("remarks")
        var remarks: String
    )
}