package remotedealer.rdrform.model

import com.google.gson.annotations.SerializedName


class ProcurementRDMFormInfoModel {

    @SerializedName("liveAuctionsIep")
    var liveAuctionsIep = ""

    @SerializedName("liveAuctionsCpt")
    var liveAuctionsCpt = ""

    @SerializedName("vehicleNotPerRequirementIep")
    var vehicleNotPerRequirementIep = ""

    @SerializedName("vehicleNotPerRequirementCpt")
    var vehicleNotPerRequirementCpt = ""

    @SerializedName("auctionsBidIep")
    var auctionsBidIep = ""

    @SerializedName("auctionsBidCpt")
    var auctionsBidCpt = ""

    @SerializedName("auctionsWonIep")
    var auctionsWonIep = ""

    @SerializedName("auctionsWonCpt")
    var auctionsWonCpt = ""

    @SerializedName("negotiationsIep")
    var negotiationsIep = ""

    @SerializedName("negotiationsCpt")
    var negotiationsCpt = ""

    @SerializedName("procuredActualIep")
    var procuredActualIep = ""

    @SerializedName("procuredActualCpt")
    var procuredActualCpt = ""

    @SerializedName("procuredAmSupport")
    var procuredAmSupport = ""

    @SerializedName("totalProcurementTarget")
    var totalProcurementTarget = ""

    @SerializedName("totalProcurementActual")
    var totalProcurementActual = ""

    @SerializedName("iepAndCptProcuredTarget")
    var iepAndCptProcuredTarget = ""

    @SerializedName("iepAndCptProcuredActual")
    var iepAndCptProcuredActual = ""

    @SerializedName("selfProcurement")
    var selfProcurement = ""

    @SerializedName("totalProcurementToday")
    var totalProcurementToday = ""

    @SerializedName("iepAndCptProcuredToday")
    var iepAndCptProcuredToday = ""
}