package remotedealer.rdrform.model

import com.google.gson.annotations.SerializedName

class RDRFormRequest {

    @SerializedName("report_for")
    lateinit var reportFor: ReportFor

    @SerializedName("procurement")
    lateinit var procurementRDMFormInfoModel: ProcurementRDMFormInfoModel

    @SerializedName("stack")
    lateinit var stackRDMFormInfoModel: StackRDMFormInfoModel

    @SerializedName("sales")
    lateinit var salesRDMFormInfoModel: SalesRDMFormInfoModel

    @SerializedName("action_item")
    lateinit var actionItemsList: ArrayList<ActionItemInfoModel>
}