package remotedealer.rdrform.model

import com.google.gson.annotations.SerializedName

class RDRFormResponse {

    @SerializedName("status")
    var status = ""

    @SerializedName("message")
    var message = ""
}