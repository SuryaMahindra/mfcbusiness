package remotedealer.rdrform.model

import com.google.gson.annotations.SerializedName


class ViewRDRFormRequest {
    
    @SerializedName("dealerCode")
    var dealerCode = ""

    @SerializedName("createdOn")
    var createdOn = ""
}