package remotedealer.util

class RedirectionConstants {

    companion object{
        const val DEALER_DASHBOARD = "Dashboard"
        const val DEALER_EVENTS = "events"
        const val RDRFORMS = "rdrforms"
        const val RDRVIEWFORMS = "rdrviewforms"
        const val VIEWDETAILS = "viewdetails"
        const val REDIRECT_KEY = "redirect_key"
        const val REDIRECT_SALES_AND_LEADS = "salesandleads"
        const val REDIRECT_PROCUREMENT = "procurement"
        const val KEY_BOOKSTOCK = "bookstock"
        const val KEY_CERTIFIED = "certified"
    }
}