package remotedealer.util

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.*
import com.mfcwl.mfc_dealer.R


object NavigationUtility {
    fun addFragment(fragment: Fragment, activity: FragmentActivity?, tittle: String?) {
        val transaction: FragmentTransaction? = activity?.supportFragmentManager?.beginTransaction()
        removeFragmentIfAlreadyPresentInBackStack(fragment,activity,tittle)
        transaction?.add(R.id.flFragment, fragment)
        transaction?.addToBackStack(tittle)
        transaction?.commit()
    }

    private fun removeFragmentIfAlreadyPresentInBackStack(fragment: Fragment,activity: FragmentActivity?, tag:String?){
        val fm: FragmentManager? = activity?.supportFragmentManager
        val backStackFragment = fm?.findFragmentByTag(fragment.javaClass.canonicalName)
        if (backStackFragment != null) {
            fm.beginTransaction().remove(backStackFragment).commit()
        }

    }

    fun replaceFragment(fragment: Fragment, activity: FragmentActivity?, tittle: String?) {
        val transaction: FragmentTransaction? = activity?.supportFragmentManager?.beginTransaction()
        transaction?.replace(R.id.flFragment, fragment)
        transaction?.commit()
    }

    fun fragmentLoad(fragment: Fragment, activity: FragmentActivity?, tittle: String?) {
        val fm: FragmentManager? = activity?.supportFragmentManager
        fm?.popBackStack()
        val fragmentTransaction: FragmentTransaction? = fm?.beginTransaction()
        fragmentTransaction?.replace(R.id.flFragment, fragment)?.addToBackStack(tittle)
        fragmentTransaction?.commitAllowingStateLoss()
    }

    fun isLastFragment(activity: FragmentActivity?): Boolean {
        val fragmentManager: FragmentManager? = activity?.supportFragmentManager
        val fragmentCount: Int? = fragmentManager?.backStackEntryCount
        return fragmentCount == 0
    }

    fun removeAllFragments(activity: FragmentActivity?) {
        val fragmentManager: FragmentManager? = activity?.supportFragmentManager
        val fragmentCount: Int? = fragmentManager?.backStackEntryCount
        for (i in 0 until fragmentManager?.backStackEntryCount!!) {
            fragmentManager.popBackStack()
        }
    }


  /*  fun getFragmentCount(activity: Activity): Int {
        val fragmentManager: FragmentManager = (activity as MainActivity).supportFragmentManager
        return fragmentManager.backStackEntryCount
    }*/


    fun showFragment(dialogFragment: DialogFragment, tagFragment: String?, addToBackStack: Boolean, activity: AppCompatActivity) {
        val fm: FragmentManager = activity.supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fm.beginTransaction()
        //        closeOpenDialogs(activity);
        if (dialogFragment == null) return
        if (addToBackStack) fragmentTransaction.addToBackStack(tagFragment)
        fragmentTransaction.commit()
        //        clearBackStack(fm);
        dialogFragment.show(fm, tagFragment)
    }
}