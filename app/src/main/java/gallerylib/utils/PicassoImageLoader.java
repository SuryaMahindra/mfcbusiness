package gallerylib.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;
import android.widget.ImageView;

import com.mfcwl.mfc_dealer.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;

/**
 * Created by veinhorn on 2/4/18.
 */

public class PicassoImageLoader implements MediaLoader {
    private int type, position;
    private String value;
    private Integer thumbnailWidth;
    private Integer thumbnailHeight;
    private Context context;

    public PicassoImageLoader(Context context, int type, String value, int position) {
        this.context = context;
        this.type = type;
        this.value = value;
        this.position = position;
    }

    public PicassoImageLoader(String url, Integer thumbnailWidth, Integer thumbnailHeight) {
        this.value = url;
        this.thumbnailWidth = thumbnailWidth;
        this.thumbnailHeight = thumbnailHeight;
    }

    @Override
    public boolean isImage() {
        return true;
    }

    @Override
    public void loadMedia(Context context, final ImageView imageView, final SuccessCallback callback) {

        if (type == 0) {
            File file = new File(value);
            Picasso.get()
                    .load(file)
//                    .placeholder(R.drawable.no_image)
                    .error(R.drawable.no_image)
                    .into(imageView, new ImageCallback(null));
        } else if (type == 1) {
            Picasso.get()
                    .load(value)
                    .placeholder(R.drawable.no_image)
                    .into(imageView, new ImageCallback(null));

        } else if (type == 2)
            imageView.setImageResource(Integer.parseInt(value));
        else {
            File file = new File(value);
            Picasso.get()
                    .load(file)
                    .placeholder(R.drawable.no_image)
                    .into(imageView, new ImageCallback(null));
        }

    }

    @Override
    public void loadThumbnail(Context context, final ImageView thumbnailView, final SuccessCallback callback) {

        if (type == 0) {
            File file = new File(value);
            Picasso.get()
                    .load(file)
                    .resize(thumbnailWidth == null ? 100 : thumbnailWidth,
                            thumbnailHeight == null ? 100 : thumbnailHeight)
                    .placeholder(R.drawable.no_image)
                    .centerInside()
                    .into(thumbnailView, new ImageCallback(callback));
        } else if (type == 1) {
            Picasso.get()
                    .load(value)
                    .resize(thumbnailWidth == null ? 100 : thumbnailWidth,
                            thumbnailHeight == null ? 100 : thumbnailHeight)
                    .placeholder(R.drawable.no_image)
                    .centerInside()
                    .into(thumbnailView, new ImageCallback(callback));
        } else if (type == 2) {
            Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_circle).copy(Bitmap.Config.ARGB_8888, true);
            Paint paint = new Paint();
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(Color.WHITE);
            paint.setTextSize(40);

            Canvas canvas = new Canvas(bm);
            canvas.drawText(String.valueOf(position + 1), bm.getWidth() / 2 - bm.getWidth() / 10, bm.getHeight() / 2 + bm.getHeight() / 10, paint);
            thumbnailView.setImageBitmap(bm);
        } else
            thumbnailView.setVisibility(View.GONE);

    }

    private static class ImageCallback implements Callback {
        private final SuccessCallback callback;

        public ImageCallback(SuccessCallback callback) {
            this.callback = callback;
        }

        @Override
        public void onSuccess() {
            callback.onSuccess();
        }

        @Override
        public void onError(Exception e) {

        }

    }
}
