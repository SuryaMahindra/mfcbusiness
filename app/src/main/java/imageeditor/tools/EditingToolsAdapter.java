package imageeditor.tools;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.R;

import java.util.ArrayList;
import java.util.List;

public class EditingToolsAdapter extends RecyclerView.Adapter<EditingToolsAdapter.ViewHolder> {

    private List<ToolModel> mToolList = new ArrayList<>();
    private OnItemSelected mOnItemSelected;

    public EditingToolsAdapter(OnItemSelected onItemSelected) {
        mOnItemSelected = onItemSelected;
        // mToolList.add(new ToolModel("Eraser", R.drawable.eraser, ToolType.ERASER));
        mToolList.add(new ToolModel("Filter", R.drawable.ic_edit_filter, ToolType.FILTER));
        mToolList.add(new ToolModel("Adjustment", R.drawable.adjustment_img, ToolType.ADJUSTMENT));
        mToolList.add(new ToolModel("Rotation", R.drawable.rotate_img, ToolType.ROTATION));
        mToolList.add(new ToolModel("Draw", R.drawable.pencil, ToolType.PAINT));
        mToolList.add(new ToolModel("Crop", R.drawable.crop, ToolType.CROP));
        mToolList.add(new ToolModel("Text", R.drawable.text_img, ToolType.TEXT));

        // mToolList.add(new ToolModel("Flip", R.drawable.rotate_img, ToolType.FLIP));
    }

    public interface OnItemSelected {
        void onToolSelected(ToolType toolType);
    }

    class ToolModel {
        private String mToolName;
        private int mToolIcon;
        private ToolType mToolType;

        ToolModel(String toolName, int toolIcon, ToolType toolType) {
            mToolName = toolName;
            mToolIcon = toolIcon;
            mToolType = toolType;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_editing_tools, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            ToolModel item = mToolList.get(position);
            holder.txtTool.setText(item.mToolName);
            holder.imgToolIcon.setImageResource(item.mToolIcon);
        } catch (Exception e) {
            e.printStackTrace();
        } catch (OutOfMemoryError memoryError) {
            System.gc();
        }
    }

    @Override
    public int getItemCount() {
        return mToolList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgToolIcon;
        TextView txtTool;

        ViewHolder(View itemView) {
            super(itemView);
            imgToolIcon = itemView.findViewById(R.id.imgToolIcon);
            txtTool = itemView.findViewById(R.id.txtTool);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemSelected.onToolSelected(mToolList.get(getLayoutPosition()).mToolType);
                }
            });
        }
    }
}
