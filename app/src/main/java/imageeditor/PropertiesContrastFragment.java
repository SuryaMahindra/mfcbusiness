package imageeditor;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.mfcwl.mfc_dealer.R;

public class PropertiesContrastFragment extends BottomSheetDialogFragment implements SeekBar.OnSeekBarChangeListener {

    public PropertiesContrastFragment() {
        // Required empty public constructor
    }
    String TAG = getClass().getSimpleName();
    private Properties mProperties;

    int valSeekbar = 50;

    public interface Properties {

        void onBrightnessChanged(int brightness);

        void onContrastChanged(int contrast);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        return inflater.inflate(R.layout.fragment_contrast_properties_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SeekBar sbBright = view.findViewById(R.id.sbbright);
        sbBright.setOnSeekBarChangeListener(this);
        sbBright.setProgress(50);

        SeekBar sbContrast = view.findViewById(R.id.sbContrast);
        sbContrast.setOnSeekBarChangeListener(this);
        sbContrast.setProgress(50);


    }

    public void setPropertiesChangeListener(Properties properties) {
        mProperties = properties;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        valSeekbar = i;

      /* switch (seekBar.getId()) {
           case R.id.sbbright:
               if (mProperties != null) {
                   mProperties.onBrightnessChanged(i);
               }
               break;

           case R.id.sbContrast:
               if (mProperties != null) {
                   mProperties.onContrastChanged(i);
               }
               break;

       }*/
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        Log.e("Seekbar ", "touch stoped ");

        switch (seekBar.getId()) {
            case R.id.sbbright:
                if (mProperties != null) {
                    mProperties.onBrightnessChanged(valSeekbar);
                }
                break;

            case R.id.sbContrast:
                if (mProperties != null) {
                    mProperties.onContrastChanged(valSeekbar);
                }
                break;

        }
    }
}