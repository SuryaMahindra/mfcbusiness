package imageeditor.base;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.IntentCompat;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Activity.RDR.ScreenDataResponse;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GAConstants;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.DealerReportRequest;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.DealerReportResponse;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.DiscussionPoint;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import rdm.EventEditActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.activity;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_DEALER_CRE;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_PROCUREMENT;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_SALES;
import static com.mfcwl.mfc_dealer.retrofitconfig.RetroBase.URL_END_DISCUSSION_POINT;
import static com.mfcwl.mfc_dealer.retrofitconfig.RetroBase.retroInterface;

/**
 * Created by Surya Reddy on 1/17/2018.
 */

public class BaseActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int READ_WRITE_STORAGE = 52;
    private ProgressDialog mProgressDialog;

    public String TAG = getClass().getSimpleName();


    public static ScreenDataResponse dealerReportResponse;
    public static String dealerName = "", dealerId = "";
    public static ArrayList<DiscussionPoint> discussionPoints = new ArrayList<>();
    public String strActionItem = "", strResponsibility = "", strTargetDate = "";
    private Activitykiller mKiller;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate: ");
        mKiller = new Activitykiller();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(mKiller, intentFilter);
    }

    public String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE | dd MMM, yyyy | HH:mm", Locale.getDefault());
        return dateFormat.format(new Date());
    }

    public boolean requestPermission(String permission) {
        boolean isGranted = ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
        if (!isGranted) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{permission},
                    READ_WRITE_STORAGE);
        }
        return isGranted;
    }

    public void isPermissionGranted(boolean isGranted, String permission) {

    }

    public void makeFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case READ_WRITE_STORAGE:
                isPermissionGranted(grantResults[0] == PackageManager.PERMISSION_GRANTED, permissions[0]);
                break;
        }
    }

    protected void showLoading(@NonNull String message) {

        try {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.setContentView(R.layout.progressdialog);
            mProgressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    protected void showSnackbar(@NonNull String message) {
        View view = findViewById(android.R.id.content);
        if (view != null) {
            Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isNotAllowed() {
        String userType = CommonMethods.getstringvaluefromkey(this, "user_type");
        if (userType.equalsIgnoreCase(USER_TYPE_DEALER_CRE) ||
                userType.equalsIgnoreCase(USER_TYPE_PROCUREMENT) ||
                userType.equalsIgnoreCase(USER_TYPE_SALES)) {
            Toast.makeText(this, R.string.you_are_not_auth, Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    public boolean bookNotAllowed() {
        if (CommonMethods.getstringvaluefromkey(this, "user_type").equalsIgnoreCase(USER_TYPE_DEALER_CRE)) {
            Toast.makeText(this, R.string.you_are_not_auth, Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    public boolean salesLeadAllowed() {
        if (CommonMethods.getstringvaluefromkey(this, "user_type").equalsIgnoreCase(USER_TYPE_SALES)) {
            Toast.makeText(this, R.string.you_are_not_auth, Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    public boolean procurementLeadAllowed() {
        if (CommonMethods.getstringvaluefromkey(this, "user_type").equalsIgnoreCase(USER_TYPE_PROCUREMENT)) {
            Toast.makeText(this, R.string.you_are_not_auth, Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    public boolean followUpNotAllowed() {
        if (CommonMethods.getstringvaluefromkey(this, "user_type").equalsIgnoreCase(USER_TYPE_SALES)) {
            Toast.makeText(this, R.string.you_are_not_auth, Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    public boolean isDealerCreUser() {
        if (CommonMethods.getstringvaluefromkey(this, "user_type").equalsIgnoreCase(USER_TYPE_DEALER_CRE)) {
            Toast.makeText(this, R.string.you_are_not_auth, Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    public boolean isSalesUser() {
        if (CommonMethods.getstringvaluefromkey(this, "user_type").equalsIgnoreCase(USER_TYPE_SALES)) {
            Toast.makeText(this, R.string.you_are_not_auth, Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    public boolean isProcurementUser() {
        if (CommonMethods.getstringvaluefromkey(this, "user_type").equalsIgnoreCase(USER_TYPE_PROCUREMENT)) {
            Toast.makeText(this, R.string.you_are_not_auth, Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    public String getTimeMMddyyy() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
        return sdf.format(new Date());
    }

    //This onclick is common for all classes
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.tvNext:
                Log.i(TAG, "onClick:Next ");
                break;
            case R.id.tvTargetDateCalendar:
                DatePickerDialog.OnDateSetListener dateListener = (view, year, month, dayOfMonth) -> {
                    TextView tvCalendar = findViewById(v.getId());

                    Calendar cal = Calendar.getInstance();
                    cal.set(Calendar.YEAR, year);
                    cal.set(Calendar.MONTH, month);
                    cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
                    tvCalendar.setText(sdf.format(cal.getTime()));
                };
                DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DatePicker,
                        dateListener,
                        Calendar.getInstance().get(Calendar.YEAR),
                        Calendar.getInstance().get(Calendar.MONTH),
                        Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
                break;
           /* case R.id.tvActionItemList:
                if (dealerReportResponse != null)
                    setListDialog((TextView) v, "Action Items", getStringArray(dealerReportResponse.stockAdequacy));
                break;*/
            case R.id.tvResponsibilityList:
                if (dealerReportResponse != null && dealerReportResponse.responsibilities != null) {
                    setDialogMultiSelectionCheckRes(R.id.tvResponsibilityList, "Responsibility", getStringArray(dealerReportResponse.responsibilities));

                } else {
                    Toast.makeText(BaseActivity.this, "No data found", Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }

    public void setListDialog(TextView tv, final String strTitle, final String[] arrVal) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(strTitle);
        alert.setItems(arrVal, (dialog, which) -> tv.setText(arrVal[which]));
        alert.create();
        alert.show();
    }

    public String[] getStringArray(List<String> actionItems) {
        return actionItems.toArray(new String[actionItems.size()]);
    }

    public void submitToWeb(DealerReportRequest request) {
        SpinnerManager.showSpinner(this);
        retroInterface.getFromWeb(request, URL_END_DISCUSSION_POINT).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(@NotNull Call<Object> call, @NotNull Response<Object> response) {
                SpinnerManager.hideSpinner(BaseActivity.this);

                discussionPoints.clear();
                try {
                    String str = new Gson().toJson(response.body());
                    String strReq = new Gson().toJson(request, DealerReportRequest.class);
                    DealerReportResponse dealerReportResponse = new Gson().fromJson(str, DealerReportResponse.class);
                    Application.getInstance().logASMEvent(GAConstants.AM_RDR_ID_END,CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));
                    Log.i(TAG, "onResponse: " + str);
                    Log.i(TAG, "onResponse: " + strReq);
                    if (dealerReportResponse.status.equalsIgnoreCase("success"))
                    {
                          alertMessage(BaseActivity.this,"Data submission success!");
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(@NotNull Call<Object> call, @NotNull Throwable t) {
                SpinnerManager.hideSpinner(BaseActivity.this);
            }
        });

    }

    public void restartApplication(final @NonNull Context context) {

    }


    public  void alertMessage(final Activity activity, final String strMsg) {

        // custom dialog

        try {
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.common_alert);
            //dialog.setTitle("Custom Dialog");
            dialog.setCancelable(false);

            TextView Message, cancel;
            Button Confirm;
            Message = dialog.findViewById(R.id.Message);
            Confirm = dialog.findViewById(R.id.Confirm);

            Message.setText(strMsg);

            Confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(activity instanceof EventEditActivity)
                    {
                        dialog.dismiss();
                        finish();
                    }
                    else {
                        dialog.dismiss();
                        Intent broadcastIntent = new Intent();
                        broadcastIntent.setAction("com.package.ACTION_LOGOUT");
                        sendBroadcast(broadcastIntent);
                        Intent intent = new Intent(BaseActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }


                }
            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public void setDialogMultiSelectionCheck(int tvId, String strTitle, String[] items) {

        TextView tv = findViewById(tvId);
        final LinkedHashMap<String, String> arrVal = getHasMapItems(items);
        arrVal.clear();

        AlertDialog dialog = new AlertDialog.Builder(this, R.style.DatePicker)
                .setTitle(strTitle)
                .setMultiChoiceItems(items, null, (dialog1, indexSelected, isChecked) -> {
                    Log.i(TAG, "setDialogMultiSelectionCheck: " + indexSelected);
                    String key = items[indexSelected];
                    if (isChecked) {
                        if (key.equalsIgnoreCase("No action required")) {

                            for (Map.Entry<String, String> entry : arrVal.entrySet()) {
                                String index = entry.getKey();
                                ((AlertDialog) dialog1).getListView().setItemChecked(Integer.parseInt(index), false);
                            }
                            arrVal.clear();
                            arrVal.put(String.valueOf(indexSelected), key);
                            ((AlertDialog) dialog1).getListView().setItemChecked(indexSelected, true);
                        } else {

                            for (Map.Entry<String, String> entry : arrVal.entrySet()) {
                                String index = entry.getKey();
                                String value = entry.getValue();
                                if (value.equalsIgnoreCase("No action required")) {
                                    arrVal.remove(index);
                                    ((AlertDialog) dialog1).getListView().setItemChecked(Integer.parseInt(index), false);
                                }
                            }
                            arrVal.put(String.valueOf(indexSelected), key);
                            ((AlertDialog) dialog1).getListView().setItemChecked(indexSelected, true);
                        }

                    } /*else {
                        arrVal.put(key, "false");
                        ((AlertDialog) dialog1).getListView().setItemChecked(indexSelected, false);
                    }*/
                }).setPositiveButton("OK", (dialog13, id) -> {
                    tv.setText(retrieveValues(arrVal));
                    dialog13.cancel();
                }).setNegativeButton("Cancel", (dialog12, id) -> {
                    dialog12.cancel();
                }).create();
        dialog.show();
        Window window = dialog.getWindow();
        if (window != null) {
            window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        }
        setDefaultData(tv.getText().toString(), arrVal, dialog);

    }


    public void setDialogMultiSelectionCheckRes(int tvId, String strTitle, String[] items) {

        TextView tv = findViewById(tvId);
        final LinkedHashMap<String, String> arrVal = getHasMapItems(items);

        AlertDialog dialog = new AlertDialog.Builder(this, R.style.DatePicker)
                .setTitle(strTitle)
                .setMultiChoiceItems(items, null, (dialog1, indexSelected, isChecked) -> {
                    Log.i(TAG, "setDialogMultiSelectionCheck: " + indexSelected);
                    String key = items[indexSelected];
                    if (isChecked) {
                        arrVal.put(key, "true");
                        ((AlertDialog) dialog1).getListView().setItemChecked(indexSelected, true);
                    } else {
                        arrVal.put(key, "false");
                        ((AlertDialog) dialog1).getListView().setItemChecked(indexSelected, false);
                    }
                }).setPositiveButton("OK", (dialog13, id) -> {
                    tv.setText(retrieveValuesRes(arrVal));
                    dialog13.cancel();
                }).setNegativeButton("Cancel", (dialog12, id) -> {
                    dialog12.cancel();
                }).create();
        dialog.show();
        Window window = dialog.getWindow();
        if (window != null) {
            window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        }
        setDefaultData(tv.getText().toString(), arrVal, dialog);

    }


    public LinkedHashMap<String, String> getHasMapItems(String[] items) {
        LinkedHashMap<String, String> array = new LinkedHashMap<>();
        for (String str : items)
            array.put(str, "false");

        return array;
    }

    public void setDefaultData(String txt, Map<String, String> arrVal, Dialog dialog) {

        String[] checkedItems = txt.split("/");

        for (String selectedValue : checkedItems) {
            int j = 0;
            for (String key : arrVal.keySet()) {
                if (selectedValue.equals(key)) {
                    arrVal.put(key, "true");
                    ((AlertDialog) dialog).getListView().setItemChecked(j, true);
                    break;
                }
                j++;
            }
        }

    }

    public String retrieveValues(Map<String, String> arrVal) {
        StringBuilder str = new StringBuilder();
        for (Map.Entry<String, String> entry : arrVal.entrySet()) {
            String key = entry.getValue();
            if (str.toString().equals(""))
                str.append(key);
            else
                str.append("|").append(key);

        }
        return str.toString();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mKiller);
    }

    private String retrieveValuesRes(Map<String, String> arrVal) {
        StringBuilder str = new StringBuilder();
        for (String key : arrVal.keySet()) {
            String value = arrVal.get(key);
            if (value.equals("true")) {
                if (str.toString().equals(""))
                    str.append(key);
                else
                    str.append(",").append(key);
            }
        }
        return str.toString();
    }

    public String retrieveValues(List list, String[] arrVal) {
        //Retrieving values from list
        StringBuilder strTemp = new StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            strTemp.append(arrVal[Integer.parseInt(list.get(i).toString())]).append(",");
        }
        try {
            return strTemp.substring(0, strTemp.length() - 1).trim();
        } catch (Exception ex) {
            return strTemp.toString().trim();
        }
    }

    public void removeAnyDuplicates(String subfa) {

        for (int i = 0; i < discussionPoints.size(); i++) {
            DiscussionPoint dp = discussionPoints.get(i);
            String mSubfa = dp.getSubFocusArea();
            if (mSubfa.equalsIgnoreCase(subfa)) {
                discussionPoints.remove(i);
            }
        }

    }

    public class Activitykiller extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    }


}
