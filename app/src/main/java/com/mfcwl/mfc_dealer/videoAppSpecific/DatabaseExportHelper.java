package com.mfcwl.mfc_dealer.videoAppSpecific;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.mfcwl.mfc_dealer.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.DATABASE_NAME;


public final class DatabaseExportHelper {
    private final static String TAG =  DatabaseExportHelper.class.getSimpleName();

    public void exportDatabase(final Context context,
                               final String databaseName) {
        try {
            final File sd = Environment.getExternalStorageDirectory();
            String fileBasePath = String.format("%s/%s", sd.getAbsolutePath(), context.getResources().getString(R.string.app_name));

            File destDirectory = new File(fileBasePath);
            destDirectory.mkdirs();

            final File dataDiroctory = Environment.getDataDirectory();
            if (sd.canWrite()) {
                //final String currentDBPath = String.format("//data//%s//databases//%s", context.getPackageName(), databaseName);
                final String currentDBPath = String.format("//data//%s//databases//%s", context.getPackageName(), databaseName);
                final File currentDB = new File(dataDiroctory, currentDBPath);
                final File backupDB = new File(destDirectory, DATABASE_NAME);
                if (currentDB.exists()) {
                    try (
                            FileChannel src = new FileInputStream(currentDB).getChannel()) {
                        try (FileChannel dst = new FileOutputStream(backupDB).getChannel()) {
                            dst.transferFrom(src, 0, src.size());
                        } catch (Exception e) {
                            Log.e(TAG, "DatabaseExportHelper: Exception " + e.getMessage());
                            throw e;
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "DatabaseExportHelper: Exception " + e.getMessage());
                        throw e;
                    }
                }
                Log.i(TAG, "DB export successful");
            }
        } catch (Exception e) {
            Log.e(TAG, "DatabaseExportHelper: Exception " + e.getMessage());
        }
    }
}