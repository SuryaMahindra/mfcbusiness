package com.mfcwl.mfc_dealer.videoAppSpecific;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.Controller.Application;

import java.util.ArrayList;
import java.util.List;

import lprServices.LPRData;
import sidekicklpr.LPRConstants;
import sidekicklpr.LPRResponse;
import sidekicklpr.StockItem;

import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_dealer_code;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_dealer_name;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_lead_id;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_lead_regno;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_lead_type;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_main_api_synced;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_video_local_path;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_video_server_url;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_video_upload_status;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_LeadID;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.TABLE_NAME_VIDEOS;
import static com.mfcwl.mfc_dealer.videoAppSpecific.DatabaseHelper.DATABASE_HELPER_INSTANCE;

///import static co.in.mfcwl.valuation.autoinspekt.db.AISQLLiteAdapter.TABLE_NAME_Master_Insurer;


/// Date: 17th July 2019 (Author: Prashant)
/// This class was created for refactoring AISQLLiteAdaptor Class
/// which was getting too big
/// That class had a tonne of INSERT, UPDATE, DELETE overand above SELECTS
/// SELECTS were passed around as Cursors...

/// This particular class encapsulates all DML's from AISQLLiteAdaptor
/// This class otherwise won't pass any coding standards.
/// This organization (of moving those DML to this class) is much better than having AISQLLiteAdaptor with 6K lines of code.
/// If you are still interested in cleaning up of this code, please read above comments to understand context in which this class existed.

public final class SqlAdapterForDML {

    private static final AISQLLiteAdapter AISQL_LITE_ADAPTER = AISQLLiteAdapter.getInstance();
    private static String TAG = SqlAdapterForDML.class.getSimpleName();
    private static SQLiteDatabase db = DATABASE_HELPER_INSTANCE.getWritableDatabase();
    private static SqlAdapterForDML INSTANCE = new SqlAdapterForDML();

    private SqlAdapterForDML() {
        Log.e(TAG, "DATABASE_HELPER_INSTANCE " + DATABASE_HELPER_INSTANCE);

    }

    public static SqlAdapterForDML getInstance() {
        Log.e(TAG, "SqlAdapterForDML " + INSTANCE);

        return INSTANCE;
    }


    public void updateVideoUrlIntoDB(String videoUrl, String strLeadId, String leadType, Context context) {

        Log.i(TAG, "updateUrlIntoImagesTable: " + strLeadId + " " + videoUrl + "=" + leadType);

        try {
            ContentValues args = new ContentValues();
            args.put(COLUMN_Key_video_server_url, videoUrl);
            args.put(COLUMN_Key_video_upload_status, "1"); // set status "1" after video url is done

            long id = db.update(TABLE_NAME_VIDEOS,
                    args,
                    String.format("%s='%s' and %s='%s'",
                            COLUMN_Key_lead_id, strLeadId,
                            COLUMN_Key_lead_type, leadType),
                    null);
            Log.i(TAG, "updateUrlIntoImagesTable: update id " + id);

        } catch (Exception ex) {
            Log.e(TAG, "Exception " + ex.getMessage());
        }


        //new

    }


    public void insertVideoData(final VideoMetadata metadata) {
        try {
            final Context context = Application.getInstance().getApplicationContext();
            final ContentValues cv = new ContentValues();
            cv.put(COLUMN_Key_lead_id, metadata.getLead());
            cv.put(COLUMN_Key_video_local_path, metadata.getLocalVideoFileName());
            cv.put(COLUMN_Key_dealer_code, metadata.getDealer_code());
            cv.put(COLUMN_Key_dealer_name, metadata.getDealer_name());
            cv.put(COLUMN_Key_lead_type, metadata.getLeadType());
            cv.put(COLUMN_Key_lead_regno, metadata.getRegno());

            cv.put(COLUMN_Key_video_server_url, "");

            cv.put(COLUMN_Key_video_upload_status, "0");
            cv.put(COLUMN_Key_main_api_synced, "0");

            if (AISQL_LITE_ADAPTER.isAvailableInTableCommon(TABLE_NAME_VIDEOS,
                    COLUMN_Key_lead_id, metadata.getLead())) {
                final String condition = String.format("%s='%s'",
                        COLUMN_Key_lead_id, metadata.getLead());
                final long id = db.update(TABLE_NAME_VIDEOS, cv, condition, null);
                Log.i(TAG, "insertVideoData: Updated " + id + " " + cv.toString());
            } else {
                final long id = db.insert(TABLE_NAME_VIDEOS, null, cv);
                Log.i(TAG, "insertVideoData: Inserted " + id + " " + cv.toString());
            }
        } catch (Exception e) {
            Log.e(TAG, "insertVideoData: " + e.getMessage());
            e.printStackTrace();
        }

        //new
        Log.i(TAG, "onRunJob: " + "jobScheduler working");
        new VideoUploadManager().uploadAllPendingMedia();
    }


    public void updateVideoStatusIntoDB(String strLeadId, String leadType) {

        Log.i(TAG, "updateUrlIntoImagesTable: " + strLeadId);

        try {
            ContentValues args = new ContentValues();
            args.put(COLUMN_Key_main_api_synced, "1"); // set status "1" after video url is done

            long id = db.update(TABLE_NAME_VIDEOS,
                    args,
                    String.format("%s='%s' and %s='%s'",
                            COLUMN_Key_lead_id, strLeadId,
                            COLUMN_Key_lead_type, leadType),
                    null);
            Log.i(TAG, "updateVideoStatusIntoDB: update id " + id);

        } catch (Exception ex) {
            Log.e(TAG, "updateVideoStatusIntoDB Exception " + ex.getMessage());
        }


    }


    public boolean deleteSteps(String strLeadId) {

        Log.e(TAG, COLUMN_LeadID + "='" + strLeadId + "' and ");

        String condition = String.format("%s='%s'",
                COLUMN_LeadID, strLeadId);
        Log.i(TAG, "deleteAISteps: condition " + condition);
        // return db.delete(TABLE_NAME_AI_Steps, COLUMN_LeadID + "='" + strLeadId + "' and " + COLUMN_Method + "='" + strMethod + "'", null) > 0;

        long id = db.delete(TABLE_NAME_VIDEOS, condition, null);

        Log.i(TAG, "deleteSteps: " + id);

        return id > 0;
    }


    public boolean insertLPRItem(LPRResponse response) {
        try {
            ContentValues lpr = new ContentValues();
            lpr.put(LPRConstants.EVENT_ID, response.getEventID());
            lpr.put(LPRConstants.DATE_TIME, response.getDateTime());
            lpr.put(LPRConstants.PLATE_IMAGE_PATH, response.getPlateImage());
            lpr.put(LPRConstants.CAR_IMAGE_PATH, response.getCarImage());
            lpr.put(LPRConstants.LATITUDE, response.getLatitude());
            lpr.put(LPRConstants.LONGITUDE, response.getLongitude());
            lpr.put(LPRConstants.SELECTED_DATA, response.getSelectedData());
            lpr.put(LPRConstants.OUTPUT_VALUE, response.getOutputValue());
            db.insertOrThrow(LPRConstants.TABLE_LPR_LIST, null, lpr);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public int getLPRListCount() {
        int count;
        String query = "SELECT * FROM " + LPRConstants.TABLE_LPR_LIST;
        Cursor cursor = db.rawQuery(query, null);
        count = cursor.getCount();
        cursor.close();
        return count;
    }

    public void deleteLPRData(){
        try {
            db.execSQL("delete from "+ LPRConstants.TABLE_LPR_LIST);
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    public List<StockItem> getLPRList() {
        List<StockItem> list = new ArrayList<>();
        try {

            String query = "SELECT * FROM " + LPRConstants.TABLE_LPR_LIST+ " WHERE " + LPRConstants.IS_SYNCED + " = '" + 0 + "'";
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    StockItem item = new StockItem();
                    item.setStockId(cursor.getInt(cursor.getColumnIndex(LPRConstants.LPR_ITEM_ID)));
                    item.setCarImagePath(cursor.getString(cursor.getColumnIndex(LPRConstants.CAR_IMAGE_PATH)));
                    item.setCarplateimage(cursor.getString(cursor.getColumnIndex(LPRConstants.PLATE_IMAGE_PATH)));
                    item.setStockDataType(cursor.getString(cursor.getColumnIndex(LPRConstants.SELECTED_DATA)));
                    item.setStockValue(cursor.getString(cursor.getColumnIndex(LPRConstants.OUTPUT_VALUE)));
                    item.setCarImageUrl(cursor.getString(cursor.getColumnIndex(LPRConstants.CAR_IMAGE_URL)));
                    item.setIsSync(cursor.getInt(cursor.getColumnIndex(LPRConstants.IS_SYNCED)));
                    list.add(item);
                    Log.e("item", new Gson().toJson(item));
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<LPRData> getLPRRequestData() {
        List<LPRData> list = new ArrayList<>();
        Cursor cursor = null;
        try {

            String query = "SELECT * FROM " + LPRConstants.TABLE_LPR_LIST+ " WHERE " + LPRConstants.IS_SYNCED + " = '" + 1 + "'";
             cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    LPRData item = new LPRData();
                    item.setEventID(cursor.getString(cursor.getColumnIndex(LPRConstants.EVENT_ID)));
                    item.setDateTime(cursor.getString(cursor.getColumnIndex(LPRConstants.DATE_TIME)));
                    item.setPlateImage(cursor.getString(cursor.getColumnIndex(LPRConstants.PLATE_IMAGE_PATH)));
                    item.setCarImage(cursor.getString(cursor.getColumnIndex(LPRConstants.CAR_IMAGE_PATH)));
                    item.setLatitude(cursor.getString(cursor.getColumnIndex(LPRConstants.LATITUDE)));
                    item.setLongitude(cursor.getString(cursor.getColumnIndex(LPRConstants.LONGITUDE)));
                    item.setSelectedData(cursor.getString(cursor.getColumnIndex(LPRConstants.SELECTED_DATA)));
                    item.setOutputValue(cursor.getString(cursor.getColumnIndex(LPRConstants.OUTPUT_VALUE)));
                    list.add(item);
                    Log.e("Data", new Gson().toJson(item));
                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            cursor.close();
        }
        return list;
    }



    public void deleteLPRItem(int lprItemId, String value) {
        try {
            db.delete(LPRConstants.TABLE_LPR_LIST, LPRConstants.LPR_ITEM_ID + "=? ", new String[]{String.valueOf(lprItemId)});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void updateLPRItemImage(int lprItemId, String value, String filePath) {
        try {
            ContentValues values = new ContentValues();
            values.put(LPRConstants.CAR_IMAGE_PATH, filePath);
            values.put(LPRConstants.CAR_IMAGE_URL, "");
            values.put(LPRConstants.IS_SYNCED, 0);
            db.update(LPRConstants.TABLE_LPR_LIST, values, LPRConstants.LPR_ITEM_ID + "=? ", new String[]{String.valueOf(lprItemId)});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isLPRItemPresent(int lprId, String selectedData, String outputValue) {
        int count;
        String query = "SELECT * FROM " + LPRConstants.TABLE_LPR_LIST + " WHERE " + LPRConstants.LPR_ITEM_ID + " != '" + lprId + "'" +
                " AND " + LPRConstants.SELECTED_DATA + " = '" + selectedData + "'" +
                " AND " + LPRConstants.OUTPUT_VALUE + " = '" + outputValue + "'";
        Cursor cursor = db.rawQuery(query, null);
        count = cursor.getCount();
        cursor.close();
        return count != 0;
    }

    public boolean updateLPRItemData(int lprItemId, String selectedData, String outputValue) {
        try {
            ContentValues values = new ContentValues();
            values.put(LPRConstants.SELECTED_DATA, selectedData);
            values.put(LPRConstants.OUTPUT_VALUE, outputValue);
            db.update(LPRConstants.TABLE_LPR_LIST, values, LPRConstants.LPR_ITEM_ID + "=? ", new String[]{String.valueOf(lprItemId)});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void updateLPRItemStatus(int lprItemId, String plateurl,String carImageUrl) {
        try {
            ContentValues values = new ContentValues();
            values.put(LPRConstants.PLATE_IMAGE_PATH, plateurl);
            values.put(LPRConstants.CAR_IMAGE_PATH, carImageUrl);
            values.put(LPRConstants.IS_SYNCED, 1);
            db.update(LPRConstants.TABLE_LPR_LIST, values, LPRConstants.LPR_ITEM_ID + "=? ", new String[]{String.valueOf(lprItemId)});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
