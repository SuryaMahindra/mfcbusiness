package com.mfcwl.mfc_dealer.videoAppSpecific;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.mfcwl.mfc_dealer.Controller.Application;

import sidekicklpr.LPRConstants;

import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_dealer_code;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_dealer_name;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_lead_id;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_lead_regno;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_lead_type;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_main_api_synced;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_video_local_path;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_video_server_url;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_video_upload_status;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.DATABASE_NAME;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.DATABASE_VERSION;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.TABLE_NAME_VIDEOS;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = DatabaseHelper.class.getSimpleName();


    //We have used all IMAGE TABLE PARAMETERS for VIDEO TABLE also.
    private static final String DATABASE_CREATE_VIDEOS =
            String.format("CREATE TABLE %s(%s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT);",
                    TABLE_NAME_VIDEOS,
                    COLUMN_Key_lead_id,
                    COLUMN_Key_dealer_name,
                    COLUMN_Key_dealer_code,
                    COLUMN_Key_lead_type,
                    COLUMN_Key_lead_regno,
                    COLUMN_Key_video_local_path,
                    COLUMN_Key_video_server_url,
                    COLUMN_Key_video_upload_status,
                    COLUMN_Key_main_api_synced
                   );


    private DatabaseHelper() {
        super(Application.getInstance().getApplicationContext(), DATABASE_NAME, null, DATABASE_VERSION);
        Log.i(TAG, "DatabaseHelper: ");

    }

    public final static DatabaseHelper DATABASE_HELPER_INSTANCE  = new DatabaseHelper();

    @Override
    public void onCreate(final SQLiteDatabase db) {
        Log.i(TAG, "onCreate: ");
        try {

            db.execSQL(DATABASE_CREATE_VIDEOS);
            db.execSQL(LPRConstants.CREATE_TABLE_LPR_LIST);


        } catch (SQLException e) {
            Log.e(TAG, e.getMessage());
        } finally {

        }
    }

    private void upgradeFromPreviousVersion(int previousVersion, SQLiteDatabase db) {
        Log.i(TAG, "upgradeFromPreviousVersion: " + previousVersion);

    }

    @Override
    public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
        Log.i(TAG, "onUpgrade: oldVersion " + oldVersion + " " + newVersion);
        db.execSQL("DROP TABLE IF EXISTS " + LPRConstants.TABLE_LPR_LIST);
        db.execSQL(LPRConstants.CREATE_TABLE_LPR_LIST);
    /*    switch(newVersion){

            case 2:

                break;
                default:
                    break;
        }
*/
       /* for (int currentVersion = oldVersion + 1; currentVersion <= newVersion; currentVersion++) {
            upgradeFromPreviousVersion(currentVersion, db);
        }*/

    }


}