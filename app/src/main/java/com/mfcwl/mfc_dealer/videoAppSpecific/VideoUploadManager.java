package com.mfcwl.mfc_dealer.videoAppSpecific;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.mfc.mfcuploader.MFCUploader;
import com.mfcwl.mfc_dealer.ASMModel.videoUrlRes;
import com.mfcwl.mfc_dealer.Model.videoUrlReq;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.Global;
import com.mfcwl.mfc_dealer.encrypt.AWSSecretsManager;
import com.mfcwl.mfc_dealer.retrofitconfig.VideoUrlServices;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import Listners.ImageUploadListner;
import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Utility.GlobalText.BUCKET_KEY;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.BUCKET_SECRET;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_dealer_code;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_dealer_name;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_lead_id;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_lead_regno;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_video_local_path;
import static com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter.COLUMN_Key_video_server_url;


public class VideoUploadManager extends AbstractMediaUploadManager implements ImageUploadListner {

    private static final String TAG = VideoUploadManager.class.getSimpleName();

    @Override
    protected List<String> getAllLeadsForPendingMediaUpload() {
        return dbAdapter.getAllVideoLeadIdsWhichAreNotOffline();
    }

    @Override
    protected void uploadMediaForSpecificLead(final String lead) {
        try {
            if (CommonMethods.isConnectingToInternet(null)) {

                final String isVideoApiSynced = dbAdapter.isVideoApiSynced(lead);
                final String isVideoUploaded = dbAdapter.isVideoUploaded(lead);

                Log.i(TAG, "lead: " + lead);
                Log.i(TAG, "isVideoApiSynced: " + isVideoApiSynced);
                Log.i(TAG, "isVideoUploaded: " + isVideoUploaded);


                if (isVideoUploaded.equals("0")) {

                    try {
                        final List<Map<String, String>> videoRecordsForLeadIdAndStatus = dbAdapter.getVideoRecordsForLeadIdAndStatus(lead, "0");
                        for (final Map<String, String> videoRecord : videoRecordsForLeadIdAndStatus) {
                            /// If lead created offline, then we don't have lead Reference No
                            /// But we will have lead id with dummy offline lead id, which is lead if itself.
                            String leadId = videoRecord.get(COLUMN_Key_lead_id);
                            String code = videoRecord.get(COLUMN_Key_dealer_code);
                            String dealer_name = videoRecord.get(COLUMN_Key_dealer_name);

                            final String localPath = videoRecord.get(COLUMN_Key_video_local_path);

                           // final String localPath = "/storage/emulated/0/DCIM/Camera/VID_20200110_165649241.mp4";

                                try {
                                    final File file = new File(localPath);
                                    if (file.exists()) {
                                        Log.d(TAG, "The video file is exists=" + localPath);
                                    }else{
                                        Log.d(TAG, "The video file is deleted=" + localPath);
                                        //local path deleted stocks
                                        SqlAdapterForDML.getInstance().deleteSteps(leadId);
                                        return;
                                    }

                                } catch (Exception e) {
                                }


                            try {

                                final AWSSecretsManager SECRETS_MANAGER = AWSSecretsManager.getInstance();
                                final String key = SECRETS_MANAGER.get(BUCKET_KEY);
                                final String secret = SECRETS_MANAGER.get(BUCKET_SECRET);
                                final String bucketName = Global.videoBucket;

                 /*               final MFCUploader mfcUploader = new MFCUploader(context, bucketName, key, secret, this, "us-west-2");
                                mfcUploader.uploadData(generateVideoUploadDirectory(dealer_name,code, leadId), localPath, leadId, "mfc");*/
                                final MFCUploader mfcUploader = new MFCUploader(context, bucketName, key, secret, this);
                                mfcUploader.uploadData(generateVideoUploadDirectory(dealer_name,code, leadId), localPath, leadId, "mfc");

                            } catch (Exception e) {
                                Log.e(TAG, "Exception: ");

                            }
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "startBackgroundProcess: " + e.getMessage());
                    }
                } else if (isVideoUploaded.equals("1") && isVideoApiSynced.equals("0")) {
                    callVideoApi(lead, "mfc");
                } else {
                    Log.i(TAG, "startBackgroundProcess: ");
                }
            } else {
                Log.d(TAG, NO_INTERNET_MESSAGE);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception in uploadImages " + e.getMessage());
        }
    }

    private void callVideoApi(final String lead, final String leadType) {

        try {
            final List<Map<String, String>> videoRecords = dbAdapter.getVideoRecordsForLeadIdAndStatus(lead, "1");
            if (videoRecords.size() > 0) {

                final Map<String, String> record = videoRecords.get(0);
                String id = record.get(COLUMN_Key_lead_id);
                String dealer_code = record.get(COLUMN_Key_dealer_code);
                String dealer_name = record.get(COLUMN_Key_dealer_name);
                String vidPath = record.get(COLUMN_Key_video_server_url);
                String regno = record.get(COLUMN_Key_lead_regno);

                videoUrlReq req = new videoUrlReq();
                CommonMethods db =new CommonMethods();

                req.setDealerCode(dealer_code);
                req.setVideoUrl(vidPath);
                req.setStockId(id);
                req.setUploadedFrom(id);
                req.setIsNonMFC("false");
                req.setUserId(db.getDb("user_id"));
                req.setUploadedFrom("mobile-android");
                req.setUploadedLoc("unknow");

                if (CommonMethods.isConnectingToInternet(null)) {
                    sendVideoUrl(context, req, "update",regno);
                }

            }
        } catch (Exception e) {
            Log.e(TAG, "callVideoApi: " + e.getMessage());
        }
    }


    private String generateVideoUploadDirectory(final String dealer_name,final String dealerId, final String stockid) {

        final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault());
        final Calendar CALENDAR = Calendar.getInstance();

        final String currentTime = DATE_FORMAT.format(CALENDAR.getTime());
        final String year = currentTime.split("_")[0];
        final String month = currentTime.split("_")[1];
        final String day = currentTime.split("_")[2];

        // return String.format("autoinspekt/%s/%s/%s/%s/original/", year, month, day, leadId); // the folder structure for the image in Prod

        return String.format("stage_web_videos/usedcarsvideo/mfc/dealer/%s/%s/%s/",dealer_name, dealerId, stockid);

    }


    public void sendVideoUrl(final Context mContext, final videoUrlReq req, String status, String regno) {

        VideoUrlServices.sendVideoUrl(req, mContext, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                try {
                    Response<videoUrlRes> mRes = (Response<videoUrlRes>) obj;

                    videoUrlRes mData = mRes.body();

                    if(mData.getStatus().equalsIgnoreCase("SUCCESS")) {
                        dbForDML.updateVideoStatusIntoDB(req.getStockId(), "mfc");
                    }

                    Log.i(TAG, "OnSuccess: " + "video success->");

                    NotificationChannel mChannel=null;
                    NotificationManager notifManager=null;

                    if (notifManager == null) {
                        notifManager = (NotificationManager) mContext.getSystemService
                                (Context.NOTIFICATION_SERVICE);
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        NotificationCompat.Builder builder;
                        Intent intent = new Intent(mContext, VideoUploadManager.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        PendingIntent pendingIntent;
                        int importance = NotificationManager.IMPORTANCE_HIGH;
                        if (mChannel == null) {
                            mChannel = new NotificationChannel
                                    ("0", "MFC Business", importance);
                            mChannel.setDescription("RegNo. "+regno+" : "+"Video successfully uploaded");
                            //mChannel.enableVibration(true);
                            notifManager.createNotificationChannel(mChannel);
                        }
                        builder = new NotificationCompat.Builder(mContext, "0");

                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        pendingIntent = PendingIntent.getActivity(mContext, 1251, intent, PendingIntent.FLAG_ONE_SHOT);
                        builder.setContentTitle("MFC Business")
                                .setContentText("RegNo. "+regno+" : "+"Video successfully uploaded")  // required
                                .setDefaults(Notification.DEFAULT_ALL)
                                .setAutoCancel(true)
                                .setSmallIcon(R.drawable.video1)
                                .setContentIntent(pendingIntent)
                               /* .setSound(RingtoneManager.getDefaultUri
                                        (RingtoneManager.TYPE_NOTIFICATION))*/;
                        Notification notification = builder.build();
                        notifManager.notify(0, notification);

                    } else {

                    Intent notificationIntent = new Intent("android.media.action.DISPLAY_NOTIFICATION");
                    notificationIntent.addCategory("android.intent.category.DEFAULT");
                    PendingIntent broadcast = PendingIntent.getBroadcast(mContext, 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext,"");
                    Notification notification = builder.setContentTitle("MFC Business")
                            .setContentText("RegNo. "+regno+" : "+"Video successfully uploaded")
                            .setTicker("New Message Alert!")
                            .setSmallIcon(R.drawable.video1)
                            .setContentIntent(broadcast).build();

                   NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(0, notification);
                    }

                    try {
                        Toast.makeText(mContext, "video uploaded successfully", Toast.LENGTH_SHORT).show();
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                } catch (Exception e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {

                Log.i(TAG, "OnFailure: "+mThrowable.getStackTrace());
            }
        });
    }

    @Override
    public void onSuccess(String imageUrl, String leadId, String leadType) {
        Log.i(TAG, "onSuccess: " + imageUrl);
        Log.i(TAG, "onSuccess: " + leadId);
        Log.i(TAG, "onSuccess: " + leadType);

        dbForDML.updateVideoUrlIntoDB(imageUrl, leadId, leadType, context);

        if (dbAdapter.isMainApiSynced(leadId).equals("0")) {
            Log.e(TAG, "skipping VideoApiSync because lead MainApiSync is not completed");
            callVideoApi(leadId, leadType);

        } else {
           // callVideoApi(leadId, leadType);
        }
    }

    @Override
    public void onProgress(int id, long bytesCurrent, long bytesTotal) {
        Log.i(TAG, "onProgress: ");

        float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
        int percentDone = (int) percentDonef;

        Log.i(TAG, "ID:" + id + " bytesCurrent: " + bytesCurrent
                + " bytesTotal: " + bytesTotal + " " + percentDone + "%");

    }

}
