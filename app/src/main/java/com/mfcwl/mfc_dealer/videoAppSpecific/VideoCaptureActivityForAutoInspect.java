package com.mfcwl.mfc_dealer.videoAppSpecific;

import android.app.Activity;
import android.content.Intent;

import com.mfcwl.mfc_dealer.Utility.CommonMethods;

import videoCapture.VideoCaptureActivity;
import videoCapture.VideoCaptureConstants;
import videoCapture.VideoCaptureIntentHandler;
import videoCapture.VideoCapturedEventHandler;


public final class VideoCaptureActivityForAutoInspect extends VideoCaptureActivity {

    @Override
    public VideoCapturedEventHandler getVideoCapturedEventHandler() {
        return new VideoCapturedHandler(this);
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    public String getTimerText(long millisUntilFinished) {

        if (((millisUntilFinished / 1000) <= 60) && ((millisUntilFinished / 1000) > 54)) {
            return "Video Recording...";
        } else if (((millisUntilFinished / 1000) <= 54) && ((millisUntilFinished / 1000) > 48)) {
            return "Video Recording...";
        } else if (((millisUntilFinished / 1000) <= 48) && ((millisUntilFinished / 1000) > 42)) {
            return "Video Recording...";
        } else if (((millisUntilFinished / 1000) <= 42) && ((millisUntilFinished / 1000) > 36)) {
            return "Video Recording...";
        } else if (((millisUntilFinished / 1000) <= 36) && ((millisUntilFinished / 1000) > 30)) {
            return "Video Recording...";
        } else if (((millisUntilFinished / 1000) <= 30) && ((millisUntilFinished / 1000) > 24)) {
            return "Video Recording...";
        } else if (((millisUntilFinished / 1000) <= 24) && ((millisUntilFinished / 1000) > 18)) {
            return "Video Recording...";
        } else if (((millisUntilFinished / 1000) <= 18) && ((millisUntilFinished / 1000) > 12)) {
            return "Video Recording...";
        } else if (((millisUntilFinished / 1000) <= 12) && ((millisUntilFinished / 1000) > 6)) {
            return "Video Recording...";
        } else if (((millisUntilFinished / 1000) <= 6) && ((millisUntilFinished / 1000) > -1)) {
            return "Video Recording...";
        }
        return "";
    }

    @Override
    public VideoCaptureIntentHandler getIntentHandler(final Intent intent) {

        intent.putExtra(VideoCaptureConstants.KEY_MUTE_AUDIO,false);

        return new VideoCaptureIntentHandlerOverrideForFilePath(intent);
    }

    @Override
    public void onBackPressed() {
        CommonMethods.setvalueAgainstKey(VideoCaptureActivityForAutoInspect.this,"videopath","");
        super.onBackPressed();
    }
}
