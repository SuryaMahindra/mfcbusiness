package com.mfcwl.mfc_dealer.videoAppSpecific;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import videoCapture.VideoCaptureConstants;
import videoCapture.VideoCapturedEventHandler;


final class VideoCapturedHandler implements VideoCapturedEventHandler {

    private static final String TAG = VideoCapturedHandler.class.getSimpleName();
    private final Activity activity;

    VideoCapturedHandler(final Activity activity) {

        this.activity = activity;
    }

    private void addVideoDataToDBTable(final String videoLocalPath, final String lead,final String dealer_code,
                                       final String leadType, final String dealer_name, final String regno) {
        Log.i(TAG, " addVideoDataToDBTable: ");
        try {

            final VideoMetadata metadata = new VideoMetadata();
            metadata.setLocalVideoFileName(videoLocalPath);
            metadata.setLead(lead);
            metadata.setDealer_code(dealer_code);
            metadata.setLeadType(leadType);
            metadata.setRegno(regno);
            metadata.setDealer_name(dealer_name);

            SqlAdapterForDML.getInstance().insertVideoData(metadata);

        } catch (Exception e) {
            Log.e(TAG, "addVideoDataToDBTable: " + e.getMessage());
        }
    }

    @Override
    public void onCapture(final Intent intent, final boolean isVideoCapturedCorrectly, final boolean shouldCallActivityFinish) {
        if (isVideoCapturedCorrectly) {
            final String filePath = intent.hasExtra(VideoCaptureConstants.KEY_RESULT) ?
                    intent.getStringExtra(VideoCaptureConstants.KEY_RESULT) : VideoCaptureConstants.NO_CAPTURE_RESULT;
            final String leadId = intent.hasExtra("lead") ?
                    intent.getStringExtra("lead") : "";

            final String dealer_code = intent.hasExtra("dealer_code") ?
                    intent.getStringExtra("dealer_code") : "";

            final String dealer_name = intent.hasExtra("dealer_name") ?
                    intent.getStringExtra("dealer_name") : "dealer";

            final String regno = intent.hasExtra("regno") ?
                    intent.getStringExtra("regno") : "NA";

            addVideoDataToDBTable(filePath, leadId, dealer_code,"mfc",dealer_name,regno);

            Log.d(TAG, "filePath="+filePath);
            Log.d(TAG, "leadId="+leadId);
            Log.d(TAG, "dealer_code="+dealer_code);
            Log.d(TAG, "dealer_name="+dealer_name);
            Log.d(TAG, "regno="+regno);

            activity.setResult(Activity.RESULT_OK, intent);
        } else {
            Log.d(TAG, "Video Capture was interrupted.");
            activity.setResult(Activity.RESULT_CANCELED, intent);
        }
        if (shouldCallActivityFinish) {
            Log.d(TAG, "onCapture... finishing activity");
            activity.finish();
        }
    }



}
