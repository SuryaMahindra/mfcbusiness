package com.mfcwl.mfc_dealer.videoAppSpecific;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Locale;

final class VideoCaptureFileLocationGenerator {

    private static final String TAG = VideoCaptureFileLocationGenerator.class.getSimpleName();

    private static final String VIDEO_FILE_EXTENSION = ".mp4";
    private static final String VIDEO = "/Videos";
    private static final String mfc = "/MFC Business";
    private static File VIDEO_CAPTURE_DIRECTORY;
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault());

    static {
        try {
            final File SD_CARD_DIRECTORY = Environment.getExternalStorageDirectory();
            VIDEO_CAPTURE_DIRECTORY = new File(SD_CARD_DIRECTORY.getAbsolutePath() + mfc + VIDEO);
            if (!VIDEO_CAPTURE_DIRECTORY.isDirectory()) {
                if (!VIDEO_CAPTURE_DIRECTORY.mkdirs()) {
                    Log.e(TAG, "Video Capture Directory was not created");
                } else {
                    Log.i(TAG, "Video Capture Directory is: " + VIDEO_CAPTURE_DIRECTORY.getAbsolutePath());
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    String generateVideoCaptureFileName(final String hint) {
        final StringBuffer sb;
        sb = new StringBuffer(VIDEO_CAPTURE_DIRECTORY.getAbsolutePath())
                .append("/")
               // .append(hint)
                //.append("_")
                .append(/*DATE_FORMAT.format(new Date())*/hint)
                .append(VIDEO_FILE_EXTENSION);
        Log.i(TAG, "VideoCaptureActivity location: " + sb);
        return sb.toString();
    }

}
