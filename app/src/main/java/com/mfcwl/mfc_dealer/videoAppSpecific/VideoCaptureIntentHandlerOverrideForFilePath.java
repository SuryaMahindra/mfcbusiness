package com.mfcwl.mfc_dealer.videoAppSpecific;

import android.content.Intent;
import android.util.Log;

import com.mfcwl.mfc_dealer.Activity.VideoActivity;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;

import java.io.File;

import videoCapture.VideoCaptureIntentHandler;

final class VideoCaptureIntentHandlerOverrideForFilePath extends VideoCaptureIntentHandler {

    private static final String TAG = VideoCaptureIntentHandlerOverrideForFilePath.class.getSimpleName();

    private String localVideoPath;
    private final Intent intent;

    VideoCaptureIntentHandlerOverrideForFilePath(final Intent intent) {
        super(intent);
        this.intent = intent;
        generateNewFilePath();
    }

    private void generateNewFilePath() {
        if (intent != null) {
            if (intent.hasExtra("lead")) {

                final String stockid = intent.hasExtra("lead") ?
                        intent.getStringExtra("lead") : "";

                localVideoPath = new VideoCaptureFileLocationGenerator().generateVideoCaptureFileName(stockid);
            } else {
                localVideoPath = "dummy.mp4";
            }
        }

        try {
            CommonMethods.setvalueAgainstKey(VideoActivity.act, "videopath", localVideoPath);
            VideoActivity.videoData = "yes";
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.i(TAG, "Requested localVideoPath=" + localVideoPath);
    }

    @Override
    public String getFilePath(final boolean ensureNewFile) {
        if (ensureNewFile) {
            deleteRecordingIfExists(localVideoPath);
            generateNewFilePath();
        }
        return localVideoPath;
    }

    private void deleteRecordingIfExists(final String fileName) {
        try {
            final File file = new File(fileName);
            if (file.exists() && file.delete()) {
                Log.d(TAG, "Partially captured video is deleted " + fileName);
            }
        } catch (Exception e) {
            Log.e(TAG, "When deleting file= " + fileName + " error=" + e.getMessage());
        }
    }



}