package com.mfcwl.mfc_dealer.videoAppSpecific;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mfc.mfcandroidlocalpersistence.SimpleCursorIteratorForSqliteDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.mfcwl.mfc_dealer.videoAppSpecific.DatabaseHelper.DATABASE_HELPER_INSTANCE;

public class AISQLLiteAdapter {

    // DB Info
    public static final String DATABASE_NAME = "mfc.db2";
    public static final int DATABASE_VERSION = 1; //old 9



    public static final String TABLE_NAME_VIDEOS = "Videos";
    public static final String COLUMN_LeadID = "lead_id";

    public static final String COLUMN_Key_lead_id = "lead_id";
    public static final String COLUMN_Key_dealer_code = "dealer_code";
    public static final String COLUMN_Key_dealer_name = "dealer";


    public static final String COLUMN_Key_video_local_path = "video_local_path";
    public static final String COLUMN_Key_video_server_url = "video_server_url";

    public static final String COLUMN_Key_video_upload_status = "video_upload_status";

    public static final String COLUMN_Key_main_api_synced = "main_api_synced";
    public static final String COLUMN_Key_lead_type = "lead_type";
    public static final String COLUMN_Key_lead_regno = "regno";




    private static String TAG = AISQLLiteAdapter.class.getSimpleName();

    /// Implementation of Singleton class
    private static AISQLLiteAdapter INSTANCE = new AISQLLiteAdapter();

    /// Note the implementation of Singleton class is by creating a static instance and no threads
    public static AISQLLiteAdapter getInstance() {
        return INSTANCE;
    }

    private static SQLiteDatabase db = DATABASE_HELPER_INSTANCE.getWritableDatabase();

    private AISQLLiteAdapter() {
    }


    public boolean isAvailableInTableCommon(
            final String tableName, final String key1, final String val1) {
        final String query = String.format("SELECT * FROM %s WHERE %s='%s'",
                tableName, key1, val1);
        return checkWhetherAConditionIsTrueInDatabase(query);
    }


    public boolean isAvailableInTableCommon(
            final String tableName, final String key1, final String val1,
            final String key2, final String val2,
            final String key3, final String val3) {
        final String query = String.format("SELECT * FROM %s WHERE %s='%s' AND %s='%s' AND %s='%s'",
                tableName, key1, val1, key2, val2, key3, val3);
        return checkWhetherAConditionIsTrueInDatabase(query);
    }



    public boolean isAvailableInTable(final String tableName, final String pkId, final String id) {
        return isAvailableInTableCommon(tableName, pkId, id);
    }


    private boolean checkWhetherAConditionIsTrueInDatabase(final String query) {
        final SimpleCursorIteratorForSqliteDatabase<String> helper = new SimpleCursorIteratorForSqliteDatabase<>();
        return 0 < helper.executeAndReturnCountForSelectQuery(db, query);
    }

    public ArrayList<String> getAllVideoLeadIdsWhichAreNotOffline() {
        final String sql = String.format("SELECT %s FROM %s WHERE %s NOT LIKE '%%OFF%%'", COLUMN_LeadID, TABLE_NAME_VIDEOS, COLUMN_LeadID);
        final SimpleCursorIteratorForSqliteDatabase<String> helper = new SimpleCursorIteratorForSqliteDatabase<>();
        return helper.executeAndReturnSingleColumnArrayList(String.class, db, sql,
                null, 0, COLUMN_LeadID);
    }

    public String isMainApiSynced(final String lead) {
        Log.i(TAG, "isMainApiSynced: " + lead);
        final String sql = String.format("SELECT * FROM %s WHERE %s='%s'",
                TABLE_NAME_VIDEOS,
                COLUMN_Key_lead_id, lead);
        final SimpleCursorIteratorForSqliteDatabase<String> helper = new SimpleCursorIteratorForSqliteDatabase<>();
        return helper.executeAndReturnSingleValue(String.class, db, sql, null,
                0, COLUMN_Key_main_api_synced, "");
    }



    public String isVideoUploaded(final String leadId) {
        Log.i(TAG, "isVideoUploaded: ");
        final String sql = String.format("SELECT %s FROM %s WHERE %s='%s'",
                COLUMN_Key_video_upload_status, TABLE_NAME_VIDEOS,
                COLUMN_Key_lead_id, leadId);
        final SimpleCursorIteratorForSqliteDatabase<String> helper = new SimpleCursorIteratorForSqliteDatabase<>();
        return helper.executeAndReturnSingleValue(String.class, db, sql, null,
                0, COLUMN_Key_video_upload_status, "");
    }

    public String isVideoGetlink(final String leadId) {
        Log.i(TAG, "isVideoUploaded: ");
        final String sql = String.format("SELECT %s FROM %s WHERE %s='%s'",
                COLUMN_Key_video_server_url, TABLE_NAME_VIDEOS,
                COLUMN_Key_lead_id, leadId);
        final SimpleCursorIteratorForSqliteDatabase<String> helper = new SimpleCursorIteratorForSqliteDatabase<>();
        return helper.executeAndReturnSingleValue(String.class, db, sql, null,
                0, COLUMN_Key_video_server_url, "");
    }

    public String isVideoLocalPath(final String leadId) {
        Log.i(TAG, "isVideoUploaded: ");
        final String sql = String.format("SELECT %s FROM %s WHERE %s='%s'",
                COLUMN_Key_video_local_path, TABLE_NAME_VIDEOS,
                COLUMN_Key_lead_id, leadId);
        final SimpleCursorIteratorForSqliteDatabase<String> helper = new SimpleCursorIteratorForSqliteDatabase<>();
        return helper.executeAndReturnSingleValue(String.class, db, sql, null,
                0, COLUMN_Key_video_local_path, "");
    }

    public String isVideoApiSynced(final String leadId) {
        Log.i(TAG, "isVideoApiSynced: ");
        final String sql = String.format("SELECT %s FROM %s WHERE %s='%s'",
                COLUMN_Key_main_api_synced, TABLE_NAME_VIDEOS,
                COLUMN_Key_lead_id, leadId);
        final SimpleCursorIteratorForSqliteDatabase<String> helper = new SimpleCursorIteratorForSqliteDatabase<>();
        return helper.executeAndReturnSingleValue(String.class, db, sql, null,
                0, COLUMN_Key_main_api_synced, "");
    }


    public List<Map<String, String>> getVideoRecordsForLeadIdAndStatus(final String leadId, final String status) {
        final String sql = String.format("SELECT * FROM %s WHERE %s='%s' AND %s='%s'",
                TABLE_NAME_VIDEOS, COLUMN_Key_lead_id, leadId, COLUMN_Key_video_upload_status, status);
        final SimpleCursorIteratorForSqliteDatabase<String> helper = new SimpleCursorIteratorForSqliteDatabase<>();
        return helper.executeAndReturnListOfKeyValues(db, sql, null);
    }

}
