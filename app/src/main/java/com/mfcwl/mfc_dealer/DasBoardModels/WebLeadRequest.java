package com.mfcwl.mfc_dealer.DasBoardModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WebLeadRequest {


    @SerializedName("filter_by_fields")
    @Expose
    private String filterByFields;

    @SerializedName("per_page")
    @Expose
    private String perPage;
    @SerializedName("page")
    @Expose
    private String page;
    @SerializedName("order_by")
    @Expose
    private String orderBy;
    @SerializedName("order_by_reverse")
    @Expose
    private String orderByReverse;
    @SerializedName("alias_fields")
    @Expose
    private String aliasFields;
    @SerializedName("custom_where")
    @Expose
    private List<CustomWhere> customWhere = null;
    @SerializedName("wherenotin")
    @Expose
    private List<Object> wherenotin = null;
    @SerializedName("access_token")
    @Expose
    private String accessToken;

    public String getPerPage() {
        return perPage;
    }

    public void setPerPage(String perPage) {
        this.perPage = perPage;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getOrderByReverse() {
        return orderByReverse;
    }

    public void setOrderByReverse(String orderByReverse) {
        this.orderByReverse = orderByReverse;
    }

    public String getAliasFields() {
        return aliasFields;
    }

    public void setAliasFields(String aliasFields) {
        this.aliasFields = aliasFields;
    }

    public List<CustomWhere> getCustomWhere() {
        return customWhere;
    }

    public void setCustomWhere(List<CustomWhere> customWhere) {
        this.customWhere = customWhere;
    }

    public List<Object> getWherenotin() {
        return wherenotin;
    }

    public void setWherenotin(List<Object> wherenotin) {
        this.wherenotin = wherenotin;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getFilterByFields() {
        return filterByFields;
    }

    public void setFilterByFields(String filterByFields) {
        this.filterByFields = filterByFields;
    }

}