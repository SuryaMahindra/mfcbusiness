package com.mfcwl.mfc_dealer.DasBoardModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DashBoardResponse {

    @SerializedName("warranty_balance")
    @Expose
    private Integer warrantyBalance;
    @SerializedName("private_leads_followup_count_sales")
    @Expose
    private Integer privateLeadsFollowupCountSales;
    @SerializedName("private_leads_followup_count_procurement")
    @Expose
    private Integer privateLeadsFollowupCountProcurement;
    @SerializedName("total_stock_count")
    @Expose
    private Integer totalStockCount;
    @SerializedName("total_old_stock_count")
    @Expose
    private Integer totalOldStockCount;

    public Integer getWarrantyBalance() {
        return warrantyBalance;
    }

    public void setWarrantyBalance(Integer warrantyBalance) {
        this.warrantyBalance = warrantyBalance;
    }

    public Integer getPrivateLeadsFollowupCountSales() {
        return privateLeadsFollowupCountSales;
    }

    public void setPrivateLeadsFollowupCountSales(Integer privateLeadsFollowupCountSales) {
        this.privateLeadsFollowupCountSales = privateLeadsFollowupCountSales;
    }

    public Integer getPrivateLeadsFollowupCountProcurement() {
        return privateLeadsFollowupCountProcurement;
    }

    public void setPrivateLeadsFollowupCountProcurement(Integer privateLeadsFollowupCountProcurement) {
        this.privateLeadsFollowupCountProcurement = privateLeadsFollowupCountProcurement;
    }

    public Integer getTotalStockCount() {
        return totalStockCount;
    }

    public void setTotalStockCount(Integer totalStockCount) {
        this.totalStockCount = totalStockCount;
    }

    public Integer getTotalOldStockCount() {
        return totalOldStockCount;
    }

    public void setTotalOldStockCount(Integer totalOldStockCount) {
        this.totalOldStockCount = totalOldStockCount;
    }

}

