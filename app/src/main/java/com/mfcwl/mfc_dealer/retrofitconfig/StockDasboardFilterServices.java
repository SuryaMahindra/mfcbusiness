package com.mfcwl.mfc_dealer.retrofitconfig;

import android.app.Activity;

import com.mfcwl.mfc_dealer.ASMModel.stockFilterReq;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class StockDasboardFilterServices extends DasboardBaseService {

    public  static Activity act;

    public static void getLeadStatus(String dealer_code ,Activity activity,final HttpCallResponse mHttpCallResponse) {

        ApiInterface mInterFace = retrofit.create(ApiInterface.class);

        Call<stockFilterReq> mCall = mInterFace.getLeadStatus(dealer_code);
        mCall.enqueue(new Callback<stockFilterReq>() {
            @Override
            public void onResponse(Call<stockFilterReq> call, Response<stockFilterReq> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<stockFilterReq> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);

            }
        });

    }

    public interface ApiInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @GET("franschisestock/getstockdashboardapi")
        Call<stockFilterReq> getLeadStatus(@Query("dealer_code") String id);
    }
}
