package com.mfcwl.mfc_dealer.retrofitconfig;

import com.mfcwl.mfc_dealer.ASMModel.signupRes;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public class forgotService extends DasboardBaseService {


    public static void getpassword(String name,final HttpCallResponse mHttpCallResponse) {

        ApiInterface mInterFace = retrofit.create(ApiInterface.class);

        Call<signupRes> mCall = mInterFace.getLeadStatus(name);
        mCall.enqueue(new Callback<signupRes>() {
            @Override
            public void onResponse(Call<signupRes> call, Response<signupRes> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }else{
                    Throwable t =new Throwable();
                    mHttpCallResponse.OnFailure(t);
                }
            }

            @Override
            public void onFailure(Call<signupRes> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });

    }

    public interface ApiInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @GET("registration/getpassword")
        Call<signupRes> getLeadStatus(@Query("username") String id);
    }
}
