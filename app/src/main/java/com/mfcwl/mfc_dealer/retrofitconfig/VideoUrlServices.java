package com.mfcwl.mfc_dealer.retrofitconfig;

import android.app.Activity;
import android.content.Context;

import com.mfcwl.mfc_dealer.ASMModel.videoUrlRes;
import com.mfcwl.mfc_dealer.Model.videoUrlReq;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.PUT;

public class VideoUrlServices extends DasboardBaseService {

    public static  Activity mContext;


    public static void sendVideoUrl(final videoUrlReq req, Context mContext, final HttpCallResponse mHttpCallResponse) {

        SendVideUrl mInterface = retrofit.create(SendVideUrl.class);

        Call<videoUrlRes> mCall = mInterface.getStock(req);

        mCall.enqueue(new Callback<videoUrlRes>() {
            @Override
            public void onResponse(Call<videoUrlRes> call, Response<videoUrlRes> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {
                    Throwable t=new Throwable ();
                    mHttpCallResponse.OnFailure(t);
                }
            }

            @Override
            public void onFailure(Call<videoUrlRes> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });
    }
    public interface SendVideUrl {
        @Headers("Content-Type: application/json; charset=utf-8")
        @PUT("dealer/stocks/video")
        Call<videoUrlRes> getStock(@Body videoUrlReq req);
    }


}
