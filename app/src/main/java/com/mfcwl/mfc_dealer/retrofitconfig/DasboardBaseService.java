package com.mfcwl.mfc_dealer.retrofitconfig;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.Global;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DasboardBaseService {

    protected static final String BASE_URL = Global.dasboard;

    protected static HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

    protected static TokenInterceptor tokenInterceptor = new TokenInterceptor();
    protected static OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(1, TimeUnit.MINUTES)
            .readTimeout(180, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(tokenInterceptor)
            .addInterceptor(interceptor)
            .build();
    protected static Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create();

    protected static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();

    private static class TokenInterceptor implements Interceptor {
        private TokenInterceptor() {

        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request initialRequest = chain.request();

            String sToken = "";
            if (sToken != null) {
                initialRequest = initialRequest.newBuilder()
                        // .addHeader("Accept", "application/json; charset=utf-8")
                        .addHeader("token", CommonMethods.getToken("token"))
                        .build();
            }

            Response response = chain.proceed(initialRequest);
            return response;
        }
    }
}
