package com.mfcwl.mfc_dealer.retrofitconfig;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import android.util.Log;

import com.mfcwl.mfc_dealer.ASMModel.DashboardEmailReq;
import com.mfcwl.mfc_dealer.ASMModel.DashboardEmailRes;
import com.mfcwl.mfc_dealer.ASMModel.DealerName;
import com.mfcwl.mfc_dealer.ASMModel.chartDataRes;
import com.mfcwl.mfc_dealer.ASMModel.cityRes;
import com.mfcwl.mfc_dealer.ASMModel.dasboardRes;
import com.mfcwl.mfc_dealer.ASMModel.dealerReq;
import com.mfcwl.mfc_dealer.ASMModel.graphDataRes;
import com.mfcwl.mfc_dealer.ASMModel.highparamRes;
import com.mfcwl.mfc_dealer.ASMModel.stateRes;
import com.mfcwl.mfc_dealer.DealerPerformanceModels.DealerPerformanceResponse;
import com.mfcwl.mfc_dealer.Model.stateReq;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.StateHead.StateManager;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.ZonalHead.AreaManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public class DasboardStatusServices extends DasboardBaseService {

    public static Activity mContext;

    public CommonMethods db = new CommonMethods();

    public static void getLeadStatus(Activity act, final HttpCallResponse mHttpCallResponse) {

        mContext = act;

        DasboardStatusServices.ApiInterface mInterFace = retrofit.create(DasboardStatusServices.ApiInterface.class);
        Call<List<dasboardRes>> mCall = mInterFace.getLeadStatus();
        mCall.enqueue(new Callback<List<dasboardRes>>() {
            @Override
            public void onResponse(@NonNull Call<List<dasboardRes>> call, @NonNull Response<List<dasboardRes>> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                    Log.i("TAG", "onResponse: " + response.body().size());
                } else {


                    if (response.code() == 400) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 401) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 500) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 404) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 304) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 503) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 405) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    }

                    Throwable t = new Throwable();
                    mHttpCallResponse.OnFailure(t);
                }
            }

            @Override
            public void onFailure(Call<List<dasboardRes>> call, Throwable t) {

                CommonMethods.alertMessage(mContext, "Try Again.");
                mHttpCallResponse.OnFailure(t);
            }
        });


    }

    public static void getLeadStatusStateHead(Activity act, final HttpCallResponse mHttpCallResponse) {

        mContext = act;

        DasboardStatusServices.ApiInterface mInterFace = retrofit.create(DasboardStatusServices.ApiInterface.class);
        Call<List<AreaManager>> mCall = mInterFace.getLeadStatusStateHead();
        mCall.enqueue(new Callback<List<AreaManager>>() {
            @Override
            public void onResponse(@NonNull Call<List<AreaManager>> call, @NonNull Response<List<AreaManager>> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                    Log.i("TAG", "onResponse: " + response.body().size());
                } else {

                    if (response.code() == 400) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 401) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 500) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 404) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 304) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 503) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 405) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    }

                    Throwable t = new Throwable();
                    mHttpCallResponse.OnFailure(t);
                }
            }

            @Override
            public void onFailure(Call<List<AreaManager>> call, Throwable t) {

                CommonMethods.alertMessage(mContext, "Try Again.");
                mHttpCallResponse.OnFailure(t);
            }
        });


    }

    public static void getLeadStatusZonalHead(Activity act, final HttpCallResponse mHttpCallResponse) {

        mContext = act;

        DasboardStatusServices.ApiInterface mInterFace = retrofit.create(DasboardStatusServices.ApiInterface.class);
        Call<List<StateManager>> mCall = mInterFace.getLeadStatusZonalHead();
        mCall.enqueue(new Callback<List<StateManager>>() {
            @Override
            public void onResponse(@NonNull Call<List<StateManager>> call, @NonNull Response<List<StateManager>> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                    Log.i("TAG", "onResponse: " + response.body().size());
                } else {


                    if (response.code() == 400) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 401) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 500) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 404) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 304) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 503) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 405) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    }

                    Throwable t = new Throwable();
                    mHttpCallResponse.OnFailure(t);
                }
            }

            @Override
            public void onFailure(Call<List<StateManager>> call, Throwable t) {
                CommonMethods.alertMessage(mContext, "Try Again.");
                mHttpCallResponse.OnFailure(t);
            }
        });


    }

    public static void sendEmailDashboard(final DashboardEmailReq req, Context mContext, final HttpCallResponse mHttpCallResponse) {


        ApiInterface mInterface = retrofit.create(ApiInterface.class);
        Call<DashboardEmailRes> mCall = mInterface.getEmail(req);

        mCall.enqueue(new Callback<DashboardEmailRes>() {
            @Override
            public void onResponse(Call<DashboardEmailRes> call, Response<DashboardEmailRes> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {

                    Throwable t = new Throwable();
                    mHttpCallResponse.OnFailure(t);

                    if (response.code() == 400) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 401) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 500) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 404) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 304) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 503) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 405) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    }
                }
            }

            @Override
            public void onFailure(Call<DashboardEmailRes> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });
    }


    public void getDealerNames(Activity act, String zone, final HttpCallResponse mHttpCallResponse) {
        mContext = act;
        ApiInterface mInterFace = retrofit.create(ApiInterface.class);
        Call<List<DealerName>> mCall = mInterFace.getDealerName(zone);
        mCall.enqueue(new Callback<List<DealerName>>() {
            @Override
            public void onResponse(@NonNull Call<List<DealerName>> call, @NonNull Response<List<DealerName>> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                    //Log.i("TAG", "onResponse: "+response.body().size());
                } else {
                    db.error_popup(response.code(), response.message(), mContext);
                    Throwable t = new Throwable();
                    mHttpCallResponse.OnFailure(t);
                }
            }

            @Override
            public void onFailure(Call<List<DealerName>> call, Throwable t) {

                CommonMethods.alertMessage(mContext, "Try Again.");
                mHttpCallResponse.OnFailure(t);
            }
        });
    }

    public void getStateList(Activity act, stateReq state, final HttpCallResponse mHttpCallResponse) {

        mContext = act;

        ApiInterface mInterface = retrofit.create(ApiInterface.class);

        Call<List<stateRes>> mCall = mInterface.getstate(state);

        mCall.enqueue(new Callback<List<stateRes>>() {
            @Override
            public void onResponse(@NonNull Call<List<stateRes>> call, @NonNull Response<List<stateRes>> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                    //Log.i("TAG", "onResponse: "+response.body().size());
                } else {

                    db.error_popup(response.code(), response.message(), mContext);
                    Throwable t = new Throwable();
                    mHttpCallResponse.OnFailure(t);
                }
            }

            @Override
            public void onFailure(Call<List<stateRes>> call, Throwable t) {

                CommonMethods.alertMessage(mContext, "Try Again.");
                mHttpCallResponse.OnFailure(t);
            }
        });


    }

    public void getCityList(Activity act, stateReq state, final HttpCallResponse mHttpCallResponse) {
        mContext = act;
        ApiInterface mInterface = retrofit.create(ApiInterface.class);

        Call<List<cityRes>> mCall = mInterface.getCityList(state);

        mCall.enqueue(new Callback<List<cityRes>>() {
            @Override
            public void onResponse(@NonNull Call<List<cityRes>> call, @NonNull Response<List<cityRes>> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                    //Log.i("TAG", "onResponse: "+response.body().size());
                } else {

                    db.error_popup(response.code(), response.message(), mContext);
                    Throwable t = new Throwable();
                    mHttpCallResponse.OnFailure(t);
                }
            }

            @Override
            public void onFailure(Call<List<cityRes>> call, Throwable t) {

                CommonMethods.alertMessage(mContext, "Try Again.");
                mHttpCallResponse.OnFailure(t);
            }
        });


    }

    public void getDealerListPost(Activity act, dealerReq req, final HttpCallResponse mHttpCallResponse) {
        mContext = act;
        ApiInterface mInterface = retrofit.create(ApiInterface.class);

        Call<List<DealerName>> mCall = mInterface.getDealerName(req);

        mCall.enqueue(new Callback<List<DealerName>>() {
            @Override
            public void onResponse(@NonNull Call<List<DealerName>> call, @NonNull Response<List<DealerName>> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                    Log.i("TAG", "onResponse: " + response.body().size());
                } else {
                    db.error_popup(response.code(), response.message(), mContext);

                    Throwable t = new Throwable();
                    mHttpCallResponse.OnFailure(t);
                }
            }

            @Override
            public void onFailure(Call<List<DealerName>> call, Throwable t) {

                CommonMethods.alertMessage(mContext, "Try Again.");
                mHttpCallResponse.OnFailure(t);
            }
        });


    }



    public void getDealerPerformace(Activity act, String zone,String statecode,String citycode,String Friday,String Dealercode, final HttpCallResponse mHttpCallResponse) {

        mContext = act;
        ApiInterface mInterface = retrofit.create(ApiInterface.class);

        Call<DealerPerformanceResponse> mCall = mInterface.getDealerPerformance(zone,statecode,citycode,Friday,Dealercode);

        mCall.enqueue(new Callback<DealerPerformanceResponse>() {
            @Override
            public void onResponse(@NonNull Call<DealerPerformanceResponse> call, @NonNull Response<DealerPerformanceResponse> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                    //  Log.i("TAG", "onResponse: "+response.body().size());
                } else {

                    db.error_popup(response.code(), response.message(), mContext);

                    Throwable t = new Throwable();
                    mHttpCallResponse.OnFailure(t);
                }
            }

            @Override
            public void onFailure(Call<DealerPerformanceResponse> call, Throwable t) {

                CommonMethods.alertMessage(mContext, "Try Again.");
                mHttpCallResponse.OnFailure(t);
            }
        });


    }


    public void getChartData(Activity act, String zone, String statecode, String citycode, String Friday,
                             String dealercode, final HttpCallResponse mHttpCallResponse) {
        mContext = act;
        ApiInterface mInterface = retrofit.create(ApiInterface.class);

        Call<chartDataRes> mCall = mInterface.getChartData(zone, statecode, citycode, Friday, dealercode);

        mCall.enqueue(new Callback<chartDataRes>() {
            @Override
            public void onResponse(@NonNull Call<chartDataRes> call, @NonNull Response<chartDataRes> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                    //  Log.i("TAG", "onResponse: "+response.body().size());
                } else {

                    db.error_popup(response.code(), response.message(), mContext);

                    Throwable t = new Throwable();
                    mHttpCallResponse.OnFailure(t);
                }
            }

            @Override
            public void onFailure(Call<chartDataRes> call, Throwable t) {

                CommonMethods.alertMessage(mContext, "Try Again.");
                mHttpCallResponse.OnFailure(t);
            }
        });


    }

    public void getHighParam(Activity act, String zone, String statecode, String citycode, String Friday,
                             String dealercode, final HttpCallResponse mHttpCallResponse) {
        mContext = act;
        ApiInterface mInterface = retrofit.create(ApiInterface.class);

        Call<highparamRes> mCall = mInterface.getHighParam(zone, statecode, citycode, Friday, dealercode);

        mCall.enqueue(new Callback<highparamRes>() {
            @Override
            public void onResponse(@NonNull Call<highparamRes> call, @NonNull Response<highparamRes> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                    //  Log.i("TAG", "onResponse: "+response.body().size());
                } else {

                    db.error_popup(response.code(), response.message(), mContext);
                    Throwable t = new Throwable();
                    mHttpCallResponse.OnFailure(t);
                }
            }

            @Override
            public void onFailure(Call<highparamRes> call, Throwable t) {

                CommonMethods.alertMessage(mContext, "Try Again.");
                mHttpCallResponse.OnFailure(t);
            }
        });


    }

    public void getGraphData(Activity act, String zone, String statecode, String citycode, String Friday,
                             String dealercode, final HttpCallResponse mHttpCallResponse) {
        mContext = act;
        ApiInterface mInterface = retrofit.create(ApiInterface.class);
        Call<graphDataRes> mCall = mInterface.getGraphData(zone, statecode, citycode, Friday, dealercode);

        mCall.enqueue(new Callback<graphDataRes>() {
            @Override
            public void onResponse(@NonNull Call<graphDataRes> call, @NonNull Response<graphDataRes> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {
                    db.error_popup(response.code(), response.message(), mContext);
                    Throwable t = new Throwable();
                    mHttpCallResponse.OnFailure(t);
                }
            }

            @Override
            public void onFailure(Call<graphDataRes> call, Throwable t) {
                CommonMethods.alertMessage(mContext, "Try Again.");
                mHttpCallResponse.OnFailure(t);
            }
        });


    }


    public void getZoneList(Activity act, final HttpCallResponse mHttpCallResponse) {

        mContext = act;
        ApiInterface mInterFace = retrofit.create(ApiInterface.class);
        Call<List<String>> mCall = mInterFace.getZoneList();
        mCall.enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(@NonNull Call<List<String>> call, @NonNull Response<List<String>> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                    //Log.i("TAG", "onResponse: "+response.body().size());
                } else {
                    db.error_popup(response.code(), response.message(), mContext);
                    Throwable t = new Throwable();
                    mHttpCallResponse.OnFailure(t);
                }
            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {

                CommonMethods.alertMessage(mContext, "Try Again.");
                mHttpCallResponse.OnFailure(t);
            }
        });

    }

    public void getDurationList(Activity act, final HttpCallResponse mHttpCallResponse) {

        mContext = act;
        ApiInterface mInterFace = retrofit.create(ApiInterface.class);
        Call<List<String>> mCall = mInterFace.getDurationList();
        mCall.enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(@NonNull Call<List<String>> call, @NonNull Response<List<String>> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                    //Log.i("TAG", "onResponse: "+response.body().size());
                } else {

                    db.error_popup(response.code(), response.message(), mContext);

                    Throwable t = new Throwable();
                    mHttpCallResponse.OnFailure(t);
                }
            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {

                CommonMethods.alertMessage(mContext, "Try Again.");
                mHttpCallResponse.OnFailure(t);
            }
        });

    }


    public interface ApiInterface {

        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("franschisereport/sendemailreport")
        Call<DashboardEmailRes> getEmail(@Body DashboardEmailReq req);

        @Headers("Content-Type: application/json; charset=utf-8")
        @GET("franchise/dashboard")
        Call<List<dasboardRes>> getLeadStatus();

        @GET("franchise/dashboard")
        Call<List<AreaManager>> getLeadStatusStateHead();

        @GET("franchise/dashboard")
        Call<List<StateManager>> getLeadStatusZonalHead();

        @GET("franchise/GetDealersList")
        Call<List<DealerName>> getDealerName(@Query("Zone") String id);


        @POST("franchise/GetStateList")
        Call<List<stateRes>> getstate(@Body stateReq req);

        @POST("franchise/GetDealersList")
        Call<List<DealerName>> getDealerName(@Body dealerReq req);

        @POST("franchise/GetCityList")
        Call<List<cityRes>> getCityList(@Body stateReq req);

        @GET("franchise/GetZoneList")
        Call<List<String>> getZoneList();

        @GET("franschisereport/GetTwoMonthsFridays")
        Call<List<String>> getDurationList();


        @GET("franschisereport/piecharttargetscreen")
        Call<chartDataRes> getChartData(@Query("zone") String zone,
                                        @Query("statecode") String statecode,
                                        @Query("citycode") String citycode,
                                        @Query("Friday") String Friday,
                                        @Query("dealercode") String dealercode);

        @GET("franschisereport/bargraphtargetscreen")
        Call<graphDataRes> getGraphData(@Query("zone") String zone,
                                        @Query("statecode") String statecode,
                                        @Query("citycode") String citycode,
                                        @Query("Friday") String Friday,
                                        @Query("dealercode") String dealercode);

        @GET("franschisereport/parametertargetscreen")
        Call<highparamRes> getHighParam(@Query("zone") String zone,
                                        @Query("statecode") String statecode,
                                        @Query("citycode") String citycode,
                                        @Query("Friday") String Friday,
                                        @Query("dealercode") String dealercode);

        @GET("franschisereport/dealerlisttargetscreen")
        Call<DealerPerformanceResponse> getDealerPerformance(@Query("zone") String zone,
                                                             @Query("statecode") String statecode,
                                                             @Query("citycode") String citycode,
                                                             @Query("Friday") String Friday,
                                                             @Query("dealercode") String dealercode);

    }
}
