package com.mfcwl.mfc_dealer.retrofitconfig;

import com.mfcwl.mfc_dealer.ASMModel.signCityRes;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public class signcityService extends DasboardBaseService {


    public static void getCity(String name,final HttpCallResponse mHttpCallResponse) {

        ApiInterface mInterFace = retrofit.create(ApiInterface.class);

        Call<List<signCityRes>> mCall = mInterFace.getLeadStatus(name);
        mCall.enqueue(new Callback<List<signCityRes>>() {
            @Override
            public void onResponse(Call<List<signCityRes>> call, Response<List<signCityRes>> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<List<signCityRes>> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);

            }
        });

    }

    public interface ApiInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @GET("registration/citylist")
        Call<List<signCityRes>> getLeadStatus(@Query("state") String id);
    }
}
