package com.mfcwl.mfc_dealer.retrofitconfig;

import com.mfcwl.mfc_dealer.ASMModel.privatelCount;
import com.mfcwl.mfc_dealer.ASMModel.privatelCountRes;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class PrivateLCountServices extends DasboardBaseService {

    public static void privateCount(privatelCount req  ,final HttpCallResponse mHttpCallResponse) {

        ApiInterface mInterFace = retrofit.create(ApiInterface.class);
        Call<List<privatelCountRes>> mCall = mInterFace.getWarrantyStockInfo(req);
        mCall.enqueue(new Callback<List<privatelCountRes>>() {
            @Override
            public void onResponse(Call<List<privatelCountRes>> call, Response<List<privatelCountRes>> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }else {
                    int x = 0;
                }
            }

            @Override
            public void onFailure(Call<List<privatelCountRes>> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
                int x = 0;
            }
        });


    }

    public interface ApiInterface {


        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("franchise/privateleadcount")
        Call<List<privatelCountRes>> getWarrantyStockInfo(@Body privatelCount mReq);
    }
}
