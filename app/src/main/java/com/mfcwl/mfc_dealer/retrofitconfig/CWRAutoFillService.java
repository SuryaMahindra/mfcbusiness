package com.mfcwl.mfc_dealer.retrofitconfig;

import android.util.Log;

import com.mfcwl.mfc_dealer.ASMModel.AutoFillResModel;
import com.mfcwl.mfc_dealer.ASMModel.CWRAutoFillModel;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class CWRAutoFillService extends DasboardBaseService
{

    public void getAutoFillData(CWRAutoFillModel cwrAutoFillModel , HttpCallResponse mCallResponse){
        CWRAutoFillInterface cwrAutoFillInterface = retrofit.create(CWRAutoFillInterface.class);
        Call<AutoFillResModel> call = cwrAutoFillInterface.pushData(cwrAutoFillModel);
        call.enqueue(new Callback<AutoFillResModel>() {
            @Override
            public void onResponse(Call<AutoFillResModel> call, Response<AutoFillResModel> response) {
                if(response.isSuccessful()){
                    mCallResponse.OnSuccess(response);
                    Log.d("Success Message"," "+response);
                }
            }

            @Override
            public void onFailure(Call<AutoFillResModel> call, Throwable throwable) {
                mCallResponse.OnFailure(throwable);
                Log.d("Failure Message"," "+throwable);
            }
        });
    }





    public interface CWRAutoFillInterface
    {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("franschisereport/GetDealerData")
        Call<AutoFillResModel> pushData(@Body CWRAutoFillModel cwrAutoFillModel);


    }
}
