package com.mfcwl.mfc_dealer.retrofitconfig;

import android.content.Context;
import android.util.Log;

import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.StockModels.leadDasRes;
import com.mfcwl.mfc_dealer.StockModels.leadsDasRq;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class LeadDasboardFilterServices extends WebLeadBaseService {

    public static void leaddashservice(Context mContext, final leadsDasRq mWebLeadRequest, final HttpCallResponse mHttpCallResponse) {
        WebLeadsInterface mLeadsInterface = retrofit.create(WebLeadsInterface.class);

        Call<leadDasRes> mCall = mLeadsInterface.getWebLeads(mWebLeadRequest);

        mCall.enqueue(new Callback<leadDasRes>() {
            @Override
            public void onResponse(Call<leadDasRes> call, Response<leadDasRes> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                }
            }

            @Override
            public void onFailure(Call<leadDasRes> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
                Log.i("onFailure", "onFailure: "+t);
            }
        });

    }

    public interface WebLeadsInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("leadsdashboard")
        Call<leadDasRes> getWebLeads(@Body leadsDasRq mRequest);
    }
}