package com.mfcwl.mfc_dealer.retrofitconfig;

import android.app.Activity;

import com.mfcwl.mfc_dealer.ASMModel.MessageCenterRes;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public class MessageCenterServices extends DasboardBaseService {

public  static Activity mContext;
    public static void getMessageCenters(Activity act,String dealer_code,final HttpCallResponse mHttpCallResponse) {
        mContext=act;
        ApiInterface mInterFace = retrofit.create(ApiInterface.class);

        Call<MessageCenterRes> mCall = mInterFace.getLeadStatus(dealer_code);
        mCall.enqueue(new Callback<MessageCenterRes>() {
            @Override
            public void onResponse(Call<MessageCenterRes> call, Response<MessageCenterRes> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }else{

                    if (response.code() == 400) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 401) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 500) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 404) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 304) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 503) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 405) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    }

                    Throwable t = new Throwable ();
                    mHttpCallResponse.OnFailure(t);
                }
            }

            @Override
            public void onFailure(Call<MessageCenterRes> call, Throwable t) {

                CommonMethods.alertMessage(mContext,"Try Again.");
                mHttpCallResponse.OnFailure(t);

            }
        });

    }

    public interface ApiInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @GET("franschisereport/getmessagecentre")
        Call<MessageCenterRes> getLeadStatus(@Query("dealercode") String id);
    }
}
