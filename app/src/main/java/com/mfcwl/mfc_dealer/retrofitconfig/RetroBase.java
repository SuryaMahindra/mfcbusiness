package com.mfcwl.mfc_dealer.retrofitconfig;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.Global;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroBase {

    private static String BASE_URL = Global.dasboard;

    public static String URL_END_DISCUSSION_POINT = "franschisereport/insertremotedealervisitreport";
    protected static TokenInterceptor tokenInterceptor = new TokenInterceptor();

    private static Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create();
    private static HttpLoggingInterceptor logging = new HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BODY);
    private static OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(logging)
            .addInterceptor(tokenInterceptor)
            .build();
    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client)
            .build();
    public static RetroInterface retroInterface = retrofit.create(RetroInterface.class);




    private static class TokenInterceptor implements Interceptor {
        private TokenInterceptor() {

        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request initialRequest = chain.request();

            String sToken = "";
            if (sToken != null) {
                initialRequest = initialRequest.newBuilder()
                        // .addHeader("Accept", "application/json; charset=utf-8")
                        .addHeader("token", CommonMethods.getToken("token"))
                        .build();
            }

            Response response = chain.proceed(initialRequest);
            return response;
        }
    }
}
