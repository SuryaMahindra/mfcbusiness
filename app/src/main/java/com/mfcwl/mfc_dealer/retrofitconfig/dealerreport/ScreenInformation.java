package com.mfcwl.mfc_dealer.retrofitconfig.dealerreport;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ScreenInformation {

    @SerializedName("dealer_code")
    @Expose
    private String dealerCode = "";
    @SerializedName("sales_target")
    @Expose
    private String salesTarget = "";
    @SerializedName("stocks_target")
    @Expose
    private String stocksTarget = "";
    @SerializedName("store_space_occupied")
    @Expose
    private String storeSpaceOccupied = "";
    @SerializedName("store_space_occupied_percent")
    @Expose
    private String storeSpaceOccupiedPercent = "";
    @SerializedName("paid_up_stock")
    @Expose
    private String paidUpStock = "";
    @SerializedName("paid_up_stock_percentage")
    @Expose
    private String paidUpStockPercentage = "";
    @SerializedName("park_and_sell_stock")
    @Expose
    private String parkAndSellStock = "";
    @SerializedName("park_and_sell_stock_percentage")
    @Expose
    private String parkAndSellStockPercentage = "";
    @SerializedName("free_space")
    @Expose
    private String freeSpace = "";
    @SerializedName("free_space_percentage")
    @Expose
    private String freeSpacePercentage = "";
    @SerializedName("procurement_target_stocks")
    @Expose
    private String procurementTargetStocks = "";
    @SerializedName("procurement_target_iep")
    @Expose
    private String procurementTargetIep = "";
    @SerializedName("procurement_target_cpt")
    @Expose
    private String procurementTargetCpt = "";
    @SerializedName("stocks_added")
    @Expose
    private String stocksAdded = "";
    @SerializedName("bids_won")
    @Expose
    private String bidsWon = "";

    @SerializedName("live_auctions_iep")
    @Expose
    private String liveAuctionsIep = "";
    @SerializedName("auctions_bids_on_iep")
    @Expose
    private String auctionsBidsOnIep = "";
    @SerializedName("auctions_bids_won_iep")
    @Expose
    private String auctionsBidsWonIep = "";
    @SerializedName("leads_oms_iep")
    @Expose
    private String leadsOmsIep = "";
    @SerializedName("leads_actual_iep")
    @Expose
    private String leadsActualIep = "";
    @SerializedName("procured_oms_iep")
    @Expose
    private String procuredOmsIep = "";
    @SerializedName("procured_actual_iep")
    @Expose
    private String procuredActualIep = "";
    @SerializedName("live_auctions_cpt")
    @Expose
    private String liveAuctionsCpt = "";
    @SerializedName("auctions_bids_on_cpt")
    @Expose
    private String auctionsBidsOnCpt = "";
    @SerializedName("auctions_bids_won_cpt")
    @Expose
    private String auctionsBidsWonCpt = "";
    @SerializedName("leads_oms_cpt")
    @Expose
    private String leadsOmsCpt = "";
    @SerializedName("leads_actual_cpt")
    @Expose
    private String leadsActualCpt = "";
    @SerializedName("procured_oms_cpt")
    @Expose
    private String procuredOmsCpt = "";
    @SerializedName("procured_actual_cpt")
    @Expose
    private String procuredActualCpt = "";
    @SerializedName("meetings")
    @Expose
    private String meetings = "";
    @SerializedName("commercial_closed")
    @Expose
    private String commercialClosed = "";
    @SerializedName("vehicles_procured_partnership")
    @Expose
    private String vehiclesProcuredPartnership = "";
    @SerializedName("leads_target")
    @Expose
    private String leadsTarget = "";
    @SerializedName("leads_generated")
    @Expose
    private String leadsGenerated = "";
    @SerializedName("leads_walkin")
    @Expose
    private String leadsWalkin = "";
    @SerializedName("leads_walkin_cold")
    @Expose
    private String leadsWalkinCold = "";
    @SerializedName("warranty_units_target")
    @Expose
    private String warrantyUnitsTarget = "";
    @SerializedName("warranty_units_sold")
    @Expose
    private String warrantyUnitsSold = "";
    @SerializedName("warranty_units_sold_price")
    @Expose
    private String warrantyUnitsSoldPrice = "";
    @SerializedName("warranty_na_cases")
    @Expose
    private String warrantyNaCases = "";
    @SerializedName("warranty_na_cases_followed_up")
    @Expose
    private String warrantyNaCasesFollowedUp = "";

    @SerializedName("points_discussed")
    @Expose
    private String pointsDiscussed= "";

    @SerializedName("reasons_low_participation_iep")
    @Expose
    public String reasonslowparticipationiep = "";

    @SerializedName("reasons_low_participation_cpt")
    @Expose
    public String reasonslowparticipationcpt = "";

    @SerializedName("stocks_certified")
    @Expose
    public String stockscertified;

    @SerializedName("stocks_without_image")
    @Expose
    public String stockswithoutimage;


    public String getCurrentStockDealerShip() {
        return currentStockDealerShip;
    }

    public void setCurrentStockDealerShip(String currentStockDealerShip) {
        this.currentStockDealerShip = currentStockDealerShip;
    }

    @SerializedName("current_stock")
    @Expose
    public String currentStockDealerShip="";




    @SerializedName("royalty_collected")
    @Expose
    public String royaltycollected;

    public String getRetailsalestarget() {
        return retailsalestarget;
    }

    public void setRetailsalestarget(String retailsalestarget) {
        this.retailsalestarget = retailsalestarget;
    }

    @SerializedName("retail_sales_target")
    @Expose
    public String retailsalestarget;

    public String getStockscertified() {
        return stockscertified;
    }

    public void setStockscertified(String stockscertified) {
        this.stockscertified = stockscertified;
    }

    public String getStockswithoutimage() {
        return stockswithoutimage;
    }

    public void setStockswithoutimage(String stockswithoutimage) {
        this.stockswithoutimage = stockswithoutimage;
    }

    public String getRoyaltycollected() {
        return royaltycollected;
    }

    public void setRoyaltycollected(String royaltycollected) {
        this.royaltycollected = royaltycollected;
    }

    public String [] getStocksinrefurbishment() {
        return stocksinrefurbishment;
    }

    public void setStocksinrefurbishment(String [] stocksinrefurbishment) {
        this.stocksinrefurbishment = stocksinrefurbishment;
    }

    @SerializedName("stocks_in_refurbishment")
    @Expose
    private String [] stocksinrefurbishment;

    public String getReasonslowparticipationiep() {
        return reasonslowparticipationiep;
    }

    public void setReasonslowparticipationiep(String reasonslowparticipationiep) {
        this.reasonslowparticipationiep = reasonslowparticipationiep;
    }

    public String getReasonslowparticipationcpt() {
        return reasonslowparticipationcpt;
    }

    public void setReasonslowparticipationcpt(String reasonslowparticipationcpt) {
        this.reasonslowparticipationcpt = reasonslowparticipationcpt;
    }

    @SerializedName("bookings_in_hand")
    @Expose
    private String bookingsInHand;
    @SerializedName("finance_cases")
    @Expose
    private String financeCases;
    @SerializedName("finance_case_penetration")
    @Expose
    private Integer financeCasePenetration;

    public String getBookingsInHand() {
        return bookingsInHand;
    }

    public void setBookingsInHand(String bookingsInHand) {
        this.bookingsInHand = bookingsInHand;
    }

    public String getFinanceCases() {
        return financeCases;
    }

    public void setFinanceCases(String financeCases) {
        this.financeCases = financeCases;
    }

    public Integer getFinanceCasePenetration() {
        return financeCasePenetration;
    }

    public void setFinanceCasePenetration(Integer financeCasePenetration) {
        this.financeCasePenetration = financeCasePenetration;
    }

    public Integer getUpdateOnFinanceCases() {
        return updateOnFinanceCases;
    }

    public void setUpdateOnFinanceCases(Integer updateOnFinanceCases) {
        this.updateOnFinanceCases = updateOnFinanceCases;
    }

    public Integer getPendingRC90days() {
        return pendingRC90days;
    }

    public void setPendingRC90days(Integer pendingRC90days) {
        this.pendingRC90days = pendingRC90days;
    }

    public Integer getMiblCases() {
        return miblCases;
    }

    public void setMiblCases(Integer miblCases) {
        this.miblCases = miblCases;
    }

    @SerializedName("update_on_finance_cases")
    @Expose
    private Integer updateOnFinanceCases;
    @SerializedName("pending_RC_90days")
    @Expose
    private Integer pendingRC90days;
    @SerializedName("mibl_cases")
    @Expose
    private Integer miblCases;

    public String getReasonforloss() {
        return reasonforloss;
    }

    public void setReasonforloss(String reasonforloss) {
        this.reasonforloss = reasonforloss;
    }




    @SerializedName("reason_for_loss")
    @Expose
    private String reasonforloss= "";

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getSalesTarget() {
        return salesTarget;
    }

    public void setSalesTarget(String salesTarget) {
        this.salesTarget = salesTarget;
    }

    public String getStocksTarget() {
        return stocksTarget;
    }

    public void setStocksTarget(String stocksTarget) {
        this.stocksTarget = stocksTarget;
    }

    public String getStoreSpaceOccupied() {
        return storeSpaceOccupied;
    }

    public void setStoreSpaceOccupied(String storeSpaceOccupied) {
        this.storeSpaceOccupied = storeSpaceOccupied;
    }

    public String getStoreSpaceOccupiedPercent() {
        return storeSpaceOccupiedPercent;
    }

    public void setStoreSpaceOccupiedPercent(String storeSpaceOccupiedPercent) {
        this.storeSpaceOccupiedPercent = storeSpaceOccupiedPercent;
    }

    public String getPaidUpStock() {
        return paidUpStock;
    }

    public void setPaidUpStock(String paidUpStock) {
        this.paidUpStock = paidUpStock;
    }

    public String getPaidUpStockPercentage() {
        return paidUpStockPercentage;
    }

    public void setPaidUpStockPercentage(String paidUpStockPercentage) {
        this.paidUpStockPercentage = paidUpStockPercentage;
    }

    public String getParkAndSellStock() {
        return parkAndSellStock;
    }

    public void setParkAndSellStock(String parkAndSellStock) {
        this.parkAndSellStock = parkAndSellStock;
    }

    public String getParkAndSellStockPercentage() {
        return parkAndSellStockPercentage;
    }

    public void setParkAndSellStockPercentage(String parkAndSellStockPercentage) {
        this.parkAndSellStockPercentage = parkAndSellStockPercentage;
    }

    public String getFreeSpace() {
        return freeSpace;
    }

    public void setFreeSpace(String freeSpace) {
        this.freeSpace = freeSpace;
    }

    public String getFreeSpacePercentage() {
        return freeSpacePercentage;
    }

    public void setFreeSpacePercentage(String freeSpacePercentage) {
        this.freeSpacePercentage = freeSpacePercentage;
    }

    public String getProcurementTargetStocks() {
        return procurementTargetStocks;
    }

    public void setProcurementTargetStocks(String procurementTargetStocks) {
        this.procurementTargetStocks = procurementTargetStocks;
    }

    public String getProcurementTargetIep() {
        return procurementTargetIep;
    }

    public void setProcurementTargetIep(String procurementTargetIep) {
        this.procurementTargetIep = procurementTargetIep;
    }

    public String getProcurementTargetCpt() {
        return procurementTargetCpt;
    }

    public void setProcurementTargetCpt(String procurementTargetCpt) {
        this.procurementTargetCpt = procurementTargetCpt;
    }

    public String getStocksAdded() {
        return stocksAdded;
    }

    public void setStocksAdded(String stocksAdded) {
        this.stocksAdded = stocksAdded;
    }

    public String getBidsWon() {
        return bidsWon;
    }

    public void setBidsWon(String bidsWon) {
        this.bidsWon = bidsWon;
    }


    public String getLiveAuctionsIep() {
        return liveAuctionsIep;
    }

    public void setLiveAuctionsIep(String liveAuctionsIep) {
        this.liveAuctionsIep = liveAuctionsIep;
    }

    public String getAuctionsBidsOnIep() {
        return auctionsBidsOnIep;
    }

    public void setAuctionsBidsOnIep(String auctionsBidsOnIep) {
        this.auctionsBidsOnIep = auctionsBidsOnIep;
    }

    public String getAuctionsBidsWonIep() {
        return auctionsBidsWonIep;
    }

    public void setAuctionsBidsWonIep(String auctionsBidsWonIep) {
        this.auctionsBidsWonIep = auctionsBidsWonIep;
    }

    public String getLeadsOmsIep() {
        return leadsOmsIep;
    }

    public void setLeadsOmsIep(String leadsOmsIep) {
        this.leadsOmsIep = leadsOmsIep;
    }

    public String getLeadsActualIep() {
        return leadsActualIep;
    }

    public void setLeadsActualIep(String leadsActualIep) {
        this.leadsActualIep = leadsActualIep;
    }

    public String getProcuredOmsIep() {
        return procuredOmsIep;
    }

    public void setProcuredOmsIep(String procuredOmsIep) {
        this.procuredOmsIep = procuredOmsIep;
    }

    public String getProcuredActualIep() {
        return procuredActualIep;
    }

    public void setProcuredActualIep(String procuredActualIep) {
        this.procuredActualIep = procuredActualIep;
    }

    public String getLiveAuctionsCpt() {
        return liveAuctionsCpt;
    }

    public void setLiveAuctionsCpt(String liveAuctionsCpt) {
        this.liveAuctionsCpt = liveAuctionsCpt;
    }

    public String getAuctionsBidsOnCpt() {
        return auctionsBidsOnCpt;
    }

    public void setAuctionsBidsOnCpt(String auctionsBidsOnCpt) {
        this.auctionsBidsOnCpt = auctionsBidsOnCpt;
    }

    public String getAuctionsBidsWonCpt() {
        return auctionsBidsWonCpt;
    }

    public void setAuctionsBidsWonCpt(String auctionsBidsWonCpt) {
        this.auctionsBidsWonCpt = auctionsBidsWonCpt;
    }

    public String getLeadsOmsCpt() {
        return leadsOmsCpt;
    }

    public void setLeadsOmsCpt(String leadsOmsCpt) {
        this.leadsOmsCpt = leadsOmsCpt;
    }

    public String getLeadsActualCpt() {
        return leadsActualCpt;
    }

    public void setLeadsActualCpt(String leadsActualCpt) {
        this.leadsActualCpt = leadsActualCpt;
    }

    public String getProcuredOmsCpt() {
        return procuredOmsCpt;
    }

    public void setProcuredOmsCpt(String procuredOmsCpt) {
        this.procuredOmsCpt = procuredOmsCpt;
    }

    public String getProcuredActualCpt() {
        return procuredActualCpt;
    }

    public void setProcuredActualCpt(String procuredActualCpt) {
        this.procuredActualCpt = procuredActualCpt;
    }

    public String getMeetings() {
        return meetings;
    }

    public void setMeetings(String meetings) {
        this.meetings = meetings;
    }

    public String getCommercialClosed() {
        return commercialClosed;
    }

    public void setCommercialClosed(String commercialClosed) {
        this.commercialClosed = commercialClosed;
    }

    public String getVehiclesProcuredPartnership() {
        return vehiclesProcuredPartnership;
    }

    public void setVehiclesProcuredPartnership(String vehiclesProcuredPartnership) {
        this.vehiclesProcuredPartnership = vehiclesProcuredPartnership;
    }

    public String getLeadsTarget() {
        return leadsTarget;
    }

    public void setLeadsTarget(String leadsTarget) {
        this.leadsTarget = leadsTarget;
    }

    public String getLeadsGenerated() {
        return leadsGenerated;
    }

    public void setLeadsGenerated(String leadsGenerated) {
        this.leadsGenerated = leadsGenerated;
    }

    public String getLeadsWalkin() {
        return leadsWalkin;
    }

    public void setLeadsWalkin(String leadsWalkin) {
        this.leadsWalkin = leadsWalkin;
    }

    public String getLeadsWalkinCold() {
        return leadsWalkinCold;
    }

    public void setLeadsWalkinCold(String leadsWalkinCold) {
        this.leadsWalkinCold = leadsWalkinCold;
    }

    public String getWarrantyUnitsTarget() {
        return warrantyUnitsTarget;
    }

    public void setWarrantyUnitsTarget(String warrantyUnitsTarget) {
        this.warrantyUnitsTarget = warrantyUnitsTarget;
    }

    public String getWarrantyUnitsSold() {
        return warrantyUnitsSold;
    }

    public void setWarrantyUnitsSold(String warrantyUnitsSold) {
        this.warrantyUnitsSold = warrantyUnitsSold;
    }

    public String getWarrantyUnitsSoldPrice() {
        return warrantyUnitsSoldPrice;
    }

    public void setWarrantyUnitsSoldPrice(String warrantyUnitsSoldPrice) {
        this.warrantyUnitsSoldPrice = warrantyUnitsSoldPrice;
    }

    public String getWarrantyNaCases() {
        return warrantyNaCases;
    }

    public void setWarrantyNaCases(String warrantyNaCases) {
        this.warrantyNaCases = warrantyNaCases;
    }

    public String getWarrantyNaCasesFollowedUp() {
        return warrantyNaCasesFollowedUp;
    }

    public void setWarrantyNaCasesFollowedUp(String warrantyNaCasesFollowedUp) {
        this.warrantyNaCasesFollowedUp = warrantyNaCasesFollowedUp;
    }


    public String getPointsDiscussed() {
        return pointsDiscussed;
    }

    public void setPointsDiscussed(String pointsDiscussed) {
        this.pointsDiscussed = pointsDiscussed;
    }


    //R4

    @SerializedName("procurement_Xmart")
    @Expose
    private String procurementXmart;
    @SerializedName("self_procurement")
    @Expose
    private String selfProcurement;

    public String getProcurementXmart() {
        return procurementXmart;
    }

    public void setProcurementXmart(String procurementXmart) {
        this.procurementXmart = procurementXmart;
    }

    public String getSelfProcurement() {
        return selfProcurement;
    }

    public void setSelfProcurement(String selfProcurement) {
        this.selfProcurement = selfProcurement;
    }


    @SerializedName("vehicle_not_per_requirement_iep")
    @Expose
    private String vehicleNotPerRequirementIep;
    @SerializedName("negotiations_iep")
    @Expose
    private String negotiationsIep;
    public String getVehicleNotPerRequirementIep() {
        return vehicleNotPerRequirementIep;
    }

    public void setVehicleNotPerRequirementIep(String vehicleNotPerRequirementIep) {
        this.vehicleNotPerRequirementIep = vehicleNotPerRequirementIep;
    }

    public String getNegotiationsIep() {
        return negotiationsIep;
    }

    public void setNegotiationsIep(String negotiationsIep) {
        this.negotiationsIep = negotiationsIep;
    }


    @SerializedName("vehicle_not_per_requirement_cpt")
    @Expose
    private String vehicleNotPerRequirementCpt;
    @SerializedName("negotiations_cpt")
    @Expose
    private String negotiationsCpt;

    public String getVehicleNotPerRequirementCpt() {
        return vehicleNotPerRequirementCpt;
    }

    public void setVehicleNotPerRequirementCpt(String vehicleNotPerRequirementCpt) {
        this.vehicleNotPerRequirementCpt = vehicleNotPerRequirementCpt;
    }

    public String getNegotiationsCpt() {
        return negotiationsCpt;
    }

    public void setNegotiationsCpt(String negotiationsCpt) {
        this.negotiationsCpt = negotiationsCpt;
    }


    @SerializedName("qualilified_leads")
    @Expose
    private String qualilifiedLeads;
    @SerializedName("vehicles_closed")
    @Expose
    private String vehiclesClosed;
    @SerializedName("repeat_leads")
    @Expose
    private String repeatLeads;
    @SerializedName("walk_ins")
    @Expose
    private String walkIns;

    public String getQualilifiedLeads() {
        return qualilifiedLeads;
    }

    public void setQualilifiedLeads(String qualilifiedLeads) {
        this.qualilifiedLeads = qualilifiedLeads;
    }

    public String getVehiclesClosed() {
        return vehiclesClosed;
    }

    public void setVehiclesClosed(String vehiclesClosed) {
        this.vehiclesClosed = vehiclesClosed;
    }

    public String getRepeatLeads() {
        return repeatLeads;
    }

    public void setRepeatLeads(String repeatLeads) {
        this.repeatLeads = repeatLeads;
    }

    public String getWalkIns() {
        return walkIns;
    }

    public void setWalkIns(String walkIns) {
        this.walkIns = walkIns;
    }


    @SerializedName("booking_OMS")
    @Expose
    private String bookingOMS;
    @SerializedName("retail_sales")
    @Expose
    private String retailSales;
    @SerializedName("total_sales")
    @Expose
    private String totalSales;
    @SerializedName("conversion_OMS")
    @Expose
    private String conversionOMS;
    @SerializedName("royalty_amount")
    @Expose
    private String royaltyAmount;
    @SerializedName("aggregator_cases")
    @Expose
    private String aggregatorCases;

    public String getBookingOMS() {
        return bookingOMS;
    }

    public void setBookingOMS(String bookingOMS) {
        this.bookingOMS = bookingOMS;
    }

    public String getRetailSales() {
        return retailSales;
    }

    public void setRetailSales(String retailSales) {
        this.retailSales = retailSales;
    }

    public String getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(String totalSales) {
        this.totalSales = totalSales;
    }

    public String getConversionOMS() {
        return conversionOMS;
    }

    public void setConversionOMS(String conversionOMS) {
        this.conversionOMS = conversionOMS;
    }

    public String getRoyaltyAmount() {
        return royaltyAmount;
    }

    public void setRoyaltyAmount(String royaltyAmount) {
        this.royaltyAmount = royaltyAmount;
    }

    public String getAggregatorCases() {
        return aggregatorCases;
    }

    public void setAggregatorCases(String aggregatorCases) {
        this.aggregatorCases = aggregatorCases;
    }


}
