package com.mfcwl.mfc_dealer.retrofitconfig;

import android.content.Context;

import com.mfcwl.mfc_dealer.Model.PaymentReq;
import com.mfcwl.mfc_dealer.Model.PaymentRes;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class PaymentServices extends DasboardBaseService {

    public static void paymentReq(Context mContext, final PaymentReq req, final HttpCallResponse mHttpCallResponse) {
        paymentInterface mLeadsInterface = retrofit.create(paymentInterface.class);

        Call<PaymentRes> mCall = mLeadsInterface.getpayLeads(req);

        mCall.enqueue(new Callback<PaymentRes>() {
            @Override
            public void onResponse(Call<PaymentRes> call, Response<PaymentRes> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {

                    Throwable t =new Throwable();
                    mHttpCallResponse.OnFailure(t);

                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                }
            }

            @Override
            public void onFailure(Call<PaymentRes> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });

    }

    public interface paymentInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("dealer/GetDealerAccountTransactions")
        Call<PaymentRes> getpayLeads(@Body PaymentReq req);
    }
}