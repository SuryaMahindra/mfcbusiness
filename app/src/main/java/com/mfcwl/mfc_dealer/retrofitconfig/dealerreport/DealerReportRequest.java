package com.mfcwl.mfc_dealer.retrofitconfig.dealerreport;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DealerReportRequest {


    @SerializedName("dealer_code")
    public String dealerCode = "";
    @SerializedName("follow_up_date")
    public String followUpDate = "";
    @SerializedName("video_url")
    public String videoUrl = "";
    @SerializedName("points")
    public ArrayList<DiscussionPoint> points = new ArrayList<>();
    @SerializedName("is_operational")
    public String isOperational="";


}
