package com.mfcwl.mfc_dealer.retrofitconfig;

import android.content.Context;

import com.mfcwl.mfc_dealer.ASMModel.signupModel;
import com.mfcwl.mfc_dealer.ASMModel.signupRes;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class SignupServices extends DasboardBaseService {

    public static void signuService(Context mContext, final signupModel req, final HttpCallResponse mHttpCallResponse) {
        WebLeadsInterface mLeadsInterface = retrofit.create(WebLeadsInterface.class);

        Call<signupRes> mCall = mLeadsInterface.getWebLeads(req);

        mCall.enqueue(new Callback<signupRes>() {
            @Override
            public void onResponse(Call<signupRes> call, Response<signupRes> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {

                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                }
            }

            @Override
            public void onFailure(Call<signupRes> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });

    }

    public interface WebLeadsInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("registration/create")
        Call<signupRes> getWebLeads(@Body signupModel req);
    }
}