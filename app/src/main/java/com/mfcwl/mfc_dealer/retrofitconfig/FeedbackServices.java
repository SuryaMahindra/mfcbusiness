package com.mfcwl.mfc_dealer.retrofitconfig;

import android.content.Context;

import com.mfcwl.mfc_dealer.Model.feedbackReq;
import com.mfcwl.mfc_dealer.Model.feedbackRes;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class FeedbackServices extends DasboardBaseService {

    public static void feedbackService(Context mContext, final feedbackReq req, final HttpCallResponse mHttpCallResponse) {
        WebLeadsInterface mLeadsInterface = retrofit.create(WebLeadsInterface.class);

        Call<feedbackRes> mCall = mLeadsInterface.getWebLeads(req);

        mCall.enqueue(new Callback<feedbackRes>() {
            @Override
            public void onResponse(Call<feedbackRes> call, Response<feedbackRes> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {

                    Throwable t =new Throwable();
                    mHttpCallResponse.OnFailure(t);

                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                }
            }

            @Override
            public void onFailure(Call<feedbackRes> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });

    }

    public interface WebLeadsInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("dealer/dealerfeedback")
        Call<feedbackRes> getWebLeads(@Body feedbackReq req);
    }
}