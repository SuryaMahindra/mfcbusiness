package com.mfcwl.mfc_dealer.retrofitconfig;

import com.mfcwl.mfc_dealer.Activity.RDR.ScreenDataResponse;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public class RDRService extends DasboardBaseService {

    public static void fetchScreenData(String dealer_code ,final HttpCallResponse mHttpCallResponse){

       ApiInterface mInterFace = retrofit.create(ApiInterface.class);

       Call<ScreenDataResponse> mCall = mInterFace.getScreenData(dealer_code);

       mCall.enqueue(new Callback<ScreenDataResponse>() {
           @Override
           public void onFailure(@NotNull Call<ScreenDataResponse> call, @NotNull Throwable t) {
               mHttpCallResponse.OnFailure(t);
           }

           @Override
           public void onResponse(@NotNull Call<ScreenDataResponse> call, @NotNull Response<ScreenDataResponse> response) {
               if(response.isSuccessful()){
                   mHttpCallResponse.OnSuccess(response);
               }
           }
       });

    }



    public interface ApiInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @GET("franschisereport/GetInformationForScreensRemoteDealerVisit")
        Call<ScreenDataResponse> getScreenData(@Query("dealercode") String id);
    }
}
