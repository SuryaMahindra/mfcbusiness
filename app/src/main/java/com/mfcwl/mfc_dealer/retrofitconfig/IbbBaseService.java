package com.mfcwl.mfc_dealer.retrofitconfig;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mfcwl.mfc_dealer.Utility.Global_Urls;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class IbbBaseService {
    protected static final String BASE_URL = Global_Urls.ibbMFC;
   // protected static HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

    protected static TokenInterceptor tokenInterceptor = new TokenInterceptor();
    protected static OkHttpClient client = new OkHttpClient.Builder().addInterceptor(tokenInterceptor).build();
    protected static Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create();

    public static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();

    private static class TokenInterceptor implements Interceptor {


        private TokenInterceptor() {

        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request initialRequest = chain.request();

            String sToken = "";
            if (sToken != null) {
                initialRequest = initialRequest.newBuilder()
                        .addHeader("Accept", "application/json; charset=utf-8")
                        /*.addHeader("username", "mfc@ibb.com")
                        .addHeader("password", "dHk69ffu7ebP")*/
                        .build();
            }

            Response response = chain.proceed(initialRequest);
            return response;
        }
    }
}
