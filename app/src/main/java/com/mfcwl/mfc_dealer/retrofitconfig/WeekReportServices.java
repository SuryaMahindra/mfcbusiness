package com.mfcwl.mfc_dealer.retrofitconfig;

import android.app.Activity;

import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.weekReportModel;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public class WeekReportServices extends DasboardBaseService {

    public  static Activity act;

    public static void getWeekStatus(String dealer_code ,Activity activity,final HttpCallResponse mHttpCallResponse) {

        ApiInterface mInterFace = retrofit.create(ApiInterface.class);

        Call<List<weekReportModel>> mCall = mInterFace.getLeadStatus(dealer_code);
        mCall.enqueue(new Callback<List<weekReportModel>>() {
            @Override
            public void onResponse(Call<List<weekReportModel>> call, Response<List<weekReportModel>> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<List<weekReportModel>> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);

            }
        });

    }

    public interface ApiInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @GET("franschisereport/getduration")
        Call<List<weekReportModel>> getLeadStatus(@Query("dealercode") String id);
    }
}
