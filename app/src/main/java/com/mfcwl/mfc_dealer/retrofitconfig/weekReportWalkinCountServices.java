package com.mfcwl.mfc_dealer.retrofitconfig;

import android.content.Context;

import com.mfcwl.mfc_dealer.ASMModel.walkinCountRes;
import com.mfcwl.mfc_dealer.ASMModel.weekReportWalkinCountModel;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class weekReportWalkinCountServices extends WebLeadBaseService {

    public static void reqservice(Context mContext, final weekReportWalkinCountModel req, final HttpCallResponse mHttpCallResponse) {
        WebLeadsInterface mLeadsInterface = retrofit.create(WebLeadsInterface.class);

        Call<walkinCountRes> mCall = mLeadsInterface.getWebLeads(req);

        mCall.enqueue(new Callback<walkinCountRes>() {
            @Override
            public void onResponse(Call<walkinCountRes> call, Response<walkinCountRes> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {

                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                }
            }

            @Override
            public void onFailure(Call<walkinCountRes> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });

    }

    public interface WebLeadsInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("salesAndWalkinCount")
        Call<walkinCountRes> getWebLeads(@Body weekReportWalkinCountModel mRequest);
    }
}