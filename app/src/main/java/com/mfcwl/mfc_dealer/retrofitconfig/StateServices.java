package com.mfcwl.mfc_dealer.retrofitconfig;

import com.mfcwl.mfc_dealer.ASMModel.stateModel;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public class StateServices extends DasboardBaseService {
    public static void getStateList(final HttpCallResponse mHttpCallResponse) {
        StateServices.ApiInterface mInterFace = retrofit.create(StateServices.ApiInterface.class);
        Call<List<stateModel>> mCall = mInterFace.getLeadStatus();
        mCall.enqueue(new Callback<List<stateModel>>() {
            @Override
            public void onResponse(Call<List<stateModel>> call, Response<List<stateModel>> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }else {
                    int x = 0;
                }
            }

            @Override
            public void onFailure(Call<List<stateModel>> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
                int x = 0;
            }
        });


    }

    public interface ApiInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @GET("registration/statelist")
        Call<List<stateModel>> getLeadStatus();
    }
}
