package com.mfcwl.mfc_dealer.retrofitconfig.dealerreport;

import com.google.gson.annotations.SerializedName;

public class DealerReportResponse {

    @SerializedName("status")
    public String status = "";
    @SerializedName("message")
    public String message = "";
}
