package com.mfcwl.mfc_dealer.retrofitconfig.dealerreport;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DiscussionPoint {

    @SerializedName("action_item")
    @Expose
    private List<String> actionItem;
    @SerializedName("target_date")
    @Expose
    private String targetDate;
    @SerializedName("focus_area")
    @Expose
    private String focusArea;
    @SerializedName("sub_focus_area")
    @Expose
    private String subFocusArea;
    @SerializedName("responsibility")
    @Expose
    private String responsibility;

    @SerializedName("screenInformation")
    private ScreenInformation screenInformation;

    public List<String> getActionItem() {
        return actionItem;
    }

    public void setActionItem(List<String> actionItem) {
        this.actionItem = actionItem;
    }

    public String getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(String targetDate) {
        this.targetDate = targetDate;
    }

    public String getFocusArea() {
        return focusArea;
    }

    public void setFocusArea(String focusArea) {
        this.focusArea = focusArea;
    }

    public String getSubFocusArea() {
        return subFocusArea;
    }

    public void setSubFocusArea(String subFocusArea) {
        this.subFocusArea = subFocusArea;
    }

    public String getResponsibility() {
        return responsibility;
    }

    public void setResponsibility(String responsibility) {
        this.responsibility = responsibility;
    }


    public ScreenInformation getScreenInformation() {
        return screenInformation;
    }

    public void setScreenInformation(ScreenInformation screenInformation) {
        this.screenInformation = screenInformation;
    }
}
