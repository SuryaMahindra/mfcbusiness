package com.mfcwl.mfc_dealer.retrofitconfig;

import com.mfcwl.mfc_dealer.ASMModel.dasboardRes;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public class StockFilterApplyServices extends DasboardBaseService {
    public static void getLeadStatus(final HttpCallResponse mHttpCallResponse) {
        StockFilterApplyServices.ApiInterface mInterFace = retrofit.create(StockFilterApplyServices.ApiInterface.class);
        Call<List<dasboardRes>> mCall = mInterFace.getLeadStatus();
        mCall.enqueue(new Callback<List<dasboardRes>>() {
            @Override
            public void onResponse(Call<List<dasboardRes>> call, Response<List<dasboardRes>> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }else {
                    int x = 0;
                }
            }

            @Override
            public void onFailure(Call<List<dasboardRes>> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
                int x = 0;
            }
        });


    }

    public interface ApiInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @GET("dashboard")
        Call<List<dasboardRes>> getLeadStatus();
    }
}
