package com.mfcwl.mfc_dealer.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;

/**
 * Created by HP 240 G4 on 12-03-2018.
 */

public class ToolsModel {


    public JSONArray getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(JSONArray imageUrl) {
        this.imageUrl = imageUrl;
    }

    public JSONArray imageUrl;

    public String getCoverimage() {
        return coverimage;
    }

    public void setCoverimage(String coverimage) {
        this.coverimage = coverimage;
    }

    public String coverimage;

    @SerializedName("current_page")
    @Expose
    public Integer currentPage;
    @SerializedName("per_page")
    @Expose
    public Integer perPage;
    @SerializedName("total")
    @Expose
    public Integer total;

    public ToolsModel(String make, String variantModel, String regno) {
        this.make = make;
        this.variant = variantModel;
        this.registrationNumber = regno;
    }

    @SerializedName("data")

    public Integer getCurrentPage() {
        return currentPage;
    }


    @SerializedName("source")
    @Expose
    public String source;
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("featured_car_text")
    @Expose
    public Object featuredCarText;
    @SerializedName("posted_date")
    @Expose
    public String postedDate;
    @SerializedName("stock_age")
    @Expose
    public Integer stockAge;
    @SerializedName("make")
    @Expose
    public String make;
    @SerializedName("model")
    @Expose
    public String model;
    @SerializedName("variant")
    @Expose
    public String variant;
    @SerializedName("reg_month")
    @Expose
    public String regMonth;
    @SerializedName("reg_year")
    @Expose
    public Integer regYear;
    @SerializedName("registraion_city")
    @Expose
    public String registraionCity;
    @SerializedName("registration_number")
    @Expose
    public String registrationNumber;
    @SerializedName("colour")
    @Expose
    public String colour;
    @SerializedName("kilometer")
    @Expose
    public Integer kilometer;
    @SerializedName("owner")
    @Expose
    public Integer owner;
    @SerializedName("insurance")
    @Expose
    public String insurance;
    @SerializedName("insurance_exp_date")
    @Expose
    public String insuranceExpDate;
    @SerializedName("selling_price")
    @Expose
    public Integer sellingPrice;
    @SerializedName("CertifiedText")
    @Expose
    public String certifiedText;
    @SerializedName("certification_number")
    @Expose
    public String certificationNumber;
    @SerializedName("warranty")
    @Expose
    public String warranty;
    @SerializedName("photo_count")
    @Expose
    public Integer photoCount;
    @SerializedName("surveyor_code")
    @Expose
    public String surveyorCode;
    @SerializedName("surveyor_modified_date")
    @Expose
    public String surveyorModifiedDate;
    @SerializedName("surveyor_kilometer")
    @Expose
    public String surveyorKilometer;
    @SerializedName("surveyor_remark")
    @Expose
    public String surveyorRemark;
    @SerializedName("dealer_code")
    @Expose
    public String dealerCode;
    @SerializedName("isbooked")
    @Expose
    public String isbooked;
    @SerializedName("issold")
    @Expose
    public String issold;
    @SerializedName("IsDisplay")
    @Expose
    public String isDisplay;
    @SerializedName("IsOffload")
    @Expose
    public String isOffload;

    public String fuel_type;
    public String bought;
    public String refurbishment_cost;

    public String getIs_display() {
        return is_display;
    }

    public void setIs_display(String is_display) {
        this.is_display = is_display;
    }

    public String getIs_certified() {
        return is_certified;
    }

    public void setIs_certified(String is_certified) {
        this.is_certified = is_certified;
    }

    public String getIs_featured_car() {
        return is_featured_car;
    }

    public void setIs_featured_car(String is_featured_car) {
        this.is_featured_car = is_featured_car;
    }

    public String getIs_featured_car_admin() {
        return is_featured_car_admin;
    }

    public void setIs_featured_car_admin(String is_featured_car_admin) {
        this.is_featured_car_admin = is_featured_car_admin;
    }

    public String getProcurement_executive_name() {
        return procurement_executive_name;
    }

    public void setProcurement_executive_name(String procurement_executive_name) {
        this.procurement_executive_name = procurement_executive_name;
    }

    public String getPrivate_vehicle() {
        return private_vehicle;
    }

    public void setPrivate_vehicle(String private_vehicle) {
        this.private_vehicle = private_vehicle;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getFinance_required() {
        return finance_required;
    }

    public void setFinance_required(String finance_required) {
        this.finance_required = finance_required;
    }

    public String getManufacture_month() {
        return manufacture_month;
    }

    public void setManufacture_month(String manufacture_month) {
        this.manufacture_month = manufacture_month;
    }

    public String getSales_executive_id() {
        return sales_executive_id;
    }

    public void setSales_executive_id(String sales_executive_id) {
        this.sales_executive_id = sales_executive_id;
    }

    public String getSales_executive_name() {
        return sales_executive_name;
    }

    public void setSales_executive_name(String sales_executive_name) {
        this.sales_executive_name = sales_executive_name;
    }

    public String getManufacture_year() {
        return manufacture_year;
    }

    public void setManufacture_year(String manufacture_year) {
        this.manufacture_year = manufacture_year;
    }

    public String getActual_selling_price() {
        return actual_selling_price;
    }

    public void setActual_selling_price(String actual_selling_price) {
        this.actual_selling_price = actual_selling_price;
    }

    public String getTransmissionType() {
        return TransmissionType;
    }

    public void setTransmissionType(String transmissionType) {
        TransmissionType = transmissionType;
    }

    public String is_display;
    public String is_certified;
    public String is_featured_car;
    public String  is_featured_car_admin;
    public String procurement_executive_name;
    public String private_vehicle;
    public String comments;
    public String finance_required;
    public String manufacture_month;
    public String sales_executive_id;
    public String sales_executive_name;
    public String manufacture_year;
    public String actual_selling_price;
    public String CertifiedText;
    public String IsDisplay;
    public String TransmissionType;

    public String getFuel_type() {
        return fuel_type;
    }

    public void setFuel_type(String fuel_type) {
        this.fuel_type = fuel_type;
    }

    public String getBought() {
        return bought;
    }

    public void setBought(String bought) {
        this.bought = bought;
    }

    public String getRefurbishment_cost() {
        return refurbishment_cost;
    }

    public void setRefurbishment_cost(String refurbishment_cost) {
        this.refurbishment_cost = refurbishment_cost;
    }

    public String getDealer_price() {
        return dealer_price;
    }

    public void setDealer_price(String dealer_price) {
        this.dealer_price = dealer_price;
    }

    public String getProcurementExecId() {
        return ProcurementExecId;
    }

    public void setProcurementExecId(String procurementExecId) {
        ProcurementExecId = procurementExecId;
    }

    public String getCng_kit() {
        return cng_kit;
    }

    public void setCng_kit(String cng_kit) {
        this.cng_kit = cng_kit;
    }

    public String getChassis_number() {
        return chassis_number;
    }

    public void setChassis_number(String chassis_number) {
        this.chassis_number = chassis_number;
    }

    public String getEngine_number() {
        return engine_number;
    }

    public void setEngine_number(String engine_number) {
        this.engine_number = engine_number;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String dealer_price;
    public String ProcurementExecId;
    public String cng_kit;
    public String chassis_number;
    public String engine_number;
    public String images;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Object getFeaturedCarText() {
        return featuredCarText;
    }

    public void setFeaturedCarText(Object featuredCarText) {
        this.featuredCarText = featuredCarText;
    }

    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public Integer getStockAge() {
        return stockAge;
    }

    public void setStockAge(int stockAge) {
        this.stockAge = stockAge;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public String getRegMonth() {
        return regMonth;
    }

    public void setRegMonth(String regMonth) {
        this.regMonth = regMonth;
    }

    public Integer getRegYear() {
        return regYear;
    }

    public void setRegYear(int regYear) {
        this.regYear = regYear;
    }

    public String getRegistraionCity() {
        return registraionCity;
    }

    public void setRegistraionCity(String registraionCity) {
        this.registraionCity = registraionCity;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public Integer getKilometer() {
        return kilometer;
    }

    public void setKilometer(int kilometer) {
        this.kilometer = kilometer;
    }

    public Integer getOwner() {
        return owner;
    }

    public void setOwner(int owner) {
        this.owner = owner;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getInsuranceExpDate() {
        return insuranceExpDate;
    }

    public void setInsuranceExpDate(String insuranceExpDate) {
        this.insuranceExpDate = insuranceExpDate;
    }

    public Integer getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(int sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public String getCertifiedText() {
        return certifiedText;
    }

    public void setCertifiedText(String certifiedText) {
        this.certifiedText = certifiedText;
    }

    public String getCertificationNumber() {
        return certificationNumber;
    }

    public void setCertificationNumber(String certificationNumber) {
        this.certificationNumber = certificationNumber;
    }

    public String getWarranty() {
        return warranty;
    }

    public void setWarranty(String warranty) {
        this.warranty = warranty;
    }

    public Integer getPhotoCount() {
        return photoCount;
    }

    public void setPhotoCount(int photoCount) {
        this.photoCount = photoCount;
    }

    public String getSurveyorCode() {
        return surveyorCode;
    }

    public void setSurveyorCode(String surveyorCode) {
        this.surveyorCode = surveyorCode;
    }

    public String getSurveyorModifiedDate() {
        return surveyorModifiedDate;
    }

    public void setSurveyorModifiedDate(String surveyorModifiedDate) {
        this.surveyorModifiedDate = surveyorModifiedDate;
    }

    public String getSurveyorKilometer() {
        return surveyorKilometer;
    }

    public void setSurveyorKilometer(String surveyorKilometer) {
        this.surveyorKilometer = surveyorKilometer;
    }

    public String getSurveyorRemark() {
        return surveyorRemark;
    }

    public void setSurveyorRemark(String surveyorRemark) {
        this.surveyorRemark = surveyorRemark;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getIsbooked() {
        return isbooked;
    }

    public void setIsbooked(String isbooked) {
        this.isbooked = isbooked;
    }

    public String getIssold() {
        return issold;
    }

    public void setIssold(String issold) {
        this.issold = issold;
    }

    public String getIsDisplay() {
        return isDisplay;
    }

    public void setIsDisplay(String isDisplay) {
        this.isDisplay = isDisplay;
    }

    public String getIsOffload() {
        return isOffload;
    }

    public void setIsOffload(String isOffload) {
        this.isOffload = isOffload;
    }

    public ToolsModel(){
        
    }

}
