package com.mfcwl.mfc_dealer.Model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class DashboardModels extends ViewModel {

    // Create a LiveData with a String
    public  MutableLiveData<String> dashboardSwtich;

    public  MutableLiveData<String> getdashboardSwtich() {
        if (dashboardSwtich == null) {
            dashboardSwtich = new MutableLiveData<String>();
        }
        return dashboardSwtich;
    }

}
