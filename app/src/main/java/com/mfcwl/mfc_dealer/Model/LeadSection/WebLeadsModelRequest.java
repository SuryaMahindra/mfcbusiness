package com.mfcwl.mfc_dealer.Model.LeadSection;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WebLeadsModelRequest {


    @SerializedName("filter_by_fields")
    @Expose
    private String filterByFields;

    @SerializedName("page")
    @Expose
    private Integer page;
    @SerializedName("order_by")
    @Expose
    private String orderBy;
    @SerializedName("wherenotin")
    @Expose
    private List<WhereNotIn> wherenotin = null;
    @SerializedName("report_prefix")
    @Expose
    private String reportPrefix;

    public List<WebLeadsWhereor> getWhereOr() {
        return whereOr;
    }

    public void setWhereOr(List<WebLeadsWhereor> whereOr) {
        this.whereOr = whereOr;
    }

    @SerializedName("where_or")
    @Expose
    private List<WebLeadsWhereor> whereOr = null;

    @SerializedName("custom_where")
    @Expose
    private List<WebLeadsCustomWhere> customWhere = null;
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("tag")
    @Expose
    private String tag;
    @SerializedName("order_by_reverse")
    @Expose
    private String orderByReverse;
    @SerializedName("alias_fields")
    @Expose
    private String aliasFields;
    @SerializedName("per_page")
    @Expose
    private Integer perPage;
    @SerializedName("select_fields")
    @Expose
    private List<String> selectFields = null;
    @SerializedName("where_in")
    @Expose
    private List<WebleadsWhereIn> whereIn = null;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public List<WhereNotIn> getWherenotin() {
        return wherenotin;
    }

    public void setWherenotin(List<WhereNotIn> wherenotin) {
        this.wherenotin = wherenotin;
    }

    public List<WebLeadsCustomWhere> getCustomWhere() {
        return customWhere;
    }

    public void setCustomWhere(List<WebLeadsCustomWhere> customWhere) {
        this.customWhere = customWhere;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getOrderByReverse() {
        return orderByReverse;
    }

    public void setOrderByReverse(String orderByReverse) {
        this.orderByReverse = orderByReverse;
    }

    public String getAliasFields() {
        return aliasFields;
    }

    public void setAliasFields(String aliasFields) {
        this.aliasFields = aliasFields;
    }

    public Integer getPerPage() {
        return perPage;
    }

    public void setPerPage(Integer perPage) {
        this.perPage = perPage;
    }

    public List<String> getSelectFields() {
        return selectFields;
    }

    public void setSelectFields(List<String> selectFields) {
        this.selectFields = selectFields;
    }

    public List<WebleadsWhereIn> getWhereIn() {
        return whereIn;
    }

    public void setWhereIn(List<WebleadsWhereIn> whereIn) {
        this.whereIn = whereIn;
    }
    public String getFilterByFields() {
        return filterByFields;
    }
    public String getReportPrefix() {
        return reportPrefix;
    }

    public void setReportPrefix(String reportPrefix) {
        this.reportPrefix = reportPrefix;
    }

    public void setFilterByFields(String filterByFields) {
        this.filterByFields = filterByFields;
    }

}
