package com.mfcwl.mfc_dealer.Model;

public class LeadstatusModel {
    public String name;
    public boolean checked;

    public LeadstatusModel(String name, boolean checked) {
        this.name = name;
        this.checked = checked;

    }
}
