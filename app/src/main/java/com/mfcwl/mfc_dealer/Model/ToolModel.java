package com.mfcwl.mfc_dealer.Model;

import imageeditor.tools.ToolType;

public class ToolModel {

    public String mToolName;
    public int mToolIcon;
    public ToolType mToolType;

 public   ToolModel(String toolName, int toolIcon, ToolType toolType) {
        mToolName = toolName;
        mToolIcon = toolIcon;
        mToolType = toolType;
    }
}
