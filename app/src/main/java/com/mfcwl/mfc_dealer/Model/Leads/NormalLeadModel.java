package com.mfcwl.mfc_dealer.Model.Leads;

/**
 * Created by HP 240 G5 on 26-03-2018.
 */

public class NormalLeadModel {

    private String postedDate;
    private String leadDate;
    private String name;
    private String mobile;
    private String email;
    private String primary_make;
    private String primary_model;
    private String primary_variant;
    private String secondary_make;
    private String secondary_model;
    private String secondary_variant;
    private String regno;
    private String regmonth;
    private String regyear;
    private String color;
    private String kms;
    private String owners;
    private String register_city;
    private String price;
    private String status;
    private String remark;
    private String followUpDate;
    private String executiveName;
    private String dispatchId;
    private String mfgyear;
    private String leadid;

    public NormalLeadModel(String postedDate, String leadDate, String name, String mobile, String email, String primary_make, String primary_model, String primary_variant, String secondary_make, String secondary_model, String secondary_variant, String regno, String regmonth, String regyear, String color, String kms, String owners, String register_city, String price, String status, String remark, String followUpDate, String executiveName,String dispatchId,String mfgyear,String leadid) {
        this.postedDate = postedDate;
        this.leadDate = leadDate;
        this.name = name;
        this.mobile = mobile;
        this.email = email;
        this.primary_make = primary_make;
        this.primary_model = primary_model;
        this.primary_variant = primary_variant;
        this.secondary_make = secondary_make;
        this.secondary_model = secondary_model;
        this.secondary_variant = secondary_variant;
        this.regno = regno;
        this.regmonth = regmonth;
        this.regyear = regyear;
        this.color = color;
        this.kms = kms;
        this.owners = owners;
        this.register_city = register_city;
        this.price = price;
        this.status = status;
        this.remark = remark;
        this.followUpDate = followUpDate;
        this.executiveName = executiveName;
        this.dispatchId = dispatchId;
        this.mfgyear = mfgyear;
        this.leadid = leadid;
    }

    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public String getLeadDate() {
        return leadDate;
    }

    public void setLeadDate(String leadDate) {
        this.leadDate = leadDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPrimary_make() {
        return primary_make;
    }

    public void setPrimary_make(String primary_make) {
        this.primary_make = primary_make;
    }

    public String getPrimary_model() {
        return primary_model;
    }

    public void setPrimary_model(String primary_model) {
        this.primary_model = primary_model;
    }

    public String getPrimary_variant() {
        return primary_variant;
    }

    public void setPrimary_variant(String primary_variant) {
        this.primary_variant = primary_variant;
    }

    public String getSecondary_make() {
        return secondary_make;
    }

    public void setSecondary_make(String secondary_make) {
        this.secondary_make = secondary_make;
    }

    public String getSecondary_model() {
        return secondary_model;
    }

    public void setSecondary_model(String secondary_model) {
        this.secondary_model = secondary_model;
    }

    public String getSecondary_variant() {
        return secondary_variant;
    }

    public void setSecondary_variant(String secondary_variant) {
        this.secondary_variant = secondary_variant;
    }

    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getRegmonth() {
        return regmonth;
    }

    public void setRegmonth(String regmonth) {
        this.regmonth = regmonth;
    }

    public String getRegyear() {
        return regyear;
    }

    public void setRegyear(String regyear) {
        this.regyear = regyear;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getKms() {
        return kms;
    }

    public void setKms(String kms) {
        this.kms = kms;
    }

    public String getOwners() {
        return owners;
    }

    public void setOwners(String owners) {
        this.owners = owners;
    }

    public String getRegister_city() {
        return register_city;
    }

    public void setRegister_city(String register_city) {
        this.register_city = register_city;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getFollowUpDate() {
        return followUpDate;
    }

    public void setFollowUpDate(String followUpDate) {
        this.followUpDate = followUpDate;
    }

    public String getExecutiveName() {
        return executiveName;
    }

    public void setExecutiveName(String executiveName) {
        this.executiveName = executiveName;
    }


    public String getDispatchId() {
        return dispatchId;
    }

    public void setDispatchId(String dispatchId) {
        this.dispatchId = dispatchId;
    }


    public String getMfgyear() {
        return mfgyear;
    }

    public void setMfgyear(String mfgyear) {
        this.mfgyear = mfgyear;
    }

    public String getLeadid() {
        return leadid;
    }

    public void setLeadid(String leadid) {
        this.leadid = leadid;
    }
}
