package com.mfcwl.mfc_dealer.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class videoUrlReq {

    @SerializedName("stock_id")
    @Expose
    private String stockId;
    @SerializedName("dealer_code")
    @Expose
    private String dealerCode;
    @SerializedName("IsNonMFC")
    @Expose
    private String isNonMFC;
    @SerializedName("uploaded_from")
    @Expose
    private String uploadedFrom;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("uploaded_loc")
    @Expose
    private String uploadedLoc;
    @SerializedName("video_url")
    @Expose
    private String videoUrl;

    public String getStockId() {
        return stockId;
    }

    public void setStockId(String stockId) {
        this.stockId = stockId;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getIsNonMFC() {
        return isNonMFC;
    }

    public void setIsNonMFC(String isNonMFC) {
        this.isNonMFC = isNonMFC;
    }

    public String getUploadedFrom() {
        return uploadedFrom;
    }

    public void setUploadedFrom(String uploadedFrom) {
        this.uploadedFrom = uploadedFrom;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUploadedLoc() {
        return uploadedLoc;
    }

    public void setUploadedLoc(String uploadedLoc) {
        this.uploadedLoc = uploadedLoc;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }
}
