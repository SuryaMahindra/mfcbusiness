package com.mfcwl.mfc_dealer.Model.LeadSection;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PrivateLeadWhereIn {

    @SerializedName("column")
    @Expose
    private String column;
    @SerializedName("values")
    @Expose
    private List<String> values = null;

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

}