package com.mfcwl.mfc_dealer.Model.LeadSection;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WebleadsWhereIn {

    @SerializedName("column")
    @Expose
    private String column;
    @SerializedName("value")
    @Expose
    private List<String> value = null;

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public List<String> getValue() {
        return value;
    }

    public void setValue(List<String> value) {
        this.value = value;
    }

}