package com.mfcwl.mfc_dealer.Model.Leads;

/**
 * Created by HP 240 G5 on 26-03-2018.
 */

public class PrivateLeadModel {

    private String id;
    private String leadtype;
    private String leadtitle;
    private String leaddate;
    private String leadcat;
    private String leadtags;
    private String leadflag;
    private String customerid;
    private String custprefix;
    private String customername;
    private String custmobile;
    private String custemail;
    private String custcomments;
    private String custxtras;
    private String custprefcommuni;
    private String areacode;
    private String city;
    private String state;
    private String areamfcbranch;
    private String areapincode;
    private String stockid;
    private String stocktype;
    private String make;
    private String model;
    private String variant;
    private String stockmodelyear;
    private String regmonth;
    private String regyear;
    private String color;
    private String kms;
    private String owner;
    private String regno;
    private String regcity;
    private String stockvinno;
    private String insurancetype;
    private String stockinsurance;
    private String stockexpprice;
    private String dealerid;
    private String demandfordealer;
    private String dealerquote;
    private String dealercomments;
    private String leadstage;
    private String leadpriority;
    private String leadstatus;
    private String leadsource;
    private String marksource;
    private String markcompaign;
    private String markdevice;
    private String clientip;
    private String followdate;
    private String remark;
    private String associatelead;
    private String refid;
    private String refsrc;
    private String status;
    private String comments;
    private String createdby;
    private String updatedby;
    private String deletedat;
    private String createdat;
    private String updatedat;
    private String manufacmonth;
    private String exename;
    private String exeid;
    private String vehicletype;
    private String leaddate2;
    private String createbydevice;
    private String updatedbydevice;


    public PrivateLeadModel(String id, String leadtype, String leadtitle, String leaddate, String leadcat, String leadtags, String leadflag, String customerid, String custprefix, String customername, String custmobile, String custemail, String custcomments, String custxtras, String custprefcommuni, String areacode, String city, String state, String areamfcbranch, String areapincode, String stockid, String stocktype, String make, String model, String variant, String stockmodelyear, String regmonth, String regyear, String color, String kms, String owner, String regno, String regcity, String stockvinno, String insurancetype, String stockinsurance, String stockexpprice, String dealerid, String demandfordealer, String dealerquote, String dealercomments, String leadstage, String leadpriority, String leadstatus, String leadsource, String marksource, String markcompaign, String markdevice, String clientip, String followdate, String remark, String associatelead, String refid, String refsrc, String status, String comments, String createdby, String updatedby, String deletedat, String createdat, String updatedat, String manufacmonth, String exename, String exeid, String vehicletype, String leaddate2, String createbydevice, String updatedbydevice) {
        this.id = id;
        this.leadtype = leadtype;
        this.leadtitle = leadtitle;
        this.leaddate = leaddate;
        this.leadcat = leadcat;
        this.leadtags = leadtags;
        this.leadflag = leadflag;
        this.customerid = customerid;
        this.custprefix = custprefix;
        this.customername = customername;
        this.custmobile = custmobile;
        this.custemail = custemail;
        this.custcomments = custcomments;
        this.custxtras = custxtras;
        this.custprefcommuni = custprefcommuni;
        this.areacode = areacode;
        this.city = city;
        this.state = state;
        this.areamfcbranch = areamfcbranch;
        this.areapincode = areapincode;
        this.stockid = stockid;
        this.stocktype = stocktype;
        this.make = make;
        this.model = model;
        this.variant = variant;
        this.stockmodelyear = stockmodelyear;
        this.regmonth = regmonth;
        this.regyear = regyear;
        this.color = color;
        this.kms = kms;
        this.owner = owner;
        this.regno = regno;
        this.regcity = regcity;
        this.stockvinno = stockvinno;
        this.insurancetype = insurancetype;
        this.stockinsurance = stockinsurance;
        this.stockexpprice = stockexpprice;
        this.dealerid = dealerid;
        this.demandfordealer = demandfordealer;
        this.dealerquote = dealerquote;
        this.dealercomments = dealercomments;
        this.leadstage = leadstage;
        this.leadpriority = leadpriority;
        this.leadstatus = leadstatus;
        this.leadsource = leadsource;
        this.marksource = marksource;
        this.markcompaign = markcompaign;
        this.markdevice = markdevice;
        this.clientip = clientip;
        this.followdate = followdate;
        this.remark = remark;
        this.associatelead = associatelead;
        this.refid = refid;
        this.refsrc = refsrc;
        this.status = status;
        this.comments = comments;
        this.createdby = createdby;
        this.updatedby = updatedby;
        this.deletedat = deletedat;
        this.createdat = createdat;
        this.updatedat = updatedat;
        this.manufacmonth = manufacmonth;
        this.exename = exename;
        this.exeid = exeid;
        this.vehicletype = vehicletype;
        this.leaddate2 = leaddate2;
        this.createbydevice = createbydevice;
        this.updatedbydevice = updatedbydevice;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLeadtype() {
        return leadtype;
    }

    public void setLeadtype(String leadtype) {
        this.leadtype = leadtype;
    }

    public String getLeadtitle() {
        return leadtitle;
    }

    public void setLeadtitle(String leadtitle) {
        this.leadtitle = leadtitle;
    }

    public String getLeaddate() {
        return leaddate;
    }

    public void setLeaddate(String leaddate) {
        this.leaddate = leaddate;
    }

    public String getLeadcat() {
        return leadcat;
    }

    public void setLeadcat(String leadcat) {
        this.leadcat = leadcat;
    }

    public String getLeadtags() {
        return leadtags;
    }

    public void setLeadtags(String leadtags) {
        this.leadtags = leadtags;
    }

    public String getLeadflag() {
        return leadflag;
    }

    public void setLeadflag(String leadflag) {
        this.leadflag = leadflag;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public String getCustprefix() {
        return custprefix;
    }

    public void setCustprefix(String custprefix) {
        this.custprefix = custprefix;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getCustmobile() {
        return custmobile;
    }

    public void setCustmobile(String custmobile) {
        this.custmobile = custmobile;
    }

    public String getCustemail() {
        return custemail;
    }

    public void setCustemail(String custemail) {
        this.custemail = custemail;
    }

    public String getCustcomments() {
        return custcomments;
    }

    public void setCustcomments(String custcomments) {
        this.custcomments = custcomments;
    }

    public String getCustxtras() {
        return custxtras;
    }

    public void setCustxtras(String custxtras) {
        this.custxtras = custxtras;
    }

    public String getCustprefcommuni() {
        return custprefcommuni;
    }

    public void setCustprefcommuni(String custprefcommuni) {
        this.custprefcommuni = custprefcommuni;
    }

    public String getAreacode() {
        return areacode;
    }

    public void setAreacode(String areacode) {
        this.areacode = areacode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAreamfcbranch() {
        return areamfcbranch;
    }

    public void setAreamfcbranch(String areamfcbranch) {
        this.areamfcbranch = areamfcbranch;
    }

    public String getAreapincode() {
        return areapincode;
    }

    public void setAreapincode(String areapincode) {
        this.areapincode = areapincode;
    }

    public String getStockid() {
        return stockid;
    }

    public void setStockid(String stockid) {
        this.stockid = stockid;
    }

    public String getStocktype() {
        return stocktype;
    }

    public void setStocktype(String stocktype) {
        this.stocktype = stocktype;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public String getStockmodelyear() {
        return stockmodelyear;
    }

    public void setStockmodelyear(String stockmodelyear) {
        this.stockmodelyear = stockmodelyear;
    }

    public String getRegmonth() {
        return regmonth;
    }

    public void setRegmonth(String regmonth) {
        this.regmonth = regmonth;
    }

    public String getRegyear() {
        return regyear;
    }

    public void setRegyear(String regyear) {
        this.regyear = regyear;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getKms() {
        return kms;
    }

    public void setKms(String kms) {
        this.kms = kms;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getRegcity() {
        return regcity;
    }

    public void setRegcity(String regcity) {
        this.regcity = regcity;
    }

    public String getStockvinno() {
        return stockvinno;
    }

    public void setStockvinno(String stockvinno) {
        this.stockvinno = stockvinno;
    }

    public String getInsurancetype() {
        return insurancetype;
    }

    public void setInsurancetype(String insurancetype) {
        this.insurancetype = insurancetype;
    }

    public String getStockinsurance() {
        return stockinsurance;
    }

    public void setStockinsurance(String stockinsurance) {
        this.stockinsurance = stockinsurance;
    }

    public String getStockexpprice() {
        return stockexpprice;
    }

    public void setStockexpprice(String stockexpprice) {
        this.stockexpprice = stockexpprice;
    }

    public String getDealerid() {
        return dealerid;
    }

    public void setDealerid(String dealerid) {
        this.dealerid = dealerid;
    }

    public String getDemandfordealer() {
        return demandfordealer;
    }

    public void setDemandfordealer(String demandfordealer) {
        this.demandfordealer = demandfordealer;
    }

    public String getDealerquote() {
        return dealerquote;
    }

    public void setDealerquote(String dealerquote) {
        this.dealerquote = dealerquote;
    }

    public String getDealercomments() {
        return dealercomments;
    }

    public void setDealercomments(String dealercomments) {
        this.dealercomments = dealercomments;
    }

    public String getLeadstage() {
        return leadstage;
    }

    public void setLeadstage(String leadstage) {
        this.leadstage = leadstage;
    }

    public String getLeadpriority() {
        return leadpriority;
    }

    public void setLeadpriority(String leadpriority) {
        this.leadpriority = leadpriority;
    }

    public String getLeadstatus() {
        return leadstatus;
    }

    public void setLeadstatus(String leadstatus) {
        this.leadstatus = leadstatus;
    }

    public String getLeadsource() {
        return leadsource;
    }

    public void setLeadsource(String leadsource) {
        this.leadsource = leadsource;
    }

    public String getMarksource() {
        return marksource;
    }

    public void setMarksource(String marksource) {
        this.marksource = marksource;
    }

    public String getMarkcompaign() {
        return markcompaign;
    }

    public void setMarkcompaign(String markcompaign) {
        this.markcompaign = markcompaign;
    }

    public String getMarkdevice() {
        return markdevice;
    }

    public void setMarkdevice(String markdevice) {
        this.markdevice = markdevice;
    }

    public String getClientip() {
        return clientip;
    }

    public void setClientip(String clientip) {
        this.clientip = clientip;
    }

    public String getFollowdate() {
        return followdate;
    }

    public void setFollowdate(String followdate) {
        this.followdate = followdate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAssociatelead() {
        return associatelead;
    }

    public void setAssociatelead(String associatelead) {
        this.associatelead = associatelead;
    }

    public String getRefid() {
        return refid;
    }

    public void setRefid(String refid) {
        this.refid = refid;
    }

    public String getRefsrc() {
        return refsrc;
    }

    public void setRefsrc(String refsrc) {
        this.refsrc = refsrc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getUpdatedby() {
        return updatedby;
    }

    public void setUpdatedby(String updatedby) {
        this.updatedby = updatedby;
    }

    public String getDeletedat() {
        return deletedat;
    }

    public void setDeletedat(String deletedat) {
        this.deletedat = deletedat;
    }

    public String getCreatedat() {
        return createdat;
    }

    public void setCreatedat(String createdat) {
        this.createdat = createdat;
    }

    public String getUpdatedat() {
        return updatedat;
    }

    public void setUpdatedat(String updatedat) {
        this.updatedat = updatedat;
    }

    public String getManufacmonth() {
        return manufacmonth;
    }

    public void setManufacmonth(String manufacmonth) {
        this.manufacmonth = manufacmonth;
    }

    public String getExename() {
        return exename;
    }

    public void setExename(String exename) {
        this.exename = exename;
    }

    public String getExeid() {
        return exeid;
    }

    public void setExeid(String exeid) {
        this.exeid = exeid;
    }

    public String getVehicletype() {
        return vehicletype;
    }

    public void setVehicletype(String vehicletype) {
        this.vehicletype = vehicletype;
    }

    public String getLeaddate2() {
        return leaddate2;
    }

    public void setLeaddate2(String leaddate2) {
        this.leaddate2 = leaddate2;
    }

    public String getCreatebydevice() {
        return createbydevice;
    }

    public void setCreatebydevice(String createbydevice) {
        this.createbydevice = createbydevice;
    }

    public String getUpdatedbydevice() {
        return updatedbydevice;
    }

    public void setUpdatedbydevice(String updatedbydevice) {
        this.updatedbydevice = updatedbydevice;
    }
}
