package com.mfcwl.mfc_dealer.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentData {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("dealer_code")
    @Expose
    private String dealerCode;
    @SerializedName("amount")
    @Expose
    private Float amount;
    @SerializedName("erp_receipt_no")
    @Expose
    private String erpReceiptNo;
    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("operation_status")
    @Expose
    private String operationStatus;
    @SerializedName("operation")
    @Expose
    private Boolean operation;
    @SerializedName("payment_gateway_transaction_id")
    @Expose
    private String paymentGatewayTransactionId;
    @SerializedName("payment_gateway_transaction_date")
    @Expose
    private String paymentGatewayTransactionDate;
    @SerializedName("transaction_type")
    @Expose
    private String transactionType;
    @SerializedName("pending_sap_post")
    @Expose
    private String pendingSapPost;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getErpReceiptNo() {
        return erpReceiptNo;
    }

    public void setErpReceiptNo(String erpReceiptNo) {
        this.erpReceiptNo = erpReceiptNo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOperationStatus() {
        return operationStatus;
    }

    public void setOperationStatus(String operationStatus) {
        this.operationStatus = operationStatus;
    }

    public Boolean getOperation() {
        return operation;
    }

    public void setOperation(Boolean operation) {
        this.operation = operation;
    }

    public String getPaymentGatewayTransactionId() {
        return paymentGatewayTransactionId;
    }

    public void setPaymentGatewayTransactionId(String paymentGatewayTransactionId) {
        this.paymentGatewayTransactionId = paymentGatewayTransactionId;
    }

    public String getPaymentGatewayTransactionDate() {
        return paymentGatewayTransactionDate;
    }

    public void setPaymentGatewayTransactionDate(String paymentGatewayTransactionDate) {
        this.paymentGatewayTransactionDate = paymentGatewayTransactionDate;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getPendingSapPost() {
        return pendingSapPost;
    }

    public void setPendingSapPost(String pendingSapPost) {
        this.pendingSapPost = pendingSapPost;
    }

}
