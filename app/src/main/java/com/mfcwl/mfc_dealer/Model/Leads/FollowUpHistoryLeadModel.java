package com.mfcwl.mfc_dealer.Model.Leads;

/**
 * Created by HP 240 G5 on 27-03-2018.
 */

public class FollowUpHistoryLeadModel {

    private String folowupdate;
    private String folowupstatus;
    private String folowupremarks;
    private String folowupcreated;


    public FollowUpHistoryLeadModel(String folowupdate, String folowupstatus, String folowupremarks,String folowupcreated) {
        this.folowupdate = folowupdate;
        this.folowupstatus = folowupstatus;
        this.folowupremarks = folowupremarks;
        this.folowupcreated = folowupcreated;
    }

    public String getFolowupdate() {
        return folowupdate;
    }

    public void setFolowupdate(String folowupdate) {
        this.folowupdate = folowupdate;
    }

    public String getFolowupstatus() {
        return folowupstatus;
    }

    public void setFolowupstatus(String folowupstatus) {
        this.folowupstatus = folowupstatus;
    }

    public String getFolowupremarks() {
        return folowupremarks;
    }

    public void setFolowupremarks(String folowupremarks) {
        this.folowupremarks = folowupremarks;
    }


    public String getFolowupcreated() {
        return folowupcreated;
    }

    public void setFolowupcreated(String folowupcreated) {
        this.folowupcreated = folowupcreated;
    }
}
