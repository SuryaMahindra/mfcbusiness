package com.mfcwl.mfc_dealer.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class feedbackReq {

    @SerializedName("dealer_code")
    @Expose
    private String dealerCode;
    @SerializedName("feedback")
    @Expose
    private String feedback;
    @SerializedName("image1")
    @Expose
    private String image;

    @SerializedName("image2")
    @Expose
    private String image2;

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getImage4() {
        return image4;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }

    @SerializedName("image3")
    @Expose
    private String image3;

    @SerializedName("image4")
    @Expose
    private String image4;

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
