package com.mfcwl.mfc_dealer.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class weekRes {
    @SerializedName("data")
    @Expose
    private List<weekResData> data = null;

    public List<weekResData> getData() {
        return data;
    }

    public void setData(List<weekResData> data) {
        this.data = data;
    }

}




