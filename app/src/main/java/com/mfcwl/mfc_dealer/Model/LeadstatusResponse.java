package com.mfcwl.mfc_dealer.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LeadstatusResponse {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("leadstatus")
    @Expose
    private List<Leadstatus> leadstatus = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Leadstatus> getLeadstatus() {
        return leadstatus;
    }

    public void setLeadstatus(List<Leadstatus> leadstatus) {
        this.leadstatus = leadstatus;
    }
}
