package com.mfcwl.mfc_dealer.Model.LeadSection;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WhereNotIn{
    @SerializedName("column")
    public String column;

    @SerializedName("value")
    public List<String> value;

}
