package com.mfcwl.mfc_dealer.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppVersionResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("is_uptodate")
    @Expose
    private Boolean isUptodate;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("url")
    @Expose
    private String url;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getIsUptodate() {
        return isUptodate;
    }

    public void setIsUptodate(Boolean isUptodate) {
        this.isUptodate = isUptodate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}