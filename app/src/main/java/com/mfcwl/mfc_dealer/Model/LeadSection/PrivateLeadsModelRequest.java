package com.mfcwl.mfc_dealer.Model.LeadSection;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PrivateLeadsModelRequest {

    @SerializedName("PageItems")
    @Expose
    private String pageItems;
    @SerializedName("Page")
    @Expose
    private String page;
    @SerializedName("wherein")
    @Expose
    private List<PrivateLeadWhereIn> wherein = null;
    @SerializedName("OrderBy")
    @Expose
    private String orderBy;
    @SerializedName("OrderByReverse")
    @Expose
    private String orderByReverse;
    @SerializedName("where_or")
    @Expose
    private List<PrivateLeadsWhereor> whereOr = null;
    @SerializedName("where")
    @Expose
    private List<PrivateLeadWhere> where = null;

    public String getPageItems() {
        return pageItems;
    }

    public void setPageItems(String pageItems) {
        this.pageItems = pageItems;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public List<PrivateLeadWhereIn> getWherein() {
        return wherein;
    }

    public void setWherein(List<PrivateLeadWhereIn> wherein) {
        this.wherein = wherein;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getOrderByReverse() {
        return orderByReverse;
    }

    public void setOrderByReverse(String orderByReverse) {
        this.orderByReverse = orderByReverse;
    }

    public List<PrivateLeadsWhereor> getWhereOr() {
        return whereOr;
    }

    public void setWhereOr(List<PrivateLeadsWhereor> whereOr) {
        this.whereOr = whereOr;
    }

    public List<PrivateLeadWhere> getWhere() {
        return where;
    }

    public void setWhere(List<PrivateLeadWhere> where) {
        this.where = where;
    }

}