package com.mfcwl.mfc_dealer.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PaymentReq {

    @SerializedName("page")
    @Expose
    private String page;
    @SerializedName("pageitems")
    @Expose
    private String pageitems;
    @SerializedName("where")
    @Expose
    private List<payWhere> where = null;
    @SerializedName("DateFrom")
    @Expose
    private String dateFrom;
    @SerializedName("DateTo")
    @Expose
    private String dateTo;
    @SerializedName("DateField")
    @Expose
    private String dateField;

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getPageitems() {
        return pageitems;
    }

    public void setPageitems(String pageitems) {
        this.pageitems = pageitems;
    }

    public List<payWhere> getWhere() {
        return where;
    }

    public void setWhere(List<payWhere> where) {
        this.where = where;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getDateField() {
        return dateField;
    }

    public void setDateField(String dateField) {
        this.dateField = dateField;
    }
}
