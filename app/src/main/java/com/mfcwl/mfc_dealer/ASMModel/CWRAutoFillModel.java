package com.mfcwl.mfc_dealer.ASMModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class CWRAutoFillModel
{
        @SerializedName("dealer_code")
        @Expose
        private String dealerCode;
        @SerializedName("dealer_type")
        @Expose
        private String dealerType;
        @SerializedName("from_date")
        @Expose
        private String fromDate;
        @SerializedName("to_date")
        @Expose
        private String toDate;

    public CWRAutoFillModel(String dealerCode, String dealerType, String fromDate, String toDate)
    {
        this.dealerCode = dealerCode;
        this.dealerType = dealerType;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    public String getDealerCode() {
            return dealerCode;
        }

        public void setDealerCode(String dealerCode) {
            this.dealerCode = dealerCode;
        }

        public String getDealerType() {
            return dealerType;
        }

        public void setDealerType(String dealerType) {
            this.dealerType = dealerType;
        }

        public String getFromDate() {
            return fromDate;
        }

        public void setFromDate(String fromDate) {
            this.fromDate = fromDate;
        }

        public String getToDate() {
            return toDate;
        }

        public void setToDate(String toDate) {
            this.toDate = toDate;
        }




    }
