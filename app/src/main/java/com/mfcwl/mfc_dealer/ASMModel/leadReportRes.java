package com.mfcwl.mfc_dealer.ASMModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class leadReportRes {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("status_code")
    @Expose
    private Integer statusCode;
    @SerializedName("web_leads_for_the_month")
    @Expose
    private Integer webLeadsForTheMonth;
    @SerializedName("open_web_leads")
    @Expose
    private Integer openWebLeads;
    @SerializedName("no_of_follow_ups_done_once")
    @Expose
    private Integer noOfFollowUpsDoneOnce;
    @SerializedName("no_of_follow_ups_done_twice")
    @Expose
    private Integer noOfFollowUpsDoneTwice;
    @SerializedName("no_of_follow_ups_done_more_than_2times")
    @Expose
    private Integer noOfFollowUpsDoneMoreThan2times;
    @SerializedName("message")
    @Expose
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getWebLeadsForTheMonth() {
        return webLeadsForTheMonth;
    }

    public void setWebLeadsForTheMonth(Integer webLeadsForTheMonth) {
        this.webLeadsForTheMonth = webLeadsForTheMonth;
    }

    public Integer getOpenWebLeads() {
        return openWebLeads;
    }

    public void setOpenWebLeads(Integer openWebLeads) {
        this.openWebLeads = openWebLeads;
    }

    public Integer getNoOfFollowUpsDoneOnce() {
        return noOfFollowUpsDoneOnce;
    }

    public void setNoOfFollowUpsDoneOnce(Integer noOfFollowUpsDoneOnce) {
        this.noOfFollowUpsDoneOnce = noOfFollowUpsDoneOnce;
    }

    public Integer getNoOfFollowUpsDoneTwice() {
        return noOfFollowUpsDoneTwice;
    }

    public void setNoOfFollowUpsDoneTwice(Integer noOfFollowUpsDoneTwice) {
        this.noOfFollowUpsDoneTwice = noOfFollowUpsDoneTwice;
    }

    public Integer getNoOfFollowUpsDoneMoreThan2times() {
        return noOfFollowUpsDoneMoreThan2times;
    }

    public void setNoOfFollowUpsDoneMoreThan2times(Integer noOfFollowUpsDoneMoreThan2times) {
        this.noOfFollowUpsDoneMoreThan2times = noOfFollowUpsDoneMoreThan2times;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
