package com.mfcwl.mfc_dealer.ASMModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DashboardEmailReq {

    @SerializedName("to_date")
    @Expose
    private String toDate;
    @SerializedName("dealer_type")
    @Expose
    private String dealerType;
    @SerializedName("dealer_code")
    @Expose
    private String dealerCode;
    @SerializedName("from_date")
    @Expose
    private String fromDate;

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getDealerType() {
        return dealerType;
    }

    public void setDealerType(String dealerType) {
        this.dealerType = dealerType;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }
}
