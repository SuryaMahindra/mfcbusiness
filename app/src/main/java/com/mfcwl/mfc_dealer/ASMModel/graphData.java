package com.mfcwl.mfc_dealer.ASMModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class graphData {

    @SerializedName("month")
    @Expose
    private String month;
    @SerializedName("red")
    @Expose
    private Integer red;
    @SerializedName("amber")
    @Expose
    private Integer amber;
    @SerializedName("green")
    @Expose
    private Integer green;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Integer getRed() {
        return red;
    }

    public void setRed(Integer red) {
        this.red = red;
    }

    public Integer getAmber() {
        return amber;
    }

    public void setAmber(Integer amber) {
        this.amber = amber;
    }

    public Integer getGreen() {
        return green;
    }

    public void setGreen(Integer green) {
        this.green = green;
    }

}
