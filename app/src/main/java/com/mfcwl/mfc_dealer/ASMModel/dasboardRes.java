package com.mfcwl.mfc_dealer.ASMModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class dasboardRes {

    @SerializedName("dealer_code")
    @Expose
    private String dealerCode = "";
    @SerializedName("dealer_name")
    @Expose
    private String dealerName = "";
    @SerializedName("dealer_type")
    @Expose
    private String dealerType = "";
    @SerializedName("balance")
    @Expose
    private Object balance;
    @SerializedName("type")
    @Expose
    private String type = "";
    @SerializedName("warrantypunch")
    @Expose
    private Object warrantypunch;

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDealerType() {
        return dealerType;
    }

    public void setDealerType(String dealerType) {
        this.dealerType = dealerType;
    }

    public Object getBalance() {
        return balance;
    }

    public void setBalance(Object balance) {
        this.balance = balance;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getWarrantypunch() {
        return warrantypunch;
    }

    public void setWarrantypunch(Object warrantypunch) {
        this.warrantypunch = warrantypunch;
    }

}
