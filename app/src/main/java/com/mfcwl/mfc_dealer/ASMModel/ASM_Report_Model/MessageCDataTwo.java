package com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mfcwl.mfc_dealer.ASMModel.MsgFilterData;

import java.util.List;

public class MessageCDataTwo {

    @SerializedName("name")
    @Expose
    private String name;

    public String getColorcode() {
        return colorcode;
    }

    public void setColorcode(String colorcode) {
        this.colorcode = colorcode;
    }

    @SerializedName("colorcode")
    @Expose
    private String colorcode;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("filter")
    @Expose
    private List<MsgFilterData> filter = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<MsgFilterData> getFilter() {
        return filter;
    }

    public void setFilter(List<MsgFilterData> filter) {
        this.filter = filter;
    }


}
