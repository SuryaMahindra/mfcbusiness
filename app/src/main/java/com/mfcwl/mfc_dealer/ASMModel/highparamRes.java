package com.mfcwl.mfc_dealer.ASMModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class highparamRes {

    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private List<highParaData> data = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<highParaData> getData() {
        return data;
    }

    public void setData(List<highParaData> data) {
        this.data = data;
    }
}
