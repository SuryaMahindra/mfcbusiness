package com.mfcwl.mfc_dealer.ASMModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class highParaData {


    @SerializedName("parameter_name")
    @Expose
    private String parameterName;
    @SerializedName("red")
    @Expose
    private Integer red;
    @SerializedName("amber")
    @Expose
    private Integer amber;
    @SerializedName("green")
    @Expose
    private Integer green;

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public Integer getRed() {
        return red;
    }

    public void setRed(Integer red) {
        this.red = red;
    }

    public Integer getAmber() {
        return amber;
    }

    public void setAmber(Integer amber) {
        this.amber = amber;
    }

    public Integer getGreen() {
        return green;
    }

    public void setGreen(Integer green) {
        this.green = green;
    }

}
