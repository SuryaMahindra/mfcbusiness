package com.mfcwl.mfc_dealer.ASMModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class chartDataModel {

    @SerializedName("Red")
    @Expose
    private Integer red;
    @SerializedName("Amber")
    @Expose
    private Integer amber;
    @SerializedName("Green")
    @Expose
    private Integer green;

    public Integer getRed() {
        return red;
    }

    public void setRed(Integer red) {
        this.red = red;
    }

    public Integer getAmber() {
        return amber;
    }

    public void setAmber(Integer amber) {
        this.amber = amber;
    }

    public Integer getGreen() {
        return green;
    }

    public void setGreen(Integer green) {
        this.green = green;
    }
}
