package com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mfcwl.mfc_dealer.AddStockModel.dvrPointData;

import java.util.List;

public class SubmittedDVReport {
    @SerializedName("id")
    Integer id;
    @SerializedName("dealer_code")
    String dealer_code;
    @SerializedName("created_on")
    String created_on;

    String points_discussed;

    String action_plan;
    @SerializedName("latitude")
    String latitude;
    @SerializedName("longitude")
    String longitude;

    public String getFollow_up_date() {
        return follow_up_date;
    }

    public void setFollow_up_date(String follow_up_date) {
        this.follow_up_date = follow_up_date;
    }

    @SerializedName("follow_up_date")
    String follow_up_date;
    @SerializedName("points")
    @Expose
    private List<dvrPointData> points = null;


    public SubmittedDVReport(Integer id, String dealer_code, String created_on, String points_discussed, String action_plan, String latitude, String longitude) {
        this.id = id;
        this.dealer_code = dealer_code;
        this.created_on = created_on;
        this.points_discussed = points_discussed;
        this.action_plan = action_plan;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public SubmittedDVReport() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDealer_code() {
        return dealer_code;
    }

    public void setDealer_code(String dealer_code) {
        this.dealer_code = dealer_code;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getPoints_discussed() {
        return points_discussed;
    }

    public void setPoints_discussed(String points_discussed) {
        this.points_discussed = points_discussed;
    }

    public String getAction_plan() {
        return action_plan;
    }

    public void setAction_plan(String action_plan) {
        this.action_plan = action_plan;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
    public List<dvrPointData> getPoints() {
        return points;
    }

    public void setPoints(List<dvrPointData> points) {
        this.points = points;
    }
}
