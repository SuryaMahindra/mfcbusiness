package com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model;

import com.google.gson.annotations.SerializedName;

public class WeeklyRModel {

    @SerializedName("dealer_code")
    String dealer_code;
    @SerializedName("dealer_type")
    String dealer_type;
    @SerializedName("from_date")
    String from_date;
    @SerializedName("to_date")
    String to_date;

    public WeeklyRModel(String dealer_code, String dealer_type, String from_date, String to_date) {
        this.dealer_code = dealer_code;
        this.dealer_type = dealer_type;
        this.from_date = from_date;
        this.to_date = to_date;
    }

}
