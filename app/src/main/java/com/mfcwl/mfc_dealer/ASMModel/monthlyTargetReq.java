package com.mfcwl.mfc_dealer.ASMModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class monthlyTargetReq {

    @SerializedName("dealer_code")
    @Expose
    private String dealerCode;
    @SerializedName("zone")
    @Expose
    private String zone;
    @SerializedName("area_manager")
    @Expose
    private String areaManager;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("sap_code")
    @Expose
    private String sapCode;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("sub_category")
    @Expose
    private String subCategory;
    @SerializedName("opening_stock")
    @Expose
    private String openingStock;
    @SerializedName("closeing_stock")
    @Expose
    private String closeingStock;
    @SerializedName("sales_paln")
    @Expose
    private String salesPaln;
    @SerializedName("procurement_plan")
    @Expose
    private String procurementPlan;
    @SerializedName("warranty_units")
    @Expose
    private String warrantyUnits;
    @SerializedName("warranty_value")
    @Expose
    private String warrantyValue;
    @SerializedName("walk_ins")
    @Expose
    private String walkIns;
    @SerializedName("sales_conversion")
    @Expose
    private String salesConversion;
    @SerializedName("oms_lead")
    @Expose
    private String omsLead;
    @SerializedName("oms_leads_conversion")
    @Expose
    private String omsLeadsConversion;
    @SerializedName("debitor_month")
    @Expose
    private String debitorMonth;
    @SerializedName("roi")
    @Expose
    private String roi;
    @SerializedName("procurement_through_iep")
    @Expose
    private String procurementThroughIep;
    @SerializedName("procurement_through_ncd")
    @Expose
    private String procurementThroughNcd;
    @SerializedName("procurement_through_cpt")
    @Expose
    private String procurementThroughCpt;

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getAreaManager() {
        return areaManager;
    }

    public void setAreaManager(String areaManager) {
        this.areaManager = areaManager;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSapCode() {
        return sapCode;
    }

    public void setSapCode(String sapCode) {
        this.sapCode = sapCode;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getOpeningStock() {
        return openingStock;
    }

    public void setOpeningStock(String openingStock) {
        this.openingStock = openingStock;
    }

    public String getCloseingStock() {
        return closeingStock;
    }

    public void setCloseingStock(String closeingStock) {
        this.closeingStock = closeingStock;
    }

    public String getSalesPaln() {
        return salesPaln;
    }

    public void setSalesPaln(String salesPaln) {
        this.salesPaln = salesPaln;
    }

    public String getProcurementPlan() {
        return procurementPlan;
    }

    public void setProcurementPlan(String procurementPlan) {
        this.procurementPlan = procurementPlan;
    }

    public String getWarrantyUnits() {
        return warrantyUnits;
    }

    public void setWarrantyUnits(String warrantyUnits) {
        this.warrantyUnits = warrantyUnits;
    }

    public String getWarrantyValue() {
        return warrantyValue;
    }

    public void setWarrantyValue(String warrantyValue) {
        this.warrantyValue = warrantyValue;
    }

    public String getWalkIns() {
        return walkIns;
    }

    public void setWalkIns(String walkIns) {
        this.walkIns = walkIns;
    }

    public String getSalesConversion() {
        return salesConversion;
    }

    public void setSalesConversion(String salesConversion) {
        this.salesConversion = salesConversion;
    }

    public String getOmsLead() {
        return omsLead;
    }

    public void setOmsLead(String omsLead) {
        this.omsLead = omsLead;
    }

    public String getOmsLeadsConversion() {
        return omsLeadsConversion;
    }

    public void setOmsLeadsConversion(String omsLeadsConversion) {
        this.omsLeadsConversion = omsLeadsConversion;
    }

    public String getDebitorMonth() {
        return debitorMonth;
    }

    public void setDebitorMonth(String debitorMonth) {
        this.debitorMonth = debitorMonth;
    }

    public String getRoi() {
        return roi;
    }

    public void setRoi(String roi) {
        this.roi = roi;
    }

    public String getProcurementThroughIep() {
        return procurementThroughIep;
    }

    public void setProcurementThroughIep(String procurementThroughIep) {
        this.procurementThroughIep = procurementThroughIep;
    }

    public String getProcurementThroughNcd() {
        return procurementThroughNcd;
    }

    public void setProcurementThroughNcd(String procurementThroughNcd) {
        this.procurementThroughNcd = procurementThroughNcd;
    }

    public String getProcurementThroughCpt() {
        return procurementThroughCpt;
    }

    public void setProcurementThroughCpt(String procurementThroughCpt) {
        this.procurementThroughCpt = procurementThroughCpt;
    }
}
