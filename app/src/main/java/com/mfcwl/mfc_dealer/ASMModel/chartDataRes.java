package com.mfcwl.mfc_dealer.ASMModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class chartDataRes {
    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private chartDataModel data;
    @SerializedName("LastSubmitedDate")
    @Expose
    private String lastSubmitedDate;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public chartDataModel getData() {
        return data;
    }

    public void setData(chartDataModel data) {
        this.data = data;
    }

    public String getLastSubmitedDate() {
        return lastSubmitedDate;
    }

    public void setLastSubmitedDate(String lastSubmitedDate) {
        this.lastSubmitedDate = lastSubmitedDate;
    }

}
