package com.mfcwl.mfc_dealer.ASMModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class stockFilterReq {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("dashboardData")
    @Expose
    private List<stockFilterData> dashboardData = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<stockFilterData> getDashboardData() {
        return dashboardData;
    }

    public void setDashboardData(List<stockFilterData> dashboardData) {
        this.dashboardData = dashboardData;
    }
}