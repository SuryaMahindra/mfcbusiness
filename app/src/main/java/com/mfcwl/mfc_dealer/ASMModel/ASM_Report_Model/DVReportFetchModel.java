package com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model;

import com.google.gson.annotations.SerializedName;

public class DVReportFetchModel {

    @SerializedName("dealer_code")
    String  dealer_code;

    @SerializedName("from_date")
    String from_date;

    @SerializedName("to_date")
    String to_date;

    public DVReportFetchModel(String dealer_code, String from_date, String to_date) {
        this.dealer_code = dealer_code;
        this.from_date = from_date;
        this.to_date = to_date;
    }

    public String getDealer_code() {
        return dealer_code;
    }

    public void setDealer_code(String dealer_code) {
        this.dealer_code = dealer_code;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }
}
