package com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model;

import com.google.gson.annotations.SerializedName;

public class DVEmail {


    //@SerializedName("id")
     String id;
   // @SerializedName("dealer_code")
    String dealer_code;
    //@SerializedName("created_on")
     String created_on;
   // @SerializedName("points_discussed")
    String points_discussed;
    //@SerializedName("action_plan")
   String action_plan;
   // @SerializedName("latitude")
     String latitude;
    //@SerializedName("longitude")
   String longitude;

    public DVEmail() {
    }

    public DVEmail(String id, String dealer_code, String created_on, String points_discussed, String action_plan, String latitude, String longitude) {
        this.id = id;
        this.dealer_code = dealer_code;
        this.created_on = created_on;
        this.points_discussed = points_discussed;
        this.action_plan = action_plan;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
