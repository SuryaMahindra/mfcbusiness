package com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model;

public class DVDisplay {

   public static String name;
   public static String points;
   public static String action;
   public static String created;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
