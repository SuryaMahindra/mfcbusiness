package com.mfcwl.mfc_dealer.ASMModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class paymentDetails {


    @SerializedName("transaction_date")
    @Expose
    private String transactionDate;
    @SerializedName("type_of_fee")
    @Expose
    private String typeOfFee;
    @SerializedName("mode_of_payment")
    @Expose
    private String modeOfPayment;
    @SerializedName("amount_paid")
    @Expose
    private String amountPaid;
    @SerializedName("TDS")
    @Expose
    private String tDS;
    @SerializedName("ref_no")
    @Expose
    private String refNo;
    @SerializedName("bank")
    @Expose
    private String bank;
    @SerializedName("dealerCode")
    @Expose
    private String dealerCode;
    @SerializedName("attachments")
    @Expose
    private String attachments;

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTypeOfFee() {
        return typeOfFee;
    }

    public void setTypeOfFee(String typeOfFee) {
        this.typeOfFee = typeOfFee;
    }

    public String getModeOfPayment() {
        return modeOfPayment;
    }

    public void setModeOfPayment(String modeOfPayment) {
        this.modeOfPayment = modeOfPayment;
    }

    public String getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(String amountPaid) {
        this.amountPaid = amountPaid;
    }

    public String getTDS() {
        return tDS;
    }

    public void setTDS(String tDS) {
        this.tDS = tDS;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }

}
