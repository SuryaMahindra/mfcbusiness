package com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model;



import com.mfcwl.mfc_dealer.AddStockModel.dvrPointData;

import java.util.ArrayList;
import java.util.List;

public class SubmittedReportModel
{
    String id;
    String companyName;
    String dealer_code;
    String daytv;
    String datetv;
    String timetv;
    String latitude;

    String created;

    public String getFollow_date() {
        return follow_date;
    }

    public void setFollow_date(String follow_date) {
        this.follow_date = follow_date;
    }

    String follow_date;

    private List<dvrPointData> points = null;

    int sort_id;

    public int getSort_id() {
        return sort_id;
    }

    public void setSort_id(int sort_id) {
        this.sort_id = sort_id;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public SubmittedReportModel(List<dvrPointData> pointt,String follow,String id, String companyName, String dealer_code, String daytv, String datetv, String timetv, String latitude, String longitude, int sort_id, String created) {
        this.id = id;
        this.companyName = companyName;
        this.dealer_code = dealer_code;
        this.daytv = daytv;
        this.datetv = datetv;
        this.timetv = timetv;
        this.latitude = latitude;
        this.longitude = longitude;
        this.sort_id = sort_id;
        this.created = created;
        this.points=pointt;
        this.follow_date=follow;
    }

    public List<dvrPointData> getPoints() {
        return points;
    }

    public void setPoints(List<dvrPointData> points) {
        this.points = points;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDealer_code() {
        return dealer_code;
    }

    public void setDealer_code(String dealer_code) {
        this.dealer_code = dealer_code;
    }

    public void setDaytv(String daytv) {
        this.daytv = daytv;
    }

    public void setDatetv(String datetv) {
        this.datetv = datetv;
    }

    public void setTimetv(String timetv) {
        this.timetv = timetv;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    String longitude;


    public String getId() {
        return id;
    }

    public SubmittedReportModel(String id, String companyName, String daytv, String datetv, String timetv) {
        this.id = id;
        this.companyName = companyName;
        this.daytv = daytv;
        this.datetv = datetv;
        this.timetv = timetv;
    }

    public void setId(String id) {
        this.id = id;
    }



    public SubmittedReportModel(String companyName, String daytv, String datetv, String timetv) {
        this.companyName = companyName;
        this.daytv = daytv;
        this.datetv = datetv;
        this.timetv = timetv;
    }

    public SubmittedReportModel() {

    }

    public String getCompanyName() {
        return companyName;
    }


    public String getDaytv() {
        return daytv;
    }


    public String getDatetv() {
        return datetv;
    }


    public String getTimetv() {
        return timetv;
    }

    public ArrayList<SubmittedReportModel> submittedReportModels()
    {
        ArrayList<SubmittedReportModel> list = new ArrayList<>();
        for(int count=0;count<=9;count++) {
            list.add(new SubmittedReportModel("Formula 1 Auto Car dealer", "Wed", "24/7/19", "17.00"));
        }
        return list;
    }




}
