package com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WeekReportSubmitModel {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("zone")
    @Expose
    private String zone;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("dealer_code")
    @Expose
    private String dealerCode;
    @SerializedName("dealer_category")
    @Expose
    private String dealerCategory;
    @SerializedName("am_token_no")
    @Expose
    private String amTokenNo;
    @SerializedName("am_name")
    @Expose
    private String amName;
    @SerializedName("sm_token_no")
    @Expose
    private String smTokenNo;
    @SerializedName("sm_name")
    @Expose
    private String smName;
    @SerializedName("stcok_opening_month")
    @Expose
    private String stcokOpeningMonth;
    @SerializedName("stock_refreshed")
    @Expose
    private String stockRefreshed;
    @SerializedName("stock_less_60_days")
    @Expose
    private String stockLess60Days;
    @SerializedName("stock_grater_60_days")
    @Expose
    private String stockGrater60Days;
    @SerializedName("current_stock")
    @Expose
    private String currentStock;
    @SerializedName("actual_stock")
    @Expose
    private String actualStock;
    @SerializedName("cpt")
    @Expose
    private String cpt;
    @SerializedName("self_procurement")
    @Expose
    private String selfProcurement;
    @SerializedName("iep")
    @Expose
    private String iep;
    @SerializedName("ncd")
    @Expose
    private String ncd;
    @SerializedName("iep_lead")
    @Expose
    private String iepLead;
    @SerializedName("ncp_lead")
    @Expose
    private String ncpLead;
    @SerializedName("iep_conversion")
    @Expose
    private String iepConversion;
    @SerializedName("ncd_conversion_per")
    @Expose
    private String ncdConversionPer;
    @SerializedName("no_of_bids")
    @Expose
    private String noOfBids;
    @SerializedName("x_mart")
    @Expose
    private String xMart;
    @SerializedName("total_procurement")
    @Expose
    private String totalProcurement;
    @SerializedName("retail_sales")
    @Expose
    private String retailSales;
    @SerializedName("offload_sales")
    @Expose
    private String offloadSales;
    @SerializedName("total_sales")
    @Expose
    private String totalSales;
    @SerializedName("booking_in_hand")
    @Expose
    private String bookingInHand;
    @SerializedName("oms_lead_conversion")
    @Expose
    private String omsLeadConversion;
    @SerializedName("oms_leads")
    @Expose
    private String omsLeads;
    @SerializedName("oms_walkin_through")
    @Expose
    private String omsWalkinThrough;
    @SerializedName("no_of_finance_cases")
    @Expose
    private String noOfFinanceCases;
    @SerializedName("warranty_no")
    @Expose
    private String warrantyNo;
    @SerializedName("warranty_values")
    @Expose
    private String warrantyValues;
    @SerializedName("warranty_percentage")
    @Expose
    private String warrantyPercentage;
    @SerializedName("open_aggregate_cases")
    @Expose
    private String openAggregateCases;
    @SerializedName("closed_aggregate_cases")
    @Expose
    private String closedAggregateCases;
    @SerializedName("total_aggregate_cases")
    @Expose
    private String totalAggregateCases;
/*    @SerializedName("debtors")
    @Expose
    private String debtors;
    @SerializedName("write_off")
    @Expose
    private String writeOff;*/
    @SerializedName("actual_collection")
    @Expose
    private String actualCollection;
    @SerializedName("no_of_dealer_visits")
    @Expose
    private String noOfDealerVisits;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("week_for_text")
    @Expose
    private String weekForText;
    @SerializedName("form_date")
    @Expose
    private String formDate;
    @SerializedName("to_date")
    @Expose
    private String toDate;
    @SerializedName("account_sap_code")
    @Expose
    private String accountSapCode;
/*    @SerializedName("activation_time")
    @Expose
    private String activationTime;*/
    @SerializedName("actual_activation_time")
    @Expose
    private String actualActivationTime;
    @SerializedName("direct_walkin")
    @Expose
    private Integer directWalkin;
    @SerializedName("dealer_name")
    @Expose
    private String dealerName;
    @SerializedName("tempdealer_code")
    @Expose
    private String tempdealer_code;

    public String getCreatedforKey() {
        return createdforKey;
    }

    public void setCreatedforKey(String createdforKey) {
        this.createdforKey = createdforKey;
    }

    @SerializedName("createdforKey")
    @Expose
    private String createdforKey;

    @SerializedName("day")
    @Expose
    private String day;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("hour")
    @Expose
    private String hour;


    @SerializedName("total_walkin")
    @Expose
    private Integer totalWalkin;

    public Integer getTotalWalkin() {
        return totalWalkin;
    }

    public void setTotalWalkin(Integer totalWalkin) {
        this.totalWalkin = totalWalkin;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getxMart() {
        return xMart;
    }

    public void setxMart(String xMart) {
        this.xMart = xMart;
    }

    /**
     * declaration over
     * @return
     */

    @SerializedName("paid_up_stock")
    @Expose
    private String paidUpStock;
    @SerializedName("park_and_sell_stock")
    @Expose
    private String parkAndSellStock;
    @SerializedName("royalty_collection")
    @Expose
    private String royaltyCollection;

    public String getPaidUpStock() {
        return paidUpStock;
    }

    public void setPaidUpStock(String paidUpStock) {
        this.paidUpStock = paidUpStock;
    }

    public String getParkAndSellStock() {
        return parkAndSellStock;
    }

    public void setParkAndSellStock(String parkAndSellStock) {
        this.parkAndSellStock = parkAndSellStock;
    }

    public String getRoyaltyCollection() {
        return royaltyCollection;
    }

    public void setRoyaltyCollection(String royaltyCollection) {
        this.royaltyCollection = royaltyCollection;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getDealerCategory() {
        return dealerCategory;
    }

    public void setDealerCategory(String dealerCategory) {
        this.dealerCategory = dealerCategory;
    }

    public String getAmTokenNo() {
        return amTokenNo;
    }

    public void setAmTokenNo(String amTokenNo) {
        this.amTokenNo = amTokenNo;
    }

    public String getAmName() {
        return amName;
    }

    public void setAmName(String amName) {
        this.amName = amName;
    }

    public String getSmTokenNo() {
        return smTokenNo;
    }

    public void setSmTokenNo(String smTokenNo) {
        this.smTokenNo = smTokenNo;
    }

    public String getSmName() {
        return smName;
    }

    public void setSmName(String smName) {
        this.smName = smName;
    }

    public String getStcokOpeningMonth() {
        return stcokOpeningMonth;
    }

    public void setStcokOpeningMonth(String stcokOpeningMonth) {
        this.stcokOpeningMonth = stcokOpeningMonth;
    }

    public String getStockRefreshed() {
        return stockRefreshed;
    }

    public void setStockRefreshed(String stockRefreshed) {
        this.stockRefreshed = stockRefreshed;
    }

    public String getStockLess60Days() {
        return stockLess60Days;
    }

    public void setStockLess60Days(String stockLess60Days) {
        this.stockLess60Days = stockLess60Days;
    }

    public String getStockGrater60Days() {
        return stockGrater60Days;
    }

    public void setStockGrater60Days(String stockGrater60Days) {
        this.stockGrater60Days = stockGrater60Days;
    }

    public String getCurrentStock() {
        return currentStock;
    }

    public void setCurrentStock(String currentStock) {
        this.currentStock = currentStock;
    }

    public String getActualStock() {
        return actualStock;
    }

    public void setActualStock(String actualStock) {
        this.actualStock = actualStock;
    }

    public String getCpt() {
        return cpt;
    }

    public void setCpt(String cpt) {
        this.cpt = cpt;
    }

    public String getSelfProcurement() {
        return selfProcurement;
    }

    public void setSelfProcurement(String selfProcurement) {
        this.selfProcurement = selfProcurement;
    }

    public String getIep() {
        return iep;
    }

    public void setIep(String iep) {
        this.iep = iep;
    }

    public String getNcd() {
        return ncd;
    }

    public void setNcd(String ncd) {
        this.ncd = ncd;
    }

    public String getIepLead() {
        return iepLead;
    }

    public void setIepLead(String iepLead) {
        this.iepLead = iepLead;
    }

    public String getNcpLead() {
        return ncpLead;
    }

    public void setNcpLead(String ncpLead) {
        this.ncpLead = ncpLead;
    }

    public String getIepConversion() {
        return iepConversion;
    }

    public void setIepConversion(String iepConversion) {
        this.iepConversion = iepConversion;
    }

    public String getNcdConversionPer() {
        return ncdConversionPer;
    }

    public void setNcdConversionPer(String ncdConversionPer) {
        this.ncdConversionPer = ncdConversionPer;
    }

    public String getNoOfBids() {
        return noOfBids;
    }

    public void setNoOfBids(String noOfBids) {
        this.noOfBids = noOfBids;
    }

    public String getXMart() {
        return xMart;
    }

    public void setXMart(String xMart) {
        this.xMart = xMart;
    }

    public String getTotalProcurement() {
        return totalProcurement;
    }

    public void setTotalProcurement(String totalProcurement) {
        this.totalProcurement = totalProcurement;
    }

    public String getRetailSales() {
        return retailSales;
    }

    public void setRetailSales(String retailSales) {
        this.retailSales = retailSales;
    }

    public String getOffloadSales() {
        return offloadSales;
    }

    public void setOffloadSales(String offloadSales) {
        this.offloadSales = offloadSales;
    }

    public String getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(String totalSales) {
        this.totalSales = totalSales;
    }

    public String getBookingInHand() {
        return bookingInHand;
    }

    public void setBookingInHand(String bookingInHand) {
        this.bookingInHand = bookingInHand;
    }

    public String getOmsLeadConversion() {
        return omsLeadConversion;
    }

    public void setOmsLeadConversion(String omsLeadConversion) {
        this.omsLeadConversion = omsLeadConversion;
    }

    public String getOmsLeads() {
        return omsLeads;
    }

    public void setOmsLeads(String omsLeads) {
        this.omsLeads = omsLeads;
    }

    public String getOmsWalkinThrough() {
        return omsWalkinThrough;
    }

    public void setOmsWalkinThrough(String omsWalkinThrough) {
        this.omsWalkinThrough = omsWalkinThrough;
    }

    public String getNoOfFinanceCases() {
        return noOfFinanceCases;
    }

    public void setNoOfFinanceCases(String noOfFinanceCases) {
        this.noOfFinanceCases = noOfFinanceCases;
    }

    public String getWarrantyNo() {
        return warrantyNo;
    }

    public void setWarrantyNo(String warrantyNo) {
        this.warrantyNo = warrantyNo;
    }

    public String getWarrantyValues() {
        return warrantyValues;
    }

    public void setWarrantyValues(String warrantyValues) {
        this.warrantyValues = warrantyValues;
    }

    public String getWarrantyPercentage() {
        return warrantyPercentage;
    }

    public void setWarrantyPercentage(String warrantyPercentage) {
        this.warrantyPercentage = warrantyPercentage;
    }

    public String getOpenAggregateCases() {
        return openAggregateCases;
    }

    public void setOpenAggregateCases(String openAggregateCases) {
        this.openAggregateCases = openAggregateCases;
    }

    public String getClosedAggregateCases() {
        return closedAggregateCases;
    }

    public void setClosedAggregateCases(String closedAggregateCases) {
        this.closedAggregateCases = closedAggregateCases;
    }

    public String getTotalAggregateCases() {
        return totalAggregateCases;
    }

    public void setTotalAggregateCases(String totalAggregateCases) {
        this.totalAggregateCases = totalAggregateCases;
    }

/*    public String getDebtors() {
        return debtors;
    }

    public void setDebtors(String debtors) {
        this.debtors = debtors;
    }

    public String getWriteOff() {
        return writeOff;
    }

    public void setWriteOff(String writeOff) {
        this.writeOff = writeOff;
    }*/

    public String getActualCollection() {
        return actualCollection;
    }

    public void setActualCollection(String actualCollection) {
        this.actualCollection = actualCollection;
    }

    public String getNoOfDealerVisits() {
        return noOfDealerVisits;
    }

    public void setNoOfDealerVisits(String noOfDealerVisits) {
        this.noOfDealerVisits = noOfDealerVisits;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getWeekForText() {
        return weekForText;
    }

    public void setWeekForText(String weekForText) {
        this.weekForText = weekForText;
    }

    public String getFormDate() {
        return formDate;
    }

    public void setFormDate(String formDate) {
        this.formDate = formDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getAccountSapCode() {
        return accountSapCode;
    }

    public void setAccountSapCode(String accountSapCode) {
        this.accountSapCode = accountSapCode;
    }

/*    public String getActivationTime() {
        return activationTime;
    }

    public void setActivationTime(String activationTime) {
        this.activationTime = activationTime;
    }*/

    public String getActualActivationTime() {
        return actualActivationTime;
    }

    public void setActualActivationTime(String actualActivationTime) {
        this.actualActivationTime = actualActivationTime;
    }

    public Integer getDirectWalkin() {
        return directWalkin;
    }

    public void setDirectWalkin(Integer directWalkin) {
        this.directWalkin = directWalkin;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

   /* @SerializedName("dealername")
    @Expose
    private String dealername;*/

    public String getTempdealer_code() {
        return tempdealer_code;
    }

    public void setTempdealer_code(String tempdealer_code) {
        this.tempdealer_code = tempdealer_code;
    }



/*    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }*/



    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

}
