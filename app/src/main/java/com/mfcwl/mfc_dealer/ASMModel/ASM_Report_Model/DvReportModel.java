package com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model;

import com.google.gson.annotations.SerializedName;

public class DvReportModel {
    @SerializedName("dealer_code")
    String dealer_code;
    @SerializedName("points_discussed")
    String points_discussed;
    @SerializedName("action_plan")
    String action_plan;
    @SerializedName("latitude")
    String latitude;
    @SerializedName("longitude")
    String longitude;

    public DvReportModel(String dealer_code, String points_discussed, String action_plan, String latitude, String longitude) {
        this.dealer_code = dealer_code;
        this.points_discussed = points_discussed;
        this.action_plan = action_plan;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public DvReportModel() {
    }

    public String getDealer_code() {
        return dealer_code;
    }

    public void setDealer_code(String dealer_code) {
        this.dealer_code = dealer_code;
    }

    public String getPoints_discussed() {
        return points_discussed;
    }

    public void setPoints_discussed(String points_discussed) {
        this.points_discussed = points_discussed;
    }

    public String getAction_plan() {
        return action_plan;
    }

    public void setAction_plan(String action_plan) {
        this.action_plan = action_plan;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
