package com.mfcwl.mfc_dealer.ASMModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MessageCenterRes {


    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("dashboardData")
    @Expose
    private List<MessageCenterResData> dashboardData = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<MessageCenterResData> getDashboardData() {
        return dashboardData;
    }

    public void setDashboardData(List<MessageCenterResData> dashboardData) {
        this.dashboardData = dashboardData;
    }
}
