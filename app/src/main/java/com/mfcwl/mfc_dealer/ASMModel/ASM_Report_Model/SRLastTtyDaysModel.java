package com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model;

import java.util.ArrayList;

public class SRLastTtyDaysModel
{
    String companyName;
    String daytv;
    String datetv;
    String timetv;

    public SRLastTtyDaysModel(String companyName, String daytv, String datetv, String timetv) {
        this.companyName = companyName;
        this.daytv = daytv;
        this.datetv = datetv;
        this.timetv = timetv;
    }

    public SRLastTtyDaysModel() {

    }

    public String getCompanyName() {
        return companyName;
    }


    public String getDaytv() {
        return daytv;
    }


    public String getDatetv() {
        return datetv;
    }


    public String getTimetv() {
        return timetv;
    }

    public ArrayList<SRLastTtyDaysModel> srLastTtyDaysModels()
    {
        ArrayList<SRLastTtyDaysModel> list = new ArrayList<>();
        for(int count=0;count<=9;count++) {
            list.add(new SRLastTtyDaysModel("Manyata Pvt Lmt.,", "Wed", "24/7/19", "17.00"));
        }
        return list;
    }



}
