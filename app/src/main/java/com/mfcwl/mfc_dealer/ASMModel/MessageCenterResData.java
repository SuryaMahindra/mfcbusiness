package com.mfcwl.mfc_dealer.ASMModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.MessageCDataTwo;

import java.util.List;

public class MessageCenterResData {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("count")
    @Expose
    private Object count;
    @SerializedName("data")
    @Expose
    private List<MessageCDataTwo> data = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Object getCount() {
        return count;
    }

    public void setCount(Object count) {
        this.count = count;
    }

    public List<MessageCDataTwo> getData() {
        return data;
    }

    public void setData(List<MessageCDataTwo> data) {
        this.data = data;
    }

}
