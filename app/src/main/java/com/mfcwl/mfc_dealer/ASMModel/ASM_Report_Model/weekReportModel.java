package com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class weekReportModel {

    @SerializedName("fromdate")
    @Expose
    private String fromdate;
    @SerializedName("todate")
    @Expose
    private String todate;
    @SerializedName("text")
    @Expose
    private String text;

    public String getFromdate() {
        return fromdate;
    }

    public void setFromdate(String fromdate) {
        this.fromdate = fromdate;
    }

    public String getTodate() {
        return todate;
    }

    public void setTodate(String todate) {
        this.todate = todate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
