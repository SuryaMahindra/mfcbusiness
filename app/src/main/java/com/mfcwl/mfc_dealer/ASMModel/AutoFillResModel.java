package com.mfcwl.mfc_dealer.ASMModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AutoFillResModel {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("zone")
    @Expose
    private String zone;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("dealer_code")
    @Expose
    private String dealerCode;
    @SerializedName("dealer_category")
    @Expose
    private String dealerCategory;
    @SerializedName("am_token_no")
    @Expose
    private String amTokenNo;
    @SerializedName("am_name")
    @Expose
    private String amName;
    @SerializedName("sm_token_no")
    @Expose
    private String smTokenNo;
    @SerializedName("sm_name")
    @Expose
    private String smName;
    @SerializedName("stcok_opening_month")
    @Expose
    private Object stcokOpeningMonth;
    @SerializedName("stock_refreshed")
    @Expose
    private Object stockRefreshed;
    @SerializedName("stock_less_60_days")
    @Expose
    private Integer stockLess60Days;
    @SerializedName("stock_grater_60_days")
    @Expose
    private Integer stockGrater60Days;
    @SerializedName("current_stock")
    @Expose
    private Object currentStock;
    @SerializedName("actual_stock")
    @Expose
    private Object actualStock;
    @SerializedName("cpt")
    @Expose
    private Object cpt;
    @SerializedName("self_procurement")
    @Expose
    private Object selfProcurement;
    @SerializedName("iep")
    @Expose
    private Object iep;
    @SerializedName("ncd")
    @Expose
    private Object ncd;
    @SerializedName("iep_lead")
    @Expose
    private Object iepLead;
    @SerializedName("ncp_lead")
    @Expose
    private Object ncpLead;
    @SerializedName("iep_conversion")
    @Expose
    private Object iepConversion;
    @SerializedName("ncd_conversion_per")
    @Expose
    private Object ncdConversionPer;
    @SerializedName("no_of_bids")
    @Expose
    private Object noOfBids;
    @SerializedName("x_mart")
    @Expose
    private Object xMart;
    @SerializedName("total_procurement")
    @Expose
    private Object totalProcurement;
    @SerializedName("retail_sales")
    @Expose
    private Integer retailSales;
    @SerializedName("offload_sales")
    @Expose
    private Integer offloadSales;
    @SerializedName("total_sales")
    @Expose
    private Object totalSales;
    @SerializedName("booking_in_hand")
    @Expose
    private Integer bookingInHand;
    @SerializedName("oms_lead_conversion")
    @Expose
    private Object omsLeadConversion;
    @SerializedName("oms_leads")
    @Expose
    private Object omsLeads;
    @SerializedName("oms_walkin_through")
    @Expose
    private Object omsWalkinThrough;
    @SerializedName("no_of_finance_cases")
    @Expose
    private Object noOfFinanceCases;
    @SerializedName("warranty_no")
    @Expose
    private String warrantyNo;
    @SerializedName("warranty_values")
    @Expose
    private String warrantyValues;
    @SerializedName("warranty_percentage")
    @Expose
    private Object warrantyPercentage;
    @SerializedName("open_aggregate_cases")
    @Expose
    private Object openAggregateCases;
    @SerializedName("closed_aggregate_cases")
    @Expose
    private Object closedAggregateCases;
    @SerializedName("total_aggregate_cases")
    @Expose
    private Object totalAggregateCases;
    /*     @SerializedName("debtors")
         @Expose
         private Object debtors;
         @SerializedName("write_off")
         @Expose
         private Object writeOff;*/
    @SerializedName("actual_collection")
    @Expose
    private Object actualCollection;
    @SerializedName("no_of_dealer_visits")
    @Expose
    private Object noOfDealerVisits;
    @SerializedName("created_on")
    @Expose
    private Object createdOn;
    @SerializedName("week_for_text")
    @Expose
    private Object weekForText;
    @SerializedName("form_date")
    @Expose
    private String formDate;
    @SerializedName("to_date")
    @Expose
    private String toDate;
    @SerializedName("account_sap_code")
    @Expose
    private String accountSapCode;
    /*        @SerializedName("activation_time")
            @Expose
            private String activationTime;*/
    @SerializedName("actual_activation_time")
    @Expose
    private Object actualActivationTime;
    @SerializedName("dealer_name")
    @Expose
    private String dealerName;
    @SerializedName("direct_walkin")
    @Expose
    private Integer directWalkin;
    @SerializedName("created_by_franchiseid")
    @Expose
    private Object createdByFranchiseid;


    @SerializedName("paid_up_stock")
    @Expose
    private String paidUpStock;
    @SerializedName("park_and_sell_stock")
    @Expose
    private String parkAndSellStock;
    @SerializedName("royalty_collection")
    @Expose
    private String royaltyCollection;

    public String getPaidUpStock() {
        return paidUpStock;
    }

    public void setPaidUpStock(String paidUpStock) {
        this.paidUpStock = paidUpStock;
    }

    public String getParkAndSellStock() {
        return parkAndSellStock;
    }

    public void setParkAndSellStock(String parkAndSellStock) {
        this.parkAndSellStock = parkAndSellStock;
    }

    public String getRoyaltyCollection() {
        return royaltyCollection;
    }

    public void setRoyaltyCollection(String royaltyCollection) {
        this.royaltyCollection = royaltyCollection;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getDealerCategory() {
        return dealerCategory;
    }

    public void setDealerCategory(String dealerCategory) {
        this.dealerCategory = dealerCategory;
    }

    public String getAmTokenNo() {
        return amTokenNo;
    }

    public void setAmTokenNo(String amTokenNo) {
        this.amTokenNo = amTokenNo;
    }

    public String getAmName() {
        return amName;
    }

    public void setAmName(String amName) {
        this.amName = amName;
    }

    public String getSmTokenNo() {
        return smTokenNo;
    }

    public void setSmTokenNo(String smTokenNo) {
        this.smTokenNo = smTokenNo;
    }

    public String getSmName() {
        return smName;
    }

    public void setSmName(String smName) {
        this.smName = smName;
    }

    public Object getStcokOpeningMonth() {
        return stcokOpeningMonth;
    }

    public void setStcokOpeningMonth(Object stcokOpeningMonth) {
        this.stcokOpeningMonth = stcokOpeningMonth;
    }

    public Object getStockRefreshed() {
        return stockRefreshed;
    }

    public void setStockRefreshed(Object stockRefreshed) {
        this.stockRefreshed = stockRefreshed;
    }

    public Integer getStockLess60Days() {
        return stockLess60Days;
    }

    public void setStockLess60Days(Integer stockLess60Days) {
        this.stockLess60Days = stockLess60Days;
    }

    public Integer getStockGrater60Days() {
        return stockGrater60Days;
    }

    public void setStockGrater60Days(Integer stockGrater60Days) {
        this.stockGrater60Days = stockGrater60Days;
    }

    public Object getCurrentStock() {
        return currentStock;
    }

    public void setCurrentStock(Object currentStock) {
        this.currentStock = currentStock;
    }

    public Object getActualStock() {
        return actualStock;
    }

    public void setActualStock(Object actualStock) {
        this.actualStock = actualStock;
    }

    public Object getCpt() {
        return cpt;
    }

    public void setCpt(Object cpt) {
        this.cpt = cpt;
    }

    public Object getSelfProcurement() {
        return selfProcurement;
    }

    public void setSelfProcurement(Object selfProcurement) {
        this.selfProcurement = selfProcurement;
    }

    public Object getIep() {
        return iep;
    }

    public void setIep(Object iep) {
        this.iep = iep;
    }

    public Object getNcd() {
        return ncd;
    }

    public void setNcd(Object ncd) {
        this.ncd = ncd;
    }

    public Object getIepLead() {
        return iepLead;
    }

    public void setIepLead(Object iepLead) {
        this.iepLead = iepLead;
    }

    public Object getNcpLead() {
        return ncpLead;
    }

    public void setNcpLead(Object ncpLead) {
        this.ncpLead = ncpLead;
    }

    public Object getIepConversion() {
        return iepConversion;
    }

    public void setIepConversion(Object iepConversion) {
        this.iepConversion = iepConversion;
    }

    public Object getNcdConversionPer() {
        return ncdConversionPer;
    }

    public void setNcdConversionPer(Object ncdConversionPer) {
        this.ncdConversionPer = ncdConversionPer;
    }

    public Object getNoOfBids() {
        return noOfBids;
    }

    public void setNoOfBids(Object noOfBids) {
        this.noOfBids = noOfBids;
    }

    public Object getXMart() {
        return xMart;
    }

    public void setXMart(Object xMart) {
        this.xMart = xMart;
    }

    public Object getTotalProcurement() {
        return totalProcurement;
    }

    public void setTotalProcurement(Object totalProcurement) {
        this.totalProcurement = totalProcurement;
    }

    public Integer getRetailSales() {
        return retailSales;
    }

    public void setRetailSales(Integer retailSales) {
        this.retailSales = retailSales;
    }

    public Integer getOffloadSales() {
        return offloadSales;
    }

    public void setOffloadSales(Integer offloadSales) {
        this.offloadSales = offloadSales;
    }

    public Object getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(Object totalSales) {
        this.totalSales = totalSales;
    }

    public Integer getBookingInHand() {
        return bookingInHand;
    }

    public void setBookingInHand(Integer bookingInHand) {
        this.bookingInHand = bookingInHand;
    }

    public Object getOmsLeadConversion() {
        return omsLeadConversion;
    }

    public void setOmsLeadConversion(Object omsLeadConversion) {
        this.omsLeadConversion = omsLeadConversion;
    }

    public Object getOmsLeads() {
        return omsLeads;
    }

    public void setOmsLeads(Object omsLeads) {
        this.omsLeads = omsLeads;
    }

    public Object getOmsWalkinThrough() {
        return omsWalkinThrough;
    }

    public void setOmsWalkinThrough(Object omsWalkinThrough) {
        this.omsWalkinThrough = omsWalkinThrough;
    }

    public Object getNoOfFinanceCases() {
        return noOfFinanceCases;
    }

    public void setNoOfFinanceCases(Object noOfFinanceCases) {
        this.noOfFinanceCases = noOfFinanceCases;
    }

    public String getWarrantyNo() {
        return warrantyNo;
    }

    public void setWarrantyNo(String warrantyNo) {
        this.warrantyNo = warrantyNo;
    }

    public String getWarrantyValues() {
        return warrantyValues;
    }

    public void setWarrantyValues(String warrantyValues) {
        this.warrantyValues = warrantyValues;
    }

    public Object getWarrantyPercentage() {
        return warrantyPercentage;
    }

    public void setWarrantyPercentage(Object warrantyPercentage) {
        this.warrantyPercentage = warrantyPercentage;
    }

    public Object getOpenAggregateCases() {
        return openAggregateCases;
    }

    public void setOpenAggregateCases(Object openAggregateCases) {
        this.openAggregateCases = openAggregateCases;
    }

    public Object getClosedAggregateCases() {
        return closedAggregateCases;
    }

    public void setClosedAggregateCases(Object closedAggregateCases) {
        this.closedAggregateCases = closedAggregateCases;
    }

    public Object getTotalAggregateCases() {
        return totalAggregateCases;
    }

    public void setTotalAggregateCases(Object totalAggregateCases) {
        this.totalAggregateCases = totalAggregateCases;
    }

/*        public Object getDebtors() {
            return debtors;
        }

        public void setDebtors(Object debtors) {
            this.debtors = debtors;
        }

        public Object getWriteOff() {
            return writeOff;
        }

        public void setWriteOff(Object writeOff) {
            this.writeOff = writeOff;
        }*/

    public Object getActualCollection() {
        return actualCollection;
    }

    public void setActualCollection(Object actualCollection) {
        this.actualCollection = actualCollection;
    }

    public Object getNoOfDealerVisits() {
        return noOfDealerVisits;
    }

    public void setNoOfDealerVisits(Object noOfDealerVisits) {
        this.noOfDealerVisits = noOfDealerVisits;
    }

    public Object getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Object createdOn) {
        this.createdOn = createdOn;
    }

    public Object getWeekForText() {
        return weekForText;
    }

    public void setWeekForText(Object weekForText) {
        this.weekForText = weekForText;
    }

    public String getFormDate() {
        return formDate;
    }

    public void setFormDate(String formDate) {
        this.formDate = formDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getAccountSapCode() {
        return accountSapCode;
    }

    public void setAccountSapCode(String accountSapCode) {
        this.accountSapCode = accountSapCode;
    }

    /*    public String getActivationTime() {
            return activationTime;
        }

        public void setActivationTime(String activationTime) {
            this.activationTime = activationTime;
        }*/

    public Object getActualActivationTime() {
        return actualActivationTime;
    }

    public void setActualActivationTime(Object actualActivationTime) {
        this.actualActivationTime = actualActivationTime;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public Integer getDirectWalkin() {
        return directWalkin;
    }

    public void setDirectWalkin(Integer directWalkin) {
        this.directWalkin = directWalkin;
    }

    public Object getCreatedByFranchiseid() {
        return createdByFranchiseid;
    }

    public void setCreatedByFranchiseid(Object createdByFranchiseid) {
        this.createdByFranchiseid = createdByFranchiseid;
    }

}

