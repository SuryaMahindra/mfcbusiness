package com.mfcwl.mfc_dealer.Adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sowmya on 20/3/18.
 */

public class FilterPriceAdapter extends RecyclerView.Adapter<FilterPriceAdapter.ViewHolder> {
    List<String> list = new ArrayList<>();

    public FilterPriceAdapter(List<String> listView){
        this.list = listView;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_price_row,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        /*String year = list.get(position);
        holder.yearFilter.setText(year);*/
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView yearFilter;
        public ViewHolder(View itemView) {
            super(itemView);
            //yearFilter = (TextView)itemView.findViewById(R.id.year_filter);
        }
    }
}
