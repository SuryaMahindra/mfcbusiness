package com.mfcwl.mfc_dealer.Adapter;

import android.app.Activity;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.graphics.Color;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.MessageCDataTwo;
import com.mfcwl.mfc_dealer.ASMModel.MsgFilterData;
import com.mfcwl.mfc_dealer.InstanceCreate.LeadSection.LeadFilterSaveInstance;
import com.mfcwl.mfc_dealer.InstanceCreate.Stocklisting;
import com.mfcwl.mfc_dealer.Model.DashboardModels;
import com.mfcwl.mfc_dealer.Procurement.Instances.ProcLeadFilterSaveInstance;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.SalesStocks.SalesInterface.SSLazyloaderSalesStocks;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessageCenterAdapter extends RecyclerView.Adapter<MessageCenterAdapter.MyViewHolder> implements View.OnClickListener {

    private List<MessageCDataTwo> messageList;
    LayoutInflater layoutInflater;
    public Context mContext;
    private DashboardModels dashboardSwtich;
    private List<MessageCDataTwo> itemListSearch;
    private SSLazyloaderSalesStocks lazyloaderSalesStock;
    Activity activity;
    int i;
    private static ClickListener clickListener;

    public  int counts=0;
public String TAG=getClass().getSimpleName();
    public MessageCenterAdapter(Context mContext, Activity act, List<MessageCDataTwo> procList) {
        this.mContext = mContext;
        this.messageList = procList;
        this.activity = act;
        layoutInflater = LayoutInflater.from(mContext);
        itemListSearch = new ArrayList<MessageCDataTwo>();
        itemListSearch.addAll(procList);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView asm_message,price_tv,day_text;
        public CircleImageView car_image;
        public RelativeLayout recycleview;
        public View line_v;

        public MyViewHolder(View view) {
            super(view);

            asm_message = (TextView) view.findViewById(R.id.asm_message);
            price_tv = (TextView) view.findViewById(R.id.price_tv);
            day_text = (TextView) view.findViewById(R.id.day_text);
            car_image = (CircleImageView) view.findViewById(R.id.car_image);
            recycleview = (RelativeLayout) view.findViewById(R.id.recycleview);
            line_v = (View) view.findViewById(R.id.line_v);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        lazyloaderSalesStock = (SSLazyloaderSalesStocks) activity;
        View itemView = layoutInflater.from(parent.getContext())
                .inflate(R.layout.row_messagecenter, parent, false);

        dashboardSwtich = ViewModelProviders.of((FragmentActivity) activity).get(DashboardModels.class);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        i = position;


        final MessageCDataTwo Data = messageList.get(position);

        /*counts = Data.getCount()!=null?Data.getCount():0;
        if(counts==0)holder.recycleview.setVisibility(View.GONE);
        if(counts==0)return;*/

        try {
            if (position % 2 == 1) {
                holder.recycleview.setBackgroundColor(activity.getResources().getColor(R.color.vlgray));

            } else {
                holder.recycleview.setBackgroundColor(activity.getResources().getColor(R.color.white));
            }
            // set data

            if( Data.getName().equalsIgnoreCase("Stocks without image")) {
                holder.asm_message.setText(Data.getMessage());
                holder.line_v.setBackgroundColor(Color.parseColor(Data.getColorcode()));
                holder.car_image.setImageResource(R.drawable.stock_icon_block);

            }else  if( Data.getName().equalsIgnoreCase("Pending Certification")) {
                holder.asm_message.setText(Data.getMessage());
                holder.line_v.setBackgroundColor(Color.parseColor(Data.getColorcode()));
                holder.car_image.setImageResource(R.drawable.stock_icon_block);
            }else  if( Data.getName().equalsIgnoreCase("Stock Pending Action")) {
                holder.asm_message.setText(Data.getMessage());
                holder.line_v.setBackgroundColor(Color.parseColor(Data.getColorcode()));
                holder.car_image.setImageResource(R.drawable.stock_icon_block);
            }else  if( Data.getName().equalsIgnoreCase("Warranty Balance")) {
                holder.asm_message.setText(Data.getMessage());
                holder.line_v.setBackgroundColor(Color.parseColor(Data.getColorcode()));
                holder.car_image.setImageResource(R.drawable.rupeee);
            }else{
                holder.asm_message.setText(Data.getMessage());
                holder.line_v.setBackgroundColor(Color.parseColor(Data.getColorcode()));
                holder.car_image.setImageResource(R.drawable.stock_icon_block);
            }

            holder.recycleview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    switchPages(v,position);

                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

        }

    private void switchPages(View view ,int position) {

        final MessageCDataTwo Data = messageList.get(position);
        try {

            MsgFilterData filter = new MsgFilterData();
            filter = Data.getFilter().get(0);


            clearDB();

        switch (Data.getName()){

            case "Stocks without image":


                CommonMethods.setvalueAgainstKey(activity, "statuslist", "asmstore");
                CommonMethods.setvalueAgainstKey(activity, "stockfilterOnclick", "true");
                CommonMethods.setvalueAgainstKey(activity, "column", filter.getColumn());
                CommonMethods.setvalueAgainstKey(activity, "operator", filter.getOperator());
                CommonMethods.setvalueAgainstKey(activity, "value", filter.getValue());

                dashboardSwtich.getdashboardSwtich().setValue("StockList");

                break;
            case "Pending Certification":

                CommonMethods.setvalueAgainstKey(activity, "statuslist", "asmstore");
                CommonMethods.setvalueAgainstKey(activity, "stockfilterOnclick", "true");
                CommonMethods.setvalueAgainstKey(activity, "column", filter.getColumn());
                CommonMethods.setvalueAgainstKey(activity, "operator", filter.getOperator());
                CommonMethods.setvalueAgainstKey(activity, "value", filter.getValue());

                dashboardSwtich.getdashboardSwtich().setValue("StockList");

                break;

            case "Stock Pending Action":

                CommonMethods.setvalueAgainstKey(activity, "statuslist", "asmstore");
                CommonMethods.setvalueAgainstKey(activity, "stockfilterOnclick", "true");
                CommonMethods.setvalueAgainstKey(activity, "column", filter.getColumn());
                CommonMethods.setvalueAgainstKey(activity, "operator", filter.getOperator());
                CommonMethods.setvalueAgainstKey(activity, "value", filter.getValue());

                dashboardSwtich.getdashboardSwtich().setValue("StockList");

                break;

                case "Booked Stock in last 2 weeks":

                    CommonMethods.setvalueAgainstKey(activity, "booknowbtn", "true");
                    CommonMethods.setvalueAgainstKey(activity, "statuslist", "asmbook");

                    CommonMethods.setvalueAgainstKey(activity, "column", "");
                    CommonMethods.setvalueAgainstKey(activity, "operator", "");
                    CommonMethods.setvalueAgainstKey(activity, "value", "");

                    clickListener.onItemClick(view, position,Data.getName());

                break;

                case "Used Car Sales Leads Created yesterday":

                    CommonMethods.setvalueAgainstKey(activity, "leadStatus", "true");

                    CommonMethods.setvalueAgainstKey(activity, "startDate", filter.getValue().toLowerCase());
                    CommonMethods.setvalueAgainstKey(activity, "endDate", filter.getValue().toLowerCase());


                    MsgFilterData filter2 = new MsgFilterData();
                    filter2 = Data.getFilter().get(1);

                    CommonMethods.setvalueAgainstKey(activity, "Lcolumn", filter2.getColumn());
                    CommonMethods.setvalueAgainstKey(activity, "Loperator", filter2.getOperator());
                    CommonMethods.setvalueAgainstKey(activity, "Lvalue", filter2.getValue().toLowerCase());

                    CommonMethods.setvalueAgainstKey(activity, "leads_status", Data.getName());
                    LeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());
                    LeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());

                    Log.i(TAG, "switchPages: "+filter2.getValue());

                    clickListener.onItemClick(view, position,Data.getName());

                break;



                case "Procurement Leads Created yesterday" :


                    CommonMethods.setvalueAgainstKey(activity, "leadStatus", "true");

                    CommonMethods.setvalueAgainstKey(activity, "leads_status", Data.getName());

                    CommonMethods.setvalueAgainstKey(activity, "startDate", filter.getValue().toLowerCase());
                    CommonMethods.setvalueAgainstKey(activity, "endDate", filter.getValue().toLowerCase());


                    MsgFilterData pfilter2 = new MsgFilterData();
                    pfilter2 = Data.getFilter().get(1);

                    CommonMethods.setvalueAgainstKey(activity, "Lcolumn", pfilter2.getColumn());
                    CommonMethods.setvalueAgainstKey(activity, "Loperator", pfilter2.getOperator());
                    CommonMethods.setvalueAgainstKey(activity, "Lvalue", pfilter2.getValue().toLowerCase());

                    Log.i(TAG, "switchPages: "+pfilter2.getValue());

                    ProcLeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());
                    ProcLeadFilterSaveInstance.getInstance().setStatusarray(new JSONArray());

                    clickListener.onItemClick(view, position,Data.getName());

                break;
                case "Used Car Sales Leads without Followup":

                    CommonMethods.setvalueAgainstKey(activity, "leadStatus", "true");

                    MsgFilterData follow1 = new MsgFilterData();
                    MsgFilterData follow2 = new MsgFilterData();

                    follow1 = Data.getFilter().get(0);
                    follow2 = Data.getFilter().get(1);

                    CommonMethods.setvalueAgainstKey(activity, "startDate", "");
                    CommonMethods.setvalueAgainstKey(activity, "endDate", "");


                    CommonMethods.setvalueAgainstKey(activity, "column", follow1.getColumn());
                    CommonMethods.setvalueAgainstKey(activity, "operator", follow1.getOperator());
                    CommonMethods.setvalueAgainstKey(activity, "value", follow1.getValue());

                    CommonMethods.setvalueAgainstKey(activity, "Lcolumn", follow2.getColumn());
                    CommonMethods.setvalueAgainstKey(activity, "Loperator", follow2.getOperator());
                    CommonMethods.setvalueAgainstKey(activity, "Lvalue", follow2.getValue());

                    CommonMethods.setvalueAgainstKey(activity, "withoutfollowup", "yes");


                    LeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());
                    LeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());


                    clickListener.onItemClick(view, position,Data.getName());

                break;
                case "Procurement Leads without Followup":

                    CommonMethods.setvalueAgainstKey(activity, "leadStatus", "true");

                    MsgFilterData pfollow1 = new MsgFilterData();
                    MsgFilterData pfollow2 = new MsgFilterData();

                    pfollow1 = Data.getFilter().get(0);
                    pfollow2 = Data.getFilter().get(1);

                    CommonMethods.setvalueAgainstKey(activity, "startDate", "");
                    CommonMethods.setvalueAgainstKey(activity, "endDate", "");

                    CommonMethods.setvalueAgainstKey(activity, "column", pfollow1.getColumn());
                    CommonMethods.setvalueAgainstKey(activity, "operator", pfollow1.getOperator());
                    CommonMethods.setvalueAgainstKey(activity, "value", pfollow1.getValue());

                    CommonMethods.setvalueAgainstKey(activity, "column2", pfollow2.getColumn());
                    CommonMethods.setvalueAgainstKey(activity, "operator2", pfollow2.getOperator());
                    CommonMethods.setvalueAgainstKey(activity, "value2", pfollow2.getValue());

                    CommonMethods.setvalueAgainstKey(activity, "withoutfollowup", "yes");
                    ProcLeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());
                    ProcLeadFilterSaveInstance.getInstance().setStatusarray(new JSONArray());


                    clickListener.onItemClick(view, position,Data.getName());

                break;
            case "Used Car Sales Leads Followup this week":


                CommonMethods.setvalueAgainstKey(activity, "leadStatus", "true");

                MsgFilterData wfollow1 = new MsgFilterData();
                MsgFilterData wfollow2 = new MsgFilterData();

                wfollow1 = Data.getFilter().get(0);
                wfollow2 = Data.getFilter().get(1);

                CommonMethods.setvalueAgainstKey(activity, "column", wfollow1.getColumn());
                CommonMethods.setvalueAgainstKey(activity, "operator", wfollow1.getOperator());
                CommonMethods.setvalueAgainstKey(activity, "value", wfollow1.getValue());

                CommonMethods.setvalueAgainstKey(activity, "column2", wfollow2.getColumn());
                CommonMethods.setvalueAgainstKey(activity, "operator2", wfollow2.getOperator());
                CommonMethods.setvalueAgainstKey(activity, "value2", wfollow2.getValue());

                CommonMethods.setvalueAgainstKey(activity, "leads_status", Data.getName());
                LeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());
                LeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());

                clickListener.onItemClick(view, position,Data.getName());

                break;
                case "Procurement Leads Followup this week":


                CommonMethods.setvalueAgainstKey(activity, "leadStatus", "true");
                    CommonMethods.setvalueAgainstKey(activity, "leads_status", Data.getName());

                MsgFilterData pwfollow1 = new MsgFilterData();
                MsgFilterData pwfollow2 = new MsgFilterData();

                    pwfollow1 = Data.getFilter().get(0);
                    pwfollow2 = Data.getFilter().get(1);

                CommonMethods.setvalueAgainstKey(activity, "column", pwfollow1.getColumn());
                CommonMethods.setvalueAgainstKey(activity, "operator", pwfollow1.getOperator());
                CommonMethods.setvalueAgainstKey(activity, "value", pwfollow1.getValue());

                CommonMethods.setvalueAgainstKey(activity, "column2", pwfollow2.getColumn());
                CommonMethods.setvalueAgainstKey(activity, "operator2", pwfollow2.getOperator());
                CommonMethods.setvalueAgainstKey(activity, "value2", pwfollow2.getValue());


                    ProcLeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());
                    ProcLeadFilterSaveInstance.getInstance().setStatusarray(new JSONArray());

                clickListener.onItemClick(view, position,Data.getName());

                break;

                case "Total Warranty Sales Within Week":

                    CommonMethods.setvalueAgainstKey(activity, "leadStatus", "true");

                    clickListener.onItemClick(view, position,Data.getName());

                break;
        }
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    public void setOnItemClickListener(ClickListener clickListener) {
        MessageCenterAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(View v, int position,String data);
    }


    @Override
    public int getItemCount() {
        return messageList.size();
    }


    private void clearDB() {

        CommonMethods.setvalueAgainstKey(activity, "column", "");
        CommonMethods.setvalueAgainstKey(activity, "operator", "");
        CommonMethods.setvalueAgainstKey(activity, "value", "");
        CommonMethods.setvalueAgainstKey(activity, "statuslist", "");

        CommonMethods.setvalueAgainstKey(activity, "photocounts", "false");
        CommonMethods.setvalueAgainstKey(activity, "thirtydaycheckbox", "false");
        CommonMethods.setvalueAgainstKey(activity, "certifiedstatus", "");

        CommonMethods.setvalueAgainstKey(activity, "filterapply", "");
        CommonMethods.setvalueAgainstKey(activity, "booknowbtn", "");
        CommonMethods.setvalueAgainstKey(activity, "unbooknowbtn", "");

        CommonMethods.setvalueAgainstKey(activity, "column2", "");
        CommonMethods.setvalueAgainstKey(activity, "operator2", "");
        CommonMethods.setvalueAgainstKey(activity, "value2", "");

        CommonMethods.setvalueAgainstKey(activity, "30Name","");
        CommonMethods.setvalueAgainstKey(activity, "messageCenter","true");

        CommonMethods.setvalueAgainstKey(activity, "leads_status", "");
        CommonMethods.setvalueAgainstKey(activity, "withoutfollowup", "");

        Stocklisting.getInstance().setStockstatus("");

    }

}