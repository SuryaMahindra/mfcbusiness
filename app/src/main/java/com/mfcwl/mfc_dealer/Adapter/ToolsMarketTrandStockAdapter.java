package com.mfcwl.mfc_dealer.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Fragment.ToolFragment;
import com.mfcwl.mfc_dealer.LazyLoader.ImageLoader;
import com.mfcwl.mfc_dealer.Model.ToolsModel;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchFlag;

/**
 * Created by sowmya on 24/4/18.
 */

public class ToolsMarketTrandStockAdapter extends RecyclerView.Adapter<ToolsMarketTrandStockAdapter.ViewHolder> {
    List<ToolsModel> list = new ArrayList<ToolsModel>();
    static Activity context;
    public static LinearLayout progress;
    boolean isLoading = false;
    public int myNum = 0;
    public static TextView dlrName, dlrNo, dlrLoc, dlrEmail;
    static String dlrname, dlrno, dlrloc, dlremail;
    static String dealer_code;
    private List<ToolsModel> itemListSearch;

    public ToolsMarketTrandStockAdapter(Activity context, List<ToolsModel> list) {
        this.list = list;
        ToolsMarketTrandStockAdapter.context = context;
        itemListSearch = new ArrayList<ToolsModel>();
        itemListSearch.addAll(list);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tools_market_trand_stocks_row, parent, false);
        progress = view.findViewById(R.id.progress);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        //String test = list.get(position);
        //holder.text.setText(test);

        ToolsModel sm = list.get(position);

        if (position >= getItemCount() - 1 && getItemCount() > 0 && !searchFlag) {
            isLoading = true;

            // progress.setVisibility(View.VISIBLE);

            ToolFragment.loadmore(getItemCount());
        }

/*        String createdDate = list.get(position).getPostedDate().toString();
        // Log.e("getPostedDate ","getPostedDate "+createdDate);
        String[] splitDateTime = createdDate.split("T", 10);
        String[] splitDate = splitDateTime[0].split("-", 10);
        String finalDate = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");
        Calendar c = Calendar.getInstance();
        String formattedDate = simpleDateFormat.format(c.getTime());
        String[] splitSpace = formattedDate.split(" ", 10);


        try {
            Date date1 = simpleDateFormat.parse(finalDate + " 00:00:00");
            Date date2 = simpleDateFormat.parse(splitSpace[0] + " 00:00:00");

            // Log.e("diff ","diff "+date1 +" "+date2 +"=="+printDifference(date1, date2));

            String day = "" + printDifference(date1, date2);

            if (!day.equalsIgnoreCase("")) {


                try {
                    myNum = Integer.parseInt(day);
                } catch (NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }
            }

            if (day.equalsIgnoreCase("0")) {
                holder.day_text.setText("" + "Today");
            } else if (day.equalsIgnoreCase("1")) {
                holder.day_text.setText("" + "Yesterday");
            } else if (myNum >= 365) {
                holder.day_text.setText("" + "1 Y ago");
            } else {
                holder.day_text.setText("" + printDifference(date1, date2) + " D ago");
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }*/


        holder.stockCarName.setText(sm.getMake().toString());
        holder.stockVariant.setText(list.get(position).getModel().toString() + " " +
                list.get(position).getVariant().toString());
        holder.sellingPrice.setText(list.get(position).getSellingPrice().toString());
        holder.dealerPrice.setText(list.get(position).getDealer_price().toString());
        holder.stockRegistration.setText(list.get(position).getRegistrationNumber().toString());
        holder.imageCount.setText(list.get(position).getPhotoCount().toString());
        holder.stockYear.setText(list.get(position).getManufacture_year().toString());
        holder.stockkms.setText(list.get(position).getKilometer().toString() + " " + "Kms");

        if (list.get(position).getIs_certified().equalsIgnoreCase("true")) {
            holder.stock_certified_toolbar.setVisibility(View.VISIBLE);
        } else {
            holder.stock_certified_toolbar.setVisibility(View.INVISIBLE);

        }

        if (list.get(position).getOwner().toString().equalsIgnoreCase("1") ||
                list.get(position).getOwner().toString().equalsIgnoreCase("")) {
            holder.stockOwner.setText(list.get(position).getOwner().toString() + " " + "Owner");
        } else {
            holder.stockOwner.setText(list.get(position).getOwner().toString() + " " + "Owners");
        }

        if (list.get(position).getCoverimage() == null) {

            Picasso.get().cancelRequest(holder.car_image);
            holder.car_image.setImageResource(R.drawable.no_car_small);

        } else {
            if (!list.get(position).getCoverimage().equalsIgnoreCase("")) {

                if (!list.get(position).getPhotoCount().toString().equals("-1") ||
                        !list.get(position).getPhotoCount().toString().equals("0")) {

                    Target target = new Target() {

                        @Override
                        public void onPrepareLoad(Drawable arg0) {

                        }

                        @Override
                        public void onBitmapLoaded(Bitmap arg0, Picasso.LoadedFrom arg1) {
                            holder.car_image.setImageBitmap(arg0);

                        }

                        @Override
                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                        }

                    };
                   /* Picasso.get()
                            .load(list.get(position).getCoverimage())
                            .placeholder(R.drawable.no_car_small)
                            .resize(250, 250)
                            .noFade()
                            .into(holder.car_image);*/

                    Picasso.get()
                            .load(Uri.parse(list.get(position).getCoverimage()))
                            .placeholder(R.drawable.no_car_small)
                            .resize(200, 200)
                            .noFade()
                            .into(target);

                } else {
                    Picasso.get().cancelRequest(holder.car_image);
                    holder.car_image.setImageResource(R.drawable.no_car_small);
                }
            } else {
                Picasso.get().cancelRequest(holder.car_image);
                holder.car_image.setImageResource(R.drawable.no_car_small);
            }
        }

        if (list.get(position).getCertifiedText() == null) {
            holder.stockCertified.setVisibility(View.INVISIBLE);
        } else {
            if (list.get(position).getIs_certified().toString().equals("1")) {
                holder.stockCertified.setText(list.get(position).getCertifiedText().toString());
                holder.stockCertified.setVisibility(View.VISIBLE);
            } else {
                holder.stockCertified.setVisibility(View.INVISIBLE);
            }
        }

        holder.getDealerDeatils.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(context, "dealer_code"), GlobalText.tool_dealer_details, GlobalText.android);

                String dealer_code = list.get(position).getDealerCode().trim().toString();
                //    Log.e("dealer_code ", "dealer_code " + dealer_code);
                WebServicesCall.webCall(context, context, jsonMake(dealer_code), "ToolsStockDetails", GlobalText.POST);

               /* CustomDialogClass customDialogClass = new CustomDialogClass(context, dealer_code);
                customDialogClass.show();*/
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView stockCarName, stockVariant, sellingPrice, stockRegistration, imageCount, dealerPrice,
                stockYear, stockkms, stockOwner, stockCertified, day_text, getDealerDeatils, stock_certified_toolbar;

        public RelativeLayout recycleview;
        public CircleImageView car_image;

        public ViewHolder(View itemView) {
            super(itemView);
            stockCarName = itemView.findViewById(R.id.asm_message);
            day_text = itemView.findViewById(R.id.day_text);
            stockVariant = itemView.findViewById(R.id.stock_variant);
            sellingPrice = itemView.findViewById(R.id.selling_price_value);
            dealerPrice = itemView.findViewById(R.id.textView2);

            stockRegistration = itemView.findViewById(R.id.sales_registration);
            imageCount = itemView.findViewById(R.id.image_number);
            stockYear = itemView.findViewById(R.id.stock_year);
            stockkms = itemView.findViewById(R.id.stock_kms);
            stockOwner = itemView.findViewById(R.id.stock_owner);
            stockCertified = itemView.findViewById(R.id.stock_certified);
            car_image = itemView.findViewById(R.id.car_image);
            getDealerDeatils = itemView.findViewById(R.id.get_dealer_details);
            stock_certified_toolbar = itemView.findViewById(R.id.stock_certified_toolbar);
            updateFontUI(context, itemView);
        }
    }

    public void updateFontUI(Activity context, View convert) {
        ArrayList<Integer> arl = new ArrayList<Integer>();
        arl.add(R.id.asm_message);
        arl.add(R.id.stock_variant);
      /*  arl.add(R.id.stock_registration);
        arl.add(R.id.stock_certified);*/

        setFontStyle(arl, context, convert);
    }

    private void setFontStyle(ArrayList<Integer> arl, Activity activity, View viewHolder) {
        for (int i = 0; i < arl.size(); i++) {
            Typeface myTypeface = Typeface.createFromAsset(activity.getAssets(), "lato_semibold.ttf");
            TextView myTextView = viewHolder.findViewById(arl.get(i));
            myTextView.setTypeface(myTypeface);
        }
    }

    public static class CustomDialogClass extends Dialog implements View.OnClickListener {

        public Activity activity;
        public Dialog d;
        public ImageView cross;
        public String dealer_code;

        public CustomDialogClass(@NonNull Activity context, String dealer_code) {
            super(context);
            this.activity = context;
            this.dealer_code = dealer_code;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.cross:
                    dismiss();
                    break;
                default:
                    break;
            }
            dismiss();
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.tools_dealer_detail_dialog);
            cross = findViewById(R.id.cross);

            dlrName = findViewById(R.id.dlrName);
            dlrNo = findViewById(R.id.dlrNo);
            dlrLoc = findViewById(R.id.dlrLoc);
            dlrEmail = findViewById(R.id.dlrEmail);
            Linkify.addLinks(dlrNo, Linkify.ALL);

            cross.setOnClickListener(this);

            // WebServicesCall.webCall(activity, activity, jsonMake(dealer_code), "ToolsStockDetails", GlobalText.POST);

            if (dlrno.equalsIgnoreCase("") || dlrno == null) {
                dlrno = "NA";
            }
            if (dlrloc.equalsIgnoreCase("") || dlrloc == null) {
                dlrloc = "NA";
            }

            if (dlremail.equalsIgnoreCase("") || dlremail == null) {
                dlremail = "NA";
            }

            dlrName.setText(dlrname);
            dlrNo.setText(dlrno);
            dlrLoc.setText(dlrloc);
            dlrEmail.setText(dlremail);
        }
    }

    public static void Parsetoolsstockdetails(JSONObject jObj, String strMethod) {

        Log.e("dealer_code ", "response " + jObj.toString());
        //dlrName,dlrNo,dlrLoc,dlrEmail

        try {
            if (jObj.getString("status").equalsIgnoreCase("SUCCESS")) {
                JSONObject object = jObj.getJSONObject("dealer");

                dlrname = object.getString("DealerName");
                dlrno = object.getString("Phone");
                dlrloc = object.getString("Location");
                dlremail = object.getString("Email");

               /* dlrName.setText(object.getString("DealerName"));
                dlrNo.setText(object.getString("Phone"));
                dlrLoc.setText(object.getString("Location"));
                dlrEmail.setText(object.getString("Email"));*/
                CustomDialogClass customDialogClass = new CustomDialogClass(context, dealer_code);
                customDialogClass.show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public static JSONObject jsonMake(String dealer_code) {

        JSONObject jObj = new JSONObject();
        try {

            jObj.put("dealercode", dealer_code);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jObj;
    }

    public void notifyDataChanged() {
        notifyDataSetChanged();
        //notifyItemInserted(getItemCount()-1);
        isLoading = false;
    }

    private void updateListImage(Context ctx, String imagepath, ImageView iv) {

        ImageLoader imageLoader = new ImageLoader(ctx);

        imageLoader.DisplayImage(CommonMethods.replaceStringURL(imagepath), iv);

    }

    public long printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        /*System.out.println("startDate : " + startDate);
        System.out.println("endDate : "+ endDate);
        System.out.println("different : " + different);*/

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        /*System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

        Log.e("elapsedDays ","elapsedDays "+elapsedDays);*/
        return elapsedDays;
    }


    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0) {
            list.addAll(itemListSearch);
        } else {
            for (ToolsModel it : itemListSearch) {
                if (it.getMake().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getModel().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getVariant().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getRegistrationNumber().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getRegistraionCity().toLowerCase(Locale.getDefault()).contains(charText)
                        ) {
                    list.add(it);
                }
            }
        }
        notifyDataSetChanged();

    }


}
