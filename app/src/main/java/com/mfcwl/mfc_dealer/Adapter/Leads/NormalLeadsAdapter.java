package com.mfcwl.mfc_dealer.Adapter.Leads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.Activity.Leads.LeadDetailsActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Fragment.Leads.NormalLeadsFragment;
import com.mfcwl.mfc_dealer.Model.Leads.NormalLeadModel;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.MonthUtility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.entersearch;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchFlag;

/**
 * Created by HP 240 G5 on 12-03-2018.
 */

public class NormalLeadsAdapter extends RecyclerView.Adapter<NormalLeadsAdapter.ViewHolder> {

    private final List<NormalLeadModel> list;
    private Activity activity;
    Context context;
    int lastItemPosition = -1;
    public static LinearLayout progress;
    boolean isLoading = false;
    private List<NormalLeadModel> itemListSearch;

    public NormalLeadsAdapter(Context context, List<NormalLeadModel> list, Activity a) {
        this.list = list;
        this.activity = a;
        this.context = context;

        itemListSearch = new ArrayList<NormalLeadModel>();
        itemListSearch.addAll(list);
    }

    @NonNull
    @Override
    public NormalLeadsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_normal_leads_item, parent, false);
        progress = view.findViewById(R.id.progress);
        return new NormalLeadsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        final NormalLeadModel normalLeadModel = this.list.get(position);

        if (position >= getItemCount() - 1 && getItemCount() > 0 && !searchFlag) {

            isLoading = true;

            //progress.setVisibility(View.VISIBLE);
            if (entersearch == false) {
                NormalLeadsFragment.loadmore(getItemCount());
            }

        }

        String createdDate = normalLeadModel.getLeadDate();
        String[] splitDateTime = createdDate.split(" ", 10);
        String[] splitDate = splitDateTime[0].split("-", 10);
        String finalDate = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");
        Calendar c = Calendar.getInstance();
        String formattedDate = simpleDateFormat.format(c.getTime());
        String[] splitSpace = formattedDate.split(" ", 10);

        try {
            Date date1 = simpleDateFormat.parse(finalDate + " 00:00:00");
            Date date2 = simpleDateFormat.parse(splitSpace[0] + " 00:00:00");

            if (printDifference(date1, date2) == 0) {
                holder.leads_days.setText("Today");
            } else if (printDifference(date1, date2) == 1) {
                holder.leads_days.setText("Yesterday");
            } else if (printDifference(date1, date2) > 364) {
                holder.leads_days.setText("" + printDifference(date1, date2) / 365 + " Y ago");
            } else {
                holder.leads_days.setText("" + printDifference(date1, date2) + " D ago");
            }


        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("ParseException ", "Normal " + e.toString());
        }


        holder.leads_name.setText(capitalize(normalLeadModel.getName().trim()));
        holder.leads_num.setText(normalLeadModel.getMobile().trim());
        holder.leads_comp_name.setText(normalLeadModel.getPrimary_make().trim());


        if (normalLeadModel.getName().trim().equalsIgnoreCase("NA")) {
            holder.leads_name.setText("Unknown");
        }

        holder.leads_car_model.setText(normalLeadModel.getPrimary_model().trim() + " " + normalLeadModel.getPrimary_variant().trim());


        try {
            String[] splitdatespace = normalLeadModel.getFollowUpDate().split(" ", 10);
            String[] splitdate1 = splitdatespace[0].split("-", 10);
            holder.leads_date.setText(splitdate1[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate1[1])] + " " + splitdate1[0]);
        } catch (Exception e) {

            holder.leads_date.setText(normalLeadModel.getFollowUpDate().trim());
        }
        //holder.leads_date.setText(normalLeadModel.getFollowUpDate());
        holder.leads_followup_history_warmorcold.setVisibility(View.VISIBLE);
        holder.leads_followup_history_warmorcold.setText(capitalize(normalLeadModel.getStatus().trim()));
        if (normalLeadModel.getStatus().equalsIgnoreCase("Open")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.open_28);
            holder.leads_followup_history_warmorcold.setText("  " + capitalize(normalLeadModel.getStatus().trim()));
            //holder.leads_followup_history_warmorcold.setVisibility(View.INVISIBLE);
        }
        if (normalLeadModel.getStatus().equalsIgnoreCase("") || normalLeadModel.getStatus().equalsIgnoreCase("null")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.open_28);
            holder.leads_followup_history_warmorcold.setText(" Open");
            //holder.leads_followup_history_warmorcold.setVisibility(View.INVISIBLE);
        }
        if (normalLeadModel.getStatus().equalsIgnoreCase("Hot")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.hot_28);
        }
        if (normalLeadModel.getStatus().equalsIgnoreCase("Warm")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.warm_28);
            holder.leads_followup_history_warmorcold.setText("  " + capitalize(normalLeadModel.getStatus().trim()));
        }
        if (normalLeadModel.getStatus().equalsIgnoreCase("Cold")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.cold_28);
        }
        if (normalLeadModel.getStatus().equalsIgnoreCase("Lost")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.lost_28);
        }
        if (normalLeadModel.getStatus().equalsIgnoreCase("Sold")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.sold_28);
        }

        if (normalLeadModel.getStatus().equalsIgnoreCase("null")) {
            holder.leads_followup_history_warmorcold.setText("NA");
        }
        if (normalLeadModel.getFollowUpDate().trim().equalsIgnoreCase("null") || normalLeadModel.getFollowUpDate().equalsIgnoreCase("0000-00-00 00:00:00")) {
            holder.leads_date.setText("NA");

            //Added Condition For If Date is Null or NA
/*
            final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
             Date date1 = null;
            String[] splitdatespaceLead = new String[0];
            try {
                splitdatespaceLead = normalLeadModel.getLeadDate().split(" ", 10);

            } catch (Exception e) {

                holder.leads_date.setText(normalLeadModel.getLeadDate().trim());
            }

            try {
                date1 = format.parse(splitdatespaceLead[0]);
            } catch (ParseException e) {
                e.printStackTrace();
                Log.e("date"," ParseException "+e.toString());

            }
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(date1);
            calendar.add(Calendar.DAY_OF_YEAR, 1);

            try {

                String[] splitdate3 = format.format(calendar.getTime()).split("-", 10);
                holder.leads_date.setText(splitdate3[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate3[1])] + " " + splitdate3[0]);
            } catch (Exception e) {

                holder.leads_date.setText(normalLeadModel.getLeadDate().trim());
            }*/

            if (holder.leads_name.getText().toString().trim().equalsIgnoreCase("NA") || holder.leads_name.getText().toString().trim().equalsIgnoreCase("") || holder.leads_name.getText().toString().trim().equalsIgnoreCase("null")) {
                holder.leads_name.setText("Unknown");
            }

            //holder.leads_date.setText(format.format(calendar.getTime()));

        }


        holder.leads_Call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.web_leads_call, GlobalText.android);

                Intent callIntent = new Intent(Intent.ACTION_VIEW);
                callIntent.setData(Uri.parse("tel:" + normalLeadModel.getMobile().trim()));//change the number
                context.startActivity(callIntent);
            }
        });
        holder.leads_Messege.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.web_leads_message, GlobalText.android);

                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:" + normalLeadModel.getMobile().trim()));
                sendIntent.putExtra("sms_body", "");
                context.startActivity(sendIntent);

            }
        });


        holder.leads_Whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.web_leads_whatsapp, GlobalText.android);

                openWhatsApp(normalLeadModel.getMobile().trim());
            }
        });


        /*holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, LeadDetailsActivity.class);
                Bundle bundle = new Bundle();
                if (normalLeadModel.getName().trim().equalsIgnoreCase("NA") || normalLeadModel.getName().trim().equalsIgnoreCase("") || normalLeadModel.getName().trim().equalsIgnoreCase("null")) {
                    bundle.putString("NM", "UnKnown");
                } else {
                    bundle.putString("NM", normalLeadModel.getName().trim());
                }

                //  Log.e("followcomments ", "remark " + normalLeadModel.getRemark().trim());

                bundle.putString("MN", normalLeadModel.getMobile().trim());
                bundle.putString("EM", normalLeadModel.getEmail().trim());
                bundle.putString("LS", normalLeadModel.getStatus().trim());
                bundle.putString("PD", normalLeadModel.getPostedDate().trim());
                bundle.putString("MK", normalLeadModel.getPrimary_make().trim());
                bundle.putString("MD", normalLeadModel.getPrimary_model().trim());
                bundle.putString("MV", normalLeadModel.getPrimary_variant().trim());
                bundle.putString("PR", normalLeadModel.getPrice().trim());
                bundle.putString("RN", normalLeadModel.getRegno().trim());
                bundle.putString("NFD", normalLeadModel.getFollowUpDate().trim());

                if (normalLeadModel.getExecutiveName() == null) {
                    bundle.putString("ENM", "NA");
                } else {
                    bundle.putString("ENM", normalLeadModel.getExecutiveName().toString().trim());

                }

                bundle.putString("RGC", normalLeadModel.getRegister_city().trim());
                bundle.putString("OWN", normalLeadModel.getOwners() + "");
                bundle.putString("MYR", normalLeadModel.getRegyear().trim());
                bundle.putString("CLR", normalLeadModel.getColor().trim());
                bundle.putString("KMS", normalLeadModel.getKms() + "");
                bundle.putString("DID", normalLeadModel.getDispatchId() + "");
                bundle.putString("RMK", normalLeadModel.getRemark().trim());
                bundle.putString("LD", holder.leads_days.getText().toString().trim());
                bundle.putString("MFY", normalLeadModel.getMfgyear().trim());
                bundle.putString("IDL", normalLeadModel.getLeadid() + "");
                CommonMethods.setvalueAgainstKey(activity, "WLEADS", "WebLeads");
                intent.putExtras(bundle);
                context.startActivity(intent);

            }
        });*/   // Commented by Sangeetha - Because there is no need to display lead details onClick of listItem
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView leads_name, leads_num, leads_comp_name, leads_car_model, leads_date, leads_days, leads_followup_history_warmorcold;
        public CircleImageView leads_Call, leads_Messege, leads_Whatsapp;

        public ViewHolder(View itemView) {
            super(itemView);
            leads_name = itemView.findViewById(R.id.leads_name);
            leads_num = itemView.findViewById(R.id.leads_num);
            leads_comp_name = itemView.findViewById(R.id.leads_comp_name);
            leads_car_model = itemView.findViewById(R.id.leads_car_model);
            leads_date = itemView.findViewById(R.id.leads_date);
            leads_days = itemView.findViewById(R.id.leads_days);
            leads_followup_history_warmorcold = itemView.findViewById(R.id.leads_followup_history_warmorcold);



            leads_Call = itemView.findViewById(R.id.leads_Call);
            leads_Messege = itemView.findViewById(R.id.leads_Messege);
            leads_Whatsapp = itemView.findViewById(R.id.leads_Whatsapp);

            if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {

            }else{
                leads_Call.setVisibility(View.INVISIBLE);
                leads_Messege.setVisibility(View.INVISIBLE);
                leads_Whatsapp .setVisibility(View.INVISIBLE);

            }

        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void notifyDataChanged() {
        notifyDataSetChanged();
        isLoading = false;
    }


    public long printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        /*System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);*/

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;


        return elapsedDays;
    }


    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0) {
            list.addAll(itemListSearch);
        } else {
            for (NormalLeadModel it : itemListSearch) {
                if (it.getPrimary_make().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getPrimary_model().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getPrimary_variant().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getName().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getMobile().toLowerCase(Locale.getDefault()).contains(charText)
                        ) {
                    list.add(it);
                }
            }
        }
        notifyDataSetChanged();

    }


    private void openWhatsApp(String number) {
        String smsNumber = "91" + number; //without '+'
        try {

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_VIEW);
            String url = "https://api.whatsapp.com/send?phone=" + smsNumber + "&text=" + "";
            sendIntent.setData(Uri.parse(url));
            activity.startActivity(sendIntent);

        } catch (Exception e) {
            Toast.makeText(context, "Error/n" + e.toString(), Toast.LENGTH_SHORT).show();
        }

    }


    private String capitalize(String capString) {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()) {
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }

}

