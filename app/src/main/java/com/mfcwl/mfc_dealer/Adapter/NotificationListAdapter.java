package com.mfcwl.mfc_dealer.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.Activity.LeadSection.LeadConstantsSection;
import com.mfcwl.mfc_dealer.Activity.Leads.FilterInstance;
import com.mfcwl.mfc_dealer.Activity.Leads.LeadConstants;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragmentNew;
import com.mfcwl.mfc_dealer.InstanceCreate.LeadSection.LeadFilterSaveInstance;
import com.mfcwl.mfc_dealer.InstanceCreate.LeadSection.SortInstanceLeadSection;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;

import org.json.JSONArray;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.resetMethodForLeads;
import static com.mfcwl.mfc_dealer.Activity.StockFilterActivity.stock_photocountsFlag;

/**
 * Created by sowmya on 10/4/18.
 */

public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.ViewHolder> {
    ArrayList<String> animalNames = new ArrayList<>();
    ArrayList<String> datetime = new ArrayList<>();
    public Activity notificationListActivity;
    private SharedPreferences mSharedPreferences = null;
    private SharedPreferences mSharedPreferencesLead = null;

    public NotificationListAdapter(Activity notificationListActivity, ArrayList<String> animalNames, ArrayList<String> date) {
        this.animalNames = animalNames;
        this.datetime = date;
        this.notificationListActivity = notificationListActivity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mSharedPreferences = notificationListActivity.getSharedPreferences("MFC", Context.MODE_PRIVATE);
        mSharedPreferencesLead = notificationListActivity.getSharedPreferences("MFCB", Context.MODE_PRIVATE);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_row, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        try {
            String s = animalNames.get(position);
            String v = datetime.get(position);


            Log.e("datetime", "datetime=" + datetime);


            if (v.equalsIgnoreCase("") || v.equalsIgnoreCase("null")) {
                v = "Now";
            }

            holder.tv_datetime.setText(" " + v);
            holder.notificationlisttext.setText(s);

            holder.notifications.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //  Log.e("notification type text", s);

                    try {

                        if (s.contains("new lead")) {
                            clearFilterLeads(notificationListActivity);
                            MainActivity.leadsreset("Sales Lead");
                            resetMethodForLeads();
                            // MyFirebaseMessagingService.Notification_Status="lead";
                            CommonMethods.setvalueAgainstKey(notificationListActivity, "Notification_Switch", "lead");

                            // CommonMethods.setvalueAgainstKey(notificationListActivity, "Notification_Type", "LeadFrag");
                            FilterInstance.getInstance().setNotification("new lead");
                            Intent in_main = new Intent(notificationListActivity, MainActivity.class);
                            notificationListActivity.startActivity(in_main);

                        } else if (s.contains("missing images") || s.contains("missing")) {

                            MainActivity.stockreset();
                            resetMethodForLeads();
                            //MyFirebaseMessagingService.Notification_Status="AgeingInventory";
                            CommonMethods.setvalueAgainstKey(notificationListActivity, "filterapply", "false");
                            CommonMethods.setvalueAgainstKey(notificationListActivity, "Notification_Switch", "noStockImage");
                            CommonMethods.setvalueAgainstKey(notificationListActivity, "temp_Switch", "noStockImage");
                            CommonMethods.setvalueAgainstKey(notificationListActivity, "photocounts", "true");

                            CommonMethods.setvalueAgainstKey(notificationListActivity, "stockfilterOnclick", "true");

                            FilterInstance.getInstance().setNotification("missing images");
                            Intent in_main = new Intent(notificationListActivity, MainActivity.class);
                            notificationListActivity.startActivity(in_main);

                        } else if (s.contains("old")) {

                            MainActivity.stockreset();
                            resetMethodForLeads();

                            stock_photocountsFlag = false;
                            CommonMethods.setvalueAgainstKey(notificationListActivity, "photocounts", "false");
                            CommonMethods.setvalueAgainstKey(notificationListActivity, "temp_Switch", "thirtydaycheckbox");
                            CommonMethods.setvalueAgainstKey(notificationListActivity, "Notification_Switch", "AgeingInventory");
                            CommonMethods.setvalueAgainstKey(notificationListActivity, "filterapply", "false");
                            CommonMethods.setvalueAgainstKey(notificationListActivity, "thirtydaycheckbox", "false");

                            FilterInstance.getInstance().setNotification("old");

                            CommonMethods.setvalueAgainstKey(notificationListActivity, "stockfilterOnclick", "true");

                            Intent in_main = new Intent(notificationListActivity, MainActivity.class);
                            notificationListActivity.startActivity(in_main);

                        } else if (s.contains("Follow") || s.contains("followed") || s.contains("Follow up")) {
                            clearFilterLeads(notificationListActivity);
                            saveTodayFollowupLeads(notificationListActivity);
                            MainActivity.leadsreset("Sales Lead");
                            resetMethodForLeads();
                            //MyFirebaseMessagingService.Notification_Status="leadFollowUp";
                            CommonMethods.setvalueAgainstKey(notificationListActivity, "Notification_Switch", "leadFollowUp");
                            //NotificationInstance.getInstance().setTodayfollowdate(CalendarTodayDate());
                            SharedPreferences.Editor mEdit = mSharedPreferences.edit();
                            mEdit.putString(LeadConstants.LEAD_FOLUP_TODAY, CalendarTodayDate());
                            mEdit.commit();
                            ClearOtherFilterData();
                            FilterInstance.getInstance().setNotification("Follow");
                            Intent in_main = new Intent(notificationListActivity, MainActivity.class);
                            notificationListActivity.startActivity(in_main);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            });
        } catch (Exception e) {

        }

    }

    private void saveTodayFollowupLeads(Activity notificationListActivity) {
        LeadConstantsSection leadConstantsSection = new LeadConstantsSection();
        HashMap<String, String> followupTodayDate = new HashMap<String, String>();

        followupTodayDate.put(LeadConstantsSection.LEAD_FOLUP_TODAY, leadConstantsSection.getFollowtodayDate());
        LeadFilterSaveInstance.getInstance().setSavedatahashmap(followupTodayDate);

        SharedPreferences.Editor mEditor = mSharedPreferencesLead.edit();
        Gson gson = new Gson();
        String follupDate = gson.toJson(followupTodayDate);
        mEditor.putString(LeadConstantsSection.LEAD_SAVE_DATE, follupDate);
        mEditor.commit();
    }

    private void clearFilterLeads(Activity notificationListActivity) {
        SortInstanceLeadSection.getInstance().setSortby("leads.created_at");
        SharedPreferences.Editor mEditor = mSharedPreferencesLead.edit();
        mEditor.putString(LeadConstantsSection.LEAD_SAVE_DATE, "");
        mEditor.putString(LeadConstantsSection.LEAD_SAVE_STATUS, "");
        LeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());
        LeadFilterSaveInstance.getInstance().setStatusarray(new JSONArray());
        mEditor.clear();
        mEditor.commit();
    }

    String todayfromDate = "", todaytoDate = "";

    public String CalendarTodayDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);

        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, 1);
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        todayfromDate = s.format(new Date(cal.getTimeInMillis()));
        todaytoDate = s.format(new Date(cal1.getTimeInMillis()));
        return todayfromDate + "@" + todaytoDate;
    }


    @Override
    public int getItemCount() {
        return animalNames.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView notificationlisttext, tv_datetime;
        public ImageView bell_icon;
        public LinearLayout notifications;

        public ViewHolder(View itemView) {
            super(itemView);
            notificationlisttext = itemView.findViewById(R.id.notification_text);
            tv_datetime = itemView.findViewById(R.id.datetime);
            bell_icon = itemView.findViewById(R.id.bell_icon);
            notifications = itemView.findViewById(R.id.notifications);


        }
    }

    private void ClearOtherFilterData() {
        SharedPreferences.Editor mEditor0 = mSharedPreferences.edit();
        mEditor0.putString(LeadConstants.LEAD_POST_TODAY, "");
        //clear others
        mEditor0.putString(LeadConstants.LEAD_POST_CUSTDATE, "");
        mEditor0.putString(LeadConstants.LEAD_POST_YESTER, "");
        mEditor0.putString(LeadConstants.LEAD_POST_7DAYS, "");
        mEditor0.putString(LeadConstants.LEAD_POST_15DAYS, "");

        mEditor0.putString(LeadConstants.LEAD_FOLUP_TMRW, "");
        //mEditor0.putString(LeadConstants.LEAD_FOLUP_TODAY, "");//because set Follow-Up date other Filter
        mEditor0.putString(LeadConstants.LEAD_FOLUP_YESTER, "");
        mEditor0.putString(LeadConstants.LEAD_FOLUP_7DAYS, "");
        mEditor0.putString(LeadConstants.LEAD_FOLUP_CUSTDATE, "");
        mEditor0.putString(LeadConstants.LEAD_STATUS_INFO, "");

        mEditor0.commit();

        try {
            LeadFilterFragmentNew.leadstatusJsonArray = new JSONArray();
        } catch (Exception e) {

        }


    }
}
