package com.mfcwl.mfc_dealer.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mfcwl.mfc_dealer.Activity.GalleryActivity;
import com.mfcwl.mfc_dealer.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by sowmya on 13/3/18.
 */

public class CustomPagerAdapter extends PagerAdapter {
    public Context mContext;
    LayoutInflater mLayoutInflater;
    public Activity activitys;
    private ArrayList<String> mListImages;


    String[] imageurl = {""};

    public CustomPagerAdapter(Activity activity, String[] imageurl) {
        mContext = activity;
        activitys = activity;
        this.imageurl = imageurl;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mListImages = new ArrayList<>();


    }


    @Override
    public int getCount() {
        return imageurl.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.stock_img_pager_item, container, false);

        ImageView imageView = itemView.findViewById(R.id.imageView);

        if (imageurl[0].equalsIgnoreCase("url")) {
            imageView.setBackgroundResource(R.drawable.button_shape_black);
        } else {

            Picasso.Builder builder = new Picasso.Builder(mContext);
            builder.listener(new Picasso.Listener() {
                @Override
                public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                    exception.printStackTrace();
                }

            });

            //  Log.e("imageurl[position]","imageurl[position]="+imageurl[position]);

            builder.build()
                    .load(imageurl[position])
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .placeholder(R.drawable.ic_loadings)
                    .noFade()
                    .into(imageView);

          /*  Picasso
                    .with(mContext)
                    .load(imageurl[position])
                    .placeholder(R.drawable.small_loading)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(imageView);*/


        /*    Glide.with(mContext)
                    .load(imageurl[position])
                    .placeholder(R.drawable.small_loading)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(imageView);*/

            Log.e("position", "position=" + position);

            //  StockStoreInfoActivity.postions(position);
        }

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListImages.clear();
                for (int i = 0; i < imageurl.length; i++) {
                    mListImages.add(imageurl[i]);
                }

                Intent intent = new Intent(activitys, GalleryActivity.class);
                intent.putExtra(GalleryActivity.ARG_IMAGES, mListImages);
                activitys.startActivity(intent);

               /* String url = "";
                url = imageurl[position];
                Log.e("OnClickListener", "OnClickListener=" + position + "url" + url);
                if (!imageurl[0].equalsIgnoreCase("url")) {
                    Intent intent = new Intent(activitys, SimpleSampleActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("URL", url);
                    intent.putExtras(bundle);
                    activitys.startActivity(intent);
                }*/
            }
        });


        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout) object);
    }
}
