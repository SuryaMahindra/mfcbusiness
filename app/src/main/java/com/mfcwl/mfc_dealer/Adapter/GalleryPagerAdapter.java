package com.mfcwl.mfc_dealer.Adapter;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;


import com.mfcwl.mfc_dealer.Fragment.GalleryFragment;

import java.lang.ref.WeakReference;

/**
 * Created by Surya on 12/06/2018.
 */
public class GalleryPagerAdapter extends FragmentStatePagerAdapter {

    private SparseArray<WeakReference<Fragment>> mPageReferenceMap = new SparseArray<WeakReference<Fragment>>();

    private int nbItems;

    public GalleryPagerAdapter(FragmentManager fm, int nbItems) {
        super(fm);
        this.nbItems = nbItems;
    }

    @Override
    public Parcelable saveState(){ return null;}

    @Override
    public Fragment getItem(int position) {
        return getFragment(position);
    }

    public Fragment getFragment(int key){
        WeakReference<Fragment> weakReference = mPageReferenceMap.get(key);

        if (null != weakReference)
            return weakReference.get();
        else
            return null;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object){

        super.destroyItem(container, position, object);
        mPageReferenceMap.remove(Integer.valueOf(position));
    }

    @Override
    public int getItemPosition(Object object) { return POSITION_NONE;}

    public Object instantiateItem(ViewGroup container, int position){

        Fragment fragment = new GalleryFragment();

        Bundle args = new Bundle();
        args.putInt(GalleryFragment.ARG_ITEM_POSITION, position);
        args.putInt(GalleryFragment.ARG_NUMBER_ITEM, getCount());

        fragment.setArguments(args);

        mPageReferenceMap.put(Integer.valueOf(position), new WeakReference<Fragment>(fragment));

        return super.instantiateItem(container, position);
    }

    @Override
    public int getCount() {
        return nbItems;
    }
}
