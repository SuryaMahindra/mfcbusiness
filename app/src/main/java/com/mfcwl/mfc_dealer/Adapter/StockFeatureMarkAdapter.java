package com.mfcwl.mfc_dealer.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.Activity.StockStoreInfoActivity;
import com.mfcwl.mfc_dealer.Model.StockModel;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sowmya on 15/3/18.
 */

public class StockFeatureMarkAdapter extends RecyclerView.Adapter<StockFeatureMarkAdapter.ViewHolder> {
    List<StockModel> addList = new ArrayList<>();
    //Context context;
    public static RelativeLayout marklist;

    public static String make;
    public static String model;
    public static String variant;
    public static String registraionCity;
    public static String registraionNo;
    public static String color;
    public static String certified;
    public static String owner;
    public static String kms;
    public static String certifiedNo;
    public static String sellingPrice;
    public static String yearMonth;
    public static String insurance;
    public static String insuranceExp_date;
    public static String selling_price;
    public static String Regyear;
    public static String source;
    public static String warranty;
    public static String surveyor_remark;
    public static String photo_count;
    public static String reg_month;

    public static String fuel_type;
    public static String bought;
    public static String refurbishment_cost;
    public static String dealer_price;
    public static String ProcurementExecId;
    public static String cng_kit;
    public static String chassis_number;
    public static String engine_number;

    //public static String is_display= stockModel.getIs_display().toString();
    public static String is_certified;
    public static String is_featured_car;
    public static String is_featured_car_admin;
    public static String procurement_executive_name;
    public static String private_vehicle;
    public static String comments;
    public static String finance_required;
    public static String manufacture_month;
    public static String sales_executive_id;
    public static String sales_executive_name;
    public static String manufacture_year;
    public static String actual_selling_price;
    public static String CertifiedText;
    public static String IsDisplay;
    public static String TransmissionType;
    public static String colour;
    public static String dealer_code;
    public static String is_offload;
    public static String posted_date;
    public static String stock_id;
    public static String is_booked;
    public static String is_display;

    public static int pos;

    public static boolean deleteBtn = false;

    public static JSONArray images;

    Activity context;
    StockModel stockModel;

    ViewHolder holder;

    public StockFeatureMarkAdapter(List<StockModel> addList, Activity context) {
        this.addList = addList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.stock_fea_mark_row, parent, false);

        marklist = view.findViewById(R.id.marklist);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        this.holder = holder;
        StockModel stockModel = addList.get(position);
        holder.stockFeaRmake.setText(stockModel.getMake());
        holder.stockFeaRmode.setText(stockModel.getModel() + " " + stockModel.getVariant());
        holder.stockFeaRreg.setText(stockModel.getRegistrationNumber());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        holder.stock_fea_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteBtn = true;
                CustomDialogClass customDialogClass = new CustomDialogClass(context, position, stockModel);
                customDialogClass.show();

               /* make = stockModel.getMake().toString();
                model = stockModel.model.toString() *//*+ " " +stockModel.getVariant().toString()*//*;
                variant = stockModel.getVariant().toString();
                registraionCity = stockModel.getRegistraionCity();
                registraionNo = stockModel.getRegistrationNumber();
                color = stockModel.getColour().toString();
                certified = stockModel.getCertifiedText();
                owner = stockModel.getOwner().toString();
                kms = stockModel.getKilometer().toString();
                certifiedNo = stockModel.getCertificationNumber().toString();
                sellingPrice = stockModel.getSellingPrice().toString();
                yearMonth = stockModel.getRegMonth().toString() + " " + stockModel.getRegYear().toString();
                insurance = stockModel.getInsurance().toString();

                if(insurance.equalsIgnoreCase("")){
                    insurance="NA";
                }
                insuranceExp_date = stockModel.getInsuranceExpDate().toString();
                selling_price = stockModel.getSellingPrice().toString();
                Regyear = stockModel.getRegYear().toString();
                source = stockModel.getSource().toString();
                warranty = stockModel.getWarranty().toString();
                surveyor_remark = stockModel.getSurveyorRemark().toString();
                photo_count = stockModel.getPhotoCount().toString();
                reg_month = stockModel.getRegMonth().toString();

                fuel_type = stockModel.getFuel_type().toString();
                bought = stockModel.getBought().toString();
                refurbishment_cost = stockModel.getRefurbishment_cost().toString();
                dealer_price = stockModel.getDealer_price().toString();
                ProcurementExecId = stockModel.getProcurementExecId().toString();
                cng_kit = stockModel.getCng_kit().toString();
                chassis_number = stockModel.getChassis_number().toString();
                engine_number = stockModel.getEngine_number().toString();

                // String is_display= stockModel.getIs_display().toString();
                is_certified= stockModel.getIs_certified().toString();
                is_featured_car= stockModel.getIs_featured_car().toString();
                is_featured_car_admin= stockModel.getIs_featured_car_admin().toString();
                procurement_executive_name= stockModel.getProcurement_executive_name().toString();
                private_vehicle= stockModel.getPrivate_vehicle().toString();
                comments= stockModel.getComments().toString();
                finance_required= stockModel.getFinance_required().toString();
                manufacture_month= stockModel.getManufacture_month().toString();
                sales_executive_id= stockModel.getSales_executive_id().toString();
                sales_executive_name= stockModel.getSales_executive_name().toString();
                manufacture_year= stockModel.getManufacture_year().toString();
                actual_selling_price= stockModel.getActual_selling_price().toString();
                CertifiedText= stockModel.getCertifiedText().toString();
                IsDisplay= stockModel.getIsDisplay().toString();
                TransmissionType= stockModel.getTransmissionType().toString();
                colour= stockModel.getColour().toString();
                dealer_code= stockModel.getDealerCode().toString();
                is_offload= stockModel.getIsOffload().toString();
                posted_date= stockModel.getPostedDate().toString();
                stock_id= stockModel.getId().toString();
                is_booked= stockModel.getIsbooked().toString();
                is_display= "true";

                WebServicesCall.webCall(context, context, deleteFeaturedMakeBookNow(), "deleteFeatureCar", GlobalText.PUT);


                addList.remove(holder.getPosition());
                notifyDataSetChanged();*/
            }
        });

    }


    @Override
    public int getItemCount() {
        return addList.size();
    }

    public void addItem(StockModel stockModel) {
        addList.add(stockModel);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView stockFeaRmake, stockFeaRmode, stockFeaRreg;
        ImageView stock_fea_delete;

        public ViewHolder(View itemView) {
            super(itemView);

            stockFeaRmake = itemView.findViewById(R.id.stock_fea_r_make);
            stockFeaRmode = itemView.findViewById(R.id.stock_fea_r_model_var);
            stockFeaRreg = itemView.findViewById(R.id.stock_fea_r_reg);
            stock_fea_delete = itemView.findViewById(R.id.stock_fea_delete);

            ArrayList<Integer> arl = new ArrayList<>();
            arl.add(R.id.stock_fea_make);


        }
    }

    private void setFontStyle(ArrayList<Integer> tv_list, Context ctx, ViewHolder viewHolder, View convertView) {

        for (int i = 0; i < tv_list.size(); i++) {
            Typeface myTypeface = Typeface.createFromAsset(ctx.getAssets(), "arialbi.ttf");
            TextView myTextView = convertView.findViewById(tv_list.get(i));
            myTextView.setTypeface(myTypeface);
        }


    }


    public static void PermissionDear(JSONObject jObj, String strMethod, Activity activity) {

        Log.e("CreatedStock ", "Parseaddregularstock " + jObj.toString());

        try {
            Log.e("fearted data res", jObj.toString());
            if (jObj.getString("status").equals("SUCCESS")) {

                StockStoreInfoActivity.white_star();
                if (!deleteBtn) activity.finish();

                Toast.makeText(activity, "Successfully unmarked", Toast.LENGTH_SHORT).show();
            } else {
                Log.e("SUCCESS -->> ", "SUCCESS-2");
                Toast.makeText(activity, jObj.getString("Message"), Toast.LENGTH_SHORT).show();
                CommonMethods.getstringvaluefromkey(activity, jObj.getString("Message"));
            }
        } catch (Exception ex) {
            Log.e("Parsestockstore-Ex", ex.toString());
        }

    }

    public static JSONObject deleteFeaturedMakeBookNow() {
        JSONObject jObj = new JSONObject();

        try {

            jObj.put("stock_id", stock_id);
            jObj.put("stock_source", source);
            jObj.put("posted_date", posted_date);
            jObj.put("vehicle_make", make);
            jObj.put("vehicle_model", model);
            jObj.put("vehicle_variant", variant);
            jObj.put("reg_month", MethodofMonth_Num(reg_month));
            jObj.put("reg_year", Regyear);
            jObj.put("registraion_city", registraionCity);
            jObj.put("registration_number", registraionNo);

            jObj.put("colour", colour);
            jObj.put("kilometer", kms);
            jObj.put("owner", owner);

            if (insurance.equalsIgnoreCase("")) {
                insurance = "NA";
            }


            jObj.put("insurance", insurance);

            if (insurance.equalsIgnoreCase("NA")) {
                jObj.put("insurance_exp_date", "");

            } else {
                jObj.put("insurance_exp_date", insuranceExp_date);

            }

            jObj.put("selling_price", selling_price);
            jObj.put("is_display", is_display);
            jObj.put("bought_price", bought);
            jObj.put("refurbishment_cost", refurbishment_cost);
            jObj.put("procurement_executive_id", "40");
            jObj.put("cng_kit", cng_kit);
            jObj.put("private_vehicle", private_vehicle);
            jObj.put("comments", comments);
            jObj.put("manufacture_month", MethodofMonth_Num(manufacture_month));
            jObj.put("is_offload", is_offload);
            jObj.put("manufacture_year", manufacture_year);
            jObj.put("updated_by_device", "MOBILE");
            jObj.put("is_featured_car", "false");
            jObj.put("is_booked", is_booked);
            jObj.put("actual_selling_price", "2850000");
            jObj.put("sales_executive_id", 12);
            jObj.put("finance_required", "true");

            jObj.put("chassis_number", chassis_number);
            jObj.put("engine_number", engine_number);
            jObj.put("dealer_price", dealer_price);
            jObj.put("dealer_code", dealer_code);

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("respose fe", e.getMessage());
        }
        return jObj;

    }


    public class CustomDialogClass extends Dialog implements View.OnClickListener {

        public Activity activity;
        public Dialog d;
        public Button yes, no;
        // ViewHolder viewHolder;

        StockModel stockModel;

        public CustomDialogClass(@NonNull Activity context, int holder, StockModel stockModel) {
            super(context);
            this.activity = context;
            pos = holder;
            this.stockModel = stockModel;
        }


        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_yes:
                    dismiss();
                    break;
                case R.id.btn_no:
                    // dismiss();
                    //activity.finish();
                    deleteFeatureditem(pos, stockModel);
                    Log.i("ree", "xccxdsdfsfdfds");
                    break;
                default:
                    break;
            }
            dismiss();
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.stock_featured_delete_dialog);
            yes = findViewById(R.id.btn_yes);
            no = findViewById(R.id.btn_no);
            yes.setOnClickListener(this);
            no.setOnClickListener(this);
        }

        public void deleteFeatureditem(int viewHolder, StockModel stockModel) {
            make = this.stockModel.getMake().toString();
            model = this.stockModel.model.toString()/* + " " +stockModel.get(position).getVariant().toString()*/;
            variant = this.stockModel.getVariant().toString();
            registraionCity = this.stockModel.getRegistraionCity();
            registraionNo = this.stockModel.getRegistrationNumber();
            color = this.stockModel.getColour().toString();
            certified = this.stockModel.getCertifiedText();
            owner = this.stockModel.getOwner().toString();
            kms = this.stockModel.getKilometer().toString();
            certifiedNo = this.stockModel.getCertificationNumber().toString();
            sellingPrice = this.stockModel.getSellingPrice().toString();
            yearMonth = this.stockModel.getRegMonth().toString() + " " + this.stockModel.getRegYear().toString();
            insurance = this.stockModel.getInsurance().toString();
            insuranceExp_date = this.stockModel.getInsuranceExpDate().toString();
            selling_price = this.stockModel.getSellingPrice().toString();
            Regyear = this.stockModel.getRegYear().toString();
            source = this.stockModel.getSource().toString();
            warranty = this.stockModel.getWarranty().toString();
            surveyor_remark = this.stockModel.getSurveyorRemark().toString();
            photo_count = this.stockModel.getPhotoCount().toString();
            reg_month = this.stockModel.getRegMonth().toString();

            fuel_type = this.stockModel.getFuel_type().toString();
            bought = this.stockModel.getBought().toString();
            refurbishment_cost = this.stockModel.getRefurbishment_cost().toString();
            dealer_price = this.stockModel.getDealer_price().toString();
            ProcurementExecId = this.stockModel.getProcurementExecId().toString();
            cng_kit = this.stockModel.getCng_kit().toString();
            chassis_number = this.stockModel.getChassis_number().toString();
            engine_number = this.stockModel.getEngine_number().toString();

            // String is_display= stockModel.getIs_display().toString();
            is_certified = this.stockModel.getIs_certified().toString();
            is_featured_car = this.stockModel.getIs_featured_car().toString();
            is_featured_car_admin = this.stockModel.getIs_featured_car_admin().toString();
            procurement_executive_name = this.stockModel.getProcurement_executive_name().toString();
            private_vehicle = this.stockModel.getPrivate_vehicle().toString();
            comments = this.stockModel.getComments().toString();
            finance_required = this.stockModel.getFinance_required().toString();
            manufacture_month = this.stockModel.getManufacture_month().toString();
            sales_executive_id = this.stockModel.getSales_executive_id().toString();
            sales_executive_name = this.stockModel.getSales_executive_name().toString();
            manufacture_year = this.stockModel.getManufacture_year().toString();
            actual_selling_price = this.stockModel.getActual_selling_price().toString();
            CertifiedText = this.stockModel.getCertifiedText().toString();
            IsDisplay = this.stockModel.getIsDisplay().toString();
            TransmissionType = this.stockModel.getTransmissionType().toString();
            colour = this.stockModel.getColour().toString();
            dealer_code = this.stockModel.getDealerCode().toString();
            is_offload = this.stockModel.getIsOffload().toString();
            posted_date = this.stockModel.getPostedDate().toString();
            stock_id = this.stockModel.getId().toString();
            is_booked = this.stockModel.getIsbooked().toString();

            is_display = "true";
            WebServicesCall.webCall(context, context, deleteFeaturedMakeBookNow(), "deleteFeatureCar", GlobalText.PUT);

            Log.e("getPosition", "getPosition=" + viewHolder);

            addList.remove(viewHolder);

            notifyDataSetChanged();

        }
    }


    public static String MethodofMonth_Num(String month) {

        if (month.equalsIgnoreCase("January") || month.equalsIgnoreCase("Jan")) {
            month = "1";
            return month;
        }


        if (month.equalsIgnoreCase("February") || month.equalsIgnoreCase("Feb")) {
            month = "2";
            return month;

        }

        if (month.equalsIgnoreCase("March") || month.equalsIgnoreCase("Mar")) {
            month = "3";
            return month;

        }

        if (month.equalsIgnoreCase("April") || month.equalsIgnoreCase("Apr")) {
            month = "4";
            return month;

        }


        if (month.equalsIgnoreCase("May") || month.equalsIgnoreCase("May")) {
            month = "5";
            return month;

        }

        if (month.equalsIgnoreCase("June") || month.equalsIgnoreCase("Jun")) {
            month = "6";
            return month;

        }

        if (month.equalsIgnoreCase("July") || month.equalsIgnoreCase("Jul")) {
            month = "7";
            return month;

        }

        if (month.equalsIgnoreCase("August") || month.equalsIgnoreCase("Aug")) {
            month = "8";
            return month;

        }


        if (month.equalsIgnoreCase("September") || month.equalsIgnoreCase("Sep")) {
            month = "9";
            return month;

        }

        if (month.equalsIgnoreCase("October") || month.equalsIgnoreCase("Oct")) {
            month = "10";
            return month;

        }

        if (month.equalsIgnoreCase("November") || month.equalsIgnoreCase("Nov")) {
            month = "11";
            return month;

        }

        if (month.equalsIgnoreCase("December") || month.equalsIgnoreCase("Dec")) {
            month = "12";
            return month;

        }

        return month;

    }
}
