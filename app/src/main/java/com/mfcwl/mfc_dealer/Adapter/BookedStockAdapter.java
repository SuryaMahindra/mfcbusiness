package com.mfcwl.mfc_dealer.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.Activity.StockStoreInfoActivity;
import com.mfcwl.mfc_dealer.Model.StockModel;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;

import java.util.ArrayList;
import java.util.List;

import static androidx.recyclerview.widget.ItemTouchHelper.Callback.makeMovementFlags;

/**
 * Created by sowmya on 9/3/18.
 */

public class BookedStockAdapter extends RecyclerView.Adapter<BookedStockAdapter.ViewHolder> {
    private List<StockModel> stockModel;

    Activity activity;
    int lastItemPosition = -1;


    public BookedStockAdapter(Activity act, List<StockModel> stockModels) {
        this.activity = act;
        this.stockModel = stockModels;
        //   updateFontUI(act);

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.stock_store_row, parent, false);

        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        StockModel sm = stockModel.get(position);

        /*if (position > lastItemPosition) {
            StockFragment.bottom_topAnimation();
            MainActivity.top_bottomAnimation();
            Log.e("Scrolled Downwards","Downwards");
        } else if (position < lastItemPosition) {
            StockFragment.top_bottomAnimation();
            MainActivity.bottom_topAnimation();

            Log.e("Scrolled Upwards","Upwards");
        } else {
            Log.e("No Vertical Scrolled","no Vertical");
        }
            lastItemPosition = position;
*/


        holder.setIsRecyclable(false);
        holder.stockCarName.setText(sm.getMake().toString());
        holder.stockVariant.setText(stockModel.get(position).getModel().toString() /*+ " " +
        stockModel.get(position).getVariant().toString()*/);
        holder.sellingPrice.setText(stockModel.get(position).getSellingPrice().toString());
        holder.stockRegistration.setText(stockModel.get(position).getRegistrationNumber().toString());
        holder.imageCount.setText(stockModel.get(position).getPhotoCount().toString());
        holder.stockYear.setText(stockModel.get(position).getRegYear().toString());
        holder.stockkms.setText(stockModel.get(position).getKilometer().toString() + " " + "Kms");
        holder.stockOwner.setText(stockModel.get(position).getOwner().toString() + " " + "Owner");


        if (stockModel.get(position).getCertifiedText().toString().equals("Certified")) {
            holder.stockCertified.setText(stockModel.get(position).getCertifiedText().toString());
            holder.stockCertified.setVisibility(View.VISIBLE);
            //Log.i("test certified1",stockModel.get(position).getCertifiedText().toString());
        } else {
            //Log.i("test certified",stockModel.get(position).getCertifiedText().toString());
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String make = stockModel.get(position).getMake().toString();
                final String model = stockModel.get(position).model.toString()/* + " " +stockModel.get(position).getVariant().toString()*/;
                final String variant = stockModel.get(position).getVariant().toString();
                final String registraionCity = stockModel.get(position).getRegistraionCity();
                final String registraionNo = stockModel.get(position).getRegistrationNumber();
                final String color = stockModel.get(position).getColour().toString();
                final String certified = stockModel.get(position).getCertifiedText();
                final String owner = stockModel.get(position).getOwner().toString();
                final String kms = stockModel.get(position).getKilometer().toString();
                final String certifiedNo = stockModel.get(position).getCertificationNumber().toString();
                final String sellingPrice = stockModel.get(position).getSellingPrice().toString();
                final String yearMonth = stockModel.get(position).getRegMonth().toString() + " " + stockModel.get(position).getRegYear().toString();
                final String insurance = stockModel.get(position).getInsurance().toString();
                final String insuranceExp_date = stockModel.get(position).getInsuranceExpDate().toString();
                final String selling_price = stockModel.get(position).getSellingPrice().toString();
                final String Regyear = stockModel.get(position).getRegYear().toString();
                final String source = stockModel.get(position).getSource().toString();
                final String warranty = stockModel.get(position).getWarranty().toString();
                final String surveyor_remark = stockModel.get(position).getSurveyorRemark().toString();
                final String photo_count = stockModel.get(position).getPhotoCount().toString();
                final String reg_month = stockModel.get(position).getRegMonth().toString();


                Log.i("test", "test");

                Intent intent = new Intent(activity.getBaseContext(), StockStoreInfoActivity.class);
                intent.putExtra("make", make);
                intent.putExtra("model", model);
                CommonMethods.setvalueAgainstKey(activity, "booknowbtn", "false");

                intent.putExtra("variant", variant);
                intent.putExtra("registraionCity", registraionCity);
                intent.putExtra("registraionNo", registraionNo);
                intent.putExtra("color", color);
                intent.putExtra("certified", certified);
                intent.putExtra("owner", owner);
                intent.putExtra("kms", kms);
                intent.putExtra("Regyear", Regyear);
                intent.putExtra("certifiedNo", certifiedNo);
                intent.putExtra("sellingPrice", sellingPrice);
                intent.putExtra("yearMonth", yearMonth);
                intent.putExtra("insurance", insurance);
                intent.putExtra("insuranceExp_date", insuranceExp_date);
                intent.putExtra("selling_price", selling_price);
                intent.putExtra("source", source);
                intent.putExtra("warranty", warranty);
                intent.putExtra("surveyor_remark", surveyor_remark);
                intent.putExtra("photo_count", photo_count);
                intent.putExtra("reg_month", reg_month);

                activity.startActivity(intent);

            }
        });
    }


    @Override
    public int getItemCount() {
        return stockModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView stockCarName, stockVariant, sellingPrice, stockRegistration, imageCount,
                stockYear, stockkms, stockOwner, stockCertified;

        public ViewHolder(View itemView) {
            super(itemView);
            stockCarName = itemView.findViewById(R.id.asm_message);
            stockVariant = itemView.findViewById(R.id.stock_variant);
            sellingPrice = itemView.findViewById(R.id.textView2);
            stockRegistration = itemView.findViewById(R.id.sales_registration);
            imageCount = itemView.findViewById(R.id.image_number);
            stockYear = itemView.findViewById(R.id.stock_year);
            stockkms = itemView.findViewById(R.id.stock_kms);
            stockOwner = itemView.findViewById(R.id.stock_owner);
            stockCertified = itemView.findViewById(R.id.stock_certified);

            updateFontUI(activity, itemView);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void updateFontUI(Activity context, View convert) {
        ArrayList<Integer> arl = new ArrayList<Integer>();
        arl.add(R.id.asm_message);
        arl.add(R.id.stock_variant);
      /*  arl.add(R.id.stock_registration);
        arl.add(R.id.stock_certified);*/


        setFontStyle(arl, context, convert);
    }

    private void setFontStyle(ArrayList<Integer> arl, Activity activity, View viewHolder) {
        for (int i = 0; i < arl.size(); i++) {
            Typeface myTypeface = Typeface.createFromAsset(activity.getAssets(), "lato_semibold.ttf");
            TextView myTextView = viewHolder.findViewById(arl.get(i));
            myTextView.setTypeface(myTypeface);
        }
    }
    
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
        int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
        return makeMovementFlags(dragFlags, swipeFlags);
    }
}
