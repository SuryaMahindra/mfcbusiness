package com.mfcwl.mfc_dealer.Adapter.Leads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.Activity.Leads.LeadDetailsActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Fragment.Leads.PrivateLeadsFragment;
import com.mfcwl.mfc_dealer.Model.Leads.PrivateLeadModel;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.MonthUtility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.entersearch;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchFlag;

/**
 * Created by HP 240 G5 on 12-03-2018.
 */

public class PrivateLeadsAdapter extends RecyclerView.Adapter<PrivateLeadsAdapter.ViewHolder> {

    private final List<PrivateLeadModel> list;
    private Activity activity;
    Context context;
    int lastItemPosition = -1;
    public static LinearLayout progress;
    boolean isLoading = false;
    private List<PrivateLeadModel> itemListSearch;

    public PrivateLeadsAdapter(Context context, List<PrivateLeadModel> list, Activity a) {
        this.list = list;
        this.activity = a;
        this.context = context;

        itemListSearch = new ArrayList<PrivateLeadModel>();
        itemListSearch.addAll(list);
    }


    @NonNull
    @Override
    public PrivateLeadsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_private_leads_item, parent, false);
        progress = view.findViewById(R.id.progress);
        return new PrivateLeadsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {


        final PrivateLeadModel normalLeadModel = this.list.get(position);

        if (position >= getItemCount() - 1 && getItemCount() > 0 && !searchFlag) {

            isLoading = true;

            //progress.setVisibility(View.VISIBLE);

            if (entersearch == false) {

                PrivateLeadsFragment.loadmore(getItemCount());
            }

        }


        holder.leads_name.setText(normalLeadModel.getCustomername().trim());
        holder.leads_num.setText(normalLeadModel.getCustmobile().trim());
        holder.leads_comp_name.setText(normalLeadModel.getMake().trim());
        if (!normalLeadModel.getVariant().trim().equals("null")) {
            holder.leads_car_model.setText(normalLeadModel.getModel().trim() + " " + normalLeadModel.getVariant().trim());
        } else {
            holder.leads_car_model.setText(normalLeadModel.getModel().trim());
        }

        try {
            String[] splitdatespace = normalLeadModel.getFollowdate().split(" ", 10);
            String[] splitdate1 = splitdatespace[0].split("-", 10);
            holder.leads_date.setText(splitdate1[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate1[1])] + " " + splitdate1[0]);
        } catch (Exception e) {

            holder.leads_date.setText(normalLeadModel.getFollowdate().trim());
        }

        holder.leads_followup_history_warmorcold.setText(normalLeadModel.getLeadstatus().trim());

        String createdDate = normalLeadModel.getCreatedat();
        String[] splitDateTime = createdDate.split(" ", 10);
        String[] splitDate = splitDateTime[0].split("-", 10);
        String finalDate = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");
        Calendar c = Calendar.getInstance();
        String formattedDate = simpleDateFormat.format(c.getTime());
        String[] splitSpace = formattedDate.split(" ", 10);

        try {
            Date date1 = simpleDateFormat.parse(finalDate + " 00:00:00");
            Date date2 = simpleDateFormat.parse(splitSpace[0] + " 00:00:00");

            if (printDifference(date1, date2) == 0) {
                holder.leads_days.setText("Today");
            } else if (printDifference(date1, date2) == 1) {
                holder.leads_days.setText("Yesterday");
            } else if (printDifference(date1, date2) > 364) {
                holder.leads_days.setText("" + printDifference(date1, date2) / 365 + " Y ago");
            } else {
                holder.leads_days.setText("" + printDifference(date1, date2) + " D ago");
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.leads_followup_history_warmorcold.setVisibility(View.VISIBLE);
        if (normalLeadModel.getLeadstatus().equalsIgnoreCase("Open")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.open_28);
            holder.leads_followup_history_warmorcold.setText("  " + normalLeadModel.getLeadstatus().trim());
            //holder.leads_followup_history_warmorcold.setVisibility(View.INVISIBLE);
        }

        if (normalLeadModel.getLeadstatus().equalsIgnoreCase("") || normalLeadModel.getLeadstatus().equalsIgnoreCase("null")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.open_28);
            holder.leads_followup_history_warmorcold.setText("  Open");
            //holder.leads_followup_history_warmorcold.setVisibility(View.INVISIBLE);
        }


        if (normalLeadModel.getLeadstatus().equalsIgnoreCase("Hot")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.hot_28);
        }
        if (normalLeadModel.getLeadstatus().equalsIgnoreCase("Warm")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.warm_28);
            holder.leads_followup_history_warmorcold.setText("  " + normalLeadModel.getLeadstatus().trim());
        }
        if (normalLeadModel.getLeadstatus().equalsIgnoreCase("Cold")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.cold_28);
        }

        if (normalLeadModel.getLeadstatus().equalsIgnoreCase("Lost")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.lost_28);
        }
        if (normalLeadModel.getLeadstatus().equalsIgnoreCase("Sold")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.sold_28);
        }


        if (normalLeadModel.getLeadstatus().equalsIgnoreCase("null")) {
            holder.leads_followup_history_warmorcold.setText("NA");
        }

        if (normalLeadModel.getFollowdate().trim().equalsIgnoreCase("null") || normalLeadModel.getFollowdate().equalsIgnoreCase("0000-00-00 00:00:00") || normalLeadModel.getFollowdate().trim().equalsIgnoreCase("")) {
            holder.leads_date.setText("NA");
        }


        holder.leads_Call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.private_leads_call, GlobalText.android);

                Intent callIntent = new Intent(Intent.ACTION_VIEW);
                callIntent.setData(Uri.parse("tel:" + normalLeadModel.getCustmobile().trim()));//change the number
                context.startActivity(callIntent);
            }
        });
        holder.leads_Messege.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.private_leads_message, GlobalText.android);

                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:" + normalLeadModel.getCustmobile().trim()));
                sendIntent.putExtra("sms_body", "");
                context.startActivity(sendIntent);
            }
        });

        holder.leads_Whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.private_leads_whatsapp, GlobalText.android);

                openWhatsApp(normalLeadModel.getCustmobile().trim());

            }
        });


        /*holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, LeadDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("NM", normalLeadModel.getCustomername().trim());
                bundle.putString("MN", normalLeadModel.getCustmobile().trim());
                bundle.putString("EM", normalLeadModel.getCustemail().trim());
                bundle.putString("LS", normalLeadModel.getLeadstatus().trim());
                bundle.putString("PD", normalLeadModel.getLeaddate().trim());
                bundle.putString("MK", normalLeadModel.getMake().trim());
                bundle.putString("MD", normalLeadModel.getModel().trim());
                bundle.putString("MV", normalLeadModel.getVariant().trim());
                bundle.putString("PR", normalLeadModel.getStockexpprice().trim());
                bundle.putString("RN", normalLeadModel.getRegno().trim());
                bundle.putString("NFD", normalLeadModel.getFollowdate().trim());
                bundle.putString("ENM", normalLeadModel.getExename().trim());
                bundle.putString("RGC", normalLeadModel.getCity().trim());
                bundle.putString("OWN", normalLeadModel.getOwner().trim());
                bundle.putString("MYR", normalLeadModel.getRegyear().trim());
                bundle.putString("CLR", normalLeadModel.getColor().trim());
                bundle.putString("KMS", normalLeadModel.getKms().trim());
                bundle.putString("DID", normalLeadModel.getDealerid().trim());
                bundle.putString("LID", normalLeadModel.getId().trim());
                bundle.putString("LT", normalLeadModel.getLeadtype().trim());
                bundle.putString("RMK", normalLeadModel.getRemark().trim());
                bundle.putString("LD", holder.leads_days.getText().toString().trim());

                CommonMethods.setvalueAgainstKey(activity, "WLEADS", "PrivateLeads");
                intent.putExtras(bundle);
                context.startActivity(intent);

            }
        });*/  // Commented by Sangeetha - Because there is no need to display lead details onClick of listItem
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView leads_name, leads_num, leads_comp_name, leads_car_model, leads_date, leads_days, leads_followup_history_warmorcold;
        public CircleImageView leads_Call, leads_Messege, leads_Whatsapp;

        public ViewHolder(View itemView) {
            super(itemView);
            leads_name = itemView.findViewById(R.id.leads_name);
            leads_num = itemView.findViewById(R.id.leads_num);
            leads_comp_name = itemView.findViewById(R.id.leads_comp_name);
            leads_car_model = itemView.findViewById(R.id.leads_car_model);
            leads_date = itemView.findViewById(R.id.leads_date);
            leads_days = itemView.findViewById(R.id.leads_days);
            leads_followup_history_warmorcold = itemView.findViewById(R.id.leads_followup_history_warmorcold);


            leads_Call = itemView.findViewById(R.id.leads_Call);
            leads_Messege = itemView.findViewById(R.id.leads_Messege);
            leads_Whatsapp = itemView.findViewById(R.id.leads_Whatsapp);
            if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {

            }else{
                leads_Call.setVisibility(View.INVISIBLE);
                leads_Messege.setVisibility(View.INVISIBLE);
                leads_Whatsapp .setVisibility(View.INVISIBLE);

            }
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public long printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

       /* System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);*/

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        return elapsedDays;
    }


    public void notifyDataChanged() {
        notifyDataSetChanged();
        isLoading = false;
    }

    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0) {
            list.addAll(itemListSearch);
        } else {
            for (PrivateLeadModel it : itemListSearch) {
                if (it.getMake().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getModel().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getVariant().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getCustomername().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getCustmobile().toLowerCase(Locale.getDefault()).contains(charText)
                        ) {
                    list.add(it);
                }
            }
        }
        notifyDataSetChanged();

    }

    private void openWhatsApp(String number) {
        String smsNumber = "91" + number; //without '+'
        try {
            /*Intent sendIntent = new Intent("android.intent.action.MAIN");
            //sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
            sendIntent.putExtra("jid", smsNumber + "@s.whatsapp.net"); //phone number without "+" prefix
            sendIntent.setPackage("com.whatsapp");
            context.startActivity(sendIntent);*/

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_VIEW);
            String url = "https://api.whatsapp.com/send?phone=" + smsNumber + "&text=" + "";
            sendIntent.setData(Uri.parse(url));
            activity.startActivity(sendIntent);

        } catch (Exception e) {
            Toast.makeText(context, "Error/n" + e.toString(), Toast.LENGTH_SHORT).show();
        }

    }

}

