package com.mfcwl.mfc_dealer.Adapter.LeadSection;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.Activity.Leads.LeadDetailsActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Interface.LeadSection.LazyloaderWebLeads;
import com.mfcwl.mfc_dealer.Model.LeadSection.WebLeadsDatum;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.MonthUtility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ASM;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_STATEHEAD;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ZONALHEAD;

public class NormalLeadsAdapterSection extends RecyclerView.Adapter<NormalLeadsAdapterSection.ViewHolder> {

    private List<WebLeadsDatum> list;
    private Activity activity;
    Context context;
    private List<WebLeadsDatum> itemListSearch;
    private LazyloaderWebLeads lazyloaderWebLeads;

    public NormalLeadsAdapterSection(Context context, List<WebLeadsDatum> list, Activity a) {
        this.list = list;
        this.activity = a;
        this.context = context;

        itemListSearch = new ArrayList<WebLeadsDatum>();
        itemListSearch.addAll(list);

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        lazyloaderWebLeads = (LazyloaderWebLeads) activity;
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_normal_leads_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        //  Log.e("mLeadbind ", "holder " + getItemCount());
        if (position >= getItemCount() - 1 && getItemCount() > 0 || list.size() <= 3) {
            lazyloaderWebLeads.loadmore(getItemCount());
        }

        final WebLeadsDatum mWebLeadsDatum = this.list.get(position);

        validateDays(mWebLeadsDatum, holder);

        validatefollowupDate(mWebLeadsDatum, holder);

        updateLeadDetails(mWebLeadsDatum, holder);

        updateLeadStatus(mWebLeadsDatum, holder);

        //Callback Listener

        holder.leads_Call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.web_leads_call, GlobalText.android);

                Intent callIntent = new Intent(Intent.ACTION_VIEW);
                callIntent.setData(Uri.parse("tel:" + mWebLeadsDatum.getMobile().trim()));//change the number
                context.startActivity(callIntent);
            }
        });
        holder.leads_Messege.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.web_leads_message, GlobalText.android);

                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:" + mWebLeadsDatum.getMobile().trim()));
                sendIntent.putExtra("sms_body", "");
                context.startActivity(sendIntent);

            }
        });

        holder.leads_Whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.web_leads_whatsapp, GlobalText.android);

                openWhatsApp(mWebLeadsDatum.getMobile().trim());
            }
        });

        String userType = CommonMethods.getstringvaluefromkey(activity,"user_type");

        if(!userType.equalsIgnoreCase(USERTYPE_ASM) ||!userType.equalsIgnoreCase(USERTYPE_STATEHEAD) ||!userType.equalsIgnoreCase(USERTYPE_ZONALHEAD) ){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    CommonMethods.setvalueAgainstKey(activity, "WLEADS", "WebLeads");

                    int pos = holder.getAdapterPosition();
                    //  Log.e("LeadDetailsPosition", position + "");
                    Intent intent = new Intent(context, LeadDetailsActivity.class);
                    Bundle bundle = new Bundle();
                    if (mWebLeadsDatum.getName().trim().equalsIgnoreCase("NA") || mWebLeadsDatum.getName().trim().equalsIgnoreCase("") || mWebLeadsDatum.getName().trim().equalsIgnoreCase("null")) {
                        bundle.putString("NM", "UnKnown");
                    } else {
                        bundle.putString("NM", mWebLeadsDatum.getName().trim());
                    }

                    //  Log.e("followcomments ", "remark " + normalLeadModel.getRemark().trim());
                    bundle.putString("MN", mWebLeadsDatum.getMobile().trim());
                    bundle.putString("EM", mWebLeadsDatum.getEmail().trim());
                    bundle.putString("LS", mWebLeadsDatum.getStatus().trim());
                    bundle.putString("PD", mWebLeadsDatum.getPostedDate().trim());
                    bundle.putString("MK", mWebLeadsDatum.getPrimaryMake().trim());
                    bundle.putString("MD", mWebLeadsDatum.getPrimaryModel().trim());
                    bundle.putString("MV", mWebLeadsDatum.getPrimaryVariant().trim());
                    bundle.putString("PR", mWebLeadsDatum.getPrice().trim());
                    bundle.putString("RN", mWebLeadsDatum.getRegno().trim());
                    try {
                        if (!mWebLeadsDatum.getFollowUpDate().trim().equalsIgnoreCase("") || mWebLeadsDatum.getFollowUpDate().trim() != null) {
                            bundle.putString("NFD", mWebLeadsDatum.getFollowUpDate().trim());
                        } else {
                            bundle.putString("NFD", "");
                        }
                    } catch (Exception e) {
                        //  e.printStackTrace();
                        bundle.putString("NFD", "");
                    }

                    try {
                        if (mWebLeadsDatum.getExecutiveName().equalsIgnoreCase("")) {
                            bundle.putString("ENM", "NA");
                        } else {
                            bundle.putString("ENM", mWebLeadsDatum.getExecutiveName().toString().trim());

                        }
                    } catch (Exception e) {
                        //e.printStackTrace();
                        bundle.putString("ENM", "NA");
                    }

                    bundle.putString("RGC", mWebLeadsDatum.getRegisterCity().trim());
                    bundle.putString("OWN", mWebLeadsDatum.getOwners() + "");
                    bundle.putString("MYR", mWebLeadsDatum.getRegyear().trim());
                    bundle.putString("CLR", mWebLeadsDatum.getColor().trim());
                    bundle.putString("KMS", mWebLeadsDatum.getKms() + "");
                    bundle.putString("DID", mWebLeadsDatum.getDispatchid() + "");
                    bundle.putString("RMK", mWebLeadsDatum.getRemark().trim());
                    bundle.putString("LD", holder.leads_days.getText().toString().trim());
                    bundle.putString("MFY", mWebLeadsDatum.getRegyear().trim());
                    bundle.putString("IDL", mWebLeadsDatum.getId() + "");
                    bundle.putString("position", pos + "");
                    bundle.putString("lead_type", mWebLeadsDatum.getLeadType());
                    intent.putExtras(bundle);
                    activity.startActivityForResult(intent, 10001);

                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView leads_name, leads_num, leads_comp_name, leads_car_model, leads_date, leads_days, leads_followup_history_warmorcold;
        private ImageView leads_Call, leads_Messege, leads_Whatsapp;

        public ViewHolder(View itemView) {
            super(itemView);
            leads_name = itemView.findViewById(R.id.leads_name);
            leads_num = itemView.findViewById(R.id.leads_num);
            leads_comp_name = itemView.findViewById(R.id.leads_comp_name);
            leads_car_model = itemView.findViewById(R.id.leads_car_model);
            leads_date = itemView.findViewById(R.id.leads_date);
            leads_days = itemView.findViewById(R.id.leads_days);
            leads_followup_history_warmorcold = itemView.findViewById(R.id.leads_followup_history_warmorcold);

            leads_Call = itemView.findViewById(R.id.leads_Call);
            leads_Messege = itemView.findViewById(R.id.leads_Messege);
            leads_Whatsapp = itemView.findViewById(R.id.leads_Whatsapp);

            if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {

            }else{
                leads_Call.setVisibility(View.INVISIBLE);
                leads_Messege.setVisibility(View.INVISIBLE);
                leads_Whatsapp.setVisibility(View.INVISIBLE);
            }

        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    private void updateLeadStatus(WebLeadsDatum mWebLeadsDatum, ViewHolder holder) {

        holder.leads_followup_history_warmorcold.setVisibility(View.VISIBLE);

        try {
            holder.leads_followup_history_warmorcold.setText(capitalize(mWebLeadsDatum.getStatus().trim()));
        } catch (Exception e) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.open_28);
            holder.leads_followup_history_warmorcold.setText(capitalize("Open"));
        }


        try {
            if (mWebLeadsDatum.getStatus().equalsIgnoreCase("Open")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.open_28);
                holder.leads_followup_history_warmorcold.setText("  " + capitalize(mWebLeadsDatum.getStatus().trim()));
            } else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("") || mWebLeadsDatum.getStatus().equalsIgnoreCase("null")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.open_28);
                holder.leads_followup_history_warmorcold.setText(" Open");
            }
            else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("Outstation")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.ic_outstation);
                holder.leads_followup_history_warmorcold.setText("  " + capitalize(mWebLeadsDatum.getStatus().trim()));
            }
            else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("Hot")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.hot_28);
            } else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("Warm")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.warm_28);
                holder.leads_followup_history_warmorcold.setText("  " + capitalize(mWebLeadsDatum.getStatus().trim()));
            } else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("Cold")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.cold_28);
            } else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("Lost")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.lost_28);
            } else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("Sold")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.sold_28);
            } else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("Visiting")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.ic_visiting);
            } else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("Search")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.ic_search_lead);
            } else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("Not-Interested")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.ic_not_intrested);
            } else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("No-Response")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.ic_no_responce);
            } else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("Finalised")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.ic_finalized);
            } else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("null")) {
                holder.leads_followup_history_warmorcold.setText("Open");
            }
        } catch (Exception e) {

        }


    }

    private void validatefollowupDate(WebLeadsDatum mWebLeadsDatum, ViewHolder holder) {

        try {
            String[] splitdatespace = mWebLeadsDatum.getFollowUpDate().split(" ", 10);
            String[] splitdate1 = splitdatespace[0].split("-", 10);
            holder.leads_date.setText(splitdate1[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate1[1])] + " " + splitdate1[0]);
        } catch (Exception e) {

            String leadDate = ((mWebLeadsDatum.getFollowUpDate() == null || mWebLeadsDatum.getFollowUpDate().trim().equalsIgnoreCase("NA") || mWebLeadsDatum.getFollowUpDate().trim().equalsIgnoreCase("")) ? "NA" : mWebLeadsDatum.getFollowUpDate().trim());
            holder.leads_date.setText(leadDate);

        }

    }

    private void validateDays(WebLeadsDatum mWebLeadsDatum, ViewHolder holder) {

        String createdDate = mWebLeadsDatum.getLeadDate();
        String[] splitDateTime = createdDate.split(" ", 10);
        String[] splitDate = splitDateTime[0].split("-", 10);
        String finalDate = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");
        Calendar c = Calendar.getInstance();
        String formattedDate = simpleDateFormat.format(c.getTime());
        String[] splitSpace = formattedDate.split(" ", 10);

        try {
            Date date1 = simpleDateFormat.parse(finalDate + " 00:00:00");
            Date date2 = simpleDateFormat.parse(splitSpace[0] + " 00:00:00");

            if (printDifference(date1, date2) == 0) {
                holder.leads_days.setText("Today");
            } else if (printDifference(date1, date2) == 1) {
                holder.leads_days.setText("Yesterday");
            } else if (printDifference(date1, date2) > 364) {
                holder.leads_days.setText("" + printDifference(date1, date2) / 365 + " Y ago");
            } else {
                holder.leads_days.setText("" + printDifference(date1, date2) + " D ago");
            }


        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("ParseException ", "Normal " + e.toString());
        }
    }

    private void updateLeadDetails(WebLeadsDatum mWebLeadsDatum, ViewHolder holder) {

        String leadName = ((mWebLeadsDatum.getName().trim() == null || mWebLeadsDatum.getName().trim().equalsIgnoreCase("NA") || mWebLeadsDatum.getName().trim().equalsIgnoreCase("")) ? "Unknown" : mWebLeadsDatum.getName().trim());
        holder.leads_name.setText(capitalize(leadName));

        String leadMobNo = ((mWebLeadsDatum.getMobile().trim() == null || mWebLeadsDatum.getMobile().trim().equalsIgnoreCase("NA") || mWebLeadsDatum.getMobile().trim().equalsIgnoreCase("")) ? "Unknown" : mWebLeadsDatum.getMobile().trim());
        holder.leads_num.setText(leadMobNo);

        String leadCompName = ((mWebLeadsDatum.getPrimaryMake().trim() == null || mWebLeadsDatum.getPrimaryMake().trim().equalsIgnoreCase("NA") || mWebLeadsDatum.getPrimaryMake().trim().equalsIgnoreCase("")) ? "NA" : mWebLeadsDatum.getPrimaryMake().trim());
        holder.leads_comp_name.setText(leadCompName);

        String leadModVar = ((mWebLeadsDatum.getPrimaryModel().trim() == null || mWebLeadsDatum.getPrimaryModel().trim().equalsIgnoreCase("NA") || mWebLeadsDatum.getPrimaryModel().trim().equalsIgnoreCase("") || mWebLeadsDatum.getPrimaryVariant().trim() == null || mWebLeadsDatum.getPrimaryVariant().trim().equalsIgnoreCase("NA") || mWebLeadsDatum.getPrimaryVariant().trim().equalsIgnoreCase("")) ? "NA" : mWebLeadsDatum.getPrimaryModel().trim() + " " + mWebLeadsDatum.getPrimaryVariant().trim());
        holder.leads_car_model.setText(leadModVar);


    }

    private String capitalize(String capString) {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()) {
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }

    private long printDifference(Date startDate, Date endDate) {
        long different = endDate.getTime() - startDate.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long elapsedDays = different / daysInMilli;
        return elapsedDays;
    }

    private void openWhatsApp(String number) {
        String smsNumber = "91" + number; //without '+'
        try {

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_VIEW);
            String url = "https://api.whatsapp.com/send?phone=" + smsNumber + "&text=" + "";
            sendIntent.setData(Uri.parse(url));
            activity.startActivity(sendIntent);

        } catch (Exception e) {
            Toast.makeText(context, "Error/n" + e.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    //searchfunctionaly WebLeads

    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0) {
            list.addAll(itemListSearch);
        } else {
            for (WebLeadsDatum it : itemListSearch) {
                if (it.getPrimaryMake().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getPrimaryModel().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getPrimaryVariant().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getName().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getMobile().toLowerCase(Locale.getDefault()).contains(charText)
                ) {
                    list.add(it);
                }
            }
        }
        try {
            lazyloaderWebLeads.updatecountweb(list.size());
        } catch (Exception e) {

        }
        notifyDataSetChanged();

    }

}
