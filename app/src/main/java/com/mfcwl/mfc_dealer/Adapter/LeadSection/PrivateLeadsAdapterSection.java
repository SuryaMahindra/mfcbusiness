package com.mfcwl.mfc_dealer.Adapter.LeadSection;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.Activity.Leads.LeadDetailsActivity;
import com.mfcwl.mfc_dealer.Interface.LeadSection.LazyloaderPrivateLeads;
import com.mfcwl.mfc_dealer.Model.LeadSection.PrivateLeadDatum;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.MonthUtility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ASM;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_STATEHEAD;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ZONALHEAD;

public class PrivateLeadsAdapterSection extends RecyclerView.Adapter<PrivateLeadsAdapterSection.ViewHolder> {

    private Activity mActivity;
    private Context mContext;

    private final List<PrivateLeadDatum> list;
    private Activity activity;
    Context context;
    private List<PrivateLeadDatum> itemListSearch;
    private LazyloaderPrivateLeads lazyloaderPrivateLeads;

    public PrivateLeadsAdapterSection(Context context, List<PrivateLeadDatum> list, Activity a) {
        this.list = list;
        this.activity = a;
        this.context = context;

        itemListSearch = new ArrayList<PrivateLeadDatum>();
        itemListSearch.addAll(list);

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        lazyloaderPrivateLeads = (LazyloaderPrivateLeads) activity;
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_private_leads_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (position >= getItemCount() - 1 && getItemCount() > 0 || list.size() <= 3) {
            lazyloaderPrivateLeads.loadmorePrivate(getItemCount());
        }

        final PrivateLeadDatum mPrivateLeadDatum = this.list.get(position);

        validateDays(mPrivateLeadDatum, holder);

        validatefollowupDate(mPrivateLeadDatum, holder);

        updateLeadDetails(mPrivateLeadDatum, holder);

        updateLeadStatus(mPrivateLeadDatum, holder);

        holder.leads_Call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_VIEW);
                callIntent.setData(Uri.parse("tel:" + mPrivateLeadDatum.getCustomerMobile().trim()));//change the number
                context.startActivity(callIntent);
            }
        });
        holder.leads_Messege.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:" + mPrivateLeadDatum.getCustomerMobile().trim()));
                sendIntent.putExtra("sms_body", "");
                context.startActivity(sendIntent);
            }
        });

        holder.leads_Whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openWhatsApp(mPrivateLeadDatum.getCustomerMobile().trim());

            }
        });


        String userType = CommonMethods.getstringvaluefromkey(activity,"user_type");

        if(!userType.equalsIgnoreCase(USERTYPE_ASM) ||!userType.equalsIgnoreCase(USERTYPE_STATEHEAD) ||!userType.equalsIgnoreCase(USERTYPE_ZONALHEAD)){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    CommonMethods.setvalueAgainstKey(activity, "WLEADS", "PrivateLeads");
                    int pos = holder.getAdapterPosition();
                    Intent intent = new Intent(context, LeadDetailsActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("NM", mPrivateLeadDatum.getCustomerName().trim());
                    bundle.putString("MN", mPrivateLeadDatum.getCustomerMobile().trim());
                    bundle.putString("EM", mPrivateLeadDatum.getCustomerEmail().trim());
                    bundle.putString("LS", mPrivateLeadDatum.getLeadStatus().trim());
                    bundle.putString("PD", mPrivateLeadDatum.getLeadDate().trim());
                    bundle.putString("MK", mPrivateLeadDatum.getMake().trim());
                    bundle.putString("MD", mPrivateLeadDatum.getModel().trim());
                    bundle.putString("MV", mPrivateLeadDatum.getVariant().trim());
                    bundle.putString("PR", String.valueOf(mPrivateLeadDatum.getStockExpectedPrice()));
                    bundle.putString("RN", mPrivateLeadDatum.getRegisterNo().trim());
                    bundle.putString("NFD", mPrivateLeadDatum.getFollowDate().trim());
                    bundle.putString("ENM", mPrivateLeadDatum.getExecutiveName().trim());
                    bundle.putString("RGC", mPrivateLeadDatum.getCity().trim());
                    bundle.putString("OWN", mPrivateLeadDatum.getOwner().toString().trim());
                    bundle.putString("MYR", mPrivateLeadDatum.getRegisterYear().trim());
                    bundle.putString("CLR", mPrivateLeadDatum.getColor().trim());
                    bundle.putString("KMS", mPrivateLeadDatum.getKms().toString().trim());
                    bundle.putString("DID", mPrivateLeadDatum.getDealerId().trim());
                    bundle.putString("LID", mPrivateLeadDatum.getId().toString().trim());
                    bundle.putString("LT", mPrivateLeadDatum.getLeadType().trim());
                    bundle.putString("RMK", mPrivateLeadDatum.getRemark().trim());
                    bundle.putString("LD", holder.leads_days.getText().toString().trim());
                    bundle.putString("position", pos + "");
                    bundle.putString("lead_type", mPrivateLeadDatum.getLeadType());
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                }
            });
        }


    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView leads_name, leads_num, leads_comp_name, leads_car_model, leads_date, leads_days, leads_followup_history_warmorcold;
        public ImageView leads_Call, leads_Messege, leads_Whatsapp;

        public ViewHolder(View itemView) {
            super(itemView);
            leads_name = itemView.findViewById(R.id.leads_name);
            leads_num = itemView.findViewById(R.id.leads_num);
            leads_comp_name = itemView.findViewById(R.id.leads_comp_name);
            leads_car_model = itemView.findViewById(R.id.leads_car_model);
            leads_date = itemView.findViewById(R.id.leads_date);
            leads_days = itemView.findViewById(R.id.leads_days);
            leads_followup_history_warmorcold = itemView.findViewById(R.id.leads_followup_history_warmorcold);


            leads_Call = itemView.findViewById(R.id.leads_Call);
            leads_Messege = itemView.findViewById(R.id.leads_Messege);
            leads_Whatsapp = itemView.findViewById(R.id.leads_Whatsapp);

            if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {

            } else {
                leads_Call.setVisibility(View.INVISIBLE);
                leads_Messege.setVisibility(View.INVISIBLE);
                leads_Whatsapp.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private void validateDays(PrivateLeadDatum mPrivateLeadDatum, ViewHolder holder) {

        if (mPrivateLeadDatum.getCreatedAt() != null) {

            String createdDate = mPrivateLeadDatum.getCreatedAt();
            String[] splitDateTime = createdDate.split(" ", 10);
            String[] splitDate = splitDateTime[0].split("-", 10);
            String finalDate = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");
            Calendar c = Calendar.getInstance();
            String formattedDate = simpleDateFormat.format(c.getTime());
            String[] splitSpace = formattedDate.split(" ", 10);

            try {
                Date date1 = simpleDateFormat.parse(finalDate + " 00:00:00");
                Date date2 = simpleDateFormat.parse(splitSpace[0] + " 00:00:00");

                if (printDifference(date1, date2) == 0) {
                    holder.leads_days.setText("Today");
                } else if (printDifference(date1, date2) == 1) {
                    holder.leads_days.setText("Yesterday");
                } else if (printDifference(date1, date2) > 364) {
                    holder.leads_days.setText("" + printDifference(date1, date2) / 365 + " Y ago");
                } else {
                    holder.leads_days.setText("" + printDifference(date1, date2) + " D ago");
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }

        }else{
            holder.leads_days.setText("NA");
        }
    }

    private void validatefollowupDate(PrivateLeadDatum mPrivateLeadDatum, ViewHolder holder) {

        try {
            String[] splitdatespace = mPrivateLeadDatum.getFollowDate().split(" ", 10);
            String[] splitdate1 = splitdatespace[0].split("-", 10);
            holder.leads_date.setText(splitdate1[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate1[1])] + " " + splitdate1[0]);
        } catch (Exception e) {
            String leadDate = ((mPrivateLeadDatum.getFollowDate() == null || mPrivateLeadDatum.getFollowDate().trim().equalsIgnoreCase("NA") || mPrivateLeadDatum.getFollowDate().trim().equalsIgnoreCase("")) ? "NA" : mPrivateLeadDatum.getFollowDate().trim());
            holder.leads_date.setText(leadDate);
        }
    }

    private void updateLeadDetails(PrivateLeadDatum mPrivateLeadDatum, ViewHolder holder) {

        holder.leads_name.setText(mPrivateLeadDatum.getCustomerName().trim());
        holder.leads_num.setText(mPrivateLeadDatum.getCustomerMobile().trim());
        holder.leads_comp_name.setText(mPrivateLeadDatum.getMake().trim());
        if (!mPrivateLeadDatum.getVariant().trim().equals("null")) {
            holder.leads_car_model.setText(mPrivateLeadDatum.getModel().trim() + " " + mPrivateLeadDatum.getVariant().trim());
        } else {
            holder.leads_car_model.setText(mPrivateLeadDatum.getModel().trim());
        }

    }

    private void updateLeadStatus(PrivateLeadDatum mPrivateLeadDatum, ViewHolder holder) {

        holder.leads_followup_history_warmorcold.setVisibility(View.VISIBLE);

        try {
            holder.leads_followup_history_warmorcold.setText("  " + capitalize(mPrivateLeadDatum.getLeadStatus().trim()));
            if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Open")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.open_28);
            } else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("") || mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("null")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.open_28);
                holder.leads_followup_history_warmorcold.setText("Open");
                // holder.leads_followup_history_warmorcold.setText("NA");
            }
            else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Outstation")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.ic_outstation);
                holder.leads_followup_history_warmorcold.setText(mPrivateLeadDatum.getLeadStatus());
            }
            else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Hot")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.hot_28);
            } else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Warm")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.warm_28);
            } else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Cold")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.cold_28);
            } else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Lost")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.lost_28);
            } else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Sold")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.sold_28);
            } else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Visiting")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.ic_visiting);
            } else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Search")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.ic_search_lead);
            } else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Not-Interested")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.ic_not_intrested);
            } else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("No-Response") || mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Noresponse")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.ic_no_responce);
            } else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Finalised")) {
                holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.ic_finalized);
            } else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("null")) {
                holder.leads_followup_history_warmorcold.setText("Open");
                // holder.leads_followup_history_warmorcold.setText("NA");
            }
        } catch (Exception e) {

        }

    }


    public long printDifference(Date startDate, Date endDate) {
        long different = endDate.getTime() - startDate.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long elapsedDays = different / daysInMilli;
        return elapsedDays;
    }

    private String capitalize(String capString) {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()) {
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }


    private void openWhatsApp(String number) {
        String smsNumber = "91" + number; //without '+'
        try {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_VIEW);
            String url = "https://api.whatsapp.com/send?phone=" + smsNumber + "&text=" + "";
            sendIntent.setData(Uri.parse(url));
            activity.startActivity(sendIntent);

        } catch (Exception e) {
            Toast.makeText(context, "Error/n" + e.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    //searchfunctionaly PrivateLeads

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0) {
            list.addAll(itemListSearch);
        } else {
            for (PrivateLeadDatum it : itemListSearch) {
                if (it.getMake().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getModel().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getVariant().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getCustomerName().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getCustomerMobile().toLowerCase(Locale.getDefault()).contains(charText)
                ) {
                    list.add(it);
                }
            }
        }
        try {
            lazyloaderPrivateLeads.updatecountPrivate(list.size());
        } catch (Exception e) {

        }
        notifyDataSetChanged();

    }

}
