package com.mfcwl.mfc_dealer.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.mfcwl.mfc_dealer.Activity.LeadSection.LeadFilterActivitySection;
import com.mfcwl.mfc_dealer.Model.LeadstatusModel;
import com.mfcwl.mfc_dealer.R;

import org.json.JSONException;

import java.util.ArrayList;

public class LeadStatusAdapter extends ArrayAdapter {

    private ArrayList dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        CheckBox checkBox;
    }

    public LeadStatusAdapter(ArrayList data, Context context) {
        super(context, R.layout.row_checkbox, data);
        this.dataSet = data;
        this.mContext = context;

    }

    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public LeadstatusModel getItem(int position) {
        return (LeadstatusModel) dataSet.get(position);
    }


    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        ViewHolder viewHolder;
        final View result;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_checkbox, parent, false);
            viewHolder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);

            result = convertView;
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        LeadstatusModel item = getItem(position);
        viewHolder.checkBox.setChecked(item.checked);

        viewHolder.checkBox.setText(item.name);
        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                item.checked = isChecked;
                if (isChecked) {
                    LeadFilterActivitySection.leadstatusJsonArray.put(item.name);
                } else {
                    LeadStatusArray(item.name);
                }
            }
        });

        return result;
    }

    private void LeadStatusArray(String status) {
        try {
            for (int i = 0; i < LeadFilterActivitySection.leadstatusJsonArray.length(); i++) {
                String mStatus = LeadFilterActivitySection.leadstatusJsonArray.getString(i);
                if (mStatus.equalsIgnoreCase(status)) {
                    LeadFilterActivitySection.leadstatusJsonArray.remove(i);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}