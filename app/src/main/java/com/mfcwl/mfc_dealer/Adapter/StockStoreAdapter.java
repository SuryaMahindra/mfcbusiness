package com.mfcwl.mfc_dealer.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.Activity.StockStoreInfoActivity;
import com.mfcwl.mfc_dealer.Fragment.BookedStockFrag;
import com.mfcwl.mfc_dealer.Fragment.StockStoreFrag;
import com.mfcwl.mfc_dealer.InstanceCreate.Stocklisting;
import com.mfcwl.mfc_dealer.LazyLoader.ImageLoader;
import com.mfcwl.mfc_dealer.Model.StockModel;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchFlag;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchVal;

/**
 * Created by sowmya on 9/3/18.
 */

public class StockStoreAdapter extends RecyclerView.Adapter<StockStoreAdapter.ViewHolder> {
    private List<StockModel> stockModel;

    Activity activity;
    int lastItemPosition = -1;

    public static LinearLayout progress;
    boolean isLoading = false;
    public String coverimage = "";
    private List<StockModel> itemListSearch;

    public boolean Flag1 = false;
    public boolean Flag2 = false;
    public int myNum = 0;
    Picasso.Builder builder;

    public StockStoreAdapter(Activity act, List<StockModel> stockModels) {
        this.activity = act;
        this.stockModel = stockModels;
        builder = new Picasso.Builder(activity);
        itemListSearch = new ArrayList<StockModel>();
        itemListSearch.addAll(stockModel);
        //   updateFontUI(act);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.stock_store_row, parent, false);

        progress = view.findViewById(R.id.progress);
        CommonMethods.MemoryClears();

        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        StockModel sm = stockModel.get(position);
        try {
            if (position >= getItemCount() - 1 && getItemCount() > 0 && !searchFlag) {
                isLoading = true;
                if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("StockStore")) {
                    StockStoreFrag.loadmore(getItemCount());
                } else {
                    BookedStockFrag.loadmore(getItemCount());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (getItemCount() == 3) {
                holder.space.setVisibility(View.VISIBLE);
            } else {
                if (holder.space.getVisibility() == View.VISIBLE) {
                    holder.space.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        String createdDate = stockModel.get(position).getPostedDate().toString();

        //  Log.e("srockstore day", createdDate);
        if (createdDate.equalsIgnoreCase("")) {
            createdDate = "2017-00-00 00:00:00";
        }

        try {

            // Log.e("getPostedDate ","getPostedDate "+createdDate);
            String[] splitDateTime = createdDate.split(" ", 10);
            String[] splitDate = splitDateTime[0].split("-", 10);
            String finalDate = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");
            Calendar c = Calendar.getInstance();
            String formattedDate = simpleDateFormat.format(c.getTime());
            String[] splitSpace = formattedDate.split(" ", 10);

            Date date1 = simpleDateFormat.parse(finalDate + " 00:00:00");
            Date date2 = simpleDateFormat.parse(splitSpace[0] + " 00:00:00");

            // Log.e("diff ","diff "+date1 +" "+date2 +"=="+printDifference(date1, date2));

            String day = "" + printDifference(date1, date2);

            if (!day.equalsIgnoreCase("")) {

                try {
                    myNum = Integer.parseInt(day);
                } catch (NumberFormatException nfe) {
                   // System.out.println("Could not parse " + nfe);
                }
            }

            if (day.equalsIgnoreCase("0")) {
                holder.day_text.setText("" + "Today");
            } else if (day.equalsIgnoreCase("1")) {
                holder.day_text.setText("" + "Yesterday");
            } else if (myNum >= 365) {
                holder.day_text.setText("" + "1 Y ago");
            } else {
                holder.day_text.setText("" + printDifference(date1, date2) + " D ago");
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.stockCarName.setText(sm.getMake().toString());
        holder.stockVariant.setText(stockModel.get(position).getModel().toString() + " " +
                stockModel.get(position).getVariant().toString());
        holder.sellingPrice.setText(stockModel.get(position).getSellingPrice().toString());
        holder.stockRegistration.setText(stockModel.get(position).getRegistrationNumber().toString());

        if (stockModel.get(position).getPhotoCount().toString().equals("-1")) {
            holder.imageCount.setText("0");
        } else {
            holder.imageCount.setText(stockModel.get(position).getPhotoCount().toString());
        }

        holder.stockYear.setText(stockModel.get(position).getManufacture_year().toString());
        holder.stockkms.setText(stockModel.get(position).getKilometer().toString() + " " + "Kms");

        if (stockModel.get(position).getOwner().toString().equalsIgnoreCase("1") ||
                stockModel.get(position).getOwner().toString().equalsIgnoreCase("")) {
            holder.stockOwner.setText(stockModel.get(position).getOwner().toString() + " " + "Owner");
        } else {
            holder.stockOwner.setText(stockModel.get(position).getOwner().toString() + " " + "Owners");
        }

        //CommonMethods.MemoryClears();

        if (stockModel.get(position).getCoverimage() == null) {
            Picasso.get().cancelRequest(holder.car_image);
            holder.car_image.setImageResource(R.drawable.no_car_small);

        } else {
            if (!stockModel.get(position).getCoverimage().equalsIgnoreCase("")) {
                if (!stockModel.get(position).getPhotoCount().toString().equals("-1") ||
                        !stockModel.get(position).getPhotoCount().toString().equals("0")) {

                    Target target = new Target() {

                        @Override
                        public void onPrepareLoad(Drawable arg0) {

                        }

                        @Override
                        public void onBitmapLoaded(Bitmap arg0, Picasso.LoadedFrom arg1) {
                            holder.car_image.setImageBitmap(arg0);
                        }

                        @Override
                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                        }

                    };

                    Picasso.get()
                            .load(Uri.parse(stockModel.get(position).getCoverimage()))
                            .placeholder(R.drawable.small_loading)
                            .resize(200, 200)
                            // .networkPolicy(NetworkPolicy.NO_CACHE)
                            // .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                            .noFade()
                            .into(target);

                  /*  Picasso.get()
                            .load(Uri.parse(stockModel.get(position).getCoverimage()))
                            .placeholder(R.drawable.small_loading)
                            .resize(250, 250)
                            .noFade()
                            .into(holder.car_image);*/

                    /*new DownloadImageTask(holder.car_image)
                            .execute(stockModel.get(position).getCoverimage());*/

                } else {
                    Picasso.get().cancelRequest(holder.car_image);
                    holder.car_image.setImageResource(R.drawable.no_car_small);
                }
            } else {
                Picasso.get().cancelRequest(holder.car_image);
                holder.car_image.setImageResource(R.drawable.no_car_small);
            }
        }

        if (stockModel.get(position).getCertifiedText() == "") {
            holder.stockCertified.setVisibility(View.INVISIBLE);
        } else {

            if (stockModel.get(position).getIs_certified().toString().equals("true")) {
                holder.stockCertified.setText(stockModel.get(position).getCertifiedText().toString());
                holder.stockCertified.setVisibility(View.VISIBLE);
            } else {
                holder.stockCertified.setVisibility(View.INVISIBLE);
            }
        }


      /*  variantLayoutManager = new LinearLayoutManager(holder.itemView.getContext());
        holder.variantRecyclerView.setLayoutManager(variantLayoutManager);
        variantAdapter = new VariantAdapter(content.getVariants());
        holder.variantRecyclerView.setAdapter(variantAdapter);*/

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                searchVal = "";
                final String make = stockModel.get(position).getMake().toString();
                final String model = stockModel.get(position).model.toString()/* + " " +stockModel.get(position).getVariant().toString()*/;
                final String variant = stockModel.get(position).getVariant().toString();
                final String registraionCity = stockModel.get(position).getRegistraionCity();
                final String registraionNo = stockModel.get(position).getRegistrationNumber();
                final String color = stockModel.get(position).getColour().toString();
                final String certified = stockModel.get(position).getCertifiedText();
                final String owner = stockModel.get(position).getOwner().toString();
                final String kms = stockModel.get(position).getKilometer().toString();
                final String certifiedNo = stockModel.get(position).getCertificationNumber().toString();
                final String sellingPrice = stockModel.get(position).getSellingPrice().toString();
                final String yearMonth = stockModel.get(position).getRegMonth().toString() + " " + stockModel.get(position).getRegYear().toString();
                final String insurance = stockModel.get(position).getInsurance().toString();
                final String insuranceExp_date = stockModel.get(position).getInsuranceExpDate().toString();
                final String selling_price = stockModel.get(position).getSellingPrice().toString();
                final String Regyear = stockModel.get(position).getRegYear().toString();
                final String source = stockModel.get(position).getSource().toString();
                final String warranty = stockModel.get(position).getWarranty().toString();
                final String surveyor_remark = stockModel.get(position).getSurveyorRemark().toString();
                final String photo_count = stockModel.get(position).getPhotoCount().toString();
                final String reg_month = stockModel.get(position).getRegMonth().toString();

                final String fuel_type = stockModel.get(position).getFuel_type().toString();
                final String bought = stockModel.get(position).getBought().toString();
                final String refurbishment_cost = stockModel.get(position).getRefurbishment_cost().toString();
                final String dealer_price = stockModel.get(position).getDealer_price().toString();
                final String ProcurementExecId = stockModel.get(position).getProcurementExecId().toString();
                final String cng_kit = stockModel.get(position).getCng_kit().toString();
                final String chassis_number = stockModel.get(position).getChassis_number().toString();
                final String engine_number = stockModel.get(position).getEngine_number().toString();

                //final String is_display= stockModel.get(position).getIs_display().toString();
                final String is_certified = stockModel.get(position).getIs_certified().toString();
                String is_featured_car = "";
                try {
                    is_featured_car = stockModel.get(position).getIs_featured_car().toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                final String is_featured_car_admin = stockModel.get(position).getIs_featured_car_admin().toString();
                final String procurement_executive_name = stockModel.get(position).getProcurement_executive_name().toString();
                final String private_vehicle = stockModel.get(position).getPrivate_vehicle().toString();
                String comments = "";
                try {
                    comments = stockModel.get(position).getComments().toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                final String finance_required = stockModel.get(position).getFinance_required().toString();
                final String manufacture_month = stockModel.get(position).getManufacture_month().toString();
                final String sales_executive_id = stockModel.get(position).getSales_executive_id().toString();
                final String sales_executive_name = stockModel.get(position).getSales_executive_name().toString();
                final String manufacture_year = stockModel.get(position).getManufacture_year().toString();
                final String actual_selling_price = stockModel.get(position).getActual_selling_price().toString();
                final String CertifiedText = stockModel.get(position).getCertifiedText().toString();
                // final String IsDisplay = stockModel.get(position).getIsDisplay().toString();
                //final String TransmissionType = stockModel.get(position).getTransmissionType().toString();
                final String colour = stockModel.get(position).getColour().toString();
                final String dealer_code = stockModel.get(position).getDealerCode().toString();
                final String is_offload = stockModel.get(position).getIsOffload().toString();
                final String posted_date = stockModel.get(position).getPostedDate().toString();
                final String stock_id = stockModel.get(position).getId().toString();
                final String is_booked = stockModel.get(position).getIsbooked().toString();
                final String is_sold = stockModel.get(position).getIssold();
                if (!stockModel.get(position).getCoverimage().toString().equalsIgnoreCase("")
                        && stockModel.get(position).getCoverimage().toString() != null) {
                    coverimage = stockModel.get(position).getCoverimage().toString();
                } else {
                    coverimage = "";
                }

                final JSONArray images = stockModel.get(position).getImageUrl();

                CommonMethods.MemoryClears();

                ArrayList<String> imageurls;
                ArrayList<String> imagenames;
                imageurls = new ArrayList<String>();
                imagenames = new ArrayList<String>();

               /* for (int i = 0; i < images.length(); i++) {

                    try {
                        Log.e("imageurl","imageurl=="+images.get(i));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }*/

                if (images != null && images.length() > 0) {
                    for (int i = 0; i < images.length(); i++) {
                        try {
                            JSONObject data = images.getJSONObject(i);
                            imageurls.add(data.getString("url"));
                            imagenames.add(data.getString("category_identifier"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    imageurls.add("url");
                    imagenames.add("category_identifier");
                }

                Object[] objectList = imageurls.toArray();
                String[] imageurl = Arrays.copyOf(objectList, objectList.length, String[].class);

                Object[] objectList_imagenames = imagenames.toArray();
                String[] imagename = Arrays.copyOf(objectList_imagenames, objectList_imagenames.length, String[].class);

              /*  for (int i = 0; i < images.length(); i++) {

                    Log.e("imageurl","imageurl"+imageurl[i]);
                    Log.e("imagename","imagename"+imagename[i]);

                }*/
                Intent intent = new Intent(activity.getBaseContext(), StockStoreInfoActivity.class);
                intent.putExtra("make", make);
                intent.putExtra("model", model);
                CommonMethods.setvalueAgainstKey(activity, "booknowbtn", "false");

                intent.putExtra("variant", variant);
                intent.putExtra("registraionCity", registraionCity);
                intent.putExtra("registraionNo", registraionNo);
                intent.putExtra("color", color);
                intent.putExtra("certified", certified);
                intent.putExtra("owner", owner);
                intent.putExtra("kms", kms);
                intent.putExtra("Regyear", Regyear);
                intent.putExtra("certifiedNo", certifiedNo);
                intent.putExtra("sellingPrice", sellingPrice);
                intent.putExtra("yearMonth", yearMonth);
                intent.putExtra("insurance", insurance);
                intent.putExtra("insuranceExp_date", insuranceExp_date);
                intent.putExtra("selling_price", selling_price);
                intent.putExtra("source", source);
                intent.putExtra("warranty", warranty);
                intent.putExtra("surveyor_remark", surveyor_remark);
                intent.putExtra("photo_count", photo_count);
                intent.putExtra("reg_month", reg_month);
                intent.putExtra("fuel_type", fuel_type);
                intent.putExtra("bought", bought);
                intent.putExtra("refurbishment_cost", refurbishment_cost);
                intent.putExtra("dealer_price", dealer_price);
                intent.putExtra("ProcurementExecId", ProcurementExecId);
                intent.putExtra("cng_kit", cng_kit);
                intent.putExtra("chassis_number", chassis_number);
                intent.putExtra("engine_number", engine_number);

                intent.putExtra("is_certified", is_certified);
                intent.putExtra("is_featured_car", is_featured_car);
                //    intent.putExtra("is_featured_car_admin", is_featured_car_admin);
                intent.putExtra("procurement_executive_name", procurement_executive_name);
                intent.putExtra("private_vehicle", private_vehicle);
                intent.putExtra("comments", comments);
                intent.putExtra("finance_required", finance_required);
                intent.putExtra("manufacture_month", manufacture_month);
                intent.putExtra("sales_executive_id", sales_executive_id);
                intent.putExtra("sales_executive_name", sales_executive_name);
                intent.putExtra("manufacture_year", manufacture_year);
                intent.putExtra("actual_selling_price", actual_selling_price);
                intent.putExtra("CertifiedText", CertifiedText);
                //intent.putExtra("IsDisplay",IsDisplay);
                // intent.putExtra("TransmissionType", TransmissionType);
                intent.putExtra("colour", colour);
                intent.putExtra("dealer_code", dealer_code);
                intent.putExtra("is_offload", is_offload);
                intent.putExtra("posted_date", posted_date);
                intent.putExtra("stock_id", stock_id);
                intent.putExtra("is_sold", is_sold);
                intent.putExtra("is_booked", is_booked);
                intent.putExtra("imageurl", imageurl);
                intent.putExtra("imagename", imagename);
                intent.putExtra("coverimage", coverimage);

                CommonMethods.setvalueAgainstKey(activity, "stock_id", stock_id);

                if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("StockStore")) {
                    Stocklisting.getInstance().setStockstatus("StockStoreFrag");
                } else if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("BookedStock")) {
                    Stocklisting.getInstance().setStockstatus("BookedStockFrag");
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                // activity.finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return stockModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView stockCarName, stockVariant, sellingPrice, stockRegistration, imageCount,
                stockYear, stockkms, stockOwner, stockCertified, day_text, space;

        public RelativeLayout recycleview;
        public CircleImageView car_image;

        public ViewHolder(View itemView) {
            super(itemView);

            space = itemView.findViewById(R.id.space);
            stockCarName = itemView.findViewById(R.id.asm_message);
            day_text = itemView.findViewById(R.id.day_text);
            stockVariant = itemView.findViewById(R.id.stock_variant);
            sellingPrice = itemView.findViewById(R.id.textView2);
            stockRegistration = itemView.findViewById(R.id.sales_registration);
            imageCount = itemView.findViewById(R.id.image_number);
            stockYear = itemView.findViewById(R.id.stock_year);
            stockkms = itemView.findViewById(R.id.stock_kms);
            stockOwner = itemView.findViewById(R.id.stock_owner);
            stockCertified = itemView.findViewById(R.id.stock_certified);
            car_image = itemView.findViewById(R.id.car_image);

            updateFontUI(activity, itemView);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void updateFontUI(Activity context, View convert) {
        ArrayList<Integer> arl = new ArrayList<Integer>();
        arl.add(R.id.asm_message);
        arl.add(R.id.stock_variant);
      /*  arl.add(R.id.stock_registration);
        arl.add(R.id.stock_certified);*/

        setFontStyle(arl, context, convert);
    }

    private void setFontStyle(ArrayList<Integer> arl, Activity activity, View viewHolder) {
        for (int i = 0; i < arl.size(); i++) {
            Typeface myTypeface = Typeface.createFromAsset(activity.getAssets(), "lato_semibold.ttf");
            TextView myTextView = viewHolder.findViewById(arl.get(i));
            myTextView.setTypeface(myTypeface);
        }
    }

    public void notifyDataChanged(List<StockModel> stockModels) {

        itemListSearch.addAll(stockModels);

        notifyDataSetChanged();

        itemListSearch.clear();
        itemListSearch.addAll(stockModel);

        //notifyItemInserted(getItemCount()-1);
        isLoading = false;
    }

    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());
        stockModel.clear();
        if (charText.length() == 0) {
            stockModel.addAll(itemListSearch);
        } else {
            for (StockModel it : itemListSearch) {
                if (it.getMake().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getModel().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getVariant().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getRegistrationNumber().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getManufacture_year().toLowerCase(Locale.getDefault()).contains(charText)

                        ) {
                    stockModel.add(it);
                }
            }
        }
        notifyDataSetChanged();
    }


    public long printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        /*System.out.println("startDate : " + startDate);
        System.out.println("endDate : "+ endDate);
        System.out.println("different : " + different);*/

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        /*System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

        Log.e("elapsedDays ","elapsedDays "+elapsedDays);*/
        return elapsedDays;
    }

    private void updateListImage(Context ctx, String imagepath, ImageView iv) {
        ImageLoader imageLoader = new ImageLoader(ctx);
        imageLoader.DisplayImage(CommonMethods.replaceStringURL(imagepath), iv);
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            if (result != null)
                bmImage.setImageBitmap(result);
            else
                bmImage.setImageResource(R.drawable.no_car_small);
        }
    }
}
