package com.mfcwl.mfc_dealer.Adapter.Leads;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.Activity.Leads.LeadsInstance;
import com.mfcwl.mfc_dealer.Model.Leads.FollowUpHistoryLeadModel;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.MonthUtility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by HP 240 G5 on 12-03-2018.
 */

public class FollowUpHistoryLeadsAdapter extends RecyclerView.Adapter<FollowUpHistoryLeadsAdapter.ViewHolder> {
    List<FollowUpHistoryLeadModel> list = new ArrayList<>();
    Context context;
    Activity activity;
    String calculateTime = "";

    public FollowUpHistoryLeadsAdapter(Context context, List<FollowUpHistoryLeadModel> list, Activity activity) {
        this.list = list;
        this.context = context;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_followup_history_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {


        final FollowUpHistoryLeadModel followUpHistoryLeadModel = list.get(position);

        String nextFollowup = "";
        if (LeadsInstance.getInstance().getWhichleads().equals("WebLeads")) {
            String[] splitdatespace = followUpHistoryLeadModel.getFolowupdate().trim().split(" ", 10);
            String[] splitdate1 = splitdatespace[0].split("-", 10);
            try {
                nextFollowup = splitdate1[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate1[1])] + " " + splitdate1[0];
            } catch (Exception e) {

                Log.e("Exception ", "nextFollowup " + e.toString());

            }
        } else {
            Log.e("whproblm ", "first else");
            String[] splitdatespace = followUpHistoryLeadModel.getFolowupdate().trim().split(" ", 10);
            String[] splitdate1 = splitdatespace[0].split("-", 10);
            Log.e("whproblm ", "else");
            try {
                Log.e("whproblm ", "try 1");
                nextFollowup = splitdate1[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate1[1])] + " " + splitdate1[0];
            } catch (Exception e) {

                Log.e("whproblm ", "nextFollowup");

            }
        }


        String sourceString1 = "Next Follow-up " + "<font color=\"black\">" + nextFollowup + "</font>";
        //  Log.e("nextFollowup ", "nextFollowup " + nextFollowup);

        if (followUpHistoryLeadModel.getFolowupstatus().trim().equalsIgnoreCase("Sold") || followUpHistoryLeadModel.getFolowupstatus().trim().equalsIgnoreCase("Lost") || nextFollowup.equalsIgnoreCase("00  0000")) {
            nextFollowup = "NA";
            String sourceString2 = "Next Follow-up " + "<font color=\"black\">" + nextFollowup + "</font>";
            holder.leads_followup_history_next_folowup.setText(Html.fromHtml(sourceString2));
        } else {
            holder.leads_followup_history_next_folowup.setText(Html.fromHtml(sourceString1));
        }
        holder.leads_followup_history_booked.setText(followUpHistoryLeadModel.getFolowupremarks());

        holder.leads_followup_history_warmorcold.setText(followUpHistoryLeadModel.getFolowupstatus());


        String createdDate = "";
        if (LeadsInstance.getInstance().getWhichleads().equals("WebLeads")) {
            String[] splitdatespace = followUpHistoryLeadModel.getFolowupcreated().trim().split(" ", 10);
            String[] splitdate1 = splitdatespace[0].split("-", 10);
            createdDate = splitdate1[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate1[1])] + " " + splitdate1[0];
        } else {
            String[] splitdatespace = followUpHistoryLeadModel.getFolowupcreated().trim().split(" ", 10);
            String[] splitdate1 = splitdatespace[0].split("-", 10);
            createdDate = splitdate1[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate1[1])] + " " + splitdate1[0];
        }

        holder.leads_followup_history_date.setText(createdDate);

        calculateTime = followUpHistoryLeadModel.getFolowupcreated().trim().replace(" ", " ");
        StringTokenizer tk = new StringTokenizer(calculateTime);
        String date = tk.nextToken();
        String time = tk.nextToken();

        /*SimpleDateFormat sdf = new SimpleDateFormat("KK:mm aa");
        SimpleDateFormat sdfs = new SimpleDateFormat("KK:mm aa");*/

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm aa");

        SimpleDateFormat date24Format = new SimpleDateFormat("HH:mm");

        Date dt;
        try {
            dt = sdf.parse(time);
            holder.leads_followup_history_time.setText(sdfs.format(dt));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            Log.e("whproblm ", "time ah");
            e.printStackTrace();
        }

        if (followUpHistoryLeadModel.getFolowupstatus().trim().equalsIgnoreCase("Open")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.open_28);
            holder.leads_followup_history_warmorcold.setText("Open");
        }
        if (followUpHistoryLeadModel.getFolowupstatus().trim().equalsIgnoreCase("Hot")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.hot_28);
            holder.leads_followup_history_warmorcold.setText("Hot");
        }
        if (followUpHistoryLeadModel.getFolowupstatus().trim().equalsIgnoreCase("Warm")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.warm_28);
            holder.leads_followup_history_warmorcold.setText("Warm");
        }
        if (followUpHistoryLeadModel.getFolowupstatus().trim().equalsIgnoreCase("Cold")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.cold_28);
            holder.leads_followup_history_warmorcold.setText("Cold");
        }
        if (followUpHistoryLeadModel.getFolowupstatus().trim().equalsIgnoreCase("Lost")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.lost_28);
            holder.leads_followup_history_warmorcold.setText("Lost");
        }
        if (followUpHistoryLeadModel.getFolowupstatus().trim().equalsIgnoreCase("Sold")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.sold_28);
            holder.leads_followup_history_warmorcold.setText("Sold");
        }
        if (followUpHistoryLeadModel.getFolowupstatus().trim().equalsIgnoreCase("Visiting") || followUpHistoryLeadModel.getFolowupstatus().trim().equalsIgnoreCase("visiting")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.ic_visiting);
            holder.leads_followup_history_warmorcold.setText(followUpHistoryLeadModel.getFolowupstatus().toString());
        }
        if (followUpHistoryLeadModel.getFolowupstatus().trim().equalsIgnoreCase("Search") || followUpHistoryLeadModel.getFolowupstatus().trim().equalsIgnoreCase("search")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.ic_search_lead);
            holder.leads_followup_history_warmorcold.setText(followUpHistoryLeadModel.getFolowupstatus().toString());
        }
        if (followUpHistoryLeadModel.getFolowupstatus().trim().equalsIgnoreCase("Not-Interested") || followUpHistoryLeadModel.getFolowupstatus().trim().equalsIgnoreCase("not-interested")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.ic_not_intrested);
            holder.leads_followup_history_warmorcold.setText(followUpHistoryLeadModel.getFolowupstatus().toString());
        }
        if (followUpHistoryLeadModel.getFolowupstatus().trim().equalsIgnoreCase("Finalised") || followUpHistoryLeadModel.getFolowupstatus().trim().equalsIgnoreCase("finalised")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.ic_finalized);
            holder.leads_followup_history_warmorcold.setText(followUpHistoryLeadModel.getFolowupstatus().toString());
        }
        if (followUpHistoryLeadModel.getFolowupstatus().trim().equalsIgnoreCase("No-Response") || followUpHistoryLeadModel.getFolowupstatus().trim().equalsIgnoreCase("no-response")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.ic_no_responce);
            holder.leads_followup_history_warmorcold.setText(followUpHistoryLeadModel.getFolowupstatus().toString());
        }
        if (followUpHistoryLeadModel.getFolowupremarks().equals("") || followUpHistoryLeadModel.getFolowupremarks().equals("NA") || followUpHistoryLeadModel.getFolowupremarks().equals("null")) {
            holder.leads_followup_history_booked.setText("No comments available");
        }
        if (followUpHistoryLeadModel.getFolowupstatus().trim().equalsIgnoreCase("") || followUpHistoryLeadModel.getFolowupstatus().trim().equalsIgnoreCase("NA") || followUpHistoryLeadModel.getFolowupstatus().trim().equalsIgnoreCase("null")) {
            holder.leads_followup_history_warmorcold.setText("Open");
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.open_28);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView leads_followup_history_date, leads_followup_history_next_folowup, leads_followup_history_booked, leads_followup_history_warmorcold, leads_followup_history_time;

        public ViewHolder(View itemView) {
            super(itemView);
            leads_followup_history_booked = itemView.findViewById(R.id.leads_followup_history_booked);
            leads_followup_history_date = itemView.findViewById(R.id.leads_followup_history_date);
            leads_followup_history_next_folowup = itemView.findViewById(R.id.leads_followup_history_next_folowup);
            leads_followup_history_warmorcold = itemView.findViewById(R.id.leads_followup_history_warmorcold);
            leads_followup_history_time = itemView.findViewById(R.id.leads_followup_history_time);
        }
    }
}

