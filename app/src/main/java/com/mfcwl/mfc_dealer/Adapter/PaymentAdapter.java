package com.mfcwl.mfc_dealer.Adapter;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.Model.PaymentData;
import com.mfcwl.mfc_dealer.R;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class PaymentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private List<PaymentData> alist;
    private Context context;
    private Activity activity;
    private boolean isLoadingAdded = false;
    private List<PaymentData> mSearch;

    public PaymentAdapter(Context context) {
        this.context = context;
        activity = (Activity) context;
        alist = new ArrayList<>();
        mSearch = new ArrayList<>();
        mSearch.addAll(alist);
    }

    public List<PaymentData> getalist() {
        return alist;
    }

    public void setalist(List<PaymentData> alist) {
        this.alist = alist;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(v2);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.row_all_payment, parent, false);
        viewHolder = new PaymentAdapter.MyViewHolder(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        PaymentData data = alist.get(position);

        switch (getItemViewType(position)) {
            case ITEM:

                try {
                    PaymentAdapter.MyViewHolder Aucholder = (PaymentAdapter.MyViewHolder) holder;

                    if (data.getPaymentGatewayTransactionId() != null) {
                        Aucholder.trans_id.setText("Transaction Id : "+data.getPaymentGatewayTransactionId());
                    } else {
                        Aucholder.trans_id.setText("Transaction Id : NA");
                    }

                    if (data.getTransactionType() != null) {
                        Aucholder.paymentstyle.setText(data.getTransactionType());
                    } else {
                        Aucholder.paymentstyle.setText("NA");
                    }

                    Aucholder.deposit_tv.setText(data.getOperationStatus());

                    if (data.getOperationStatus().equalsIgnoreCase("Deposit")) {

                        Aucholder.price_tv.setTextColor(activity.getResources().getColor(R.color.green_color_picker));
                    } else {
                        Aucholder.price_tv.setTextColor(activity.getResources().getColor(R.color.Red));

                    }

                    NumberFormat formatter = new DecimalFormat("#,##,##,###");
                    String price = "0";
                    try {
                        price = formatter.format(data.getAmount());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    Aucholder.price_tv.setText(" " + price);

                    String date = "", val = data.getCreatedOn();

                    if (val != null && !val.isEmpty()) {

                        if (val.length() >= 10) {
                            date = val.substring(0, 10);
                        }
                    } else {
                        date = "NA";
                    }

                    Aucholder.date_tv.setText(date);


                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case LOADING:
//                Do nothing
                break;
        }

    }

    @Override
    public int getItemCount() {
        return alist == null ? 0 : alist.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == alist.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    /*
   Helpers
   _________________________________________________________________________________________________
    */

    public void add(PaymentData mc) {
        alist.add(mc);
        notifyItemInserted(alist.size() - 1);
    }

    public void addAll(List<PaymentData> mcList) {
        for (PaymentData mc : mcList) {
            add(mc);
        }
        mSearch.clear();
        mSearch.addAll(alist);
    }

    public void remove(PaymentData city) {
        int position = alist.indexOf(city);
        if (position > -1) {
            alist.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new PaymentData());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = alist.size() - 1;
        PaymentData item = getItem(position);

        if (item != null) {
            alist.remove(position);
            notifyItemRemoved(position);
        }
    }

    public PaymentData getItem(int position) {
        return alist.get(position);
    }


   /*
   View Holders
   _________________________________________________________________________________________________
    */

    /**
     * Main list's content ViewHolder
     */
    protected class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView trans_id, paymentstyle, price_tv, deposit_tv, date_tv;

        public MyViewHolder(View itemView) {
            super(itemView);

            trans_id = (TextView) itemView.findViewById(R.id.trans_id);
            paymentstyle = (TextView) itemView.findViewById(R.id.paymentstyle);
            price_tv = (TextView) itemView.findViewById(R.id.price_tv);
            deposit_tv = (TextView) itemView.findViewById(R.id.deposit_tv);
            date_tv = (TextView) itemView.findViewById(R.id.date_tv);


        }
    }


    protected class LoadingVH extends RecyclerView.ViewHolder {

        public LoadingVH(View itemView) {
            super(itemView);
        }
    }

    // Search Filter
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        alist.clear();
        if (charText.length() == 0) {
            alist.addAll(mSearch);
        } else {

            for (PaymentData it : mSearch) {
                if (it.getName().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getErpReceiptNo().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getRemark().toLowerCase(Locale.getDefault()).contains(charText)) {

                    alist.add(it);
                }
            }
        }
        notifyDataSetChanged();
    }
}