package com.mfcwl.mfc_dealer.NetworkConnect;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.Global;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.Global_Urls;

import org.json.JSONException;
import org.json.JSONObject;

import static com.mfcwl.mfc_dealer.Utility.Global.IbbAccessToken;
import static com.mfcwl.mfc_dealer.Utility.Global.leadAccessToken;


public class WebServicesCall implements WebServices.CallbackInterface {

    private static Activity activity;
    private static String strMethod;
    private static Context context;
    private static int requestMethods;

    public static void webCall1(Activity activity, Context context,
                                JSONObject jObj, String strMethod, String str, int requestMethod) {
        if (CommonMethods.isInternetWorking(activity)) {

            WebServicesCall webServicesCall = new WebServicesCall();

            WebServicesCall.activity = activity;
            WebServicesCall.strMethod = strMethod;
            WebServicesCall.context = context;

            requestMethods = requestMethod;
            if (strMethod.equals("GetModelVariant")) {
                webServicesCall.webcallforgetmodelvariant(jObj, str);
            }
            if (strMethod.equals("ProGetModelVariant")) {
                webServicesCall.webcallforgetmodelvariant(jObj, str);
            }
            if (strMethod.equals("EditStockGetModelVariant")) {
                webServicesCall.webcallforgetmodelvariant(jObj, str);
            } else if (strMethod.equals("LeadsGetModelVariant")) {
                webServicesCall.webcallforleadsgetmodelvariant(jObj, str);
            }


        } else {
            CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
        }
    }

    public static void webCall(Activity activity, Context context,
                               JSONObject jObj, String strMethod, int requestMethod) {

        //  Log.e("home_submit_etv1", "home_submit_etv1" + strMethod);

        if (CommonMethods.isInternetWorking(activity)) {

            WebServicesCall webServicesCall = new WebServicesCall();

            WebServicesCall.activity = activity;
            WebServicesCall.strMethod = strMethod;
            WebServicesCall.context = context;

            requestMethods = requestMethod;

            if (strMethod.equals("Login")) {
                webServicesCall.webcallforLogin(jObj);
            } else if (strMethod.equals("AppVersion")) {
                webServicesCall.webcallforAppVersion(jObj);
            } else if (strMethod.equals("Logout")) {
                webServicesCall.webcallforlogout(jObj);
            } else if (strMethod.equals("StockStore")) {
                webServicesCall.webcallforstockstore(jObj);
            } else if (strMethod.equals("ToolsStockStore")) {
                webServicesCall.webcallfortoolsstockstore(jObj);
            } else if (strMethod.equals("ToolsStockDetails")) {
                webServicesCall.webcallfortoolsstockdetails(jObj);
            } else if (strMethod.equals("Notification_2")) {
                webServicesCall.webcallforNotification(jObj);
            } else if (strMethod.equals("BookedStock")) {
                webServicesCall.webcallforbookedstock(jObj);
            } else if (strMethod.equals("BoookNow")) {
                webServicesCall.webcallforbooknow(jObj);
            } else if (strMethod.equals("FeatureCar")) {
                webServicesCall.webcallforbooknow(jObj);
            } else if (strMethod.equals("FeaturedCarlist")) {
                webServicesCall.webcallforstockstore(jObj);
            } else if (strMethod.equals("EditStock")) {
                webServicesCall.webcallforeditstock(jObj);
            } else if (strMethod.equals("GetCity")) {
                webServicesCall.webcallforgetcity(jObj);
            } else if (strMethod.equals("EditStockGetCity")) {
                webServicesCall.webcallforgetcity(jObj);
            } else if (strMethod.equals("PermissionDear")) {
                webServicesCall.webcallforPersionDealery(jObj);
            } else if (strMethod.equals("deleteFeatureCar")) {
                webServicesCall.webcallforbooknow(jObj);
            } else if (strMethod.equals("StockDetails")) {
                webServicesCall.webcallforstockdetails(jObj);
            } else if (strMethod.equals("SalesDetails")) {
                webServicesCall.webcallforstockdetails(jObj);
            }
            //Dashboard
            else if (strMethod.equals("dashBroad")) {
                webServicesCall.webcallforDashBroad(jObj);
            } else if (strMethod.equals("AddRegularStock")) {
                webServicesCall.webcallforaddregularstock(jObj);
            }

            //IBB
            else if (strMethod.equals("IBBAccessToken")) {
                webServicesCall.webcallforIBBAccessToken(jObj, activity);
            } else if (strMethod.equals("IBByear")) {
                webServicesCall.webcallforIBByear(jObj, activity);
            } else if (strMethod.equals("IBBmonth")) {
                webServicesCall.webcallforIBBmonth(jObj, activity);
            } else if (strMethod.equals("IBBmake")) {
                webServicesCall.webcallforIBBmake(jObj, activity);
            } else if (strMethod.equals("IBBmodel")) {
                webServicesCall.webcallforIBBmodel(jObj, activity);
            } else if (strMethod.equals("IBBvariant")) {
                webServicesCall.webcallforIBBvariant(jObj, activity);
            } else if (strMethod.equals("IBBowner")) {
                webServicesCall.webcallforIBBowner(jObj, activity);
            } else if (strMethod.equals("IBBcolor")) {
                webServicesCall.webcallforIBBcolor(jObj, activity);
            } else if (strMethod.equals("IBBcity")) {
                webServicesCall.webcallforIBBcity(jObj, activity);
            } else if (strMethod.equals("IBBcheckPrice")) {
                webServicesCall.webcallforIBBcheckPrice(jObj, activity);
            }
            //Lead
            else if (strMethod.equals("LeadAccessToken")) {
                //    Log.e("Read-1", "Read-1");
                webServicesCall.webcallforLeadAccessToken(jObj, activity);
            } else if (strMethod.equals("LeadStore")) {
                webServicesCall.webcallforleadstore(jObj, activity);
            } else if (strMethod.equals("WebLeadStore")) {
                webServicesCall.webcallforWebLeadStore(jObj, activity);
            } else if (strMethod.equals("PrivateLeadStore")) {
                webServicesCall.webcallforprivateleadstore(jObj, activity);
            } else if (strMethod.equals("PrivateUpdateLeadStore")) {
                webServicesCall.webcallforPrivateUpdateLeadStore(jObj, activity);
            } else if (strMethod.equals("UpdateFollowLead")) {
                webServicesCall.webcallforupdatefollowlead(jObj, activity);
            } else if (strMethod.equals("ProcUpdateFollowLead")) {
                webServicesCall.webcallforupdatefollowlead(jObj, activity);
            } else if (strMethod.equals("UpdateFollowLeadForBooknow")) {
                webServicesCall.webcallforUpdateFollowLeadForBooknow(jObj, activity);
            } else if (strMethod.equals("UpdateFollowLeadPrivate")) {
                webServicesCall.webcallforupdatefollowleadprivate(jObj, activity);
            } else if (strMethod.equals("UpdateFollowLeadPrivateForBookNow")) {
                webServicesCall.webcallforUpdateFollowLeadPrivateForBookNow(jObj, activity);
            } else if (strMethod.equals("FollowUpHistory")) {
                webServicesCall.webcallfollowuphistory(jObj, activity);
            } else if (strMethod.equals("ProcFollowUpHistory")) {
                webServicesCall.webcallProcfollowuphistory(jObj, activity);
            } else if (strMethod.equals("AddLead")) {
                webServicesCall.webcalladdLead(jObj, activity);
            } else if (strMethod.equals("BooknowGetStock")) {
                webServicesCall.webbooknowGetStock(jObj, activity);
            } else if (strMethod.equals("ParsePerticularStock")) {
                webServicesCall.webbookparseperticustock(jObj, activity);
            } else if (strMethod.equals("BooknowStock")) {
                webServicesCall.webbookbooknowstock(jObj, activity);
            } else if (strMethod.equals("UpdateFollowLeadBookNow")) {
                webServicesCall.webcallforupdatefollowleadbooknow(jObj, activity);
            } else if (strMethod.equals("FollowUpHistoryPrivateLeads")) {
                webServicesCall.webcallforFollowUpHistoryPrivateLeads(jObj, activity);
            } else if (strMethod.equals("LeadStoreCount")) {
                webServicesCall.webcallforleadstorecount(jObj, activity);
            } else if (strMethod.equalsIgnoreCase("ParkAndSellDelete")) {
                try {
                    webServicesCall.webcallforDeleteParkAndSell(jObj.getString("stock_id"), activity);
                } catch (Exception e) {
                    Log.i("TAG", "webCall: ");
                }
            }

        } else {
            CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
        }

    }

    public void webcallforDeleteParkAndSell(String stockId, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.parkAndSellDelete + stockId, getAccessTokenjObj().toString());
    }

    private JSONObject getAccessTokenjObj() {
        JSONObject jObj = new JSONObject();
        try {
            jObj.put("access_token", CommonMethods.getstringvaluefromkey(activity, "access_token"));
        } catch (Exception e) {
        }

        return jObj;
    }

    //ibb
    private void webcallforIBBcheckPrice(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.ibbMFCURL + "MFC", jObj.toString());
    }

    private void webcallforIBBcity(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global_Urls.ibbMFC + "MFC", jObj.toString());
    }

    private void webcallforIBBcolor(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.ibbMFCURL + "MFC", jObj.toString());
    }

    private void webcallforIBBowner(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.ibbMFCURL + "MFC", jObj.toString());
    }

    private void webcallforIBBvariant(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.ibbMFCURL + "MFC", jObj.toString());
    }

    private void webcallforIBBmodel(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.ibbMFCURL + "MFC", jObj.toString());
    }

    private void webcallforIBBmake(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.ibbMFCURL + "MFC", jObj.toString());
    }

    private void webcallforIBByear(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.ibbMFCURL + "MFC", jObj.toString());
    }

    private void webcallforIBBmonth(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.ibbMFCURL + "MFC", jObj.toString());
    }

    private void webcallforIBBAccessToken(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(IbbAccessToken, jObj.toString());
    }

    public void webcallforLogin(JSONObject jObj) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.commonBaseURL + "login/users", jObj.toString());
    }

    //reena
    public void webcallforDashBroad(JSONObject jObj) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.stock_dealerURL + "dashboard/dashboard", jObj.toString());
    }

    public void webcallforbookedstock(JSONObject jObj) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.mainURL, jObj.toString());
    }

    public void webcallforNotification(JSONObject jObj) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.stock_dealerURL + "dealer/pull-notifications", jObj.toString());
    }

    public void webcallforbooknow(JSONObject jObj) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.stock_dealerURL + "dealer/stocks/update", jObj.toString());
    }

    public void webcallforstockdetails(JSONObject jObj) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.mainURL, jObj.toString());
    }

    public void webcallforeditstock(JSONObject jObj) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.stock_dealerURL + "dealer/stocks/update", jObj.toString());
    }

    public void webcallforAppVersion(JSONObject jObj) {

        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.stock_dealerURL + "token/checkappversion", jObj.toString());
    }

    public void webcallforlogout(JSONObject jObj) {

        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.stock_dealerURL + "logout/dealer-logout", jObj.toString());
    }

    public void webcallforstockstore(JSONObject jObj) {

        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.mainURL, jObj.toString());
    }

    public void webcallfortoolsstockstore(JSONObject jObj) {

        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.mainURL, jObj.toString());
    }

    public void webcallfortoolsstockdetails(JSONObject jObj) {

        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.stock_dealerURL + "dealer/get-details", jObj.toString());
    }


    public void webcallforgetcity(JSONObject jObj) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.cityUrl, jObj.toString());
    }


    public void webcallforgetmake(JSONObject jObj) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.addstockURL + "makelist", jObj.toString());
    }

    public void webcallforPersionDealery(JSONObject jObj) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.stock_dealerURL + "dealer/permissions", jObj.toString());
    }

    public void webcallforgetmodelvariant(JSONObject jObj, String str) {
        Log.e("getmodelvariant ", "getmodelvariant " + str);
        WebServices parser = new WebServices(context, this, requestMethods);

        if (CommonMethods.getstringvaluefromkey(activity, "Commercial").equalsIgnoreCase("true")) {

            String url = Global.addstockURL + "commercial-modeldetails/" + str;
            url = url.replace(" ", "%20");
            parser.execute(url, jObj.toString());

            Log.e("commercial-modeldetails", "commercial-modeldetails url " + url);

        } else {

            String url = Global.addstockURL + "modeldetails/" + str;
            url = url.replace(" ", "%20");
            Log.e("commercial-modeldetails", "commercial-modeldetails url " + url);
            parser.execute(url, jObj.toString());
            Log.e("modeldetails ", "modeldetails url " + url);

        }

    }


    public void webcallforaddregularstock(JSONObject jObj) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.stock_dealerURL + "dealer/stocks/create", jObj.toString());
    }

    //Lead

    public void webcallforLeadAccessToken(JSONObject jObj, Activity activity) {

        WebServices parser = new WebServices(context, this, requestMethods);

        parser.execute(leadAccessToken, jObj.toString());

    }


    public void webcallforleadstore(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.leadeadBaseUrl + "mfcwapp/getleads", jObj.toString());

    }

    public void webcallforWebLeadStore(JSONObject jObj, Activity activity) {
        Log.e("WebLeadStore ", "jobj " + jObj.toString());
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.leadeadBaseUrl + "mfcwapp/getleads", jObj.toString());

    }

    public void webcallforprivateleadstore(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.stock_dealerURL + "dealer/private-leads/getlead", jObj.toString());
    }

    public void webcallforPrivateUpdateLeadStore(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.stock_dealerURL + "dealer/private-leads/getlead", jObj.toString());
    }

    public void webcallforupdatefollowlead(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.leadeadBaseUrl + "mfcwapp/leadupdate", jObj.toString());
    }

    public void webcallforUpdateFollowLeadForBooknow(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.leadeadBaseUrl + "mfcwapp/leadupdate", jObj.toString());
    }

    public void webcallforupdatefollowleadprivate(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.stock_dealerURL + "dealer/private-leads/update", jObj.toString());
    }

    public void webcallforUpdateFollowLeadPrivateForBookNow(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.stock_dealerURL + "dealer/private-leads/update", jObj.toString());
    }


    public void webcallProcfollowuphistory(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.leadeadBaseUrl + "mfcwapp/getleadhistory", jObj.toString());
    }

    public void webcallfollowuphistory(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.leadeadBaseUrl + "mfcwapp/getleadhistory", jObj.toString());
    }


    public void webcalladdLead(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.stock_dealerURL + "dealer/private-leads/create", jObj.toString());
    }

    public void webbooknowGetStock(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.stock_dealerURL + "dealer/stocks/store", jObj.toString());
    }

    public void webbookparseperticustock(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.stock_dealerURL + "dealer/stocks/store", jObj.toString());
    }

    public void webbookbooknowstock(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.stock_dealerURL + "dealer/stocks/update", jObj.toString());
    }

    public void webcallforupdatefollowleadbooknow(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.stock_dealerURL + "dealer/private-leads/get-followup", jObj.toString());
    }

    public void webcallforFollowUpHistoryPrivateLeads(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.stock_dealerURL + "dealer/private-leads/get-followup", jObj.toString());
    }

    public void webcallforleadsgetmodelvariant(JSONObject jObj, String str) {

        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.stock_dealerURL + "master/modeldetails/" + str, jObj.toString());
    }

    public void webcallforleadstorecount(JSONObject jObj, Activity activity) {
        WebServices parser = new WebServices(context, this, requestMethods);
        parser.execute(Global.leadeadBaseUrl + "mfcwapp/getleads", jObj.toString());
    }

    @Override
    public void onRequestCompleted(JSONObject object) {

        Log.e("onRequestCompleted", "onRequestCompleted=" + object.toString());
        String str = "";
        if (object != null) {

            if (!object.isNull("Message")) {

                try {
                    str = object.getString("Message");
                    //    Log.e("Message", "Message=" + str);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (str.equalsIgnoreCase("Unauthorized")) {
                    error_popup();
                } else {
                    JsonObjectParse.Parse(activity, context, object, strMethod);
                }

            } else {
                JsonObjectParse.Parse(activity, context, object, strMethod);
            }

        } else {

            CommonMethods.alertMessage(activity, GlobalText.NETWORK_ERROR);

        }
    }


    public static void error_popup() {

        if (activity != null) {
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            dialog.setContentView(R.layout.alert_error);
            TextView Message, Confirm;
            Message = dialog.findViewById(R.id.Message);
            Confirm = dialog.findViewById(R.id.ok_error);
            dialog.setCancelable(false);

            Message.setText(GlobalText.AUTH_ERROR);

            Confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                    MainActivity.logOut();
                }
            });

            dialog.show();
        }
    }

    public static void error_popup2(Activity activity, int str, String error) {
        if (activity != null) {
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            dialog.setContentView(R.layout.common_alert);
            TextView Message = dialog.findViewById(R.id.Message);
            Button Confirm = dialog.findViewById(R.id.Confirm);
            dialog.setCancelable(false);
            String strI = Integer.toString(str);
            String status = "";

            if (strI.equalsIgnoreCase("401")) {
                status = "401 Unauthorized Error.";
            } else if (strI.equalsIgnoreCase("400")) {
                status = "400 Bad Request Error.";
            } else if (strI.equalsIgnoreCase("500")) {
                status = "500 Internal Server Error.";
            } else if (strI.equalsIgnoreCase("404")) {
                status = "Not found 404 Error.";
            } else if (strI.equalsIgnoreCase("304")) {
                status = "Not Modified 304 Error.";
            } else if (strI.equalsIgnoreCase("503")) {
                status = "Gateway timeout 503 Error.";
            } else if (strI.equalsIgnoreCase("405")) {
                status = "405 Server Error(In Request Parameter)";
            }

            if (strI.equalsIgnoreCase("401")) {
                Message.setText(GlobalText.AUTH_ERROR + " \n" + " " + status);
            } else if (strI.equalsIgnoreCase("500")) {
                Message.setText(status);
            } else {
                Message.setText(error);
            }

            Confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (strI.equalsIgnoreCase("401")) {
                        CommonMethods.setvalueAgainstKey(activity, "appstatus", "0");
                        MainActivity.logOut();
                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                    }
                }
            });

            dialog.show();
        }
    }

    public static void error_popup_retrofit(int str, String error, Context mContext) {
        final Dialog dialog = new Dialog(mContext, R.style.Theme_AppCompat_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.common_alert);
        TextView Message = dialog.findViewById(R.id.Message);
        Button Confirm = dialog.findViewById(R.id.Confirm);
        dialog.setCancelable(false);

        if (str == 400) {
            Message.setText("400 Bad Request Error.");
        } else if (str == 401) {
            Message.setText("401 Unauthorized Error.");
        } else if (str == 500) {
            Message.setText("500 Internal Server Error.");
        } else if (str == 404) {
            Message.setText("Not found 404 Error.");
        } else if (str == 304) {
            Message.setText("Not Modified 304 Error.");
        } else if (str == 503) {
            Message.setText("Gateway timeout 503 Error.");
        } else if (str == 405) {
            Message.setText("405 Server Error(In Request Parameter)");
        } else {
            Message.setText(error);
        }

        Confirm.setOnClickListener(v -> {

            if (str == 401) {
                CommonMethods.setvalueAgainstKey(MainActivity.activity, "appstatus", "0");
                MainActivity.logOut();
                dialog.dismiss();
            } else {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
