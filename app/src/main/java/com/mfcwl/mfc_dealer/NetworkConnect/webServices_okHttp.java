
package com.mfcwl.mfc_dealer.NetworkConnect;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.WindowManager;

import com.mfcwl.mfc_dealer.Activity.LoginActivity;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Credentials;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.mfcwl.mfc_dealer.Activity.LoginActivity.activity;

/**
 * Created by HP 240 G4 on 09-03-2018.
 */

public class webServices_okHttp {

    public static String credential = "";

    public static final MediaType JSON = MediaType.parse("application/text; charset=utf-8");
    public static ProgressDialog pd;

    static Context con;
    String postBody;
    public static Request request;
    private static String TAG = webServices_okHttp.class.getSimpleName();

    //System.out.println("Printing Response Header...\n");

    public static void MethodGetCallDetails(final Activity act, String postUrl, final String postBody, final String postBody2, final String strMethod) {
        con = act;
        Log.i(TAG, "URL: " + postUrl);
        Log.i(TAG, "POST: " + postBody);
        Log.i(TAG, "POST: " + postBody2);
        // if (pd == null) {
        pd = createProgressDialog(con);
        // }

        if (pd != null) {
            Log.e("dialog.show", "dialog.show=2");

            pd.show();
        }
        /*if(pd!=null) {
            pd.show();
        }
*/

        OkHttpClient httpClient = new OkHttpClient
                .Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(180, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .authenticator((route, response) -> {
            credential = Credentials.basic(postBody, postBody2/*"prestige", "Mahindra"*/);

            if (responseCount(response) >= 3) {
                return null;
            }

            Log.i(TAG, "authenticate=: " + CommonMethods.getstringvaluefromkey(act, "device_id").toString());
            Log.i(TAG, "authenticate=: " + CommonMethods.getstringvaluefromkey(act, "device_type").toString());
            Log.i(TAG, "authenticate=: " + CommonMethods.getstringvaluefromkey(act, "device_version").toString());


            Log.e(TAG, "Authorization " + response.request().newBuilder().header("Authorization", credential).build());
            return response.request().newBuilder()
                    .header("Authorization", credential)
                    .header("device_id", CommonMethods.getstringvaluefromkey(act, "device_id").toString())
                    .header("device_os", CommonMethods.getstringvaluefromkey(act, "device_type").toString())
                    .header("device_os_version", CommonMethods.getstringvaluefromkey(act, "device_version").toString())
                    .build();

        }).build();


        RequestBody body = RequestBody.create(JSON, credential);

        request = new Request.Builder()
                .url(postUrl)
                .post(body)
                .build();


        httpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                //response.getStatusLine().getStatusCode();
                Log.e(TAG, "onFailure" + e.toString());
                try {
                    if (pd != null && pd.isShowing()) {
                        pd.dismiss();
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                Log.e(TAG, "headers=" + response.headers().get("token"));
                LoginActivity.mToken = response.headers().get("token");
                CommonMethods.setvalueAgainstKey(activity, "token", response.headers().get("token"));

                Log.e(TAG, "onResponse" + response.code());
                try {
                    CommonMethods.setvalueAgainstKey(activity, "access_token_from_header", response.headers().get("Token"));
                }catch (Exception e){
                    e.printStackTrace();
                }
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
                // LoginActivity.avi.smoothToHide();
                MethodofParseJsonData(response.body().string().trim(), response.code());


            }


            private void MethodofParseJsonData(final String s, int code) {

                Log.e("enter -> ", "ParseJson " + s);

                act.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            if (code == 401) {

                                //WebServicesCall.error_popup3(401, act);
                                CommonMethods.alertMessage(act, GlobalText.USER_NAME_INVALID);
                            } else {

                                JSONObject jsonObje = new JSONObject(s);
                                okHttpResponse.Parse(act, con, jsonObje, strMethod);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });

            }


        });


    }

    public static ProgressDialog createProgressDialog(Context mContext) {
        ProgressDialog dialog = new ProgressDialog(mContext);
        try {

            Log.e("dialog.show", "dialog.show=1");
            dialog.show();

        } catch (WindowManager.BadTokenException e) {


        }
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.progressdialog);
        // dialog.setMessage(Message);
        return dialog;
    }

    private static int responseCount(Response response) {
        int result = 1;
        while ((response = response.priorResponse()) != null) {
            result++;
        }
        return result;
    }


}

