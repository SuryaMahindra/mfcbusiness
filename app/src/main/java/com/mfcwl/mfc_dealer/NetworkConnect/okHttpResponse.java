package com.mfcwl.mfc_dealer.NetworkConnect;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.mfcwl.mfc_dealer.Activity.LoginActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class okHttpResponse {

    Activity activity;
    Context context;

    public static void Parse(Activity activity, Context context,
                             JSONObject jObj, String strMethod) {

        Log.e("strMethod=", "strMethod=" + strMethod);
        Log.e("jObj=", "jObj=" + jObj);

        okHttpResponse webServicesCall = new okHttpResponse();
        webServicesCall.activity = activity;
        webServicesCall.context = context;

        try {

            if (strMethod.equals("Login")) {
                LoginActivity.ParseLogin(jObj, strMethod);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
