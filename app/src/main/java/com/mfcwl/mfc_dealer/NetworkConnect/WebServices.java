/*
 * Copyright (c) 2014 Mahindra First Choice Wheels and/or its affiliates. All rights reserved. Mahindra First Choice Wheels
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.mfcwl.mfc_dealer.NetworkConnect;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.WindowManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mfcwl.mfc_dealer.Activity.SplashActivity;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;


/**
 * @author Mahindra First Choice Wheels
 */

public class WebServices extends AsyncTask<String, Void, JSONObject> {

    public interface CallbackInterface {

        void onRequestCompleted(JSONObject object);

    }

    private static final String TAG = WebServices.class.getSimpleName();
    private static final int CONNECTION_TIMEOUT = 1000 * 50;
    private static final int WAIT_RESPONSE_TIMEOUT = 1000 * 80;

    private String result = "";
    public String errors = "";
    private JSONObject jsonObject = null;
    private CallbackInterface mCallback;
    Context mContext;
    ProgressDialog pd;
    private int requestMethods;
    Activity activity;

    public WebServices(Context context, CallbackInterface callback, int requestMethod) {
        mContext = context;
        mCallback = callback;
        requestMethods = requestMethod;
    }

    @Override
    protected void onPreExecute() {

        /*pd = new ProgressDialog(mContext);
        pd.setTitle("Loading...");
        pd.setMessage("Please Wait...");
        pd.setCancelable(false);*/


        if (pd == null) {
            pd = createProgressDialog(mContext);
        }

        try {
            if (SplashActivity.progress && pd != null) {
                pd.show();
            }
        } catch (WindowManager.BadTokenException e) {
            Log.i(TAG, "onPreExecute: "+e.getMessage());
        }

        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        // TODO Auto-generated method stub
        super.onProgressUpdate(values);

    }

    protected JSONObject doInBackground(String... params) {

        String strURL = params[0];
        String strParams = params[1];

        Log.i(TAG, "doInBackground: " + strURL);
        Log.i(TAG, "doInBackground: " + strParams);
        JSONObject jObject = new JSONObject();
        try {

            jObject = new JSONObject(strParams);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return getJSONFromURL(strURL, jObject);
    }

    @Override
    protected void onPostExecute(JSONObject result) {
        super.onPostExecute(result);

        Log.i(TAG, "onPostExecute: " + result.toString());

        if (pd != null && pd.isShowing()) {
            pd.dismiss();
        }
        mCallback.onRequestCompleted(result);
        // Log.e(" check the response here ---------------->>>> ", result.toString());
    }

    private JSONObject getJSONFromURL(String strURL, JSONObject jObj) {
        try {

            // Log.e("URL is -->> ", strURL);
            //Log.e("data sending is -->> ", jObj.toString());
            // Log.e("requestMethods ", "requestMethods " + requestMethods);
            RequestQueue queue = Volley.newRequestQueue(mContext);

            final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                    requestMethods, strURL, jObj,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            jsonObject = response;
                          try {
                              if (pd != null && pd.isShowing()) {
                                  pd.dismiss();
                              }
                          }catch (Exception e){
                              e.printStackTrace();
                          }
                            //response.getStatusLine().getStatusCode();
                            //  Log.e(" check the response here---------------->>>> ", jsonObject.toString());
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    // As of f605da3 the following should work
                    NetworkResponse response = error.networkResponse;
                    if (error instanceof ServerError && response != null) {
                        try {
                            String res = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                            // Now you can use any deserializer to make sense of data
                            JSONObject obj = new JSONObject(res);
                            jsonObject = obj;

                            if (!obj.isNull("Message")) {

                                Log.e("jsonObject", "jsonObject=" + obj.getString("Message"));
                                errors = obj.getString("Message").toString();

                            } else {
                                errors = "Server error.";
                            }

                        } catch (UnsupportedEncodingException e1) {
                            // Couldn't properly decode data to string
                            e1.printStackTrace();
                        } catch (JSONException e2) {
                            // returned data is not JSONObject?
                            e2.printStackTrace();
                        }
                    }

                    if (pd != null && pd.isShowing()) {
                        pd.dismiss();

                    }

                    try {
                        activity = (Activity) mContext;
                        WebServicesCall.error_popup2(activity, error.networkResponse.statusCode, errors);
                    } catch (Exception e) {
                        //CommonMethods.alertMessage("Error"+ e);
                    }

                }

            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Accept", "application/json; charset=utf-8");
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));
                    Log.e("token ", "stock-token= " + CommonMethods.getstringvaluefromkey(activity, "token").toString());

                    return headers;
                }
            };

            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(jsObjRequest);
            int i = 0, n = 1;
            do {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Log.d(" OS", "OS");
                }

                if (jsonObject != null) {
                    n = 0;
                }
            } while (i != n);
            return jsonObject;

        } catch (Exception ex) {

            return jsonObject;

        }

    }

    public static ProgressDialog createProgressDialog(Context mContext) {
        ProgressDialog dialog = new ProgressDialog(mContext);
        try {
            if (SplashActivity.progress && dialog != null && !dialog.isShowing()) {
                dialog.show();
            }
        } catch (WindowManager.BadTokenException e) {
            Log.e("BadTokenException", "ProgressDialog=" + e.toString());

        }
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.progressdialog);
        // dialog.setMessage(Message);
        return dialog;
    }

}