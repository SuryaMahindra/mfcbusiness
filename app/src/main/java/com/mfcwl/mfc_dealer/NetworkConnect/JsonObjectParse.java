package com.mfcwl.mfc_dealer.NetworkConnect;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;
import android.util.Log;

import com.mfcwl.mfc_dealer.Activity.AddLeadActivity;
import com.mfcwl.mfc_dealer.Activity.AddStockActivity;
import com.mfcwl.mfc_dealer.Activity.EditStockActivity;
import com.mfcwl.mfc_dealer.Activity.Leads.LeadDetailsActivity;
import com.mfcwl.mfc_dealer.Activity.LoginActivity;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Activity.NotificationListActivity;
import com.mfcwl.mfc_dealer.Activity.SplashActivity;
import com.mfcwl.mfc_dealer.Activity.StockFeaturedCarActivity;
import com.mfcwl.mfc_dealer.Activity.StockStoreInfoActivity;
import com.mfcwl.mfc_dealer.Adapter.StockFeatureMarkAdapter;
import com.mfcwl.mfc_dealer.Adapter.ToolsMarketTrandStockAdapter;
import com.mfcwl.mfc_dealer.Fragment.BookedStockFrag;
import com.mfcwl.mfc_dealer.Fragment.HomeFragment;
import com.mfcwl.mfc_dealer.Fragment.Leads.FollowupHistoryFragment;
import com.mfcwl.mfc_dealer.Fragment.Leads.NormalLeadsDetailsFragment;
import com.mfcwl.mfc_dealer.Fragment.Leads.NormalLeadsFragment;
import com.mfcwl.mfc_dealer.Fragment.Leads.PrivateLeadsFragment;
import com.mfcwl.mfc_dealer.Fragment.StockStoreFrag;
import com.mfcwl.mfc_dealer.Fragment.ToolFragment;
import com.mfcwl.mfc_dealer.Fragment.ToolIbbPriceFrag;
import com.mfcwl.mfc_dealer.Procurement.Activity.ProcConvertStock;
import com.mfcwl.mfc_dealer.Procurement.Fragment.ProcFollowUpHistory;
import com.mfcwl.mfc_dealer.Procurement.Fragment.ProcurementAboutLead;
import com.mfcwl.mfc_dealer.SalesStocks.Activity.SalesDetailsActivity;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import org.json.JSONObject;


public class JsonObjectParse {

    Activity activity;
    Context context;

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static void Parse(Activity activity, Context context,
                             JSONObject jObj, String strMethod) {
        Log.e("ResonseAsync", jObj.toString());
        Log.e("strMethod:=", "strMethod:=" + strMethod);

        JsonObjectParse webServicesCall = new JsonObjectParse();

        webServicesCall.activity = activity;
        webServicesCall.context = context;

        try {

            if (strMethod.equals("Login")) {
                LoginActivity.ParseLogin(jObj, strMethod);
            } else if (strMethod.equals("AppVersion")) {
                LoginActivity.ParseAppVersion(jObj, strMethod);
            } else if (strMethod.equals("Logout")) {
                MainActivity.ParseLogout(jObj, strMethod);
            } else if (strMethod.equals("StockStore")) {
                //StockStoreFrag.Parsestockstore(jObj, strMethod);
            } else if (strMethod.equals("Notification_2")) {
                NotificationListActivity.ParseNotification(jObj, strMethod);
            } else if (strMethod.equals("ToolsStockStore")) {
                ToolFragment.Parsetoolsstockstore(jObj, strMethod);
            } else if (strMethod.equals("ToolsStockDetails")) {
                ToolsMarketTrandStockAdapter.Parsetoolsstockdetails(jObj, strMethod);
            } else if (strMethod.equals("BookedStock")) {
                BookedStockFrag.Parsestockstore(jObj, strMethod);
            } else if (strMethod.equals("BoookNow")) {
                StockStoreInfoActivity.Parsebooknow(jObj, strMethod);
            } else if (strMethod.equals("EditStock")) {
                EditStockActivity.Parseeditstock(jObj, strMethod);
            } else if (strMethod.equals("FeatureCar")) {
                StockFeaturedCarActivity.Parsebooknow(jObj, strMethod);
            } else if (strMethod.equals("FeaturedCarlist")) {
                StockFeaturedCarActivity.Parsestockstore(jObj, strMethod);
            } else if (strMethod.equals("GetCity")) {
                AddStockActivity.Parsegetcity(jObj, strMethod);
            } else if (strMethod.equals("EditStockGetCity")) {
                EditStockActivity.Parsegetcity(jObj, strMethod);
            } else if (strMethod.equals("GetMake")) {
                AddStockActivity.Parsegetmake(jObj, strMethod, activity);
            } else if (strMethod.equals("EditStockGetMake")) {
                EditStockActivity.Parsegetmake(jObj, strMethod, activity);
            } else if (strMethod.equals("GetModelVariant")) {
                AddStockActivity.Parsegetmodalvariant(jObj, strMethod, activity);
            } else if (strMethod.equals("ProGetModelVariant")) {
                ProcConvertStock.Parsegetmodalvariant(jObj, strMethod, activity);
            } else if (strMethod.equals("EditStockGetModelVariant")) {
                EditStockActivity.Parsegetmodalvariant(jObj, strMethod, activity);
            } else if (strMethod.equals("AddRegularStock")) {
                AddStockActivity.Parseaddregularstock(jObj, strMethod, activity);
            } else if (strMethod.equals("PermissionDear")) {
                StockStoreInfoActivity.PermissionDear(jObj, strMethod, activity);
            } else if (strMethod.equals("deleteFeatureCar")) {
                StockFeatureMarkAdapter.PermissionDear(jObj, strMethod, activity);

            } else if (strMethod.equals("StockDetails")) {
                StockStoreInfoActivity.stockdetailspage(jObj, strMethod, activity);

            } else if (strMethod.equals("SalesDetails")) {
                SalesDetailsActivity.stockdetailspage(jObj, strMethod, activity);

            } else if (strMethod.equals("dashBroad")) {
                HomeFragment.ParseDashBroad(jObj, strMethod, activity);
            }
            //IBB
            else if (strMethod.equals("IBBAccessToken")) {
                MainActivity.ParseIBBAccessToken(jObj, activity);
            } else if (strMethod.equals("IBByear")) {
                ToolIbbPriceFrag.ParseIBByear(jObj, activity);
            } else if (strMethod.equals("IBBmonth")) {
                ToolIbbPriceFrag.ParseIBBmonth(jObj, activity);
            } else if (strMethod.equals("IBBmake")) {
                ToolIbbPriceFrag.ParseIBBmake(jObj, activity);
            } else if (strMethod.equals("IBBmodel")) {
                ToolIbbPriceFrag.ParseIBBmodel(jObj, activity);
            } else if (strMethod.equals("IBBvariant")) {
                ToolIbbPriceFrag.ParseIBBvariant(jObj, activity);
            } else if (strMethod.equals("IBBowner")) {
                ToolIbbPriceFrag.ParseIBBowner(jObj, activity);
            } else if (strMethod.equals("IBBcolor")) {
                ToolIbbPriceFrag.ParseIBBcolor(jObj, activity);
            } else if (strMethod.equals("IBBcity")) {
                ToolIbbPriceFrag.ParseIBBcity(jObj, activity);
            } else if (strMethod.equals("IBBcheckPrice")) {
                ToolIbbPriceFrag.ParseIBBcheckPrice(jObj, activity);
            }

            //Lead
            else if (strMethod.equals("LeadAccessToken")) {
                LoginActivity.ParseLeadAccessToken(jObj, activity);
            } else if(strMethod.equals("LeadToken")){
                SplashActivity.ParseLeadToken(jObj, activity);
            }else if (strMethod.equals("LeadsGetModelVariant")) {
                AddLeadActivity.Parsegetmodalvariant(jObj, activity);
            } else if (strMethod.equals("LeadStore")) {
                NormalLeadsFragment.Parseleadstore(jObj, activity);
            } else if (strMethod.equals("WebLeadStore")) {
                Log.e("WebLeadStore ", "is coming");
                //LeadDetailsActivity.Parsewebleadstore(jObj,activity);
                NormalLeadsDetailsFragment.Parsewebleadstore(jObj, activity);
            } else if (strMethod.equals("PrivateLeadStore")) {
                PrivateLeadsFragment.Parseprivateleadstore(jObj, activity);
            } else if (strMethod.equals("PrivateUpdateLeadStore")) {
                NormalLeadsDetailsFragment.ParsePrivateUpdateLeadStore(jObj, activity);
            } else if (strMethod.equals("UpdateFollowLead")) {
                LeadDetailsActivity.Parseupdatefollowlead(jObj, activity);
            } else if (strMethod.equals("ProcUpdateFollowLead")) {
                ProcurementAboutLead.Parseupdatefollowlead(jObj, activity);
            } else if (strMethod.equals("FollowUpHistory")) {
                FollowupHistoryFragment.Parseupdatefollowuphistory(jObj, activity);
            } else if (strMethod.equals("ProcFollowUpHistory")) {
                ProcFollowUpHistory.Parseupdatefollowuphistory(jObj, activity);
            } /*else if (strMethod.equals("ProcUpdateFollowLead")) {
                ProcurementAboutLead.Parseupdatefollowlead(jObj, activity);
            }*/ else if (strMethod.equals("AddLead")) {
                AddLeadActivity.Parsecreateprivatelead(jObj, activity);
            } else if (strMethod.equals("BooknowGetStock")) {
                LeadDetailsActivity.Parsebooknowgetstock(jObj, activity);
            } else if (strMethod.equals("ParsePerticularStock")) {
                LeadDetailsActivity.ParsePerticularStock(jObj, activity);
            } else if (strMethod.equals("BooknowStock")) {
                LeadDetailsActivity.ParseBookNowStock(jObj, activity);
            } else if (strMethod.equals("UpdateFollowLeadBookNow")) {
                LeadDetailsActivity.ParseUpdateFollowupLeadBookNow(jObj, activity);
            } else if (strMethod.equals("FollowUpHistoryPrivateLeads")) {
                FollowupHistoryFragment.ParseUpdateFollowUpHistoryPrivateLeads(jObj, activity);
            } else if (strMethod.equals("UpdateFollowLeadPrivate")) {
                LeadDetailsActivity.ParseUpdateFollowLeadPrivate(jObj, activity);
            } else if (strMethod.equals("UpdateFollowLeadPrivateForBookNow")) {
                LeadDetailsActivity.ParseUpdateFollowLeadPrivateForBookNow(jObj, activity);
            } else if (strMethod.equals("UpdateFollowLeadForBooknow")) {
                LeadDetailsActivity.ParseUpdateFollowLeadForBooknow(jObj, activity);
            } else if (strMethod.equals("LeadStoreCount")) {
                HomeFragment.ParseleadstoreCount(jObj, activity);
            } else if(strMethod.equals("ParkAndSellDelete")) {
                StockStoreFrag.swipeRefresh_s_applyapply();
                activity.finish();
            }

        } catch (Exception e) {
            Log.e("Exception", "Exception=" + e);
            CommonMethods.alertMessage(activity, GlobalText.SERVER_ERROR);
        }

    }


}
