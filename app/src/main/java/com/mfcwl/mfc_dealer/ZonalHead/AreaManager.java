package com.mfcwl.mfc_dealer.ZonalHead;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mfcwl.mfc_dealer.ASMModel.dasboardRes;
import com.mfcwl.mfc_dealer.ResponseModel.Dealer;

import java.util.List;

public class AreaManager {

    @SerializedName("area_manager_id")
    @Expose
    private Integer areaManagerId;
    @SerializedName("area_manager_name")
    @Expose
    private String areaManagerName;
    @SerializedName("dealers")
    @Expose
    private List<dasboardRes> dealers = null;

    public Integer getAreaManagerId() {
        return areaManagerId;
    }

    public void setAreaManagerId(Integer areaManagerId) {
        this.areaManagerId = areaManagerId;
    }

    public String getAreaManagerName() {
        return areaManagerName;
    }

    public void setAreaManagerName(String areaManagerName) {
        this.areaManagerName = areaManagerName;
    }

    public List<dasboardRes> getDealers() {
        return dealers;
    }

    public void setDealers(List<dasboardRes> dealers) {
        this.dealers = dealers;
    }

}
