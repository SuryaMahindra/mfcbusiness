package com.mfcwl.mfc_dealer.StateHead;

import android.app.Activity;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.FragmentActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.ASMModel.dasboardRes;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Model.DashboardModels;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.mfcwl.mfc_dealer.Utility.GlobalText.HEADING;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.dealerTypeLablel;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.label4Wheeler;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.warrantyPunchLabel;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.warrentyBalLabel;

public class StateHeadDashboardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private Activity activity;
    private List<dasboardRes> mlivelist;
    private List<dasboardRes> mSearch;
    private DashboardModels dashboardSwtich;

    private boolean isLoadingAdded = false;

    public StateHeadDashboardAdapter(Context context) {
        activity = (Activity) context;
        mlivelist = new ArrayList<>();
        mSearch = new ArrayList<>();
        mSearch.addAll(mlivelist);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new StateHeadDashboardAdapter.LoadingVH(v2);
                break;
            default:
                return null;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.row_all_live_auction, parent, false);
        viewHolder = new MyLiveHolder(v1);

        dashboardSwtich = ViewModelProviders.of((FragmentActivity) activity).get(DashboardModels.class);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        dasboardRes data = mlivelist.get(position);
        try {

            switch (getItemViewType(position)) {
                case ITEM:

                    MyLiveHolder Aucholder = (MyLiveHolder) holder;
                    Aucholder.dealearName.setText(data.getDealerName());

                    if (data.getDealerCode().equals(HEADING)) {
                        Aucholder.card_view.setVisibility(View.GONE);
                        Aucholder.sectionHeading.setVisibility(View.VISIBLE);
                        Aucholder.sectionHeading.setText(data.getDealerName());
                        Aucholder.sectionHeading.setBackgroundColor(activity.getResources().getColor(R.color.grey));
                    } else {
                        Aucholder.card_view.setVisibility(View.VISIBLE);
                        Aucholder.sectionHeading.setVisibility(View.GONE);

                        if (data.getType() != null) {
                            if (data.getType().equalsIgnoreCase(label4Wheeler)) {
                                Aucholder.fwDealer_tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_f_wheeler, 0, 0, 0);
                            } else {
                                Aucholder.fwDealer_tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_t_wheeler, 0, 0, 0);
                            }

                            Aucholder.fwDealer_tv.setText(data.getType());
                        } else {
                            Aucholder.fwDealer_tv.setText("");
                        }

                        if (data.getDealerType() != null) {
                            Aucholder.dealer_type_tv.setText(String.format("%s%s", dealerTypeLablel, data.getDealerType()));
                        } else {
                            Aucholder.dealer_type_tv.setText(dealerTypeLablel);
                        }

                        if (data.getBalance() != null) {
                            Aucholder.warranty_bal.setText(String.format("%s%s", warrentyBalLabel, data.getBalance()));
                        } else {
                            Aucholder.warranty_bal.setText(String.format("%s0", warrentyBalLabel));
                        }

                        if (data.getWarrantypunch() != null) {
                            Aucholder.twsr_tv.setText(String.format("%s%s", warrantyPunchLabel, data.getWarrantypunch()));
                        } else {
                            Aucholder.twsr_tv.setText(String.format("%s0", warrantyPunchLabel));
                        }
                    }
                    break;

                case LOADING:
                    break;

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
    }

    @Override
    public int getItemCount() {
        return mlivelist == null ? 0 : mlivelist.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == mlivelist.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public void add(dasboardRes mc) {
        mlivelist.add(mc);
        notifyItemInserted(mlivelist.size() - 1);
    }

    public void addAll(List<dasboardRes> mcList) {
        for (dasboardRes mc : mcList) {
            add(mc);
        }

        mSearch.clear();
        mSearch.addAll(mlivelist);
    }


    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public dasboardRes getItem(int position) {
        position = position == -1 ? 0 : position;
        return mlivelist.get(position);
    }

    /**
     * Main list's content ViewHolder
     */
    protected class MyLiveHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView dealearName, dealer_type_tv, fwDealer_tv, warranty_bal, twsr_tv;
        TextView sectionHeading;
        LinearLayout mainLayout;
        CardView card_view;

        BottomNavigationView mNavigationView;

        MyLiveHolder(View itemView) {
            super(itemView);
            mainLayout = itemView.findViewById(R.id.mainLayout);
            sectionHeading = itemView.findViewById(R.id.sectionHeading);
            dealearName = itemView.findViewById(R.id.dealearName);
            dealer_type_tv = itemView.findViewById(R.id.dealer_type_tv);
            fwDealer_tv = itemView.findViewById(R.id.fwDealer_tv);
            warranty_bal = itemView.findViewById(R.id.warranty_bal);
            twsr_tv = itemView.findViewById(R.id.twsr_tv);

            card_view = itemView.findViewById(R.id.card_view);
            mNavigationView = itemView.findViewById(R.id.navigation);

            dealearName.setOnClickListener(this);
            card_view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            int position = getAdapterPosition();
            dasboardRes data = mlivelist.get(position);

            switch (view.getId()) {
                case R.id.card_view:

                    try {

                        MainActivity.searchClose();

                        CommonMethods.setvalueAgainstKey(activity, "status", "");


                        if (data.getWarrantypunch() != null) {
                            CommonMethods.setvalueAgainstKey(activity, "getWarrantypunch", data.getWarrantypunch().toString());
                        } else {
                            CommonMethods.setvalueAgainstKey(activity, "getWarrantypunch", "0");
                        }
                        if (data.getBalance() != null) {

                            //  String val =  BigDecimal(data.getBalance().toString()).stripTrailingZeros().toPlainString();

                            CommonMethods.setvalueAgainstKey(activity, "getBalance", String.valueOf(data.getBalance()));
                        } else {
                            CommonMethods.setvalueAgainstKey(activity, "getBalance", "0");
                        }

                        CommonMethods.setvalueAgainstKey(activity, "dealer_types", data.getType());
                        CommonMethods.setvalueAgainstKey(activity, "dealerName", data.getDealerName());

                        if (data.getType().equalsIgnoreCase("4 Wheeler")) {
                            MainActivity.navigation.getMenu().clear();
                            MainActivity.navigation.inflateMenu(R.menu.navigation);
                            MainActivity.navigation.setVisibility(View.VISIBLE);
                            CommonMethods.setvalueAgainstKey(activity, "dealer_code", data.getDealerCode());
                            dashboardSwtich.getdashboardSwtich().setValue("MessageCenter");
                        } else {
                            Toast.makeText(activity, "2 Wheeler Dealer  ", Toast.LENGTH_LONG).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;

                case R.id.dealearName:

                    try {

                        MainActivity.searchClose();

                        if (data.getWarrantypunch() != null) {
                            CommonMethods.setvalueAgainstKey(activity, "getWarrantypunch", data.getWarrantypunch().toString());
                        } else {
                            CommonMethods.setvalueAgainstKey(activity, "getWarrantypunch", "0");
                        }
                        if (data.getBalance() != null) {
                            CommonMethods.setvalueAgainstKey(activity, "getBalance", String.valueOf(data.getBalance()));
                        } else {
                            CommonMethods.setvalueAgainstKey(activity, "getBalance", "0");
                        }

                        CommonMethods.setvalueAgainstKey(activity, "dealer_types", data.getType());
                        CommonMethods.setvalueAgainstKey(activity, "dealerName", data.getDealerName());

                        if (data.getType().equalsIgnoreCase("4 Wheeler")) {
                            MainActivity.navigation.getMenu().clear();
                            MainActivity.navigation.inflateMenu(R.menu.navigation);
                            MainActivity.navigation.setVisibility(View.VISIBLE);
                            CommonMethods.setvalueAgainstKey(activity, "dealer_code", data.getDealerCode());
                            dashboardSwtich.getdashboardSwtich().setValue("MessageCenter");
                        } else {
                            Toast.makeText(activity, "2 Wheeler Dealer  ", Toast.LENGTH_LONG).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;

            }
        }
    }

    public void remove(dasboardRes data) {
        int position = mlivelist.indexOf(data);
        if (position > -1) {
            mlivelist.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    private class LoadingVH extends RecyclerView.ViewHolder {

        private LoadingVH(View itemView) {
            super(itemView);
        }

    }


    // Search Filter
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        mlivelist.clear();
        if (charText.length() == 0) {
            mlivelist.addAll(mSearch);
        } else {

            for (dasboardRes it : mSearch) {
                if (it.getDealerName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    mlivelist.add(it);
                }
            }
        }
        notifyDataSetChanged();
    }


}