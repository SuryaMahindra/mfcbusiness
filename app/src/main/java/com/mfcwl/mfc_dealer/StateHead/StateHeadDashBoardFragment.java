package com.mfcwl.mfc_dealer.StateHead;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.mfcwl.mfc_dealer.ASMModel.dasboardRes;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.Helper;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.ZonalHead.AreaManager;
import com.mfcwl.mfc_dealer.retrofitconfig.DasboardStatusServices;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchVal;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.test;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.HEADING;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.stateHeadDashboard;

public class StateHeadDashBoardFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, TextWatcher, View.OnClickListener {

    String TAG = getClass().getSimpleName();
    Unbinder unbinder;

    @BindView(R.id.recylerView)
    RecyclerView recyclerView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.ivClearSearch)
    ImageView ivClearSearch;

    static StateHeadDashboardAdapter adapter;

    Context c;
    Activity a;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.state_head_fragment, container, false);

        setHasOptionsMenu(true);

        recyclerView = view.findViewById(R.id.recylerView);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        etSearch = view.findViewById(R.id.etSearch);
        ivClearSearch = view.findViewById(R.id.ivClearSearch);


        unbinder = ButterKnife.bind(this, view);

        initView();

        return view;
    }

    private void initView() {

        a = getActivity();
        c = getContext();

        CommonMethods.setvalueAgainstKey(a, "status", "statedashboard");
        MainActivity.setMainHeading(stateHeadDashboard);
        test("Search..");

        swipeRefreshLayout.setOnRefreshListener(this);
        etSearch.addTextChangedListener(this);
        ivClearSearch.setOnClickListener(this);

        adapter = new StateHeadDashboardAdapter(getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        dashboardReq();
        MainActivity.navigation.setVisibility(View.GONE);
    }

    private void dashboardReq() {

        SpinnerManager.showSpinner(getActivity());

        DasboardStatusServices.getLeadStatusStateHead(getActivity(), new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                Log.i(TAG, "OnSuccess: ");
                SpinnerManager.hideSpinner(getActivity());
                Response<List<AreaManager>> mResponse = (Response<List<AreaManager>>) obj;

                List<AreaManager> areaManagerList = mResponse.body();
                List<dasboardRes> dealerItems = new ArrayList<>();

                for (AreaManager areaManager : areaManagerList) {
                    dasboardRes dealer = new dasboardRes();
                    dealer.setDealerCode(HEADING);
                    dealer.setDealerName(areaManager.getAreaManagerName());
                    dealerItems.add(dealer);
                    dealerItems.addAll(areaManager.getDealers());
                }
                Log.i(TAG, "OnSuccess: " + areaManagerList.size());
                Log.i(TAG, "OnSuccess: " + dealerItems.size());

                adapter.clear();

                Helper.dealerName.clear();
                Helper.dealerCode.clear();

                if (!areaManagerList.isEmpty()) {
                    adapter.addAll(dealerItems);

                    for (int i = 0; i <= dealerItems.size() - 1; i++) {
                        Helper.dealerName.add(i, dealerItems.get(i).getDealerName());
                        Helper.dealerCode.add(i, dealerItems.get(i).getDealerCode());
                    }

                }


            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                Log.i(TAG, "OnFailure: ");
                SpinnerManager.hideSpinner(getActivity());
            }


        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        unbinder.unbind();
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        MainActivity.setMainHeading(stateHeadDashboard);
        dashboardReq();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onResume() {

        MainActivity.setMainHeading(stateHeadDashboard);
        super.onResume();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        Log.i(TAG, "beforeTextChanged: " + s + " " + start + " " + count + " " + after);
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        Log.i(TAG, "onTextChanged: " + s + " " + start + " " + before + " " + count);
    }

    @Override
    public void afterTextChanged(Editable s) {
        Log.i(TAG, "afterTextChanged: " + s);
        adapter.filter(s.toString());
    }

    @Override
    public void onClick(View v) {
        etSearch.setText("");
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);


        MenuItem asm_mail = menu.findItem(R.id.asm_mail);
        MenuItem asmHome = menu.findItem(R.id.asmHome);
        MenuItem add_image = menu.findItem(R.id.add_image);
        MenuItem share = menu.findItem(R.id.share);
        MenuItem delete_fea = menu.findItem(R.id.delete_fea);
        MenuItem stock_fil_clear = menu.findItem(R.id.stock_fil_clear);
        MenuItem notification_bell = menu.findItem(R.id.notification_bell);
        MenuItem notification_cancel = menu.findItem(R.id.notification_cancel);

        asm_mail.setVisible(false);
        asmHome.setVisible(false);
        add_image.setVisible(false);
        share.setVisible(false);
        delete_fea.setVisible(false);
        stock_fil_clear.setVisible(false);
        notification_bell.setVisible(false);
        notification_cancel.setVisible(false);

    }

    public static void onSerarchResultUpdate(String query) {
        searchVal = query;

        if (adapter != null) {
            adapter.filter(query);
        }

    }
}
