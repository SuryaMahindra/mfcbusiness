package com.mfcwl.mfc_dealer.StateHead;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mfcwl.mfc_dealer.ZonalHead.AreaManager;

import java.util.List;

public class StateManager {

    @SerializedName("state_manager_id")
    @Expose
    private Integer stateManagerId;
    @SerializedName("state_manager_name")
    @Expose
    private String stateManagerName;
    @SerializedName("area_managers")
    @Expose
    private List<AreaManager> areaManagers = null;

    public Integer getStateManagerId() {
        return stateManagerId;
    }

    public void setStateManagerId(Integer stateManagerId) {
        this.stateManagerId = stateManagerId;
    }

    public String getStateManagerName() {
        return stateManagerName;
    }

    public void setStateManagerName(String stateManagerName) {
        this.stateManagerName = stateManagerName;
    }

    public List<AreaManager> getAreaManagers() {
        return areaManagers;
    }

    public void setAreaManagers(List<AreaManager> areaManagers) {
        this.areaManagers = areaManagers;
    }

}
