package com.mfcwl.mfc_dealer.RetrofitConfigLeadSection;

import com.mfcwl.mfc_dealer.Model.LeadstatusResponse;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PPLResponse;
import com.mfcwl.mfc_dealer.Procurement.RetrofitConfig.ProcPrivateLeadService;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.retrofitconfig.LeadStatusBaseService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public class LeadStatusServices extends LeadStatusBaseService {
    public static void getLeadStatus(final HttpCallResponse mHttpCallResponse) {
        LeadStatusServices.ApiInterface mInterFace = retrofit.create(LeadStatusServices.ApiInterface.class);
        Call<LeadstatusResponse> mCall = mInterFace.getLeadStatus();
        mCall.enqueue(new Callback<LeadstatusResponse>() {
            @Override
            public void onResponse(Call<LeadstatusResponse> call, Response<LeadstatusResponse> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<LeadstatusResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });


    }

    public interface ApiInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @GET("get-allleadstatus")
        Call<LeadstatusResponse> getLeadStatus();
    }
}
