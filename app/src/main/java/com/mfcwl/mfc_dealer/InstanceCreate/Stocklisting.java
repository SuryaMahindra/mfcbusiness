package com.mfcwl.mfc_dealer.InstanceCreate;

public class Stocklisting {

    private static Stocklisting mInstance = null;
    private String stockstatus;


    private Stocklisting(){
        stockstatus = "";

    }

    public static Stocklisting getInstance(){
        if(mInstance == null)
        {
            mInstance = new Stocklisting();
        }
        return mInstance;
    }

    public String getStockstatus() {
        return stockstatus;
    }

    public void setStockstatus(String stockstatus) {
        this.stockstatus = stockstatus;
    }
}
