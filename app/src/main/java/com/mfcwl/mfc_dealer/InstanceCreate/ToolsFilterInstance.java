package com.mfcwl.mfc_dealer.InstanceCreate;

public class ToolsFilterInstance {

    private static ToolsFilterInstance mInstance = null;
    private String toolsfilterby;


    private ToolsFilterInstance(){
        toolsfilterby = "";

    }

    public static ToolsFilterInstance getInstance(){
        if(mInstance == null)
        {
            mInstance = new ToolsFilterInstance();
        }
        return mInstance;
    }

    public String getToolsfilterby() {
        return toolsfilterby;
    }

    public void setToolsfilterby(String toolsfilterby) {
        this.toolsfilterby = toolsfilterby;
    }
}
