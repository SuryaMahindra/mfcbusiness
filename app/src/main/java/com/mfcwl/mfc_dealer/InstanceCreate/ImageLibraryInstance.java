package com.mfcwl.mfc_dealer.InstanceCreate;

public class ImageLibraryInstance {

    private static ImageLibraryInstance mInstance = null;
    private String imagepath;
    private String imagestatus;



    private String uploadstatus;


    private ImageLibraryInstance(){
        imagepath = "";
        imagestatus = "";
        uploadstatus = "";

    }

    public static ImageLibraryInstance getInstance(){
        if(mInstance == null)
        {
            mInstance = new ImageLibraryInstance();
        }
        return mInstance;
    }

    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    public String getImagestatus() {
        return imagestatus;
    }

    public void setImagestatus(String imagestatus) {
        this.imagestatus = imagestatus;

    }
    public String getUploadstatus() {
        return uploadstatus;
    }

    public void setUploadstatus(String uploadstatus) {
        this.uploadstatus = uploadstatus;
    }
}
