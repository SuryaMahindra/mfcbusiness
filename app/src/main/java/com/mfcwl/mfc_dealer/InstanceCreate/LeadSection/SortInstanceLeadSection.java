package com.mfcwl.mfc_dealer.InstanceCreate.LeadSection;

public class SortInstanceLeadSection {

    private static SortInstanceLeadSection mInstance = null;
    private String whichsort;
    private String sortby;

    private SortInstanceLeadSection(){
        whichsort = "";
        sortby = "";

    }

    public static SortInstanceLeadSection getInstance(){
        if(mInstance == null)
        {
            mInstance = new SortInstanceLeadSection();
        }
        return mInstance;
    }

    public String getWhichsort() {
        return whichsort;
    }

    public void setWhichsort(String whichsort) {
        this.whichsort = whichsort;
    }

    public String getSortby() {
        return sortby;
    }

    public void setSortby(String sortby) {
        this.sortby = sortby;
    }
}
