package com.mfcwl.mfc_dealer.InstanceCreate.LeadSection;

import org.json.JSONArray;

import java.util.HashMap;

public class LeadFilterSaveInstance {

    private static LeadFilterSaveInstance mInstance = null;

    HashMap<String, String> savedatahashmap;
    JSONArray statusarray = null;

    private LeadFilterSaveInstance(){
        savedatahashmap = new HashMap<String, String>();
        statusarray = new JSONArray();
    }

    public static LeadFilterSaveInstance getInstance(){
        if(mInstance == null)
        {
            mInstance = new LeadFilterSaveInstance();
        }
        return mInstance;
    }

    public HashMap<String, String> getSavedatahashmap() {
        return savedatahashmap;
    }

    public void setSavedatahashmap(HashMap<String, String> savedatahashmap) {
        this.savedatahashmap = savedatahashmap;
    }

    public JSONArray getStatusarray() {
        return statusarray;
    }

    public void setStatusarray(JSONArray statusarray) {
        this.statusarray = statusarray;
    }
}
