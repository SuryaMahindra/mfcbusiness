package com.mfcwl.mfc_dealer.graphdata.interfaces.dataprovider;


import com.mfcwl.mfc_dealer.graphdata.data.BarData;

public interface BarDataProvider extends BarLineScatterCandleBubbleDataProvider {

    BarData getBarData();
    boolean isDrawBarShadowEnabled();
    boolean isDrawValueAboveBarEnabled();
    boolean isHighlightFullBarEnabled();
}
