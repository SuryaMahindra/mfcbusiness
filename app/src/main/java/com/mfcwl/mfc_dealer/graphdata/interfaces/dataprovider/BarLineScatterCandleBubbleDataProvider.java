package com.mfcwl.mfc_dealer.graphdata.interfaces.dataprovider;


import com.mfcwl.mfc_dealer.graphdata.components.YAxis;
import com.mfcwl.mfc_dealer.graphdata.data.BarLineScatterCandleBubbleData;
import com.mfcwl.mfc_dealer.graphdata.utils.Transformer;

public interface BarLineScatterCandleBubbleDataProvider extends ChartInterface {

    Transformer getTransformer(YAxis.AxisDependency axis);
    boolean isInverted(YAxis.AxisDependency axis);
    
    float getLowestVisibleX();
    float getHighestVisibleX();

    BarLineScatterCandleBubbleData getData();
}
