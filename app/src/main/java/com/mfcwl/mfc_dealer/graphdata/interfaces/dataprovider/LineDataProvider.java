package com.mfcwl.mfc_dealer.graphdata.interfaces.dataprovider;


import com.mfcwl.mfc_dealer.graphdata.components.YAxis;
import com.mfcwl.mfc_dealer.graphdata.data.LineData;

public interface LineDataProvider extends BarLineScatterCandleBubbleDataProvider {

    LineData getLineData();

    YAxis getAxis(YAxis.AxisDependency dependency);
}
