package com.mfcwl.mfc_dealer.graphdata.interfaces.dataprovider;


import com.mfcwl.mfc_dealer.graphdata.data.ScatterData;

public interface ScatterDataProvider extends BarLineScatterCandleBubbleDataProvider {

    ScatterData getScatterData();
}
