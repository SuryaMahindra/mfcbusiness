package com.mfcwl.mfc_dealer.DashBoardServices;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.DasBoardModels.DashBoardResponse;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PWLRequest;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.retrofitconfig.BaseService;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public class DashBoardService extends BaseService {

    public static void fetchDashBoardInfo(Context mContext, final HttpCallResponse mHttpCallResponse) {
        DashBoardInterface mInterface = retrofit.create(DashBoardInterface.class);

        Call<DashBoardResponse> mCall = mInterface.getDashBoardInfo();
        mCall.enqueue(new Callback<DashBoardResponse>() {
            @Override
            public void onResponse(@NotNull Call<DashBoardResponse> call, @NotNull Response<DashBoardResponse> response) {

                Log.i("HomeFragment", "URL: "+mCall.request().url().toString());

                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {
                    if (response.code() == 400) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 401) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 500) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 404) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 304) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 503) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 405) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<DashBoardResponse> call, @NotNull Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });
    }

    public interface DashBoardInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @GET("dashboard/dashboard")
        Call<DashBoardResponse> getDashBoardInfo();
    }
}
