package com.mfcwl.mfc_dealer.DashBoardServices;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.DasBoardModels.WebLeadRequest;
import com.mfcwl.mfc_dealer.DasBoardModels.WebLeadsResponse;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PWLRequest;
import com.mfcwl.mfc_dealer.RequestModel.WebLeadReq1;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.retrofitconfig.WebLeadBaseService;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class WebLeadsService extends WebLeadBaseService {

    public static void fetchWebLeads(Context mContext, final WebLeadReq1 mWebLeadRequest, final HttpCallResponse mHttpCallResponse) {
        WebLeadsInterface mLeadsInterface = retrofit.create(WebLeadsInterface.class);

        Call<WebLeadsResponse> mCall = mLeadsInterface.getWebLeads(mWebLeadRequest);
        mCall.enqueue(new Callback<WebLeadsResponse>() {
            @Override
            public void onResponse(@NotNull Call<WebLeadsResponse> call, @NotNull Response<WebLeadsResponse> response) {

                Log.i("HomeFragment", "URL "+mCall.request().url().toString());
                String str = new Gson().toJson(mWebLeadRequest, WebLeadReq1.class);
                Log.i("HomeFragment", "REQ "+str);

                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                }
            }

            @Override
            public void onFailure(Call<WebLeadsResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });

    }

    public interface WebLeadsInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("getleads")
        Call<WebLeadsResponse> getWebLeads(@Body WebLeadReq1 mRequest);
    }
}
