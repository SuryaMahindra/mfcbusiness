package com.mfcwl.mfc_dealer.Procurement.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Activity.SplashActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.Procurement.Adapter.ProcFollowUpHistoryAdapter;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PPFHFollowup;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PPFHRequest;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PPFHResponse;
import com.mfcwl.mfc_dealer.Procurement.RetrofitConfig.ProcPrivateLeadService;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

@SuppressLint("ValidFragment")
public class ProcFollowUpHistory extends Fragment {

    static RecyclerView rv_procFollowUpHistory;

    public String leadid;
    public String leadtype;

    private static List<PPFHFollowup> mList;

    private static ProcFollowUpHistoryAdapter madapter;
    String TAG = getClass().getSimpleName();
    public ProcFollowUpHistory(String leadid, String leadtype) {
        // Required empty public constructor
        this.leadid = leadid;
        this.leadtype = leadtype;

    }

    public static void Parseupdatefollowuphistory(JSONObject jObj, Activity activity) {
        //   Log.e("ParseUpdate ", "followuphistory " + jObj.toString());
        try {
            if (jObj.getString("status").equals("failure")) {
                WebServicesCall.error_popup2(activity, Integer.parseInt(jObj.getString("status_code")), "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ArrayList myJobList = new ArrayList<>();
        try {
            JSONArray arrayList = jObj.getJSONArray("followup_history");
            for (int i = 0; i < arrayList.length(); i++) {
                JSONObject jsonobject = new JSONObject(arrayList.getString(i));
                PPFHFollowup mppfhf = new PPFHFollowup();
                mppfhf.setCreatedOn(jsonobject.getString("created_at"));
                mppfhf.setLeadStatus(jsonobject.getString("followup_status"));
                mppfhf.setFollowUp(jsonobject.getString("followup_date"));
                mppfhf.setLeadRemark(jsonobject.getString("followup_comments"));
               /* FollowUpHistoryLeadModel historyLeadModel = new FollowUpHistoryLeadModel(jsonobject.getString("followup_date"),
                        jsonobject.getString("followup_status"),
                        jsonobject.getString("followup_comments"), jsonobject.getString("created_at"));*/
                myJobList.add(mppfhf);
            }
            attachtoAdapter(myJobList, activity);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.fragment_proc_follow_up_history, container, false);
        InitUI(view);
        return view;
    }

    private void InitUI(View view) {
        Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"), GlobalText.proc_followupHistory, GlobalText.android);
        rv_procFollowUpHistory = view.findViewById(R.id.rv_procFollowUpHistory);
        SplashActivity.progress = true;
        rv_procFollowUpHistory.setLayoutManager(new LinearLayoutManager(getContext()));
        rv_procFollowUpHistory.setHasFixedSize(true);
        DividerItemDecoration itemDecor = new DividerItemDecoration(getContext(), DividerItemDecoration.HORIZONTAL);
        rv_procFollowUpHistory.addItemDecoration(itemDecor);
        rv_procFollowUpHistory.setAdapter(madapter);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (CommonMethods.getstringvaluefromkey(MainActivity.activity, "PWLEADS").equals("WebLeads")) {
            WebServicesCall.webCall(MainActivity.activity, MainActivity.activity, jsonMake(), "ProcFollowUpHistory", GlobalText.POST);
        }
        if (CommonMethods.getstringvaluefromkey(MainActivity.activity, "PWLEADS").equals("PrivateLeads")) {
            FollowRequest();
        }
    }

  /*  @Override
    public void onResume() {
        super.onResume();

        if (CommonMethods.getstringvaluefromkey(getActivity(), "PWLEADS").equals("WebLeads")) {
            WebServicesCall.webCall(getActivity(), getContext(), jsonMake(), "ProcFollowUpHistory", GlobalText.POST);
        }
        if (CommonMethods.getstringvaluefromkey(getActivity(), "PWLEADS").equals("PrivateLeads")) {
            FollowRequest();
        }
    }*/

    public JSONObject jsonMake() {
        JSONObject jObj = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            jObj.put("access_token", CommonMethods.getstringvaluefromkey(getActivity(), "access_token"));
            jObj.put("dispatch_id", leadid);
            jObj.put("tag", "android");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        //  Log.e("Parseleadstore ", "jObj " + jObj.toString());
        return jObj;
    }


    private void FollowRequest() {
        PPFHRequest req = new PPFHRequest();
        req.setType("mfc");
        req.setLeadId(leadid);
        req.setLeadType(leadtype);
        FollowResponse(getContext(), req);
    }

    private void FollowResponse(Context context, PPFHRequest req) {
        SpinnerManager.showSpinner(context);
        ProcPrivateLeadService.PPFHLead(context, req, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(context);

                Response<PPFHResponse> mRes = (Response<PPFHResponse>) obj;
                PPFHResponse mData = mRes.body();
                List<PPFHFollowup> mlist = mData.getFollowUps();

                mList = new ArrayList<>();
                mList.addAll(mlist);
                attachtoAdapter(mList, (Activity) context);
            }

            @Override
            public void OnFailure(Throwable t) {
                SpinnerManager.hideSpinner(context);
                t.printStackTrace();
            }
        });
    }

    private static void attachtoAdapter(List<PPFHFollowup> mlist, Activity activity) {
        madapter = new ProcFollowUpHistoryAdapter(activity, mlist);
        rv_procFollowUpHistory.setHasFixedSize(true);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(activity);
        rv_procFollowUpHistory.setLayoutManager(mLinearLayoutManager);
        rv_procFollowUpHistory.setAdapter(madapter);
    }
}
