package com.mfcwl.mfc_dealer.Procurement.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Procurement.Activity.ProcurementDetails;
import com.mfcwl.mfc_dealer.Procurement.ProcInterface.PLazyloaderWebLeads;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PWLDatum;
import com.mfcwl.mfc_dealer.Procurement.Singleton.ConvertStockInstance;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.MonthUtility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProcurementWebLeadAdapter extends RecyclerView.Adapter<ProcurementWebLeadAdapter.MyViewHolder> implements View.OnClickListener {

    private List<PWLDatum> procList;
    LayoutInflater layoutInflater;
    public Context mContext;

    private List<PWLDatum> itemListSearch;
    private PLazyloaderWebLeads lazyloaderWebLeads;
    Activity a;
    int i;

    public ProcurementWebLeadAdapter(Context mContext, Activity a, List<PWLDatum> procList) {
        this.mContext = mContext;
        this.procList = procList;
        this.a = a;
        layoutInflater = LayoutInflater.from(mContext);
        itemListSearch = new ArrayList<PWLDatum>();
        itemListSearch.addAll(procList);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView proc_leadName, proc_leadNum, proc_leadCompName, proc_leadCarModel, proc_leadDate, proc_leadDays,
                proc_leadFollowupHistory;
        public ImageView proc_leadWhatsapp, proc_leadMessege, proc_leadCall;

        public MyViewHolder(View view) {
            super(view);
            proc_leadName = (TextView) view.findViewById(R.id.proc_leadName);
            proc_leadNum = (TextView) view.findViewById(R.id.proc_leadNum);
            proc_leadCompName = (TextView) view.findViewById(R.id.proc_leadCompName);
            proc_leadCarModel = (TextView) view.findViewById(R.id.proc_leadCarModel);
            proc_leadDate = (TextView) view.findViewById(R.id.proc_leadDate);
            proc_leadDays = (TextView) view.findViewById(R.id.proc_leadDays);
            proc_leadFollowupHistory = (TextView) view.findViewById(R.id.proc_leadFollowupHistory);
            proc_leadWhatsapp = (ImageView) view.findViewById(R.id.proc_leadWhatsapp);
            proc_leadMessege = (ImageView) view.findViewById(R.id.proc_leadMessege);
            proc_leadCall = (ImageView) view.findViewById(R.id.proc_leadCall);


            if (CommonMethods.getstringvaluefromkey(a, "user_type").equalsIgnoreCase("dealer")) {

            }else{
                proc_leadWhatsapp.setVisibility(View.INVISIBLE);
                proc_leadMessege.setVisibility(View.INVISIBLE);
                proc_leadCall .setVisibility(View.INVISIBLE);

            }

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        lazyloaderWebLeads = (PLazyloaderWebLeads) a;
        View itemView = layoutInflater.from(parent.getContext())
                .inflate(R.layout.row_proc_weblead, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.proc_leadWhatsapp:
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(a, "dealer_code"), GlobalText.proc_web_whatsapp, GlobalText.android);
                openWhatsApp(procList.get(i).getMobile().trim());
                break;
            case R.id.proc_leadMessege:
              Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(a, "dealer_code"), GlobalText.proc_web_message, GlobalText.android);
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:" + procList.get(i).getMobile().trim()));
                sendIntent.putExtra("sms_body", "");
                mContext.startActivity(sendIntent);
                break;
            case R.id.proc_leadCall:
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(a, "dealer_code"), GlobalText.proc_web_call, GlobalText.android);
                Intent callIntent = new Intent(Intent.ACTION_VIEW);
                callIntent.setData(Uri.parse("tel:" + procList.get(i).getMobile().trim()));//change the number
                mContext.startActivity(callIntent);
                break;
        }
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        i = position;
        holder.proc_leadWhatsapp.setOnClickListener(this);
        holder.proc_leadMessege.setOnClickListener(this);
        holder.proc_leadCall.setOnClickListener(this);

        try {
            if (position >= getItemCount() - 1 && getItemCount() > 0 || procList.size() <= 3) {
                lazyloaderWebLeads.Ploadmore(getItemCount());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        final PWLDatum mWebLeadsDatum = procList.get(position);

        //ValidateDate
        String createdDate = mWebLeadsDatum.getLeadDate();
        String[] splitDateTime = createdDate.split(" ", 10);
        String[] splitDate = splitDateTime[0].split("-", 10);
        String finalDate = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");
        Calendar c = Calendar.getInstance();
        String formattedDate = simpleDateFormat.format(c.getTime());
        String[] splitSpace = formattedDate.split(" ", 10);

        try {
            Date date1 = simpleDateFormat.parse(finalDate + " 00:00:00");
            Date date2 = simpleDateFormat.parse(splitSpace[0] + " 00:00:00");

            if (printDifference(date1, date2) == 0) {
                holder.proc_leadDays.setText("Today");
            } else if (printDifference(date1, date2) == 1) {
                holder.proc_leadDays.setText("Yesterday");
            } else if (printDifference(date1, date2) > 364) {
                holder.proc_leadDays.setText("" + printDifference(date1, date2) / 365 + " Y ago");
            } else {
                holder.proc_leadDays.setText("" + printDifference(date1, date2) + " D ago");
            }

        } catch (ParseException e) {
            //  e.printStackTrace();
            Log.e("ParseException ", "Normal " + e.toString());
        }

        //Validate Follow up Date
        try {
            String[] splitdatespace = mWebLeadsDatum.getFollowUpDate().split(" ", 10);
            String[] splitdate1 = splitdatespace[0].split("-", 10);
            holder.proc_leadDate.setText(splitdate1[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate1[1])] + " " + splitdate1[0]);
        } catch (Exception e) {

            String leadDate = ((mWebLeadsDatum.getFollowUpDate() == null || mWebLeadsDatum.getFollowUpDate().trim().equalsIgnoreCase("NA") || mWebLeadsDatum.getFollowUpDate().trim().equalsIgnoreCase("")) ? "NA" : mWebLeadsDatum.getFollowUpDate().trim());
            holder.proc_leadDate.setText(leadDate);

        }

        //Update Lead Details
        String leadName = ((mWebLeadsDatum.getName().trim() == null || mWebLeadsDatum.getName().trim().equalsIgnoreCase("NA") || mWebLeadsDatum.getName().trim().equalsIgnoreCase("")) ? "Unknown" : mWebLeadsDatum.getName().trim());
        holder.proc_leadName.setText(capitalize(leadName));
        String leadMobNo = ((mWebLeadsDatum.getMobile().trim() == null || mWebLeadsDatum.getMobile().trim().equalsIgnoreCase("NA") || mWebLeadsDatum.getMobile().trim().equalsIgnoreCase("")) ? "Unknown" : mWebLeadsDatum.getMobile().trim());
        holder.proc_leadNum.setText(leadMobNo);
        String leadCompName = ((mWebLeadsDatum.getPrimaryMake().trim() == null || mWebLeadsDatum.getPrimaryMake().trim().equalsIgnoreCase("NA") || mWebLeadsDatum.getPrimaryMake().trim().equalsIgnoreCase("")) ? "NA" : mWebLeadsDatum.getPrimaryMake().trim());
        holder.proc_leadCompName.setText(leadCompName);
        String leadModVar = ((mWebLeadsDatum.getPrimaryModel().trim() == null || mWebLeadsDatum.getPrimaryModel().trim().equalsIgnoreCase("NA") || mWebLeadsDatum.getPrimaryModel().trim().equalsIgnoreCase("") || mWebLeadsDatum.getPrimaryVariant().trim() == null || mWebLeadsDatum.getPrimaryVariant().trim().equalsIgnoreCase("NA") || mWebLeadsDatum.getPrimaryVariant().trim().equalsIgnoreCase("")) ? "NA" : mWebLeadsDatum.getPrimaryModel().trim() + " " + mWebLeadsDatum.getPrimaryVariant().trim());
        holder.proc_leadCarModel.setText(leadModVar);

        //Update Lead Status
        holder.proc_leadFollowupHistory.setVisibility(View.VISIBLE);

        try {
            holder.proc_leadFollowupHistory.setText(capitalize(mWebLeadsDatum.getStatus().trim()));
        } catch (Exception e) {
            holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.open_28);
            holder.proc_leadFollowupHistory.setText(capitalize("Open"));
        }

        try {
            if (mWebLeadsDatum.getStatus().equalsIgnoreCase("Open")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.open_28);
                holder.proc_leadFollowupHistory.setText("  " + capitalize(mWebLeadsDatum.getStatus().trim()));
            }
            else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("Outstation")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.ic_outstation);
                holder.proc_leadFollowupHistory.setText("  " + capitalize(mWebLeadsDatum.getStatus().trim()));
            }else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("") || mWebLeadsDatum.getStatus().equalsIgnoreCase("null")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.open_28);
                holder.proc_leadFollowupHistory.setText(" Open");
            } else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("Hot")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.hot_28);
            } else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("Warm")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.warm_28);
                holder.proc_leadFollowupHistory.setText("  " + capitalize(mWebLeadsDatum.getStatus().trim()));
            } else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("Cold")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.cold_28);
            } else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("Lost")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.lost_28);
            } else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("Sold")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.sold_28);
            } else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("Bought")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.sold_28);
            } else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("Visiting")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.ic_visiting);
            } else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("Search")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.ic_search_lead);
            } else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("Not-Interested")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.ic_not_intrested);
            } else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("No-Response")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.ic_no_responce);
            } else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("Finalised")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.ic_finalized);
            } else if (mWebLeadsDatum.getStatus().equalsIgnoreCase("null")) {
                holder.proc_leadFollowupHistory.setText("Open");
            }
        } catch (Exception e) {

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Saving data to SingleTon
                try {
                    ConvertStockInstance.getInstance().setExecutiveid("");
                    ConvertStockInstance.getInstance().setMfgyear("");
                    ConvertStockInstance.getInstance().setMfgmonth("");

                    ConvertStockInstance.getInstance().setRegisteryear(mWebLeadsDatum.getRegyear());
                    ConvertStockInstance.getInstance().setRegistermonth(mWebLeadsDatum.getRegmonth());

                    ConvertStockInstance.getInstance().setVehicletype("");

                    ConvertStockInstance.getInstance().setMake(mWebLeadsDatum.getPrimaryMake());
                    ConvertStockInstance.getInstance().setModel(mWebLeadsDatum.getPrimaryModel());
                    ConvertStockInstance.getInstance().setVariant(mWebLeadsDatum.getPrimaryVariant());

                    ConvertStockInstance.getInstance().setRegisternumber(mWebLeadsDatum.getRegno());
                    ConvertStockInstance.getInstance().setColor(mWebLeadsDatum.getColor());

                    ConvertStockInstance.getInstance().setKms("" + mWebLeadsDatum.getKms());

                    ConvertStockInstance.getInstance().setOwner("" + mWebLeadsDatum.getOwners());
                    ConvertStockInstance.getInstance().setRegistercity("" + mWebLeadsDatum.getRegisterCity());
                    ConvertStockInstance.getInstance().setInsurancetype("");

                    ConvertStockInstance.getInstance().setLeadid("" + mWebLeadsDatum.getId());
                    ConvertStockInstance.getInstance().setLeadremark(mWebLeadsDatum.getRemark());

                    if (!mWebLeadsDatum.getLeadDate().equalsIgnoreCase("")) {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date date = null;
                        try {
                            date = dateFormat.parse(mWebLeadsDatum.getLeadDate());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
                        ConvertStockInstance.getInstance().setLeaddate(dateFormat1.format(date));
                    } else {
                        ConvertStockInstance.getInstance().setLeaddate("");
                    }

                    // ConvertStockInstance.getInstance().setLeaddate(mWebLeadsDatum.getLeadDate());
                    ConvertStockInstance.getInstance().setDealerid(mWebLeadsDatum.getDispatchid() + "");
                    ConvertStockInstance.getInstance().setExename(mWebLeadsDatum.getExecutiveName());

                    ConvertStockInstance.getInstance().setInsurancedate("");

                    // mContext.startActivity(new Intent(mContext, ProcurementDetails.class));
                    CommonMethods.setvalueAgainstKey(a, "PWLEADS", "WebLeads");
                    int pos = holder.getAdapterPosition();
                    Intent intent = new Intent(mContext, ProcurementDetails.class);
                    // Bundle bundle = new Bundle();
                    if (mWebLeadsDatum.getName().trim().equalsIgnoreCase("NA") || mWebLeadsDatum.getName().trim().equalsIgnoreCase("") || mWebLeadsDatum.getName().trim().equalsIgnoreCase("null")) {
                        intent.putExtra("NM", "UnKnown");
                    } else {
                        intent.putExtra("NM", mWebLeadsDatum.getName().trim());
                    }
                    intent.putExtra("MN", mWebLeadsDatum.getMobile().trim());
                    intent.putExtra("EM", mWebLeadsDatum.getEmail().trim());
                    intent.putExtra("LS", mWebLeadsDatum.getStatus().trim());
                    intent.putExtra("PD", mWebLeadsDatum.getPostedDate().trim());
                    intent.putExtra("MK", mWebLeadsDatum.getPrimaryMake().trim());
                    intent.putExtra("MD", mWebLeadsDatum.getPrimaryModel().trim());
                    intent.putExtra("MV", mWebLeadsDatum.getPrimaryVariant().trim());
                    intent.putExtra("PR", mWebLeadsDatum.getPrice().trim());
                    intent.putExtra("RN", mWebLeadsDatum.getRegno().trim());
                    intent.putExtra("NFD", mWebLeadsDatum.getFollowUpDate().trim());

                    if (mWebLeadsDatum.getExecutiveName() == null) {
                        intent.putExtra("ENM", "NA");
                    } else {
                        intent.putExtra("ENM", mWebLeadsDatum.getExecutiveName().toString().trim());
                    }

                    intent.putExtra("RGC", mWebLeadsDatum.getRegisterCity().trim());
                    intent.putExtra("OWN", mWebLeadsDatum.getOwners() + "");
                    intent.putExtra("MYR", mWebLeadsDatum.getRegyear().trim());
                    intent.putExtra("CLR", mWebLeadsDatum.getColor().trim());
                    intent.putExtra("KMS", mWebLeadsDatum.getKms() + "");
                    intent.putExtra("DID", mWebLeadsDatum.getDispatchid() + "");
                    intent.putExtra("RMK", mWebLeadsDatum.getRemark().trim());
                    intent.putExtra("LD", holder.proc_leadDays.getText().toString().trim());
                    try {
                        intent.putExtra("MFY", mWebLeadsDatum.getMfgYear().trim());
                    } catch (Exception e) {
                        intent.putExtra("MFY", "");
                    }
                    intent.putExtra("IDL", mWebLeadsDatum.getId() + "");
                    intent.putExtra("position", pos + "");
                    intent.putExtra("lead_type", mWebLeadsDatum.getLeadType());
                    //intent.putExtras(bundle);
                    mContext.startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return procList.size();
    }


    private String capitalize(String capString) {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()) {
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }

    private long printDifference(Date startDate, Date endDate) {
        long different = endDate.getTime() - startDate.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long elapsedDays = different / daysInMilli;
        return elapsedDays;
    }

    private void openWhatsApp(String number) {
        String smsNumber = "91" + number; //without '+'
        try {

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_VIEW);
            String url = "https://api.whatsapp.com/send?phone=" + smsNumber + "&text=" + "";
            sendIntent.setData(Uri.parse(url));
            mContext.startActivity(sendIntent);

        } catch (Exception e) {
            Toast.makeText(mContext, "Error/n" + e.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    //searchfunctionaly WebLeads

    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());
        procList.clear();
        if (charText.length() == 0) {
            procList.addAll(itemListSearch);
        } else {
            for (PWLDatum it : itemListSearch) {
                if (it.getPrimaryMake().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getPrimaryModel().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getPrimaryVariant().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getName().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getMobile().toLowerCase(Locale.getDefault()).contains(charText)
                ) {
                    procList.add(it);
                }
            }
        }
        try {
            lazyloaderWebLeads.Pupdatecountweb(procList.size());
        } catch (Exception e) {

        }
        notifyDataSetChanged();

    }
}