package com.mfcwl.mfc_dealer.Procurement.Activity;

import android.content.Intent;
import android.graphics.Color;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Procurement.Fragment.ProcFollowUpHistory;
import com.mfcwl.mfc_dealer.Procurement.Fragment.ProcurementAboutLead;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;

public class ProcurementDetails extends AppCompatActivity implements TabLayout.OnTabSelectedListener, View.OnClickListener {

    ViewPager vp_ProcDetails;
    ImageView iv_back;
    String Lname = "";
    String Lmobile = "";
    String Lemail = "";
    static String Lstatus = "";
    String Lposteddate = "";
    String Lmake = "";
    String Lmodel = "";
    String Lprice = "";
    String Lregno = "";
    static String Lfollupdate = "";
    String Lexename = "", Ldays = "", Lvariant, lead_type = "";
    public static String position = "";

    public static String Lregcity = "", Lowner = "", Lregyear = "", Lcolor = "", Lkms = "", Ldid = "", Leadid = "", Remarks = "", Lmfyear = "", Webleadid = "";
    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate: ");
        setContentView(R.layout.activity_procurement_details);
        getValue();
        InitUI();
    }

    private void InitUI() {
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_procDetails);
        vp_ProcDetails = (ViewPager) findViewById(R.id.vp_ProcDetails);
        iv_back = (ImageView) findViewById(R.id.iv_back);

        ProcurementPagerAdapter viewPagerAdapter = new ProcurementPagerAdapter(getSupportFragmentManager());
        vp_ProcDetails.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(vp_ProcDetails);
        tabLayout.setTabTextColors(Color.parseColor("#aaaaaa"), Color.parseColor("#2d2d2d"));
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#e5e5e5"));

        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(vp_ProcDetails));
        iv_back.setOnClickListener(this::onClick);
    }

    protected void getValue() {
        Intent bundle1 = getIntent();
        Lname = bundle1.getStringExtra("NM");
        Lmobile = bundle1.getStringExtra("MN");
        Lemail = bundle1.getStringExtra("EM");
        Lstatus = bundle1.getStringExtra("LS");
        Lposteddate = bundle1.getStringExtra("PD");
        Lmake = bundle1.getStringExtra("MK");
        Lmodel = bundle1.getStringExtra("MD");
        Lprice = bundle1.getStringExtra("PR");

        Lregno = bundle1.getStringExtra("RN");
        Lfollupdate = bundle1.getStringExtra("NFD");
        Lexename = bundle1.getStringExtra("ENM");
        // Log.e("private:lead date", Lfollupdate);

        Lregcity = bundle1.getStringExtra("RGC");
        Lowner = bundle1.getStringExtra("OWN");
        Lregyear = bundle1.getStringExtra("MYR");
        Lcolor = bundle1.getStringExtra("CLR");
        Lkms = bundle1.getStringExtra("KMS");
        Ldid = bundle1.getStringExtra("DID");
        Ldays = bundle1.getStringExtra("LD");
        Lvariant = bundle1.getStringExtra("MV");

        if (CommonMethods.getstringvaluefromkey(this, "PWLEADS").equals("PrivateLeads")) {
            Lmfyear = bundle1.getStringExtra("MFY");
            Leadid = bundle1.getStringExtra("LID");
            Remarks = bundle1.getStringExtra("RMK");
        }
        if (CommonMethods.getstringvaluefromkey(this, "PWLEADS").equals("WebLeads")) {
            Remarks = bundle1.getStringExtra("RMK");
            Lmfyear = bundle1.getStringExtra("MFY");
            Webleadid = bundle1.getStringExtra("IDL");
        }

        position = bundle1.getStringExtra("position");
        lead_type = bundle1.getStringExtra("lead_type");

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        switch (tab.getPosition()) {
            case 0:
                vp_ProcDetails.setCurrentItem(0);
                break;

            case 1:
                vp_ProcDetails.setCurrentItem(1);
                break;

        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                try {
                    // if (CommonMethods.getstringvaluefromkey(this, "isProcLeadDetails").equalsIgnoreCase("true")) {
                    MainActivity.getProcFragmentRefreshListener().onRefresh();
                    // }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            //  if (CommonMethods.getstringvaluefromkey(this, "isProcLeadDetails").equalsIgnoreCase("true")) {
            MainActivity.getProcFragmentRefreshListener().onRefresh();
            //  }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finish();
    }

    public class ProcurementPagerAdapter extends FragmentStatePagerAdapter {
        private String[] tabTitles = new String[]{"About Lead", "Follow-up History"};

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }

        public ProcurementPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    if (CommonMethods.getstringvaluefromkey(ProcurementDetails.this, "PWLEADS").equals("PrivateLeads")) {
                        return new ProcurementAboutLead(Lname, Lmobile, Lemail, Lstatus, Lposteddate, Lmake, Lmodel, Lprice, Lregno, Lfollupdate, Lexename, Lregcity, Lowner, Lregyear, Lcolor, Lkms, Ldid, Remarks, Ldays, Lvariant, Leadid, Lmfyear, lead_type);
                    } else {
                        return new ProcurementAboutLead(Lname, Lmobile, Lemail, Lstatus, Lposteddate, Lmake, Lmodel, Lprice, Lregno, Lfollupdate, Lexename, Lregcity, Lowner, Lregyear, Lcolor, Lkms, Ldid, Remarks, Ldays, Lvariant, Webleadid, Lmfyear, lead_type);
                    }
                case 1:
                    if (CommonMethods.getstringvaluefromkey(ProcurementDetails.this, "PWLEADS").equals("PrivateLeads")) {
                        return new ProcFollowUpHistory(Leadid, lead_type);
                    } else {
                        return new ProcFollowUpHistory(Ldid, lead_type);
                    }
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return tabTitles.length;
        }
    }
}
