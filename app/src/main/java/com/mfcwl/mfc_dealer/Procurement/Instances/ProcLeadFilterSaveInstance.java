package com.mfcwl.mfc_dealer.Procurement.Instances;

import com.mfcwl.mfc_dealer.InstanceCreate.LeadSection.LeadFilterSaveInstance;

import org.json.JSONArray;

import java.util.HashMap;

public class ProcLeadFilterSaveInstance {

    private static ProcLeadFilterSaveInstance mInstance = null;

    HashMap<String, String> savedatahashmap;
    JSONArray statusarray = null;

    private ProcLeadFilterSaveInstance() {
        savedatahashmap = new HashMap<String, String>();
        statusarray = new JSONArray();
    }

    public static ProcLeadFilterSaveInstance getInstance() {
        if (mInstance == null) {
            mInstance = new ProcLeadFilterSaveInstance();
        }
        return mInstance;
    }

    public HashMap<String, String> getSavedatahashmap() {
        return savedatahashmap;
    }

    public void setSavedatahashmap(HashMap<String, String> savedatahashmap) {
        this.savedatahashmap = savedatahashmap;
    }

    public JSONArray getStatusarray() {
        return statusarray;
    }

    public void setStatusarray(JSONArray statusarray) {
        this.statusarray = statusarray;
    }
}
