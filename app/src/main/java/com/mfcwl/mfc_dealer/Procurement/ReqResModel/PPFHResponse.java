package com.mfcwl.mfc_dealer.Procurement.ReqResModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PPFHResponse {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("FollowUps")
    @Expose
    private List<PPFHFollowup> followUps = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<PPFHFollowup> getFollowUps() {
        return followUps;
    }

    public void setFollowUps(List<PPFHFollowup> followUps) {
        this.followUps = followUps;
    }
}
