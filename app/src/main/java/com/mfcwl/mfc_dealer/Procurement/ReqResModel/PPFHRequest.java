package com.mfcwl.mfc_dealer.Procurement.ReqResModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PPFHRequest {
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("LeadId")
    @Expose
    private String leadId;
    @SerializedName("LeadType")
    @Expose
    private String leadType;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getLeadType() {
        return leadType;
    }

    public void setLeadType(String leadType) {
        this.leadType = leadType;
    }

}
