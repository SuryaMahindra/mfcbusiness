package com.mfcwl.mfc_dealer.Procurement.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.Activity.LeadSection.LeadConstantsSection;
import com.mfcwl.mfc_dealer.Activity.Leads.PrivateLeadsConstant;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragmentNew;
import com.mfcwl.mfc_dealer.Model.LeadSection.PrivateLeadWhere;
import com.mfcwl.mfc_dealer.Model.LeadSection.PrivateLeadWhereIn;
import com.mfcwl.mfc_dealer.Model.LeadSection.PrivateLeadsModelRequest;
import com.mfcwl.mfc_dealer.Model.LeadSection.PrivateLeadsWhereor;
import com.mfcwl.mfc_dealer.Procurement.Activity.ProcurementDetails;
import com.mfcwl.mfc_dealer.Procurement.Adapter.ProcurementPrivateLeadAdapter;
import com.mfcwl.mfc_dealer.Procurement.Instances.ProcLeadFilterSaveInstance;
import com.mfcwl.mfc_dealer.Procurement.Instances.ProcSortInstanceLeadSection;
import com.mfcwl.mfc_dealer.Procurement.ProcInterface.PFilterApply;
import com.mfcwl.mfc_dealer.Procurement.ProcInterface.PLazyloaderPrivateLeads;
import com.mfcwl.mfc_dealer.Procurement.ProcInterface.PSearchPL;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PPLResponse;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PPLResponseDatum;
import com.mfcwl.mfc_dealer.Procurement.RetrofitConfig.ProcPrivateLeadService;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GAConstants;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import rdm.NotificationActivity;
import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.activity;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchImage;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchicon;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toggle;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toolbar;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.tv_header_title;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ASM;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_DEALER_CRE;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_PROCUREMENT;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_SALES;

public class ProcPrivateLead extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    Button proc_btnAllLeads, proc_btnFollowUpLeads;
    SwipeRefreshLayout proc_swipeRefresh;
    RecyclerView proc_recyclerView;
    TextView proc_noResults;

    private int pageItem = 23;
    private String page_no = "1";
    private int counts = 1;
    private int negotiationLeadCount = 0;
    private boolean isLoadmore;

    private List<PrivateLeadWhere> mwhereList;
    private List<PrivateLeadWhereIn> mwhereinList;
    private List<PrivateLeadsWhereor> mwhereorList;

    // private String whichLeads = "AllLeads";
    private String searchQuery = "";

    private JSONArray leadstatusJsonArray = new JSONArray();
    private ProcurementPrivateLeadAdapter mAdapter;
    private List<PPLResponseDatum> mFollowuplist;
    private List<PPLResponseDatum> mAllList;
    private List<String> mWhereList;

    public boolean flag = false;
    int i = 0;
    String TAG = getClass().getSimpleName();
    // private List<PrivateLeadWhere> mwhereList;
    private HashMap<String, String> postedfollDateHashMap = new HashMap<String, String>();

    PrivateLeadsModelRequest privateLeadsModelRequest = new PrivateLeadsModelRequest();

    public ProcPrivateLead() {

    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);


        String uType = CommonMethods.getstringvaluefromkey(activity, "user_type");
        searchImage.setImageResource(R.drawable.search);
          MenuItem asm_mail = menu.findItem(R.id.asm_mail);

          //new ASM
          if (uType.equalsIgnoreCase("dealer")) {
              asm_mail.setVisible(false);
          }else if(CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_DEALER_CRE) ||
                  CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_SALES) ||
                  CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_PROCUREMENT)){
              asm_mail.setVisible(false);
          }else{
              asm_mail.setVisible(true);
          }
          MenuItem asmHome = menu.findItem(R.id.asmHome);
          MenuItem add_image = menu.findItem(R.id.add_image);
          MenuItem share = menu.findItem(R.id.share);
          MenuItem delete_fea = menu.findItem(R.id.delete_fea);
          MenuItem stock_fil_clear = menu.findItem(R.id.stock_fil_clear);
          MenuItem notification_bell = menu.findItem(R.id.notification_bell);
          MenuItem notification_cancel = menu.findItem(R.id.notification_cancel);

          asmHome.setVisible(false);
          add_image.setVisible(false);
          share.setVisible(false);
          delete_fea.setVisible(false);
          stock_fil_clear.setVisible(false);
          notification_bell.setVisible(false);
          notification_cancel.setVisible(false);


          asm_mail.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

              @Override
              public boolean onMenuItemClick(MenuItem item) {
                  //Toast.makeText(getActivity(),"proc private leads sending email...",Toast.LENGTH_LONG).show();
                  if (CommonMethods.isInternetWorking(getActivity())) {

                      String isEmail="";

                      PrivateLeadWhereIn email =new PrivateLeadWhereIn();

                      ArrayList<String> emailArray = new ArrayList<String>();
                      emailArray.add("true");

                      email.setColumn("isEmail");
                      email.setValues(emailArray);

                      for(int i =0;i<mwhereinList.size();i++){

                          if(mwhereinList.get(i).getColumn().equalsIgnoreCase("isEmail")){
                              isEmail="isEmail";
                          }
                      }

                      if(isEmail.equalsIgnoreCase("")) {
                          mwhereinList.add(email);
                          privateLeadsModelRequest.setWherein(mwhereinList);
                      }

                      privateLeadsModelRequest.setWherein(mwhereinList);

                      // custom dialog
                      Dialog dialog = new Dialog(activity);
                      dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                      dialog.getWindow().setGravity(Gravity.TOP|Gravity.RIGHT);
                      dialog.setContentView(R.layout.common_cus_dialog);
                      TextView Message, Message2;
                      Button cancel, Confirm;
                      Message = dialog.findViewById(R.id.Message);
                      Message2 = dialog.findViewById(R.id.Message2);
                      cancel = dialog.findViewById(R.id.cancel);
                      Confirm = dialog.findViewById(R.id.Confirm);
                      Message.setText(GlobalText.are_you_sure_to_send_mail);
                      //Message2.setText("Go ahead and Submit ?");
                      Confirm.setOnClickListener(new View.OnClickListener() {
                          @Override
                          public void onClick(View v) {
                              sendEmailProcPrivateLeadRequest(getActivity(), privateLeadsModelRequest);
                              dialog.dismiss();
                          }
                      });
                      cancel.setOnClickListener(new View.OnClickListener() {
                          @Override
                          public void onClick(View v) {
                              dialog.dismiss();
                          }
                      });
                      dialog.show();



                  } else {
                      CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);

                  }
                  return true;
              }
          });


    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.fragment_procurement_web_leads, container, false);
        setHasOptionsMenu(true);

        if(CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(USERTYPE_ASM))
        {
            if(tv_header_title!=null)
            {
                searchicon.setVisibility(View.VISIBLE);
                searchImage.setVisibility(View.VISIBLE);
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Procurement Leads");
                tv_header_title.setTextColor(getResources().getColor(R.color.white));
            }
        }
        else {
            if (tv_header_title != null) {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Procurement Leads");
                tv_header_title.setTextColor(getResources().getColor(R.color.white));
            }
        }
        InitUI(view);
        return view;
    }

    private void InitUI(View view) {


        proc_btnAllLeads = view.findViewById(R.id.proc_btnAllLeads);
        proc_btnFollowUpLeads = view.findViewById(R.id.proc_btnFollowUpLeads);
        proc_swipeRefresh = view.findViewById(R.id.proc_swipeRefresh);
        proc_recyclerView = view.findViewById(R.id.proc_recyclerView);
        proc_noResults = view.findViewById(R.id.proc_noResults);

        proc_swipeRefresh.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.CYAN);
        proc_swipeRefresh.setOnRefreshListener(this);

        isLoadmore = false;

        if (PrivateLeadsConstant.getInstance().getWhichleads().equalsIgnoreCase("AllLeads")) {
            proc_btnAllLeads.setTextColor(Color.BLACK);
            proc_btnAllLeads.setBackgroundResource(R.drawable.my_button);
            proc_btnFollowUpLeads.setTextColor(Color.parseColor("#b9b9b9"));
            proc_btnFollowUpLeads.setBackgroundResource(R.drawable.my_buttom_gray_light);
        } else if (PrivateLeadsConstant.getInstance().getWhichleads().equalsIgnoreCase("FollowupLeads")) {
            proc_btnFollowUpLeads.setTextColor(Color.BLACK);
            proc_btnFollowUpLeads.setBackgroundResource(R.drawable.my_button);
            proc_btnAllLeads.setTextColor(Color.parseColor("#b9b9b9"));
            proc_btnAllLeads.setBackgroundResource(R.drawable.my_buttom_gray_light);
        }

        onClickListener();

        if (CommonMethods.getstringvaluefromkey(getActivity(), "ProcPrivateLeads").equalsIgnoreCase("true")) {
            try {
                i = Integer.parseInt(ProcurementDetails.position);
            } catch (Exception e) {
                i = 0;
            }
        } else {
            i = 0;
        }
        Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"), GlobalText.proc_privateLeads, GlobalText.android);
        bottomTabScrolling();
        PrivateProcLeadRequest(getActivity());
        onCallbackEvents();
        int margin = getResources().getDimensionPixelSize(R.dimen.recyclepadding);
        //new ASM
        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
        }else{
            ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) proc_swipeRefresh.getLayoutParams();
            mlp.setMargins(0,0,0,margin);
        }

    }

    private void onClickListener() {
        proc_btnAllLeads.setOnClickListener(this::onClick);
        proc_btnFollowUpLeads.setOnClickListener(this::onClick);
    }

    private void bottomTabScrolling() {
        proc_recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            int verticalOffset;
            boolean scrollingUp;

            // Determines the scroll UP/DOWN direction
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                verticalOffset += dy;
                scrollingUp = dy > 0;
                if (scrollingUp) {
                    if (flag) {
                        MainActivity.bottom_topAnimation();
                        flag = false;
                    }
                } else {
                    if (!flag) {
                        MainActivity.top_bottomAnimation();
                        flag = true;
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        restoreInputValues();
        i = 0;
        switch (v.getId()) {
            case R.id.proc_btnAllLeads:
                PrivateLeadsConstant.getInstance().setWhichleads("AllLeads");
                proc_btnAllLeads.setTextColor(Color.BLACK);
                proc_btnAllLeads.setBackgroundResource(R.drawable.my_button);
                proc_btnFollowUpLeads.setTextColor(Color.parseColor("#b9b9b9"));
                proc_btnFollowUpLeads.setBackgroundResource(R.drawable.my_buttom_gray_light);
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"), GlobalText.proc_private_allLeads, GlobalText.android);
                PrivateProcLeadRequest(getActivity());
                break;
            case R.id.proc_btnFollowUpLeads:
                PrivateLeadsConstant.getInstance().setWhichleads("FollowupLeads");
                proc_btnFollowUpLeads.setTextColor(Color.BLACK);
                proc_btnFollowUpLeads.setBackgroundResource(R.drawable.my_button);
                proc_btnAllLeads.setTextColor(Color.parseColor("#b9b9b9"));
                proc_btnAllLeads.setBackgroundResource(R.drawable.my_buttom_gray_light);
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"), GlobalText.proc_private_followLeads, GlobalText.android);
                preparefollowupPrivateLeadRequest(getActivity());
                break;
        }
    }

    private void restoreInputValues() {
        page_no = "1";
        counts = 1;
        negotiationLeadCount = 0;
        isLoadmore = false;
    }

    @Override
    public void onRefresh() {
        restoreInputValues();
        proc_swipeRefresh.setRefreshing(true);
        if (PrivateLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
            PrivateProcLeadRequest(getActivity());
        } else {
            preparefollowupPrivateLeadRequest(getActivity());
        }
    }

    private void onCallbackEvents() {

        ((MainActivity) getActivity()).setFragmentPPrivateSortListener(new MainActivity.FragmentPPrivateSortListener() {

            @Override
            public void onSortPPrivate() {
                if (CommonMethods.getstringvaluefromkey(getActivity(), "ProcPrivateLeads").equalsIgnoreCase("false")) {
                    i = 0;
                }
                restoreInputValues();
                if (PrivateLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
                    PrivateProcLeadRequest(getActivity());
                } else {
                    //preparefollowupPrivateLeadRequest();
                    preparefollowupPrivateLeadRequest(getActivity());
                }
            }
        });

        ((MainActivity) getActivity()).PupdateSearchPrivateLead(new PSearchPL() {
            @Override
            public void onPSearchPrivateLead(String query) {

                searchQuery = query;
                if (mAllList.size() == 0) {
                    i = 0;
                    restoreInputValues();
                    if (PrivateLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
                        PrivateProcLeadRequest(getActivity());
                    } else {
                        preparefollowupPrivateLeadRequest(getActivity());
                    }
                }

                mAdapter.filter(query);

                /*if (mAdapter != null) {
                    mAdapter.filter(query);
                } else {

                }*/
            }
        });

        ((MainActivity) getActivity()).setPLazyloaderPrivateLeadsListener(new PLazyloaderPrivateLeads() {
            @Override
            public void PloadmorePrivate(int ItemCount) {
                counts = counts + 1;
                double maxPageNumber = Math.ceil((double) (negotiationLeadCount) / (double) pageItem);
                if (counts > maxPageNumber) {
                    return;
                }
                page_no = Integer.toString(counts);

                isLoadmore = true;

                if (PrivateLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
                    PrivateProcLeadRequest(getActivity());
                } else {
                    preparefollowupPrivateLeadRequest(getActivity());
                }
            }

            @Override
            public void PupdatecountPrivate(int count) {
                //   Log.e("updatecountPrivate ", "Filter Size " + count);
                if (count <= 3) {
                    mAllList.clear();
                    if (CommonMethods.getstringvaluefromkey(getActivity(), "ProcPrivateLeads").equalsIgnoreCase("false")) {
                        i = 0;
                    }
                    restoreInputValues();
                    if (PrivateLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
                        PrivateProcLeadRequest(getActivity());
                    } else {
                        preparefollowupPrivateLeadRequest(getActivity());
                    }
                }
            }
        });

        //Apply Filter

        ((MainActivity) getActivity()).setPApplyPrivateFilterListener(new PFilterApply() {
            @Override
            public void applyPFilter(String typeLead) {
                restoreInputValues();
                if (CommonMethods.getstringvaluefromkey(getActivity(), "ProcPrivateLeads").equalsIgnoreCase("false")) {
                    i = 0;
                }
                if (PrivateLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
                    PrivateProcLeadRequest(getActivity());
                } else {
                    preparefollowupPrivateLeadRequest(getActivity());
                }
            }
        });
    }

    private void attachtoAdapter(List<PPLResponseDatum> mList) {
        mAdapter = new ProcurementPrivateLeadAdapter(getActivity(), getContext(), mList);
        proc_recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getActivity());
        proc_recyclerView.setLayoutManager(mLinearLayoutManager);
        proc_recyclerView.setAdapter(mAdapter);

        proc_recyclerView.scrollToPosition(i);

    }

    private void PrivateProcLeadRequest(Context mContext) {
        mwhereList = new ArrayList<>();
        mwhereinList = new ArrayList<>();
        mwhereorList = new ArrayList<>();

        //new changes
         privateLeadsModelRequest = new PrivateLeadsModelRequest();

        privateLeadsModelRequest.setPageItems(Integer.toString(pageItem));
        privateLeadsModelRequest.setPage(page_no);
        sortbyPrivateLeads(privateLeadsModelRequest);

        searchbyPrivateLeads(mwhereorList);
        privateLeadsModelRequest.setWhereOr(mwhereorList);

        //Posted Followup Date Filter
        setPostedFollowupDateInfo();

        privateLeadsModelRequest.setWhere(mwhereList);

        //Update Lead Status
        PrivateLeadWhereIn privateLeadWhereIn = new PrivateLeadWhereIn();
        // setLeadType(privateLeadWhereIn);
        setLeadStatus(privateLeadWhereIn);

        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_DEALER_CRE) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_SALES) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_PROCUREMENT)) {
            PrivateLeadWhere where = new PrivateLeadWhere();
            where.setColumn("executive_id");
            where.setOperator("=");
            where.setValue(CommonMethods.getstringvaluefromkey(activity, "user_id"));
            mwhereList.add(where);
            privateLeadsModelRequest.setWhere(mwhereList);
        }

        privateLeadsModelRequest.setWherein(mwhereinList);
        ProcPrivateLeadRequest((Activity) mContext, privateLeadsModelRequest);

    }

    private void searchbyPrivateLeads(List<PrivateLeadsWhereor> mwhereorList) {

        if (!searchQuery.equals("")) {
            //Make
            PrivateLeadsWhereor vehicle_make = new PrivateLeadsWhereor();
            vehicle_make.setColumn("make");
            vehicle_make.setOperator("like");
            vehicle_make.setValue(searchQuery.trim());
            mwhereorList.add(vehicle_make);

            //Model
            PrivateLeadsWhereor vehicle_model = new PrivateLeadsWhereor();
            vehicle_model.setColumn("model");
            vehicle_model.setOperator("like");
            vehicle_model.setValue(searchQuery.trim());
            mwhereorList.add(vehicle_model);

            //Variant
            PrivateLeadsWhereor vehicle_variant = new PrivateLeadsWhereor();
            vehicle_variant.setColumn("variant");
            vehicle_variant.setOperator("like");
            vehicle_variant.setValue(searchQuery.trim());
            mwhereorList.add(vehicle_variant);

            //Name
            PrivateLeadsWhereor customer_name = new PrivateLeadsWhereor();
            customer_name.setColumn("customer_name");
            customer_name.setOperator("like");
            customer_name.setValue(searchQuery.trim());
            mwhereorList.add(customer_name);

            //Mobile
            PrivateLeadsWhereor customer_mobile = new PrivateLeadsWhereor();
            customer_mobile.setColumn("customer_mobile");
            customer_mobile.setOperator("like");
            customer_mobile.setValue(searchQuery.trim());
            mwhereorList.add(customer_mobile);

        }
    }

    private void sortbyPrivateLeads(PrivateLeadsModelRequest privateLeadsModelRequest) {

        if (ProcSortInstanceLeadSection.getInstance().getSortby().equals("posted_date_l_o")) {
            privateLeadsModelRequest.setOrderBy("created_at");
            privateLeadsModelRequest.setOrderByReverse("true");
        } else if (ProcSortInstanceLeadSection.getInstance().getSortby().equals("posted_date_o_l")) {
            privateLeadsModelRequest.setOrderBy("created_at");
            privateLeadsModelRequest.setOrderByReverse("false");
        } else if (ProcSortInstanceLeadSection.getInstance().getSortby().equals("folowup_date_l_o")) {
            privateLeadsModelRequest.setOrderBy("follow_date");
            privateLeadsModelRequest.setOrderByReverse("true");
        } else if (ProcSortInstanceLeadSection.getInstance().getSortby().equals("folowup_date_o_l")) {
            privateLeadsModelRequest.setOrderBy("follow_date");
            privateLeadsModelRequest.setOrderByReverse("false");
        } else {
            privateLeadsModelRequest.setOrderBy("created_at");
            privateLeadsModelRequest.setOrderByReverse("true");
        }

    }

    private void setLeadType(PrivateLeadWhereIn wherein) {
        ArrayList<String> listType = new ArrayList<String>();
        wherein.setColumn("lead_type");
        listType.add("procurement");
        wherein.setValues(listType);
        mwhereinList.add(wherein);
    }

    private void setLeadStatus(PrivateLeadWhereIn privateLeadWhereIn) {

        privateLeadWhereIn.setColumn("lead_type");
        privateLeadWhereIn.setValues(Collections.singletonList("procurement"));
        mwhereinList.add(privateLeadWhereIn);

        ArrayList<String> listStatus = new ArrayList<String>();
        leadstatusJsonArray = ProcLeadFilterSaveInstance.getInstance().getStatusarray();


        if (CommonMethods.getstringvaluefromkey(getActivity(), "user_type").equalsIgnoreCase("dealer")) {

            if (leadstatusJsonArray.length() != 0) {
                privateLeadWhereIn.setColumn("lead_status");
                for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                    try {
                        listStatus.add(leadstatusJsonArray.getString(i));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                privateLeadWhereIn.setValues(listStatus);
                mwhereinList.add(privateLeadWhereIn);
            }

        } else {

            if (CommonMethods.getstringvaluefromkey(getActivity(), "leadStatus").equalsIgnoreCase("true")) {


                if (!CommonMethods.getstringvaluefromkey(getActivity(), "Lvalue").equalsIgnoreCase("")) {

                    if (leadstatusJsonArray.length() != 0) {
                        privateLeadWhereIn.setColumn("lead_status");

                        for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                            try {
                                LeadFilterFragmentNew.leadstatusJsonArray.remove(i);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        leadstatusJsonArray.put(CommonMethods.getstringvaluefromkey(getActivity(), "Lvalue"));
                        listStatus.add(CommonMethods.getstringvaluefromkey(getActivity(), "Lvalue"));

                        privateLeadWhereIn.setValues(listStatus);
                        mwhereinList.add(privateLeadWhereIn);
                    }
                }
            } else {

                if (leadstatusJsonArray.length() != 0) {
                    privateLeadWhereIn.setColumn("lead_status");
                    for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                        try {
                            listStatus.add(leadstatusJsonArray.getString(i));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    privateLeadWhereIn.setValues(listStatus);
                    mwhereinList.add(privateLeadWhereIn);
                }

            }

            ArrayList<String> listType = new ArrayList<String>();
            PrivateLeadWhereIn wherein = new PrivateLeadWhereIn();
            wherein.setColumn("lead_type");
            listType.add("procurement");
            wherein.setValues(listType);
            mwhereinList.add(wherein);

            ArrayList<String> code = new ArrayList<String>();
            PrivateLeadWhereIn whereinn = new PrivateLeadWhereIn();
            whereinn.setColumn("dealer_id");
            code.add(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"));
            whereinn.setValues(code);
            mwhereinList.add(whereinn);

        }

    }

    private void ProcPrivateLeadRequest(Activity mActivity, PrivateLeadsModelRequest leadRequest) {
        proc_swipeRefresh.setRefreshing(false);
        SpinnerManager.showSpinner(mActivity);
        ProcPrivateLeadService.ProcPrivateLead(getContext(), leadRequest, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mActivity);

                Response<PPLResponse> mRes = (Response<PPLResponse>) obj;
                PPLResponse mData = mRes.body();

                String str = new Gson().toJson(mData, PPLResponse.class);
                Log.i(TAG, " "+str);

                negotiationLeadCount = mData.getTotal();
                List<PPLResponseDatum> mlist = mData.getData();
                Log.i(TAG, "OnSuccess: " + mlist.size());
                if (isLoadmore) {
                    mAdapter.notifyItemInserted(mAllList.size() - 1);
                    mAllList.addAll(mlist);
                    mAdapter.notifyDataSetChanged();
                } else {
                    mAllList = new ArrayList<>();
                    mAllList.clear();
                    mAllList.addAll(mlist);
                    attachtoAdapter(mAllList);
                }
                isLeadsAvailable(negotiationLeadCount);
            }

            @Override
            public void OnFailure(Throwable t) {
                SpinnerManager.hideSpinner(mActivity);
                proc_swipeRefresh.setRefreshing(false);
                t.printStackTrace();
            }
        });
    }

    private void sendEmailProcPrivateLeadRequest(Activity mActivity, PrivateLeadsModelRequest leadRequest) {
        proc_swipeRefresh.setRefreshing(false);
        SpinnerManager.showSpinner(mActivity);
        ProcPrivateLeadService.sendEmailProcPrivateLead(getContext(), leadRequest, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mActivity);

                Response<String> mRes = (Response<String>) obj;
                String mData = mRes.body();
                CommonMethods.alertMessage(getActivity(),"Mail sent"+"\n"+mData.toString());

            }

            @Override
            public void OnFailure(Throwable t) {
                SpinnerManager.hideSpinner(mActivity);
                proc_swipeRefresh.setRefreshing(false);
                t.printStackTrace();
            }
        });
    }

    private void isLeadsAvailable(int negotiationLeadCount) {
        if (negotiationLeadCount == 0) {
            proc_noResults.setVisibility(View.VISIBLE);
            proc_swipeRefresh.setVisibility(View.GONE);

        } else {
            proc_noResults.setVisibility(View.GONE);
            proc_swipeRefresh.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onResume() {
        if(CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(USERTYPE_ASM))
        {
            toggle.setHomeAsUpIndicator(R.drawable.ic_new_hamburger_menu);
            searchicon.setVisibility(View.VISIBLE);
            searchImage.setVisibility(View.VISIBLE);
            toolbar.setBackgroundColor(getResources().getColor(R.color.black));
            if(tv_header_title!=null)
            {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Procurement Leads");
                tv_header_title.setTextColor(getResources().getColor(R.color.white));
            }
            Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_PROCUREMENT_LEAD,CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));

        }
        else {
            if (tv_header_title != null) {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Procurement Leads");
                tv_header_title.setTextColor(getResources().getColor(R.color.white));
            }
        }
        super.onResume();
    }

    private void preparefollowupPrivateLeadRequest(Context mContext) {

        mFollowuplist = new ArrayList<>();
        mWhereList = new ArrayList<>();

        mwhereList = new ArrayList<>();
        mwhereinList = new ArrayList<>();
        mwhereorList = new ArrayList<>();

        PrivateLeadsModelRequest privateLeadsModelRequest = new PrivateLeadsModelRequest();

        privateLeadsModelRequest.setPageItems(Integer.toString(pageItem));
        privateLeadsModelRequest.setPage(page_no);

        sortbyPrivateLeads(privateLeadsModelRequest);

        searchbyPrivateLeads(mwhereorList);

        //Posted Followup Date Filter
        setPostedFollowupDateInfo();
        privateLeadsModelRequest.setWhere(mwhereList);

        //Update Lead Status
        PrivateLeadWhereIn privateLeadWhereIn = new PrivateLeadWhereIn();
        setLeadStatus(privateLeadWhereIn);
        privateLeadsModelRequest.setWherein(mwhereinList);

        privateLeadsModelRequest.setWhereOr(mwhereorList);

        //follow-up date (apply future date over here)
        PrivateLeadWhere privateLeadWhere = new PrivateLeadWhere();
        Date d = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(d);
        privateLeadWhere.setColumn("follow_date");
        privateLeadWhere.setOperator("<=");
        privateLeadWhere.setValue(formattedDate);
        mwhereList.add(privateLeadWhere);

        String usertype = CommonMethods.getstringvaluefromkey(activity, "user_type");
        if (usertype.equalsIgnoreCase(USER_TYPE_DEALER_CRE) ||
                usertype.equalsIgnoreCase(USER_TYPE_SALES) ||
                usertype.equalsIgnoreCase(USER_TYPE_PROCUREMENT)) {
            PrivateLeadWhere where2 = new PrivateLeadWhere();
            where2.setColumn("executive_id");
            where2.setOperator("=");
            where2.setValue(CommonMethods.getstringvaluefromkey(activity, "user_id"));
            mwhereList.add(where2);
        }

        privateLeadsModelRequest.setWhere(mwhereList);

        sendfollowupPrivateLeadDetails(mContext, privateLeadsModelRequest);

    }

    private void sendfollowupPrivateLeadDetails(Context mContext, PrivateLeadsModelRequest privateLeadsModelRequest) {
        proc_swipeRefresh.setRefreshing(false);
        SpinnerManager.showSpinner(mContext);
        ProcPrivateLeadService.ProcPrivateLead(getContext(), privateLeadsModelRequest, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);

                Response<PPLResponse> mRes = (Response<PPLResponse>) obj;
                PPLResponse mData = mRes.body();
                negotiationLeadCount = mData.getTotal();
                List<PPLResponseDatum> mlist = mData.getData();
                Log.i(TAG, "OnSuccess: " + mlist.size());
                PPLResponseDatum mPrivateLeadDatum = null;

                for (int i = 0; i < mlist.size(); i++) {
                    mPrivateLeadDatum = mlist.get(i);
                    if (mPrivateLeadDatum.getLeadStatus() != null && !mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Sold") &&
                            !mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Lost") &&
                            !mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Closed") &&
                            !mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Bought") &&
                            !mPrivateLeadDatum.getStatus().equalsIgnoreCase("Not-Interested") &&
                            !mPrivateLeadDatum.getStatus().equalsIgnoreCase("Finalised")) {

                        mFollowuplist.add(mPrivateLeadDatum);
                    }
                }

                if (isLoadmore) {
                    mAdapter.notifyItemInserted(mAllList.size() - 1);
                    mAllList.addAll(mFollowuplist);
                    mAdapter.notifyDataSetChanged();
                } else {
                    mAllList = new ArrayList<>();
                    mAllList.clear();
                    mAllList.addAll(mFollowuplist);
                    attachtoAdapter(mAllList);
                }
                isLeadsAvailable(negotiationLeadCount);
            }

            @Override
            public void OnFailure(Throwable t) {
                SpinnerManager.hideSpinner(mContext);
                proc_swipeRefresh.setRefreshing(false);
                t.printStackTrace();
            }
        });
    }

    private void setPostedFollowupDateInfo() {
        postedfollDateHashMap = ProcLeadFilterSaveInstance.getInstance().getSavedatahashmap();
        String postdateinfo = "";

        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_TODAY) != null) {
            postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_TODAY);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_YESTER) != null) {
            postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_YESTER);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_7DAYS) != null) {
            postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_7DAYS);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_15DAYS) != null) {
            postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_15DAYS);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE) != null) {
            postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE);
        }

        PrivateLeadWhere postDateCustomWhereStart = new PrivateLeadWhere();
        PrivateLeadWhere postDateCustomWhereEnd = new PrivateLeadWhere();

        if (CommonMethods.getstringvaluefromkey(getActivity(), "user_type").equalsIgnoreCase("dealer")) {
            if (postdateinfo != null && postdateinfo.contains("@")) {
                //split the date and atach to query
                String[] createddates = postdateinfo.split("@");

                try {
                    postDateCustomWhereStart.setColumn("created_at");
                    postDateCustomWhereEnd.setColumn("created_at");

                    if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE) != null) {
                        postDateCustomWhereStart.setOperator(">=");
                        postDateCustomWhereEnd.setOperator("<=");

                    } else {
                        postDateCustomWhereStart.setOperator(">=");
                        postDateCustomWhereEnd.setOperator("<");
                    }

                    postDateCustomWhereStart.setValue(createddates[0]);
                    postDateCustomWhereEnd.setValue(createddates[1]);
                    mwhereList.add(postDateCustomWhereStart);
                    mwhereList.add(postDateCustomWhereEnd);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {

            if (CommonMethods.getstringvaluefromkey(getActivity(), "leadStatus").equalsIgnoreCase("true")) {

                try {
                    // remove
                    if (!CommonMethods.getstringvaluefromkey(getActivity(), "leads_status")
                            .equalsIgnoreCase("")) {

                    }else{
                        ProcLeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());
                    }
                  /////  ProcLeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());

                } catch (Exception e) {

                }


                String startDate = CommonMethods.getstringvaluefromkey(getActivity(), "startDate").toString();
                String endDate = CommonMethods.getstringvaluefromkey(getActivity(), "endDate").toString();

                if (!startDate.equalsIgnoreCase("")) {

                    try {
                        postDateCustomWhereStart.setColumn("created_at");
                        postDateCustomWhereEnd.setColumn("created_at");

                        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE) != null) {
                            postDateCustomWhereStart.setOperator(">=");
                            postDateCustomWhereEnd.setOperator("<=");

                        } else {
                            postDateCustomWhereStart.setOperator(">=");
                            postDateCustomWhereEnd.setOperator("<");
                        }

                        postDateCustomWhereStart.setValue(startDate);
                        postDateCustomWhereEnd.setValue(endDate);
                        mwhereList.add(postDateCustomWhereStart);
                        mwhereList.add(postDateCustomWhereEnd);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            } else {


                if (postdateinfo != null && postdateinfo.contains("@")) {
                    //split the date and atach to query
                    String[] createddates = postdateinfo.split("@");

                    try {
                        postDateCustomWhereStart.setColumn("created_at");
                        postDateCustomWhereEnd.setColumn("created_at");

                        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE) != null) {
                            postDateCustomWhereStart.setOperator(">=");
                            postDateCustomWhereEnd.setOperator("<=");

                        } else {
                            postDateCustomWhereStart.setOperator(">=");
                            postDateCustomWhereEnd.setOperator("<");
                        }

                        postDateCustomWhereStart.setValue(createddates[0]);
                        postDateCustomWhereEnd.setValue(createddates[1]);
                        mwhereList.add(postDateCustomWhereStart);
                        mwhereList.add(postDateCustomWhereEnd);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }
        }

        String followdateinfo = "";

        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TMRW) != null) {
            followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TMRW);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TODAY) != null) {
            followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TODAY);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_YESTER) != null) {
            followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_YESTER);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_7DAYS) != null) {
            followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_7DAYS);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_CUSTDATE) != null) {
            followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_CUSTDATE);
        }

        PrivateLeadWhere followDateCustomWhereStart = new PrivateLeadWhere();
        PrivateLeadWhere followDateCustomWhereEnd = new PrivateLeadWhere();
        if (followdateinfo != null && followdateinfo.contains("@")) {

            String[] follow = followdateinfo.split("@");
            try {
                followDateCustomWhereStart.setColumn("follow_date");
                followDateCustomWhereEnd.setColumn("follow_date");

                if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TMRW) != null) {
                    followDateCustomWhereStart.setOperator(">");
                    followDateCustomWhereEnd.setOperator("<=");
                    followDateCustomWhereStart.setValue(follow[1]);
                    followDateCustomWhereEnd.setValue(follow[0]);

                }
                if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TODAY) != null || postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_YESTER) != null
                        || postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_7DAYS) != null) {
                    followDateCustomWhereStart.setOperator(">=");
                    followDateCustomWhereEnd.setOperator("<");
                    followDateCustomWhereStart.setValue(follow[0]);
                    followDateCustomWhereEnd.setValue(follow[1]);
                }
                if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_CUSTDATE) != null) {
                    followDateCustomWhereStart.setOperator(">=");
                    followDateCustomWhereEnd.setOperator("<=");
                    followDateCustomWhereStart.setValue(follow[0]);
                    followDateCustomWhereEnd.setValue(follow[1]);
                }

                mwhereList.add(followDateCustomWhereStart);
                mwhereList.add(followDateCustomWhereEnd);


            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            if (CommonMethods.getstringvaluefromkey(getActivity(), "leadStatus").equalsIgnoreCase("true")
                    && !CommonMethods.getstringvaluefromkey(getActivity(), "value2").equalsIgnoreCase("")
                    && !CommonMethods.getstringvaluefromkey(getActivity(), "messageCenter").equalsIgnoreCase("true")) {


                followDateCustomWhereStart.setColumn("follow_date");
                followDateCustomWhereEnd.setColumn("follow_date");

                followDateCustomWhereStart.setOperator(CommonMethods.getstringvaluefromkey(getActivity(), "operator"));
                followDateCustomWhereEnd.setOperator(CommonMethods.getstringvaluefromkey(getActivity(), "operator2"));

                followDateCustomWhereStart.setValue(CommonMethods.getstringvaluefromkey(getActivity(), "value"));
                followDateCustomWhereEnd.setValue(CommonMethods.getstringvaluefromkey(getActivity(), "value2"));


                mwhereList.add(followDateCustomWhereStart);
                mwhereList.add(followDateCustomWhereEnd);

            }
        }
    }
}