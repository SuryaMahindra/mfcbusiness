package com.mfcwl.mfc_dealer.Procurement.RetrofitConfig;

import android.content.Context;

import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.Procurement.BaseService.AddProcBaseService;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.AddProcLeadRequest;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.AddProcLeadResponse;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class AddProcLeadSevice extends AddProcBaseService {
    public static void addProcLeadServer(AddProcLeadRequest reqAddLead, Context mContext, HttpCallResponse mHttpCallResponse) {
        AddProcLeadInterface mAddLeadService = retrofit.create(AddProcLeadInterface.class);
        Call<AddProcLeadResponse> mCall = mAddLeadService.createLead(reqAddLead);
        mCall.enqueue(new Callback<AddProcLeadResponse>() {
            @Override
            public void onResponse(Call<AddProcLeadResponse> call, Response<AddProcLeadResponse> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                }
            }

            @Override
            public void onFailure(Call<AddProcLeadResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });
    }

    public interface AddProcLeadInterface {

        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("private-leads/create")
        Call<AddProcLeadResponse> createLead(@Body AddProcLeadRequest mReqAddLead);
    }
}


