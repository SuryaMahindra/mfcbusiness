package com.mfcwl.mfc_dealer.Procurement.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.AddStockModel.CityMaster;
import com.mfcwl.mfc_dealer.AddStockModel.CommercialMakelist;
import com.mfcwl.mfc_dealer.AddStockModel.ModelDetails;
import com.mfcwl.mfc_dealer.AddStockModel.ModelValue;
import com.mfcwl.mfc_dealer.AddStockServices.YearService;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.AddProcLeadRequest;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.AddProcLeadResponse;
import com.mfcwl.mfc_dealer.Procurement.RetrofitConfig.AddProcLeadSevice;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.Global;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.Global_Urls;
import com.mfcwl.mfc_dealer.Utility.MonthUtility;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddProcLead extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout proc_linearBack;
    ImageView iv_close, iv_back;
    //Id Declaration
    private TextView prod_leadtypetv, prod_vehicletv, tv_prod_leaddate, tv_prod_leadstatus,
            tv_prod_city, tv_add_prod_years, tv_add_prod_month, tv_prod_color, tv_prod_owner,
            tv_prod_leadsource, tv_prod_make, tv_prod_modelVariant,
            tv_prod_insurance, tv_prod_insuExp, tv_prod_exeInfo, tv_follwdate;

    private TextView tv_add_prod_reg_years, tv_add_prod_reg_month, tv_Expline;

    private EditText et_prod_custName, et_prod_cusmobile, et_prod_cusemail, et_prod_cusaddress,
            et_prod_pincode, tv_prod_kms, et_proc_cust_exp, tv_prod_remark, tv_prod_reg,
            et_prod_refsource, et_prod_stockVin, tv_prodRemark;

    //Validation
    private TextView tv_prod_leadtypevaild, tv_prod_vehicletypevaild, tv_prod_leaddatevaild,
            tv_prod_leadstatusvaild, tv_prod_Cusnamevaild, tv_prod_numbervalid, tv_prod_emailvaild,
            tv_prod_CusaddressValid, tv_prod_cityvaild, tv_prod_yearvalid, tv_prod_month_error,
            tv_prod_colorvaild, tv_prod_kms_error, tv_prod_owner_error,
            tv_prod_makevaild, tv_prod_modelvariantvaild, tv_prod_cusbudgetvaild, tv_prod_reg_error,
            tv_prod_insu_error, tv_prod_insuexp_error, tv_prod_exeInfoValid, tv_followupdatevaild;

    private TextView tv_prod_reg_yearvalid, tv_prod_reg_month_error;


    Button prod_addcancel, add_proc_btn;

    TextView tv_prod_leadDatelbl, tv_prod_leadStatuslbl, tv_prod_cusName, tv_prod_cusmobilelbl,
            tv_prod_cusemaillbl, tv_prod_cusaddresslbl, tv_prod_citylbl, tv_prod_manufacturedonlbl,
            tv_prod_colorlbl, tv_prod_kmslbl, tv_prod_ownerlbl, tv_prod_makelbl,
            tv_prod_modellbl, tv_prod_cusbudgetlbl, tv_prod_reglbl, tv_prod_insulbl, tv_prod_insexpdlbl,
            tv_prod_exeinfolbl, tv_prod_registeronlbl, follwdatelbl;

    public HashMap<String, String> citylist;
    public ArrayList<String> addcitylist;
    public ArrayList<String> ColorValueList;
    public ArrayList<String> makeValueList;
    public ArrayList<String> modelVariantList;
    public ArrayList<String> execValueIdList;
    public ArrayList<String> execValueList;
    public ArrayList<String> manufList;
    public ArrayList<String> mregList;
    public HashMap<String, String> modelList;
    public HashMap<String, String> variantList;

    String executive_id = "", model = "", variant = "";

    public boolean reg;
    int monthpos;
    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate: ");
        setContentView(R.layout.activity_add_proc_lead);
        InitUI();
        OnClickListener();

    }

    protected void InitUI() {
       // MyApplication.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(AddProcLead.this, "dealer_code"), GlobalText.add_proc_leads, GlobalText.android);
        proc_linearBack = (LinearLayout) findViewById(R.id.proc_linearBack);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_close = (ImageView) findViewById(R.id.iv_close);

        prod_leadtypetv = (TextView) findViewById(R.id.prod_leadtypetv);
        prod_vehicletv = (TextView) findViewById(R.id.prod_vehicletv);
        tv_prod_leaddate = (TextView) findViewById(R.id.tv_prod_leaddate);
        tv_prod_leadstatus = (TextView) findViewById(R.id.tv_prod_leadstatus);
        tv_prod_city = (TextView) findViewById(R.id.tv_prod_city);
        tv_add_prod_years = (TextView) findViewById(R.id.tv_add_prod_years);
        tv_add_prod_month = (TextView) findViewById(R.id.tv_add_prod_month);
        tv_add_prod_reg_years = (TextView) findViewById(R.id.tv_add_prod_reg_years);
        tv_add_prod_reg_month = (TextView) findViewById(R.id.tv_add_prod_reg_month);
        tv_prod_color = (TextView) findViewById(R.id.tv_prod_color);
        tv_prod_owner = (TextView) findViewById(R.id.tv_prod_owner);
        tv_prod_leadsource = (TextView) findViewById(R.id.tv_prod_leadsource);
        tv_prod_make = (TextView) findViewById(R.id.tv_prod_make);
        tv_prod_modelVariant = (TextView) findViewById(R.id.tv_prod_modelVariant);
        tv_prod_insurance = (TextView) findViewById(R.id.tv_prod_insurance);
        tv_prod_insuExp = (TextView) findViewById(R.id.tv_prod_insuExp);
        tv_prod_exeInfo = (TextView) findViewById(R.id.tv_prod_exeInfo);
        tv_follwdate = (TextView) findViewById(R.id.tv_follwdate);

        et_prod_custName = (EditText) findViewById(R.id.et_prod_custName);
        et_prod_cusmobile = (EditText) findViewById(R.id.et_prod_cusmobile);
        et_prod_cusemail = (EditText) findViewById(R.id.et_prod_cusemail);
        et_prod_cusaddress = (EditText) findViewById(R.id.et_prod_cusaddress);
        et_prod_pincode = (EditText) findViewById(R.id.et_prod_pincode);
        tv_prod_kms = (EditText) findViewById(R.id.tv_prod_kms);
        et_proc_cust_exp = (EditText) findViewById(R.id.et_proc_cust_exp);
        tv_prod_remark = (EditText) findViewById(R.id.tv_prod_remark);
        tv_prod_reg = (EditText) findViewById(R.id.tv_prod_reg);
        et_prod_refsource = (EditText) findViewById(R.id.et_prod_refsource);
        et_prod_stockVin = (EditText) findViewById(R.id.et_prod_stockVin);
        tv_prodRemark = (EditText) findViewById(R.id.tv_prodRemark);

        tv_Expline = (TextView) findViewById(R.id.tv_Expline);

        tv_prod_leadtypevaild = (TextView) findViewById(R.id.tv_prod_leadtypevaild);
        tv_prod_vehicletypevaild = (TextView) findViewById(R.id.tv_prod_vehicletypevaild);
        tv_prod_leaddatevaild = (TextView) findViewById(R.id.tv_prod_leaddatevaild);
        tv_prod_leadstatusvaild = (TextView) findViewById(R.id.tv_prod_leadstatusvaild);
        tv_prod_Cusnamevaild = (TextView) findViewById(R.id.tv_prod_Cusnamevaild);
        tv_prod_numbervalid = (TextView) findViewById(R.id.tv_prod_numbervalid);
        tv_prod_emailvaild = (TextView) findViewById(R.id.tv_prod_emailvaild);
        tv_prod_CusaddressValid = (TextView) findViewById(R.id.tv_prod_CusaddressValid);
        tv_prod_cityvaild = (TextView) findViewById(R.id.tv_prod_cityvaild);
        tv_prod_yearvalid = (TextView) findViewById(R.id.tv_prod_yearvalid);
        tv_prod_month_error = (TextView) findViewById(R.id.tv_prod_month_error);
        tv_prod_reg_yearvalid = (TextView) findViewById(R.id.tv_prod_reg_yearvalid);
        tv_prod_reg_month_error = (TextView) findViewById(R.id.tv_prod_reg_month_error);
        tv_prod_colorvaild = (TextView) findViewById(R.id.tv_prod_colorvaild);
        tv_prod_kms_error = (TextView) findViewById(R.id.tv_prod_kms_error);
        tv_prod_owner_error = (TextView) findViewById(R.id.tv_prod_owner_error);
        tv_prod_makevaild = (TextView) findViewById(R.id.tv_prod_makevaild);
        tv_prod_modelvariantvaild = (TextView) findViewById(R.id.tv_prod_modelvariantvaild);
        tv_prod_cusbudgetvaild = (TextView) findViewById(R.id.tv_prod_cusbudgetvaild);
        tv_prod_reg_error = (TextView) findViewById(R.id.tv_prod_reg_error);
        tv_prod_insu_error = (TextView) findViewById(R.id.tv_prod_insu_error);
        tv_prod_insuexp_error = (TextView) findViewById(R.id.tv_prod_insuexp_error);
        tv_prod_exeInfoValid = (TextView) findViewById(R.id.tv_prod_exeInfoValid);
        tv_followupdatevaild = (TextView) findViewById(R.id.tv_followupdatevaild);

        prod_addcancel = (Button) findViewById(R.id.prod_addcancel);
        add_proc_btn = (Button) findViewById(R.id.add_proc_btn);

        String star = "<font color='#B40404'>*</font>";
        tv_prod_leadDatelbl = (TextView) findViewById(R.id.tv_prod_leadDatelbl);
        tv_prod_leadStatuslbl = (TextView) findViewById(R.id.tv_prod_leadStatuslbl);
        tv_prod_cusName = (TextView) findViewById(R.id.tv_prod_cusName);
        tv_prod_cusmobilelbl = (TextView) findViewById(R.id.tv_prod_cusmobilelbl);
        tv_prod_cusemaillbl = (TextView) findViewById(R.id.tv_prod_cusemaillbl);
        tv_prod_cusaddresslbl = (TextView) findViewById(R.id.tv_prod_cusaddresslbl);
        tv_prod_citylbl = (TextView) findViewById(R.id.tv_prod_citylbl);
        tv_prod_manufacturedonlbl = (TextView) findViewById(R.id.tv_prod_manufacturedonlbl);
        tv_prod_colorlbl = (TextView) findViewById(R.id.tv_prod_colorlbl);
        tv_prod_kmslbl = (TextView) findViewById(R.id.tv_prod_kmslbl);
        tv_prod_ownerlbl = (TextView) findViewById(R.id.tv_prod_ownerlbl);
        tv_prod_makelbl = (TextView) findViewById(R.id.tv_prod_makelbl);
        tv_prod_modellbl = (TextView) findViewById(R.id.tv_prod_modellbl);
        tv_prod_cusbudgetlbl = (TextView) findViewById(R.id.tv_prod_cusbudgetlbl);
        tv_prod_reglbl = (TextView) findViewById(R.id.tv_prod_reglbl);
        tv_prod_insulbl = (TextView) findViewById(R.id.tv_prod_insulbl);
        tv_prod_insexpdlbl = (TextView) findViewById(R.id.tv_prod_insexpdlbl);
        tv_prod_exeinfolbl = (TextView) findViewById(R.id.tv_prod_exeinfolbl);
        tv_prod_registeronlbl = (TextView) findViewById(R.id.tv_prod_registeronlbl);
        follwdatelbl = (TextView) findViewById(R.id.follwdatelbl);

        tv_prod_leadDatelbl.setText(Html.fromHtml("LEAD DATE " + star));
        tv_prod_leadStatuslbl.setText(Html.fromHtml("LEAD STATUS " + star));
        tv_prod_cusName.setText(Html.fromHtml("CUSTOMER NAME " + star));
        tv_prod_cusmobilelbl.setText(Html.fromHtml("MOBILE " + star));
        tv_prod_cusemaillbl.setText(Html.fromHtml("CUSTOMER EMAIL ID " + star));
        // tv_prod_cusaddresslbl.setText(Html.fromHtml("CUSTOMER ADDRESS " + star));
        tv_prod_citylbl.setText(Html.fromHtml("CITY " + star));
        tv_prod_manufacturedonlbl.setText(Html.fromHtml("MANUFACTURED ON " + star));
        tv_prod_colorlbl.setText(Html.fromHtml("COLOR " + star));
        tv_prod_kmslbl.setText(Html.fromHtml("KMS " + star));
        tv_prod_ownerlbl.setText(Html.fromHtml("OWNER " + star));
        tv_prod_makelbl.setText(Html.fromHtml("MAKE " + star));
        tv_prod_modellbl.setText(Html.fromHtml("MODEL AND VARIANT " + star));
        tv_prod_cusbudgetlbl.setText(Html.fromHtml("CUSTOMER EXPECTATION " + star));
        tv_prod_reglbl.setText(Html.fromHtml("REGISTRATION NUMBER " + star));
        tv_prod_insulbl.setText(Html.fromHtml("INSURANCE " + star));
        //tv_prod_insexpdlbl.setText(Html.fromHtml("INS. EXPIRY DATE " + star));
        tv_prod_exeinfolbl.setText(Html.fromHtml("EXECUTIVE INFORMATION " + star));
        tv_prod_registeronlbl.setText(Html.fromHtml("REGISTERED ON " + star));
        follwdatelbl.setText(Html.fromHtml("FOLLOW DATE " + star));


        // Registration Validation ==============================
        tv_prod_reg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String value = s.toString();

                if (value.length() == 1) {
                    reg = value.substring(0).matches("\\d+(?:\\.\\d+)?");
                    if (reg) {

                        tv_prod_reg_error.setVisibility(View.VISIBLE);
                        tv_prod_reg_error.setText("Please enter correct format.");

                        tv_prod_reg.setText("");

                        CommonMethods.alertMessage(AddProcLead.this, "Please enter correct format.");
                    } else {
                        tv_prod_reg_error.setVisibility(View.GONE);
                    }

                } else if (value.length() >= 1) {
                    reg = value.substring(1).matches("\\d+(?:\\.\\d+)?");
                    if (reg) {

                        tv_prod_reg_error.setVisibility(View.VISIBLE);
                        tv_prod_reg_error.setText("Please enter correct format.");
                        tv_prod_reg.setText("");

                        CommonMethods.alertMessage(AddProcLead.this, "Please enter correct format.");
                    } else {
                        if (!reg)
                            tv_prod_reg_error.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


    }

    private void OnClickListener() {
        prod_addcancel.setOnClickListener(this::onClick);
        add_proc_btn.setOnClickListener(this::onClick);
        iv_back.setOnClickListener(this::onClick);
        iv_close.setOnClickListener(this::onClick);
        tv_follwdate.setOnClickListener(this::onClick);

        prod_vehicletv.setOnClickListener(this::onClick);
        tv_prod_leadstatus.setOnClickListener(this::onClick);
        tv_prod_city.setOnClickListener(this::onClick);
        tv_prod_leadsource.setOnClickListener(this::onClick);
        tv_prod_leaddate.setOnClickListener(this::onClick);
        tv_add_prod_years.setOnClickListener(this::onClick);
        tv_add_prod_month.setOnClickListener(this::onClick);
        tv_add_prod_reg_years.setOnClickListener(this::onClick);
        tv_add_prod_reg_month.setOnClickListener(this::onClick);
        tv_prod_color.setOnClickListener(this::onClick);
        tv_prod_owner.setOnClickListener(this::onClick);
        tv_prod_leadsource.setOnClickListener(this::onClick);
        tv_prod_make.setOnClickListener(this::onClick);
        tv_prod_modelVariant.setOnClickListener(this::onClick);
        tv_prod_insurance.setOnClickListener(this::onClick);
        tv_prod_insuExp.setOnClickListener(this::onClick);
        tv_prod_exeInfo.setOnClickListener(this::onClick);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.prod_addcancel:
                finish();
                break;

            case R.id.add_proc_btn:
                isValidation();
                break;

            case R.id.iv_back:
                finish();
                break;

            case R.id.iv_close:
                finish();
                break;

            case R.id.tv_follwdate:
                if (!tv_prod_leaddate.getText().toString().equals("")) {
                    setDate(v, "followDate");
                } else {
                    Toast.makeText(AddProcLead.this, "Please Select the Lead Date ", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.prod_vehicletv:
                String[] vehlist = {"Private", "Commercial"};
                setAlertDialog(v, this, "Vehicle", vehlist);
                break;

            case R.id.tv_prod_leadstatus:
                // String[] leadStatus = {"Hot", "Warm", "Cold", "Lost"};
                String[] leadStatus = MainActivity.statusList.toArray(new String[0]);
                setAlertDialog(v, this, "Lead Status", leadStatus);
                break;

            case R.id.tv_prod_city:
                if (CommonMethods.isInternetWorking(this)) {
                    fetchCity("City", "citylist", AddProcLead.this);
                } else {
                    CommonMethods.alertMessage(this, GlobalText.CHECK_NETWORK_CONNECTION);
                }
                break;

            case R.id.tv_prod_leadsource:
                String[] leadSource = {"Walk-In", "Hoarding", "Event", "Newspaper", "Internet", "Reference"};
                setAlertDialog(v, this, "Lead Source", leadSource);
                break;

            case R.id.tv_prod_leaddate:
                setDate(v, "leadDate");
                break;

            case R.id.tv_add_prod_years:
                if (CommonMethods.isInternetWorking(this)) {
                    tv_add_prod_month.setText("");
                    try {
                        VolleyApi("Manufactured year");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                } else {
                    CommonMethods.alertMessage(AddProcLead.this, GlobalText.CHECK_NETWORK_CONNECTION);
                }
                break;

            case R.id.tv_add_prod_month:
                if (!tv_add_prod_years.getText().toString().trim().equalsIgnoreCase("")) {
                    Calendar calen = Calendar.getInstance();
                    int Manuyear = calen.get(Calendar.YEAR);
                    int Manumonth = calen.get(Calendar.MONTH);
                    if (tv_add_prod_years.getText().toString().equalsIgnoreCase(Manuyear + "")) {
                        ArrayList<String> monthlist = new ArrayList<>();
                        monthlist.addAll(Arrays.asList(MonthUtility.showMonthsTillDate(Manumonth)));
                        SelectPopupList("Manufactured Month", monthlist);
                    } else {
                        ArrayList<String> monthlist = new ArrayList<>();
                        monthlist.addAll(Arrays.asList(MonthUtility.monthlist));
                        SelectPopupList("Manufactured Month", monthlist);
                    }
                } else {
                    Toast.makeText(AddProcLead.this, "Please select manufactured year", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.tv_add_prod_reg_years:
                if (!tv_add_prod_years.getText().toString().trim().equalsIgnoreCase("")) {
                    if (CommonMethods.isInternetWorking(this)) {
                        tv_add_prod_reg_month.setText("");
                        VolleyApi("Register year");
                    } else {
                        CommonMethods.alertMessage(AddProcLead.this, GlobalText.CHECK_NETWORK_CONNECTION);
                    }
                } else {
                    Toast.makeText(AddProcLead.this, "Please select manufactured date", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.tv_add_prod_reg_month:
                if (!tv_add_prod_reg_years.getText().toString().equalsIgnoreCase("")) {
                    Calendar calendar = Calendar.getInstance();
                    int year = calendar.get(Calendar.YEAR);
                    int month = calendar.get(Calendar.MONTH);
                    if (tv_add_prod_reg_years.getText().toString().equalsIgnoreCase(year + "")) {
                        ArrayList<String> monthlistreg = new ArrayList<>();
                        monthlistreg.addAll(Arrays.asList(MonthUtility.showMonthsTillDate(month)));
                        SelectPopupList("Register Month", monthlistreg);
                    } else {
                        ArrayList<String> monthlistreg = new ArrayList<>();
                        if (tv_add_prod_years.getText().toString().trim().equalsIgnoreCase(tv_add_prod_reg_years.getText().toString().trim())) {
                            for (int i = 0; i < MonthUtility.monthlist.length; i++) {
                                if (i >= monthpos) {
                                    monthlistreg.addAll(Arrays.asList(MonthUtility.monthlist[i]));
                                }
                            }
                            //  SelectPopupList("Register Month", monthlistreg);

                        } else {
                            //  ArrayList<String> monthlistreg = new ArrayList<>();
                            monthlistreg.addAll(Arrays.asList(MonthUtility.monthlist));
                        }
                        SelectPopupList("Register Month", monthlistreg);
                    }
                } else {
                    Toast.makeText(AddProcLead.this, "Please select registered year", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.tv_prod_color:
                if (CommonMethods.isInternetWorking(this)) {
                    VolleyApi("Color");
                } else {
                    CommonMethods.alertMessage(AddProcLead.this, GlobalText.CHECK_NETWORK_CONNECTION);
                }
                break;

            case R.id.tv_prod_owner:
                String[] ownerlist = {"1", "2", "3", "4", "5"};
                setAlertDialog(v, this, "Owner", ownerlist);
                break;

            case R.id.tv_prod_make:
                if (CommonMethods.isInternetWorking(AddProcLead.this)) {
                    fetchCommercialMakeList(AddProcLead.this);
                } else {
                    CommonMethods.alertMessage(AddProcLead.this, GlobalText.CHECK_NETWORK_CONNECTION);
                }
                break;

            case R.id.tv_prod_modelVariant:
                String make = tv_prod_make.getText().toString().trim();
                if (!make.equalsIgnoreCase("")) {
                    if (CommonMethods.isInternetWorking(AddProcLead.this)) {
                        fetchModelVariant(make, AddProcLead.this);
                    } else {
                        CommonMethods.alertMessage(AddProcLead.this, GlobalText.CHECK_NETWORK_CONNECTION);
                    }
                } else {
                    Toast.makeText(this, "Please select make", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.tv_prod_insurance:
                tv_prod_insuExp.setText("");
                String[] insuranceList = {"Comprehensive", "Third Party", "NA"};
                setAlertDialog(v, this, "Insurance", insuranceList);
                break;

            case R.id.tv_prod_insuExp:
                if (tv_prod_insurance.getText().toString().trim().equals("")) {
                    Toast.makeText(this, "Please select insurance", Toast.LENGTH_SHORT).show();
                } else {
                    setDate(v, "insuExp");
                }
                break;

            case R.id.tv_prod_exeInfo:
                if (CommonMethods.isInternetWorking(this)) {
                    VolleyApi("Executive");
                } else {
                    CommonMethods.alertMessage(AddProcLead.this, GlobalText.CHECK_NETWORK_CONNECTION);
                }
                break;

        }
    }

    private void isValidation() {
        String registrationValid = "[A-Z]{2}[0-9]{2}[A-Z]{2}[0-9]{4}";
        if (!prod_leadtypetv.getText().toString().equalsIgnoreCase("") &&
                !prod_vehicletv.getText().toString().equalsIgnoreCase("") &&
                !tv_prod_leaddate.getText().toString().equalsIgnoreCase("") &&
                !tv_prod_leadstatus.getText().toString().equalsIgnoreCase("") &&
                !et_prod_custName.getText().toString().equalsIgnoreCase("") &&
                !et_prod_cusmobile.getText().toString().equalsIgnoreCase("") &&
                !et_prod_cusemail.getText().toString().equalsIgnoreCase("") &&
                Patterns.EMAIL_ADDRESS.matcher(et_prod_cusemail.getText().toString().trim()).matches() &&
                // !et_prod_cusaddress.getText().toString().equalsIgnoreCase("") &&
                !tv_prod_city.getText().toString().equalsIgnoreCase("") &&
                !tv_add_prod_years.getText().toString().equalsIgnoreCase("") &&
                !tv_add_prod_month.getText().toString().equalsIgnoreCase("") &&
                !tv_add_prod_reg_years.getText().toString().equalsIgnoreCase("") &&
                !tv_add_prod_reg_month.getText().toString().equalsIgnoreCase("") &&
                !tv_prod_color.getText().toString().equalsIgnoreCase("") &&
                !tv_prod_kms.getText().toString().equalsIgnoreCase("") &&
                !tv_prod_owner.getText().toString().equalsIgnoreCase("") &&
                !tv_prod_make.getText().toString().equalsIgnoreCase("") &&
                !tv_prod_modelVariant.getText().toString().equalsIgnoreCase("") &&
                !et_proc_cust_exp.getText().toString().equalsIgnoreCase("") &&
                //!tv_prod_remark.getText().toString().equalsIgnoreCase("") &&
                !tv_prod_reg.getText().toString().equalsIgnoreCase("") &&
                // tv_prod_reg.getText().toString().matches(registrationValid) &&
                !tv_prod_insurance.getText().toString().equalsIgnoreCase("") &&
                // !tv_prod_insuExp.getText().toString().equalsIgnoreCase("") &&
                !tv_prod_exeInfo.getText().toString().equalsIgnoreCase("") &&
                !tv_follwdate.getText().toString().equalsIgnoreCase("")) {

            if (tv_prod_reg.length() < 5) {

                if (tv_prod_reg.length() < 5) {
                    tv_prod_reg_error.setVisibility(View.VISIBLE);
                    tv_prod_reg_error.setText("Invalid Registration No.");
                    return;
                } else {

                    tv_prod_reg_error.setVisibility(View.INVISIBLE);

                }

            }

            if(tv_prod_reg.getText().toString().matches("[a-zA-Z ]+")){
                tv_prod_reg_error.setVisibility(View.VISIBLE);
                tv_prod_reg_error.setText("Invalid Registration No.");
                return;
            }else{
                tv_prod_reg_error.setVisibility(View.INVISIBLE);
            }


            // Retrofit Request
            AddRequest();

        } else {

            if (prod_leadtypetv.getText().toString().equalsIgnoreCase("")) {
                tv_prod_leadtypevaild.setVisibility(View.VISIBLE);
            } else {
                tv_prod_leadtypevaild.setVisibility(View.GONE);
            }
            if (prod_vehicletv.getText().toString().equalsIgnoreCase("")) {
                tv_prod_vehicletypevaild.setVisibility(View.VISIBLE);
            } else {
                tv_prod_vehicletypevaild.setVisibility(View.GONE);
            }
            if (tv_prod_leaddate.getText().toString().equalsIgnoreCase("")) {
                tv_prod_leaddatevaild.setVisibility(View.VISIBLE);
            } else {
                tv_prod_leaddatevaild.setVisibility(View.GONE);
            }

            if (tv_prod_leadstatus.getText().toString().equalsIgnoreCase("")) {
                tv_prod_leadstatusvaild.setVisibility(View.VISIBLE);
            } else {
                tv_prod_leadstatusvaild.setVisibility(View.GONE);
            }

            if (et_prod_custName.getText().toString().equalsIgnoreCase("")) {
                tv_prod_Cusnamevaild.setVisibility(View.VISIBLE);
            } else {
                tv_prod_Cusnamevaild.setVisibility(View.GONE);
            }

            if (et_prod_cusmobile.getText().toString().equalsIgnoreCase("")) {
                tv_prod_numbervalid.setVisibility(View.VISIBLE);
            } else {
                tv_prod_numbervalid.setVisibility(View.GONE);
            }

            if (et_prod_cusemail.getText().toString().equalsIgnoreCase("")) {
                tv_prod_emailvaild.setVisibility(View.VISIBLE);
            } else {
                tv_prod_emailvaild.setVisibility(View.GONE);
            }

            if (!Patterns.EMAIL_ADDRESS.matcher(et_prod_cusemail.getText().toString()).matches()) {
                tv_prod_emailvaild.setVisibility(View.VISIBLE);
            } else {
                tv_prod_cityvaild.setVisibility(View.GONE);
            }

           /* if (et_prod_cusaddress.getText().toString().equalsIgnoreCase("")) {
                tv_prod_CusaddressValid.setVisibility(View.VISIBLE);
            } else {
                tv_prod_CusaddressValid.setVisibility(View.GONE);
            }*/

            if (tv_prod_city.getText().toString().equalsIgnoreCase("")) {
                tv_prod_cityvaild.setVisibility(View.VISIBLE);
            } else {
                tv_prod_cityvaild.setVisibility(View.GONE);
            }

            if (tv_add_prod_years.getText().toString().equalsIgnoreCase("")) {
                tv_prod_yearvalid.setVisibility(View.VISIBLE);
            } else {
                tv_prod_yearvalid.setVisibility(View.GONE);
            }

            if (tv_add_prod_month.getText().toString().equalsIgnoreCase("")) {
                tv_prod_month_error.setVisibility(View.VISIBLE);
            } else {
                tv_prod_month_error.setVisibility(View.GONE);
            }

            if (tv_add_prod_reg_years.getText().toString().equalsIgnoreCase("")) {
                tv_prod_reg_yearvalid.setVisibility(View.VISIBLE);
            } else {
                tv_prod_reg_yearvalid.setVisibility(View.GONE);
            }
            if (tv_add_prod_reg_month.getText().toString().equalsIgnoreCase("")) {
                tv_prod_reg_month_error.setVisibility(View.VISIBLE);
            } else {
                tv_prod_reg_month_error.setVisibility(View.GONE);
            }

            if (tv_prod_color.getText().toString().equalsIgnoreCase("")) {
                tv_prod_colorvaild.setVisibility(View.VISIBLE);
            } else {
                tv_prod_colorvaild.setVisibility(View.GONE);
            }

            if (tv_prod_kms.getText().toString().equalsIgnoreCase("")) {
                tv_prod_kms_error.setVisibility(View.VISIBLE);
            } else {
                tv_prod_kms_error.setVisibility(View.GONE);
            }
            if (tv_prod_owner.getText().toString().equalsIgnoreCase("")) {
                tv_prod_owner_error.setVisibility(View.VISIBLE);
            } else {
                tv_prod_owner_error.setVisibility(View.GONE);
            }
            if (tv_prod_make.getText().toString().equalsIgnoreCase("")) {
                tv_prod_makevaild.setVisibility(View.VISIBLE);
            } else {
                tv_prod_makevaild.setVisibility(View.GONE);
            }
            if (tv_prod_modelVariant.getText().toString().equalsIgnoreCase("")) {
                tv_prod_modelvariantvaild.setVisibility(View.VISIBLE);
            } else {
                tv_prod_modelvariantvaild.setVisibility(View.GONE);
            }
            if (et_proc_cust_exp.getText().toString().equalsIgnoreCase("")) {
                tv_prod_cusbudgetvaild.setVisibility(View.VISIBLE);
            } else {
                tv_prod_cusbudgetvaild.setVisibility(View.GONE);
            }

            if (tv_prod_reg.getText().toString().equalsIgnoreCase("")) {
                tv_prod_reg_error.setVisibility(View.VISIBLE);
                tv_prod_reg_error.setText("This field is required");
            } else {
                tv_prod_reg_error.setVisibility(View.GONE);
            }

            /*if (!tv_prod_reg.getText().toString().matches(registrationValid)) {
                tv_prod_reg_error.setVisibility(View.VISIBLE);
                tv_prod_reg_error.setText("Enter valid registration No. For Ex - KA00AA0000");
            } else {
                tv_prod_reg_error.setVisibility(View.GONE);
            }*/
            if (tv_prod_insurance.getText().toString().equalsIgnoreCase("")) {
                tv_prod_insu_error.setVisibility(View.VISIBLE);
            } else {
                tv_prod_insu_error.setVisibility(View.GONE);
            }
            if (tv_prod_exeInfo.getText().toString().equalsIgnoreCase("")) {
                tv_prod_exeInfoValid.setVisibility(View.VISIBLE);
            } else {
                tv_prod_exeInfoValid.setVisibility(View.GONE);
            }
            if (tv_follwdate.getText().toString().equalsIgnoreCase("")) {
                tv_followupdatevaild.setVisibility(View.VISIBLE);
            } else {
                tv_followupdatevaild.setVisibility(View.GONE);
            }

            if(tv_prod_reg.getText().toString().matches("[a-zA-Z ]+")){
                tv_prod_reg_error.setVisibility(View.VISIBLE);
                tv_prod_reg_error.setText("Invalid Registration No.");
                return;
            }else{
                tv_prod_reg_error.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void setAlertDialog(View v, Activity a, final String strTitle,
                                final String[] arrVal) {
        AlertDialog.Builder alert = new AlertDialog.Builder(a);
        alert.setTitle(strTitle);
        alert.setItems(arrVal, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (strTitle.equals("Vehicle")) {
                    prod_vehicletv.setText(arrVal[which]);
                    tv_prod_make.setText("");
                    tv_prod_modelVariant.setText("");
                } else if (strTitle.equalsIgnoreCase("Lead Status")) {
                    tv_prod_leadstatus.setText(arrVal[which]);
                } else if (strTitle.equalsIgnoreCase("Lead Source")) {
                    tv_prod_leadsource.setText(arrVal[which]);
                } else if (strTitle.equalsIgnoreCase("Insurance")) {
                    tv_prod_insurance.setText(arrVal[which]);
                    if (tv_prod_insurance.getText().toString().equalsIgnoreCase("NA")) {
                        tv_prod_insexpdlbl.setVisibility(View.INVISIBLE);
                        tv_prod_insuExp.setVisibility(View.INVISIBLE);
                        tv_prod_insuexp_error.setVisibility(View.INVISIBLE);
                        tv_Expline.setVisibility(View.INVISIBLE);
                    } else {
                        tv_prod_insexpdlbl.setVisibility(View.VISIBLE);
                        tv_prod_insuExp.setVisibility(View.VISIBLE);
                        tv_Expline.setVisibility(View.VISIBLE);
                    }
                } else if (strTitle.equalsIgnoreCase("Owner")) {
                    tv_prod_owner.setText(arrVal[which]);
                }
            }
        });
        alert.create();
        alert.show();

    }

    public void setDate(View v, String leadDate) {

        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int selectedYear,
                                  int selectedMonth, int selectedDay) {

                myCalendar.set(Calendar.YEAR, selectedYear);
                myCalendar.set(Calendar.MONTH, selectedMonth);
                myCalendar.set(Calendar.DAY_OF_MONTH, selectedDay);

                String strselectedDay = String.valueOf(selectedDay);
                if (strselectedDay.length() == 1) {
                    strselectedDay = "0" + strselectedDay;
                }
                String strselectedMonth = String.valueOf(selectedMonth + 1);
                if (strselectedMonth.length() == 1) {
                    strselectedMonth = "0" + strselectedMonth;
                }
                String strDate = selectedYear + "-" + strselectedMonth + "-"
                        + strselectedDay;

                if (leadDate.equalsIgnoreCase("leadDate")) {
                    tv_prod_leaddate.setText(strDate);
                } else if (leadDate.equalsIgnoreCase("insuExp")) {
                    tv_prod_insuExp.setText(strDate);
                } else if (leadDate.equalsIgnoreCase("followDate")) {
                    tv_follwdate.setText(strDate);
                }
            }
        };

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                datePickerListener, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        if (leadDate.equalsIgnoreCase("followDate")) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = sdf.parse(tv_prod_leaddate.getText().toString().trim());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long millis = date.getTime();
            datePickerDialog.getDatePicker().setMinDate(millis);
        } else if (leadDate.equalsIgnoreCase("leadDate")) {
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        } else if (leadDate.equalsIgnoreCase("insuExp")) {
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        }

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            dialog.dismiss();

                        }
                    }
                });

        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    @SuppressLint("NewApi")
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {

                            DatePicker datePicker = datePickerDialog
                                    .getDatePicker();
                            datePickerListener.onDateSet(datePicker,
                                    datePicker.getYear(),
                                    datePicker.getMonth(),
                                    datePicker.getDayOfMonth());

                        }
                        dialog.dismiss();
                    }
                });

        datePickerDialog.setCancelable(false);
        datePickerDialog.show();
    }
    //Calling city Api

    // Retrofit
    public void fetchCity(String value, String city, final Context mContext) {
        SpinnerManager.showSpinner(mContext);
        addcitylist = new ArrayList<String>();
        citylist = new HashMap<String, String>();
        YearService.getCityFromServer(city, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                try {
                    retrofit2.Response<List<CityMaster>> mResponse = (retrofit2.Response<List<CityMaster>>) obj;
                    List<CityMaster> cityList = mResponse.body();
                    for (int i = 0; i < cityList.size(); i++) {
                        citylist.put(cityList.get(i).getCityname(), String.valueOf(cityList.get(i).getCitycode()));
                        addcitylist.add(cityList.get(i).getCityname());
                    }
                    SelectPopupList(value, addcitylist);
                } catch (Exception e) {

                }
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
            }
        });
    }

    //makelist and commercial-makelist
    public void fetchCommercialMakeList(final Context mContext) {
        SpinnerManager.showSpinner(mContext);
        makeValueList = new ArrayList<>();
        YearService.getMakelistFromServer(new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                tv_prod_modelVariant.setText("");
                try {
                    retrofit2.Response<List<CommercialMakelist>> mResponse = (retrofit2.Response<List<CommercialMakelist>>) obj;

                    List<CommercialMakelist> commercialList = mResponse.body();
                    for (int i = 0; i < commercialList.size(); i++) {
                        makeValueList.add(commercialList.get(i).getMake());
                    }
                    SelectPopupList("Make", makeValueList);

                } catch (Exception e) {
                }
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
            }
        });

    }

    //model details and commercial
    public void fetchModelVariant(String make, Context mContext) {
        modelVariantList = new ArrayList<>();
        modelList = new HashMap<String, String>();
        variantList = new HashMap<String, String>();
        SpinnerManager.showSpinner(mContext);
        YearService.getModelDetailsFromServer(make, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                try {
                    retrofit2.Response<ModelDetails> mResponse = (retrofit2.Response<ModelDetails>) obj;
                    List<ModelValue> modelValues = new ArrayList<>();
                    ModelDetails list = mResponse.body();
                    modelValues = list.getModelValues();
                    for (int i = 0; i < modelValues.size(); i++) {
                        modelVariantList.add(modelValues.get(i).getDisplay());
                        modelList.put(modelValues.get(i).getDisplay(), modelValues.get(i).getModel());
                        variantList.put(modelValues.get(i).getDisplay(), modelValues.get(i).getVariant());
                    }
                    SelectPopupList("Model and Variant", modelVariantList);
                } catch (Exception e) {
                    // Log.e(TAG, "model: " + e.getMessage());
                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                // Log.e(TAG, "model: " + mThrowable.getMessage());
                SpinnerManager.hideSpinner(mContext);
            }
        });
    }


    public void SelectPopupList(String city, ArrayList<String> mlist) {
        final Dialog dialog_data = new Dialog(this, R.style.full_screen_dialog);
        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_data.getWindow().setGravity(Gravity.CENTER);
        dialog_data.setContentView(R.layout.citylist);
        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
        lp_number_picker.copyFrom(window.getAttributes());
        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;
        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);
        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        ImageView backicon = dialog_data.findViewById(R.id.backicon);
        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);

        if (city.equalsIgnoreCase("City")) {
            searchtittle.setText("Select City");
            filterText.setHint("Search City");
        } else if (city.equalsIgnoreCase("Color")) {
            searchtittle.setText("Select Color");
            filterText.setHint("Search Color");
        } else if (city.equalsIgnoreCase("Reg City")) {
            searchtittle.setText("Select Reg City");
            filterText.setHint("Search Reg City");
        } else if (city.equalsIgnoreCase("Make")) {
            searchtittle.setText("Select Make");
            filterText.setHint("Search Make");
        } else if (city.equalsIgnoreCase("Model and Variant")) {
            searchtittle.setText("Select Model and Variant");
            filterText.setHint("Search Model and Variant");
        } else if (city.equalsIgnoreCase("Executive")) {
            searchtittle.setText("Select Executive Info");
            filterText.setHint("Search Executive Info");
        } else if (city.equalsIgnoreCase("Manufactured Month")) {
            searchtittle.setText("Select Manufactured Month");
            filterText.setHint("Search Month");
        } else if (city.equalsIgnoreCase("Year")) {
            searchtittle.setText("Select Manufactured Year");
            filterText.setHint("Search Year");
        } else if (city.equalsIgnoreCase("Register Month")) {
            searchtittle.setText("Select Register Month");
            filterText.setHint("Search Month");
        } else if (city.equalsIgnoreCase("Yearreg")) {
            searchtittle.setText("Select Register Year");
            filterText.setHint("Search Year");
        }

        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mlist);
        alertdialog_Listview.setAdapter(adapter);

        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                String value = a.getAdapter().getItem(position).toString();
                TextView textview = v.findViewById(android.R.id.text1);
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                if (city.equalsIgnoreCase("City")) {
                    tv_prod_city.setText(value);
                } else if (city.equalsIgnoreCase("Color")) {
                    tv_prod_color.setText(value);
                } else if (city.equalsIgnoreCase("Make")) {
                    tv_prod_make.setText(value);
                } else if (city.equalsIgnoreCase("Model and Variant")) {
                    for (Map.Entry<String, String> entry : modelList.entrySet()) {
                        if (value.equalsIgnoreCase(entry.getKey())) {
                            model = entry.getValue();
                        }
                    }
                    for (Map.Entry<String, String> entry : variantList.entrySet()) {
                        if (value.equalsIgnoreCase(entry.getKey())) {
                            variant = entry.getValue();
                        }
                    }
                    tv_prod_modelVariant.setText(value);
                } else if (city.equalsIgnoreCase("Executive")) {
                    executive_id = execValueIdList.get(position);
                    tv_prod_exeInfo.setText(value);
                    for (int i = 0; i < execValueIdList.size(); i++) {
                        String[] splitValues = execValueIdList.get(i).toString().split("@", 10);
                        if (value.equals(splitValues[0])) {
                            executive_id = splitValues[1];
                        }
                    }
                } else if (city.equalsIgnoreCase("Manufactured Month")) {
                    tv_add_prod_month.setText(value);
                    //  monthpos = position;
                    Date date = null;
                    try {
                        date = new SimpleDateFormat("MMMM").parse(value);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    monthpos = cal.get(Calendar.MONTH);
                    tv_add_prod_reg_month.setText("");
                } else if (city.equalsIgnoreCase("Year")) {
                    tv_add_prod_years.setText(value);
                    tv_add_prod_reg_years.setText("");
                    /*if (Integer.parseInt(tv_add_prod_reg_years.getText().toString().trim()) >= Integer.parseInt(tv_add_prod_years.getText().toString())) {
                        tv_add_prod_reg_years.setText("");
                    }*/
                } else if (city.equalsIgnoreCase("Yearreg")) {
                    if (Long.parseLong(tv_add_prod_years.getText().toString()) <= Long.parseLong(value)) {
                        tv_add_prod_reg_years.setText(value);
                    } else {
                        tv_add_prod_reg_years.setText("");
                        Toast.makeText(AddProcLead.this, "Register Year Should be equal or greater than manufacturing year ", Toast.LENGTH_LONG).show();
                    }
                } else if (city.equalsIgnoreCase("Register Month")) {
                    tv_add_prod_reg_month.setText(value);
                }

                if (dialog_data != null) {
                    dialog_data.dismiss();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });

        backicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_data.dismiss();
            }
        });
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                }
            }
        });
        dialog_data.show();
    }

    private void VolleyApi(String value) {
        String url = "";
        if (value.equalsIgnoreCase("Color")) {
            ColorValueList = new ArrayList<>();
            url = Global_Urls.addstock + "colour";
        } else if (value.equalsIgnoreCase("Executive")) {
            execValueIdList = new ArrayList<>();
            execValueList = new ArrayList<>();
            url = Global.stock_dealerURL + "dealer/active-executive-list/procurement";
        } else if (value.equalsIgnoreCase("Manufactured year")) {
            manufList = new ArrayList<>();
            url = Global_Urls.addstock + "years";
        } else if (value.equalsIgnoreCase("Register year")) {
            mregList = new ArrayList<>();
            url = Global_Urls.addstock + "years";
        }

        SpinnerManager.showSpinner(AddProcLead.this);
        JsonArrayRequest strReq = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                SpinnerManager.hideSpinner(AddProcLead.this);
                for (int i = 0; i < response.length(); i++) {
                    try {
                        if (value.equalsIgnoreCase("Color")) {
                            JSONObject data = response.getJSONObject(i);
                            ColorValueList.add(data.getString("colour"));
                        } else if (value.equalsIgnoreCase("Executive")) {
                            JSONObject data = response.getJSONObject(i);
                            execValueList.add(data.getString("Text"));
                            execValueIdList.add(data.getString("Text") + "@" + data.getString("Value"));
                        } else if (value.equalsIgnoreCase("Manufactured year")) {
                            manufList.add(response.getString(i));
                        } else if (value.equalsIgnoreCase("Register year")) {
                            if (Integer.parseInt(response.getString(i)) >= Integer.parseInt(tv_add_prod_years.getText().toString()))
                                mregList.add(response.getString(i));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (value.equalsIgnoreCase("Color")) {
                    SelectPopupList("Color", ColorValueList);
                } else if (value.equalsIgnoreCase("Executive")) {
                    SelectPopupList("Executive", execValueList);
                } else if (value.equalsIgnoreCase("Manufactured year")) {
                    SelectPopupList("Year", manufList);
                } else if (value.equalsIgnoreCase("Register year")) {
                    SelectPopupList("Yearreg", mregList);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                SpinnerManager.hideSpinner(AddProcLead.this);
               /* ProgressDialogsdismiss();
                try {
                    error_popup2(error.networkResponse.statusCode);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json; charset=utf-8");
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("token", CommonMethods.getstringvaluefromkey(AddProcLead.this, "token"));
                //  Log.e("token ", "requestMethods " + CommonMethods.getstringvaluefromkey(AddProcLead.this, "token").toString());

                return headers;
            }
        };
        Application.getInstance().addToRequestQueue(strReq);

    }

    //Add Retrofit Request

    private void AddRequest() {
        AddProcLeadRequest mprocLead = new AddProcLeadRequest();
        mprocLead.setLeadType(prod_leadtypetv.getText().toString().trim());
        mprocLead.setVehicleType(prod_vehicletv.getText().toString().trim());
        mprocLead.setLeadDate(tv_prod_leaddate.getText().toString().trim());
        mprocLead.setLeadStatus(tv_prod_leadstatus.getText().toString().trim());
        mprocLead.setCustomerName(et_prod_custName.getText().toString().trim());
        mprocLead.setCustomerMobile(et_prod_cusmobile.getText().toString().trim());
        mprocLead.setCustomerEmail(et_prod_cusemail.getText().toString().trim());
        mprocLead.setCustomerAddress(et_prod_cusaddress.getText().toString().trim());
        mprocLead.setCity(tv_prod_city.getText().toString().trim());

        //Dublicate Registeryear , month passing and state
        mprocLead.setRegisterYear(tv_add_prod_reg_years.getText().toString().trim());
        mprocLead.setRegisterMonth(MonthUtility.getMonthNumber(tv_add_prod_reg_month.getText().toString().trim()));
        //mprocLead.setState("Karnataka");

        mprocLead.setManufacturingYear(tv_add_prod_years.getText().toString().trim());

        //mprocLead.setManufacturingMonth(tv_add_prod_month.getText().toString().trim());
        mprocLead.setManufacturingMonth(MonthUtility.getMonthNumber(tv_add_prod_month.getText().toString().trim()));

        mprocLead.setColor(tv_prod_color.getText().toString().trim());
        mprocLead.setKilometer(tv_prod_kms.getText().toString().trim());
        mprocLead.setOwner(tv_prod_owner.getText().toString().trim());
        mprocLead.setMake(tv_prod_make.getText().toString().trim());
        mprocLead.setModel(model);
        mprocLead.setVariant(variant);
        mprocLead.setCustomerBudget(et_proc_cust_exp.getText().toString().trim());
        mprocLead.setBuyingQuote(tv_prod_remark.getText().toString().trim());
        mprocLead.setRegisterNo(tv_prod_reg.getText().toString().trim());
        mprocLead.setInsuranceType(tv_prod_insurance.getText().toString().trim());

        if (!tv_prod_insurance.getText().toString().trim().equals("NA")) {
            mprocLead.setInsuranceExpiry(tv_prod_insuExp.getText().toString().trim());
        } else {
            mprocLead.setInsuranceExpiry("");
        }
        mprocLead.setExecutiveName(tv_prod_exeInfo.getText().toString().trim());
        mprocLead.setSalesExecutiveId(executive_id);
        mprocLead.setRefSrc(et_prod_refsource.getText().toString().trim());
        mprocLead.setStockVinno(et_prod_stockVin.getText().toString().trim());
        mprocLead.setRemark(tv_prodRemark.getText().toString().trim());
        mprocLead.setPincode(et_prod_pincode.getText().toString().trim());
        mprocLead.setLeadSource(tv_prod_leadsource.getText().toString());

       /* DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        mprocLead.setFollowDate(sdf.format(date));*/
        mprocLead.setFollowDate(tv_follwdate.getText().toString().trim());
        mprocLead.setCreatedByDevice("OMS App-android");

        if (CommonMethods.isInternetWorking(this)) {
            addProcurementLead(mprocLead, AddProcLead.this);

        } else {
            CommonMethods.alertMessage(AddProcLead.this, GlobalText.CHECK_NETWORK_CONNECTION);
        }
    }

    //add Procurement lead
    private void addProcurementLead(AddProcLeadRequest mprocLead, Context mContext) {
        SpinnerManager.showSpinner(mContext);

        AddProcLeadSevice.addProcLeadServer(mprocLead, mContext, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                try {
                    retrofit2.Response<AddProcLeadResponse> mResponse = (retrofit2.Response<AddProcLeadResponse>) obj;
                    if (mResponse.body().getStatus().equalsIgnoreCase("SUCCESS")) {
                        Toast.makeText(AddProcLead.this, "Procurement Lead Added Successfully", Toast.LENGTH_LONG).show();
                        finish();
                    } else {
                        Toast.makeText(AddProcLead.this, " " + mResponse.body().getStatus(), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
            }
        });
    }
}
