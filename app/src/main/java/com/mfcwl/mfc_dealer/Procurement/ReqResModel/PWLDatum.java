package com.mfcwl.mfc_dealer.Procurement.ReqResModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PWLDatum {
    @SerializedName("PostedDate")
    @Expose
    private String postedDate;
    @SerializedName("LeadDate")
    @Expose
    private String leadDate;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("dispatchid")
    @Expose
    private Integer dispatchid;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Mobile")
    @Expose
    private String mobile;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("primary_make")
    @Expose
    private String primaryMake;
    @SerializedName("primary_model")
    @Expose
    private String primaryModel;
    @SerializedName("primary_variant")
    @Expose
    private String primaryVariant;
    @SerializedName("secondary_make")
    @Expose
    private String secondaryMake;
    @SerializedName("secondary_model")
    @Expose
    private String secondaryModel;
    @SerializedName("secondary_variant")
    @Expose
    private String secondaryVariant;
    @SerializedName("regno")
    @Expose
    private String regno;
    @SerializedName("regmonth")
    @Expose
    private String regmonth;
    @SerializedName("regyear")
    @Expose
    private String regyear;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("kms")
    @Expose
    private Integer kms;
    @SerializedName("mfg_year")
    @Expose
    private String mfgYear;
    @SerializedName("owners")
    @Expose
    private Integer owners;
    @SerializedName("register_city")
    @Expose
    private String registerCity;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("FollowUpDate")
    @Expose
    private String followUpDate;
    @SerializedName("ExecutiveName")
    @Expose
    private String executiveName;
    @SerializedName("lead_type")
    @Expose
    private String leadType;

    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public String getLeadDate() {
        return leadDate;
    }

    public void setLeadDate(String leadDate) {
        this.leadDate = leadDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDispatchid() {
        return dispatchid;
    }

    public void setDispatchid(Integer dispatchid) {
        this.dispatchid = dispatchid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPrimaryMake() {
        return primaryMake;
    }

    public void setPrimaryMake(String primaryMake) {
        this.primaryMake = primaryMake;
    }

    public String getPrimaryModel() {
        return primaryModel;
    }

    public void setPrimaryModel(String primaryModel) {
        this.primaryModel = primaryModel;
    }

    public String getPrimaryVariant() {
        return primaryVariant;
    }

    public void setPrimaryVariant(String primaryVariant) {
        this.primaryVariant = primaryVariant;
    }

    public String getSecondaryMake() {
        return secondaryMake;
    }

    public void setSecondaryMake(String secondaryMake) {
        this.secondaryMake = secondaryMake;
    }

    public String getSecondaryModel() {
        return secondaryModel;
    }

    public void setSecondaryModel(String secondaryModel) {
        this.secondaryModel = secondaryModel;
    }

    public String getSecondaryVariant() {
        return secondaryVariant;
    }

    public void setSecondaryVariant(String secondaryVariant) {
        this.secondaryVariant = secondaryVariant;
    }

    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getRegmonth() {
        return regmonth;
    }

    public void setRegmonth(String regmonth) {
        this.regmonth = regmonth;
    }

    public String getRegyear() {
        return regyear;
    }

    public void setRegyear(String regyear) {
        this.regyear = regyear;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getKms() {
        return kms;
    }

    public void setKms(Integer kms) {
        this.kms = kms;
    }

    public String getMfgYear() {
        return mfgYear;
    }

    public void setMfgYear(String mfgYear) {
        this.mfgYear = mfgYear;
    }

    public Integer getOwners() {
        return owners;
    }

    public void setOwners(Integer owners) {
        this.owners = owners;
    }

    public String getRegisterCity() {
        return registerCity;
    }

    public void setRegisterCity(String registerCity) {
        this.registerCity = registerCity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getFollowUpDate() {
        return followUpDate;
    }

    public void setFollowUpDate(String followUpDate) {
        this.followUpDate = followUpDate;
    }

    public String getExecutiveName() {
        return executiveName;
    }

    public void setExecutiveName(String executiveName) {
        this.executiveName = executiveName;
    }

    public String getLeadType() {
        return leadType;
    }

    public void setLeadType(String leadType) {
        this.leadType = leadType;
    }

}
