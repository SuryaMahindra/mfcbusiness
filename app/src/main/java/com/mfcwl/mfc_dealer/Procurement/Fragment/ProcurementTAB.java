package com.mfcwl.mfc_dealer.Procurement.Fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mfcwl.mfc_dealer.Activity.Leads.LeadsInstance;
import com.mfcwl.mfc_dealer.Activity.Leads.PrivateLeadsConstant;
import com.mfcwl.mfc_dealer.Activity.Leads.WebLeadsConstant;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Procurement.Activity.ProcurementFilterBy;
import com.mfcwl.mfc_dealer.Procurement.Activity.ProcurementSortBy;
import com.mfcwl.mfc_dealer.Procurement.Instances.ProcLeadFilterSaveInstance;
import com.mfcwl.mfc_dealer.Procurement.ProcInterface.PFilterApply;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GAConstants;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import java.lang.ref.WeakReference;

import rdm.PowerBIFragment;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.activity;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.iv_toggle_back_btn;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.layout_back_btn;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.linearLayoutVisible;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.navigation;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchImage;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchicon;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toggle;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toolbar;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.tv_header_title;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ASM;

public class ProcurementTAB extends Fragment implements TabLayout.OnTabSelectedListener,
        View.OnClickListener {

    LinearLayout ll_asm_back_btn,ll_headerdata_proc, ll_proc_sortBy, ll_proc_filter;
    ImageView iv_back_asm_procurement_tab,iv_proc_sortBy, iv_proc_filter;
    TabLayout procTab;
    ViewPager proc_viewpager;

    public static String LeadType = "";

    private final SparseArray<WeakReference<Fragment>> instantiatedFragments = new SparseArray<>();
    private SharedPreferences mSharedPreferences = null;
    private PFilterApply filterApply;
    String TAG = getClass().getSimpleName();
    private String dealerName, dealerCode;

    public ProcurementTAB() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null)
        {
            dealerCode = getArguments().getString("DCODE");
            dealerName = getArguments().getString("DNAME");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.fragment_procurement_tab, container, false);
        toolbar.setBackgroundColor(getResources().getColor(R.color.black));

        if(CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(USERTYPE_ASM))
        {
            linearLayoutVisible.setVisibility(View.VISIBLE);
            searchicon.setVisibility(View.VISIBLE);
            searchImage.setVisibility(View.VISIBLE);

            if(tv_header_title!=null)
            {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Procurement Leads");
                tv_header_title.setTextColor(getResources().getColor(R.color.white));
            }
        }
        else
            {
                if (tv_header_title != null) {
                    tv_header_title.setVisibility(View.VISIBLE);
                    tv_header_title.setText("Procurement Leads");
                }
            }


        InitUI(view);
        onClickListener();
        return view;

    }

    private void InitUI(View view) {
        setHasOptionsMenu(true);
        filterApply = (PFilterApply) getActivity();
        CommonMethods.setvalueAgainstKey(getActivity(), "asm_pages", "procurement_list");

        //ASM //already filter data clear
        if (CommonMethods.getstringvaluefromkey(getActivity(), "procurementFilterClear").equalsIgnoreCase("changes")) {

            CommonMethods.setvalueAgainstKey(getActivity(), "procurementFilterClear", "no");
        }
        iv_back_asm_procurement_tab=view.findViewById(R.id.iv_back_asm_procurement_tab);
        ll_asm_back_btn=view.findViewById(R.id.ll_asm_back_btn);
        if(CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(USERTYPE_ASM))
        {
          //  navigation.setVisibility(View.GONE);
            ll_asm_back_btn.setVisibility(View.VISIBLE);
            iv_back_asm_procurement_tab.setVisibility(View.VISIBLE);

        }
        else
        {
            ll_asm_back_btn.setVisibility(View.GONE);
            iv_back_asm_procurement_tab.setVisibility(View.GONE);
        }

        ll_headerdata_proc = view.findViewById(R.id.ll_headerdata_proc);
        ll_proc_sortBy = view.findViewById(R.id.ll_proc_sortBy);
        ll_proc_filter = view.findViewById(R.id.ll_proc_filter);
        iv_proc_filter = view.findViewById(R.id.iv_proc_filter);
        iv_proc_sortBy = view.findViewById(R.id.iv_proc_sortBy);
        procTab = view.findViewById(R.id.procTab);
        proc_viewpager = view.findViewById(R.id.proc_viewpager);
        proc_viewpager.setAdapter(new ProcurementTAB.LeadsPagerAdapter(getFragmentManager(), procTab.getTabCount()));
        proc_viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(procTab));
        procTab.setupWithViewPager(proc_viewpager);
        procTab.setTabTextColors(Color.parseColor("#aaaaaa"), Color.parseColor("#2d2d2d"));
        procTab.setSelectedTabIndicatorColor(Color.parseColor("#e5e5e5"));

        procTab.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(proc_viewpager));
        if(iv_back_asm_procurement_tab.getVisibility()==View.VISIBLE)
        {
            iv_back_asm_procurement_tab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loadFragment(new PowerBIFragment(activity));
                }
            });

        }
        WebLeadsConstant.getInstance().setWhichleads("AllLeads");
        PrivateLeadsConstant.getInstance().setWhichleads("AllLeads");

        CommonMethods.setvalueAgainstKey(getActivity(), "ProcPrivateLeads", "false");
        CommonMethods.setvalueAgainstKey(getActivity(), "ProcWebLeads", "false");

        CommonMethods.setvalueAgainstKey(getActivity(), "status", "");
        CommonMethods.setvalueAgainstKey(getActivity(), "Pstatus", "WebLeads");

        if (LeadsInstance.getInstance().getLeaddirection().equals("ProcPrivateLeadsPage")) {
            LeadsInstance.getInstance().setLeaddirection("");
            CommonMethods.setvalueAgainstKey(getActivity(), "Pstatus", "PrivateLeads");
            Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"), GlobalText.proc_privateLeads, GlobalText.android);
            proc_viewpager.setCurrentItem(1);
        }

        try {
            ((MainActivity) getActivity()).setProcFragmentRefreshListener(new MainActivity.ProcFragmentRefreshListener() {
                @Override
                public void onRefresh() {
                    // Refresh Your Fragment
                    try {
                        if (CommonMethods.getstringvaluefromkey(getActivity(), "isProcLeadDetails").equalsIgnoreCase("true")) {
                            CommonMethods.setvalueAgainstKey(getActivity(), "isProcLeadDetails", "false");

                            if (CommonMethods.getstringvaluefromkey(getActivity(), "PWLEADS").equalsIgnoreCase("WebLeads")) {
                                CommonMethods.setvalueAgainstKey(getActivity(), "ProcWebLeads", "true");
                            }
                            if (CommonMethods.getstringvaluefromkey(getActivity(), "PWLEADS").equalsIgnoreCase("PrivateLeads")) {
                                CommonMethods.setvalueAgainstKey(getActivity(), "ProcPrivateLeads", "true");
                            }

                            final ViewPager viewPager = view.findViewById(R.id.proc_viewpager);
                            viewPager.setAdapter(new LeadsPagerAdapter(getFragmentManager(), procTab.getTabCount()));
                            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(procTab));

                            if (CommonMethods.getstringvaluefromkey(getActivity(), "PWLEADS").equalsIgnoreCase("WebLeads")) {
                                viewPager.setCurrentItem(0);
                            }
                            if (CommonMethods.getstringvaluefromkey(getActivity(), "PWLEADS").equalsIgnoreCase("PrivateLeads")) {
                                viewPager.setCurrentItem(1);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void onClickListener() {
        procTab.addOnTabSelectedListener(this);
        ll_proc_sortBy.setOnClickListener(this::onClick);
        ll_proc_filter.setOnClickListener(this::onClick);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuItem item = menu.findItem(R.id.notification_bell);
        item.setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        switch (tab.getPosition()) {
            case 0:
                CommonMethods.setvalueAgainstKey(getActivity(), "Pstatus", "WebLeads");
                filterApply.applyPFilter("WebLeads");
                //  proc_viewpager.setCurrentItem(0);
                break;

            case 1:
                CommonMethods.setvalueAgainstKey(getActivity(), "Pstatus", "PrivateLeads");
                filterApply.applyPFilter("PrivateLeads");
                //  proc_viewpager.setCurrentItem(1);
                break;

        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_proc_sortBy:

                if (CommonMethods.getstringvaluefromkey(getActivity(), "PWLEADS").equalsIgnoreCase("WebLeads")) {
                    CommonMethods.setvalueAgainstKey(getActivity(), "ProcWebLeads", "false");
                }
                if (CommonMethods.getstringvaluefromkey(getActivity(), "PWLEADS").equalsIgnoreCase("PrivateLeads")) {
                    CommonMethods.setvalueAgainstKey(getActivity(), "ProcPrivateLeads", "false");
                }

                ProcurementSortBy sortBy = new ProcurementSortBy(getContext(), getActivity());
                sortBy.show();
                break;
            case R.id.ll_proc_filter:

                if (CommonMethods.getstringvaluefromkey(getActivity(), "PWLEADS").equalsIgnoreCase("WebLeads")) {
                    CommonMethods.setvalueAgainstKey(getActivity(), "ProcWebLeads", "false");
                }
                if (CommonMethods.getstringvaluefromkey(getActivity(), "PWLEADS").equalsIgnoreCase("PrivateLeads")) {
                    CommonMethods.setvalueAgainstKey(getActivity(), "ProcPrivateLeads", "false");
                }


                Intent intent = new Intent(getActivity(), ProcurementFilterBy.class);
                startActivityForResult(intent, 400);
                break;
        }
    }

    public class LeadsPagerAdapter extends FragmentStatePagerAdapter {
        private String[] tabTitles = new String[]{" Web Leads ", "Private Leads"};

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }

        public LeadsPagerAdapter(FragmentManager fm, int mNumOfTabs) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new ProcurementWebLeads();
                case 1:
                    return new ProcPrivateLead();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return tabTitles.length;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            final Fragment fragment = (Fragment) super.instantiateItem(container, position);
            instantiatedFragments.put(position, new WeakReference<>(fragment));
            return fragment;
        }

        @Nullable
        public Fragment getFragment(final int position) {
            final WeakReference<Fragment> wr = instantiatedFragments.get(position);
            if (wr != null) {
                return wr.get();
            } else {
                return null;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ProcLeadFilterSaveInstance.getInstance().getSavedatahashmap().size() != 0 ||
                ProcLeadFilterSaveInstance.getInstance().getStatusarray().length() != 0) {

            iv_proc_filter.setBackgroundResource(R.drawable.filter_icon_yellow);
        } else {
            if (CommonMethods.getstringvaluefromkey(getActivity(), "user_type").equalsIgnoreCase("dealer")) {
                iv_proc_filter.setBackgroundResource(R.drawable.filter_48x18);
            } else {
                if (!CommonMethods.getstringvaluefromkey(getActivity(), "leads_status")
                        .equalsIgnoreCase("")
                ||CommonMethods.getstringvaluefromkey(getActivity(), "leadStatus")
                        .equalsIgnoreCase("true")) {
                    iv_proc_filter.setBackgroundResource(R.drawable.filter_icon_yellow);
                }else{
                    iv_proc_filter.setBackgroundResource(R.drawable.filter_48x18);
                }
            }
        }




        if(CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(USERTYPE_ASM))
        {
            toggle.setHomeAsUpIndicator(R.drawable.ic_new_hamburger_menu);
            searchicon.setVisibility(View.VISIBLE);
            searchImage.setVisibility(View.VISIBLE);
            toolbar.setBackgroundColor(getResources().getColor(R.color.black));
            if(tv_header_title!=null)
            {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Procurement Leads");
                tv_header_title.setTextColor(getResources().getColor(R.color.white));
            }

            Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_PROCUREMENT_LEAD,CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));

        }
        else
        {
            searchicon.setVisibility(View.VISIBLE);
            linearLayoutVisible.setVisibility(View.VISIBLE);
            if (tv_header_title != null) {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Procurement Leads");
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);
            if (resultCode == 500) {
                if (!CommonMethods.getstringvaluefromkey(getActivity(), "Pstatus").equalsIgnoreCase(""))
                    filterApply.applyPFilter(CommonMethods.getstringvaluefromkey(getActivity(), "Pstatus"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadFragment(Fragment mFragment) {
        Bundle mBundle = new Bundle();
        mBundle.putString("DNAME", dealerName);
        mBundle.putString("DCODE", dealerCode);
        mFragment.setArguments(mBundle);
        switchContent(R.id.fillLayout, mFragment);
    }

    public void switchContent(int id, Fragment fragment) {
        if (activity == null)
            return;
        if (activity instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) activity;
            Fragment frag = fragment;
            mainActivity.switchContent(id, frag);
        }
    }


}
