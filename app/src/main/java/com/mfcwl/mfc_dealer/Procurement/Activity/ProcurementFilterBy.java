package com.mfcwl.mfc_dealer.Procurement.Activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.Activity.LeadSection.LeadConstantsSection;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Interface.LeadSection.FilterSelected;
import com.mfcwl.mfc_dealer.Model.LeadstatusModel;
import com.mfcwl.mfc_dealer.Popup.LeadSection.CalendarFilterDialog;
import com.mfcwl.mfc_dealer.Procurement.Adapter.ProcLeadStatusAdapter;
import com.mfcwl.mfc_dealer.Procurement.Instances.ProcLeadFilterSaveInstance;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

public class ProcurementFilterBy extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, FilterSelected {

    private FrameLayout mApplyFrameLayout, mCloseFrameLayout;
    private TextView clearFilter;
    private TextView mPostdate, mFollowup, mLeadstatus;
    private TextView mPostcalendardate, mFollowupcalendardate, mPostfromdate, mPosttodate, mFollfromdate, mFolltodate;

    private CardView mCardpostdate, mCardfolldate, mCardleadstatus;

    private CheckBox chposttoday, chpostyester, chpost7days, chpost15days;
    private CheckBox chfollowtmrw, chfollowtoday, chfollowyester, chfollowlast7days;

    private HashMap<String, String> postedfollDateHashMap;

    public static JSONArray leadstatusJsonArray;

    LeadConstantsSection leadConstantsSection;

    private String typeLead = "";

    private boolean flagpostdate = true, flagfollowup = true, flagstatus = true;
    private String customPostedstartDateString = "", customPostedendDateString = "";
    private String customFollowstartDateString = "", customFollowendDateString = "";

    private SharedPreferences mSharedPreferences = null;

    ProcLeadStatusAdapter adapter;
    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate: ");
        setContentView(R.layout.procurement_filter);


        InitUI();
    }

    private void InitUI() {
        mSharedPreferences = getSharedPreferences("MFCP", Context.MODE_PRIVATE);
        mApplyFrameLayout = (FrameLayout) findViewById(R.id.mApplyFrameLayout);
        mCloseFrameLayout = (FrameLayout) findViewById(R.id.mCloseFrameLayout);

        Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(ProcurementFilterBy.this, "dealer_code"), GlobalText.proc_filter, GlobalText.android);

        clearFilter = (TextView) findViewById(R.id.clearFilter);

        mPostdate = (TextView) findViewById(R.id.postdate);
        mFollowup = (TextView) findViewById(R.id.followup);
        mLeadstatus = (TextView) findViewById(R.id.leadstatus);

        mPostcalendardate = (TextView) findViewById(R.id.postcalendardate);
        mFollowupcalendardate = (TextView) findViewById(R.id.followupcalendardate);
        mFollfromdate = (TextView) findViewById(R.id.fromdatefoll);
        mFolltodate = (TextView) findViewById(R.id.todatefoll);
        mPostfromdate = (TextView) findViewById(R.id.postfromdate);
        mPosttodate = (TextView) findViewById(R.id.posttodate);

        //CardView
        mCardpostdate = (CardView) findViewById(R.id.mCardpostdate);
        mCardfolldate = (CardView) findViewById(R.id.mCardfolldate);
        mCardleadstatus = (CardView) findViewById(R.id.mCardleadstatus);

        cardVisibiltyCheck();

        //CheckBox for Posted Date
        chposttoday = (CheckBox) findViewById(R.id.chposttoday);
        chpostyester = (CheckBox) findViewById(R.id.chpostyester);
        chpost7days = (CheckBox) findViewById(R.id.chpost7days);
        chpost15days = (CheckBox) findViewById(R.id.chpost15days);

        //CheckBox for Followup Date
        chfollowtmrw = (CheckBox) findViewById(R.id.chfollowtmrw);
        chfollowtoday = (CheckBox) findViewById(R.id.chfollowtoday);
        chfollowyester = (CheckBox) findViewById(R.id.chfollowyester);
        chfollowlast7days = (CheckBox) findViewById(R.id.chfollowlast7days);

        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Initialize HashMap
        postedfollDateHashMap = new HashMap<String, String>();
        leadstatusJsonArray = new JSONArray();

        leadConstantsSection = new LeadConstantsSection();

        restoreSelection();

        clearFilter.setOnClickListener(this);
        mApplyFrameLayout.setOnClickListener(this);
        mCloseFrameLayout.setOnClickListener(this);

        //PostedDate Callback Listener
        chposttoday.setOnCheckedChangeListener(this);
        chpostyester.setOnCheckedChangeListener(this);
        chpost7days.setOnCheckedChangeListener(this);
        chpost15days.setOnCheckedChangeListener(this);

        //Followup Callback Listener
        chfollowtmrw.setOnCheckedChangeListener(this);
        chfollowtoday.setOnCheckedChangeListener(this);
        chfollowyester.setOnCheckedChangeListener(this);
        chfollowlast7days.setOnCheckedChangeListener(this);

        //Calendar Callback Listener
        mPostcalendardate.setOnClickListener(this);
        mFollowupcalendardate.setOnClickListener(this);

        mPostdate.setOnClickListener(this);
        mFollowup.setOnClickListener(this);
        mLeadstatus.setOnClickListener(this);

        GridView status_grid = (GridView) findViewById(R.id.status_grid);
        ArrayList dataModels = new ArrayList();

        try {
            for (int i = 0; i < MainActivity.leadstatus.size(); i++) {
                String data = MainActivity.leadstatus.get(i).getStatus();
                if (leadstatusJsonArray != null) {
                    String data1 = "";
                    try {
                        for (int j = 0; j < leadstatusJsonArray.length(); j++) {
                            String item = leadstatusJsonArray.get(j).toString();
                            if (data.equalsIgnoreCase(item)) {
                                data1 = data;
                            }
                        }
                        if (data1.equalsIgnoreCase(data)) {
                            dataModels.add(new LeadstatusModel(data, true));
                        } else {
                            dataModels.add(new LeadstatusModel(data, false));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    dataModels.add(new LeadstatusModel(MainActivity.leadstatus.get(i).getStatus(), false));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        adapter = new ProcLeadStatusAdapter(dataModels, getApplicationContext());
        status_grid.setAdapter(adapter);

        status_grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {

                try {
                    LeadstatusModel dataModel = (LeadstatusModel) dataModels.get(position);
                    dataModel.checked = !dataModel.checked;
                    adapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (!CommonMethods.getstringvaluefromkey(ProcurementFilterBy.this, "user_type").equalsIgnoreCase("dealer")
        ) {

            if (!CommonMethods.getstringvaluefromkey(ProcurementFilterBy.this, "startDate").equalsIgnoreCase("")) {

                String startDate = CommonMethods.getstringvaluefromkey(ProcurementFilterBy.this, "startDate").toString();
                String endDate = CommonMethods.getstringvaluefromkey(ProcurementFilterBy.this, "endDate").toString();

                mPostfromdate.setText(startDate + " - ");
                mPosttodate.setText(endDate);
            }
        }

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.postdate:
                if (flagpostdate) {
                    mCardpostdate.setVisibility(View.VISIBLE);
                    mPostdate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                    flagpostdate = false;
                } else {
                    mCardpostdate.setVisibility(View.GONE);
                    mPostdate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
                    flagpostdate = true;
                }
                break;
            case R.id.followup:
                if (flagfollowup) {
                    mCardfolldate.setVisibility(View.VISIBLE);
                    mFollowup.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                    flagfollowup = false;
                } else {
                    mCardfolldate.setVisibility(View.GONE);
                    mFollowup.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
                    flagfollowup = true;
                }

                break;
            case R.id.leadstatus:
                if (flagstatus) {
                    mCardleadstatus.setVisibility(View.VISIBLE);
                    mLeadstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                    flagstatus = false;
                } else {
                    mCardleadstatus.setVisibility(View.GONE);
                    mLeadstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
                    flagstatus = true;
                }

                break;

            case R.id.clearFilter:
                //ASM
                CommonMethods.setvalueAgainstKey(ProcurementFilterBy.this, "leadStatus", "");

                CommonMethods.setvalueAgainstKey(ProcurementFilterBy.this,"Lvalue","");
                CommonMethods.setvalueAgainstKey(ProcurementFilterBy.this,"leads_status","");
                CommonMethods.setvalueAgainstKey(ProcurementFilterBy.this,"startDate","");
                CommonMethods.setvalueAgainstKey(ProcurementFilterBy.this, "withoutfollowup", "");

                SharedPreferences.Editor mEditor = mSharedPreferences.edit();

                mEditor.putString(LeadConstantsSection.LEAD_SAVE_DATE, "");
                mEditor.putString(LeadConstantsSection.LEAD_SAVE_STATUS, "");
                ProcLeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());
                ProcLeadFilterSaveInstance.getInstance().setStatusarray(new JSONArray());
                mEditor.clear();
                mEditor.commit();
                setResult(500);
                finish();
                break;

            case R.id.mApplyFrameLayout:


                CommonMethods.setvalueAgainstKey(ProcurementFilterBy.this, "leadStatus", "");

                CommonMethods.setvalueAgainstKey(ProcurementFilterBy.this,"Lvalue","");
                CommonMethods.setvalueAgainstKey(ProcurementFilterBy.this,"leads_status","");

                isPostedCheckBox();
                isFollowCheckBox();
                // isStatusCheckBox();
                ProcLeadFilterSaveInstance.getInstance().setSavedatahashmap(postedfollDateHashMap);
                ProcLeadFilterSaveInstance.getInstance().setStatusarray(leadstatusJsonArray);
                SaveDevice();
                setResult(500);
                finish();
                break;

            case R.id.mCloseFrameLayout:
                finish();
                break;

            case R.id.postcalendardate:
                mPostcalendardate.setEnabled(false);
                CalendarFilterDialog postcalendarFilterDialog = new CalendarFilterDialog(this, "CustomPostDate");
                postcalendarFilterDialog.setCancelable(false);
                postcalendarFilterDialog.show();
                break;

            case R.id.followupcalendardate:
                mFollowupcalendardate.setEnabled(false);
                CalendarFilterDialog followcalendarFilterDialog = new CalendarFilterDialog(this, "CustomFollowDate");
                followcalendarFilterDialog.setCancelable(false);
                followcalendarFilterDialog.show();
                break;

            default:
                break;
        }
    }

    private void SaveDevice() {
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();
        Gson gson = new Gson();
        String postfollDate = gson.toJson(postedfollDateHashMap);
        String status = gson.toJson(leadstatusJsonArray);
        mEditor.putString(LeadConstantsSection.LEAD_SAVE_DATE, postfollDate);
        mEditor.putString(LeadConstantsSection.LEAD_SAVE_STATUS, status);
        mEditor.commit();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.chposttoday || buttonView.getId() == R.id.chpostyester ||
                buttonView.getId() == R.id.chpost7days || buttonView.getId() == R.id.chpost15days) {
            unCheckPostedCheckBox();
        } else if (buttonView.getId() == R.id.chfollowtmrw || buttonView.getId() == R.id.chfollowtoday ||
                buttonView.getId() == R.id.chfollowyester || buttonView.getId() == R.id.chfollowlast7days) {
            unCheckFollowupCheckBox();
        }
        switch (buttonView.getId()) {
            //Posted Date
            case R.id.chposttoday:
                if (isChecked) {
                    chposttoday.setChecked(true);
                } else {
                    chposttoday.setChecked(false);
                }
                break;
            case R.id.chpostyester:
                if (isChecked) {
                    chpostyester.setChecked(true);
                } else {
                    chpostyester.setChecked(false);
                }
                break;
            case R.id.chpost7days:
                if (isChecked) {
                    chpost7days.setChecked(true);
                } else {
                    chpost7days.setChecked(false);
                }
                break;
            case R.id.chpost15days:
                if (isChecked) {
                    chpost15days.setChecked(true);
                } else {
                    chpost15days.setChecked(false);
                }
                break;
            //End


            //Followup Date
            case R.id.chfollowtmrw:
                if (isChecked) {
                    chfollowtmrw.setChecked(true);
                } else {
                    chfollowtmrw.setChecked(false);
                }
                break;
            case R.id.chfollowtoday:
                if (isChecked) {
                    chfollowtoday.setChecked(true);
                } else {
                    chfollowtoday.setChecked(false);
                }
                break;
            case R.id.chfollowyester:
                if (isChecked) {
                    chfollowyester.setChecked(true);
                } else {
                    chfollowyester.setChecked(false);
                }
                break;
            case R.id.chfollowlast7days:
                if (isChecked) {
                    chfollowlast7days.setChecked(true);
                } else {
                    chfollowlast7days.setChecked(false);
                }
                break;

        }

    }


    private void unCheckPostedCheckBox() {
        chposttoday.setChecked(false);
        chpostyester.setChecked(false);
        chpost7days.setChecked(false);
        chpost15days.setChecked(false);
        mPostfromdate.setText("");
        mPosttodate.setText("");
    }

    private void unCheckFollowupCheckBox() {
        chfollowtmrw.setChecked(false);
        chfollowtoday.setChecked(false);
        chfollowyester.setChecked(false);
        chfollowlast7days.setChecked(false);
        mFollfromdate.setText("");
        mFolltodate.setText("");
    }


    private void cardVisibiltyCheck() {
        mCardpostdate.setVisibility(View.GONE);
        mCardfolldate.setVisibility(View.GONE);
        mCardleadstatus.setVisibility(View.GONE);
    }


    @Override
    public void onFilterSelection(String startDateString, String endDateString, String dateType) {
        mPostcalendardate.setEnabled(true);
        mFollowupcalendardate.setEnabled(true);



        if (startDateString.equals("") || endDateString.equals("")) {
            customPostedstartDateString = startDateString;
            customPostedendDateString = endDateString;
            customFollowstartDateString = startDateString;
            customFollowendDateString = endDateString;
            if (dateType.equals("CustomPostDate")) {
                mPostfromdate.setText("");
                mPosttodate.setText("");
                //postedfollDateHashMap.remove(LeadConstantsSection.LEAD_POST_CUSTDATE);
            } else {
                mFollfromdate.setText("");
                mFolltodate.setText("");
                //postedfollDateHashMap.remove(LeadConstantsSection.LEAD_FOLUP_CUSTDATE);
            }
            return;
        }
        if (dateType.equals("CustomPostDate")) {
            unCheckPostedCheckBox();
            customPostedstartDateString = startDateString;
            customPostedendDateString = endDateString;
            showCustomPostedDate(startDateString, endDateString);
        } else {
            unCheckFollowupCheckBox();
            customFollowstartDateString = startDateString;
            customFollowendDateString = endDateString;
            showCustomFollowupDate(startDateString, endDateString);
        }


    }


    private void showCustomPostedDate(String startDateString, String endDateString) {

        CommonMethods.setvalueAgainstKey(ProcurementFilterBy.this, "startDate","");

        String[] splitStartDate = startDateString.split("-", 10);
        String[] splitEndDate = endDateString.split("-", 10);
        mPostfromdate.setText(splitStartDate[2] + " " + leadConstantsSection.MethodofMonth(splitStartDate[1]) + " " + splitStartDate[0] + " To ");
        mPosttodate.setText(splitEndDate[2] + " " + leadConstantsSection.MethodofMonth(splitEndDate[1]) + " " + splitEndDate[0]);
    }

    private void showCustomFollowupDate(String startDateString, String endDateString) {
        String[] splitStartDate = startDateString.split("-", 10);
        String[] splitEndDate = endDateString.split("-", 10);
        mFollfromdate.setText(splitStartDate[2] + " " + leadConstantsSection.MethodofMonth(splitStartDate[1]) + " " + splitStartDate[0] + " To ");
        mFolltodate.setText(splitEndDate[2] + " " + leadConstantsSection.MethodofMonth(splitEndDate[1]) + " " + splitEndDate[0]);
    }


    private void restoreSelection() {

        postedfollDateHashMap = ProcLeadFilterSaveInstance.getInstance().getSavedatahashmap();
        leadstatusJsonArray = ProcLeadFilterSaveInstance.getInstance().getStatusarray();
        //restore Posted Date
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_TODAY) != null) {
            mCardpostdate.setVisibility(View.VISIBLE);
            chposttoday.setChecked(true);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_YESTER) != null) {
            mCardpostdate.setVisibility(View.VISIBLE);
            chpostyester.setChecked(true);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_7DAYS) != null) {
            mCardpostdate.setVisibility(View.VISIBLE);
            chpost7days.setChecked(true);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_15DAYS) != null) {
            mCardpostdate.setVisibility(View.VISIBLE);
            chpost15days.setChecked(true);
        }

        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE) != null) {
            mCardpostdate.setVisibility(View.VISIBLE);
            String[] splitPostDate = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE).split("@", 10);
            customPostedstartDateString = splitPostDate[0];
            customPostedendDateString = splitPostDate[1];
            showCustomPostedDate(customPostedstartDateString, customPostedendDateString);
        }

        //restore Follow-Up Date

        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TMRW) != null) {
            mCardfolldate.setVisibility(View.VISIBLE);
            chfollowtmrw.setChecked(true);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TODAY) != null) {
            mCardfolldate.setVisibility(View.VISIBLE);
            chfollowtoday.setChecked(true);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_YESTER) != null) {
            mCardfolldate.setVisibility(View.VISIBLE);
            chfollowyester.setChecked(true);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_7DAYS) != null) {
            mCardfolldate.setVisibility(View.VISIBLE);
            chfollowlast7days.setChecked(true);
        }

        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_CUSTDATE) != null) {
            mCardfolldate.setVisibility(View.VISIBLE);
            String[] splitFollowDate = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_CUSTDATE).split("@", 10);
            customFollowstartDateString = splitFollowDate[0];
            customFollowendDateString = splitFollowDate[1];
            showCustomFollowupDate(customFollowstartDateString, customFollowendDateString);
        }

        //restore Lead Status

        if (leadstatusJsonArray.length() == 0) {
            mCardleadstatus.setVisibility(View.GONE);

        } else {
            mCardleadstatus.setVisibility(View.VISIBLE);

        }
    }

    private void isPostedCheckBox() {

        if (!customPostedstartDateString.equals("") && !customPostedendDateString.equals("")) {
            postedfollDateHashMap.put(LeadConstantsSection.LEAD_POST_CUSTDATE, customPostedstartDateString + "@" + customPostedendDateString);
        } else {
            postedfollDateHashMap.remove(LeadConstantsSection.LEAD_POST_CUSTDATE);
        }

        if (chposttoday.isChecked()) {
            postedfollDateHashMap.put(LeadConstantsSection.LEAD_POST_TODAY, leadConstantsSection.getTodayDate());
            postedfollDateHashMap.remove(LeadConstantsSection.LEAD_POST_CUSTDATE);
        } else {
            postedfollDateHashMap.remove(LeadConstantsSection.LEAD_POST_TODAY);
        }
        if (chpostyester.isChecked()) {
            postedfollDateHashMap.put(LeadConstantsSection.LEAD_POST_YESTER, leadConstantsSection.getYesterdayDate());
            postedfollDateHashMap.remove(LeadConstantsSection.LEAD_POST_CUSTDATE);
        } else {
            postedfollDateHashMap.remove(LeadConstantsSection.LEAD_POST_YESTER);
        }
        if (chpost7days.isChecked()) {
            postedfollDateHashMap.put(LeadConstantsSection.LEAD_POST_7DAYS, leadConstantsSection.get7days());
            postedfollDateHashMap.remove(LeadConstantsSection.LEAD_POST_CUSTDATE);
        } else {
            postedfollDateHashMap.remove(LeadConstantsSection.LEAD_POST_7DAYS);
        }
        if (chpost15days.isChecked()) {
            postedfollDateHashMap.put(LeadConstantsSection.LEAD_POST_15DAYS, leadConstantsSection.get15days());
            postedfollDateHashMap.remove(LeadConstantsSection.LEAD_POST_CUSTDATE);
        } else {
            postedfollDateHashMap.remove(LeadConstantsSection.LEAD_POST_15DAYS);
        }


    }

    private void isFollowCheckBox() {

        // Log.e("isFollowCheckBox ", "start " + customFollowstartDateString);
        //  Log.e("isFollowCheckBox ", "end " + customFollowendDateString);

        if (!customFollowstartDateString.equals("") && !customFollowendDateString.equals("")) {
            postedfollDateHashMap.put(LeadConstantsSection.LEAD_FOLUP_CUSTDATE, customFollowstartDateString + "@" + customFollowendDateString);
        } else {
            postedfollDateHashMap.remove(LeadConstantsSection.LEAD_FOLUP_CUSTDATE);
        }


        //Followup Date
        if (chfollowtmrw.isChecked()) {
            postedfollDateHashMap.put(LeadConstantsSection.LEAD_FOLUP_TMRW, leadConstantsSection.getFollowTomorrowDate());
            postedfollDateHashMap.remove(LeadConstantsSection.LEAD_FOLUP_CUSTDATE);
        } else {
            postedfollDateHashMap.remove(LeadConstantsSection.LEAD_FOLUP_TMRW);
        }
        if (chfollowtoday.isChecked()) {
            postedfollDateHashMap.put(LeadConstantsSection.LEAD_FOLUP_TODAY, leadConstantsSection.getFollowtodayDate());
            postedfollDateHashMap.remove(LeadConstantsSection.LEAD_FOLUP_CUSTDATE);
        } else {
            postedfollDateHashMap.remove(LeadConstantsSection.LEAD_FOLUP_TODAY);
        }

        if (chfollowyester.isChecked()) {
            postedfollDateHashMap.put(LeadConstantsSection.LEAD_FOLUP_YESTER, leadConstantsSection.getFollowyesterDate());
            postedfollDateHashMap.remove(LeadConstantsSection.LEAD_FOLUP_CUSTDATE);
        } else {
            postedfollDateHashMap.remove(LeadConstantsSection.LEAD_FOLUP_YESTER);
        }

        if (chfollowlast7days.isChecked()) {
            postedfollDateHashMap.put(LeadConstantsSection.LEAD_FOLUP_7DAYS, leadConstantsSection.getFollowlast7Date());
            postedfollDateHashMap.remove(LeadConstantsSection.LEAD_FOLUP_CUSTDATE);
        } else {
            postedfollDateHashMap.remove(LeadConstantsSection.LEAD_FOLUP_7DAYS);
        }

    }
}
