package com.mfcwl.mfc_dealer.Procurement.ReqResModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PWList {
    @SerializedName("Page")
    @Expose
    private Integer page;
    @SerializedName("PageItems")
    @Expose
    private Integer pageItems;
    @SerializedName("OrderBy")
    @Expose
    private String orderBy;
    @SerializedName("OrderByReverse")
    @Expose
    private String orderByReverse;
    @SerializedName("wherein")
    @Expose
    private List<Wherein> wherein = null;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageItems() {
        return pageItems;
    }

    public void setPageItems(Integer pageItems) {
        this.pageItems = pageItems;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getOrderByReverse() {
        return orderByReverse;
    }

    public void setOrderByReverse(String orderByReverse) {
        this.orderByReverse = orderByReverse;
    }

    public List<Wherein> getWherein() {
        return wherein;
    }

    public void setWherein(List<Wherein> wherein) {
        this.wherein = wherein;
    }

}
