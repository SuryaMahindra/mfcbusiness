package com.mfcwl.mfc_dealer.Procurement.ConvertToStockRequestResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConvertStockRequest {

    @SerializedName("lead_id")
    @Expose
    private String leadId;
    @SerializedName("LeadRemark")
    @Expose
    private String leadRemark;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("updated_by_device")
    @Expose
    private String updatedByDevice;
    @SerializedName("Stockdata")
    @Expose
    private Stockdata stockdata;

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getLeadRemark() {
        return leadRemark;
    }

    public void setLeadRemark(String leadRemark) {
        this.leadRemark = leadRemark;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUpdatedByDevice() {
        return updatedByDevice;
    }

    public void setUpdatedByDevice(String updatedByDevice) {
        this.updatedByDevice = updatedByDevice;
    }

    public Stockdata getStockdata() {
        return stockdata;
    }

    public void setStockdata(Stockdata stockdata) {
        this.stockdata = stockdata;
    }

}