package com.mfcwl.mfc_dealer.Procurement.ReqResModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PDashWebLeadsDatumResponse {
    @SerializedName("PostedDate")
    @Expose
    private String postedDate;
    @SerializedName("LeadDate")
    @Expose
    private String leadDate;

    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public String getLeadDate() {
        return leadDate;
    }

    public void setLeadDate(String leadDate) {
        this.leadDate = leadDate;
    }

}
