package com.mfcwl.mfc_dealer.Procurement.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.Activity.LeadSection.LeadConstantsSection;
import com.mfcwl.mfc_dealer.Activity.Leads.WebLeadsConstant;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.Procurement.Activity.ProcurementDetails;
import com.mfcwl.mfc_dealer.Procurement.Adapter.ProcurementWebLeadAdapter;
import com.mfcwl.mfc_dealer.Procurement.Instances.ProcLeadFilterSaveInstance;
import com.mfcwl.mfc_dealer.Procurement.Instances.ProcSortInstanceLeadSection;
import com.mfcwl.mfc_dealer.Procurement.ProcInterface.PFilterApply;
import com.mfcwl.mfc_dealer.Procurement.ProcInterface.PLazyloaderWebLeads;
import com.mfcwl.mfc_dealer.Procurement.ProcInterface.PSearchWL;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PWLCustomWhere;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PWLDatum;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PWLRequest;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PWLResponse;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PWLWhereIn;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PWLWhereor;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.RetrofitServicesLeadSection.PrivateLeadService;
import com.mfcwl.mfc_dealer.RetrofitServicesLeadSection.WebLeadService;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GAConstants;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import rdm.NotificationActivity;
import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.activity;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchImage;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchicon;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toggle;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toolbar;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.tv_header_title;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ASM;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_DEALER_CRE;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_PROCUREMENT;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_SALES;

public class ProcurementWebLeads extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    Button proc_btnAllLeads, proc_btnFollowUpLeads;
    SwipeRefreshLayout proc_swipeRefresh;
    RecyclerView proc_recyclerView;
    TextView proc_noResults;

    private List<PWLCustomWhere> mWebLeadsCustomWheres;
    private List<String> mWebLeadsWherenotin;
    private List<PWLWhereor> mWhereorList;
    private List<PWLWhereIn> mWherein;
    private List<PWLCustomWhere> webLeadsCustomWheresList;

    private List<PWLDatum> mFollowuplist;
    private List<PWLDatum> mLeadsDatumList;

    private int pageItem = 100;
    private String page_no = "1";
    private int counts = 1;
    private int negotiationLeadCount = 0;
    private boolean isLoadmore;
    private String searchQuery = "";
    ProcurementWebLeadAdapter madapter;

    private HashMap<String, String> postedfollDateHashMap = new HashMap<String, String>();
    private JSONArray leadstatusJsonArray = new JSONArray();

    public boolean flag = false;

    int verticalOffset;
    boolean scrollingUp;
    PWLRequest webLeadsModelRequest = new PWLRequest();
    int i = 0;

    public ProcurementWebLeads() {
    }

    public String TAG = getClass().getSimpleName();
    LeadConstantsSection leadConstantsSection;


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);


        String uType = CommonMethods.getstringvaluefromkey(activity, "user_type");
        searchImage.setImageResource(R.drawable.search);
           MenuItem asm_mail = menu.findItem(R.id.asm_mail);

           //new ASM
           if (uType.equalsIgnoreCase("dealer")) {
               asm_mail.setVisible(false);
           } else if(CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_DEALER_CRE) ||
                   CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_SALES) ||
                   CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_PROCUREMENT)){
               asm_mail.setVisible(false);
           }else {
               asm_mail.setVisible(true);
           }
           MenuItem asmHome = menu.findItem(R.id.asmHome);
           MenuItem add_image = menu.findItem(R.id.add_image);
           MenuItem share = menu.findItem(R.id.share);
           MenuItem delete_fea = menu.findItem(R.id.delete_fea);
           MenuItem stock_fil_clear = menu.findItem(R.id.stock_fil_clear);
           MenuItem notification_bell = menu.findItem(R.id.notification_bell);
           MenuItem notification_cancel = menu.findItem(R.id.notification_cancel);

           asmHome.setVisible(false);
           add_image.setVisible(false);
           share.setVisible(false);
           delete_fea.setVisible(false);
           stock_fil_clear.setVisible(false);
           notification_bell.setVisible(false);
           notification_cancel.setVisible(false);



           asm_mail.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

               @Override
               public boolean onMenuItemClick(MenuItem item) {
                   //Toast.makeText(getActivity()," proc web leads sending email...",Toast.LENGTH_LONG).show();
                   if (CommonMethods.isInternetWorking(getActivity())) {

                       // custom dialog
                       Dialog dialog = new Dialog(activity);
                       dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                       dialog.getWindow().setGravity(Gravity.TOP | Gravity.RIGHT);
                       dialog.setContentView(R.layout.common_cus_dialog);
                       TextView Message, Message2;
                       Button cancel, Confirm;
                       Message = dialog.findViewById(R.id.Message);
                       Message2 = dialog.findViewById(R.id.Message2);
                       cancel = dialog.findViewById(R.id.cancel);
                       Confirm = dialog.findViewById(R.id.Confirm);
                       Message.setText(GlobalText.are_you_sure_to_send_mail);
                       //Message2.setText("Go ahead and Submit ?");
                       Confirm.setOnClickListener(new View.OnClickListener() {
                           @Override
                           public void onClick(View v) {
                               sendEmailWebLeadDetails(getActivity(), webLeadsModelRequest);
                               dialog.dismiss();
                           }
                       });
                       cancel.setOnClickListener(new View.OnClickListener() {
                           @Override
                           public void onClick(View v) {
                               dialog.dismiss();
                           }
                       });
                       dialog.show();

                   } else {
                       CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);

                   }
                   return true;
               }
           });

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.fragment_procurement_web_leads, container, false);
        setHasOptionsMenu(true);

        if(CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(USERTYPE_ASM))
        {
            if(tv_header_title!=null)
            {
                searchicon.setVisibility(View.VISIBLE);
                searchImage.setVisibility(View.VISIBLE);
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Procurement Leads");
                tv_header_title.setTextColor(getResources().getColor(R.color.white));
            }
        }
        else
        {
            if (tv_header_title != null) {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Procurement Leads");
            }
        }


        InitUI(view);
        onClickListener();

        return view;

    }

    private void InitUI(View view) {


        proc_btnAllLeads = view.findViewById(R.id.proc_btnAllLeads);
        proc_btnFollowUpLeads = view.findViewById(R.id.proc_btnFollowUpLeads);
        proc_swipeRefresh = view.findViewById(R.id.proc_swipeRefresh);
        proc_recyclerView = view.findViewById(R.id.proc_recyclerView);
        proc_noResults = view.findViewById(R.id.proc_noResults);

        leadConstantsSection = new LeadConstantsSection();

        proc_swipeRefresh.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.CYAN);
        proc_swipeRefresh.setOnRefreshListener(this);

        isLoadmore = false;

        if (WebLeadsConstant.getInstance().getWhichleads().equalsIgnoreCase("AllLeads")) {
            proc_btnAllLeads.setTextColor(Color.BLACK);
            proc_btnAllLeads.setBackgroundResource(R.drawable.my_button);
            proc_btnFollowUpLeads.setTextColor(Color.parseColor("#b9b9b9"));
            proc_btnFollowUpLeads.setBackgroundResource(R.drawable.my_buttom_gray_light);
        } else if (WebLeadsConstant.getInstance().getWhichleads().equalsIgnoreCase("FollowupLeads")) {
            proc_btnFollowUpLeads.setTextColor(Color.BLACK);
            proc_btnFollowUpLeads.setBackgroundResource(R.drawable.my_button);
            proc_btnAllLeads.setTextColor(Color.parseColor("#b9b9b9"));
            proc_btnAllLeads.setBackgroundResource(R.drawable.my_buttom_gray_light);
        }

        if (CommonMethods.getstringvaluefromkey(getActivity(), "ProcWebLeads").equalsIgnoreCase("true")) {
            try {
                i = Integer.parseInt(ProcurementDetails.position);
            } catch (Exception e) {
                i = 0;
                // e.printStackTrace();
            }
        } else {
            i = 0;
        }
        Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"), GlobalText.proc_webLeads, GlobalText.android);
        bottomTabScrolling();
        prepareWebLeadRequest();
        onCallbackEvents();
        int margin = getResources().getDimensionPixelSize(R.dimen.recyclepadding);
        //new ASM
        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
        } else {
            ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) proc_swipeRefresh.getLayoutParams();
            mlp.setMargins(0, 0, 0, margin);
        }
    }

    private void bottomTabScrolling() {
        proc_recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {

            // Determines the scroll UP/DOWN direction
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                verticalOffset += dy;
                scrollingUp = dy > 0;
                if (scrollingUp) {
                    if (flag) {
                        MainActivity.bottom_topAnimation();
                        flag = false;
                    }
                } else {
                    if (!flag) {
                        MainActivity.top_bottomAnimation();
                        flag = true;
                    }
                }
            }
        });
    }

    private void onClickListener() {
        proc_btnAllLeads.setOnClickListener(this::onClick);
        proc_btnFollowUpLeads.setOnClickListener(this::onClick);
    }

    @Override
    public void onClick(View v) {
        restoreInputValues();
        i = 0;
        switch (v.getId()) {
            case R.id.proc_btnAllLeads:
                WebLeadsConstant.getInstance().setWhichleads("AllLeads");
                proc_btnAllLeads.setTextColor(Color.BLACK);
                proc_btnAllLeads.setBackgroundResource(R.drawable.my_button);
                proc_btnFollowUpLeads.setTextColor(Color.parseColor("#b9b9b9"));
                proc_btnFollowUpLeads.setBackgroundResource(R.drawable.my_buttom_gray_light);
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"), GlobalText.proc_web_allLeads, GlobalText.android);
                prepareWebLeadRequest();
                break;
            case R.id.proc_btnFollowUpLeads:
                WebLeadsConstant.getInstance().setWhichleads("FollowupLeads");
                proc_btnFollowUpLeads.setTextColor(Color.BLACK);
                proc_btnFollowUpLeads.setBackgroundResource(R.drawable.my_button);
                proc_btnAllLeads.setTextColor(Color.parseColor("#b9b9b9"));
                proc_btnAllLeads.setBackgroundResource(R.drawable.my_buttom_gray_light);
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"), GlobalText.proc_web_followLeads, GlobalText.android);
                preparefollowupWebLeadRequest();
                break;

        }
    }

    private void onCallbackEvents() {

        ((MainActivity) getActivity()).setPFragmentSortListener(new MainActivity.PFragmentSortListener() {

            @Override
            public void onSortPWeb() {
                if (CommonMethods.getstringvaluefromkey(getActivity(), "ProcWebLeads").equalsIgnoreCase("false")) {
                    i = 0;
                }
                restoreInputValues();
                if (WebLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
                    prepareWebLeadRequest();
                } else {
                    preparefollowupWebLeadRequest();
                }
            }
        });

        ((MainActivity) getActivity()).PupdateSearchWebLead(new PSearchWL() {
            @Override
            public void onPSearchWL(String query) {
                searchQuery = query;
                if (mLeadsDatumList.isEmpty()) {
                    restoreInputValues();
                    i = 0;
                    if (WebLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
                        prepareWebLeadRequest();
                    } else {
                        preparefollowupWebLeadRequest();
                    }
                }

                madapter.filter(query);

               /* if (madapter != null) {
                    madapter.filter(query);
                }*/
            }
        });

        ((MainActivity) getActivity()).setPLazyloaderWebLeadsListener(new PLazyloaderWebLeads() {
            @Override
            public void Ploadmore(int ItemCount) {

                counts = counts + 1;
                double maxPageNumber = Math.ceil((double) (negotiationLeadCount) / (double) pageItem);
                if (counts > maxPageNumber) {
                    return;
                }
                page_no = Integer.toString(counts);

                isLoadmore = true;

                if (WebLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
                    prepareWebLeadRequest();
                } else {
                    preparefollowupWebLeadRequest();
                }

            }

            @Override
            public void Pupdatecountweb(int count) {
                //  Log.e("updatecount ", "size " + count);
                if (count <= 3) {
                    // mLeadsDatumList.clear();
                    if (CommonMethods.getstringvaluefromkey(getActivity(), "ProcWebLeads").equalsIgnoreCase("false")) {
                        i = 0;
                    }
                    restoreInputValues();
                    if (WebLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
                        prepareWebLeadRequest();
                    } else {
                        preparefollowupWebLeadRequest();
                    }
                }
            }
        });

        //Apply Filter

        ((MainActivity) getActivity()).setPApplyWebFilterListener(new PFilterApply() {
            @Override
            public void applyPFilter(String typeLead) {
                //Posted Date
                if (CommonMethods.getstringvaluefromkey(getActivity(), "ProcWebLeads").equalsIgnoreCase("false")) {
                    i = 0;
                }
                restoreInputValues();
                if (WebLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
                    prepareWebLeadRequest();
                } else {
                    preparefollowupWebLeadRequest();
                }
                postedfollDateHashMap = ProcLeadFilterSaveInstance.getInstance().getSavedatahashmap();
                leadstatusJsonArray = ProcLeadFilterSaveInstance.getInstance().getStatusarray();

            }
        });

    }


    private void attachtoAdapter(List<PWLDatum> mlist) {
        madapter = new ProcurementWebLeadAdapter(getContext(), getActivity(), mlist);
        proc_recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getContext());
        proc_recyclerView.setLayoutManager(mLinearLayoutManager);
        proc_recyclerView.setAdapter(madapter);

        proc_recyclerView.scrollToPosition(i);

    }

    private void restoreInputValues() {
        page_no = "1";
        counts = 1;
        negotiationLeadCount = 0;
        isLoadmore = false;
    }

    @Override
    public void onRefresh() {
        restoreInputValues();
        i = 0;
        proc_swipeRefresh.setRefreshing(true);
        if (tv_header_title != null) {
            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText("Procurement Leads");
        }

        if (WebLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
            prepareWebLeadRequest();
        } else {
            preparefollowupWebLeadRequest();
        }
    }

    @Override
    public void onResume() {

        if(CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(USERTYPE_ASM))
        {
            toggle.setHomeAsUpIndicator(R.drawable.ic_new_hamburger_menu);
            searchicon.setVisibility(View.VISIBLE);
            searchImage.setVisibility(View.VISIBLE);
            toolbar.setBackgroundColor(getResources().getColor(R.color.black));
            if(tv_header_title!=null)
            {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Procurement Leads");
                tv_header_title.setTextColor(getResources().getColor(R.color.white));
            }
            Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_PROCUREMENT_LEAD,CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));

        }
        else
        {
            if (tv_header_title != null) {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Procurement Leads");
            }
        }


        super.onResume();
    }

    private void preparefollowupWebLeadRequest() {

        webLeadsCustomWheresList = new ArrayList<>();
        mWherein = new ArrayList<>();
        mWhereorList = new ArrayList<>();
        mFollowuplist = new ArrayList<>();
        mWebLeadsWherenotin = new ArrayList<>();
        mWebLeadsCustomWheres = new ArrayList<>();

        PWLRequest webLeadsModelRequest = new PWLRequest();

        webLeadsModelRequest.setFilterByFields("");
        webLeadsModelRequest.setPerPage(Integer.toString(pageItem));
        webLeadsModelRequest.setPage(page_no);

        webLeadsModelRequest.setAccessToken(CommonMethods.getstringvaluefromkey(getActivity(), "access_token"));

        //Search input Values
        searchbyWebLeads(mWhereorList);

        webLeadsModelRequest.setWhereOr(mWhereorList);
        webLeadsModelRequest.setAliasFields("leads.created_at as PostedDate,leads.created_at as LeadDate,leads.id as id,dispatched.id as dispatchid,leads.customer_name as Name,leads.customer_mobile as Mobile,leads.customer_email as Email,leads.primary_make,leads.primary_model,leads.primary_variant,leads.secondary_make,leads.secondary_model,leads.secondary_variant,leads.register_no as regno,leads.register_month as regmonth,leads.register_year as regyear,leads.color,leads.kms,leads.owner as owners,leads.register_city,leads.stock_listed_price as price,dispatched.status,dispatched.followup_comments as remark,dispatched.followup_date as FollowUpDate,dispatched.employee_name as ExecutiveName,leads.lead_type");

        sortbyWebLeads(webLeadsModelRequest);

        webLeadsModelRequest.setCustomWhere(mWebLeadsCustomWheres);
        webLeadsModelRequest.setWherenotin(mWebLeadsWherenotin);

        webLeadsModelRequest.setReportPrefix("Follow up Sales Lead Report");
        webLeadsModelRequest.setTag("android");

        //follow-up date (apply future date over here)
        PWLCustomWhere webLeadsCustomWhere = new PWLCustomWhere();
        Date d = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(d);
        webLeadsCustomWhere.setColumn("dispatched.followup_date");
        webLeadsCustomWhere.setOperator("<=");
        webLeadsCustomWhere.setValue(formattedDate + " 23:59:59");
        webLeadsCustomWheresList.add(webLeadsCustomWhere);
        //End

        setCustomWhereWebLeads(webLeadsCustomWhere);

        //Posted Followup Date Filter
        setPostedFollowupDateInfo();

        //Update Lead Status
        PWLWhereIn webleadsWhereIn = new PWLWhereIn();
        setLeadStatus(webleadsWhereIn);
        webLeadsModelRequest.setWhereIn(mWherein);

        webLeadsModelRequest.setCustomWhere(webLeadsCustomWheresList);
        proc_swipeRefresh.setRefreshing(false);
        sendWebLeadFollowupDetails(getContext(), webLeadsModelRequest);
    }

    private void sendWebLeadFollowupDetails(final Context mContext, PWLRequest request) {
        SpinnerManager.showSpinner(mContext);
        WebLeadService.procWebLead(mContext, request, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);

                Response<PWLResponse> mRes = (Response<PWLResponse>) obj;
                PWLResponse mData = mRes.body();
                String str = new Gson().toJson(mData, PWLResponse.class);
                Log.i(TAG, "OnSuccess: " + str);
                if (!mData.getStatus().equals("failure")) {
                    negotiationLeadCount = mData.getTotal();
                    List<PWLDatum> mlist = mData.getData();
                    PWLDatum mWebLeadsDatum = null;
                    for (int i = 0; i < mlist.size(); i++) {
                        mWebLeadsDatum = mlist.get(i);
                        if (mWebLeadsDatum.getStatus() != null && !mWebLeadsDatum.getStatus().equalsIgnoreCase("Sold") &&
                                !mWebLeadsDatum.getStatus().equalsIgnoreCase("Lost") &&
                                !mWebLeadsDatum.getStatus().equalsIgnoreCase("Closed") &&
                                !mWebLeadsDatum.getStatus().equalsIgnoreCase("Bought") &&
                                !mWebLeadsDatum.getStatus().equalsIgnoreCase("Not-Interested") &&
                                !mWebLeadsDatum.getStatus().equalsIgnoreCase("Finalised")) {
                            mFollowuplist.add(mWebLeadsDatum);
                        }
                    }
                    if (isLoadmore) {
                        madapter.notifyItemInserted(mLeadsDatumList.size() - 1);
                        mLeadsDatumList.addAll(mFollowuplist);
                        madapter.notifyDataSetChanged();
                    } else {
                        mLeadsDatumList = new ArrayList<>();
                        mLeadsDatumList.addAll(mFollowuplist);
                        attachtoAdapter(mLeadsDatumList);
                    }
                    isLeadsAvailable(negotiationLeadCount);
                } else {
                    WebServicesCall.error_popup2(getActivity(), Integer.parseInt(mData.getStatusCode().toString()), "");
                }


            }

            @Override
            public void OnFailure(Throwable t) {
                SpinnerManager.hideSpinner(mContext);
                t.printStackTrace();
            }
        });
    }

    private void prepareWebLeadRequest() {
        webLeadsCustomWheresList = new ArrayList<>();
        mWherein = new ArrayList<>();
        mWhereorList = new ArrayList<>();
        mWebLeadsWherenotin = new ArrayList<String>();
        mWebLeadsCustomWheres = new ArrayList<>();

        // new chamges in email
        //PWLRequest webLeadsModelRequest = new PWLRequest();
        webLeadsModelRequest = new PWLRequest();

        webLeadsModelRequest.setFilterByFields("");
        webLeadsModelRequest.setPerPage(Integer.toString(pageItem));
        webLeadsModelRequest.setPage(page_no);

        webLeadsModelRequest.setAccessToken(CommonMethods.getstringvaluefromkey(getActivity(), "access_token"));

        //Search input Values
        searchbyWebLeads(mWhereorList);

        webLeadsModelRequest.setWhereOr(mWhereorList);

        webLeadsModelRequest.setAliasFields("leads.created_at as PostedDate,leads.created_at as LeadDate,leads.id as id,dispatched.id as dispatchid,leads.customer_name as Name,leads.customer_mobile as Mobile,leads.customer_email as Email,leads.primary_make,leads.primary_model,leads.primary_variant,leads.secondary_make,leads.secondary_model,leads.secondary_variant,leads.register_no as regno,leads.register_month as regmonth,leads.register_year as regyear,leads.color,leads.kms,mfg_year,leads.owner as owners,leads.register_city,leads.stock_listed_price as price,dispatched.status,dispatched.followup_comments as remark,dispatched.followup_date as FollowUpDate,dispatched.employee_name as ExecutiveName,leads.lead_type");
        sortbyWebLeads(webLeadsModelRequest);

        webLeadsModelRequest.setCustomWhere(mWebLeadsCustomWheres);
        webLeadsModelRequest.setWherenotin(mWebLeadsWherenotin);

        webLeadsModelRequest.setReportPrefix("Follow up Sales Lead Report");
        webLeadsModelRequest.setTag("android");

        PWLCustomWhere webLeadsCustomWhere = new PWLCustomWhere();
        setCustomWhereWebLeads(webLeadsCustomWhere);

        //Posted Followup Date Filter
        setPostedFollowupDateInfo();


        //Update Lead Status
        PWLWhereIn webleadsWhereIn = new PWLWhereIn();
        setLeadStatus(webleadsWhereIn);

        webLeadsModelRequest.setWhereIn(mWherein);
        webLeadsModelRequest.setCustomWhere(webLeadsCustomWheresList);

        proc_swipeRefresh.setRefreshing(false);


        sendWebLeadDetails(getContext(), webLeadsModelRequest);

        /*Log.i(TAG, "OnSuccess: "+postedfollDateHashMap);
        postedfollDateHashMap = ProcLeadFilterSaveInstance.getInstance().getSavedatahashmap();
        Log.i(TAG, "OnSuccess: "+postedfollDateHashMap);*/

    }

    private void sendWebLeadDetails(final Context mContext, PWLRequest request) {
        SpinnerManager.showSpinner(mContext);

        WebLeadService.procWebLead(mContext, request, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                Response<PWLResponse> mRes = (Response<PWLResponse>) obj;
                PWLResponse mData = mRes.body();

                String str = new Gson().toJson(mData,PWLResponse.class);
                Log.i(TAG, "OnSuccess: " + str);

                if (!mData.getStatus().equals("failure")) {
                    negotiationLeadCount = mData.getTotal();
                    List<PWLDatum> mlist = mData.getData();

                    if (isLoadmore) {
                        madapter.notifyItemInserted(mLeadsDatumList.size() - 1);
                        mLeadsDatumList.addAll(mlist);
                        madapter.notifyDataSetChanged();
                    } else {
                        mLeadsDatumList = new ArrayList<>();
                        mLeadsDatumList.addAll(mlist);
                        attachtoAdapter(mLeadsDatumList);
                    }
                    isLeadsAvailable(negotiationLeadCount);


                } else {
                    WebServicesCall.error_popup2(getActivity(), Integer.parseInt(mData.getStatusCode().toString()), "");
                }


            }

            @Override
            public void OnFailure(Throwable t) {
                SpinnerManager.hideSpinner(mContext);
                t.printStackTrace();
            }
        });

    }

    private void sendEmailWebLeadDetails(final Context mContext, PWLRequest request) {
        SpinnerManager.showSpinner(mContext);

        PrivateLeadService.sendEmailProcpostWebLead(request, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                Response<String> mRes = (Response<String>) obj;
                String mData = mRes.body();
                CommonMethods.alertMessage(getActivity(), "Mail sent" + "\n" + mData.toString());

            }

            @Override
            public void OnFailure(Throwable t) {
                SpinnerManager.hideSpinner(mContext);
                t.printStackTrace();
            }
        });

    }


    private void isLeadsAvailable(int negotiationLeadCount) {


        if (negotiationLeadCount == 0) {
            proc_noResults.setVisibility(View.VISIBLE);
            proc_swipeRefresh.setVisibility(View.GONE);
        } else {
            proc_noResults.setVisibility(View.GONE);
            proc_swipeRefresh.setVisibility(View.VISIBLE);
        }
    }

    private void sortbyWebLeads(PWLRequest webLeadsModelRequest) {

        if (ProcSortInstanceLeadSection.getInstance().getSortby().equals("posted_date_l_o")) {
            webLeadsModelRequest.setOrderBy("leads.created_at");
            webLeadsModelRequest.setOrderByReverse("true");
        } else if (ProcSortInstanceLeadSection.getInstance().getSortby().equals("posted_date_o_l")) {
            webLeadsModelRequest.setOrderBy("leads.created_at");
            webLeadsModelRequest.setOrderByReverse("false");
        } else if (ProcSortInstanceLeadSection.getInstance().getSortby().equals("folowup_date_l_o")) {
            webLeadsModelRequest.setOrderBy("dispatched.followup_date");
            webLeadsModelRequest.setOrderByReverse("true");
        } else if (ProcSortInstanceLeadSection.getInstance().getSortby().equals("folowup_date_o_l")) {
            webLeadsModelRequest.setOrderBy("dispatched.followup_date");
            webLeadsModelRequest.setOrderByReverse("false");
        } else {
            webLeadsModelRequest.setOrderBy("leads.created_at");
            webLeadsModelRequest.setOrderByReverse("true");
        }
    }

    private void setCustomWhereWebLeads(PWLCustomWhere webLeadsCustomWhere) {
        //Add Dealer Id into DashboardCustomWhere
        webLeadsCustomWhere.setColumn("dispatched.target_id");
        webLeadsCustomWhere.setOperator("=");
        webLeadsCustomWhere.setValue(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"));
        webLeadsCustomWheresList.add(webLeadsCustomWhere);
        //End

        //Add Target Source into DashboardCustomWhere
        webLeadsCustomWhere = new PWLCustomWhere();
        webLeadsCustomWhere.setColumn("leads.lead_type");
        webLeadsCustomWhere.setOperator("=");
        webLeadsCustomWhere.setValue("procurement");
        webLeadsCustomWheresList.add(webLeadsCustomWhere);

        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_DEALER_CRE) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_SALES) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_PROCUREMENT)) {
            webLeadsCustomWhere = new PWLCustomWhere();
            webLeadsCustomWhere.setColumn("dispatched.employee_id");
            webLeadsCustomWhere.setOperator("=");
            webLeadsCustomWhere.setValue(CommonMethods.getstringvaluefromkey(activity, "user_id"));
            webLeadsCustomWheresList.add(webLeadsCustomWhere);
        }
    }

    private void setPostedFollowupDateInfo() {

        postedfollDateHashMap = ProcLeadFilterSaveInstance.getInstance().getSavedatahashmap();

        String postdateinfo = "";

        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_TODAY) != null) {
            postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_TODAY);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_YESTER) != null) {
            postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_YESTER);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_7DAYS) != null) {
            postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_7DAYS);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_15DAYS) != null) {
            postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_15DAYS);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE) != null) {
            postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE);
        }

        //   Log.e("postdateinfo ", "LEAD_POST_CUSTDATE " + postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE));
        PWLCustomWhere postDateCustomWhereStart = new PWLCustomWhere();
        PWLCustomWhere postDateCustomWhereEnd = new PWLCustomWhere();

        if (CommonMethods.getstringvaluefromkey(getActivity(), "user_type").equalsIgnoreCase("dealer")) {

            if (postdateinfo != null && postdateinfo.contains("@")) {
                //split the date and atach to query
                String[] createddates = postdateinfo.split("@");

                try {
                    postDateCustomWhereStart.setColumn("leads.created_at");
                    postDateCustomWhereEnd.setColumn("leads.created_at");
                    if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_TODAY) != null ||
                            postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_YESTER) != null ||
                            postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_7DAYS) != null ||
                            postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_15DAYS) != null) {
                        postDateCustomWhereStart.setOperator(">=");
                        postDateCustomWhereEnd.setOperator("<");
                        postDateCustomWhereStart.setValue(createddates[0]);
                        postDateCustomWhereEnd.setValue(createddates[1]);
                    }

                    if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE) != null) {
                        postDateCustomWhereStart.setOperator(">=");
                        postDateCustomWhereEnd.setOperator("<=");
                        postDateCustomWhereStart.setValue(createddates[0] + " 00:00:00");
                        postDateCustomWhereEnd.setValue(createddates[1] + " 23:59:59");
                    }

                    webLeadsCustomWheresList.add(postDateCustomWhereStart);
                    webLeadsCustomWheresList.add(postDateCustomWhereEnd);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {

            if (CommonMethods.getstringvaluefromkey(getActivity(), "leadStatus").equalsIgnoreCase("true")) {

                try {
                    // remove
                    ProcLeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());
                    ProcLeadFilterSaveInstance.getInstance().setStatusarray(new JSONArray());

                } catch (Exception e) {

                }


                if (CommonMethods.getstringvaluefromkey(getActivity(), "value2").equalsIgnoreCase("")) {
                    try {
                        postDateCustomWhereStart.setColumn("leads.created_at");
                        postDateCustomWhereEnd.setColumn("leads.created_at");

                        if (CommonMethods.getstringvaluefromkey(getActivity(), "leads_status")
                                .equalsIgnoreCase("Procurement Leads Created yesterday")) {

                            postedfollDateHashMap.put(LeadConstantsSection.LEAD_POST_YESTER, leadConstantsSection.getYesterdayDate());
                            postedfollDateHashMap.remove(LeadConstantsSection.LEAD_FOLUP_7DAYS);
                            postedfollDateHashMap.remove(LeadConstantsSection.LEAD_POST_CUSTDATE);

                            ProcLeadFilterSaveInstance.getInstance().setSavedatahashmap(postedfollDateHashMap);
                        }


                        String startDate = CommonMethods.getstringvaluefromkey(getActivity(), "startDate").toString();
                        String endDate = CommonMethods.getstringvaluefromkey(getActivity(), "endDate").toString();

                        if (!startDate.equalsIgnoreCase("")) {

                            if (!startDate.equalsIgnoreCase("")) {
                                postDateCustomWhereStart.setOperator(">=");
                                postDateCustomWhereEnd.setOperator("<");
                                postDateCustomWhereStart.setValue(startDate);
                                postDateCustomWhereEnd.setValue(endDate);

                                postDateCustomWhereStart.setOperator(">=");
                                postDateCustomWhereEnd.setOperator("<=");
                                postDateCustomWhereStart.setValue(startDate + " 00:00:00");
                                postDateCustomWhereEnd.setValue(endDate + " 23:59:59");


                                webLeadsCustomWheresList.add(postDateCustomWhereStart);
                                webLeadsCustomWheresList.add(postDateCustomWhereEnd);
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            } else {


                if (postdateinfo != null && postdateinfo.contains("@")) {
                    //split the date and atach to query
                    String[] createddates = postdateinfo.split("@");

                    try {
                        postDateCustomWhereStart.setColumn("leads.created_at");
                        postDateCustomWhereEnd.setColumn("leads.created_at");
                        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_TODAY) != null ||
                                postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_YESTER) != null ||
                                postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_7DAYS) != null ||
                                postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_15DAYS) != null) {
                            postDateCustomWhereStart.setOperator(">=");
                            postDateCustomWhereEnd.setOperator("<");
                            postDateCustomWhereStart.setValue(createddates[0]);
                            postDateCustomWhereEnd.setValue(createddates[1]);
                        }

                        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE) != null) {
                            postDateCustomWhereStart.setOperator(">=");
                            postDateCustomWhereEnd.setOperator("<=");
                            postDateCustomWhereStart.setValue(createddates[0] + " 00:00:00");
                            postDateCustomWhereEnd.setValue(createddates[1] + " 23:59:59");
                        }

                        webLeadsCustomWheresList.add(postDateCustomWhereStart);
                        webLeadsCustomWheresList.add(postDateCustomWhereEnd);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }

        }

        String followdateinfo = "";

        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TMRW) != null) {
            followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TMRW);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TODAY) != null) {
            followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TODAY);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_YESTER) != null) {
            followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_YESTER);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_7DAYS) != null) {
            followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_7DAYS);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_CUSTDATE) != null) {
            followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_CUSTDATE);
        }

        PWLCustomWhere followDateCustomWhereStart = new PWLCustomWhere();
        PWLCustomWhere followDateCustomWhereEnd = new PWLCustomWhere();
        if (followdateinfo != null && followdateinfo.contains("@")&&
                CommonMethods.getstringvaluefromkey(getActivity(), "Lvalue").equalsIgnoreCase("")) {

            String[] follow = followdateinfo.split("@");
            try {

                followDateCustomWhereStart.setColumn("dispatched.followup_date");
                followDateCustomWhereEnd.setColumn("dispatched.followup_date");
                if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TMRW) != null) {
                    followDateCustomWhereStart.setOperator(">");
                    followDateCustomWhereEnd.setOperator("<=");
                    followDateCustomWhereStart.setValue(follow[1]);
                    followDateCustomWhereEnd.setValue(follow[0]);
                }
                if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TODAY) != null ||
                        postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_YESTER) != null ||
                        postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_7DAYS) != null) {
                    followDateCustomWhereStart.setOperator(">=");
                    followDateCustomWhereEnd.setOperator("<");
                    followDateCustomWhereStart.setValue(follow[0]);
                    followDateCustomWhereEnd.setValue(follow[1]);
                }
                if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_CUSTDATE) != null) {
                    followDateCustomWhereStart.setOperator(">=");
                    followDateCustomWhereEnd.setOperator("<=");
                    followDateCustomWhereStart.setValue(follow[0] + " 00:00:00");
                    followDateCustomWhereEnd.setValue(follow[1] + " 23:59:59");
                }

                webLeadsCustomWheresList.add(followDateCustomWhereStart);
                webLeadsCustomWheresList.add(followDateCustomWhereEnd);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {


            if (CommonMethods.getstringvaluefromkey(getActivity(), "leadStatus").equalsIgnoreCase("true")
                    && !CommonMethods.getstringvaluefromkey(getActivity(), "value2").equalsIgnoreCase("")
                    ||CommonMethods.getstringvaluefromkey(getActivity(), "withoutfollowup").equalsIgnoreCase("yes")) {


                if (CommonMethods.getstringvaluefromkey(getActivity(), "leads_status")
                        .equalsIgnoreCase("Procurement Leads Followup this week")) {

                    postedfollDateHashMap.remove(LeadConstantsSection.LEAD_POST_YESTER);
                    postedfollDateHashMap.put(LeadConstantsSection.LEAD_FOLUP_7DAYS, leadConstantsSection.getFollowlast7Date());
                    ProcLeadFilterSaveInstance.getInstance().setSavedatahashmap(postedfollDateHashMap);
                }


                followDateCustomWhereStart.setColumn("dispatched.followup_date");
                followDateCustomWhereEnd.setColumn("dispatched.followup_date");


                followDateCustomWhereStart.setOperator(CommonMethods.getstringvaluefromkey(getActivity(), "operator"));
                followDateCustomWhereEnd.setOperator(CommonMethods.getstringvaluefromkey(getActivity(), "operator2"));

                followDateCustomWhereStart.setValue(CommonMethods.getstringvaluefromkey(getActivity(), "value"));
                followDateCustomWhereEnd.setValue(CommonMethods.getstringvaluefromkey(getActivity(), "value2"));


                webLeadsCustomWheresList.add(followDateCustomWhereStart);
                if(!CommonMethods.getstringvaluefromkey(getActivity(), "operator").equalsIgnoreCase("=")) {
                    webLeadsCustomWheresList.add(followDateCustomWhereEnd);
                }
            }

        }
    }

    @SuppressLint("NewApi")
    private void setLeadStatus(PWLWhereIn webleadsWhereIn) {
        ArrayList<String> listStatus = new ArrayList<String>();
        leadstatusJsonArray = ProcLeadFilterSaveInstance.getInstance().getStatusarray();


        // New ASM
        if (CommonMethods.getstringvaluefromkey(getActivity(), "user_type").equalsIgnoreCase("dealer")) {

            //   Log.e("setLeadStatus ", "json " + leadstatusJsonArray);


            webleadsWhereIn.setColumn("dispatched.status");
            if (leadstatusJsonArray.length() != 0) {

                for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                    try {
                        listStatus.add(leadstatusJsonArray.getString(i));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                webleadsWhereIn.setValue(listStatus);
                mWherein.add(webleadsWhereIn);
            }

        } else {
            if (CommonMethods.getstringvaluefromkey(getActivity(), "leadStatus").equalsIgnoreCase("true")) {

                //remove if already presents in json arry
                if (leadstatusJsonArray.length() != 0) {
                    try {
                        for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                            leadstatusJsonArray.remove(i);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                if (!CommonMethods.getstringvaluefromkey(getActivity(), "Lvalue").equalsIgnoreCase("")
                        && CommonMethods.getstringvaluefromkey(getActivity(), "leads_status").equalsIgnoreCase("")
                        || CommonMethods.getstringvaluefromkey(getActivity(), "withoutfollowup").equalsIgnoreCase("yes")) {

                    try {

                        leadstatusJsonArray.put(CommonMethods.getstringvaluefromkey(getActivity(), "Lvalue"));
                        listStatus.add(CommonMethods.getstringvaluefromkey(getActivity(), "Lvalue"));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    webleadsWhereIn.setColumn("dispatched.status");
                    webleadsWhereIn.setValue(listStatus);
                    mWherein.add(webleadsWhereIn);

                }
            } else {
                webleadsWhereIn.setColumn("dispatched.status");
                if (leadstatusJsonArray.length() != 0) {

                    for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                        try {
                            listStatus.add(leadstatusJsonArray.getString(i));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    webleadsWhereIn.setValue(listStatus);
                    mWherein.add(webleadsWhereIn);
                }


            }
        }

    }

    private void searchbyWebLeads(List<PWLWhereor> mWhereorList) {

        if (!searchQuery.equals("")) {
            //Make
            PWLWhereor vehicle_make = new PWLWhereor();
            vehicle_make.setColumn("leads.primary_make");
            vehicle_make.setOperator("like");
            vehicle_make.setValue("%" + searchQuery.trim() + "%");
            mWhereorList.add(vehicle_make);

            //Model
            PWLWhereor vehicle_model = new PWLWhereor();
            vehicle_model.setColumn("leads.primary_model");
            vehicle_model.setOperator("like");
            vehicle_model.setValue("%" + searchQuery.trim() + "%");
            mWhereorList.add(vehicle_model);

            //Variant
            PWLWhereor vehicle_variant = new PWLWhereor();
            vehicle_variant.setColumn("leads.primary_variant");
            vehicle_variant.setOperator("like");
            vehicle_variant.setValue("%" + searchQuery.trim() + "%");
            mWhereorList.add(vehicle_variant);

            //Name
            PWLWhereor customer_name = new PWLWhereor();
            customer_name.setColumn("leads.customer_name");
            customer_name.setOperator("like");
            customer_name.setValue("%" + searchQuery.trim() + "%");
            mWhereorList.add(customer_name);

            //Mobile
            PWLWhereor customer_mobile = new PWLWhereor();
            customer_mobile.setColumn("leads.customer_mobile");
            customer_mobile.setOperator("like");
            customer_mobile.setValue("%" + searchQuery.trim() + "%");
            mWhereorList.add(customer_mobile);

        }
    }

}
