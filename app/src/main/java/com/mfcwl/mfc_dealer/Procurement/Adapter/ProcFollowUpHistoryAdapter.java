package com.mfcwl.mfc_dealer.Procurement.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.Activity.Leads.LeadsInstance;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PPFHFollowup;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.MonthUtility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

public class ProcFollowUpHistoryAdapter extends RecyclerView.Adapter<ProcFollowUpHistoryAdapter.ViewHolder> {
    List<PPFHFollowup> mlist;
    Context context;

    String calculateTime = "";

    public ProcFollowUpHistoryAdapter(Context context, List<PPFHFollowup> mlist) {
        this.mlist = mlist;
        this.context = context;
    }

    @NonNull
    @Override
    public ProcFollowUpHistoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_followup_history_item, parent, false);

        return new ProcFollowUpHistoryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProcFollowUpHistoryAdapter.ViewHolder holder, int position) {

        PPFHFollowup list = mlist.get(position);
        String nextFollowup = "";
        if (LeadsInstance.getInstance().getWhichleads().equals("WebLeads")) {
            String[] splitdatespace = list.getFollowUp().trim().split(" ", 10);
            String[] splitdate1 = splitdatespace[0].split("-", 10);
            try {
                nextFollowup = splitdate1[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate1[1])] + " " + splitdate1[0];
            } catch (Exception e) {

                Log.e("Exception ", "nextFollowup " + e.toString());

            }
        } else {
            Log.e("whproblm ", "first else");
            String[] splitdatespace = list.getFollowUp().trim().split(" ", 10);
            String[] splitdate1 = splitdatespace[0].split("-", 10);
            Log.e("whproblm ", "else");
            try {
                Log.e("whproblm ", "try 1");
                nextFollowup = splitdate1[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate1[1])] + " " + splitdate1[0];
            } catch (Exception e) {

                Log.e("whproblm ", "nextFollowup");

            }
        }


        String sourceString1 = "Next Follow-up " + "<font color=\"black\">" + nextFollowup + "</font>";
        //  Log.e("nextFollowup ", "nextFollowup " + nextFollowup);

        if (list.getLeadStatus().trim().equalsIgnoreCase("Sold") || list.getLeadStatus().trim().equalsIgnoreCase("Lost") || nextFollowup.equalsIgnoreCase("00  0000")) {
            nextFollowup = "NA";
            String sourceString2 = "Next Follow-up " + "<font color=\"black\">" + nextFollowup + "</font>";
            holder.leads_followup_history_next_folowup.setText(Html.fromHtml(sourceString2));
        } else {
            holder.leads_followup_history_next_folowup.setText(Html.fromHtml(sourceString1));
        }
        holder.leads_followup_history_booked.setText(list.getLeadRemark());

        holder.leads_followup_history_warmorcold.setText(list.getLeadRemark());


        String createdDate = "";
        if (LeadsInstance.getInstance().getWhichleads().equals("WebLeads")) {
            String[] splitdatespace = list.getCreatedOn().trim().split(" ", 10);
            String[] splitdate1 = splitdatespace[0].split("-", 10);
            createdDate = splitdate1[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate1[1])] + " " + splitdate1[0];
        } else {
            String[] splitdatespace = list.getCreatedOn().trim().split(" ", 10);
            String[] splitdate1 = splitdatespace[0].split("-", 10);
            createdDate = splitdate1[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate1[1])] + " " + splitdate1[0];
        }

        holder.leads_followup_history_date.setText(createdDate);

        calculateTime = list.getCreatedOn().trim().replace(" ", " ");
        StringTokenizer tk = new StringTokenizer(calculateTime);
        String date = tk.nextToken();
        String time = tk.nextToken();

        /*SimpleDateFormat sdf = new SimpleDateFormat("KK:mm aa");
        SimpleDateFormat sdfs = new SimpleDateFormat("KK:mm aa");*/

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm aa");

        SimpleDateFormat date24Format = new SimpleDateFormat("HH:mm");

        Date dt;
        try {
            dt = sdf.parse(time);
            holder.leads_followup_history_time.setText(sdfs.format(dt));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            Log.e("whproblm ", "time ah");
            e.printStackTrace();
        }

        if (list.getLeadStatus().trim().equalsIgnoreCase("Open")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.open_28);
            holder.leads_followup_history_warmorcold.setText("Open");
        }
        if (list.getLeadStatus().trim().equalsIgnoreCase("Hot")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.hot_28);
            holder.leads_followup_history_warmorcold.setText("Hot");
        }
        if (list.getLeadStatus().trim().equalsIgnoreCase("Warm")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.warm_28);
            holder.leads_followup_history_warmorcold.setText("Warm");
        }
        if (list.getLeadStatus().trim().equalsIgnoreCase("Cold")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.cold_28);
            holder.leads_followup_history_warmorcold.setText("Cold");
        }
        if (list.getLeadStatus().trim().equalsIgnoreCase("Lost")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.lost_28);
            holder.leads_followup_history_warmorcold.setText("Lost");
        }
        if (list.getLeadStatus().trim().equalsIgnoreCase("Sold")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.sold_28);
            holder.leads_followup_history_warmorcold.setText("Sold");
        }
        if (list.getLeadStatus().trim().equalsIgnoreCase("bought") || list.getLeadStatus().trim().equalsIgnoreCase("Bought")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.sold_28);
            holder.leads_followup_history_warmorcold.setText("Bought");
        }
        if (list.getLeadStatus().trim().equalsIgnoreCase("Visiting") || list.getLeadStatus().trim().equalsIgnoreCase("visiting")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.ic_visiting);
            holder.leads_followup_history_warmorcold.setText(list.getLeadStatus().toString());
        }
        if (list.getLeadStatus().trim().equalsIgnoreCase("Search") || list.getLeadStatus().trim().equalsIgnoreCase("search")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.ic_search_lead);
            holder.leads_followup_history_warmorcold.setText(list.getLeadStatus().toString());
        }
        if (list.getLeadStatus().trim().equalsIgnoreCase("Not-Interested") || list.getLeadStatus().trim().equalsIgnoreCase("not-interested")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.ic_not_intrested);
            holder.leads_followup_history_warmorcold.setText(list.getLeadStatus().toString());
        }
        if (list.getLeadStatus().trim().equalsIgnoreCase("Finalised") || list.getLeadStatus().trim().equalsIgnoreCase("finalised")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.ic_finalized);
            holder.leads_followup_history_warmorcold.setText(list.getLeadStatus().toString());
        }
        if (list.getLeadStatus().trim().equalsIgnoreCase("No-Response") || list.getLeadStatus().trim().equalsIgnoreCase("no-response")) {
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.ic_no_responce);
            holder.leads_followup_history_warmorcold.setText(list.getLeadStatus().toString());
        }
        if (list.getLeadStatus().trim().equalsIgnoreCase("") || list.getLeadStatus().trim().equalsIgnoreCase("NA") || list.getLeadStatus().trim().equalsIgnoreCase("null")) {
            holder.leads_followup_history_warmorcold.setText("Open");
            holder.leads_followup_history_warmorcold.setBackgroundResource(R.drawable.open_28);
        }
        if (list.getLeadRemark().equals("") || list.getLeadRemark().equals("NA") || list.getLeadRemark().equals("null")) {
            holder.leads_followup_history_booked.setText("No comments available");
        }
    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView leads_followup_history_date, leads_followup_history_next_folowup, leads_followup_history_booked, leads_followup_history_warmorcold, leads_followup_history_time;

        public ViewHolder(View itemView) {
            super(itemView);
            leads_followup_history_booked = itemView.findViewById(R.id.leads_followup_history_booked);
            leads_followup_history_date = itemView.findViewById(R.id.leads_followup_history_date);
            leads_followup_history_next_folowup = itemView.findViewById(R.id.leads_followup_history_next_folowup);
            leads_followup_history_warmorcold = itemView.findViewById(R.id.leads_followup_history_warmorcold);
            leads_followup_history_time = itemView.findViewById(R.id.leads_followup_history_time);
        }
    }
}

