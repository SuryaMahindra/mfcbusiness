package com.mfcwl.mfc_dealer.Procurement.Singleton;

public class ConvertStockInstance {

    private static ConvertStockInstance mInstance = null;

    private String executiveid;
    private String mfgyear;
    private String mfgmonth;

    private String registeryear;
    private String registermonth;

    private String vehicletype;
    private String make;
    private String model;
    private String variant;

    private String registernumber;
    private String color;
    private String kms;
    private String owner;
    private String registercity;
    private String insurancetype;
    private String insurancedate;

    private String leadid;
    private String leadremark;
    private String leaddate;
    private String dealerid;
    private String exename;

    private ConvertStockInstance() {
        executiveid = "";
        mfgyear = "";
        mfgmonth = "";
        registeryear = "";

        vehicletype = "";
        registermonth = "";
        make = "";
        model = "";
        variant = "";
        registernumber = "";

        color = "";
        kms = "";
        owner = "";
        registercity = "";
        insurancetype = "";
        insurancedate = "";

        leadid = "";
        leadremark = "";
        leaddate = "";
        dealerid = "";
        exename = "";
    }

    public static ConvertStockInstance getInstance() {
        if (mInstance == null) {
            mInstance = new ConvertStockInstance();
        }
        return mInstance;
    }

    public static void clear() {
        mInstance = null;
    }

    public String getExecutiveid() {
        return executiveid;
    }

    public void setExecutiveid(String executiveid) {
        this.executiveid = executiveid;
    }

    public String getRegisteryear() {
        return registeryear;
    }

    public void setRegisteryear(String registeryear) {
        this.registeryear = registeryear;
    }


    public String getVehicletype() {
        return vehicletype;
    }

    public void setVehicletype(String vehicletype) {
        this.vehicletype = vehicletype;
    }

    public String getRegistermonth() {
        return registermonth;
    }

    public void setRegistermonth(String registermonth) {
        this.registermonth = registermonth;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public String getRegisternumber() {
        return registernumber;
    }

    public void setRegisternumber(String registernumber) {
        this.registernumber = registernumber;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getKms() {
        return kms;
    }

    public void setKms(String kms) {
        this.kms = kms;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getRegistercity() {
        return registercity;
    }

    public void setRegistercity(String registercity) {
        this.registercity = registercity;
    }

    public String getInsurancetype() {
        return insurancetype;
    }

    public void setInsurancetype(String insurancetype) {
        this.insurancetype = insurancetype;
    }

    public String getInsurancedate() {
        return insurancedate;
    }

    public void setInsurancedate(String insurancedate) {
        this.insurancedate = insurancedate;
    }

    public String getMfgyear() {
        return mfgyear;
    }

    public void setMfgyear(String mfgyear) {
        this.mfgyear = mfgyear;
    }

    public String getMfgmonth() {
        return mfgmonth;
    }

    public void setMfgmonth(String mfgmonth) {
        this.mfgmonth = mfgmonth;
    }

    public String getLeadid() {
        return leadid;
    }

    public void setLeadid(String leadid) {
        this.leadid = leadid;
    }

    public String getLeadremark() {
        return leadremark;
    }

    public void setLeadremark(String leadremark) {
        this.leadremark = leadremark;
    }

    public String getLeaddate() {
        return leaddate;
    }

    public void setLeaddate(String leaddate) {
        this.leaddate = leaddate;
    }

    public String getDealerid() {
        return dealerid;
    }

    public void setDealerid(String dealerid) {
        this.dealerid = dealerid;
    }

    public String getExename() {
        return exename;
    }

    public void setExename(String exename) {
        this.exename = exename;
    }
}
