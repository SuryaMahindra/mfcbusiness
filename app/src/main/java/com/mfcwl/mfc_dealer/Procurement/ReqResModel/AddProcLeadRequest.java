package com.mfcwl.mfc_dealer.Procurement.ReqResModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddProcLeadRequest {
    
    @SerializedName("lead_type")
    @Expose
    private String leadType;
    @SerializedName("vehicle_type")
    @Expose
    private String vehicleType;
    @SerializedName("lead_date")
    @Expose
    private String leadDate;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("customer_mobile")
    @Expose
    private String customerMobile;
    @SerializedName("customer_email")
    @Expose
    private String customerEmail;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("customer_address")
    @Expose
    private String customerAddress;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("manufacturing_month")
    @Expose
    private String manufacturingMonth;
    @SerializedName("manufacturing_year")
    @Expose
    private String manufacturingYear;
    @SerializedName("make")
    @Expose
    private String make;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("variant")
    @Expose
    private String variant;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("kilometer")
    @Expose
    private String kilometer;
    @SerializedName("owner")
    @Expose
    private String owner;
    @SerializedName("register_month")
    @Expose
    private String registerMonth;
    @SerializedName("register_year")
    @Expose
    private String registerYear;
    @SerializedName("register_no")
    @Expose
    private String registerNo;
    @SerializedName("stock_vinno")
    @Expose
    private String stockVinno;
    @SerializedName("insurance_type")
    @Expose
    private String insuranceType;
    @SerializedName("insurance_expiry")
    @Expose
    private String insuranceExpiry;
    @SerializedName("customer_budget")
    @Expose
    private String customerBudget;
    @SerializedName("buying_quote")
    @Expose
    private String buyingQuote;
    @SerializedName("lead_status")
    @Expose
    private String leadStatus;
    @SerializedName("lead_source")
    @Expose
    private String leadSource;
    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("ref_src")
    @Expose
    private String refSrc;
    @SerializedName("follow_date")
    @Expose
    private String followDate;
    @SerializedName("sales_executive_id")
    @Expose
    private String salesExecutiveId;
    @SerializedName("executive_name")
    @Expose
    private String executiveName;

    @SerializedName("created_by_device")
    @Expose
    private String createdByDevice;

    public String getLeadType() {
        return leadType;
    }

    public void setLeadType(String leadType) {
        this.leadType = leadType;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getLeadDate() {
        return leadDate;
    }

    public void setLeadDate(String leadDate) {
        this.leadDate = leadDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getManufacturingMonth() {
        return manufacturingMonth;
    }

    public void setManufacturingMonth(String manufacturingMonth) {
        this.manufacturingMonth = manufacturingMonth;
    }

    public String getManufacturingYear() {
        return manufacturingYear;
    }

    public void setManufacturingYear(String manufacturingYear) {
        this.manufacturingYear = manufacturingYear;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getKilometer() {
        return kilometer;
    }

    public void setKilometer(String kilometer) {
        this.kilometer = kilometer;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getRegisterMonth() {
        return registerMonth;
    }

    public void setRegisterMonth(String registerMonth) {
        this.registerMonth = registerMonth;
    }

    public String getRegisterYear() {
        return registerYear;
    }

    public void setRegisterYear(String registerYear) {
        this.registerYear = registerYear;
    }

    public String getRegisterNo() {
        return registerNo;
    }

    public void setRegisterNo(String registerNo) {
        this.registerNo = registerNo;
    }

    public String getStockVinno() {
        return stockVinno;
    }

    public void setStockVinno(String stockVinno) {
        this.stockVinno = stockVinno;
    }

    public String getInsuranceType() {
        return insuranceType;
    }

    public void setInsuranceType(String insuranceType) {
        this.insuranceType = insuranceType;
    }

    public String getInsuranceExpiry() {
        return insuranceExpiry;
    }

    public void setInsuranceExpiry(String insuranceExpiry) {
        this.insuranceExpiry = insuranceExpiry;
    }

    public String getCustomerBudget() {
        return customerBudget;
    }

    public void setCustomerBudget(String customerBudget) {
        this.customerBudget = customerBudget;
    }

    public String getBuyingQuote() {
        return buyingQuote;
    }

    public void setBuyingQuote(String buyingQuote) {
        this.buyingQuote = buyingQuote;
    }

    public String getLeadStatus() {
        return leadStatus;
    }

    public void setLeadStatus(String leadStatus) {
        this.leadStatus = leadStatus;
    }

    public String getLeadSource() {
        return leadSource;
    }

    public void setLeadSource(String leadSource) {
        this.leadSource = leadSource;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRefSrc() {
        return refSrc;
    }

    public void setRefSrc(String refSrc) {
        this.refSrc = refSrc;
    }

    public String getFollowDate() {
        return followDate;
    }

    public void setFollowDate(String followDate) {
        this.followDate = followDate;
    }

    public String getSalesExecutiveId() {
        return salesExecutiveId;
    }

    public void setSalesExecutiveId(String salesExecutiveId) {
        this.salesExecutiveId = salesExecutiveId;
    }

    public String getExecutiveName() {
        return executiveName;
    }

    public void setExecutiveName(String executiveName) {
        this.executiveName = executiveName;
    }

    public String getCreatedByDevice() {
        return createdByDevice;
    }

    public void setCreatedByDevice(String createdByDevice) {
        this.createdByDevice = createdByDevice;
    }

}