package com.mfcwl.mfc_dealer.Procurement.ReqResModel;

public class PWLead {

    private String postedDate;
    private String leadDate;

    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public String getLeadDate() {
        return leadDate;
    }

    public void setLeadDate(String leadDate) {
        this.leadDate = leadDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPrimaryMake() {
        return primaryMake;
    }

    public void setPrimaryMake(String primaryMake) {
        this.primaryMake = primaryMake;
    }

    public String getPrimaryModel() {
        return primaryModel;
    }

    public void setPrimaryModel(String primaryModel) {
        this.primaryModel = primaryModel;
    }

    public String getPrimaryVariant() {
        return primaryVariant;
    }

    public void setPrimaryVariant(String primaryVariant) {
        this.primaryVariant = primaryVariant;
    }

    public String getSecondaryMake() {
        return secondaryMake;
    }

    public void setSecondaryMake(String secondaryMake) {
        this.secondaryMake = secondaryMake;
    }

    public String getSecondaryModel() {
        return secondaryModel;
    }

    public void setSecondaryModel(String secondaryModel) {
        this.secondaryModel = secondaryModel;
    }

    public String getSecondaryVariant() {
        return secondaryVariant;
    }

    public void setSecondaryVariant(String secondaryVariant) {
        this.secondaryVariant = secondaryVariant;
    }

    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getRegmonth() {
        return regmonth;
    }

    public void setRegmonth(String regmonth) {
        this.regmonth = regmonth;
    }

    public String getRegyear() {
        return regyear;
    }

    public void setRegyear(String regyear) {
        this.regyear = regyear;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getKms() {
        return kms;
    }

    public void setKms(String kms) {
        this.kms = kms;
    }

    public String getMfgYear() {
        return mfgYear;
    }

    public void setMfgYear(String mfgYear) {
        this.mfgYear = mfgYear;
    }

    public String getOwners() {
        return owners;
    }

    public void setOwners(String owners) {
        this.owners = owners;
    }

    public String getRegisterCity() {
        return registerCity;
    }

    public void setRegisterCity(String registerCity) {
        this.registerCity = registerCity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFollowUpDate() {
        return followUpDate;
    }

    public void setFollowUpDate(String followUpDate) {
        this.followUpDate = followUpDate;
    }

    public String getExecutiveName() {
        return executiveName;
    }

    public void setExecutiveName(String executiveName) {
        this.executiveName = executiveName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDispatchid() {
        return dispatchid;
    }

    public void setDispatchid(Integer dispatchid) {
        this.dispatchid = dispatchid;
    }

    private String name;
    private String mobile;
    private String email;
    private String primaryMake;
    private String primaryModel;
    private String primaryVariant;
    private String secondaryMake;
    private String secondaryModel;
    private String secondaryVariant;
    private String regno;
    private String regmonth;
    private String regyear;
    private String color;
    private String kms;
    private String mfgYear;
    private String owners;
    private String registerCity;
    private String price;
    private String status;
    private String followUpDate;
    private String executiveName;
    private Integer id;
    private Integer dispatchid;

}
