package com.mfcwl.mfc_dealer.Procurement.Instances;

public class ProcSortInstanceLeadSection {
    private static ProcSortInstanceLeadSection mInstance = null;
    private String whichsort;
    private String sortby;

    private ProcSortInstanceLeadSection() {
        whichsort = "";
        sortby = "";

    }

    public static ProcSortInstanceLeadSection getInstance() {
        if (mInstance == null) {
            mInstance = new ProcSortInstanceLeadSection();
        }
        return mInstance;
    }

    public String getWhichsort() {
        return whichsort;
    }

    public void setWhichsort(String whichsort) {
        this.whichsort = whichsort;
    }

    public String getSortby() {
        return sortby;
    }

    public void setSortby(String sortby) {
        this.sortby = sortby;
    }
}
