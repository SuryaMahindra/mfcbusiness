package com.mfcwl.mfc_dealer.Procurement.ConvertToStockRequestResponse;

import android.content.Context;

import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.Procurement.BaseService.ProcPrivateLeadBaseService;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class ConvertStockService extends ProcPrivateLeadBaseService {

    public static void PconvertStock(ConvertStockRequest convertStockRequest, final HttpCallResponse mHttpCallResponse) {
        ConverStockInterface mInterFace = retrofit.create(ConverStockInterface.class);
        Call<ConvertStockResponse> mCall = mInterFace.convertStock(convertStockRequest);
        mCall.enqueue(new Callback<ConvertStockResponse>() {
            @Override
            public void onResponse(Call<ConvertStockResponse> call, Response<ConvertStockResponse> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<ConvertStockResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });

    }

    public interface ConverStockInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("procurement/converttostock")
        Call<ConvertStockResponse> convertStock(@Body ConvertStockRequest convertStockRequest);
    }


}