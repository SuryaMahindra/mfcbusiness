package com.mfcwl.mfc_dealer.Procurement.PLDetailsRequestResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FollowUpdateRequest {
    @SerializedName("LeadId")
    @Expose
    private String leadId;
    @SerializedName("LeadType")
    @Expose
    private String leadType;
    @SerializedName("LeadStatus")
    @Expose
    private String leadStatus;
    @SerializedName("LeadRemark")
    @Expose
    private String leadRemark;
    @SerializedName("Followup")
    @Expose
    private String followup;
    @SerializedName("executive_id")
    @Expose
    private String executiveId;
    @SerializedName("executive_name")
    @Expose
    private String executiveName;
    @SerializedName("updated_by_device")
    @Expose
    private String updatedByDevice;

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getLeadType() {
        return leadType;
    }

    public void setLeadType(String leadType) {
        this.leadType = leadType;
    }

    public String getLeadStatus() {
        return leadStatus;
    }

    public void setLeadStatus(String leadStatus) {
        this.leadStatus = leadStatus;
    }

    public String getLeadRemark() {
        return leadRemark;
    }

    public void setLeadRemark(String leadRemark) {
        this.leadRemark = leadRemark;
    }

    public String getFollowup() {
        return followup;
    }

    public void setFollowup(String followup) {
        this.followup = followup;
    }

    public String getExecutiveId() {
        return executiveId;
    }

    public void setExecutiveId(String executiveId) {
        this.executiveId = executiveId;
    }

    public String getExecutiveName() {
        return executiveName;
    }

    public void setExecutiveName(String executiveName) {
        this.executiveName = executiveName;
    }

    public String getUpdatedByDevice() {
        return updatedByDevice;
    }

    public void setUpdatedByDevice(String updatedByDevice) {
        this.updatedByDevice = updatedByDevice;
    }
}
