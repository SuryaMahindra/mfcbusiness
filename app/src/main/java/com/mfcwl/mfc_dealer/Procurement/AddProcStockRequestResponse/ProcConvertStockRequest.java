package com.mfcwl.mfc_dealer.Procurement.AddProcStockRequestResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProcConvertStockRequest {
    @SerializedName("lead_id")
    @Expose
    private String leadId;
    @SerializedName("cng_kit")
    @Expose
    private String cngKit;
    @SerializedName("register_no")
    @Expose
    private String registerNo;
    @SerializedName("chassis_number")
    @Expose
    private String chassisNumber;
    @SerializedName("engine_number")
    @Expose
    private String engineNumber;
    @SerializedName("dealer_price")
    @Expose
    private String dealerPrice;
    @SerializedName("is_offload")
    @Expose
    private String isOffload;
    @SerializedName("is_featured_car")
    @Expose
    private String isFeaturedCar;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("LeadRemark")
    @Expose
    private String leadRemark;
    @SerializedName("Followup")
    @Expose
    private String followup;
    @SerializedName("updated_by_device")
    @Expose
    private String updatedByDevice;
    @SerializedName("refurbishment_cost")
    @Expose
    private String refurbishmentCost;
    @SerializedName("actual_selling_price")
    @Expose
    private String actualSellingPrice;

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getCngKit() {
        return cngKit;
    }

    public void setCngKit(String cngKit) {
        this.cngKit = cngKit;
    }

    public String getRegisterNo() {
        return registerNo;
    }

    public void setRegisterNo(String registerNo) {
        this.registerNo = registerNo;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public String getEngineNumber() {
        return engineNumber;
    }

    public void setEngineNumber(String engineNumber) {
        this.engineNumber = engineNumber;
    }

    public String getDealerPrice() {
        return dealerPrice;
    }

    public void setDealerPrice(String dealerPrice) {
        this.dealerPrice = dealerPrice;
    }

    public String getIsOffload() {
        return isOffload;
    }

    public void setIsOffload(String isOffload) {
        this.isOffload = isOffload;
    }

    public String getIsFeaturedCar() {
        return isFeaturedCar;
    }

    public void setIsFeaturedCar(String isFeaturedCar) {
        this.isFeaturedCar = isFeaturedCar;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLeadRemark() {
        return leadRemark;
    }

    public void setLeadRemark(String leadRemark) {
        this.leadRemark = leadRemark;
    }

    public String getFollowup() {
        return followup;
    }

    public void setFollowup(String followup) {
        this.followup = followup;
    }

    public String getUpdatedByDevice() {
        return updatedByDevice;
    }

    public void setUpdatedByDevice(String updatedByDevice) {
        this.updatedByDevice = updatedByDevice;
    }

    public String getRefurbishmentCost() {
        return refurbishmentCost;
    }

    public void setRefurbishmentCost(String refurbishmentCost) {
        this.refurbishmentCost = refurbishmentCost;
    }

    public String getActualSellingPrice() {
        return actualSellingPrice;
    }

    public void setActualSellingPrice(String actualSellingPrice) {
        this.actualSellingPrice = actualSellingPrice;
    }

}
