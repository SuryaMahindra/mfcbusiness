package com.mfcwl.mfc_dealer.Procurement.ReqResModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mfcwl.mfc_dealer.DasBoardModels.CustomWhere;

import java.util.List;

public class DashboardWebLeadsRequest {
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("alias_fields")
    @Expose
    private String aliasFields;
    @SerializedName("custom_where")
    @Expose
    private List<DashboardCustomWhere> customWhere = null;
    @SerializedName("order_by")
    @Expose
    private String orderBy;
    @SerializedName("order_by_reverse")
    @Expose
    private String orderByReverse;
    @SerializedName("page")
    @Expose
    private String page;
    @SerializedName("per_page")
    @Expose
    private String perPage;
    @SerializedName("wherenotin")
    @Expose
    private List<Object> wherenotin = null;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAliasFields() {
        return aliasFields;
    }

    public void setAliasFields(String aliasFields) {
        this.aliasFields = aliasFields;
    }

    public List<DashboardCustomWhere> getCustomWhere() {
        return customWhere;
    }

    public void setCustomWhere(List<DashboardCustomWhere> customWhere) {
        this.customWhere = customWhere;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getOrderByReverse() {
        return orderByReverse;
    }

    public void setOrderByReverse(String orderByReverse) {
        this.orderByReverse = orderByReverse;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getPerPage() {
        return perPage;
    }

    public void setPerPage(String perPage) {
        this.perPage = perPage;
    }

    public List<Object> getWherenotin() {
        return wherenotin;
    }

    public void setWherenotin(List<Object> wherenotin) {
        this.wherenotin = wherenotin;
    }
}
