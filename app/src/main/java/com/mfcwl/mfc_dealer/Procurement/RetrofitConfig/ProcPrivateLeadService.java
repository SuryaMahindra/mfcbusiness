package com.mfcwl.mfc_dealer.Procurement.RetrofitConfig;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.Model.LeadSection.PrivateLeadsModelRequest;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.PayUPayment.PayUPaymentResponse;
import com.mfcwl.mfc_dealer.PayUPayment.PaymentRequest;
import com.mfcwl.mfc_dealer.Procurement.AddProcStockRequestResponse.ProcConvertStockRequest;
import com.mfcwl.mfc_dealer.Procurement.AddProcStockRequestResponse.ProcConvertStockResponse;
import com.mfcwl.mfc_dealer.Procurement.BaseService.ProcPrivateLeadBaseService;
import com.mfcwl.mfc_dealer.Procurement.PLDetailsRequestResponse.FollowUpdateRequest;
import com.mfcwl.mfc_dealer.Procurement.PLDetailsRequestResponse.FollowUpdateResponse;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PPFHRequest;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PPFHResponse;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PPLResponse;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PWLRequest;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public class ProcPrivateLeadService extends ProcPrivateLeadBaseService {

    public static void ProcPrivateLead(Context context, PrivateLeadsModelRequest privateLeadRequest, final HttpCallResponse mHttpCallResponse) {
        ProcPrivateLeadService.PrivateLeadsInterFace mInterFace = retrofit.create(ProcPrivateLeadService.PrivateLeadsInterFace.class);
        Call<PPLResponse> mCall = mInterFace.privateleads(privateLeadRequest);
        Log.i("TAG", " "+mCall.request().url().toString());
        String str = new Gson().toJson(privateLeadRequest, PrivateLeadsModelRequest.class);
        Log.i("TAG", " "+str);
        mCall.enqueue(new Callback<PPLResponse>() {
            @Override
            public void onResponse(Call<PPLResponse> call, Response<PPLResponse> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), context);
                }
            }

            @Override
            public void onFailure(Call<PPLResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });


    }

    public static void sendEmailProcPrivateLead(Context context, PrivateLeadsModelRequest privateLeadRequest, final HttpCallResponse mHttpCallResponse) {
        ProcPrivateLeadService.senEmailPrivateLeadsInterFace mInterFace = retrofit.create(ProcPrivateLeadService.senEmailPrivateLeadsInterFace.class);
        Call<String> mCall = mInterFace.privateleads(privateLeadRequest);
        mCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), context);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });


    }


    public static void PPFHLead(Context context, PPFHRequest privateLeadRequest, final HttpCallResponse mHttpCallResponse) {
        ProcPrivateLeadService.PPFHLeadsInterFace mInterFace = retrofit.create(ProcPrivateLeadService.PPFHLeadsInterFace.class);
        Call<PPFHResponse> mCall = mInterFace.privateleads(privateLeadRequest);
        mCall.enqueue(new Callback<PPFHResponse>() {
            @Override
            public void onResponse(Call<PPFHResponse> call, Response<PPFHResponse> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), context);
                }
            }

            @Override
            public void onFailure(Call<PPFHResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
                int x = 10;
            }
        });


    }

    //Update Follow Up

    public static void PPFollowUpdate(Context context, FollowUpdateRequest privateLeadRequest, final HttpCallResponse mHttpCallResponse) {
        ProcPrivateLeadService.FollowUpdate mInterFace = retrofit.create(ProcPrivateLeadService.FollowUpdate.class);
        Call<FollowUpdateResponse> mCall = mInterFace.privateleads(privateLeadRequest);
        mCall.enqueue(new Callback<FollowUpdateResponse>() {
            @Override
            public void onResponse(Call<FollowUpdateResponse> call, Response<FollowUpdateResponse> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), context);
                }
            }

            @Override
            public void onFailure(Call<FollowUpdateResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });


    }


    //Proc Convert Stock

    public static void PconvertStock(Context context, ProcConvertStockRequest privateLeadRequest, final HttpCallResponse mHttpCallResponse) {
        ProcPrivateLeadService.ProcconvertStock mInterFace = retrofit.create(ProcPrivateLeadService.ProcconvertStock.class);
        Call<ProcConvertStockResponse> mCall = mInterFace.privateleads(privateLeadRequest);
        mCall.enqueue(new Callback<ProcConvertStockResponse>() {
            @Override
            public void onResponse(Call<ProcConvertStockResponse> call, Response<ProcConvertStockResponse> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), context);
                }
            }

            @Override
            public void onFailure(Call<ProcConvertStockResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });


    }

    //Update Follow Up

    public static void CallingPayment(Activity activity, PaymentRequest mrequest, String amount, String txnid, final HttpCallResponse mHttpCallResponse) {
        ProcPrivateLeadService.ForPayment mInterFace = retrofit.create(ProcPrivateLeadService.ForPayment.class);
        Call<PayUPaymentResponse> mCall = mInterFace.mpayment(mrequest);

        Log.i("TAG", "TAG: "+mCall.request().url().toString());
        mCall.enqueue(new Callback<PayUPaymentResponse>() {
            @Override
            public void onResponse(Call<PayUPaymentResponse> call, Response<PayUPaymentResponse> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {
                    mHttpCallResponse.OnSuccess(response);
                   SpinnerManager.hideSpinner(activity);
                    CommonMethods.setvalueAgainstKey(activity, "amount", amount);
                    CommonMethods.setvalueAgainstKey(activity, "txnid", txnid);
                    /* WebServicesCall.error_popup_retrofit(response.code(), response.message(), activity);*/
                }
            }

            @Override
            public void onFailure(Call<PayUPaymentResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });
    }


    public interface senEmailPrivateLeadsInterFace {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("private-leads/getlead")
        Call<String> privateleads(@Body PrivateLeadsModelRequest privateLeadRequest);
    }


    public interface PrivateLeadsInterFace {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("private-leads/getlead")
        Call<PPLResponse> privateleads(@Body PrivateLeadsModelRequest privateLeadRequest);
    }

    public interface PPFHLeadsInterFace {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("private-leads/get-followup")
        Call<PPFHResponse> privateleads(@Body PPFHRequest PPFollowHistoryResponse);
    }

    public interface FollowUpdate {
        @Headers("Content-Type: application/json; charset=utf-8")
        @PUT("private-leads/update")
        Call<FollowUpdateResponse> privateleads(@Body FollowUpdateRequest FollowUpdateResponse);
    }

    public interface ProcconvertStock {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("procurement/converttostock")
        Call<ProcConvertStockResponse> privateleads(@Body ProcConvertStockRequest ProcConvertStockResponse);
    }
    // For Payment Required
    public interface ForPayment {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("warrantytransaction")
        Call<PayUPaymentResponse> mpayment(@Body PaymentRequest mpaymentrequest);
    }

}
