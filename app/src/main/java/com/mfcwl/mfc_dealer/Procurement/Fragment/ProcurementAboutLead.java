package com.mfcwl.mfc_dealer.Procurement.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.Activity.AddStockActivity;
import com.mfcwl.mfc_dealer.Activity.Leads.LeadsInstance;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.Procurement.Activity.ProcConvertStock;
import com.mfcwl.mfc_dealer.Procurement.PLDetailsRequestResponse.FollowUpdateRequest;
import com.mfcwl.mfc_dealer.Procurement.PLDetailsRequestResponse.FollowUpdateResponse;
import com.mfcwl.mfc_dealer.Procurement.RetrofitConfig.ProcPrivateLeadService;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.MonthUtility;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Response;

@SuppressLint("ValidFragment")
public class ProcurementAboutLead extends Fragment implements View.OnTouchListener {

    TextView leads_book_now, proc_leads_update_followup;
    public static String lname, lmobile, lemail, lstatus, lposteddate, lmake, lmodel, lprice, lregno, lfollupdate, lexename,
            lregcity, lowner, lregyear, lcolor, lkms, ldid, remarks, ldays, Lvariant = "", Lmfyear, Leadid, Ltype, Webleadid;

    public static CardView cardview2;

    public static boolean lost = false;
    public static Spinner spinSelectStatus;

    public String[] status = {"Select Status", "Hot", "Warm", "Cold", "Lost"};
    static String selectStatus = "", lead_type = "";
    public static EditText edit_Remark;
    public static TextView datefollup, Calendarline;

    public static String upleadstatus = "", upnextfollowdate = "";

    public static TextView leads_details_next_followup, leads_details_exe_name, leads_details_name, leads_details_num, leads_details_email, leads_details_days, leads_details_hot;
    public static TextView leads_details_model, leads_details_model_name, leads_details_price, leads_details_model_num, leads_details_message;
    public static TextView leads_details_register_city_name, leads_details_color_which, leads_details_owner_name, leads_details_kms_which, leads_details_year_which, leads_details_fuel_which;
    String name = "";
    public ImageView leads_details_phonecall, leads_details_messages, leads_details_whatsapp;

    public static String strDate = "";
    static LinearLayout ll_about,badgeid;

    String followupStatus;
    String TAG = getClass().getSimpleName();
    public ProcurementAboutLead(String lname, String lmobile, String lemail, String lstatus, String lposteddate, String lmake, String lmodel, String lprice, String lregno, String lfollupdate, String lexename, String lregcity, String lowner, String lregyear, String lcolor, String lkms, String ldid, String remarks, String ldays, String lvariant, String leadid, String lmfyear, String lead_type) {
        this.lname = lname;
        this.lmobile = lmobile;
        this.lemail = lemail;
        this.lstatus = lstatus;
        this.lposteddate = lposteddate;
        this.lmake = lmake;
        this.lmodel = lmodel;
        this.lprice = lprice;
        this.lregno = lregno;
        this.lfollupdate = lfollupdate;
        this.lexename = lexename;
        this.lregcity = lregcity;
        this.lowner = lowner;
        this.lregyear = lregyear;
        this.lcolor = lcolor;
        this.lkms = lkms;
        this.ldid = ldid;
        this.remarks = remarks;
        this.ldays = ldays;
        this.Lvariant = lvariant;
        this.Leadid = leadid;
        this.Lmfyear = lmfyear;
        this.lead_type = lead_type;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_procurement_about_lead, container, false);

        InitUI(view);
        OnClickListener();
        return view;
    }

    private void InitUI(View view) {
        Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"), GlobalText.proc_Aboutlead, GlobalText.android);
        leads_book_now = view.findViewById(R.id.leads_book_now);
        proc_leads_update_followup = view.findViewById(R.id.proc_leads_update_followup);

        cardview2 = view.findViewById(R.id.cardview2);
        leads_details_next_followup = view.findViewById(R.id.leads_details_next_followup);
        leads_details_exe_name = view.findViewById(R.id.leads_details_exe_name);

        leads_details_name = view.findViewById(R.id.leads_details_name);
        leads_details_num = view.findViewById(R.id.leads_details_num);
        leads_details_email = view.findViewById(R.id.leads_details_email);
        leads_details_days = view.findViewById(R.id.leads_details_days);
        leads_details_hot = view.findViewById(R.id.leads_details_hot);

        leads_details_model = view.findViewById(R.id.leads_details_model);
        leads_details_model_name = view.findViewById(R.id.leads_details_model_name);
        leads_details_price = view.findViewById(R.id.leads_details_price);
        leads_details_model_num = view.findViewById(R.id.leads_details_model_num);
        leads_details_message = view.findViewById(R.id.leads_details_message);

        leads_details_register_city_name = view.findViewById(R.id.leads_details_register_city_name);
        leads_details_color_which = view.findViewById(R.id.leads_details_color_which);
        leads_details_owner_name = view.findViewById(R.id.leads_details_owner_name);
        leads_details_kms_which = view.findViewById(R.id.leads_details_kms_which);
        leads_details_year_which = view.findViewById(R.id.leads_details_year_which);

        leads_details_fuel_which = view.findViewById(R.id.leads_details_fuel_which);

        leads_details_phonecall = view.findViewById(R.id.leads_details_phonecall);
        leads_details_messages = view.findViewById(R.id.leads_details_messages);
        leads_details_whatsapp = view.findViewById(R.id.leads_details_whatsapp);

        ll_about = (LinearLayout) view.findViewById(R.id.ll_about);
        badgeid = (LinearLayout) view.findViewById(R.id.badgeid);

        if(CommonMethods.getstringvaluefromkey(getActivity(),"user_type").equalsIgnoreCase("dealer") ||
        CommonMethods.getstringvaluefromkey(getActivity(), "user_type").equalsIgnoreCase(GlobalText.USER_TYPE_PROCUREMENT)){
            ll_about.setVisibility(View.VISIBLE);
            badgeid.setVisibility(View.VISIBLE);
        }else{
            ll_about.setVisibility(View.GONE);
            badgeid.setVisibility(View.GONE);
        }

        //name = getActivity().getIntent().getExtras().getString("NM");
        leads_details_name.setText(capitalize(lname));
        leads_details_num.setText(lmobile);
        leads_details_email.setText(lemail);
        leads_details_hot.setText(capitalize(lstatus));
        leads_details_model.setText(lmake);
        leads_details_model_name.setText(lmodel + " " + Lvariant);
        leads_details_price.setText(lprice);
        leads_details_message.setText(remarks);
        if (remarks.trim().equals("") || remarks.trim().equals("NA")) {
            leads_details_message.setText("No comments available");
        }
        leads_details_days.setText(ldays);

        if (lstatus.equalsIgnoreCase("Open")) {
            leads_details_hot.setBackgroundResource(R.drawable.open_28);
            //leads_details_hot.setVisibility(View.INVISIBLE);
        }
        if (lstatus.equalsIgnoreCase("") || lstatus.equalsIgnoreCase("null")) {
            leads_details_hot.setBackgroundResource(R.drawable.open_28);
            leads_details_hot.setText(" Open");
            //leads_details_hot.setVisibility(View.INVISIBLE);
        }
        if (lstatus.equalsIgnoreCase("Hot")) {
            leads_details_hot.setBackgroundResource(R.drawable.hot_28);
            leads_details_hot.setVisibility(View.VISIBLE);
        }
        if (lstatus.equalsIgnoreCase("Warm")) {
            leads_details_hot.setBackgroundResource(R.drawable.warm_28);
            leads_details_hot.setVisibility(View.VISIBLE);
        }
        if (lstatus.equalsIgnoreCase("Cold")) {
            leads_details_hot.setBackgroundResource(R.drawable.cold_28);
            leads_details_hot.setVisibility(View.VISIBLE);
        }
        if (lstatus.equalsIgnoreCase("Lost")) {
            leads_details_hot.setBackgroundResource(R.drawable.lost_28);
            leads_details_hot.setVisibility(View.VISIBLE);
            ll_about.setVisibility(View.INVISIBLE);
        }
        if (lstatus.equalsIgnoreCase("Bought")) {
            leads_details_hot.setBackgroundResource(R.drawable.sold_28);
            leads_details_hot.setVisibility(View.VISIBLE);
            ll_about.setVisibility(View.INVISIBLE);

        }
        if (lstatus.equalsIgnoreCase("Sold")) {
            leads_details_hot.setBackgroundResource(R.drawable.sold_28);
            leads_details_hot.setVisibility(View.VISIBLE);
        }
        if (lstatus.equalsIgnoreCase("Visiting")) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_visiting);
            leads_details_hot.setVisibility(View.VISIBLE);
        }
        if (lstatus.equalsIgnoreCase("Search")) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_search_lead);
            leads_details_hot.setVisibility(View.VISIBLE);
        }
        if (lstatus.equalsIgnoreCase("Not-Interested")) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_not_intrested);
            leads_details_hot.setVisibility(View.VISIBLE);
            ll_about.setVisibility(View.INVISIBLE);
        }
        if (lstatus.equalsIgnoreCase("No-Response")) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_no_responce);
            leads_details_hot.setVisibility(View.VISIBLE);
        }
        if (lstatus.equalsIgnoreCase("Finalised")) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_finalized);
            leads_details_hot.setVisibility(View.VISIBLE);
            ll_about.setVisibility(View.INVISIBLE);
        }

        try {

            if (LeadsInstance.getInstance().getWhichleads().equals("WebLeads")) {
                String[] splitdatespace = lfollupdate.split(" ", 10);
                String[] splitdate1 = splitdatespace[0].split("-", 10);
                lfollupdate = splitdate1[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate1[1])] + " " + splitdate1[0];

            } else {
                String[] splitdatespace = lfollupdate.split(" ", 10);
                String[] splitdate1 = splitdatespace[0].split("-", 10);
                lfollupdate = splitdate1[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate1[1])] + " " + splitdate1[0];
            }
            //   Log.e("leadfollow up date", lfollupdate);
        } catch (Exception e) {

        }

        if (lexename.trim().equalsIgnoreCase("null") || lexename.trim().equalsIgnoreCase("")) {
            lexename = "NA";
        }
        if (lfollupdate != null) {
            if (lfollupdate.startsWith("00") || lfollupdate.startsWith("null") || lfollupdate.equals("")) {
                lfollupdate = "NA";
            }
        } else {
            lfollupdate = "NA";
        }

        String sourceString1 = "Next Follow-up " + "<br>" + "<font color=\"black\">" + lfollupdate + "</font>" + "</br> ";
        String sourceString2 = "Executive Name " + "<br>" + "<font color=\"black\">" + lexename + "</font>" + "</br> ";
        leads_details_next_followup.setText(Html.fromHtml(sourceString1));
        leads_details_exe_name.setText(Html.fromHtml(sourceString2));

        if (lregcity == null || lregcity.equalsIgnoreCase("null") || lregcity.equalsIgnoreCase("")) {
            lregcity = "NA";
        }
        leads_details_register_city_name.setText(lregcity);

        if (lowner == null || lowner.equalsIgnoreCase("null") || lowner.equalsIgnoreCase("")) {
            lowner = "NA";
        }
        leads_details_owner_name.setText(lowner);

        if (lcolor == null || lcolor.equalsIgnoreCase("null") || lcolor.equalsIgnoreCase("")) {
            lcolor = "NA";
        }
        leads_details_color_which.setText(lcolor);

        if (lkms == null || lkms.equalsIgnoreCase("null") || lkms.equalsIgnoreCase("")) {
            lkms = "NA";
        }

        leads_details_kms_which.setText(lkms);

        if (lregno == null || lregno.equalsIgnoreCase("null") || lregno.equalsIgnoreCase("")) {
            lregno = "NA";
        }
        leads_details_model_num.setText(lregno);
        if (Lmfyear == null || Lmfyear.equalsIgnoreCase("null") || Lmfyear.equalsIgnoreCase("")) {
            Lmfyear = "NA";
        }
        leads_details_year_which.setText(Lmfyear);

    }

    private void OnClickListener() {
        leads_book_now.setOnTouchListener(this::onTouch);
        proc_leads_update_followup.setOnTouchListener(this::onTouch);
        leads_details_whatsapp.setOnTouchListener(this::onTouch);
        leads_details_phonecall.setOnTouchListener(this::onTouch);
        leads_details_messages.setOnTouchListener(this::onTouch);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.leads_book_now:
                Intent mintent = null;
                if (CommonMethods.getstringvaluefromkey(getActivity(), "PWLEADS").equalsIgnoreCase("WebLeads")) {
                    mintent = new Intent(getActivity(), AddStockActivity.class);
                } else {
                    mintent = new Intent(getActivity(), ProcConvertStock.class);
                }
                startActivity(mintent);
                // BookNow();
                break;
            case R.id.proc_leads_update_followup:
                UpdateFollowUp();
                break;
            case R.id.leads_details_whatsapp:
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"), GlobalText.proc_Aboutlead_call, GlobalText.android);
                openWhatsApp(lmobile.trim());
                break;
            case R.id.leads_details_phonecall:
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"), GlobalText.proc_Aboutlead_message, GlobalText.android);
                Intent callIntent = new Intent(Intent.ACTION_VIEW);
                callIntent.setData(Uri.parse("tel:" + lmobile.trim()));//change the number
                startActivity(callIntent);
                break;
            case R.id.leads_details_messages:
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"), GlobalText.proc_Aboutlead_whatsApp, GlobalText.android);
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:" + lmobile.trim()));
                sendIntent.putExtra("sms_body", "");
                startActivity(sendIntent);
                break;

        }
        return false;
    }


    private void openWhatsApp(String number) {
        String smsNumber = "91" + number; //without '+'
        try {

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_VIEW);
            String url = "https://api.whatsapp.com/send?phone=" + smsNumber + "&text=" + "";
            sendIntent.setData(Uri.parse(url));
            startActivity(sendIntent);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void BookNow() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        dialog.dismiss();
                        Intent mintent = null;
                        if (CommonMethods.getstringvaluefromkey(getActivity(), "PWLEADS").equalsIgnoreCase("WebLeads")) {
                            mintent = new Intent(getActivity(), AddStockActivity.class);
                        } else {
                            mintent = new Intent(getActivity(), ProcConvertStock.class);
                        }
                        startActivity(mintent);
                        // startActivity(new Intent(getActivity(), ProcConvertStock.class).putExtra("leadid", Leadid).putExtra("followup", datefollup.getText().toString().trim()));
                       /* Bundle mbundle = new Bundle();
                        mbundle.putString("leadid", Leadid);
                        if (!strDate.equalsIgnoreCase("")) {
                            mbundle.putString("followup", strDate);
                        } else {
                            mbundle.putString("followup", lfollupdate);
                        }

                        mbundle.putString("make", lmake);
                        mbundle.putString("model", lmodel);
                        mbundle.putString("variant", Lvariant);

                        mbundle.putString("price", lprice);
                        mbundle.putString("regyear", lregyear);
                        mbundle.putString("mfgyear", Lmfyear);
                        mbundle.putString("regno", lregno);
                        mbundle.putString("color", lcolor);
                        mbundle.putString("regcity", lregcity);
                        mbundle.putString("kms", lkms);
                        mbundle.putString("owner", lowner);
                        mintent.putExtras(mbundle);*/

                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure to book this lead & convert it to a new stock? ")
                .setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    public void UpdateFollowUp() {
        Dialog dialog = new Dialog(getActivity(), R.style.full_screen_dialog);
        dialog.setContentView(R.layout.booknowpop2);
        String star = "<font color='#B40404'>*</font>";
        Button updatenowbtn = dialog.findViewById(R.id.updatenowbtn);
        Button cancelbtn = dialog.findViewById(R.id.addleadcancel);

        TextView next_followupdate = dialog.findViewById(R.id.next_followupdate);
        TextView lead_status = dialog.findViewById(R.id.lead_status);
        edit_Remark = dialog.findViewById(R.id.edit_Remark);
        spinSelectStatus = dialog.findViewById(R.id.spinSelectStatus);
        datefollup = dialog.findViewById(R.id.datefollup);
        TextView Calendarline = dialog.findViewById(R.id.Calendarline);

        lead_status.setText(Html.fromHtml("LEAD STATUS " + star));
        next_followupdate.setText(Html.fromHtml("NEXT FOLLOW UP DATE " + star));

        CommonMethods.setvalueAgainstKey(getActivity(), "updatenowbtn", "false");
        getActivity().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        List<String> mList = new ArrayList<>(MainActivity.statusList);
        mList.add(0, "Select Status");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner2, mList);

        spinSelectStatus.setAdapter(adapter);
        spinSelectStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                View view1 = adapterView.getSelectedView();
                TextView tv = (TextView) view1;
                if (i == 0) {
                    // Set the disable item text color
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                selectStatus = adapterView.getSelectedItem().toString();

                if (i != 0) {
                    followupStatus = MainActivity.leadstatus.get(i - 1).getIsFollowup();
                    if (followupStatus.equalsIgnoreCase("false")) {
                        next_followupdate.setVisibility(View.INVISIBLE);
                        datefollup.setVisibility(View.INVISIBLE);
                        Calendarline.setVisibility(View.INVISIBLE);
                    } else {
                        next_followupdate.setVisibility(View.VISIBLE);
                        datefollup.setVisibility(View.VISIBLE);
                        Calendarline.setVisibility(View.VISIBLE);
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        datefollup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDateFuture(view);
            }
        });

        updatenowbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long milliseconds = System.currentTimeMillis();
                Calendar cl = Calendar.getInstance();
                cl.setTimeInMillis(milliseconds);  //here your time in miliseconds
                String date = "" + cl.get(Calendar.DAY_OF_MONTH);
                String[] splitIfun = next_followupdate.getText().toString().split("-", 10);
                CommonMethods.setvalueAgainstKey(getActivity(), "updatenowbtn", "true");
                remarks = edit_Remark.getText().toString().trim();
                if (selectStatus.equals("Select Status")) {
                    Toast.makeText(getActivity(), "Please select the status ", Toast.LENGTH_LONG).show();
                } else {
                    if (followupStatus.equalsIgnoreCase("false")) {
                        if (CommonMethods.getstringvaluefromkey(getActivity(), "PWLEADS").equals("PrivateLeads")) {
                            dialog.dismiss();
                            RequestFollowUpUpdate("lost");
                        }
                        if (CommonMethods.getstringvaluefromkey(getActivity(), "PWLEADS").equals("WebLeads")) {
                            dialog.dismiss();
                            WebServicesCall.webCall(getActivity(), getActivity(), jsonMake(), "ProcUpdateFollowLead", GlobalText.POST);
                        }
                    } else {
                        if (datefollup.getText().toString().equals("")) {
                            Toast.makeText(getActivity(), "Please select the follow up date", Toast.LENGTH_LONG).show();
                        } else {
                            if (CommonMethods.getstringvaluefromkey(getActivity(), "PWLEADS").equals("PrivateLeads")) {
                                dialog.dismiss();
                                RequestFollowUpUpdate("");
                            }
                            if (CommonMethods.getstringvaluefromkey(getActivity(), "PWLEADS").equals("WebLeads")) {
                                dialog.dismiss();
                                WebServicesCall.webCall(getActivity(), getActivity(), jsonMake(), "ProcUpdateFollowLead", GlobalText.POST);
                            }
                        }
                    }
                }
            }
        });
        dialog.show();
    }

    private String capitalize(String capString) {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()) {
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }
        return capMatcher.appendTail(capBuffer).toString();
    }

    private void RequestFollowUpUpdate(String lost) {
        FollowUpdateRequest req = new FollowUpdateRequest();
        req.setLeadId(Leadid);
        req.setLeadType(lead_type);
        req.setLeadStatus(selectStatus);
        req.setUpdatedByDevice("OMS App-android");
        if (edit_Remark.getText().toString().trim().equals("")) {
            req.setLeadRemark("NA");
        } else {
            req.setLeadRemark(edit_Remark.getText().toString());
        }
        lfollupdate = datefollup.getText().toString();
        if (lost.equalsIgnoreCase("lost")) {
            req.setFollowup("");
        } else {
            req.setFollowup(datefollup.getText().toString());
        }
        req.setExecutiveId("");
        req.setExecutiveName("");

        ResponseFollowUpUpdate(getActivity(), req);
    }

    private void ResponseFollowUpUpdate(Context context, FollowUpdateRequest req) {
        SpinnerManager.showSpinner(context);
        ProcPrivateLeadService.PPFollowUpdate(context, req, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(context);

                Response<FollowUpdateResponse> mRes = (Response<FollowUpdateResponse>) obj;
                FollowUpdateResponse mData = mRes.body();
                if (mData.getStatus().equalsIgnoreCase("SUCCESS")) {

                    CommonMethods.setvalueAgainstKey(getActivity(), "isProcLeadDetails", "true");
                    Toast.makeText(getActivity(), mData.getMessage(), Toast.LENGTH_LONG).show();

                    if (selectStatus.equalsIgnoreCase("Hot")) {
                        leads_details_hot.setBackgroundResource(R.drawable.hot_28);
                        leads_details_hot.setText("Hot");
                        leads_details_hot.setVisibility(View.VISIBLE);
                        if(CommonMethods.getstringvaluefromkey(getActivity(),"user_type").equalsIgnoreCase("dealer")){
                            ll_about.setVisibility(View.VISIBLE);
                        }else{
                            ll_about.setVisibility(View.GONE);
                        }

                    }
                    if (selectStatus.equalsIgnoreCase("Warm")) {
                        leads_details_hot.setBackgroundResource(R.drawable.warm_28);
                        leads_details_hot.setVisibility(View.VISIBLE);
                        leads_details_hot.setText("Warm");
                        if(CommonMethods.getstringvaluefromkey(getActivity(),"user_type").equalsIgnoreCase("dealer")){
                            ll_about.setVisibility(View.VISIBLE);
                        }else{
                            ll_about.setVisibility(View.GONE);
                        }
                    }
                    if (selectStatus.equalsIgnoreCase("Cold")) {
                        leads_details_hot.setBackgroundResource(R.drawable.cold_28);
                        leads_details_hot.setVisibility(View.VISIBLE);
                        leads_details_hot.setText("Cold");
                    }
                    if (selectStatus.equalsIgnoreCase("Lost")) {
                        leads_details_hot.setBackgroundResource(R.drawable.lost_28);
                        leads_details_hot.setVisibility(View.VISIBLE);
                        leads_details_hot.setText("Lost");
                        ll_about.setVisibility(View.INVISIBLE);
                    }
                    if (selectStatus.equalsIgnoreCase("Bought")) {
                        leads_details_hot.setBackgroundResource(R.drawable.sold_28);
                        leads_details_hot.setVisibility(View.VISIBLE);
                        leads_details_hot.setText("Bought");
                        ll_about.setVisibility(View.INVISIBLE);
                    }
                    if (selectStatus.equalsIgnoreCase("Sold")) {
                        leads_details_hot.setBackgroundResource(R.drawable.sold_28);
                        leads_details_hot.setVisibility(View.VISIBLE);
                        leads_details_hot.setText("Sold");
                        if(CommonMethods.getstringvaluefromkey(getActivity(),"user_type").equalsIgnoreCase("dealer")){
                            ll_about.setVisibility(View.VISIBLE);
                        }else{
                            ll_about.setVisibility(View.GONE);
                        }                    }
                    if (selectStatus.equalsIgnoreCase("Visiting")) {
                        leads_details_hot.setBackgroundResource(R.drawable.ic_visiting);
                        leads_details_hot.setVisibility(View.VISIBLE);
                        leads_details_hot.setText(selectStatus);
                        if(CommonMethods.getstringvaluefromkey(getActivity(),"user_type").equalsIgnoreCase("dealer")){
                            ll_about.setVisibility(View.VISIBLE);
                        }else{
                            ll_about.setVisibility(View.GONE);
                        }                    }
                    if (selectStatus.equalsIgnoreCase("Search")) {
                        leads_details_hot.setBackgroundResource(R.drawable.ic_search_lead);
                        leads_details_hot.setVisibility(View.VISIBLE);
                        leads_details_hot.setText(selectStatus);
                        if(CommonMethods.getstringvaluefromkey(getActivity(),"user_type").equalsIgnoreCase("dealer")){
                            ll_about.setVisibility(View.VISIBLE);
                        }else{
                            ll_about.setVisibility(View.GONE);
                        }                    }
                    if (selectStatus.equalsIgnoreCase("Not-Interested")) {
                        leads_details_hot.setBackgroundResource(R.drawable.ic_not_intrested);
                        leads_details_hot.setVisibility(View.VISIBLE);
                        leads_details_hot.setText(selectStatus);
                        ll_about.setVisibility(View.INVISIBLE);
                    }
                    if (selectStatus.equalsIgnoreCase("No-Response")) {
                        leads_details_hot.setBackgroundResource(R.drawable.ic_no_responce);
                        leads_details_hot.setVisibility(View.VISIBLE);
                        leads_details_hot.setText(selectStatus);
                        if(CommonMethods.getstringvaluefromkey(getActivity(),"user_type").equalsIgnoreCase("dealer")){
                            ll_about.setVisibility(View.VISIBLE);
                        }else{
                            ll_about.setVisibility(View.GONE);
                        }                    }
                    if (selectStatus.equalsIgnoreCase("Finalised")) {
                        leads_details_hot.setBackgroundResource(R.drawable.ic_finalized);
                        leads_details_hot.setVisibility(View.VISIBLE);
                        leads_details_hot.setText(selectStatus);
                        ll_about.setVisibility(View.INVISIBLE);
                    }

                    try {
                        if (!selectStatus.equalsIgnoreCase("Lost") && !selectStatus.equalsIgnoreCase("Not-Interested")
                                && !selectStatus.equalsIgnoreCase("Finalised")) {
                            String[] splitdatespace = lfollupdate.split(" ", 10);
                            String[] splitdate1 = splitdatespace[0].split("-", 10);
                            String date = (splitdate1[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate1[1])] + " " + splitdate1[0]);
                            String sourceString1 = "Next Follow-up " + "<br>" + "<font color=\"black\">" + date + "</font>" + "</br> ";
                            leads_details_next_followup.setText(Html.fromHtml(sourceString1));

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (remarks.trim().equals("") || remarks.trim().equals("NA")) {
                        leads_details_message.setText("No comments available");
                    } else {
                        leads_details_message.setText(remarks);
                    }

                } else {
                    Toast.makeText(getActivity(), mData.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void OnFailure(Throwable t) {
                SpinnerManager.hideSpinner(context);
                t.printStackTrace();
            }
        });
    }

    private JSONObject jsonMake() {

        JSONObject jObj = new JSONObject();

        try {
            jObj.put("access_token", CommonMethods.getstringvaluefromkey(getActivity(), "access_token"));
            jObj.put("dispatch_id", ldid);
            jObj.put("employee_id", "");
            jObj.put("employee_name", "");
            jObj.put("employee_type", lead_type);
            jObj.put("tag", "android");
            if (datefollup.getText().toString().equals("")) {
                jObj.put("followup_date", "0000-00-00");
            } else {
                jObj.put("followup_date", datefollup.getText().toString());
            }
            jObj.put("followup_status", selectStatus);
            if (edit_Remark.getText().toString().equals("")) {
                jObj.put("followup_comments", "NA");
            } else {
                jObj.put("followup_comments", edit_Remark.getText().toString());
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        //  Log.e("Request Proc Lead ", "jObj " + jObj.toString());

        return jObj;

    }

    public void setDateFuture(View v) {

        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
            // when dialog box is closed, below method will be called.
            public void onDateSet(DatePicker view, int selectedYear,
                                  int selectedMonth, int selectedDay) {

                myCalendar.set(Calendar.YEAR, selectedYear);
                myCalendar.set(Calendar.MONTH, selectedMonth);
                myCalendar.set(Calendar.DAY_OF_MONTH, selectedDay);

                // System.out.println();
                String strselectedDay = String.valueOf(selectedDay);
                if (strselectedDay.length() == 1) {
                    strselectedDay = "0" + strselectedDay;
                }
                String strselectedMonth = String.valueOf(selectedMonth + 1);
                if (strselectedMonth.length() == 1) {
                    strselectedMonth = "0" + strselectedMonth;
                }

                strDate = selectedYear + "-" + strselectedMonth + "-"
                        + strselectedDay;
                datefollup.setText(strDate);

            }
        };

        final DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                datePickerListener, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });
        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    @SuppressLint("NewApi")
                    public void onClick(DialogInterface dialog, int which) {
                        DatePicker datePicker = datePickerDialog
                                .getDatePicker();
                        datePickerListener.onDateSet(datePicker,
                                datePicker.getYear(),
                                datePicker.getMonth(),
                                datePicker.getDayOfMonth());

                        dialog.dismiss();

                    }
                });

        long currentTime = System.currentTimeMillis();
        datePickerDialog.getDatePicker().setMinDate(currentTime);
        datePickerDialog.setCancelable(false);
        datePickerDialog.show();

    }

    public static void Parseupdatefollowlead(JSONObject jObj, Activity activity) {
        upleadstatus = spinSelectStatus.getSelectedItem().toString();
        upnextfollowdate = datefollup.getText().toString();
        // upremark = edit_Remark.getText().toString();

        try {
            if (jObj.getString("status").equalsIgnoreCase("success")) {

                CommonMethods.setvalueAgainstKey(activity, "isProcLeadDetails", "true");

                Toast.makeText(activity, "Updated successfully", Toast.LENGTH_LONG).show();
                if (selectStatus.equalsIgnoreCase("Hot")) {
                    leads_details_hot.setBackgroundResource(R.drawable.hot_28);
                    leads_details_hot.setText("Hot");
                    leads_details_hot.setVisibility(View.VISIBLE);
                }
                if (selectStatus.equalsIgnoreCase("Warm")) {
                    leads_details_hot.setBackgroundResource(R.drawable.warm_28);
                    leads_details_hot.setVisibility(View.VISIBLE);
                    leads_details_hot.setText("Warm");
                }
                if (selectStatus.equalsIgnoreCase("Cold")) {
                    leads_details_hot.setBackgroundResource(R.drawable.cold_28);
                    leads_details_hot.setVisibility(View.VISIBLE);
                    leads_details_hot.setText("Cold");
                }
                if (selectStatus.equalsIgnoreCase("Lost")) {
                    leads_details_hot.setBackgroundResource(R.drawable.lost_28);
                    leads_details_hot.setVisibility(View.VISIBLE);
                    leads_details_hot.setText("Lost");
                    ll_about.setVisibility(View.INVISIBLE);

                }
                if (selectStatus.equalsIgnoreCase("Sold")) {
                    leads_details_hot.setBackgroundResource(R.drawable.sold_28);
                    leads_details_hot.setVisibility(View.VISIBLE);
                    leads_details_hot.setText("Sold");
                }
                if (selectStatus.equalsIgnoreCase("Visiting")) {
                    leads_details_hot.setBackgroundResource(R.drawable.ic_visiting);
                    leads_details_hot.setVisibility(View.VISIBLE);
                    leads_details_hot.setText(selectStatus);
                }
                if (selectStatus.equalsIgnoreCase("Search")) {
                    leads_details_hot.setBackgroundResource(R.drawable.ic_search_lead);
                    leads_details_hot.setVisibility(View.VISIBLE);
                    leads_details_hot.setText(selectStatus);

                }
                if (selectStatus.equalsIgnoreCase("Not-Interested")) {
                    leads_details_hot.setBackgroundResource(R.drawable.ic_not_intrested);
                    leads_details_hot.setVisibility(View.VISIBLE);
                    leads_details_hot.setText(selectStatus);
                    ll_about.setVisibility(View.INVISIBLE);
                }
                if (selectStatus.equalsIgnoreCase("No-Response")) {
                    leads_details_hot.setBackgroundResource(R.drawable.ic_no_responce);
                    leads_details_hot.setVisibility(View.VISIBLE);
                    leads_details_hot.setText(selectStatus);
                }
                if (selectStatus.equalsIgnoreCase("Finalised")) {
                    leads_details_hot.setBackgroundResource(R.drawable.ic_finalized);
                    leads_details_hot.setVisibility(View.VISIBLE);
                    leads_details_hot.setText(selectStatus);
                    ll_about.setVisibility(View.INVISIBLE);
                }


                if (!upnextfollowdate.equalsIgnoreCase("")) {
                    String[] splitdate1 = upnextfollowdate.split("-", 10);
                    String date = (splitdate1[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate1[1])] + " " + splitdate1[0]);
                    String sourceString1 = "Next Follow-up " + "<br>" + "<font color=\"black\">" + date + "</font>" + "</br> ";
                    leads_details_next_followup.setText(Html.fromHtml(sourceString1));
                }
                if (remarks.trim().equals("") || remarks.trim().equals("NA")) {
                    leads_details_message.setText("No comments available");
                } else {
                    leads_details_message.setText(remarks);
                }
            } else {
                if (jObj.getString("status").equals("failure")) {
                    WebServicesCall.error_popup2(activity, Integer.parseInt(jObj.getString("status_code")), jObj.getString("message"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
