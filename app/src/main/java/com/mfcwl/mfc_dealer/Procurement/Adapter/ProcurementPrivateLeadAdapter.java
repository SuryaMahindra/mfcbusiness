package com.mfcwl.mfc_dealer.Procurement.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Procurement.Activity.ProcurementDetails;
import com.mfcwl.mfc_dealer.Procurement.ProcInterface.PLazyloaderPrivateLeads;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PPLResponseDatum;
import com.mfcwl.mfc_dealer.Procurement.Singleton.ConvertStockInstance;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.MonthUtility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProcurementPrivateLeadAdapter extends RecyclerView.Adapter<ProcurementPrivateLeadAdapter.MyViewHolder> implements View.OnClickListener {

    private List<PPLResponseDatum> procList;
    LayoutInflater layoutInflater;
    public Context mContext;
    Activity a;
    int i;
    private List<PPLResponseDatum> itemListSearch;
    private PLazyloaderPrivateLeads lazyloaderPrivateLeads;

    public ProcurementPrivateLeadAdapter(Activity a, Context mContext, List<PPLResponseDatum> procList) {
        this.mContext = mContext;
        this.procList = procList;
        layoutInflater = LayoutInflater.from(mContext);
        itemListSearch = new ArrayList<PPLResponseDatum>();
        itemListSearch.addAll(procList);
        this.a = a;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView proc_leadName, proc_leadNum, proc_leadCompName, proc_leadCarModel, proc_leadDate, proc_leadDays,
                proc_leadFollowupHistory;
        public ImageView proc_leadWhatsapp, proc_leadMessege, proc_leadCall;

        public MyViewHolder(View view) {
            super(view);
            proc_leadName = (TextView) view.findViewById(R.id.proc_leadName);
            proc_leadNum = (TextView) view.findViewById(R.id.proc_leadNum);
            proc_leadCompName = (TextView) view.findViewById(R.id.proc_leadCompName);
            proc_leadCarModel = (TextView) view.findViewById(R.id.proc_leadCarModel);
            proc_leadDate = (TextView) view.findViewById(R.id.proc_leadDate);
            proc_leadDays = (TextView) view.findViewById(R.id.proc_leadDays);
            proc_leadFollowupHistory = (TextView) view.findViewById(R.id.proc_leadFollowupHistory);
            proc_leadWhatsapp = (ImageView) view.findViewById(R.id.proc_leadWhatsapp);
            proc_leadMessege = (ImageView) view.findViewById(R.id.proc_leadMessege);
            proc_leadCall = (ImageView) view.findViewById(R.id.proc_leadCall);

            if (CommonMethods.getstringvaluefromkey(a, "user_type").equalsIgnoreCase("dealer")) {

            }else{
                proc_leadWhatsapp.setVisibility(View.INVISIBLE);
                proc_leadMessege.setVisibility(View.INVISIBLE);
                proc_leadCall .setVisibility(View.INVISIBLE);

            }

        }
    }

    @Override
    public ProcurementPrivateLeadAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        lazyloaderPrivateLeads = (PLazyloaderPrivateLeads) a;
        View itemView = layoutInflater.from(parent.getContext())
                .inflate(R.layout.row_proc_weblead, parent, false);

        return new ProcurementPrivateLeadAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.proc_leadWhatsapp:
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(a, "dealer_code"), GlobalText.proc_private_whatsapp, GlobalText.android);
                openWhatsApp(procList.get(i).getCustomerMobile().trim());
                break;
            case R.id.proc_leadMessege:
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(a, "dealer_code"), GlobalText.proc_private_message, GlobalText.android);
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:" + procList.get(i).getCustomerMobile().trim()));
                sendIntent.putExtra("sms_body", "");
                mContext.startActivity(sendIntent);
                break;
            case R.id.proc_leadCall:
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(a, "dealer_code"), GlobalText.proc_private_call, GlobalText.android);
                Intent callIntent = new Intent(Intent.ACTION_VIEW);
                callIntent.setData(Uri.parse("tel:" + procList.get(i).getCustomerMobile().trim()));//change the number
                mContext.startActivity(callIntent);
                break;
        }
    }

    @Override
    public void onBindViewHolder(ProcurementPrivateLeadAdapter.MyViewHolder holder, int position) {
        PPLResponseDatum list = procList.get(position);
        i = position;
        holder.proc_leadWhatsapp.setOnClickListener(this);
        holder.proc_leadMessege.setOnClickListener(this);
        holder.proc_leadCall.setOnClickListener(this);

        try {
            if (position >= getItemCount() - 1 && getItemCount() > 0 || procList.size() <= 3) {
                lazyloaderPrivateLeads.PloadmorePrivate(getItemCount());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        validateDays(list, holder);
        validatefollowupDate(list, holder);
        updateLeadDetails(list, holder);
        updateLeadStatus(list, holder);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Save Data into the SingleTon
                try {
                    ConvertStockInstance.getInstance().setExecutiveid("" + list.getExecutiveId());
                    ConvertStockInstance.getInstance().setMfgyear(list.getStockModelYear());
                    ConvertStockInstance.getInstance().setMfgmonth("" + list.getManufacturingMonth());

                    ConvertStockInstance.getInstance().setRegisteryear(list.getRegisterYear());
                    ConvertStockInstance.getInstance().setRegistermonth(list.getRegisterMonth());

                    ConvertStockInstance.getInstance().setVehicletype(list.getVehicleType());

                    ConvertStockInstance.getInstance().setMake(list.getMake());
                    ConvertStockInstance.getInstance().setModel(list.getModel());
                    ConvertStockInstance.getInstance().setVariant(list.getVariant());

                    ConvertStockInstance.getInstance().setRegisternumber(list.getRegisterNo());
                    ConvertStockInstance.getInstance().setColor(list.getColor());

                    ConvertStockInstance.getInstance().setKms("" + list.getKms());
                    ConvertStockInstance.getInstance().setOwner("" + list.getOwner());
                    ConvertStockInstance.getInstance().setRegistercity("" + list.getCity());
                    ConvertStockInstance.getInstance().setInsurancetype("" + list.getInsuranceType());

                    ConvertStockInstance.getInstance().setLeadid("" + list.getId());
                    ConvertStockInstance.getInstance().setLeadremark(list.getRemark());
                    ConvertStockInstance.getInstance().setLeaddate(list.getLeadDate());
                    ConvertStockInstance.getInstance().setDealerid(list.getDealerId());
                    ConvertStockInstance.getInstance().setExename(list.getExecutiveName());

                    if (!list.getStockInsurance().equalsIgnoreCase("")) {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date date = null;
                        try {
                            date = dateFormat.parse(list.getStockInsurance());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
                        ConvertStockInstance.getInstance().setInsurancedate(dateFormat1.format(date));
                    } else {
                        ConvertStockInstance.getInstance().setInsurancedate("");
                    }

                    CommonMethods.setvalueAgainstKey(a, "PWLEADS", "PrivateLeads");
                    int pos = holder.getAdapterPosition();
                    Intent intent = new Intent(mContext, ProcurementDetails.class);
                    intent.putExtra("NM", list.getCustomerName().trim());
                    intent.putExtra("MN", list.getCustomerMobile().trim());
                    intent.putExtra("EM", list.getCustomerEmail().trim());
                    intent.putExtra("LS", list.getLeadStatus().trim());
                    intent.putExtra("PD", list.getLeadDate().trim());
                    intent.putExtra("MK", list.getMake().trim());
                    intent.putExtra("MD", list.getModel().trim());
                    intent.putExtra("MV", list.getVariant().trim());
                    intent.putExtra("PR", list.getStockExpectedPrice().toString().trim());
                    intent.putExtra("RN", list.getRegisterNo().trim());
                    intent.putExtra("NFD", list.getFollowDate().trim());
                    intent.putExtra("ENM", list.getExecutiveName().trim());
                    intent.putExtra("RGC", list.getCity().trim());
                    intent.putExtra("OWN", list.getOwner().toString().trim());
                    intent.putExtra("MYR", list.getRegisterYear().trim());
                    intent.putExtra("CLR", list.getColor().trim());
                    intent.putExtra("KMS", list.getKms().toString().trim());
                    intent.putExtra("DID", list.getDealerId().trim());
                    intent.putExtra("LID", list.getId().toString().trim());
                    intent.putExtra("MFY", list.getStockModelYear().trim());
                    intent.putExtra("RMK", list.getRemark().trim());
                    intent.putExtra("LD", holder.proc_leadDays.getText().toString().trim());
                    intent.putExtra("position", pos + "");
                    intent.putExtra("lead_type", list.getLeadType());
                    mContext.startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return procList.size();
    }

    private void openWhatsApp(String number) {
        String smsNumber = "91" + number; //without '+'
        try {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_VIEW);
            String url = "https://api.whatsapp.com/send?phone=" + smsNumber + "&text=" + "";
            sendIntent.setData(Uri.parse(url));
            mContext.startActivity(sendIntent);

        } catch (Exception e) {
            Toast.makeText(mContext, "Error/n" + e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private void validateDays(PPLResponseDatum mPrivateLeadDatum, MyViewHolder holder) {

        String createdDate = mPrivateLeadDatum.getCreatedAt();
        String[] splitDateTime = createdDate.split(" ", 10);
        String[] splitDate = splitDateTime[0].split("-", 10);
        String finalDate = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");
        Calendar c = Calendar.getInstance();
        String formattedDate = simpleDateFormat.format(c.getTime());
        String[] splitSpace = formattedDate.split(" ", 10);

        try {
            Date date1 = simpleDateFormat.parse(finalDate + " 00:00:00");
            Date date2 = simpleDateFormat.parse(splitSpace[0] + " 00:00:00");

            if (printDifference(date1, date2) == 0) {
                holder.proc_leadDays.setText("Today");
            } else if (printDifference(date1, date2) == 1) {
                holder.proc_leadDays.setText("Yesterday");
            } else if (printDifference(date1, date2) > 364) {
                holder.proc_leadDays.setText("" + printDifference(date1, date2) / 365 + " Y ago");
            } else {
                holder.proc_leadDays.setText("" + printDifference(date1, date2) + " D ago");
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void validatefollowupDate(PPLResponseDatum mPrivateLeadDatum, MyViewHolder holder) {

        try {
            String[] splitdatespace = mPrivateLeadDatum.getFollowDate().split(" ", 10);
            String[] splitdate1 = splitdatespace[0].split("-", 10);
            holder.proc_leadDate.setText(splitdate1[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate1[1])] + " " + splitdate1[0]);
        } catch (Exception e) {
            String leadDate = ((mPrivateLeadDatum.getFollowDate() == null || mPrivateLeadDatum.getFollowDate().trim().equalsIgnoreCase("NA") || mPrivateLeadDatum.getFollowDate().trim().equalsIgnoreCase("")) ? "NA" : mPrivateLeadDatum.getFollowDate().trim());
            holder.proc_leadDate.setText(leadDate);
        }
    }

    private void updateLeadDetails(PPLResponseDatum mPrivateLeadDatum, MyViewHolder holder) {

        holder.proc_leadName.setText(mPrivateLeadDatum.getCustomerName().trim());
        holder.proc_leadNum.setText(mPrivateLeadDatum.getCustomerMobile().trim());
        holder.proc_leadCompName.setText(mPrivateLeadDatum.getMake().trim());
        if (!mPrivateLeadDatum.getVariant().trim().equals("null")) {
            holder.proc_leadCarModel.setText(mPrivateLeadDatum.getModel().trim() + " " + mPrivateLeadDatum.getVariant().trim());
        } else {
            holder.proc_leadCarModel.setText(mPrivateLeadDatum.getModel().trim());
        }

    }

    private void updateLeadStatus(PPLResponseDatum mPrivateLeadDatum, MyViewHolder holder) {

        holder.proc_leadFollowupHistory.setVisibility(View.VISIBLE);

        try {
            holder.proc_leadFollowupHistory.setText("  " + capitalize(mPrivateLeadDatum.getLeadStatus().trim()));
            if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Open")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.open_28);
            } else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("") || mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("null")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.open_28);
               // holder.proc_leadFollowupHistory.setText("NA");
                holder.proc_leadFollowupHistory.setText("Open");
            }
            else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Outstation")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.ic_outstation);
            }else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Hot")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.hot_28);
            } else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Warm")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.warm_28);
            } else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Cold")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.cold_28);
            } else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Lost")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.lost_28);
            } else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Sold")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.sold_28);
            } else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Bought")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.sold_28);
            } else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Visiting")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.ic_visiting);
            } else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Search")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.ic_search_lead);
            } else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Not-Interested")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.ic_not_intrested);
            } else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("No-Response")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.ic_no_responce);
            } else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Finalised")) {
                holder.proc_leadFollowupHistory.setBackgroundResource(R.drawable.ic_finalized);
            } else if (mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("null")) {
               // holder.proc_leadFollowupHistory.setText("NA");
                holder.proc_leadFollowupHistory.setText("Open");
            }
        } catch (Exception e) {

        }
    }

    public long printDifference(Date startDate, Date endDate) {
        long different = endDate.getTime() - startDate.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long elapsedDays = different / daysInMilli;
        return elapsedDays;
    }

    private String capitalize(String capString) {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()) {
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }
    //searchfunctionaly PrivateLeads

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        procList.clear();
        if (charText.length() == 0) {
            procList.addAll(itemListSearch);
        } else {
            for (PPLResponseDatum it : itemListSearch) {
                if (it.getMake().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getModel().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getVariant().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getCustomerName().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getCustomerMobile().toLowerCase(Locale.getDefault()).contains(charText)
                ) {
                    procList.add(it);
                }
            }
        }
        try {
            lazyloaderPrivateLeads.PupdatecountPrivate(procList.size());
        } catch (Exception e) {

        }
        notifyDataSetChanged();
    }
}
