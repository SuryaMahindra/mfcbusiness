package com.mfcwl.mfc_dealer.RetrofitServicesLeadSection;

import android.util.Log;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.Model.LeadSection.PrivateLeadsModelRequest;
import com.mfcwl.mfc_dealer.Model.LeadSection.PrivateLeadsModelResponse;
import com.mfcwl.mfc_dealer.Model.LeadSection.WebLeadsModelRequest;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PWLRequest;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.RetrofitConfigLeadSection.PrivateLeadBaseService;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class PrivateLeadService extends PrivateLeadBaseService {

    public static void postPrivateLead(final PrivateLeadsModelRequest privateLeadRequest, final HttpCallResponse mHttpCallResponse){
        PrivateLeadsInterFace mInterFace = retrofit.create(PrivateLeadsInterFace.class);
        Call<PrivateLeadsModelResponse> mCall = mInterFace.privateleads(privateLeadRequest);
        Log.i("PrivateLeadService", " "+mCall.request().url().toString());
        String str = new Gson().toJson(privateLeadRequest, PrivateLeadsModelRequest.class);
        Log.i("PrivateLeadService", " "+str);
        mCall.enqueue(new Callback<PrivateLeadsModelResponse>() {
            @Override
            public void onResponse(@NotNull Call<PrivateLeadsModelResponse> call, @NotNull Response<PrivateLeadsModelResponse> response) {
                if(response.isSuccessful()){
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<PrivateLeadsModelResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });


    }

    public static void sendEmailpostPrivateLead(final PrivateLeadsModelRequest privateLeadRequest, final HttpCallResponse mHttpCallResponse){
        sendEmailPrivateLeadsInterFace mInterFace = retrofit.create(sendEmailPrivateLeadsInterFace.class);
        Call<String> mCall = mInterFace.privateleads(privateLeadRequest);
        mCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });


    }

    // this web leads but stock server cahll
    public static void sendEmailpostWebLead(final WebLeadsModelRequest webLeadRequest, final HttpCallResponse mHttpCallResponse){
        sendEmailWebLeadsInterFace mInterFace = retrofit.create(sendEmailWebLeadsInterFace.class);
        Call<String> mCall = mInterFace.privateleads(webLeadRequest);
        mCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });


    }

    // this web leads but stock server cahll
    public static void sendEmailProcpostWebLead(final PWLRequest webLeadRequest, final HttpCallResponse mHttpCallResponse){
        sendEmailProcWebLeadsInterFace mInterFace = retrofit.create(sendEmailProcWebLeadsInterFace.class);
        Call<String> mCall = mInterFace.privateleads(webLeadRequest);
        mCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });


    }


    //web in stock procurement
    public interface sendEmailProcWebLeadsInterFace{
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("franschisereport/sendwebleademail")
        Call<String> privateleads(@Body PWLRequest webLeadRequest);
    }

    //web in stock
    public interface sendEmailWebLeadsInterFace{
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("franschisereport/sendwebleademail")
        Call<String> privateleads(@Body WebLeadsModelRequest webLeadRequest);
    }



    public interface sendEmailPrivateLeadsInterFace{
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("dealer/private-leads/getlead")
        Call<String> privateleads(@Body PrivateLeadsModelRequest privateLeadRequest);
    }

    public interface PrivateLeadsInterFace{
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("dealer/private-leads/getlead")
        Call<PrivateLeadsModelResponse> privateleads(@Body PrivateLeadsModelRequest privateLeadRequest);
    }
}
