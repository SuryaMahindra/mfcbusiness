package com.mfcwl.mfc_dealer.RetrofitServicesLeadSection;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.Model.LeadSection.WebLeadsModelRequest;
import com.mfcwl.mfc_dealer.Model.LeadSection.WebLeadsModelResponse;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.DashboardWebLeadsRequest;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PPFHRequest;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PPFHResponse;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PWLRequest;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PWLResponse;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.ProcDashWebLeadResponse;
import com.mfcwl.mfc_dealer.Procurement.RetrofitConfig.ProcPrivateLeadService;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.RetrofitConfigLeadSection.WebLeadBaseService;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class WebLeadService extends WebLeadBaseService {

    //Sales Web Lead
    public static void postWebLead(final WebLeadsModelRequest webLeadsModelRequest, final HttpCallResponse mHttpCallResponse) {
        WebLeadsInterFace mInterFace = retrofit.create(WebLeadsInterFace.class);
        Call<WebLeadsModelResponse> mCall = mInterFace.webleads(webLeadsModelRequest);
        Log.i("TAG", " "+mCall.request().url().toString());
        String str = new Gson().toJson(webLeadsModelRequest, WebLeadsModelRequest.class);
        Log.i("TAG", " "+str);
        mCall.enqueue(new Callback<WebLeadsModelResponse>() {
            @Override
            public void onResponse(Call<WebLeadsModelResponse> call, Response<WebLeadsModelResponse> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<WebLeadsModelResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });


    }


    //Procurement Web Lead
    public static void procWebLead(Context context, PWLRequest webLeadsModelRequest, final HttpCallResponse mHttpCallResponse) {
        WebLeadsInterFace mInterFace = retrofit.create(WebLeadsInterFace.class);
        Call<PWLResponse> mCall = mInterFace.Procwebleads(webLeadsModelRequest);
        Log.i("TAG", " "+mCall.request().url().toString());
        String str = new Gson().toJson(webLeadsModelRequest, PWLRequest.class);
        Log.i("TAG", " "+str);
        mCall.enqueue(new Callback<PWLResponse>() {
            @Override
            public void onResponse(Call<PWLResponse> call, Response<PWLResponse> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), context);
                }
            }

            @Override
            public void onFailure(Call<PWLResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });


    }


    //Procurement Dashboard Web Lead
    public static void procDashboardWebLead(Context context, DashboardWebLeadsRequest webLeadsModelRequest, final HttpCallResponse mHttpCallResponse) {
        WebLeadsInterFace mInterFace = retrofit.create(WebLeadsInterFace.class);
        Call<ProcDashWebLeadResponse> mCall = mInterFace.Procdashwebleads(webLeadsModelRequest);
        mCall.enqueue(new Callback<ProcDashWebLeadResponse>() {
            @Override
            public void onResponse(@NotNull Call<ProcDashWebLeadResponse> call, @NotNull Response<ProcDashWebLeadResponse> response) {
                Log.i("HomeFragment", "URL " + mCall.request().url().toString());
                String str = new Gson().toJson(webLeadsModelRequest, DashboardWebLeadsRequest.class);
                Log.i("HomeFragment", "REQ " + str);

                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), context);
                }
            }

            @Override
            public void onFailure(Call<ProcDashWebLeadResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });
    }

    public static void PPFHLead(Context context, PPFHRequest privateLeadRequest, final HttpCallResponse mHttpCallResponse) {
        ProcPrivateLeadService.PPFHLeadsInterFace mInterFace = retrofit.create(ProcPrivateLeadService.PPFHLeadsInterFace.class);
        Call<PPFHResponse> mCall = mInterFace.privateleads(privateLeadRequest);
        mCall.enqueue(new Callback<PPFHResponse>() {
            @Override
            public void onResponse(Call<PPFHResponse> call, Response<PPFHResponse> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), context);
                }
            }

            @Override
            public void onFailure(Call<PPFHResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });

    }

    public interface WebLeadsInterFace {
        //SALES WEB LEAD
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("mfcwapp/getleads")
        Call<WebLeadsModelResponse> webleads(@Body WebLeadsModelRequest webLeadsModelRequest);

        //PROCUREMENT WEB LEAD
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("mfcwapp/getleads")
        Call<PWLResponse> Procwebleads(@Body PWLRequest PWLRequest);

        // DASHBOARD
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("mfcwapp/getleads")
        Call<ProcDashWebLeadResponse> Procdashwebleads(@Body DashboardWebLeadsRequest PWLRequest);

    }
}
