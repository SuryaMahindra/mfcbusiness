package com.mfcwl.mfc_dealer.LoginServices;

import android.content.Context;

import com.mfcwl.mfc_dealer.Model.AppVersion;
import com.mfcwl.mfc_dealer.Model.AppVersionResponse;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.RequestModel.LogOutRequest;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.ResponseModel.LogOutResponse;
import com.mfcwl.mfc_dealer.retrofitconfig.BaseService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class LoginService extends BaseService {


    public static void getAppVersion(final String mVersion, final String mDeviceType, final HttpCallResponse mHttpCallResponse) {
        AppversionInterface mInterface = retrofit.create(AppversionInterface.class);
        AppVersion mAppVersion = new AppVersion();
        mAppVersion.setApp_version(mVersion);
        mAppVersion.setDevice_type(mDeviceType);
        Call<AppVersionResponse> mCall = mInterface.checkAppVersion(mAppVersion);
        mCall.enqueue(new Callback<AppVersionResponse>() {
            @Override
            public void onResponse(Call<AppVersionResponse> call, Response<AppVersionResponse> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }

            }

            @Override
            public void onFailure(Call<AppVersionResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });


    }


    public interface AppversionInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("token/checkappversion")
        Call<AppVersionResponse> checkAppVersion(@Body AppVersion mAppVersion);

        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("logout/dealer-logout")
        Call<LogOutResponse> getLogOut(@Body LogOutRequest mLogOutRequest);
    }

    // Logout
    public static void logOutfromServer(LogOutRequest mLogOutRequest, Context mContext, HttpCallResponse mHttpCallResponse) {

        AppversionInterface mInterface = retrofit.create(AppversionInterface.class);
        Call<LogOutResponse> mCall = mInterface.getLogOut(mLogOutRequest);
        mCall.enqueue(new Callback<LogOutResponse>() {
            @Override
            public void onResponse(Call<LogOutResponse> call, Response<LogOutResponse> response) {

                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
                if (response.code() == 400) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 401) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 500) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 404) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 304) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 503) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 405) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                }
            }

            @Override
            public void onFailure(Call<LogOutResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });

    }
}
