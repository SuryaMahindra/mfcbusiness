package com.mfcwl.mfc_dealer.encrypt;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.mfcwl.mfc_dealer.Controller.Application;

import static com.mfcwl.mfc_dealer.Utility.GlobalText.BUCKET_KEY;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.BUCKET_SECRET;


public final class AWSSecretsManager {

    private static String TAG = AWSSecretsManager.class.getSimpleName();
    private static final String UTF_8 = "UTF-8";
    private static String AWS = "aws";

    /// Implementation of sort-of threadsafe Singleton
    private static AWSSecretsManager INSTANCE = new AWSSecretsManager();
    private AWSSecretsManager(){
    }
    public static AWSSecretsManager getInstance() {return INSTANCE;}

    public void parseAWSSecret(s3Res data) throws Exception {
       // if (jsonObject.has(AWS)) {
            try {
              //  final JSONObject awsObj = new JSONObject(jsonObject.getString(AWS));

                //Log.i(TAG, "getAccessKey=: "+data.getAccessKey());
                //Log.i(TAG, "getSecretKey=: "+data.getSecretKey());

                final DataEncrypt dataEncrypt = new DataEncrypt();

                final String decryptedData1 = new String(dataEncrypt.decrypt(data.getAccessKey()), UTF_8).trim();
                final String decryptedData2 = new String(dataEncrypt.decrypt(data.getSecretKey()), UTF_8).trim();


                set(BUCKET_KEY, decryptedData1);
               set(BUCKET_SECRET, decryptedData2);

               // Log.i(TAG, "BUCKET_KEY: " + get(BUCKET_KEY));
               // Log.i(TAG, "BUCKET_KEY: " + get(BUCKET_SECRET));

            } catch (Exception e) {
                Log.e(TAG, "ParseLogin:Exception " + e.getMessage());
                throw e;
            }
       // }
    }

    /// Please don't expose this method. This is internal functionality of this class
    private void set(final String key, final String value) {
        try {
            final SharedPreferences.Editor editor = getDefaultSharedPreferences().edit();
            editor.putString(key, value);
            editor.apply();
        } catch (Exception e) {
            Log.e(TAG, "AWSSecretsManager: " + e.getMessage());
        }
    }

    private SharedPreferences getDefaultSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(Application.getInstance());
    }

    public String get(final String key){
        return getDefaultSharedPreferences().getString(key, "");
    }
}
