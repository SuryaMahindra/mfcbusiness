package com.mfcwl.mfc_dealer.encrypt;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class s3Res {

    @SerializedName("accessKey")
    @Expose
    private String accessKey;
    @SerializedName("secretKey")
    @Expose
    private String secretKey;
    @SerializedName("aeskey")
    @Expose
    private String aeskey;

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getAeskey() {
        return aeskey;
    }

    public void setAeskey(String aeskey) {
        this.aeskey = aeskey;
    }

}
