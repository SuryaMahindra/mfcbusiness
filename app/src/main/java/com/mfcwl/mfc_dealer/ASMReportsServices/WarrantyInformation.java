package com.mfcwl.mfc_dealer.ASMReportsServices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WarrantyInformation {

    @SerializedName("warranty_type")
    @Expose
    private String warrantyType;
    @SerializedName("warranty_number")
    @Expose
    private Integer warrantyNumber;
    @SerializedName("revenue")
    @Expose
    private Double revenue;

    public String getWarrantyType() {
        return warrantyType;
    }

    public void setWarrantyType(String warrantyType) {
        this.warrantyType = warrantyType;
    }

    public Integer getWarrantyNumber() {
        return warrantyNumber;
    }

    public void setWarrantyNumber(Integer warrantyNumber) {
        this.warrantyNumber = warrantyNumber;
    }

    public Double getRevenue() {
        return revenue;
    }

    public void setRevenue(Double revenue) {
        this.revenue = revenue;
    }

}