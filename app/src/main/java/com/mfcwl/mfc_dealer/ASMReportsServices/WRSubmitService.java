package com.mfcwl.mfc_dealer.ASMReportsServices;

import android.util.Log;

import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.WeekReportSubmitModel;
import com.mfcwl.mfc_dealer.ASMModel.amsCreateRes;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.retrofitconfig.DasboardBaseService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class WRSubmitService extends DasboardBaseService
{

    public void pushSingleData(WeekReportSubmitModel weeklyReportSubmitModel , HttpCallResponse mCallResponse){
        WRSubmitInterface wrSubmitInterface = retrofit.create(WRSubmitInterface.class);
        Call<List<WeekReportSubmitModel>> call = wrSubmitInterface.pushData(weeklyReportSubmitModel);
        call.enqueue(new Callback<List<WeekReportSubmitModel>>() {
            @Override
            public void onResponse(Call<List<WeekReportSubmitModel>> call, Response<List<WeekReportSubmitModel>> response) {
                if(response.isSuccessful()){
                    mCallResponse.OnSuccess(response);
                    Log.d("Success Message"," "+response);
                }
            }

            @Override
            public void onFailure(Call<List<WeekReportSubmitModel>> call, Throwable throwable) {
                mCallResponse.OnFailure(throwable);
                Log.d("Failure Message"," "+throwable);
            }
        });
    }

    public void pushMultipleData(List<WeekReportSubmitModel> weeklyReportSubmitModel , HttpCallResponse mCallResponse){
        WRSubmitInterface wrSubmitInterface = retrofit.create(WRSubmitInterface.class);
        Call<amsCreateRes> call = wrSubmitInterface.pushLsitofData(weeklyReportSubmitModel);
        call.enqueue(new Callback<amsCreateRes>() {
            @Override
            public void onResponse(Call<amsCreateRes> call, Response<amsCreateRes> response) {
                if(response.isSuccessful()){
                    mCallResponse.OnSuccess(response);
                    Log.d("Success Message"," "+response);
                }
            }

            @Override
            public void onFailure(Call<amsCreateRes> call, Throwable throwable) {
                mCallResponse.OnFailure(throwable);
                Log.d("Failure Message"," "+throwable);
            }
        });
    }

public interface WRSubmitInterface
{
    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("franschisereport/submitreports")
    Call <List<WeekReportSubmitModel>> pushData(@Body WeekReportSubmitModel weeklyReportSubmitModel);

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("franschisereport/submitreports")
    Call <amsCreateRes> pushLsitofData(@Body List<WeekReportSubmitModel> weeklyReportSubmitModel);
}
}
