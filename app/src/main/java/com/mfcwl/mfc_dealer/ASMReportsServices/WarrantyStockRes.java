package com.mfcwl.mfc_dealer.ASMReportsServices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WarrantyStockRes{

    @SerializedName("warranty_balance")
    @Expose
    private Double warrantyBalance;
    @SerializedName("warranty_number")
    @Expose
    private Integer warrantyNumber;
    @SerializedName("warranty_revenue")
    @Expose
    private Double warrantyRevenue;
    @SerializedName("warranty_information")
    @Expose
    private List<WarrantyInformation> warrantyInformation = null;

    public Double getWarrantyBalance() {
        return warrantyBalance;
    }

    public void setWarrantyBalance(Double warrantyBalance) {
        this.warrantyBalance = warrantyBalance;
    }

    public Integer getWarrantyNumber() {
        return warrantyNumber;
    }

    public void setWarrantyNumber(Integer warrantyNumber) {
        this.warrantyNumber = warrantyNumber;
    }

    public Double getWarrantyRevenue() {
        return warrantyRevenue;
    }

    public void setWarrantyRevenue(Double warrantyRevenue) {
        this.warrantyRevenue = warrantyRevenue;
    }

    public List<WarrantyInformation> getWarrantyInformation() {
        return warrantyInformation;
    }

    public void setWarrantyInformation(List<WarrantyInformation> warrantyInformation) {
        this.warrantyInformation = warrantyInformation;
    }

}