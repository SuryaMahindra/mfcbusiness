package com.mfcwl.mfc_dealer.ASMReportsServices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DealerInfoRes {

    @SerializedName("dealer_name")
    @Expose
    private String dealerName;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("area_manager")
    @Expose
    private String areaManager;
    @SerializedName("state_head")
    @Expose
    private Object stateHead;

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAreaManager() {
        return areaManager;
    }

    public void setAreaManager(String areaManager) {
        this.areaManager = areaManager;
    }

    public Object getStateHead() {
        return stateHead;
    }

    public void setStateHead(Object stateHead) {
        this.stateHead = stateHead;
    }

}