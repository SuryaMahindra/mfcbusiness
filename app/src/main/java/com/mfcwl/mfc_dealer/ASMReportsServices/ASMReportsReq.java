package com.mfcwl.mfc_dealer.ASMReportsServices;

public class ASMReportsReq {
    private String dealer_code;
    private String dealer_type;
    private String from_date;
    private String to_date;

    public String getDealer_code() {
        return dealer_code;
    }

    public void setDealer_code(String dealer_code) {
        this.dealer_code = dealer_code;
    }

    public String getDealer_type() {
        return dealer_type;
    }

    public void setDealer_type(String dealer_type) {
        this.dealer_type = dealer_type;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }
}
