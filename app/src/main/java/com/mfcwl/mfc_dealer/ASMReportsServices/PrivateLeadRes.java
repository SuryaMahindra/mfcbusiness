package com.mfcwl.mfc_dealer.ASMReportsServices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrivateLeadRes {

    @SerializedName("private_lead")
    @Expose
    private Integer privateLead;

    public Integer getPrivateLead() {
        return privateLead;
    }

    public void setPrivateLead(Integer privateLead) {
        this.privateLead = privateLead;
    }

}