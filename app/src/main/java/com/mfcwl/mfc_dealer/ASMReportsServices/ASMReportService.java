package com.mfcwl.mfc_dealer.ASMReportsServices;

import android.util.Log;

import com.mfcwl.mfc_dealer.ASMSalesServices.TotalSalesCountRes;
import com.mfcwl.mfc_dealer.ASMSalesServices.WarrantySalesCountReq;
import com.mfcwl.mfc_dealer.ASMSalesServices.WarrantySalesCountRes;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.retrofitconfig.DasboardBaseService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class ASMReportService extends DasboardBaseService {

    public static void fetchDealerInfo(ASMReportsReq mReq, HttpCallResponse mCallResponse){
        ASMReportsInterface mInterface = retrofit.create(ASMReportsInterface.class);

        Call<DealerInfoRes> mCall = mInterface.getDealerInfo(mReq);
        mCall.enqueue(new Callback<DealerInfoRes>() {
            @Override
            public void onResponse(Call<DealerInfoRes> call, Response<DealerInfoRes> response) {
                if(response.isSuccessful()){
                    mCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<DealerInfoRes> call, Throwable t) {
                    mCallResponse.OnFailure(t);
            }
        });


    }

    public static void fetchStockInfo(ASMReportsReq mReq, HttpCallResponse mCallResponse){
        ASMReportsInterface mInterface = retrofit.create(ASMReportsInterface.class);

        Call<StockInfoRes> mCall = mInterface.getStocksInfo(mReq);

        mCall.enqueue(new Callback<StockInfoRes>() {
            @Override
            public void onResponse(Call<StockInfoRes> call, Response<StockInfoRes> response) {
                if(response.isSuccessful()){
                    mCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<StockInfoRes> call, Throwable t) {
                  mCallResponse.OnFailure(t);
            }
        });

    }

    public static void fetchSalesInfo(ASMReportsReq mReq, HttpCallResponse mCallResponse){
        ASMReportsInterface mInterface = retrofit.create(ASMReportsInterface.class);

        Call<SalesRes> mCall = mInterface.getSalesInfo(mReq);

        mCall.enqueue(new Callback<SalesRes>() {
            @Override
            public void onResponse(Call<SalesRes> call, Response<SalesRes> response) {
                if(response.isSuccessful()){
                    mCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<SalesRes> call, Throwable t) {
                mCallResponse.OnFailure(t);
            }
        });

    }


    public static void fetchWarrantyInfo(ASMReportsReq mReq, HttpCallResponse mCallResponse){
        ASMReportsInterface mInterface = retrofit.create(ASMReportsInterface.class);

        Call<WarrantyStockRes> mCall = mInterface.getWarrantyStockInfo(mReq);

        mCall.enqueue(new Callback<WarrantyStockRes>() {
            @Override
            public void onResponse(Call<WarrantyStockRes> call, Response<WarrantyStockRes> response) {
                if(response.isSuccessful()){
                    mCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<WarrantyStockRes> call, Throwable t) {
                mCallResponse.OnFailure(t);
            }
        });

    }


    public static void fetchprivateleadcountInfo(ASMReportsReq mReq, HttpCallResponse mCallResponse){
        ASMReportsInterface mInterface = retrofit.create(ASMReportsInterface.class);

        Call<PrivateLeadRes> mCall = mInterface.getprivateLeadCountInfo(mReq);

        mCall.enqueue(new Callback<PrivateLeadRes>() {
            @Override
            public void onResponse(Call<PrivateLeadRes> call, Response<PrivateLeadRes> response) {
                if(response.isSuccessful()){
                    mCallResponse.OnSuccess(response);

                }
            }

            @Override
            public void onFailure(Call<PrivateLeadRes> call, Throwable t) {
                mCallResponse.OnFailure(t);
            }
        });

    }

    public interface ASMReportsInterface{
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("franschisereport/dealerinformation")
        Call<DealerInfoRes> getDealerInfo(@Body ASMReportsReq mReq);

        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("franschisereport/stockinformation")
        Call<StockInfoRes> getStocksInfo(@Body ASMReportsReq mReq);

        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("franschisereport/sales")
        Call<SalesRes> getSalesInfo(@Body ASMReportsReq mReq);

        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("franschisereport/warrantystockinformation")
        Call<WarrantyStockRes> getWarrantyStockInfo(@Body ASMReportsReq mReq);
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("franschisereport/salesleadinformation")
        Call<PrivateLeadRes> getprivateLeadCountInfo(@Body ASMReportsReq mReq);
    }
}
