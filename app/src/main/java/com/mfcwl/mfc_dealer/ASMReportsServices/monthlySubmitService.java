package com.mfcwl.mfc_dealer.ASMReportsServices;

import android.util.Log;

import com.mfcwl.mfc_dealer.ASMModel.monthlyTagetRes;
import com.mfcwl.mfc_dealer.ASMModel.monthlyTargetReq;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.retrofitconfig.DasboardBaseService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class monthlySubmitService extends DasboardBaseService
{

    public void pushMultipleData(monthlyTargetReq monthlyTargetReq , HttpCallResponse mCallResponse){
        SubmitInterface wrSubmitInterface = retrofit.create(SubmitInterface.class);
        Call<monthlyTagetRes> call = wrSubmitInterface.pushLsitofData(monthlyTargetReq);
        call.enqueue(new Callback<monthlyTagetRes>() {
            @Override
            public void onResponse(Call<monthlyTagetRes> call, Response<monthlyTagetRes> response) {
                if(response.isSuccessful()){
                    mCallResponse.OnSuccess(response);
                    Log.d("Success Message"," "+response);
                }
            }

            @Override
            public void onFailure(Call<monthlyTagetRes> call, Throwable throwable) {
                mCallResponse.OnFailure(throwable);
                Log.d("Failure Message"," "+throwable);
            }
        });
    }

public interface SubmitInterface
{

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("franchise/SubmitSMDashBoardInformation")
    Call <monthlyTagetRes> pushLsitofData(@Body monthlyTargetReq monthlyTargetReq);
}
}
