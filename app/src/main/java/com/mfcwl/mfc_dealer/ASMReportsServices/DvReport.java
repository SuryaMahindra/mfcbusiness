package com.mfcwl.mfc_dealer.ASMReportsServices;

import android.util.Log;

import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.DVEmail;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.DVReportFetchModel;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.DvReportModel;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.SubmittedDVReport;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.WeekReportSubmitModel;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.WeeklyRModel;
import com.mfcwl.mfc_dealer.AddStockModel.DVRReqModel;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.retrofitconfig.DasboardBaseService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public class DvReport extends DasboardBaseService {

    public static void fetchDVInfo(DVReportFetchModel mReq, HttpCallResponse mCallResponse){
        ASMReportsInterfaceDv mInterface = retrofit.create(ASMReportsInterfaceDv.class);

        Call<List<SubmittedDVReport> > mCall = mInterface.getDVReport(mReq);
        mCall.enqueue(new Callback<List<SubmittedDVReport>>() {
            @Override
            public void onResponse(Call<List<SubmittedDVReport>> call, Response<List<SubmittedDVReport>> response) {
                mCallResponse.OnSuccess(response);
            }

            @Override
            public void onFailure(Call<List<SubmittedDVReport>> call, Throwable t) {
                mCallResponse.OnFailure(t);
            }
        });

    }

    public static void pushdata(DVRReqModel dm , HttpCallResponse mCallResponse){
        ASMReportsInterfaceDv mInterface = retrofit.create(ASMReportsInterfaceDv.class);
        Call<DvReportModel> call = mInterface.pushData(dm);
        call.enqueue(new Callback<DvReportModel>() {
            @Override
            public void onResponse(Call<DvReportModel> call, Response<DvReportModel> response) {
                if(response.isSuccessful()){
                    mCallResponse.OnSuccess(response);
                    Log.d("onsuccesssmsg"," "+response);
                }
            }

            @Override
            public void onFailure(Call<DvReportModel> call, Throwable t) {
            mCallResponse.OnFailure(t);
                Log.d("onfailuremsg"," "+t);
            }
        });
    }

    public void pushEMail(List<DVEmail> dm ,String i, HttpCallResponse mCallResponse){
        ASMReportsInterfaceDv mInterface = retrofit.create(ASMReportsInterfaceDv.class);
        Call<String> call = mInterface.pushEmail(dm,i);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                mCallResponse.OnSuccess(response);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                    mCallResponse.OnFailure(t);
            }
        });
    }

    public void getWR(WeeklyRModel wr,HttpCallResponse mCallResponse){
        ASMReportsInterfaceDv mInterface = retrofit.create(ASMReportsInterfaceDv.class);
        Call<List<WeekReportSubmitModel>> call = mInterface.getWR(wr);
        call.enqueue(new Callback<List<WeekReportSubmitModel>>() {
            @Override
            public void onResponse(Call<List<WeekReportSubmitModel>> call, Response<List<WeekReportSubmitModel>> response) {
                mCallResponse.OnSuccess(response);
            }

            @Override
            public void onFailure(Call<List<WeekReportSubmitModel>> call, Throwable t) {
                mCallResponse.OnFailure(t);
            }
        });
    }

    public void pushEMailWR(List<WeekReportSubmitModel> dm ,String i, HttpCallResponse mCallResponse){
        ASMReportsInterfaceDv mInterface = retrofit.create(ASMReportsInterfaceDv.class);
        Call<String> call = mInterface.pushEmailWR(dm,i);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                mCallResponse.OnSuccess(response);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                mCallResponse.OnFailure(t);
            }
        });
    }



    public interface ASMReportsInterfaceDv{
//        @Headers("Content-Type: application/json; charset=utf-8")
//        @POST("franschisereport/dealerinformation")
//        Call<DealerInfoRes> getDealerInfo(@Body ASMReportsReq mReq);


        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("franschisereport/insertdealervisitreport")
        Call<DvReportModel> pushData(@Body DVRReqModel mdvReportModel);

        @Headers("Content-Type: application/json; charset=utf-8")
        //@HTTP(method = "GET", path = "franschisereport/getdealervisitreport",hasBody = true)
        @POST("franschisereport/getdealervisitreport")
        Call<List<SubmittedDVReport>> getDVReport(@Body DVReportFetchModel mdvReportModel);

        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("franschisereport/dealervisitsendemail/")
        Call<String> pushEmail(@Body List<DVEmail> submittedDVReport, @Query("id") String id);

        @Headers("Content-Type: application/json; charset=utf-8")
//        @HTTP(method = "GET", path = "franschisereport/getdealerreports", hasBody = true)
        @POST("franschisereport/getdealerreports")
        Call<List<WeekReportSubmitModel>> getWR(@Body WeeklyRModel submittedDVReport);

        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("franschisereport/sendweeklyreportemail/")
        Call<String> pushEmailWR(@Body List<WeekReportSubmitModel> submittedDVReport, @Query("id") String id);



    }

}
