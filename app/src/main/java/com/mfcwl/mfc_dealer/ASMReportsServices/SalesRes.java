package com.mfcwl.mfc_dealer.ASMReportsServices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalesRes {

    @SerializedName("booked")
    @Expose
    private Integer booked;
    @SerializedName("sold")
    @Expose
    private Integer sold;

    public Integer getBooked() {
        return booked;
    }

    public void setBooked(Integer booked) {
        this.booked = booked;
    }

    public Integer getSold() {
        return sold;
    }

    public void setSold(Integer sold) {
        this.sold = sold;
    }

}