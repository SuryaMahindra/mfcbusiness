package com.mfcwl.mfc_dealer.ASMReportsServices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StockInfoRes {

    @SerializedName("store_stock_count")
    @Expose
    private Integer storeStockCount;
    @SerializedName("stock_without_image_count")
    @Expose
    private Integer stockWithoutImageCount;
    @SerializedName("certified_stock_count")
    @Expose
    private Integer certifiedStockCount;
    @SerializedName("stock_greaterthan90days_count")
    @Expose
    private Integer stockGreaterthan90daysCount;
    @SerializedName("stock_60daysto90days_old_count")
    @Expose
    private Integer stock60daysto90daysOldCount;
    @SerializedName("stock_30daysto60days_old_count")
    @Expose
    private Integer stock30daysto60daysOldCount;
    @SerializedName("stock_lesstahn30days_old_count")
    @Expose
    private Integer stockLesstahn30daysOldCount;

    public Integer getStoreStockCount() {
        return storeStockCount;
    }

    public void setStoreStockCount(Integer storeStockCount) {
        this.storeStockCount = storeStockCount;
    }

    public Integer getStockWithoutImageCount() {
        return stockWithoutImageCount;
    }

    public void setStockWithoutImageCount(Integer stockWithoutImageCount) {
        this.stockWithoutImageCount = stockWithoutImageCount;
    }

    public Integer getCertifiedStockCount() {
        return certifiedStockCount;
    }

    public void setCertifiedStockCount(Integer certifiedStockCount) {
        this.certifiedStockCount = certifiedStockCount;
    }

    public Integer getStockGreaterthan90daysCount() {
        return stockGreaterthan90daysCount;
    }

    public void setStockGreaterthan90daysCount(Integer stockGreaterthan90daysCount) {
        this.stockGreaterthan90daysCount = stockGreaterthan90daysCount;
    }

    public Integer getStock60daysto90daysOldCount() {
        return stock60daysto90daysOldCount;
    }

    public void setStock60daysto90daysOldCount(Integer stock60daysto90daysOldCount) {
        this.stock60daysto90daysOldCount = stock60daysto90daysOldCount;
    }

    public Integer getStock30daysto60daysOldCount() {
        return stock30daysto60daysOldCount;
    }

    public void setStock30daysto60daysOldCount(Integer stock30daysto60daysOldCount) {
        this.stock30daysto60daysOldCount = stock30daysto60daysOldCount;
    }

    public Integer getStockLesstahn30daysOldCount() {
        return stockLesstahn30daysOldCount;
    }

    public void setStockLesstahn30daysOldCount(Integer stockLesstahn30daysOldCount) {
        this.stockLesstahn30daysOldCount = stockLesstahn30daysOldCount;
    }

}