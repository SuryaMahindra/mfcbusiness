package com.mfcwl.mfc_dealer.AddStockModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class ColourMaster {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("colour")
    @Expose
    private String colour;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }
}
