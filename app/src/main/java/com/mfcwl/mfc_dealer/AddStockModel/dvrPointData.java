package com.mfcwl.mfc_dealer.AddStockModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class dvrPointData{

        @SerializedName("points_discussed")
        @Expose
        private String pointsDiscussed;
        @SerializedName("action_plan")
        @Expose
        private String actionPlan;
        @SerializedName("target_date")
        @Expose
        private String targetDate;
        @SerializedName("focus_area")
        @Expose
        private String focusArea;
        @SerializedName("responsibility")
        @Expose
        private String responsibility;

        public String getPointsDiscussed() {
            return pointsDiscussed;
        }

        public void setPointsDiscussed(String pointsDiscussed) {
            this.pointsDiscussed = pointsDiscussed;
        }

        public String getActionPlan() {
            return actionPlan;
        }

        public void setActionPlan(String actionPlan) {
            this.actionPlan = actionPlan;
        }

        public String getTargetDate() {
            return targetDate;
        }

        public void setTargetDate(String targetDate) {
            this.targetDate = targetDate;
        }

        public String getFocusArea() {
            return focusArea;
        }

        public void setFocusArea(String focusArea) {
            this.focusArea = focusArea;
        }

        public String getResponsibility() {
            return responsibility;
        }

        public void setResponsibility(String responsibility) {
            this.responsibility = responsibility;
        }
}
