package com.mfcwl.mfc_dealer.AddStockModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommercialMakelist {
    @SerializedName("make")
    @Expose
    private String make;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    //for model
    @SerializedName("model_values")
    @Expose
    private List<ModelValue> modelValues = null;

    public List<ModelValue> getModelValues() {
        return modelValues;
    }

    public void setModelValues(List<ModelValue> modelValues) {
        this.modelValues = modelValues;
    }
}
