package com.mfcwl.mfc_dealer.AddStockModel;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class ModelDetails {
    @SerializedName("make")
    @Expose
    private String make;
    @SerializedName("model_values")
    @Expose
    private List<ModelValue> modelValues = null;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public List<ModelValue> getModelValues() {
        return modelValues;
    }

    public void setModelValues(List<ModelValue> modelValues) {
        this.modelValues = modelValues;
    }

}
