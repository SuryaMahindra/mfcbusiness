package com.mfcwl.mfc_dealer.AddStockModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class RequestCreateStock {


    @SerializedName("stock_source")
    @Expose
    private String stockSource;
    @SerializedName("posted_date")
    @Expose
    private String postedDate;
    @SerializedName("vehicle_make")
    @Expose
    private String vehicleMake;
    @SerializedName("vehicle_model")
    @Expose
    private String vehicleModel;
    @SerializedName("vehicle_variant")
    @Expose
    private String vehicleVariant;
    @SerializedName("reg_month")
    @Expose
    private String regMonth;
    @SerializedName("reg_year")
    @Expose
    private String regYear;
    @SerializedName("registraion_city")
    @Expose
    private String registraionCity;
    @SerializedName("registration_number")
    @Expose
    private String registrationNumber;
    @SerializedName("colour")
    @Expose
    private String colour;
    @SerializedName("kilometer")
    @Expose
    private String kilometer;
    @SerializedName("owner")
    @Expose
    private String owner;
    @SerializedName("insurance")
    @Expose
    private String insurance;
    @SerializedName("insurance_exp_date")
    @Expose
    private String insuranceExpDate;
    @SerializedName("selling_price")
    @Expose
    private String sellingPrice;
    @SerializedName("dealer_code")
    @Expose
    private String dealerCode;
    @SerializedName("is_display")
    @Expose
    private String isDisplay;
    @SerializedName("bought_price")
    @Expose
    private String boughtPrice;
    @SerializedName("refurbishment_cost")
    @Expose
    private String refurbishmentCost;
    @SerializedName("procurement_executive_id")
    @Expose
    private String procurementExecutiveId;
    @SerializedName("procurement_executive_name")
    @Expose
    private String procurementExecutiveName;
    @SerializedName("cng_kit")
    @Expose
    private String cngKit;
    @SerializedName("chassis_number")
    @Expose
    private String chassisNumber;
    @SerializedName("engine_number")
    @Expose
    private String engineNumber;
    @SerializedName("private_vehicle")
    @Expose
    private String privateVehicle;
    @SerializedName("comments")
    @Expose
    private String comments;
    @SerializedName("manufacture_month")
    @Expose
    private String manufactureMonth;
    @SerializedName("dealer_price")
    @Expose
    private String dealerPrice;
    @SerializedName("is_offload")
    @Expose
    private String isOffload;
    @SerializedName("manufacture_year")
    @Expose
    private String manufactureYear;
    @SerializedName("is_featured_car")
    @Expose
    private String isFeaturedCar;
    @SerializedName("created_by_device")
    @Expose
    private String createdByDevice;

    public String getStockSource() {
        return stockSource;
    }

    public void setStockSource(String stockSource) {
        this.stockSource = stockSource;
    }

    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getVehicleVariant() {
        return vehicleVariant;
    }

    public void setVehicleVariant(String vehicleVariant) {
        this.vehicleVariant = vehicleVariant;
    }

    public String getRegMonth() {
        return regMonth;
    }

    public void setRegMonth(String regMonth) {
        this.regMonth = regMonth;
    }

    public String getRegYear() {
        return regYear;
    }

    public void setRegYear(String regYear) {
        this.regYear = regYear;
    }

    public String getRegistraionCity() {
        return registraionCity;
    }

    public void setRegistraionCity(String registraionCity) {
        this.registraionCity = registraionCity;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getKilometer() {
        return kilometer;
    }

    public void setKilometer(String kilometer) {
        this.kilometer = kilometer;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getInsuranceExpDate() {
        return insuranceExpDate;
    }

    public void setInsuranceExpDate(String insuranceExpDate) {
        this.insuranceExpDate = insuranceExpDate;
    }

    public String getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(String sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getIsDisplay() {
        return isDisplay;
    }

    public void setIsDisplay(String isDisplay) {
        this.isDisplay = isDisplay;
    }

    public String getBoughtPrice() {
        return boughtPrice;
    }

    public void setBoughtPrice(String boughtPrice) {
        this.boughtPrice = boughtPrice;
    }

    public String getRefurbishmentCost() {
        return refurbishmentCost;
    }

    public void setRefurbishmentCost(String refurbishmentCost) {
        this.refurbishmentCost = refurbishmentCost;
    }

    public String getProcurementExecutiveId() {
        return procurementExecutiveId;
    }

    public void setProcurementExecutiveId(String procurementExecutiveId) {
        this.procurementExecutiveId = procurementExecutiveId;
    }

    public String getProcurementExecutiveName() {
        return procurementExecutiveName;
    }

    public void setProcurementExecutiveName(String procurementExecutiveName) {
        this.procurementExecutiveName = procurementExecutiveName;
    }

    public String getCngKit() {
        return cngKit;
    }

    public void setCngKit(String cngKit) {
        this.cngKit = cngKit;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public String getEngineNumber() {
        return engineNumber;
    }

    public void setEngineNumber(String engineNumber) {
        this.engineNumber = engineNumber;
    }

    public String getPrivateVehicle() {
        return privateVehicle;
    }

    public void setPrivateVehicle(String privateVehicle) {
        this.privateVehicle = privateVehicle;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getManufactureMonth() {
        return manufactureMonth;
    }

    public void setManufactureMonth(String manufactureMonth) {
        this.manufactureMonth = manufactureMonth;
    }

    public String getDealerPrice() {
        return dealerPrice;
    }

    public void setDealerPrice(String dealerPrice) {
        this.dealerPrice = dealerPrice;
    }

    public String getIsOffload() {
        return isOffload;
    }

    public void setIsOffload(String isOffload) {
        this.isOffload = isOffload;
    }

    public String getManufactureYear() {
        return manufactureYear;
    }

    public void setManufactureYear(String manufactureYear) {
        this.manufactureYear = manufactureYear;
    }

    public String getIsFeaturedCar() {
        return isFeaturedCar;
    }

    public void setIsFeaturedCar(String isFeaturedCar) {
        this.isFeaturedCar = isFeaturedCar;
    }

    public String getCreatedByDevice() {
        return createdByDevice;
    }

    public void setCreatedByDevice(String createdByDevice) {
        this.createdByDevice = createdByDevice;
    }
}
