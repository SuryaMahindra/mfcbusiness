package com.mfcwl.mfc_dealer.AddStockModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReqUpdateStock {

    @SerializedName("stock_id")
    @Expose
    private String stockId;
    @SerializedName("stock_source")
    @Expose
    private String stockSource;
    @SerializedName("posted_date")
    @Expose
    private String postedDate;
    @SerializedName("vehicle_make")
    @Expose
    private String vehicleMake;
    @SerializedName("vehicle_model")
    @Expose
    private String vehicleModel;
    @SerializedName("vehicle_variant")
    @Expose
    private String vehicleVariant;
    @SerializedName("reg_month")
    @Expose
    private String regMonth;
    @SerializedName("reg_year")
    @Expose
    private String regYear;
    @SerializedName("registraion_city")
    @Expose
    private String registraionCity;
    @SerializedName("registration_number")
    @Expose
    private String registrationNumber;
    @SerializedName("colour")
    @Expose
    private String colour;
    @SerializedName("kilometer")
    @Expose
    private String kilometer;
    @SerializedName("owner")
    @Expose
    private String owner;
    @SerializedName("insurance")
    @Expose
    private String insurance;
    @SerializedName("insurance_exp_date")
    @Expose
    private String insuranceExpDate;
    @SerializedName("selling_price")
    @Expose
    private String sellingPrice;
    @SerializedName("bought_price")
    @Expose
    private String boughtPrice;
    @SerializedName("refurbishment_cost")
    @Expose
    private String refurbishmentCost;
    @SerializedName("procurement_executive_id")
    @Expose
    private String procurementExecutiveId;
    @SerializedName("cng_kit")
    @Expose
    private String cngKit;
    @SerializedName("private_vehicle")
    @Expose
    private String privateVehicle;
    @SerializedName("comments")
    @Expose
    private String comments;
    @SerializedName("manufacture_month")
    @Expose
    private String manufactureMonth;
    @SerializedName("is_offload")
    @Expose
    private String isOffload;
    @SerializedName("manufacture_year")
    @Expose
    private String manufactureYear;
    @SerializedName("updated_by_device")
    @Expose
    private String updatedByDevice;
    @SerializedName("is_featured_car")
    @Expose
    private String isFeaturedCar;
    @SerializedName("is_booked")
    @Expose
    private String isBooked;
    @SerializedName("actual_selling_price")
    @Expose
    private String actualSellingPrice;
    @SerializedName("sales_executive_id")
    @Expose
    private String salesExecutiveId;
    @SerializedName("finance_required")
    @Expose
    private boolean financeRequired;
    @SerializedName("chassis_number")
    @Expose
    private String chassisNumber;
    @SerializedName("engine_number")
    @Expose
    private String engineNumber;
    @SerializedName("dealer_price")
    @Expose
    private String dealerPrice;
    @SerializedName("dealer_code")
    @Expose
    private String dealerCode;

    public String getStockId() {
        return stockId;
    }

    public void setStockId(String stockId) {
        this.stockId = stockId;
    }

    public String getStockSource() {
        return stockSource;
    }

    public void setStockSource(String stockSource) {
        this.stockSource = stockSource;
    }

    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getVehicleVariant() {
        return vehicleVariant;
    }

    public void setVehicleVariant(String vehicleVariant) {
        this.vehicleVariant = vehicleVariant;
    }

    public String getRegMonth() {
        return regMonth;
    }

    public void setRegMonth(String regMonth) {
        this.regMonth = regMonth;
    }

    public String getRegYear() {
        return regYear;
    }

    public void setRegYear(String regYear) {
        this.regYear = regYear;
    }

    public String getRegistraionCity() {
        return registraionCity;
    }

    public void setRegistraionCity(String registraionCity) {
        this.registraionCity = registraionCity;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getKilometer() {
        return kilometer;
    }

    public void setKilometer(String kilometer) {
        this.kilometer = kilometer;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getInsuranceExpDate() {
        return insuranceExpDate;
    }

    public void setInsuranceExpDate(String insuranceExpDate) {
        this.insuranceExpDate = insuranceExpDate;
    }

    public String getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(String sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public String getBoughtPrice() {
        return boughtPrice;
    }

    public void setBoughtPrice(String boughtPrice) {
        this.boughtPrice = boughtPrice;
    }

    public String getRefurbishmentCost() {
        return refurbishmentCost;
    }

    public void setRefurbishmentCost(String refurbishmentCost) {
        this.refurbishmentCost = refurbishmentCost;
    }

    public String getProcurementExecutiveId() {
        return procurementExecutiveId;
    }

    public void setProcurementExecutiveId(String procurementExecutiveId) {
        this.procurementExecutiveId = procurementExecutiveId;
    }

    public String getCngKit() {
        return cngKit;
    }

    public void setCngKit(String cngKit) {
        this.cngKit = cngKit;
    }

    public String getPrivateVehicle() {
        return privateVehicle;
    }

    public void setPrivateVehicle(String privateVehicle) {
        this.privateVehicle = privateVehicle;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getManufactureMonth() {
        return manufactureMonth;
    }

    public void setManufactureMonth(String manufactureMonth) {
        this.manufactureMonth = manufactureMonth;
    }

    public String getIsOffload() {
        return isOffload;
    }

    public void setIsOffload(String isOffload) {
        this.isOffload = isOffload;
    }

    public String getManufactureYear() {
        return manufactureYear;
    }

    public void setManufactureYear(String manufactureYear) {
        this.manufactureYear = manufactureYear;
    }

    public String getUpdatedByDevice() {
        return updatedByDevice;
    }

    public void setUpdatedByDevice(String updatedByDevice) {
        this.updatedByDevice = updatedByDevice;
    }

    public String getIsFeaturedCar() {
        return isFeaturedCar;
    }

    public void setIsFeaturedCar(String isFeaturedCar) {
        this.isFeaturedCar = isFeaturedCar;
    }

    public String getIsBooked() {
        return isBooked;
    }

    public void setIsBooked(String isBooked) {
        this.isBooked = isBooked;
    }

    public String getActualSellingPrice() {
        return actualSellingPrice;
    }

    public void setActualSellingPrice(String actualSellingPrice) {
        this.actualSellingPrice = actualSellingPrice;
    }

    public String getSalesExecutiveId() {
        return salesExecutiveId;
    }

    public void setSalesExecutiveId(String salesExecutiveId) {
        this.salesExecutiveId = salesExecutiveId;
    }

    public boolean isFinanceRequired() {
        return financeRequired;
    }

    public void setFinanceRequired(boolean financeRequired) {
        this.financeRequired = financeRequired;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public String getEngineNumber() {
        return engineNumber;
    }

    public void setEngineNumber(String engineNumber) {
        this.engineNumber = engineNumber;
    }

    public String getDealerPrice() {
        return dealerPrice;
    }

    public void setDealerPrice(String dealerPrice) {
        this.dealerPrice = dealerPrice;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }
}
