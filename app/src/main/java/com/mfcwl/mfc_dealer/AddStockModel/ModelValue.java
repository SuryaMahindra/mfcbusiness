package com.mfcwl.mfc_dealer.AddStockModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class ModelValue {


    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("variant")
    @Expose
    private String variant;
    @SerializedName("display")
    @Expose
    private String display;
    @SerializedName("fuel_type")
    @Expose
    private String fuelType;
    @SerializedName("segment_type")
    @Expose
    private String segmentType;
    @SerializedName("body_style")
    @Expose
    private String bodyStyle;
    @SerializedName("start_month")
    @Expose
    private String startMonth;
    @SerializedName("start_year")
    @Expose
    private int startYear;
    @SerializedName("end_month")
    @Expose
    private String endMonth;
    @SerializedName("end_year")
    @Expose
    private int endYear;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("price")
    @Expose
    private int price;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getSegmentType() {
        return segmentType;
    }

    public void setSegmentType(String segmentType) {
        this.segmentType = segmentType;
    }

    public String getBodyStyle() {
        return bodyStyle;
    }

    public void setBodyStyle(String bodyStyle) {
        this.bodyStyle = bodyStyle;
    }

    public String getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(String startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public String getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(String endMonth) {
        this.endMonth = endMonth;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
