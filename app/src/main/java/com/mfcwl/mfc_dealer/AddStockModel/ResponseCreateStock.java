package com.mfcwl.mfc_dealer.AddStockModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class ResponseCreateStock {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("stock_id")
    @Expose
    private int stockId;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStockId() {
        return stockId;
    }

    public void setStockId(int stockId) {
        this.stockId = stockId;
    }
}
