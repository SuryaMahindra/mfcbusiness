package com.mfcwl.mfc_dealer.AddStockModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class CityMaster {
    @SerializedName("citycode")
    @Expose
    private int citycode;
    @SerializedName("cityname")
    @Expose
    private String cityname;

    public int getCitycode() {
        return citycode;
    }

    public void setCitycode(int citycode) {
        this.citycode = citycode;
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

}
