package com.mfcwl.mfc_dealer.ASMFragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.MessageCDataTwo;
import com.mfcwl.mfc_dealer.ASMModel.MessageCenterRes;
import com.mfcwl.mfc_dealer.ASMModel.MessageCenterResData;
import com.mfcwl.mfc_dealer.Adapter.MessageCenterAdapter;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Fragment.LeadSection.LeadsFragmentSection;
import com.mfcwl.mfc_dealer.Fragment.StockFragment;
import com.mfcwl.mfc_dealer.Procurement.Fragment.ProcurementTAB;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.SalesStocks.Fragment.SalesInstance;
import com.mfcwl.mfc_dealer.SalesStocks.Fragment.SalesStockTAB;
import com.mfcwl.mfc_dealer.StateHead.StateHeadDashBoardFragment;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.ZonalHead.ZonalHeadDashBoardFragment;
import com.mfcwl.mfc_dealer.retrofitconfig.MessageCenterServices;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.navigation;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchClose;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchVisble;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.tv_header_title;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ASM;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_STATEHEAD;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ZONALHEAD;

public class MessageCenter extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    TextView warrentybalance, mtd;

    public static MenuItem items;
    public static Menu menus;

    SwipeRefreshLayout mess_swipeRefresh;
    RecyclerView mess_recyclerView;
    TextView mess_noResults;
    private List<MessageCDataTwo> mLeadsDatumList;
    MessageCenterAdapter madapter;
    public String TAG = getClass().getSimpleName();

    int countss = 0;
    Activity activity;
    Context context;

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        //menu.clear();

        MenuItem asmHome = menu.findItem(R.id.asmHome);

        asmHome.setVisible(true);
        asmHome.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (CommonMethods.getstringvaluefromkey(activity, "user_type")) {
                    case USERTYPE_ZONALHEAD: {
                        Fragment loadFragments = new ZonalHeadDashBoardFragment();
                        loadFragment(loadFragments);
                    }
                    break;

                    case USERTYPE_STATEHEAD: {
                        Fragment loadFragments = new StateHeadDashBoardFragment();
                        loadFragment(loadFragments);
                    }
                    break;

                    case USERTYPE_ASM:
                    default: {
                        Fragment loadFragments = new DasboardFragment();
                        loadFragment(loadFragments);
                    }
                    break;
                }
                return true;

            }
        });


        MenuItem asm_mail = menu.findItem(R.id.asm_mail);
        MenuItem add_image = menu.findItem(R.id.add_image);
        MenuItem share = menu.findItem(R.id.share);
        MenuItem delete_fea = menu.findItem(R.id.delete_fea);
        MenuItem stock_fil_clear = menu.findItem(R.id.stock_fil_clear);
        MenuItem notification_bell = menu.findItem(R.id.notification_bell);
        MenuItem notification_cancel = menu.findItem(R.id.notification_cancel);

        asm_mail.setVisible(false);
        add_image.setVisible(false);
        share.setVisible(false);
        delete_fea.setVisible(false);
        stock_fil_clear.setVisible(false);
        notification_bell.setVisible(false);
        notification_cancel.setVisible(false);


    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.fragment_messagecenter, container, false);
        setHasOptionsMenu(true);

        navigation.getMenu().clear();
        navigation.inflateMenu(R.menu.navigation);

        navigation.setVisibility(View.VISIBLE);

        CommonMethods.setvalueAgainstKey(getActivity(), "asm_pages", "messagecenter");

        warrentybalance = view.findViewById(R.id.warrentybalance);
        mtd = view.findViewById(R.id.mtd);
        CommonMethods.setvalueAgainstKey(getActivity(), "MessageCenter", "");

        String value = CommonMethods.getstringvaluefromkey(getActivity(), "getBalance");

        warrentybalance.setText("\u20B9" + value + "\n" + "warranty balance");
        mtd.setText("\u20B9" + CommonMethods.getstringvaluefromkey(getActivity(), "getWarrantypunch") + "\n" + "T.W.S.R.(MTD)");


        mess_swipeRefresh = view.findViewById(R.id.mess_swipeRefresh);
        mess_recyclerView = view.findViewById(R.id.mess_recyclerView);
        mess_noResults = view.findViewById(R.id.mess_noResults);

        mess_swipeRefresh.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.CYAN);
        mess_swipeRefresh.setOnRefreshListener(this);


        if (CommonMethods.isInternetWorking(getActivity())) {
            MessageCenterRequest();
        } else {
            mess_swipeRefresh.setRefreshing(true);
            CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);
        }
        activity = getActivity();
        context = getContext();
        return view;
    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment


        if (fragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fillLayout, fragment)
                    .commit();

            searchVisble("Dashboard");

            return true;
        }
        return false;
    }

    @Override
    public void onRefresh() {

        mess_swipeRefresh.setRefreshing(true);

        if (CommonMethods.isInternetWorking(getActivity())) {
            MessageCenterRequest();
        } else {
            mess_swipeRefresh.setRefreshing(true);
            CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);
        }
    }

    private void attachtoAdapter(List<MessageCDataTwo> mlist) {

        madapter = new MessageCenterAdapter(context, activity, mlist);
        mess_recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getContext());
        mess_recyclerView.setLayoutManager(mLinearLayoutManager);
        mess_recyclerView.setAdapter(madapter);

        madapter.setOnItemClickListener(new MessageCenterAdapter.ClickListener() {

            @Override
            public void onItemClick(View v, int position, String Data) {

                try {

                    Fragment loadFragments = new LeadsFragmentSection();
                    Fragment ploadFragments = new ProcurementTAB();
                    Fragment sloadFragments = new StockFragment();
                    Fragment twloadFragments = new SalesStockTAB();

                    switch (Data) {

                        case "Booked Stock in last 2 weeks":

                            loadFragment_M(sloadFragments ,"store");

                            break;
                            case "Used Car Sales Leads Created yesterday":

                            loadFragment_M(loadFragments,"lead");

                            break;
                        case "Procurement Leads Created yesterday":

                            loadFragment_P(ploadFragments);

                            break;

                        case "Used Car Sales Leads without Followup":

                            loadFragment_M(loadFragments,"lead");

                            break;
                        case "Procurement Leads without Followup":

                            loadFragment_P(ploadFragments);

                            break;
                        case "Used Car Sales Leads Followup this week":

                            loadFragment_M(loadFragments,"lead");

                            break;

                        case "Procurement Leads Followup this week":

                            loadFragment_P(ploadFragments);

                            break;

                            case "Total Warranty Sales Within Week":

                                SalesInstance.getInstance().setSalesdirection("SalesWarranty");
                                CommonMethods.setvalueAgainstKey(getActivity(), "SalesWarranty", "SalesWarranty");

                                loadFragment_M(twloadFragments,"warranty");

                            break;

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void MessageCenterRequest() {

        mess_swipeRefresh.setRefreshing(false);
        sendSalesStockDetails(getContext());

    }

    private void sendSalesStockDetails(final Context mContext) {

        mess_noResults.setVisibility(View.GONE);

        String dealer_code = CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code");
        SpinnerManager.showSpinner(mContext);
        MessageCenterServices.getMessageCenters(getActivity(), dealer_code, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                mess_swipeRefresh.setRefreshing(false);
                Response<MessageCenterRes> mRes = (Response<MessageCenterRes>) obj;
                MessageCenterRes mData = mRes.body();

                List<MessageCenterResData> mlist = mData.getDashboardData();


                List<MessageCDataTwo> mlistData = mlist.get(0).getData();

                if (!mlistData.isEmpty()) {

                    mLeadsDatumList = new ArrayList<>();
                    mLeadsDatumList.addAll(mlistData);
                    attachtoAdapter(mLeadsDatumList);
                } else {
                    mess_noResults.setVisibility(View.VISIBLE);
                    mess_swipeRefresh.setVisibility(View.GONE);
                }


            }

            @Override
            public void OnFailure(Throwable t) {
                SpinnerManager.hideSpinner(mContext);
                mess_swipeRefresh.setRefreshing(false);
                t.printStackTrace();
            }
        });

    }

    private void isLeadsAvailable(int TotalCount) {
        if (TotalCount == 0) {
            mess_noResults.setVisibility(View.VISIBLE);
            mess_swipeRefresh.setVisibility(View.GONE);
        } else {
            mess_noResults.setVisibility(View.GONE);
            mess_swipeRefresh.setVisibility(View.VISIBLE);
        }
    }

    private boolean loadFragment_M(Fragment fragment,String status) {


        switch (status){

            case "store":
                navigation.getMenu().findItem(R.id.navigation_stock).setChecked(true);

                break;

                case "warranty":
                    navigation.getMenu().findItem(R.id.navigation_sales).setChecked(true);
                break;
            case "lead":
                try {
                    navigation.getMenu().findItem(R.id.navigation_leads).setChecked(true);
                    searchClose();
                    CommonMethods.setvalueAgainstKey(getActivity(), "leadasm_status", "USEDCARSALES");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
                default:
                    break;
        }


        if (tv_header_title != null) {

            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText(CommonMethods.getstringvaluefromkey(getActivity(), "dealerName"));
        }

        //switching fragment
        if (fragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fillLayout, fragment)
                    .commit();
            return true;
        }

        return false;
    }

    private boolean loadFragment_P(Fragment fragment) {


        try {
            navigation.getMenu().findItem(R.id.navigation_leads).setChecked(true);
            searchClose();
            CommonMethods.setvalueAgainstKey(getActivity(), "leadasm_status", "PROCUREMENT");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (tv_header_title != null) {

            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText(CommonMethods.getstringvaluefromkey(getActivity(), "dealerName"));
        }

        //switching fragment
        if (fragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fillLayout, fragment)
                    .commit();
            return true;
        }

        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        Application.getInstance().trackScreenView(getActivity(), GlobalText.asm_message_center);
    }
}
