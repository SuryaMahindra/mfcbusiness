package com.mfcwl.mfc_dealer.ASMFragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mfcwl.mfc_dealer.ASMAdapter.DasboardAdapter;
import com.mfcwl.mfc_dealer.ASMModel.DashboardEmailReq;
import com.mfcwl.mfc_dealer.ASMModel.DashboardEmailRes;
import com.mfcwl.mfc_dealer.ASMModel.dasboardRes;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.Helper;
import com.mfcwl.mfc_dealer.Utility.PaginationScrollListener;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.retrofitconfig.DasboardStatusServices;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchVal;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchicon;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_DEALER_CRE;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_PROCUREMENT;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_SALES;


@SuppressLint("ValidFragment")
public class DasboardFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    // Object
    RecyclerView recycler_view;
    ProgressBar progressBar;
    List<dasboardRes> liveList;

    SwipeRefreshLayout das_swipeRefresh;
    public static DasboardAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    TextView live_noResults;
    //Pagination
    private int counts = 10;
    private static int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 1;
    private int currentPage = PAGE_START;
    public String TAG=getClass().getSimpleName();
    Activity activity;


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        MenuItem asmHome = menu.findItem(R.id.asmHome);
        MenuItem asm_mail = menu.findItem(R.id.asm_mail);
        MenuItem add_image = menu.findItem(R.id.add_image);
        MenuItem share = menu.findItem(R.id.share);
        MenuItem delete_fea = menu.findItem(R.id.delete_fea);
        MenuItem stock_fil_clear = menu.findItem(R.id.stock_fil_clear);
        MenuItem notification_bell = menu.findItem(R.id.notification_bell);
        MenuItem notification_cancel = menu.findItem(R.id.notification_cancel);

        asmHome.setVisible(false);
        add_image.setVisible(false);
        share.setVisible(false);
        delete_fea.setVisible(false);
        stock_fil_clear.setVisible(false);
        notification_bell.setVisible(false);
        notification_cancel.setVisible(false);

        //new ASM
        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
            asm_mail.setVisible(false);
        } else if(CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_DEALER_CRE) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_SALES) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_PROCUREMENT)){
            asm_mail.setVisible(false);
        } else {
            asm_mail.setVisible(true);
        }

        asm_mail.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                //Toast.makeText(getActivity(),"booked stocks sending email...",Toast.LENGTH_LONG).show();
                if (CommonMethods.isInternetWorking(activity)) {

                    DashboardEmailReq req =new DashboardEmailReq();

                    try {

                        List<String> dealerCode = new ArrayList<String>();
                        dealerCode.clear();
                        dealerCode = Helper.dealerCode;
                        String code ="";

                        if(!dealerCode.isEmpty()) {
                            String[] codeArr = dealerCode.toArray(new String[dealerCode.size()]);
                            code=Arrays.toString(codeArr);

                            code= code.replace("[","");
                            code= code.replace("]","");
                            code= code.replace(" ","");

                            req.setDealerCode(code);
                        }else{
                            req.setDealerCode("Others");
                        }

                        req.setDealerType("RD");

                        req.setFromDate("2018-03-01");
                        req.setToDate("2019-06-28");

                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    // custom dialog
                    Dialog dialog = new Dialog(activity);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setGravity(Gravity.TOP|Gravity.RIGHT);
                    dialog.setContentView(R.layout.common_cus_dialog);
                    TextView Message, Message2;
                    Button cancel, Confirm;
                    Message = dialog.findViewById(R.id.Message);
                    Message2 = dialog.findViewById(R.id.Message2);
                    cancel = dialog.findViewById(R.id.cancel);
                    Confirm = dialog.findViewById(R.id.Confirm);
                    Message.setText(GlobalText.are_you_sure_to_send_mail);
                    //Message2.setText("Go ahead and Submit ?");
                    Confirm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            sendEmailDeatils(activity, req);
                            dialog.dismiss();
                        }
                    });
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();


                } else {
                    CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);

                }
                return true;
            }
        });

    }

    public   void sendEmailDeatils(final Context mContext, final DashboardEmailReq req) {

        SpinnerManager.showSpinner(mContext);

        DasboardStatusServices.sendEmailDashboard(req, mContext, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                SpinnerManager.hideSpinner(mContext);

                try {
                    Response<DashboardEmailRes> mRes = (Response<DashboardEmailRes>) obj;

                    DashboardEmailRes mData = mRes.body();

                    CommonMethods.alertMessage(activity,"Mail sent"+"\n"+mData.getMessage().toString());


                }catch (Exception e){

                    e.printStackTrace();
                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dasboard_, container, false);
        setHasOptionsMenu(true);
        CommonMethods.setvalueAgainstKey(getActivity(), "asm_pages", "dashboard");

        activity = (Activity) getActivity();
       // SpinnerManager.hideSpinner(getActivity());
        live_noResults = view.findViewById(R.id.live_noResults);

        CommonMethods.setvalueAgainstKey(getActivity(),"status","dashboard");
        CommonMethods.setvalueAgainstKey(getActivity(),"Pstatus","dashboard");
        CommonMethods.setvalueAgainstKey(getActivity(),"dealerName","");


        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Kolkata"));
        Date currentLocalTime = cal.getTime();
        DateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        date.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        String localTime = date.format(currentLocalTime);
       // System.out.println(localTime);
        //Log.i(TAG, "onCreateView: "+localTime);


        try {

            searchicon.setVisibility(View.VISIBLE);
            MainActivity.navigation.getMenu().clear();
            MainActivity.navigation.inflateMenu(R.menu.asm_btmnavigation);
            MainActivity.navigation.getMenu().findItem(R.id.navigation_createWR).setCheckable(false);
            MainActivity.navigation.setVisibility(View.VISIBLE);


        }catch (Exception e){
            e.printStackTrace();
        }

        live_noResults.setOnClickListener(v -> {

        });



        InitUI(view);
        return view;
    }

    private void InitUI(View rootview) {


        recycler_view = rootview.findViewById(R.id.recycler_view);
        progressBar = rootview.findViewById(R.id.main_progress);
        live_noResults = rootview.findViewById(R.id.live_noResults);
        das_swipeRefresh = rootview.findViewById(R.id.Live_swipeRefresh);
        das_swipeRefresh.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.CYAN);
        das_swipeRefresh.setOnRefreshListener(this);

        liveList = new ArrayList<>();


        // SetAdapter();
        // if(adapter==null) {
        adapter = new DasboardAdapter(getActivity());
        // }
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.setAdapter(adapter);


        recycler_view.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {

               // dasboardReq();

                if (TOTAL_PAGES != 1) {
                    isLoading = true;
                    currentPage += 1;

                }

            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        if (CommonMethods.isInternetWorking(getActivity())) {
            dasboardReq();
        } else {
            CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);
        }

    }

    public static void onSerarchResultUpdate(String query) {
        searchVal = query;

        if (adapter == null) {

        } else {
            adapter.filter(query);
        }
        //System.out.println("Search Values>>>>>>>>>> "+query);

    }


    private void dasboardReq() {

        SpinnerManager.showSpinner(getActivity());

        DasboardStatusServices.getLeadStatus(getActivity(),new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(getActivity());
                Response<List<dasboardRes>> mRes = (Response<List<dasboardRes>>) obj;
                List<dasboardRes> mData = mRes.body();

                try {
                    String str = new Gson().toJson(mData, new TypeToken<List<dasboardRes>>() {
                    }.getType());
                    Log.i(TAG, "OnSuccess: " + str);
                } catch (Exception e) {
                    Log.e(TAG, "Excetion : " + e.getMessage());
                }

                liveList.clear();
                adapter.clear();
                liveList =mData;

                Helper.dealerName.clear();
                Helper.dealerCode.clear();

                if (!liveList.isEmpty()) {
                    if (live_noResults.getVisibility() == View.VISIBLE) {
                        live_noResults.setVisibility(View.GONE);
                    }
                    das_swipeRefresh.setRefreshing(false);

                    adapter.addAll(liveList);

                    for(int i=0; i<=liveList.size()-1;i++){
                        Helper.dealerName.add(i,liveList.get(i).getDealerName());
                        Helper.dealerCode.add(i,liveList.get(i).getDealerCode());
                    }

                } else {

                    if (live_noResults.getVisibility() == View.GONE) {
                        live_noResults.setVisibility(View.VISIBLE);
                    }
                    das_swipeRefresh.setRefreshing(false);
                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {

                try{
                    Helper.dealerName.clear();
                    Helper.dealerCode.clear();
                }catch (Exception e){
                    e.printStackTrace();
                }

                das_swipeRefresh.setRefreshing(false);
                if (live_noResults.getVisibility() == View.GONE) {
                    live_noResults.setVisibility(View.VISIBLE);
                }
                SpinnerManager.hideSpinner(getActivity());
            }


        });
    }

    @Override
    public void onRefresh() {

        counts = 10;
        currentPage = PAGE_START;
        isLastPage = false;
        TOTAL_PAGES = 1;
        dasboardReq();
    }

    @Override
    public void onResume() {
        super.onResume();

        Application.getInstance().trackScreenView(getActivity(),GlobalText.asm_dashboard);
       // MyApplication.getInstance().trackScreenView(GlobalText.asm_dashboard);
    }


}
