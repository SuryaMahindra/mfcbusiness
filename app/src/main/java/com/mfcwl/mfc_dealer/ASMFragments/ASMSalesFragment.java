package com.mfcwl.mfc_dealer.ASMFragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.mfcwl.mfc_dealer.ASMSalesServices.ASMSalesService;
import com.mfcwl.mfc_dealer.ASMSalesServices.TotalSalesCountRes;
import com.mfcwl.mfc_dealer.ASMSalesServices.WarrantySalesCountReq;
import com.mfcwl.mfc_dealer.ASMSalesServices.WarrantySalesCountRes;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.SalesStocks.Activity.SaleStockFilterBy;
import com.mfcwl.mfc_dealer.SalesStocks.Fragment.SalesInstance;
import com.mfcwl.mfc_dealer.SalesStocks.Fragment.SalesStockTAB;
import com.mfcwl.mfc_dealer.SalesStocks.Instances.SSConstantsSection;
import com.mfcwl.mfc_dealer.SalesStocks.Instances.SSFilterSaveInstance;
import com.mfcwl.mfc_dealer.StateHead.StateHeadDashBoardFragment;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.ZonalHead.ZonalHeadDashBoardFragment;

import org.json.JSONArray;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.activity;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchVisble;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.tv_header_title;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ASM;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_STATEHEAD;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ZONALHEAD;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_SALES;

public class ASMSalesFragment extends Fragment {

    private final Calendar calendar = Calendar.getInstance();
    private final Calendar calendar_end = Calendar.getInstance();

    private final String dateFormat = "yyyy-MM-dd";
    private LinearLayout mStart, mEnd;
    private TextView mTxtStart, mTxtEnd;
    private boolean isStart;
    private final SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);
    private Date date;

    private TextView mTotalSalesCountlabel, mwarranty_title;
    private View view;
    private int sumofwarranty = 0;
    private WarrantySalesCountReq mReq;

    private int totalsalescount = 0;

    private static JSONArray leadstatusJsonArray, leadstatusJsonTotalArray;

    private HashMap<String, String> postedfollDateHashMap;


    public static MenuItem items;
    public static Menu menus;

    /* @Override
     public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)


     {
         menu.clear();
         inflater.inflate(R.menu.menu_item, menu);
         this.menus=menu;
         menu.findItem(R.id.asmHome).setVisible(true);
         menu.findItem(R.id.add_image).setVisible(false);
         menu.findItem(R.id.share).setVisible(false);
         menu.findItem(R.id.delete_fea).setVisible(false);
         menu.findItem(R.id.stock_fil_clear).setVisible(false);
         menu.findItem(R.id.notification_bell).setVisible(false);
         menu.findItem(R.id.notification_cancel).setVisible(false);
         menu.findItem(R.id.asm_mail).setVisible(false);

         super.onCreateOptionsMenu(menu,inflater);
     }

     @Override
     public boolean onOptionsItemSelected(MenuItem item) {

         items=item;
         switch (item.getItemId()) {

             case R.id.asmHome:

                 Fragment loadFragments = new DasboardFragment();
                 loadFragmentH(loadFragments);

                 return true;

         }
         return super.onOptionsItemSelected(item);
     }*/
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        //menu.clear();

        MenuItem asmHome = menu.findItem(R.id.asmHome);

        asmHome.setVisible(true);
        asmHome.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (CommonMethods.getstringvaluefromkey(getActivity(), "user_type")) {
                    case USERTYPE_ZONALHEAD: {
                        Fragment loadFragments = new ZonalHeadDashBoardFragment();
                        loadFragmentH(loadFragments);
                    }
                    break;

                    case USERTYPE_STATEHEAD: {
                        Fragment loadFragments = new StateHeadDashBoardFragment();
                        loadFragmentH(loadFragments);
                    }
                    break;

                    case USERTYPE_ASM:
                    default: {
                        Fragment loadFragments = new DasboardFragment();
                        loadFragmentH(loadFragments);
                    }
                    break;
                }

                return true;
            }
        });


        MenuItem asm_mail = menu.findItem(R.id.asm_mail);
        MenuItem add_image = menu.findItem(R.id.add_image);
        MenuItem share = menu.findItem(R.id.share);
        MenuItem delete_fea = menu.findItem(R.id.delete_fea);
        MenuItem stock_fil_clear = menu.findItem(R.id.stock_fil_clear);
        MenuItem notification_bell = menu.findItem(R.id.notification_bell);
        MenuItem notification_cancel = menu.findItem(R.id.notification_cancel);

        asm_mail.setVisible(false);
        add_image.setVisible(false);
        share.setVisible(false);
        delete_fea.setVisible(false);
        stock_fil_clear.setVisible(false);
        notification_bell.setVisible(false);
        notification_cancel.setVisible(false);

        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_SALES)) {
            asmHome.setVisible(false);
        }
    }

    String TAG = getClass().getSimpleName();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        view = inflater.inflate(R.layout.fragment_asmsales, container, false);

        setHasOptionsMenu(true);


        CommonMethods.setvalueAgainstKey(getActivity(), "asm_pages", "sales_dashboard");

        mStart = (LinearLayout) view.findViewById(R.id.llstart);
        mEnd = (LinearLayout) view.findViewById(R.id.llend);
        mTxtStart = (TextView) view.findViewById(R.id.txtstart);
        mTxtEnd = (TextView) view.findViewById(R.id.txtend);
        mTotalSalesCountlabel = (TextView) view.findViewById(R.id.totalsalescount);
        mwarranty_title = (TextView) view.findViewById(R.id.warranty_title);
        date = new Date();

        leadstatusJsonArray = new JSONArray();
        leadstatusJsonTotalArray = new JSONArray();
        postedfollDateHashMap = new HashMap<String, String>();

        startDateSet();
        endDateSet();
        clearDB();

        mTotalSalesCountlabel.setOnClickListener(v -> {

            postedfollDateHashMap.put(SSConstantsSection.LEAD_FOLUP_CUSTDATE, mTxtStart.getText().toString() + "@" + mTxtEnd.getText().toString());
            SSFilterSaveInstance.getInstance().setSavedatahashmap(postedfollDateHashMap);

            if(totalsalescount == 0){
                return;
            }

            SalesInstance.getInstance().setSalesdirection("");
            SalesStockTAB leadsFragment = new SalesStockTAB();
            loadFragment(leadsFragment);
        });

        mwarranty_title.setOnClickListener(v -> {


            SSFilterSaveInstance.getInstance().setStatusarray(leadstatusJsonTotalArray);

            postedfollDateHashMap.put(SSConstantsSection.LEAD_FOLUP_CUSTDATE, mTxtStart.getText().toString() + "@" + mTxtEnd.getText().toString());
            SSFilterSaveInstance.getInstance().setSavedatahashmap(postedfollDateHashMap);

            if(sumofwarranty == 0){
                return;
            }
            SalesInstance.getInstance().setSalesdirection("SalesWarranty");
            SalesStockTAB leadsFragment = new SalesStockTAB();
            loadFragment(leadsFragment);
        });

        mStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDatePickerDialog(getActivity()).show();
                isStart = true;
            }
        });

        mEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDatePickerDialog_End(getActivity()).show();
                isStart = false;


            }
        });

        mReq = new WarrantySalesCountReq();
        mReq.setDealercode(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"));
        mReq.setFromdate(mTxtStart.getText().toString());
        mReq.setTodate(mTxtEnd.getText().toString());

       /* mReq.setDealercode("242");
        mReq.setFromdate("2019-03-01");
        mReq.setTodate("2019-06-28");*/


        if (CommonMethods.isInternetWorking(getActivity())) {
            getWarrantyCount(getActivity(), mReq);
            getTotalCount(getActivity(), mReq);
        } else {
            CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);
        }


        return view;
    }


    private void startDateSet() {


        try {

            if (CommonMethods.getstringvaluefromkey(getActivity(), "salesStartDate").equalsIgnoreCase("")) {
                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);

                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, 1);

                mTxtStart.setText(sdf.format(calendar.getTime()));
            } else {

                String[] splitDateTime = CommonMethods.getstringvaluefromkey(getActivity(), "salesStartDate").split(" ", 10);
                String[] splitDate = splitDateTime[0].split("-", 10);

                int year = Integer.parseInt(splitDate[0]);
                int month = Integer.parseInt(splitDate[1]);
                int day = Integer.parseInt(splitDate[2]);

                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month - 1);
                calendar.set(Calendar.DAY_OF_MONTH, day);

                mTxtStart.setText(sdf.format(calendar.getTime()));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void endDateSet() {

        try {

            if (CommonMethods.getstringvaluefromkey(getActivity(), "salesEndDate").equalsIgnoreCase("")) {
                mTxtEnd.setText(sdf.format(date));
            } else {

                String[] splitDateTime = CommonMethods.getstringvaluefromkey(getActivity(), "salesEndDate").split(" ", 10);
                String[] splitDate = splitDateTime[0].split("-", 10);

                int year = Integer.parseInt(splitDate[0]);
                int month = Integer.parseInt(splitDate[1]);
                int day = Integer.parseInt(splitDate[2]);

                calendar_end.set(Calendar.YEAR, year);
                calendar_end.set(Calendar.MONTH, month - 1);
                calendar_end.set(Calendar.DAY_OF_MONTH, day);

                mTxtEnd.setText(sdf.format(calendar_end.getTime()));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void clearDB() {

        CommonMethods.setvalueAgainstKey(getActivity(), "salesmfc", "");
        CommonMethods.setvalueAgainstKey(getActivity(), "sales_certified", "");
        CommonMethods.setvalueAgainstKey(getActivity(), "sales_startpricedata", "");
        CommonMethods.setvalueAgainstKey(getActivity(), "sales_endpricedata", "");

        CommonMethods.setvalueAgainstKey(getActivity(), "column2", "");
        CommonMethods.setvalueAgainstKey(getActivity(), "operator2", "");
        CommonMethods.setvalueAgainstKey(getActivity(), "value2", "");
        CommonMethods.setvalueAgainstKey(getActivity(), "leads_status", "");


        SSFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());
        SSFilterSaveInstance.getInstance().setStatusarray(new JSONArray());

        //price vales send
        SaleStockFilterBy.startPriceValues = "0";
        SaleStockFilterBy.endPriceValues = "10000000";


    }


    /**
     * Return a new date picker listener tied to the specified TextView field
     *
     * @return
     */
    private DatePickerDialog.OnDateSetListener getOnDateSetListener() {
        return new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                mTxtStart.setText(sdf.format(calendar.getTime()));

            }
        };
    }

    private DatePickerDialog.OnDateSetListener getOnDateSetListener_End() {
        return new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                calendar_end.set(Calendar.YEAR, year);
                calendar_end.set(Calendar.MONTH, monthOfYear);
                calendar_end.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                mTxtEnd.setText(sdf.format(calendar_end.getTime()));

                mReq = new WarrantySalesCountReq();
                mReq.setDealercode(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"));
                mReq.setFromdate(mTxtStart.getText().toString());
                mReq.setTodate(mTxtEnd.getText().toString());

                CommonMethods.setvalueAgainstKey(getActivity(), "salesStartDate", mTxtStart.getText().toString());
                CommonMethods.setvalueAgainstKey(getActivity(), "salesEndDate", mTxtEnd.getText().toString());


                getWarrantyCount(getActivity(), mReq);
                getTotalCount(getActivity(), mReq);

            }
        };
    }


    /**
     * Return new DatePickerDialog for field
     *
     * @param context
     * @return
     */
    private DatePickerDialog getDatePickerDialog(Context context) {
        return new DatePickerDialog(context, getOnDateSetListener(), calendar
                .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
    }

    private DatePickerDialog getDatePickerDialog_End(Context context) {
        return new DatePickerDialog(context, getOnDateSetListener_End(), calendar_end
                .get(Calendar.YEAR), calendar_end.get(Calendar.MONTH),
                calendar_end.get(Calendar.DAY_OF_MONTH));
    }


    private void getWarrantyCount(Context mContext, WarrantySalesCountReq mReq) {
        SpinnerManager.showSpinner(mContext);
        ASMSalesService.fetchWarrantySalesCount(mReq, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);

                sumofwarranty = 0;
                leadstatusJsonTotalArray = new JSONArray();
                postedfollDateHashMap = new HashMap<String, String>();

                try {
                    Response<List<WarrantySalesCountRes>> mRes = (Response<List<WarrantySalesCountRes>>) obj;
                    List<WarrantySalesCountRes> mData = mRes.body();

                    LinearLayout svi = (LinearLayout) view.findViewById(R.id.warranty_data);
                    svi.removeAllViews();
                    LayoutInflater layoutInflater = getLayoutInflater();

                    if (mData.size() > 0) {

                        for (int i = 0; i < mData.size(); i++) {
                            View rowView = layoutInflater.inflate(R.layout.row_list, null, false);

                            // Left
                            LinearLayout linearLayoutLeft = (LinearLayout) rowView.findViewById(R.id.row_left_element);
                            TextView count = (TextView) linearLayoutLeft.findViewById(R.id.count);
                            TextView label = (TextView) linearLayoutLeft.findViewById(R.id.label);
                            TextView amount = (TextView) linearLayoutLeft.findViewById(R.id.amount);
                            count.setText(mData.get(i).getTotal().toString());
                            label.setText(mData.get(i).getWarrantyType().toString());

                            amount.setText("\u20B9" + mData.get(i).getAmount().toString());

                            svi.addView(rowView);

                            //sumofwarranty =mData.get(i).getTotal();

                            leadstatusJsonTotalArray.put((mData.get(i).getWarrantyType()));

                            sumofwarranty = sumofwarranty + mData.get(i).getTotal();


                            int finalIndex = i;

                            linearLayoutLeft.setOnClickListener(v -> {

                                try {

                                    leadstatusJsonArray.put(mData.get(finalIndex).getWarrantyType());


                                    postedfollDateHashMap.put(SSConstantsSection.LEAD_FOLUP_CUSTDATE, mTxtStart.getText().toString() + "@" + mTxtEnd.getText().toString());
                                    SSFilterSaveInstance.getInstance().setSavedatahashmap(postedfollDateHashMap);

                                    SSFilterSaveInstance.getInstance().setStatusarray(leadstatusJsonArray);

                                    SalesInstance.getInstance().setSalesdirection("SalesWarranty");
                                    CommonMethods.setvalueAgainstKey(getActivity(), "SalesWarranty", "SalesWarranty");


                                    SalesStockTAB leadsFragment = new SalesStockTAB();
                                    loadFragment(leadsFragment);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                            });

                        }
                        svi.refreshDrawableState();
                        mwarranty_title.setText("" + sumofwarranty + " Warranty Sales");

                    } else {
                        // Toast.makeText(mContext,"No data found",Toast.LENGTH_LONG).show();
                        mwarranty_title.setText("" + sumofwarranty + " Warranty Sales");

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);

                Toast.makeText(mContext, "No data found", Toast.LENGTH_LONG).show();

            }
        });

    }

    private void getTotalCount(Context mContext, WarrantySalesCountReq mReq) {

        ASMSalesService.fetchTotalSalesCount(mReq, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                Response<TotalSalesCountRes> mRes = (Response<TotalSalesCountRes>) obj;
                TotalSalesCountRes data = mRes.body();
                totalsalescount = data.getTotalCount();
                mTotalSalesCountlabel.setText("" + data.getTotalCount() + " Total sales");

            }

            @Override
            public void OnFailure(Throwable mThrowable) {

            }
        });

    }

    private boolean loadFragmentH(Fragment fragment) {
        //switching fragment

        searchVisble("Dashboard");


        if (fragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fillLayout, fragment)
                    .commit();

            return true;
        }
        return false;
    }


    private boolean loadFragment(Fragment fragment) {
        //switching fragment


        if (fragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fillLayout, fragment)
                    .commit();
            return true;
        }

        return false;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (tv_header_title != null) {
            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText(CommonMethods.getstringvaluefromkey(getActivity(), "dealerName"));
        }

        Application.getInstance().trackScreenView(getActivity(), GlobalText.asm_sales_dashboard);
    }
}
