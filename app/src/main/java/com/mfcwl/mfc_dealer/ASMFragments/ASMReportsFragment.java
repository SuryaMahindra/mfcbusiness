package com.mfcwl.mfc_dealer.ASMFragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.ASMModel.leadReportRes;
import com.mfcwl.mfc_dealer.ASMReportsServices.ASMReportService;
import com.mfcwl.mfc_dealer.ASMReportsServices.ASMReportsReq;
import com.mfcwl.mfc_dealer.ASMReportsServices.DealerInfoRes;
import com.mfcwl.mfc_dealer.ASMReportsServices.PrivateLeadRes;
import com.mfcwl.mfc_dealer.ASMReportsServices.SalesRes;
import com.mfcwl.mfc_dealer.ASMReportsServices.StockInfoRes;
import com.mfcwl.mfc_dealer.ASMReportsServices.WarrantyInformation;
import com.mfcwl.mfc_dealer.ASMReportsServices.WarrantyStockRes;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.StateHead.StateHeadDashBoardFragment;
import com.mfcwl.mfc_dealer.StockModels.leadsDasRq;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.ZonalHead.ZonalHeadDashBoardFragment;
import com.mfcwl.mfc_dealer.retrofitconfig.LeadReportServices;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchVisble;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.tv_header_title;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ASM;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_STATEHEAD;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ZONALHEAD;

public class ASMReportsFragment extends Fragment {
    String TAG = getClass().getSimpleName();
    private final Calendar calendar = Calendar.getInstance();
    private final Calendar calendar_end = Calendar.getInstance();
    private final String dateFormat = "yyyy-MM-dd";
    private LinearLayout mStart, mEnd;
    private TextView mTxtStart, mTxtEnd;
    private boolean isStart;
    private final SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);
    private Date date;
    private View view;
    private TextView mDealerName, mLocation, mState, mAreaManager, mStateHead;

    private TextView totalstorestocks, photographs, certifiedstocks, grt90, days6090, days3060, less30;

    private TextView pl,wl,owl,followup1,followup2,followupmore;

    private TextView booked, sold;

    private TextView warrantybal,warrantynos,warrantyrevenue;

    public static MenuItem items;
    public static Menu menus;


    ASMReportsReq mReq;

    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)


    {
        menu.clear();
        inflater.inflate(R.menu.menu_item, menu);
        this.menus=menu;
        menu.findItem(R.id.asmHome).setVisible(true);
        menu.findItem(R.id.add_image).setVisible(false);
        menu.findItem(R.id.share).setVisible(false);
        menu.findItem(R.id.delete_fea).setVisible(false);
        menu.findItem(R.id.stock_fil_clear).setVisible(false);
        menu.findItem(R.id.notification_bell).setVisible(false);
        menu.findItem(R.id.notification_cancel).setVisible(false);
        menu.findItem(R.id.asm_mail).setVisible(false);

        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        items=item;
        switch (item.getItemId()) {

            case R.id.asmHome:

                Fragment loadFragments = new DasboardFragment();
                loadFragmentH(loadFragments);

                return true;

        }
        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        //menu.clear();

        MenuItem asmHome = menu.findItem(R.id.asmHome);

        asmHome.setVisible(true);
        asmHome.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (CommonMethods.getstringvaluefromkey(getActivity(), "user_type")) {
                    case USERTYPE_ZONALHEAD: {
                        Fragment loadFragments = new ZonalHeadDashBoardFragment();
                        loadFragmentH(loadFragments);
                    }
                    break;

                    case USERTYPE_STATEHEAD: {
                        Fragment loadFragments = new StateHeadDashBoardFragment();
                        loadFragmentH(loadFragments);
                    }
                    break;

                    case USERTYPE_ASM:
                    default: {
                        Fragment loadFragments = new DasboardFragment();
                        loadFragmentH(loadFragments);
                    }
                    break;
                }
                return true;
            }
        });


        MenuItem asm_mail = menu.findItem(R.id.asm_mail);
        MenuItem add_image = menu.findItem(R.id.add_image);
        MenuItem share = menu.findItem(R.id.share);
        MenuItem delete_fea = menu.findItem(R.id.delete_fea);
        MenuItem stock_fil_clear = menu.findItem(R.id.stock_fil_clear);
        MenuItem notification_bell = menu.findItem(R.id.notification_bell);
        MenuItem notification_cancel = menu.findItem(R.id.notification_cancel);

        asm_mail.setVisible(false);
        add_image.setVisible(false);
        share.setVisible(false);
        delete_fea.setVisible(false);
        stock_fil_clear.setVisible(false);
        notification_bell.setVisible(false);
        notification_cancel.setVisible(false);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        view = inflater.inflate(R.layout.fragment_asmreports, container, false);

        setHasOptionsMenu(true);

        CommonMethods.setvalueAgainstKey(getActivity(), "asm_pages", "report_page");

        mStart = (LinearLayout) view.findViewById(R.id.llstart);
        mEnd = (LinearLayout) view.findViewById(R.id.llend);
        mTxtStart = (TextView) view.findViewById(R.id.txtstart);
        mTxtEnd = (TextView) view.findViewById(R.id.txtend);

        mDealerName = (TextView) view.findViewById(R.id.dealername);
        mLocation = (TextView) view.findViewById(R.id.location);
        mState = (TextView) view.findViewById(R.id.state);
        mAreaManager = (TextView) view.findViewById(R.id.areamanager);
        mStateHead = (TextView) view.findViewById(R.id.statehead);

        totalstorestocks = (TextView) view.findViewById(R.id.totalstorestocks);
        photographs = (TextView) view.findViewById(R.id.photographs);
        certifiedstocks = (TextView) view.findViewById(R.id.certifiedstocks);
        grt90 = (TextView) view.findViewById(R.id.grt90);
        days6090 = (TextView) view.findViewById(R.id.days6090);
        days3060 = (TextView) view.findViewById(R.id.days3060);
        less30 = (TextView) view.findViewById(R.id.less30);

        pl = (TextView) view.findViewById(R.id.pl);
        wl = (TextView) view.findViewById(R.id.wl);
        owl = (TextView) view.findViewById(R.id.owl);
        followup1 = (TextView) view.findViewById(R.id.follwups1);
        followup2= (TextView) view.findViewById(R.id.follwups2);
        followupmore= (TextView) view.findViewById(R.id.follwupsmore);
        booked = (TextView) view.findViewById(R.id.booked);
        sold = (TextView) view.findViewById(R.id.sold);
        warrantybal = (TextView) view.findViewById(R.id.warratybal);
        warrantynos = (TextView) view.findViewById(R.id.WarrantyNos);
        warrantyrevenue = (TextView) view.findViewById(R.id.WarratyRevenue);

        startDateSet();
        endDateSet();

        mStart.setOnClickListener(v -> {
            getDatePickerDialog(getActivity()).show();
            isStart = true;
        });

        mEnd.setOnClickListener(v -> {
            getDatePickerDialog_End(getActivity()).show();
            isStart = false;
        });



        reportReques();

        return view;
    }

    private void reportReques() {

        leadsDasRq req = new leadsDasRq();
        req.setAccessToken(CommonMethods.getstringvaluefromkey(getActivity(), "access_token"));
        req.setDealerCode(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"));
        req.setFromDate(mTxtStart.getText().toString());
        req.setToDate(mTxtEnd.getText().toString());
        req.setDealerType("USEDCARSALES");
        req.setTag("web");

        leadReportservice(getActivity(), req);


        mReq = new ASMReportsReq();
        mReq.setDealer_code(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"));
        mReq.setDealer_type(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_types"));
        mReq.setFrom_date(mTxtStart.getText().toString());
        mReq.setTo_date(mTxtEnd.getText().toString());


        /*if (CommonMethods.isInternetWorking(getActivity())) {

            getDelearInfo(getActivity(), mReq);
            getStocksInfo(getActivity(), mReq);
            getSalesInfo(getActivity(), mReq);
            getWarrantyInfo(getActivity(), mReq);
            getprivateInfo(getActivity(), mReq);

        } else {
            CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);
        }*/

    }

    private void leadReportservice(final Context mContext, final leadsDasRq leadsDasRq) {
        SpinnerManager.showSpinner(mContext);
        LeadReportServices.leaddashservice(mContext, leadsDasRq, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
               // SpinnerManager.hideSpinner(mContext);
                Response<leadReportRes> mRes = (Response<leadReportRes>) obj;
                leadReportRes mData = mRes.body();

                String msg =mData.getMessage().toString();

                if(mData.getStatus().equalsIgnoreCase("success")) {

                    wl.setText("" + mData.getWebLeadsForTheMonth());
                    owl.setText("" + mData.getOpenWebLeads());
                    followup1.setText("" + mData.getNoOfFollowUpsDoneOnce());
                    followup2.setText("" + mData.getNoOfFollowUpsDoneTwice());
                    followupmore.setText("" + mData.getNoOfFollowUpsDoneMoreThan2times());

                    if (CommonMethods.isInternetWorking(getActivity())) {

                        getDelearInfo(getActivity(), mReq);

                    } else {
                        CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);
                    }

                }else{

                    if(mData.getStatusCode().equals(401)){

                        WebServicesCall.error_popup_retrofit(401, msg, mContext);

                    }else{
                        CommonMethods.alertMessage((Activity) mContext,msg);
                    }

                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                try {
                    SpinnerManager.hideSpinner(mContext);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //  Toast.makeText(mContext, mThrowable.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Return a new date picker listener tied to the specified TextView field
     *
     * @return
     */
    private DatePickerDialog.OnDateSetListener getOnDateSetListener() {
        return new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
/*
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);
                if (isStart) {*/
                    mTxtStart.setText(sdf.format(calendar.getTime()));
               /* } else {
                    mTxtEnd.setText(sdf.format(calendar.getTime()));

                    reportReques();

                }*/
            }
        };
    }

    private DatePickerDialog.OnDateSetListener getOnDateSetListener_End() {
        return new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                calendar_end.set(Calendar.YEAR, year);
                calendar_end.set(Calendar.MONTH, monthOfYear);
                calendar_end.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                mTxtEnd.setText(sdf.format(calendar_end.getTime()));

                CommonMethods.setvalueAgainstKey(getActivity(), "reportStartDate", mTxtStart.getText().toString());
                CommonMethods.setvalueAgainstKey(getActivity(), "reportEndDate", mTxtEnd.getText().toString());

                reportReques();
            }
        };
    }


    /**
     * Return new DatePickerDialog for field
     *
     * @param context
     * @return
     */
    private DatePickerDialog getDatePickerDialog(Context context) {
        return new DatePickerDialog(context, getOnDateSetListener(), calendar
                .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
    }
    private DatePickerDialog getDatePickerDialog_End(Context context) {
        return new DatePickerDialog(context, getOnDateSetListener_End(), calendar_end
                .get(Calendar.YEAR), calendar_end.get(Calendar.MONTH),
                calendar_end.get(Calendar.DAY_OF_MONTH));
    }

    public Date addOneMonth() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 1);
        return cal.getTime();
    }


    private void getDelearInfo(Context mContext, ASMReportsReq mReq) {
       // SpinnerManager.showSpinner(mContext);
        ASMReportService.fetchDealerInfo(mReq, new HttpCallResponse() {

            @Override
            public void OnSuccess(Object obj) {
              //  SpinnerManager.hideSpinner(mContext);
                Response<DealerInfoRes> res = (Response<DealerInfoRes>) obj;

                DealerInfoRes data = res.body();
                mDealerName.setText(data.getDealerName());

                if (data.getLocation() != null) {
                    mLocation.setText(data.getLocation());
                } else {
                    mLocation.setText("NA");
                }
                if (data.getState() != null) {
                    mState.setText(data.getState());
                } else {
                    mState.setText("NA");
                }
                if (data.getAreaManager() != null) {
                    mAreaManager.setText(data.getAreaManager());
                } else {
                    mAreaManager.setText("NA");
                }
                if (data.getStateHead() != null) {
                    mStateHead.setText(data.getStateHead().toString());

                } else {
                    mStateHead.setText("NA");
                }

                if (CommonMethods.isInternetWorking(getActivity())) {

                   getStocksInfo(getActivity(), mReq);

                } else {
                    CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);
                }
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);

            }
        });

    }

    private void getStocksInfo(Context mContext, ASMReportsReq mReq) {

      //  SpinnerManager.showSpinner(mContext);
        ASMReportService.fetchStockInfo(mReq, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
              //  SpinnerManager.hideSpinner(mContext);

                Response<StockInfoRes> res = (Response<StockInfoRes>) obj;
                StockInfoRes data = res.body();
                totalstorestocks.setText("" + data.getStoreStockCount());
                photographs.setText("" + data.getStockWithoutImageCount());
                certifiedstocks.setText("" + data.getCertifiedStockCount());
                grt90.setText("" + data.getStockGreaterthan90daysCount());
                days6090.setText("" + data.getStock60daysto90daysOldCount());
                days3060.setText("" + data.getStock30daysto60daysOldCount());
                less30.setText("" + data.getStockLesstahn30daysOldCount());

                if (CommonMethods.isInternetWorking(getActivity())) {

                    getSalesInfo(getActivity(), mReq);
                    getWarrantyInfo(getActivity(), mReq);


                } else {
                    CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);
                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);

            }
        });

    }


    private void getSalesInfo(Context mContext, ASMReportsReq mReq) {

        SpinnerManager.showSpinner(mContext);
        ASMReportService.fetchSalesInfo(mReq, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);

                Response<SalesRes> res = (Response<SalesRes>) obj;
                SalesRes data = res.body();

                if(data.getBooked()!=null) {
                    booked.setText(""+data.getBooked());
                }else{
                    booked.setText( "0");
                }

                if(data.getSold()!=null) {
                    sold.setText(""+data.getSold());
                }else{
                    sold.setText("0");
                }

               /* booked.setText(""+data.getBooked());
                sold.setText(""+data.getSold());*/

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);

            }
        });

    }

    private void getWarrantyInfo(Context mContext, ASMReportsReq mReq) {

      //  SpinnerManager.showSpinner(mContext);
        ASMReportService.fetchWarrantyInfo(mReq, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
              //  SpinnerManager.hideSpinner(mContext);

                Response<WarrantyStockRes> res = (Response<WarrantyStockRes>) obj;
                WarrantyStockRes data = res.body();

                if(data.getWarrantyBalance()!=null) {
                    warrantybal.setText(""+data.getWarrantyBalance());
                }else{
                    warrantybal.setText( "0");
                }

                if(data.getWarrantyNumber()!=null) {
                    warrantynos.setText(""+ data.getWarrantyNumber());
                }else{
                    warrantynos.setText("" + "0");
                }
                if(data.getWarrantyRevenue()!=null) {
                    warrantyrevenue.setText(""+data.getWarrantyRevenue());
                }else{
                    warrantyrevenue.setText("0");
                }


                List<WarrantyInformation> mData =data.getWarrantyInformation();

                LinearLayout svi = (LinearLayout) view.findViewById(R.id.warranty_data);
                svi.removeAllViews();
                LayoutInflater layoutInflater = getLayoutInflater();

                for(int i =0;i< mData.size();i++){
                    View rowView = layoutInflater.inflate(R.layout.row_reports, null, false);

                    // Left
                    LinearLayout linearLayoutLeft = (LinearLayout) rowView.findViewById(R.id.row_left_element);
                    TextView count = (TextView) linearLayoutLeft.findViewById(R.id.count);
                    TextView label = (TextView) linearLayoutLeft.findViewById(R.id.label);
                    TextView amount = (TextView) linearLayoutLeft.findViewById(R.id.amount);
                    count.setText(mData.get(i).getWarrantyType());

                    label.setText(""+mData.get(i).getWarrantyNumber());

                    if(mData.get(i).getRevenue()!=null) {
                        amount.setText("\u20B9"+mData.get(i).getRevenue().toString());
                    }else{
                        amount.setText("\u20B9"+"0");
                    }

                    svi.addView(rowView);
                }

                svi.refreshDrawableState();


                if (CommonMethods.isInternetWorking(getActivity())) {

                      getprivateInfo(getActivity(), mReq);

                } else {
                    CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);
                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);

            }
        });

    }



    private void getprivateInfo(Context mContext, ASMReportsReq mReq) {

        //SpinnerManager.showSpinner(mContext);
        ASMReportService.fetchprivateleadcountInfo(mReq, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);

                Response<PrivateLeadRes> res = (Response<PrivateLeadRes>) obj;
                PrivateLeadRes data = res.body();
                pl.setText(""+ data.getPrivateLead());

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);

            }
        });

    }


    private boolean loadFragmentH(Fragment fragment) {
        //switching fragment

        searchVisble("Dashboard");

        if (fragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fillLayout, fragment)
                    .commit();

            return true;
        }
        return false;
    }


    private void startDateSet() {

        try {

            if (CommonMethods.getstringvaluefromkey(getActivity(), "reportStartDate").equalsIgnoreCase("")) {
                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);

                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, 1);

                mTxtStart.setText(sdf.format(calendar.getTime()));
            } else {

                String[] splitDateTime = CommonMethods.getstringvaluefromkey(getActivity(), "reportStartDate").split(" ", 10);
                String[] splitDate = splitDateTime[0].split("-", 10);

                int year = Integer.parseInt(splitDate[0]);
                int month = Integer.parseInt(splitDate[1]);
                int day = Integer.parseInt(splitDate[2]);

                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month-1);
                calendar.set(Calendar.DAY_OF_MONTH, day);

                mTxtStart.setText(sdf.format(calendar.getTime()));

            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void endDateSet() {

        try {

            if (CommonMethods.getstringvaluefromkey(getActivity(), "salesEndDate").equalsIgnoreCase("")) {
                mTxtEnd.setText(sdf.format(calendar_end.getTime()));
            } else {

                String[] splitDateTime = CommonMethods.getstringvaluefromkey(getActivity(), "salesEndDate").split(" ", 10);
                String[] splitDate = splitDateTime[0].split("-", 10);

                int year = Integer.parseInt(splitDate[0]);
                int month = Integer.parseInt(splitDate[1]);
                int day = Integer.parseInt(splitDate[2]);

                calendar_end.set(Calendar.YEAR, year);
                calendar_end.set(Calendar.MONTH, month-1);
                calendar_end.set(Calendar.DAY_OF_MONTH, day);

                mTxtEnd.setText(sdf.format(calendar_end.getTime()));

            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }


    @Override
    public void onResume() {
        super.onResume();

        if (tv_header_title != null) {
            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText(CommonMethods.getstringvaluefromkey(getActivity(), "dealerName"));
        }

        Application.getInstance().trackScreenView(getActivity(),GlobalText.asm_report_dashboard);
    }
}
