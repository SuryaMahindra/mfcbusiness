package com.mfcwl.mfc_dealer.ASMFragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import im.delight.android.webview.AdvancedWebView;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchicon;

public class PowerBIReports extends Fragment implements AdvancedWebView.Listener {

    private AdvancedWebView mWebView;

    private String POWERBI_URL = "https://newadmin.stagemfc.com/powerbi";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.powerbi_reports, container, false);
        mWebView = view.findViewById(R.id.webview);
        mWebView.setListener(getActivity(), this);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.setFocusable(true);
        mWebView.setFocusableInTouchMode(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setDatabaseEnabled(true);
        mWebView.getSettings().setAppCacheEnabled(true);
        mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        mWebView.setInitialScale(1);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);

        mWebView.loadUrl(POWERBI_URL+"?AMId="+ CommonMethods.getstringvaluefromkey(getActivity(),"user_id"));


        try {

            searchicon.setVisibility(View.VISIBLE);
            MainActivity.navigation.getMenu().clear();
            MainActivity.navigation.inflateMenu(R.menu.asm_btmnavigation);
            MainActivity.navigation.getMenu().findItem(R.id.navigation_createWR).setCheckable(false);
            MainActivity.navigation.setVisibility(View.VISIBLE);


        }catch (Exception e){
            e.printStackTrace();
        }

        return view;
    }



    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        SpinnerManager.showSpinner(getActivity());
       // Log.e("url",""+url);

    }

    @Override
    public void onPageFinished(String url) {
     SpinnerManager.hideSpinner(getActivity());
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {

    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {

    }

    @Override
    public void onExternalPageRequest(String url) {

    }

    @SuppressLint("NewApi")
    @Override
    public void onResume() {
        super.onResume();
        mWebView.onResume();

    }

    @SuppressLint("NewApi")
    @Override
    public void onPause() {
        mWebView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mWebView.onDestroy();

        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        mWebView.onActivityResult(requestCode, resultCode, intent);

    }
}
