package com.mfcwl.mfc_dealer.ASMFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.ASMModel.FilterData;
import com.mfcwl.mfc_dealer.ASMModel.stockFilterData;
import com.mfcwl.mfc_dealer.ASMModel.stockFilterReq;
import com.mfcwl.mfc_dealer.ASMModel.stockWhere;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Fragment.StockFragment;
import com.mfcwl.mfc_dealer.InstanceCreate.Stocklisting;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.StateHead.StateHeadDashBoardFragment;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.ZonalHead.ZonalHeadDashBoardFragment;
import com.mfcwl.mfc_dealer.retrofitconfig.StockDasboardFilterServices;

import org.json.JSONObject;

import java.io.InputStream;
import java.util.List;

import lprServices.LPRDashboardResponse;
import lprServices.LPRService;
import lprServices.ReconcialationRequest;
import retrofit2.Response;
import sidekicklpr.StockReconciliationReport;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchVisble;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.tv_header_title;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ASM;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_STATEHEAD;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ZONALHEAD;

public class ASMStockFragment extends Fragment {

    TextView storeStock_tv,booked_title,offload_title;
    LinearLayout stock_lastweek;
    JSONObject apiParamsJSON = null;
    public String TAG = getClass().getSimpleName();

    List<FilterData> filterdata1;
    List<FilterData> filterdata2;
    List<FilterData> filterdata3;

    private LinearLayout llatdealership,llinoms,lldiscrepancy;
    private TextView countatdealership,countinoms,countdiscrepancy;
    private String dealercode;
    public static MenuItem items;
    public static Menu menus;
/*
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)

    {
        menu.clear();
        inflater.inflate(R.menu.menu_item, menu);
        this.menus=menu;
        menu.findItem(R.id.asmHome).setVisible(true);
        menu.findItem(R.id.add_image).setVisible(false);
        menu.findItem(R.id.share).setVisible(false);
        menu.findItem(R.id.delete_fea).setVisible(false);
        menu.findItem(R.id.stock_fil_clear).setVisible(false);
        menu.findItem(R.id.notification_bell).setVisible(false);
        menu.findItem(R.id.notification_cancel).setVisible(false);
        menu.findItem(R.id.asm_mail).setVisible(false);

        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        items=item;
        switch (item.getItemId()) {

            case R.id.asmHome:

                Fragment loadFragments = new DasboardFragment();
                loadFragmentH(loadFragments);

                return true;

        }
        return super.onOptionsItemSelected(item);
    }
*/

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        //menu.clear();

        MenuItem asmHome = menu.findItem(R.id.asmHome);

        asmHome.setVisible(true);
        asmHome.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (CommonMethods.getstringvaluefromkey(getActivity(), "user_type")) {
                    case USERTYPE_ZONALHEAD: {
                        Fragment loadFragments = new ZonalHeadDashBoardFragment();
                        loadFragmentH(loadFragments);
                    }
                    break;

                    case USERTYPE_STATEHEAD: {
                        Fragment loadFragments = new StateHeadDashBoardFragment();
                        loadFragmentH(loadFragments);
                    }
                    break;

                    case USERTYPE_ASM:
                    default: {
                        Fragment loadFragments = new DasboardFragment();
                        loadFragmentH(loadFragments);
                    }
                    break;
                }
                return true;
            }
        });


        MenuItem asm_mail = menu.findItem(R.id.asm_mail);
        MenuItem add_image = menu.findItem(R.id.add_image);
        MenuItem share = menu.findItem(R.id.share);
        MenuItem delete_fea = menu.findItem(R.id.delete_fea);
        MenuItem stock_fil_clear = menu.findItem(R.id.stock_fil_clear);
        MenuItem notification_bell = menu.findItem(R.id.notification_bell);
        MenuItem notification_cancel = menu.findItem(R.id.notification_cancel);

        asm_mail.setVisible(false);
        add_image.setVisible(false);
        share.setVisible(false);
        delete_fea.setVisible(false);
        stock_fil_clear.setVisible(false);
        notification_bell.setVisible(false);
        notification_cancel.setVisible(false);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.fragment_asmstock, container, false);

        setHasOptionsMenu(true);
        MainActivity.navigation.setVisibility(View.VISIBLE);
        dealercode = CommonMethods.getstringvaluefromkey(getActivity(), "dcode");
        CommonMethods.setvalueAgainstKey(getActivity(), "asm_pages", "stock_dashboard");

        storeStock_tv = view.findViewById(R.id.storeStock_tv);
        booked_title = view.findViewById(R.id.booked_title);
        offload_title = view.findViewById(R.id.offload_title);

        llatdealership = view.findViewById(R.id.lldiatdealership);
        llinoms = view.findViewById(R.id.lloms);
        lldiscrepancy = view.findViewById(R.id.lldiscrepancy);

        llatdealership.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushToReconciliationScreen();
            }
        });
        llinoms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushToReconciliationScreen();
            }
        });

        lldiscrepancy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pushToReconciliationScreen();
            }
        });

        countatdealership = view.findViewById(R.id.countatdealership);
        countinoms = view.findViewById(R.id.countoms);
        countdiscrepancy = view.findViewById(R.id.countdiscrepancy);


        stock_lastweek = view.findViewById(R.id.stock_lastweek);
        stock_lastweek.setOnClickListener(v -> {

        });

        clearDB();

        if (CommonMethods.isInternetWorking(getActivity())) {
            dasboardReq(view);
        } else {
            CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);
        }

        return view;
    }



    private void clearDB() {

        CommonMethods.setvalueAgainstKey(getActivity(), "column", "");
        CommonMethods.setvalueAgainstKey(getActivity(), "operator", "");
        CommonMethods.setvalueAgainstKey(getActivity(), "value", "");
        CommonMethods.setvalueAgainstKey(getActivity(), "statuslist", "");

        CommonMethods.setvalueAgainstKey(getActivity(), "photocounts", "false");
        CommonMethods.setvalueAgainstKey(getActivity(), "thirtydaycheckbox", "false");
        CommonMethods.setvalueAgainstKey(getActivity(), "certifiedstatus", "");

        CommonMethods.setvalueAgainstKey(getActivity(), "filterapply", "");
        CommonMethods.setvalueAgainstKey(getActivity(), "booknowbtn", "");
        CommonMethods.setvalueAgainstKey(getActivity(), "unbooknowbtn", "");

        CommonMethods.setvalueAgainstKey(getActivity(), "30Name","");

        Stocklisting.getInstance().setStockstatus("");

    }

    private void dasboardReq(View view) {

        String dealer_code=CommonMethods.getstringvaluefromkey(getActivity(),"dealer_code");

        SpinnerManager.showSpinner(getActivity());

        StockDasboardFilterServices.getLeadStatus(dealer_code,getActivity(),new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                SpinnerManager.hideSpinner(getActivity());

                Response<stockFilterReq> mRes = (Response<stockFilterReq>) obj;
                stockFilterReq mData = mRes.body();
               /* dasboardRes mActualdata = mData.get(0);
                Log.i("OnSuccess", "OnSuccess=: "+ mRes.body());*/

                Log.i(TAG, "OnSuccess=1: "+ mRes.body());
                displayData(mData,view);

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(getActivity());
            }

        });
    }

    private void displayData(stockFilterReq mData,View view) {

        if(mData == null){
            Log.e(TAG, "displayData: Data is null ");
            return;
        }
        /*stockFilterReq stockData = gson.fromJson(apiParamsJSON.toString(), stockFilterReq.class);*/

        List<stockFilterData> sectionData=mData.getDashboardData();

        storeStock_tv.setText(sectionData.get(0).getCount() + " " + sectionData.get(0).getTitle());
        booked_title.setText(sectionData.get(1).getCount() + " " + sectionData.get(1).getTitle());
        offload_title.setText(sectionData.get(2).getCount() + " " + sectionData.get(2).getTitle());

        for (int i = 0; i < sectionData.size(); i++) {

            Log.i(TAG, "filterdata: " + sectionData.get(i).getData());

            filterdata1 = sectionData.get(0).getData();
            filterdata2 = sectionData.get(1).getData();
            filterdata3 = sectionData.get(2).getData();

        }

          try {

              LinearLayout svi = (LinearLayout) view.findViewById(R.id.stock_lastweek);
              LayoutInflater layoutInflater = getLayoutInflater();

            int index;

            for (index = 0; index < filterdata1.size(); index++) {

                View rowView = layoutInflater.inflate(R.layout.row_layout, null, false);

                // Left
                LinearLayout linearLayoutLeft = (LinearLayout) rowView.findViewById(R.id.row_left_element);
                TextView textViewLeft = (TextView) linearLayoutLeft.findViewById(R.id.default_id_title);
                TextView textViewRight = (TextView) linearLayoutLeft.findViewById(R.id.default_id_options);
                textViewLeft.setText(filterdata1.get(index).getCount().toString());
                textViewRight.setText(filterdata1.get(index).getName().toString());

                int finalIndex = index;
                linearLayoutLeft.setOnClickListener(v -> {

                    List<stockWhere> where = filterdata1.get(finalIndex).getFilter();

                    CommonMethods.setvalueAgainstKey(getActivity(), "30Name", filterdata1.get(finalIndex).getName());

                    CommonMethods.setvalueAgainstKey(getActivity(), "stockdashboardname", filterdata1.get(finalIndex).getName());


                    for (int i = 0; i < where.size(); i++) {

                        Log.i(TAG, "Column:= " + where.get(i).getColumn());
                        Log.i(TAG, "Operator:= " + where.get(i).getOperator());
                        Log.i(TAG, "Value:= " + where.get(i).getValue());

                        CommonMethods.setvalueAgainstKey(getActivity(), "statuslist", "asmstore");
                        CommonMethods.setvalueAgainstKey(getActivity(), "stockfilterOnclick", "true");

                        CommonMethods.setvalueAgainstKey(getActivity(), "column", where.get(i).getColumn());
                        CommonMethods.setvalueAgainstKey(getActivity(), "operator", where.get(i).getOperator());
                        CommonMethods.setvalueAgainstKey(getActivity(), "value", where.get(i).getValue());
                    }

                    Fragment loadFragments = new StockFragment();
                    loadFragment(loadFragments);

                });
                svi.addView(rowView);
            }

            svi.refreshDrawableState();

        } catch (Exception e) {
            e.printStackTrace();
        }

          //booked stock data
        try {

            LinearLayout svi = (LinearLayout) view.findViewById(R.id.booked_data);
            LayoutInflater layoutInflater = getLayoutInflater();

            int index;

            for (index = 0; index < filterdata2.size(); index++) {

                View rowView = layoutInflater.inflate(R.layout.row_layout, null, false);

                // Left
                LinearLayout linearLayoutLeft = (LinearLayout) rowView.findViewById(R.id.row_left_element);
                TextView textViewLeft = (TextView) linearLayoutLeft.findViewById(R.id.default_id_title);
                TextView textViewRight = (TextView) linearLayoutLeft.findViewById(R.id.default_id_options);
                textViewLeft.setText(filterdata2.get(index).getCount().toString());
                textViewRight.setText(filterdata2.get(index).getName().toString());

                int finalIndex = index;
                linearLayoutLeft.setOnClickListener(v -> {
                    CommonMethods.setvalueAgainstKey(getActivity(), "30Name","");
                    List<stockWhere> where = filterdata2.get(finalIndex).getFilter();
                    for (int i = 0; i < where.size(); i++) {

                        Log.i(TAG, "Column:= " + where.get(i).getColumn());
                        Log.i(TAG, "Operator:= " + where.get(i).getOperator());
                        Log.i(TAG, "Value:= " + where.get(i).getValue());

                        CommonMethods.setvalueAgainstKey(getActivity(), "booknowbtn", "true");
                        CommonMethods.setvalueAgainstKey(getActivity(), "statuslist", "asmbook");

                        CommonMethods.setvalueAgainstKey(getActivity(), "column", where.get(i).getColumn());
                        CommonMethods.setvalueAgainstKey(getActivity(), "operator", where.get(i).getOperator());
                        CommonMethods.setvalueAgainstKey(getActivity(), "value", where.get(i).getValue());
                    }

                    Fragment loadFragments = new StockFragment();
                    loadFragment(loadFragments);

                });
                svi.addView(rowView);
            }

            svi.refreshDrawableState();

        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    private boolean loadFragmentH(Fragment fragment) {
        //switching fragment

        searchVisble("Dashboard");

        if (fragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fillLayout, fragment)
                    .commit();

            return true;
        }
        return false;
    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment

        if (tv_header_title != null) {

            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText(CommonMethods.getstringvaluefromkey(getActivity(), "dealerName"));
        }


        if (fragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fillLayout, fragment)
                    .commit();

            return true;
        }
        return false;
    }

    public static String loadJSONFromAsset(Context mContext, int screen) {
        String json = null;
        InputStream is = null;
        try {
            switch (screen) {
                case 1:
                    is = mContext.getAssets().open("stock_data.json");
                    break;
                case 2:
                    is = mContext.getAssets().open("leads_data.json");
                    break;
            }
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return json;
    }
    @Override
    public void onResume() {
        super.onResume();

        if (tv_header_title != null) {
            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText(CommonMethods.getstringvaluefromkey(getActivity(), "dealerName"));
        }

        Application.getInstance().trackScreenView(getActivity(),GlobalText.asm_stocks_dashboard);

        ReconcialationRequest request = new ReconcialationRequest();
        request.setDealerCode(dealercode);
        fetchLPRData(request);
    }

    private void fetchLPRData(ReconcialationRequest mRequest){
        LPRService.getLPRDashBoardDateFromServer(mRequest, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                Response<LPRDashboardResponse> mRes = (Response<LPRDashboardResponse>) obj;
                LPRDashboardResponse mData = mRes.body();
                countatdealership.setText(""+Math.abs(mData.getAtDealershipcount()));
                countinoms.setText(""+Math.abs(mData.getInoms()));
                countdiscrepancy.setText(""+Math.abs(mData.getDispcrepancy()));

            }

            @Override
            public void OnFailure(Throwable mThrowable) {

            }
        });
    }

    private void pushToReconciliationScreen(){
        Intent recon = new Intent(getActivity(),StockReconciliationReport.class);
        startActivity(recon);
    }


}
