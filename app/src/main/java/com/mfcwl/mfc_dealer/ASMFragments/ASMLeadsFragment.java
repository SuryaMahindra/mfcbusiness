package com.mfcwl.mfc_dealer.ASMFragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.ASMModel.privatelCount;
import com.mfcwl.mfc_dealer.ASMModel.privatelCountRes;
import com.mfcwl.mfc_dealer.Fragment.LeadSection.LeadsFragmentSection;
import com.mfcwl.mfc_dealer.InstanceCreate.LeadSection.LeadFilterSaveInstance;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.Procurement.Fragment.ProcurementTAB;
import com.mfcwl.mfc_dealer.Procurement.Instances.ProcLeadFilterSaveInstance;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.StateHead.StateHeadDashBoardFragment;
import com.mfcwl.mfc_dealer.StockModels.leadDasRes;
import com.mfcwl.mfc_dealer.StockModels.leadFilterASM;
import com.mfcwl.mfc_dealer.StockModels.leadsDasRq;
import com.mfcwl.mfc_dealer.StockModels.leadstatusASM;
import com.mfcwl.mfc_dealer.StockModels.webleadsASM;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.ZonalHead.ZonalHeadDashBoardFragment;
import com.mfcwl.mfc_dealer.retrofitconfig.LeadDasboardFilterServices;
import com.mfcwl.mfc_dealer.retrofitconfig.PrivateLCountServices;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchVisble;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.tv_header_title;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ASM;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_STATEHEAD;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ZONALHEAD;


public class ASMLeadsFragment extends Fragment {

    JSONObject apiParamsJSON = null;

    TextView webLead_title, private_leads_title;
    public String TAG = getClass().getSimpleName();

    List<webleadsASM> webleads;
    List<leadstatusASM> leadStatus;

    private final Calendar calendar = Calendar.getInstance();
    private final Calendar calendar_end = Calendar.getInstance();
    private final String dateFormat = "yyyy-MM-dd";
    private LinearLayout mStart, mEnd;
    private TextView mTxtStart, mTxtEnd;
    private boolean isStart;
    private final SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);
    private Date date;

    public View views;

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)

    LocalDate localDate;

    @TargetApi(Build.VERSION_CODES.O)
    public static MenuItem items;
    public static Menu menus;
   /* @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)


    {
        menu.clear();
        inflater.inflate(R.menu.menu_item, menu);
        this.menus=menu;
        menu.findItem(R.id.asmHome).setVisible(true);
        menu.findItem(R.id.add_image).setVisible(false);
        menu.findItem(R.id.share).setVisible(false);
        menu.findItem(R.id.delete_fea).setVisible(false);
        menu.findItem(R.id.stock_fil_clear).setVisible(false);
        menu.findItem(R.id.notification_bell).setVisible(false);
        menu.findItem(R.id.notification_cancel).setVisible(false);
        menu.findItem(R.id.asm_mail).setVisible(false);

        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        items=item;
        switch (item.getItemId()) {

            case R.id.asmHome:

                Fragment loadFragments = new DasboardFragment();
                loadFragmentH(loadFragments);

                return true;

        }
        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        //menu.clear();

        MenuItem asmHome = menu.findItem(R.id.asmHome);

        asmHome.setVisible(true);
        asmHome.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (CommonMethods.getstringvaluefromkey(getActivity(), "user_type")) {
                    case USERTYPE_ZONALHEAD: {
                        Fragment loadFragments = new ZonalHeadDashBoardFragment();
                        loadFragmentH(loadFragments);
                    }
                    break;
                    case USERTYPE_STATEHEAD: {
                        Fragment loadFragments = new StateHeadDashBoardFragment();
                        loadFragmentH(loadFragments);
                    }
                    break;
                    case USERTYPE_ASM:
                    default: {
                        Fragment loadFragments = new DasboardFragment();
                        loadFragmentH(loadFragments);
                    }
                    break;
                }
                return true;
            }
        });


        MenuItem asm_mail = menu.findItem(R.id.asm_mail);
        MenuItem add_image = menu.findItem(R.id.add_image);
        MenuItem share = menu.findItem(R.id.share);
        MenuItem delete_fea = menu.findItem(R.id.delete_fea);
        MenuItem stock_fil_clear = menu.findItem(R.id.stock_fil_clear);
        MenuItem notification_bell = menu.findItem(R.id.notification_bell);
        MenuItem notification_cancel = menu.findItem(R.id.notification_cancel);

        asm_mail.setVisible(false);
        add_image.setVisible(false);
        share.setVisible(false);
        delete_fea.setVisible(false);
        stock_fil_clear.setVisible(false);
        notification_bell.setVisible(false);
        notification_cancel.setVisible(false);


    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.fragment_asmleads, container, false);

        views = view;
        setHasOptionsMenu(true);
        Log.i(TAG, "onCreateView: ");
        CommonMethods.setvalueAgainstKey(getActivity(), "asm_pages", "lead_sales_dashboard");

        webLead_title = view.findViewById(R.id.webLead_title);
        private_leads_title = view.findViewById(R.id.private_leads_title);

        mStart = (LinearLayout) view.findViewById(R.id.llstart);
        mEnd = (LinearLayout) view.findViewById(R.id.llend);
        mTxtStart = (TextView) view.findViewById(R.id.txtstart);
        mTxtEnd = (TextView) view.findViewById(R.id.txtend);
        date = new Date();

        startDateSet();
        endDateSet();

       /* mTxtStart.setText(sdf.format(date));
        mTxtEnd.setText(sdf.format(date));*/

        mStart.setOnClickListener(v -> {
            getDatePickerDialog(getActivity()).show();
            isStart = true;
        });

        mEnd.setOnClickListener(v -> {
            getDatePickerDialog_End(getActivity()).show();
            isStart = false;
        });


        CommonMethods.setvalueAgainstKey(getActivity(), "leasStatus", "false");
        CommonMethods.setvalueAgainstKey(getActivity(), "Lcolumn", "");
        CommonMethods.setvalueAgainstKey(getActivity(), "Loperator", "");
        CommonMethods.setvalueAgainstKey(getActivity(), "Lvalue", "");

        CommonMethods.setvalueAgainstKey(getActivity(), "column2", "");
        CommonMethods.setvalueAgainstKey(getActivity(), "operator2", "");
        CommonMethods.setvalueAgainstKey(getActivity(), "value2", "");
        CommonMethods.setvalueAgainstKey(getActivity(), "leads_status", "");
        CommonMethods.setvalueAgainstKey(getActivity(), "withoutfollowup", "");


        if (CommonMethods.isInternetWorking(getActivity())) {
            leadRequest(views);
        } else {
            CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);
        }

        return view;
    }

    private void leadRequest(View view) {

        leadsDasRq req = new leadsDasRq();
        req.setAccessToken(CommonMethods.getstringvaluefromkey(getActivity(), "access_token"));
        req.setDealerCode(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"));
        req.setFromDate(mTxtStart.getText().toString() + " 00:00:00");
        req.setToDate(mTxtEnd.getText().toString() + " 23:59:59");

        req.setTag("web");

        if (CommonMethods.getstringvaluefromkey(getActivity(), "leadasm_status").equalsIgnoreCase("USEDCARSALES")) {
            req.setDealerType("USEDCARSALES");
        } else {
            req.setDealerType("PROCUREMENT");
        }

        leaddashservice(getActivity(), req, view);
    }


    private void leaddashservice(final Context mContext, final leadsDasRq leadsDasRq, View view) {
        SpinnerManager.showSpinner(mContext);
        LeadDasboardFilterServices.leaddashservice(mContext, leadsDasRq, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                Response<leadDasRes> mRes = (Response<leadDasRes>) obj;
                leadDasRes mData = mRes.body();

                if (webleads != null) webleads.clear();
                if (leadStatus != null) leadStatus.clear();

                String msg = mData.getMessage();

                if (mData.getStatus().equalsIgnoreCase("success")) {
                    displayData(mData, view);
                } else {
                    if (mData.getStatusCode().equals(401)) {
                        WebServicesCall.error_popup_retrofit(401, msg, mContext);
                    } else {
                        CommonMethods.alertMessage((Activity) mContext, msg);
                    }
                }
                // Toast.makeText(mContext, mRes.body().toString(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                try {
                    SpinnerManager.hideSpinner(mContext);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //  Toast.makeText(mContext, mThrowable.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void displayData(leadDasRes mData, View view) {


        webleads = mData.getWebLeads();
        leadStatus = mData.getLeadStatus();


        try {

            LinearLayout svi = null;
            LayoutInflater layoutInflater = null;

            View rowView = null;

            svi = (LinearLayout) view.findViewById(R.id.wedLead_data);
            layoutInflater = getLayoutInflater();
            svi.removeAllViews();

            int index;

            for (index = 0; index < webleads.size(); index++) {

                rowView = layoutInflater.inflate(R.layout.row_layout, null, false);

                // Left
                LinearLayout linearLayoutLeft = (LinearLayout) rowView.findViewById(R.id.row_left_element);
                TextView textViewLeft = (TextView) linearLayoutLeft.findViewById(R.id.default_id_title);
                TextView textViewRight = (TextView) linearLayoutLeft.findViewById(R.id.default_id_options);
                textViewLeft.setText(webleads.get(index).getCount().toString());
                textViewRight.setText(webleads.get(index).getDisplayName().toString());

                int finalIndex = index;
                linearLayoutLeft.setOnClickListener(v -> {

                    List<leadFilterASM> where = webleads.get(finalIndex).getFilter();
                    CommonMethods.setvalueAgainstKey(getActivity(), "leadStatus", "true");

                    Log.i(TAG, "Column:= " + "sadfasd");


                    for (int i = 0; i < where.size(); i++) {

                        Log.i(TAG, "Column:= " + where.get(i).getColumn());
                        Log.i(TAG, "Operator:= " + where.get(i).getOperator());
                        Log.i(TAG, "Value:= " + where.get(i).getValue());

                        CommonMethods.setvalueAgainstKey(getActivity(), "Lcolumn", where.get(i).getColumn());
                        CommonMethods.setvalueAgainstKey(getActivity(), "Loperator", where.get(i).getOperator());
                        CommonMethods.setvalueAgainstKey(getActivity(), "Lvalue", where.get(i).getValue().toLowerCase());

                    }

                    CommonMethods.setvalueAgainstKey(getActivity(), "startDate", mTxtStart.getText().toString());
                    CommonMethods.setvalueAgainstKey(getActivity(), "endDate", mTxtEnd.getText().toString());

                   // CommonMethods.setvalueAgainstKey(getActivity(), "Lvalue", webleads.get(finalIndex).getStaus());

                    if (CommonMethods.getstringvaluefromkey(getActivity(), "leadasm_status").equalsIgnoreCase("USEDCARSALES")) {

                        LeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());
                        LeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());

                        Fragment loadFragments = new LeadsFragmentSection();
                        loadFragment(loadFragments);
                    } else {


                        ProcLeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());
                        ProcLeadFilterSaveInstance.getInstance().setStatusarray(new JSONArray());

                        ProcurementTAB leadsFragment = new ProcurementTAB();
                        loadFragment(leadsFragment);
                    }


                });


                svi.addView(rowView);
            }

            svi.refreshDrawableState();

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

            LinearLayout svi = (LinearLayout) view.findViewById(R.id.status_data);
            LayoutInflater layoutInflater = getLayoutInflater();
            svi.removeAllViews();

            int index;

            for (index = 0; index < leadStatus.size(); index++) {

                View rowView = layoutInflater.inflate(R.layout.row_layout, null, false);

                // Left
                LinearLayout linearLayoutLeft = (LinearLayout) rowView.findViewById(R.id.row_left_element);
                TextView textViewLeft = (TextView) linearLayoutLeft.findViewById(R.id.default_id_title);
                TextView textViewRight = (TextView) linearLayoutLeft.findViewById(R.id.default_id_options);
                textViewLeft.setText(leadStatus.get(index).getTotal().toString());
                textViewRight.setText(leadStatus.get(index).getDisplayName().toString());

                int finalIndex = index;
                linearLayoutLeft.setOnClickListener(v -> {

                    List<leadFilterASM> where = leadStatus.get(finalIndex).getFilter();

                    CommonMethods.setvalueAgainstKey(getActivity(), "leadStatus", "true");


                    for (int i = 0; i < where.size(); i++) {

                        Log.i(TAG, "Column:= " + where.get(i).getColumn());
                        Log.i(TAG, "Operator:= " + where.get(i).getOperator());
                        Log.i(TAG, "Value:= " + where.get(i).getValue());

                        CommonMethods.setvalueAgainstKey(getActivity(), "Lcolumn", where.get(i).getColumn());
                        CommonMethods.setvalueAgainstKey(getActivity(), "Loperator", where.get(i).getOperator());
                        CommonMethods.setvalueAgainstKey(getActivity(), "Lvalue", where.get(i).getValue().toLowerCase());

                    }

                    CommonMethods.setvalueAgainstKey(getActivity(), "startDate", mTxtStart.getText().toString());
                    CommonMethods.setvalueAgainstKey(getActivity(), "endDate", mTxtEnd.getText().toString());


                    if (CommonMethods.getstringvaluefromkey(getActivity(), "leadasm_status").equalsIgnoreCase("USEDCARSALES")) {
                        Fragment loadFragments = new LeadsFragmentSection();
                        loadFragment(loadFragments);
                    } else {
                        ProcurementTAB leadsFragment = new ProcurementTAB();
                        loadFragment(leadsFragment);
                    }

                });

                svi.addView(rowView);
            }

            svi.refreshDrawableState();

        } catch (Exception e) {
            e.printStackTrace();
        }

        privatelCount req = new privatelCount();
        req.setDealercode(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"));
        req.setFromdate(mTxtStart.getText().toString());
        req.setTodate(mTxtEnd.getText().toString());

        if (CommonMethods.getstringvaluefromkey(getActivity(), "leadasm_status").equalsIgnoreCase("USEDCARSALES")) {
            req.setLeadType("sales");
        } else {
            req.setLeadType("procurement");
        }

        if (CommonMethods.isInternetWorking(getActivity())) {
            privateCountReq(req);
        } else {
            CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);
        }

    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fillLayout, fragment)
                    .commit();
            return true;
        }

        return false;
    }

    private DatePickerDialog.OnDateSetListener getOnDateSetListener() {
        return new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                mTxtStart.setText(sdf.format(calendar.getTime()));

            }
        };
    }

    private DatePickerDialog.OnDateSetListener getOnDateSetListener_End() {
        return new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                calendar_end.set(Calendar.YEAR, year);
                calendar_end.set(Calendar.MONTH, monthOfYear);
                calendar_end.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                mTxtEnd.setText(sdf.format(calendar_end.getTime()));

                CommonMethods.setvalueAgainstKey(getActivity(), "leadStartDate", mTxtStart.getText().toString());
                CommonMethods.setvalueAgainstKey(getActivity(), "leadEndDate", mTxtEnd.getText().toString());

                if (CommonMethods.isInternetWorking(getActivity())) {
                    leadRequest(views);
                } else {
                    CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);
                }

            }
        };
    }


    private void privateCountReq(privatelCount req) {

        SpinnerManager.showSpinner(getActivity());

        PrivateLCountServices.privateCount(req, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(getActivity());
                Response<List<privatelCountRes>> mRes = (Response<List<privatelCountRes>>) obj;
                List<privatelCountRes> mData = mRes.body();


                if (mData == null || mData.isEmpty()) {
                    // Toast.makeText(getActivity(),"isEmpty",Toast.LENGTH_LONG).show();
                } else {
                    //  Toast.makeText(getActivity(),"sadf",Toast.LENGTH_LONG).show();

                    // Log.i(TAG, "OnSuccess:=== "+mData.size());

                    int str = mData.get(0).getLeadCount();

                    //Toast.makeText(getActivity(), "data", Toast.LENGTH_LONG).show();

                    private_leads_title.setText(String.valueOf(str) + " Walk-in Leads");
                }
            }

            @Override
            public void OnFailure(Throwable mThrowable) {

                SpinnerManager.hideSpinner(getActivity());
            }

        });
    }


    /**
     * Return new DatePickerDialog for field
     *
     * @param context
     * @return
     */
    private DatePickerDialog getDatePickerDialog(Context context) {
        return new DatePickerDialog(context, getOnDateSetListener(), calendar
                .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
    }

    private DatePickerDialog getDatePickerDialog_End(Context context) {
        return new DatePickerDialog(context, getOnDateSetListener_End(), calendar_end
                .get(Calendar.YEAR), calendar_end.get(Calendar.MONTH),
                calendar_end.get(Calendar.DAY_OF_MONTH));
    }


    public Date addOneMonth() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 0);
        return cal.getTime();
    }

    private boolean loadFragmentH(Fragment fragment) {
        //switching fragment

        searchVisble("Dashboard");


        if (fragment != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fillLayout, fragment)
                    .commit();

            return true;
        }
        return false;
    }





    private void startDateSet() {


        try {

            if (CommonMethods.getstringvaluefromkey(getActivity(), "leadStartDate").equalsIgnoreCase("")) {
                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);

                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, 1);

                mTxtStart.setText(sdf.format(calendar.getTime()));
            } else {

                String[] splitDateTime = CommonMethods.getstringvaluefromkey(getActivity(), "leadStartDate").split(" ", 10);
                String[] splitDate = splitDateTime[0].split("-", 10);

                int year = Integer.parseInt(splitDate[0]);
                int month = Integer.parseInt(splitDate[1]);
                int day = Integer.parseInt(splitDate[2]);

                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month-1);
                calendar.set(Calendar.DAY_OF_MONTH, day);

                mTxtStart.setText(sdf.format(calendar.getTime()));

            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void endDateSet() {

        try {

            if (CommonMethods.getstringvaluefromkey(getActivity(), "leadEndDate").equalsIgnoreCase("")) {
                mTxtEnd.setText(sdf.format(calendar_end.getTime()));
            } else {

                String[] splitDateTime = CommonMethods.getstringvaluefromkey(getActivity(), "leadEndDate").split(" ", 10);
                String[] splitDate = splitDateTime[0].split("-", 10);

                int year = Integer.parseInt(splitDate[0]);
                int month = Integer.parseInt(splitDate[1]);
                int day = Integer.parseInt(splitDate[2]);

                calendar_end.set(Calendar.YEAR, year);
                calendar_end.set(Calendar.MONTH, month-1);
                calendar_end.set(Calendar.DAY_OF_MONTH, day);

                mTxtEnd.setText(sdf.format(calendar_end.getTime()));

            }

        }catch (Exception e){
            e.printStackTrace();
        }



    }

    @Override
    public void onResume() {
        super.onResume();


        if (tv_header_title != null) {
            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText(CommonMethods.getstringvaluefromkey(getActivity(), "dealerName"));
        }

        //MyApplication.getInstance().trackScreenView(GlobalText.asm_leads_dashboard);
    }
}
