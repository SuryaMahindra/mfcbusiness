package com.mfcwl.mfc_dealer.ASMFragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.mfcwl.mfc_dealer.R
import com.mfcwl.mfc_dealer.Utility.CommonMethods
import com.mfcwl.mfc_dealer.Utility.SpinnerManager
import kotlinx.android.synthetic.main.rdm_dealer_list_dialog.*
import remotedealer.adapter.DealersAdapter
import remotedealer.dashboard.ui.RemoteDealerDashboard
import remotedealer.interfaces.DealerInterface
import remotedealer.listener.DealerListener
import remotedealer.model.DealerItem
import remotedealer.model.dashboard.DealerListResponse
import remotedealer.rdrform.ui.*
import remotedealer.retrofit.RetroBase
import remotedealer.util.NavigationUtility
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ASMDealerFragment(var code:String, var mToken: String, var callback:DealerInterface) : BaseRDMFormFragment() {
    var TAG = javaClass.simpleName
    lateinit var vi: View
    private lateinit var dealerData: ArrayList<DealerItem>

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        Log.i(TAG, "onCreateView: ")
//        RDMHomeActivity.currentFragment = TAG
        vi = inflater.inflate(R.layout.rdm_dealer_list_dialog, container, false)
        getDealersList()
        return vi
    }

    private fun getDealersList() {
        SpinnerManager.showSpinner(context)
        RetroBase.retroInterface.getFromWeb("${RetroBase.URL_END_DASHBOARD_DEALERLIST}?asmId=${CommonMethods.getstringvaluefromkey(activity, "user_id")}", "Bearer $mToken").enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                try {
                    SpinnerManager.hideSpinner(context)
                    val strRes = Gson().toJson(response.body())
                    val modelRes = Gson().fromJson(strRes, DealerListResponse::class.java)
                    Log.i(TAG, "onResponse: ${modelRes.data.size}")
                    dealerData = modelRes.data
                    callAdapter()
                } catch (e: Exception) {
                    SpinnerManager.hideSpinner(context)

                    Log.e(TAG, "onResponse: " + e.message)
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                SpinnerManager.hideSpinner(context)
                t.printStackTrace()
            }
        })
    }

    private fun callAdapter() {
        val adapter = activity?.let {
            context?.let { it1 ->
                DealersAdapter(it1,
                        it,
                        code,
                        dealerData,
                        object : DealerListener {
                            override fun onDealerClicked(dealer: DealerItem) {

                                callback.onUpdate(dealer)
                                CommonMethods.setvalueAgainstKey(activity, "dealer_code", dealer.code)
                                preferenceManager.writeString(SELECTED_DEALER_CODE, dealerCode)

                                NavigationUtility.removeAllFragments(activity)

                                when (RDMHomeActivity.currentFragment) {
                                    RDMHomeActivity.DASHBOARD -> {
//                                    tvSavedLbl.visibility = View.GONE
                                        setCurrentFragment(RemoteDealerDashboard(dealer.code, mToken))
                                    }
                                    RDMHomeActivity.PROCANDSALES -> {
//                                    tvSavedLbl.visibility = View.GONE
                                        val frag = ProcAndSalesFragment()
                                        frag.code = dealer.code
                                        setCurrentFragment(frag)
                                    }
                                    RDMHomeActivity.SALESANDLEADS -> {
//                            tvSavedLbl.visibility = View.GONE
                                        val frag = RDMSalesAndLeadsFragment(dealer.code, mToken, "")
                                        setCurrentFragment(frag)
                                    }
                                    RDMHomeActivity.EVENTS -> {
//                                    tvSavedLbl.visibility = View.GONE
                                        val frag = EventListFragment()
                                        frag.code = dealer.code
                                        frag.name = dealer.name
                                        frag.meetingType = dealer.type
                                        setCurrentFragment(frag)
                                    }
                                    RDMHomeActivity.RDRFORM -> {
                                        val rdmFormFragment = if (dealer.rdrstatus.equals("Completed", ignoreCase = true)) {
                                            RDMFormFragment.newInstance(dealer.code, mToken, true)
                                        } else {
                                            RDMFormFragment.newInstance(dealer.code, mToken, false)
                                        }
                                        setCurrentFragment(rdmFormFragment)
                                    }
                                    else -> {
                                        val frag = RDMSalesAndLeadsFragment(dealer.code, mToken, "")
                                        setCurrentFragment(frag)
                                        Log.i(TAG, "onDealerClicked: ")
                                    }
                                }
                            }
                        })
            }
        }

        recyclerViewDealer.layoutManager = LinearLayoutManager(context)
        recyclerViewDealer.adapter = adapter
    }



    fun setCurrentFragment(fragment: Fragment) {
        NavigationUtility.replaceFragment(fragment, activity, fragment.tag)
    }

}