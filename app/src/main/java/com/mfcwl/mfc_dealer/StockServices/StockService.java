package com.mfcwl.mfc_dealer.StockServices;


import android.content.Context;

import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.StockModels.StockRequest;
import com.mfcwl.mfc_dealer.StockModels.StockResponse;
import com.mfcwl.mfc_dealer.retrofitconfig.BaseService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class StockService extends BaseService {

    public static void fetchStock(final StockRequest mStockRequest, Context mContext, final HttpCallResponse mHttpCallResponse) {
        StockStoreInterface mInterface = retrofit.create(StockStoreInterface.class);

        Call<StockResponse> mCall = mInterface.getStock(mStockRequest);

        mCall.enqueue(new Callback<StockResponse>() {
            @Override
            public void onResponse(Call<StockResponse> call, Response<StockResponse> response) {

                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {

                    Throwable t=new Throwable ();
                    mHttpCallResponse.OnFailure(t);

                    if (response.code() == 400) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 401) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 500) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 404) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 304) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 503) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 405) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    }
                }
            }

            @Override
            public void onFailure(Call<StockResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });
    }


    public static void fetchBookStcok(final StockRequest mStockRequest, Context mContext, final HttpCallResponse mHttpCallResponse) {
        StockStoreInterface mInterface = retrofit.create(StockStoreInterface.class);

        Call<StockResponse> mCall = mInterface.getStock(mStockRequest);

        mCall.enqueue(new Callback<StockResponse>() {
            @Override
            public void onResponse(Call<StockResponse> call, Response<StockResponse> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {

                    Throwable t=new Throwable ();
                    mHttpCallResponse.OnFailure(t);

                    if (response.code() == 400) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 401) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 500) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 404) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 304) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 503) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 405) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    }
                }
            }

            @Override
            public void onFailure(Call<StockResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });
    }

    public static void sendEmail(final StockRequest mStockRequest, Context mContext, final HttpCallResponse mHttpCallResponse) {

        SendEmailStockStoreInterface mInterface = retrofit.create(SendEmailStockStoreInterface.class);

        Call<String> mCall = mInterface.getStock(mStockRequest);

        mCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {

                    Throwable t=new Throwable ();
                    mHttpCallResponse.OnFailure(t);

                    if (response.code() == 400) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 401) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 500) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 404) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 304) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 503) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 405) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });
    }



    public interface StockStoreInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("dealer/stocks/store")
        Call<StockResponse> getStock(@Body StockRequest mStockRequest);
    }

    public interface SendEmailStockStoreInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("dealer/stocks/store")
        Call<String> getStock(@Body StockRequest mStockRequest);
    }



}
