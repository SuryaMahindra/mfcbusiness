package com.mfcwl.mfc_dealer.ImageFilter;

import android.graphics.Bitmap;

import com.mfcwl.mfc_dealer.ImageFilter.Filter;

public class ThumbnailItem {
    public String filterName;
    public Bitmap image;
    public Filter filter;

    public ThumbnailItem() {
        image = null;
        filter = new Filter();
    }
}
