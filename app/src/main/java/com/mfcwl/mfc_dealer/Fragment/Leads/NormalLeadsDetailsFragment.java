package com.mfcwl.mfc_dealer.Fragment.Leads;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.Activity.Leads.LeadsInstance;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseModel.PrivateLeadItem;
import com.mfcwl.mfc_dealer.ResponseModel.PrivateLeadResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.MonthUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.mfcwl.mfc_dealer.Activity.Leads.LeadDetailsActivity.Invisible;

/**
 * Created by HP 240 G5 on 12-03-2018.
 */

@SuppressLint("ValidFragment")
public class NormalLeadsDetailsFragment extends Fragment {


    String lname, lmobile, lemail, lstatus, lposteddate, lmake, lmodel, lprice, lregno, lfollupdate, lexename, lregcity, lowner, lregyear, lcolor, lkms, ldid, remarks, ldays, Lvariant, Lmfyear;

    public static CardView cardview2;

    public static boolean lost = false;

    public NormalLeadsDetailsFragment() {

    }

    public NormalLeadsDetailsFragment(String lname, String lmobile, String lemail, String lstatus, String lposteddate, String lmake, String lmodel, String lprice, String lregno, String lfollupdate, String lexename, String lregcity, String lowner, String lregyear, String lcolor, String lkms, String ldid, String remarks, String ldays, String Lvariant) {
        this.lname = lname;
        this.lmobile = lmobile;
        this.lemail = lemail;
        this.lstatus = lstatus;
        this.lposteddate = lposteddate;
        this.lmake = lmake;
        this.lmodel = lmodel;
        this.lprice = lprice;
        this.lregno = lregno;
        this.lfollupdate = lfollupdate;
        this.lexename = lexename;
        this.lregcity = lregcity;
        this.lowner = lowner;
        this.lregyear = lregyear;
        this.lcolor = lcolor;
        this.lkms = lkms;
        this.ldid = ldid;
        this.remarks = remarks;
        this.ldays = ldays;
        this.Lvariant = Lvariant;
    }


    public NormalLeadsDetailsFragment(String lname, String lmobile, String lemail, String lstatus, String lposteddate, String lmake, String lmodel, String lprice, String lregno, String lfollupdate, String lexename, String lregcity, String lowner, String lregyear, String lcolor, String lkms, String ldid, String remarks, String ldays, String Lvariant, String Lmfyear) {
        this.lname = lname;
        this.lmobile = lmobile;
        this.lemail = lemail;
        this.lstatus = lstatus;
        this.lposteddate = lposteddate;
        this.lmake = lmake;
        this.lmodel = lmodel;
        this.lprice = lprice;
        this.lregno = lregno;
        this.lfollupdate = lfollupdate;
        this.lexename = lexename;
        this.lregcity = lregcity;
        this.lowner = lowner;
        this.lregyear = lregyear;
        this.lcolor = lcolor;
        this.lkms = lkms;
        this.ldid = ldid;
        this.remarks = remarks;
        this.ldays = ldays;
        this.Lvariant = Lvariant;
        this.Lmfyear = Lmfyear;
    }


    @SuppressLint("ValidFragment")
    public NormalLeadsDetailsFragment(String lname) {
        // Required empty public constructor

        this.lname = lname;
    }


    public static TextView leads_details_next_followup, leads_details_exe_name, leads_details_name, leads_details_num, leads_details_email, leads_details_days, leads_details_hot;
    public static TextView leads_details_model, leads_details_model_name, leads_details_price, leads_details_model_num, leads_details_message;
    public static TextView leads_details_register_city_name, leads_details_color_which, leads_details_owner_name, leads_details_kms_which, leads_details_year_which, leads_details_fuel_which;
    String name = "";
    public static ImageView leads_details_phonecall, leads_details_messages, leads_details_whatsapp;

    public static LinearLayout badgeid;
    String TAG = getClass().getSimpleName();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*if (getArguments() != null) {
            name = getArguments().getString("paramsname");
            Log.e("getArguments ","getArguments "+name);
        }
        else
        {
            Log.e("getArguments ","getArguments null ");
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.layout_leads_details, container, false);
        cardview2 = view.findViewById(R.id.cardview2);
        leads_details_next_followup = view.findViewById(R.id.leads_details_next_followup);
        leads_details_exe_name = view.findViewById(R.id.leads_details_exe_name);

        leads_details_name = view.findViewById(R.id.leads_details_name);
        leads_details_num = view.findViewById(R.id.leads_details_num);
        leads_details_email = view.findViewById(R.id.leads_details_email);
        leads_details_days = view.findViewById(R.id.leads_details_days);
        leads_details_hot = view.findViewById(R.id.leads_details_hot);

        leads_details_model = view.findViewById(R.id.leads_details_model);
        leads_details_model_name = view.findViewById(R.id.leads_details_model_name);
        leads_details_price = view.findViewById(R.id.leads_details_price);
        leads_details_model_num = view.findViewById(R.id.leads_details_model_num);
        leads_details_message = view.findViewById(R.id.leads_details_message);

        leads_details_register_city_name = view.findViewById(R.id.leads_details_register_city_name);
        leads_details_color_which = view.findViewById(R.id.leads_details_color_which);
        leads_details_owner_name = view.findViewById(R.id.leads_details_owner_name);
        leads_details_kms_which = view.findViewById(R.id.leads_details_kms_which);
        leads_details_year_which = view.findViewById(R.id.leads_details_year_which);

        leads_details_fuel_which = view.findViewById(R.id.leads_details_fuel_which);

        leads_details_phonecall = view.findViewById(R.id.leads_details_phonecall);
        leads_details_messages = view.findViewById(R.id.leads_details_messages);

        leads_details_whatsapp = view.findViewById(R.id.leads_details_whatsapp);

        badgeid = view.findViewById(R.id.badgeid);

        if (CommonMethods.getstringvaluefromkey(getActivity(), "user_type").equalsIgnoreCase("dealer")) {

        } else {
            badgeid.setVisibility(View.INVISIBLE);
        }

        //name = getActivity().getIntent().getExtras().getString("NM");
        leads_details_name.setText(capitalize(lname));
        leads_details_num.setText(lmobile);
        leads_details_email.setText(lemail);
        leads_details_hot.setText(capitalize(lstatus));
        leads_details_model.setText(lmake);
        if (Lvariant.equals("null")) {
            Lvariant = "";
        }
        leads_details_model_name.setText(lmodel + " " + Lvariant);
        leads_details_price.setText(lprice);
        leads_details_message.setText(remarks);
        if (remarks.trim().equals("") || remarks.trim().equals("NA")) {
            leads_details_message.setText("No comments available");
        }
        leads_details_days.setText(ldays);

        if (lstatus.equalsIgnoreCase("Open")) {
            leads_details_hot.setBackgroundResource(R.drawable.open_28);
            //leads_details_hot.setVisibility(View.INVISIBLE);
        }
        if (lstatus.equalsIgnoreCase("") || lstatus.equalsIgnoreCase("null")) {
            leads_details_hot.setBackgroundResource(R.drawable.open_28);
            leads_details_hot.setText(" Open");
            //leads_details_hot.setVisibility(View.INVISIBLE);
        }
        if (lstatus.equalsIgnoreCase("Hot")) {
            leads_details_hot.setBackgroundResource(R.drawable.hot_28);
            leads_details_hot.setVisibility(View.VISIBLE);
        }
        if (lstatus.equalsIgnoreCase("Warm")) {
            leads_details_hot.setBackgroundResource(R.drawable.warm_28);
            leads_details_hot.setVisibility(View.VISIBLE);
        }
        if (lstatus.equalsIgnoreCase("Cold")) {
            leads_details_hot.setBackgroundResource(R.drawable.cold_28);
            leads_details_hot.setVisibility(View.VISIBLE);
        }
        if (lstatus.equalsIgnoreCase("Lost")) {
            leads_details_hot.setBackgroundResource(R.drawable.lost_28);
            leads_details_hot.setVisibility(View.VISIBLE);
            Invisible();

        }
        if (lstatus.equalsIgnoreCase("Sold")) {
            leads_details_hot.setBackgroundResource(R.drawable.sold_28);
            leads_details_hot.setVisibility(View.VISIBLE);
        }
        if (lstatus.equalsIgnoreCase("Visiting")) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_visiting);
            leads_details_hot.setVisibility(View.VISIBLE);
        }
        if (lstatus.equalsIgnoreCase("Search")) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_search_lead);
            leads_details_hot.setVisibility(View.VISIBLE);
        }
        if (lstatus.equalsIgnoreCase("Not-Interested")) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_not_intrested);
            leads_details_hot.setVisibility(View.VISIBLE);
            Invisible();
        }
        if (lstatus.equalsIgnoreCase("Finalised")) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_finalized);
            leads_details_hot.setVisibility(View.VISIBLE);
            Invisible();
        }
        if (lstatus.equalsIgnoreCase("No-Response")) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_no_responce);
            leads_details_hot.setVisibility(View.VISIBLE);
        }

        try {

            if (LeadsInstance.getInstance().getWhichleads().equals("WebLeads")) {
                String[] splitdatespace = lfollupdate.split(" ", 10);
                String[] splitdate1 = splitdatespace[0].split("-", 10);
                lfollupdate = splitdate1[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate1[1])] + " " + splitdate1[0];

            } else {
                String[] splitdatespace = lfollupdate.split(" ", 10);
                String[] splitdate1 = splitdatespace[0].split("-", 10);
                lfollupdate = splitdate1[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate1[1])] + " " + splitdate1[0];
            }
            Log.e("leadfollow up date", lfollupdate);
        } catch (Exception e) {

        }

        if (lexename.trim().equalsIgnoreCase("null") || lexename.trim().equalsIgnoreCase("")) {
            lexename = "NA";
        }
        if (lfollupdate != null) {
            if (lfollupdate.startsWith("00") || lfollupdate.startsWith("null") || lfollupdate.equals("")) {
                lfollupdate = "NA";
            }
        } else {
            lfollupdate = "NA";
        }

        String sourceString1 = "Next Follow-up " + "<br>" + "<font color=\"black\">" + lfollupdate + "</font>" + "</br> ";
        String sourceString2 = "Executive Name " + "<br>" + "<font color=\"black\">" + lexename + "</font>" + "</br> ";
        leads_details_next_followup.setText(Html.fromHtml(sourceString1));
        leads_details_exe_name.setText(Html.fromHtml(sourceString2));

        if (lregcity == null || lregcity.equalsIgnoreCase("null") || lregcity.equalsIgnoreCase("")) {
            lregcity = "NA";
        }
        leads_details_register_city_name.setText(lregcity);

        if (lowner == null || lowner.equalsIgnoreCase("null") || lowner.equalsIgnoreCase("")) {
            lowner = "NA";
        }
        leads_details_owner_name.setText(lowner);
        //leads_details_year_which.setText(lregyear);

        if (lcolor == null || lcolor.equalsIgnoreCase("null") || lcolor.equalsIgnoreCase("")) {
            lcolor = "NA";
        }
        leads_details_color_which.setText(lcolor);

        if (lkms == null || lkms.equalsIgnoreCase("null") || lkms.equalsIgnoreCase("")) {
            lkms = "NA";
        }

        leads_details_kms_which.setText(lkms);

        Log.e("LDID ", "Val " + ldid);

        if (LeadsInstance.getInstance().getWhichleads().equals("WebLeads")) {
            if (lregno == null || lregno.equalsIgnoreCase("null") || lregno.equalsIgnoreCase("")) {
                lregno = "NA";
            }
            leads_details_model_num.setText(lregno);
            if (Lmfyear == null || Lmfyear.equalsIgnoreCase("null") || Lmfyear.equalsIgnoreCase("")) {
                Lmfyear = "NA";
            }
            leads_details_year_which.setText(Lmfyear);
            cardview2.setVisibility(View.GONE);
        } else {
            if (lregno == null || lregno.equalsIgnoreCase("null") || lregno.equalsIgnoreCase("")) {
                lregno = "NA";
            }
            leads_details_model_num.setText(lregno);
            cardview2.setVisibility(View.GONE);
        }

        leads_details_model_num.setVisibility(View.VISIBLE);

        leads_details_phonecall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"), GlobalText.leads_details_call, GlobalText.android);
                Intent callIntent = new Intent(Intent.ACTION_VIEW);
                callIntent.setData(Uri.parse("tel:" + lmobile));//change the number
                startActivity(callIntent);
            }
        });

        leads_details_messages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"), GlobalText.leads_details_message, GlobalText.android);

                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:" + lmobile));
                sendIntent.putExtra("sms_body", "");
                startActivity(sendIntent);
            }
        });

        leads_details_whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"), GlobalText.leads_details_whatsapp, GlobalText.android);

                openWhatsApp(lmobile);
            }
        });


        return view;

    }


    private void openWhatsApp(String number) {
        String smsNumber = "91" + number; //without '+'
        try {
            /*Intent sendIntent = new Intent("android.intent.action.MAIN");
            //sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.setType("stock_fragment/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my stock_fragment to send.");
            sendIntent.putExtra("jid", smsNumber + "@s.whatsapp.net"); //phone number without "+" prefix
            sendIntent.setPackage("com.whatsapp");
            context.startActivity(sendIntent);*/

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_VIEW);
            String url = "https://api.whatsapp.com/send?phone=" + smsNumber + "&stock_fragment=" + "";
            sendIntent.setData(Uri.parse(url));
            startActivity(sendIntent);

        } catch (Exception e) {
            Toast.makeText(getActivity(), "Error/n" + e.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    public static void updateLeadState(String upleadstatus, String upnextfollowdate, String upremark) {

        leads_details_hot.setText(upleadstatus);

        if (upleadstatus.equalsIgnoreCase("Open")) {
            leads_details_hot.setBackgroundResource(R.drawable.hot_28);
            leads_details_hot.setVisibility(View.INVISIBLE);
        }
        if (upleadstatus.equalsIgnoreCase("Hot")) {
            leads_details_hot.setBackgroundResource(R.drawable.hot_28);
            leads_details_hot.setVisibility(View.VISIBLE);
        }
        if (upleadstatus.equalsIgnoreCase("Warm")) {
            leads_details_hot.setBackgroundResource(R.drawable.warm_28);
            leads_details_hot.setVisibility(View.VISIBLE);
        }
        if (upleadstatus.equalsIgnoreCase("Cold")) {
            leads_details_hot.setBackgroundResource(R.drawable.cold_28);
            leads_details_hot.setVisibility(View.VISIBLE);
        }
        if (upleadstatus.equalsIgnoreCase("Lost")) {
            leads_details_hot.setBackgroundResource(R.drawable.lost_28);
            leads_details_hot.setVisibility(View.VISIBLE);
        }
        if (upleadstatus.equalsIgnoreCase("Sold")) {
            leads_details_hot.setBackgroundResource(R.drawable.sold_28);
            leads_details_hot.setVisibility(View.VISIBLE);
        }

        if (upnextfollowdate.equals("")) {
            upnextfollowdate = "NA";
        }

        String updatefollowdate = "Next Follow-up " + "<br>" + "<font color=\"black\">" + upnextfollowdate + "</font>" + "</br> ";
        leads_details_next_followup.setText(Html.fromHtml(updatefollowdate));
        if (leads_details_message.getText().toString().equals("NA") || leads_details_message.getText().toString().equals("")) {
            upremark = "No comments available";
            leads_details_message.setText(upremark);
        } else {
            leads_details_message.setText(upremark);
        }

    }

    private String capitalize(String capString) {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = null;
       try {

           if(capString !=  null){
               capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
               while (capMatcher.find()) {
                   capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
               }

           }

       }catch (Exception e){
           e.printStackTrace();
       }
        return capMatcher.appendTail(capBuffer).toString();
    }

    public static void Parsewebleadstore(JSONObject jObj, Activity activity) {

        String updateFollowup = "";
        String remarkweb = "";
        try {
            if (jObj.getString("status").equals("failure")) {

            } else {
                Log.e("NewCheck123 ", "Response " + jObj.toString());

                JSONArray arrayList = jObj.getJSONArray("data");
                JSONObject jsonobject = new JSONObject(arrayList.getString(0));

                leads_details_hot.setText(jsonobject.getString("status"));

                if (jsonobject.getString("status").equalsIgnoreCase("Open")) {
                    leads_details_hot.setBackgroundResource(R.drawable.hot_28);
                    leads_details_hot.setVisibility(View.INVISIBLE);
                }
                if (jsonobject.getString("status").equalsIgnoreCase("Hot")) {
                    leads_details_hot.setBackgroundResource(R.drawable.hot_28);
                    leads_details_hot.setVisibility(View.VISIBLE);
                }
                if (jsonobject.getString("status").equalsIgnoreCase("Warm")) {
                    leads_details_hot.setBackgroundResource(R.drawable.warm_28);
                    leads_details_hot.setVisibility(View.VISIBLE);
                }
                if (jsonobject.getString("status").equalsIgnoreCase("Cold")) {
                    leads_details_hot.setBackgroundResource(R.drawable.cold_28);
                    leads_details_hot.setVisibility(View.VISIBLE);
                }
                if (jsonobject.getString("status").equalsIgnoreCase("Lost")) {
                    leads_details_hot.setBackgroundResource(R.drawable.lost_28);
                    leads_details_hot.setVisibility(View.VISIBLE);
                    lost = true;
                    Invisible();
                }
                if (jsonobject.getString("status").equalsIgnoreCase("Sold")) {
                    leads_details_hot.setBackgroundResource(R.drawable.sold_28);
                    leads_details_hot.setVisibility(View.VISIBLE);
                }
                if (jsonobject.getString("status").equalsIgnoreCase("Visiting")) {
                    leads_details_hot.setBackgroundResource(R.drawable.ic_visiting);
                    leads_details_hot.setVisibility(View.VISIBLE);
                }
                if (jsonobject.getString("status").equalsIgnoreCase("Search")) {
                    leads_details_hot.setBackgroundResource(R.drawable.ic_search_lead);
                    leads_details_hot.setVisibility(View.VISIBLE);
                }
                if (jsonobject.getString("status").equalsIgnoreCase("Not-Interested")) {
                    leads_details_hot.setBackgroundResource(R.drawable.ic_not_intrested);
                    leads_details_hot.setVisibility(View.VISIBLE);
                    lost = true;
                    Invisible();
                }
                if (jsonobject.getString("status").equalsIgnoreCase("Finalised")) {
                    leads_details_hot.setBackgroundResource(R.drawable.ic_finalized);
                    leads_details_hot.setVisibility(View.VISIBLE);
                    lost = true;
                    Invisible();
                }
                if (jsonobject.getString("status").equalsIgnoreCase("No-Response")) {
                    leads_details_hot.setBackgroundResource(R.drawable.ic_no_responce);
                    leads_details_hot.setVisibility(View.VISIBLE);
                }

                updateFollowup = jsonobject.getString("FollowUpDate");

                if (updateFollowup.equals("") || updateFollowup.trim().equals("000000") || updateFollowup.equals("00 0000") || updateFollowup.equals("00  0000")) {
                    updateFollowup = "NA";
                }

                try {
                    String[] splitdatespace = updateFollowup.split(" ", 10);
                    String[] splitdate1 = splitdatespace[0].split("-", 10);
                    updateFollowup = splitdate1[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate1[1])] + " " + splitdate1[0];
                    Log.e("updateFollowup ", "updateFollowup " + updateFollowup);
                    if (updateFollowup.equals("") || updateFollowup.trim().equals("000000") || updateFollowup.equals("00 0000") || updateFollowup.equals("00  0000")) {
                        updateFollowup = "NA";
                    }
                    updateFollowup = "Next Follow-up " + "<br>" + "<font color=\"black\">" + updateFollowup + "</font>" + "</br> ";
                    leads_details_next_followup.setText(Html.fromHtml(updateFollowup));

                } catch (Exception e) {
                    updateFollowup = "Next Follow-up " + "<br>" + "<font color=\"black\">" + updateFollowup + "</font>" + "</br> ";

                    leads_details_next_followup.setText(Html.fromHtml(updateFollowup));
                }


                remarkweb = jsonobject.getString("remark");

                if (remarkweb.equals("NA") || remarkweb.equals("")) {
                    remarkweb = "No comments available";
                    leads_details_message.setText(remarkweb);
                } else {
                    leads_details_message.setText(remarkweb);
                }

                FollowupHistoryFragment.reloadAPI();
            }
        } catch (JSONException e) {

        }

    }

    public static void ParsePrivateUpdateLeadStore(JSONObject jObj, Activity activity) {

        String updateFollowup = "";
        String remarkpri = "";

        Log.e("PrivateUpdateLeadStore ", "jobj " + jObj.toString());

        try {

            JSONArray arrayList = jObj.getJSONArray("data");
            JSONObject jsonobject = new JSONObject(arrayList.getString(0));

            leads_details_hot.setText(jsonobject.getString("lead_status"));

            if (jsonobject.getString("lead_status").equalsIgnoreCase("Open")) {
                leads_details_hot.setBackgroundResource(R.drawable.hot_28);
                leads_details_hot.setVisibility(View.INVISIBLE);
            }
            if (jsonobject.getString("lead_status").equalsIgnoreCase("Hot")) {
                leads_details_hot.setBackgroundResource(R.drawable.hot_28);
                leads_details_hot.setVisibility(View.VISIBLE);
            }
            if (jsonobject.getString("lead_status").equalsIgnoreCase("Warm")) {
                leads_details_hot.setBackgroundResource(R.drawable.warm_28);
                leads_details_hot.setVisibility(View.VISIBLE);
            }
            if (jsonobject.getString("lead_status").equalsIgnoreCase("Cold")) {
                leads_details_hot.setBackgroundResource(R.drawable.cold_28);
                leads_details_hot.setVisibility(View.VISIBLE);

            }
            if (jsonobject.getString("lead_status").equalsIgnoreCase("Lost")) {
                leads_details_hot.setBackgroundResource(R.drawable.lost_28);
                leads_details_hot.setVisibility(View.VISIBLE);
                lost = true;
                Invisible();
            }
            if (jsonobject.getString("lead_status").equalsIgnoreCase("Sold")) {
                leads_details_hot.setBackgroundResource(R.drawable.sold_28);
                leads_details_hot.setVisibility(View.VISIBLE);
            }
            if (jsonobject.getString("lead_status").equalsIgnoreCase("Visiting")) {
                leads_details_hot.setBackgroundResource(R.drawable.ic_visiting);
                leads_details_hot.setVisibility(View.VISIBLE);
            }
            if (jsonobject.getString("lead_status").equalsIgnoreCase("Search")) {
                leads_details_hot.setBackgroundResource(R.drawable.ic_search_lead);
                leads_details_hot.setVisibility(View.VISIBLE);
            }
            if (jsonobject.getString("lead_status").equalsIgnoreCase("Not-Interested")) {
                leads_details_hot.setBackgroundResource(R.drawable.ic_not_intrested);
                leads_details_hot.setVisibility(View.VISIBLE);
                lost = true;
                Invisible();
            }
            if (jsonobject.getString("lead_status").equalsIgnoreCase("Finalised")) {
                leads_details_hot.setBackgroundResource(R.drawable.ic_finalized);
                leads_details_hot.setVisibility(View.VISIBLE);
                lost = true;
                Invisible();
            }
            if (jsonobject.getString("lead_status").equalsIgnoreCase("No-Response")) {
                leads_details_hot.setBackgroundResource(R.drawable.ic_no_responce);
                leads_details_hot.setVisibility(View.VISIBLE);
            }

            updateFollowup = jsonobject.getString("follow_date");

            if (updateFollowup.equals("") || updateFollowup.trim().equals("000000") || updateFollowup.equals("00 0000") || updateFollowup.equals("00  0000")) {
                updateFollowup = "NA";
            }

            try {
                String[] splitdatespace = updateFollowup.split(" ", 10);
                String[] splitdate1 = splitdatespace[0].split("-", 10);
                updateFollowup = splitdate1[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate1[1])] + " " + splitdate1[0];
                Log.e("updateFollowup ", "updateFollowup " + updateFollowup);
                updateFollowup = "Next Follow-up " + "<br>" + "<font color=\"black\">" + updateFollowup + "</font>" + "</br> ";
                leads_details_next_followup.setText(Html.fromHtml(updateFollowup));

            } catch (Exception e) {
                updateFollowup = "Next Follow-up " + "<br>" + "<font color=\"black\">" + updateFollowup + "</font>" + "</br> ";

                leads_details_next_followup.setText(Html.fromHtml(updateFollowup));
            }


            remarkpri = jsonobject.getString("remark");

            if (remarkpri.equals("NA") || remarkpri.equals("")) {
                remarkpri = "No comments available";
                leads_details_message.setText(remarkpri);
            } else {
                leads_details_message.setText(remarkpri);
            }

            FollowupHistoryFragment.reloadAPI();
        } catch (JSONException e) {

        }


    }


    public static void RetroParsePrivateLeadStore(PrivateLeadResponse mPrivateLeadResponse) {

        String updateFollowup = "";
        String remarkpri = "";


        List<PrivateLeadItem> mlist = mPrivateLeadResponse.getData();

        leads_details_hot.setText(mlist.get(0).getLeadStatus());
        if (mlist.get(0).getLeadStatus().equalsIgnoreCase("Open")) {
            leads_details_hot.setBackgroundResource(R.drawable.hot_28);
            leads_details_hot.setVisibility(View.INVISIBLE);
        }
        if (mlist.get(0).getLeadStatus().equalsIgnoreCase("Hot")) {
            leads_details_hot.setBackgroundResource(R.drawable.hot_28);
            leads_details_hot.setVisibility(View.VISIBLE);
        }
        if (mlist.get(0).getLeadStatus().equalsIgnoreCase("Warm")) {
            leads_details_hot.setBackgroundResource(R.drawable.warm_28);
            leads_details_hot.setVisibility(View.VISIBLE);
        }
        if (mlist.get(0).getLeadStatus().equalsIgnoreCase("Cold")) {
            leads_details_hot.setBackgroundResource(R.drawable.cold_28);
            leads_details_hot.setVisibility(View.VISIBLE);

        }
        if (mlist.get(0).getLeadStatus().equalsIgnoreCase("Lost")) {
            leads_details_hot.setBackgroundResource(R.drawable.lost_28);
            leads_details_hot.setVisibility(View.VISIBLE);
            lost = true;
            Invisible();
        }
        if (mlist.get(0).getLeadStatus().equalsIgnoreCase("Sold")) {
            leads_details_hot.setBackgroundResource(R.drawable.sold_28);
            leads_details_hot.setVisibility(View.VISIBLE);
        }
        if (mlist.get(0).getLeadStatus().equalsIgnoreCase("Visiting")) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_visiting);
            leads_details_hot.setVisibility(View.VISIBLE);
        }
        if (mlist.get(0).getLeadStatus().equalsIgnoreCase("Search")) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_search_lead);
            leads_details_hot.setVisibility(View.VISIBLE);
        }
        if (mlist.get(0).getLeadStatus().equalsIgnoreCase("Not-Interested")) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_not_intrested);
            leads_details_hot.setVisibility(View.VISIBLE);
            lost = true;
            Invisible();
        }
        if (mlist.get(0).getLeadStatus().equalsIgnoreCase("Finalised")) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_finalized);
            leads_details_hot.setVisibility(View.VISIBLE);
            lost = true;
            Invisible();
        }
        if (mlist.get(0).getLeadStatus().equalsIgnoreCase("No-Response")) {
            leads_details_hot.setBackgroundResource(R.drawable.ic_no_responce);
            leads_details_hot.setVisibility(View.VISIBLE);
        }
        updateFollowup = mlist.get(0).getFollowDate();

        if (updateFollowup.equals("") || updateFollowup.trim().equals("000000") || updateFollowup.equals("00 0000") || updateFollowup.equals("00  0000")) {
            updateFollowup = "NA";
        }

        try {
            String[] splitdatespace = updateFollowup.split(" ", 10);
            String[] splitdate1 = splitdatespace[0].split("-", 10);
            updateFollowup = splitdate1[2] + " " + MonthUtility.Month[Integer.parseInt(splitdate1[1])] + " " + splitdate1[0];
            Log.e("updateFollowup ", "updateFollowup " + updateFollowup);
            updateFollowup = "Next Follow-up " + "<br>" + "<font color=\"black\">" + updateFollowup + "</font>" + "</br> ";
            leads_details_next_followup.setText(Html.fromHtml(updateFollowup));

        } catch (Exception e) {
            updateFollowup = "Next Follow-up " + "<br>" + "<font color=\"black\">" + updateFollowup + "</font>" + "</br> ";

            leads_details_next_followup.setText(Html.fromHtml(updateFollowup));
        }
        remarkpri = mlist.get(0).getRemark();

        if (remarkpri.equals("NA") || remarkpri.equals("")) {
            remarkpri = "No comments available";
            leads_details_message.setText(remarkpri);
        } else {
            leads_details_message.setText(remarkpri);
        }

        FollowupHistoryFragment.reloadAPI();


    }

    @Override
    public void onResume() {
        super.onResume();
        // put your code here...

        Application.getInstance().trackScreenView(getActivity(), GlobalText.NormalLeadDetails_Fragment);

    }

}


