package com.mfcwl.mfc_dealer.Fragment.Leads;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.Activity.Leads.LeadConstants;
import com.mfcwl.mfc_dealer.Activity.Leads.LeadsInstance;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Activity.SplashActivity;
import com.mfcwl.mfc_dealer.Adapter.Leads.PrivateLeadsAdapter;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Model.Leads.PrivateLeadModel;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchVal;
import static com.mfcwl.mfc_dealer.Fragment.Leads.NormalLeadsFragment.oneTimeSearch;

/**
 * Created by HP 240 G5 on 12-03-2018.
 */

public class PrivateLeadsFragment extends Fragment {


    public static RecyclerView privateLeadsRecy;
    public static TextView noresults;
    public static PrivateLeadsAdapter mPrivateLeadsAdapter;
    public static boolean mPrivate = false;


    public static Activity activity;
    public static Context c;
    public static List<PrivateLeadModel> myPriList;
    public static Button btnAllLeads;
    public static Button btnFollowUpLeads;
    private static String whichLeadsPrivate = "allleads";

    public static int countsPrivate = 1;
    public static int negotiationLeadCountPrivate = 0;
    public static int pageItemPrivate = 100;
    public static String page_noPrivate = "1";
    public static LinearLayoutManager linearLayoutManager;
    int lastVisibleItemPosition = 0, totalItemCount = 0;
    public static SwipeRefreshLayout swipeRefreshLayoutPrivate;
    public boolean Flag1 = false;
    public static int filtercode;

    public static boolean flagRes;
    public static View viewsprivateLeads;
    private static SharedPreferences mSharedPreferences = null;
    String TAG = getClass().getSimpleName();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        // Inflate the layout for this fragment
        mSharedPreferences = getActivity().getSharedPreferences("MFC", Context.MODE_PRIVATE);

        View view = inflater.inflate(R.layout.layout_private_leads, container, false);
        myPriList = new ArrayList<>();
        activity = getActivity();
        c = getContext();
        filtercode = 0;
        countsPrivate = 1;
        negotiationLeadCountPrivate = 0;
        pageItemPrivate = 100;
        page_noPrivate = "1";
        flagRes = true;
        privateLeadsRecy = view.findViewById(R.id.normal_leads_recycler);
        noresults = view.findViewById(R.id.noresults);
        swipeRefreshLayoutPrivate = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayoutPrivate.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.CYAN);
        noresults.setVisibility(View.GONE);
        privateLeadsRecy.setLayoutManager(new WrapContentLinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));

        btnAllLeads = view.findViewById(R.id.btnAllLeads);
        btnFollowUpLeads = view.findViewById(R.id.btnFollowUpLeads);

        CommonMethods.setvalueAgainstKey(activity, "loadPri", "false");

        mPrivateLeadsAdapter = new PrivateLeadsAdapter(c, myPriList, activity);
        privateLeadsRecy.setHasFixedSize(false);
        linearLayoutManager = new LinearLayoutManager(c);
        privateLeadsRecy.setLayoutManager(linearLayoutManager);
        privateLeadsRecy.setAdapter(mPrivateLeadsAdapter);

        //WebServicesCall.webCall(activity, getContext(), jsonMake(), "LeadStore", GlobalText.POST);
        SplashActivity.progress = true;

        swipeRefreshLayoutPrivate.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                SplashActivity.progress = false;
                if (CommonMethods.isInternetWorking(activity)) {
                    if (whichLeadsPrivate.equals("allleads")) {
                        privateLeadsRecy.getRecycledViewPool().clear();
                        mPrivateLeadsAdapter.notifyDataChanged();
                        ShowAllLeads();
                    } else {
                        privateLeadsRecy.getRecycledViewPool().clear();
                        mPrivateLeadsAdapter.notifyDataChanged();
                        ShowFollowUpLeads();
                    }

                }
            }
        });

        btnAllLeads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //NotificationInstance.getInstance().setTodayfollowdate("");
                SplashActivity.progress = true;
                if (mPrivateLeadsAdapter != null) {
                    privateLeadsRecy.getRecycledViewPool().clear();
                    mPrivateLeadsAdapter.notifyDataChanged();
                }
                btnAllLeads.setTextColor(Color.BLACK);
                btnAllLeads.setBackgroundResource(R.drawable.my_button);
                btnFollowUpLeads.setBackgroundResource(R.drawable.my_buttom_gray_light);
                btnFollowUpLeads.setTextColor(Color.parseColor("#b9b9b9"));
                ShowAllLeads();

            }
        });

        btnFollowUpLeads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //NotificationInstance.getInstance().setTodayfollowdate("");
                SplashActivity.progress = true;
                if (mPrivateLeadsAdapter != null) {
                    privateLeadsRecy.getRecycledViewPool().clear();
                    mPrivateLeadsAdapter.notifyDataChanged();
                }

                btnFollowUpLeads.setTextColor(Color.BLACK);
                btnFollowUpLeads.setBackgroundResource(R.drawable.my_button);
                btnAllLeads.setBackgroundResource(R.drawable.my_buttom_gray_light);
                btnAllLeads.setTextColor(Color.parseColor("#b9b9b9"));

                ShowFollowUpLeads();

            }
        });


        RecyclerView.OnScrollListener nRecyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == 2) {
                    try {
                        if (lastVisibleItemPosition + 1 == totalItemCount) {
                            //Write you code here
                            if (!searchVal.equals("")) {

                                countsPrivate = countsPrivate + 1;

                                double maxPageNumber = Math.ceil((double) (negotiationLeadCountPrivate) / (double) pageItemPrivate); // d = 3.0

                                if (countsPrivate > maxPageNumber) {
                                    return;
                                }
                                page_noPrivate = Integer.toString(countsPrivate);
                                SearchingByPrivateLeads();
                                oneTimeSearch = true;
                            } else {

                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();

            }
        };

        privateLeadsRecy.addOnScrollListener(nRecyclerViewOnScrollListener);


        // We need to detect scrolling changes in the RecyclerView
        privateLeadsRecy.setOnScrollListener(new RecyclerView.OnScrollListener() {

            // Keeps track of the overall vertical offset in the list
            int verticalOffset;

            // Determines the scroll UP/DOWN direction
            boolean scrollingUp;


            @Override
            public final void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                verticalOffset += dy;
                scrollingUp = dy > 0;
                viewsprivateLeads = recyclerView;
                if (scrollingUp) {


                    if (Flag1) {

                        MainActivity.bottom_topAnimation();

                        Flag1 = false;

                    }
                } else {


                    if (!Flag1) {

                        MainActivity.top_bottomAnimation();

                        Flag1 = true;

                    }

                }
            }
        });

        return view;

    }


    public static void loadmore(int count) {

        // LoginActivity.progress = false;
        countsPrivate = countsPrivate + 1;

        double maxPageNumber = Math.ceil((double) (negotiationLeadCountPrivate) / (double) pageItemPrivate); // d = 3.0
        if (countsPrivate > maxPageNumber) {
            // PrivateLeadsAdapter.progress.setVisibility(View.GONE);
            /*Snackbar.make(viewsprivateLeads, "End of results", Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();*/
            //ShowSnackBar("End of results");
            return;
        }
        // PrivateLeadsAdapter.progress.setVisibility(View.GONE);
        page_noPrivate = Integer.toString(countsPrivate);

        CommonMethods.setvalueAgainstKey(activity, "loadPri", "true");
        if (CommonMethods.isInternetWorking(activity)) {

            if (CommonMethods.getstringvaluefromkey(activity, "moredata").equalsIgnoreCase("moredata")) {
                /*Snackbar.make(viewsprivateLeads, "Scroll down to see more...", Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show();*/
                //ShowSnackBar("Scroll down to see more...");
                WebServicesCall.webCall(activity, activity, jsonMake(), "PrivateLeadStore", GlobalText.POST);
            } else {

                Toast toast = Toast.makeText(activity, "No More Data.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }

    }

    private static void ShowSnackBar(String msg) {


        try {


            Snackbar snackbar = Snackbar
                    .make(viewsprivateLeads, msg, Snackbar.LENGTH_SHORT)
                    .setAction("", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                        }
                    });


            View sbView = snackbar.getView();
            TextView textView = sbView.findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            } else {
                textView.setGravity(Gravity.CENTER_HORIZONTAL);
            }
            snackbar.show();// Changing message text color
        } catch (Exception e) {
            Log.e("Exception ", "Exception " + e.toString());
        }


    }


    private void ShowAllLeads() {
        if (myPriList != null) {
            myPriList.clear();

            /*privateLeadsRecy.getRecycledViewPool().clear();
            mPrivateLeadsAdapter.notifyDataChanged();*/
        }


        countsPrivate = 1;
        negotiationLeadCountPrivate = 0;
        pageItemPrivate = 100;
        page_noPrivate = "1";
        whichLeadsPrivate = "allleads";

        SortByPrivateLeads();

    }

    private void ShowFollowUpLeads() {
        if (myPriList != null) {
            myPriList.clear();

            /*privateLeadsRecy.getRecycledViewPool().clear();
            mPrivateLeadsAdapter.notifyDataChanged();*/
        }

        countsPrivate = 1;
        negotiationLeadCountPrivate = 0;
        pageItemPrivate = 100;
        page_noPrivate = "1";
        whichLeadsPrivate = "followleads";

        SortByPrivateLeads();
    }

    public static JSONObject jsonMake() {

        JSONObject jObj = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONArray jsondateArray = new JSONArray();
        JSONObject jObj1 = new JSONObject();

        JSONObject jObj2 = new JSONObject();
        JSONObject jObj3 = new JSONObject();

        JSONObject jObj4 = new JSONObject();
        JSONObject jObj5 = new JSONObject();

        JSONObject jObj6 = new JSONObject();
        JSONObject jObj7 = new JSONObject();

        JSONObject jObj8 = new JSONObject();
        JSONObject jObj9 = new JSONObject();

        JSONObject jObj10 = new JSONObject();
        JSONObject jObjleads = new JSONObject();
        JSONObject jObjcode = new JSONObject();

        JSONObject jObj11 = new JSONObject();

        JSONObject jObj12 = new JSONObject();
        JSONObject jObj13 = new JSONObject();


        JSONObject jObjmake = new JSONObject();
        JSONObject jObjmodel = new JSONObject();
        JSONObject jObjvariant = new JSONObject();
        JSONObject jObjname = new JSONObject();
        JSONObject jObjmobile = new JSONObject();

        JSONArray jsonSearchVal = new JSONArray();

        JSONArray statusArray = new JSONArray();

        JSONArray ltypeArray = new JSONArray();
        JSONArray codeArray = new JSONArray();

        statusArray.put("");
        statusArray.put("HOT");
        statusArray.put("WARM");
        statusArray.put("OPEN");
        statusArray.put("COLD");
        /*statusArray.put("LOST");
        statusArray.put("SOLD");*/


        if (!searchVal.equalsIgnoreCase("")) {
            swipeRefreshLayoutPrivate.setEnabled(false);
        }

        String postdateinfo = "";

        try {
            if (mSharedPreferences.getString(LeadConstants.LEAD_POST_TODAY, "") != "") {
                postdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_POST_TODAY, "");
            }

            if (mSharedPreferences.getString(LeadConstants.LEAD_POST_YESTER, "") != "") {
                postdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_POST_YESTER, "");
            }


            if (mSharedPreferences.getString(LeadConstants.LEAD_POST_7DAYS, "") != "") {
                postdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_POST_7DAYS, "");
            }

            if (mSharedPreferences.getString(LeadConstants.LEAD_POST_15DAYS, "") != "") {
                postdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_POST_15DAYS, "");
            }

            if (mSharedPreferences.getString(LeadConstants.LEAD_POST_CUSTDATE, "") != "") {
                postdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_POST_CUSTDATE, "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (postdateinfo != null && postdateinfo.contains("@")) {
            //split the date and atach to query
            String[] createddates = postdateinfo.split("@");

            try {


                if (mSharedPreferences.getString(LeadConstants.LEAD_POST_YESTER, "") != "") {
                    jObj2.put("column", "created_at");
                    jObj2.put("operator", ">=");
                    jObj3.put("operator", "<");
                } else {
                    jObj2.put("column", "created_at");
                    jObj2.put("operator", ">=");
                    jObj3.put("operator", "<=");
                }

                jObj2.put("value", createddates[0]);
                jObj3.put("column", "created_at");
                jObj3.put("value", createddates[1]);
                jsondateArray.put(jObj2);
                jsondateArray.put(jObj3);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        String followdateinfo = "";

        try {
            if (mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_TMRW, "") != "") {
                followdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_TMRW, "");
            }

            if (mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_TODAY, "") != "") {
                followdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_TODAY, "");
            }
            if (mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_YESTER, "") != "") {
                followdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_YESTER, "");
            }

            if (mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_7DAYS, "") != "") {
                followdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_7DAYS, "");
            }

            if (mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_CUSTDATE, "") != "") {
                followdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_CUSTDATE, "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {

        if (followdateinfo != null && followdateinfo.contains("@")) {

            String[] follow = followdateinfo.split("@");
            try {


                if (mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_TMRW, "") != "" || mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_TODAY, "") != "" || mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_YESTER, "") != "") {
                    jObj4.put("column", "follow_date");
                    jObj4.put("operator", "=");
                    jObj4.put("value", follow[0]);
                    jsondateArray.put(jObj4);
                } else {
                    jObj4.put("column", "follow_date");
                    jObj4.put("operator", ">=");
                    jObj5.put("operator", "<=");
                    jObj4.put("value", follow[0]);
                    jObj5.put("column", "follow_date");
                    jObj5.put("value", follow[1]);
                    jsondateArray.put(jObj4);
                    jsondateArray.put(jObj5);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        }else{

            try {

                String startDate = CommonMethods.getstringvaluefromkey(activity, "startDate");
                String endDate = CommonMethods.getstringvaluefromkey(activity, "endDate");

                if(!startDate.equalsIgnoreCase("")
                &&!endDate.equalsIgnoreCase("")) {

                    jObj4.put("column", "created_at");

                    jObj4.put("operator", ">=");
                    jObj5.put("operator", "<=");


                    jObj4.put("value", startDate);
                    jObj5.put("column", "created_at");
                    jObj5.put("value", endDate);

                    jsondateArray.put(jObj4);
                    jsondateArray.put(jObj5);

                    jObj.put("where", jsondateArray);
                }

            }catch (Exception e){
                e.printStackTrace();
            }

        }


        try {
            jObj.put("PageItems", Integer.toString(pageItemPrivate));
            jObj.put("Page", page_noPrivate);

            if (whichLeadsPrivate.equals("followleads")) {


                if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {

                    jObj1.put("column", "lead_status");
                    if (LeadFilterFragmentNew.leadstatusJsonArray.length() != 0) {

                        jObj1.put("values", LeadFilterFragmentNew.leadstatusJsonArray);
                    } else {
                        jObj1.put("values", statusArray);
                    }
                    jsonArray.put(jObj1);
                    jObj.put("wherein", jsonArray);
                }else {
                    if (CommonMethods.getstringvaluefromkey(activity, "leadStatus").equalsIgnoreCase("true")) {
                        //remove if already presents in json arry
                        if (LeadFilterFragmentNew.leadstatusJsonArray.length() != 0) {
                            try {
                                for (int i = 0; i < LeadFilterFragmentNew.leadstatusJsonArray.length(); i++) {
                                    LeadFilterFragmentNew.leadstatusJsonArray.remove(i);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        LeadFilterFragmentNew.leadstatusJsonArray.put(CommonMethods.getstringvaluefromkey(activity, "Lvalue"));

                        jObj1.put("column", "lead_status");
                        if (LeadFilterFragmentNew.leadstatusJsonArray.length() != 0) {

                            jObj1.put("values", LeadFilterFragmentNew.leadstatusJsonArray);
                        } else {
                            jObj1.put("values", statusArray);
                        }
                        jsonArray.put(jObj1);
                        jObj.put("wherein", jsonArray);

                    }else{
                        jObj1.put("column", "lead_status");
                        if (LeadFilterFragmentNew.leadstatusJsonArray.length() != 0) {

                            jObj1.put("values", LeadFilterFragmentNew.leadstatusJsonArray);
                        } else {
                            jObj1.put("values", statusArray);
                        }
                        jsonArray.put(jObj1);
                        jObj.put("wherein", jsonArray);
                    }
                }

            } else {

                if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {

                    jObj10.put("column", "lead_status");
                    if (LeadFilterFragmentNew.leadstatusJsonArray.length() != 0) {


                        jObj10.put("values", LeadFilterFragmentNew.leadstatusJsonArray);
                        jsonArray.put(jObj10);
                        jObj.put("wherein", jsonArray);


                    } else {
                        jObj.put("wherein", jsonArray);
                    }




                }else{


                        if (LeadFilterFragmentNew.leadstatusJsonArray.length() != 0) {
                            try {
                                for (int i = 0; i < LeadFilterFragmentNew.leadstatusJsonArray.length(); i++) {
                                    LeadFilterFragmentNew.leadstatusJsonArray.remove(i);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        LeadFilterFragmentNew.leadstatusJsonArray.put(CommonMethods.getstringvaluefromkey(activity, "Lvalue"));

                    if(!CommonMethods.getstringvaluefromkey(activity, "Lvalue").equalsIgnoreCase("")) {
                        jObj1.put("column", "lead_status");
                        if (LeadFilterFragmentNew.leadstatusJsonArray.length() != 0) {

                            jObj1.put("values", LeadFilterFragmentNew.leadstatusJsonArray);
                        } else {
                            jObj1.put("values", statusArray);
                        }
                        jsonArray.put(jObj1);


                    }

                    jObj.put("wherein", jsonArray);
                }


                //

                // lead types
                ltypeArray.put("SALES");
                jObjleads.put("column", "lead_type");
                jObjleads.put("values", ltypeArray);
                jsonArray.put(jObjleads);
                jObj.put("wherein", jsonArray);

                // lead types
                codeArray.put(CommonMethods.getstringvaluefromkey(activity, "dealer_code"));
                jObjcode.put("column", "dealer_id");
                jObjcode.put("values", codeArray);
                jsonArray.put(jObjcode);
                jObj.put("wherein", jsonArray);

            }


            if (LeadSortCustomDialogClass.leadsortbystatus.equalsIgnoreCase("posted_date_l_o")) {
                jObj.put("OrderBy", "created_at");
                jObj.put("OrderByReverse", "true");
            } else if (LeadSortCustomDialogClass.leadsortbystatus.equalsIgnoreCase("posted_date_o_l")) {
                jObj.put("OrderBy", "created_at");
                jObj.put("OrderByReverse", "false");
            } else if (LeadSortCustomDialogClass.leadsortbystatus.equalsIgnoreCase("folowup_date_l_o")) {
                jObj.put("OrderBy", "follow_date");
                jObj.put("OrderByReverse", "true");


            } else if (LeadSortCustomDialogClass.leadsortbystatus.equalsIgnoreCase("folowup_date_o_l")) {

                jObj.put("OrderBy", "follow_date");
                jObj.put("OrderByReverse", "false");
            } else {
                jObj.put("OrderBy", "created_at");
                jObj.put("OrderByReverse", "true");
            }


            if (whichLeadsPrivate.equals("followleads")) {
                Date d = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(d);
                jObj11.put("column", "follow_date");
                jObj11.put("operator", "<=");
                jObj11.put("value", formattedDate);
                jsondateArray.put(jObj11);
            }


            if (!searchVal.equals("")) {
                jObjmake.put("column", "make");
                jObjmake.put("operator", "like");
                jObjmake.put("value", searchVal.trim());
                jsonSearchVal.put(jObjmake);

                jObjmodel.put("column", "model");
                jObjmodel.put("operator", "like");
                jObjmodel.put("value", searchVal.trim());
                jsonSearchVal.put(jObjmodel);

                jObjvariant.put("column", "variant");
                jObjvariant.put("operator", "like");
                jObjvariant.put("value", searchVal.trim());
                jsonSearchVal.put(jObjvariant);

                jObjname.put("column", "customer_name");
                jObjname.put("operator", "like");
                jObjname.put("value", searchVal.trim());
                jsonSearchVal.put(jObjname);

                jObjmobile.put("column", "customer_mobile");
                jObjmobile.put("operator", "like");
                jObjmobile.put("value", searchVal.trim());
                jsonSearchVal.put(jObjmobile);
            }

            jObj.put("where_or", jsonSearchVal);


            jObj.put("where", jsondateArray);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Log.e("ParseleadstorePrivate ", "Request " + jObj.toString());

        return jObj;
    }

    /*public static JSONObject jsonMake() {

        JSONObject jObj = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONArray jsondateArray = new JSONArray();
        JSONObject jObj1 = new JSONObject();

        JSONObject jObj2 = new JSONObject();
        JSONObject jObj3 = new JSONObject();

        JSONObject jObj4 = new JSONObject();
        JSONObject jObj5 = new JSONObject();

        JSONObject jObj6 = new JSONObject();
        JSONObject jObj7 = new JSONObject();

        JSONObject jObj8 = new JSONObject();
        JSONObject jObj9 = new JSONObject();

        JSONObject jObj10 = new JSONObject();

        JSONObject jObj11 = new JSONObject();

        JSONObject jObj12 = new JSONObject();
        JSONObject jObj13 = new JSONObject();


        JSONObject jObjmake = new JSONObject();
        JSONObject jObjmodel = new JSONObject();
        JSONObject jObjvariant = new JSONObject();
        JSONObject jObjname = new JSONObject();
        JSONObject jObjmobile = new JSONObject();

        JSONArray jsonSearchVal = new JSONArray();

        JSONArray statusArray = new JSONArray();
        statusArray.put("");
        statusArray.put("HOT");
        statusArray.put("WARM");
        statusArray.put("OPEN");
        statusArray.put("COLD");
        *//*statusArray.put("LOST");
        statusArray.put("SOLD");*//*


        if (!searchVal.equalsIgnoreCase("")) {
            swipeRefreshLayoutPrivate.setEnabled(false);
        }

        try {
            jObj.put("PageItems", Integer.toString(pageItemPrivate));
            jObj.put("Page", page_noPrivate);

            if (whichLeadsPrivate.equals("followleads")) {
                jObj1.put("column", "lead_status");
                if (LeadFilterFragment.leadstatusJsonArray.length() != 0) {

                    jObj1.put("values", LeadFilterFragment.leadstatusJsonArray);
                } else {
                    jObj1.put("values", statusArray);
                }
                jsonArray.put(jObj1);
                jObj.put("wherein", jsonArray);

            } else {

                jObj10.put("column", "lead_status");
                if (LeadFilterFragment.leadstatusJsonArray.length() != 0) {
                    jObj10.put("values", LeadFilterFragment.leadstatusJsonArray);
                    jsonArray.put(jObj10);
                    jObj.put("wherein", jsonArray);

                } else {
                    jObj.put("wherein", jsonArray);
                }

            }

            if (LeadSortCustomDialogClass.leadsortbystatus.equalsIgnoreCase("posted_date_l_o")) {
                jObj.put("OrderBy", "created_at");
                jObj.put("OrderByReverse", "true");
            } else if (LeadSortCustomDialogClass.leadsortbystatus.equalsIgnoreCase("posted_date_o_l")) {
                jObj.put("OrderBy", "created_at");
                jObj.put("OrderByReverse", "false");
            } else if (LeadSortCustomDialogClass.leadsortbystatus.equalsIgnoreCase("folowup_date_l_o")) {
                jObj.put("OrderBy", "follow_date");
                jObj.put("OrderByReverse", "true");


            } else if (LeadSortCustomDialogClass.leadsortbystatus.equalsIgnoreCase("folowup_date_o_l")) {

                jObj.put("OrderBy", "follow_date");
                jObj.put("OrderByReverse", "false");
            } else {
                jObj.put("OrderBy", "created_at");
                jObj.put("OrderByReverse", "true");
            }

            if (!LeadFilterFragment.postFromDate.equals("") && !LeadFilterFragment.postToDate.equals("")) {
                jObj2.put("column", "created_at");
                jObj2.put("operator", ">=");
                jObj2.put("value", LeadFilterFragment.postFromDate);
                jObj3.put("column", "created_at");
                jObj3.put("operator", "<=");
                jObj3.put("value", LeadFilterFragment.postToDate);
                jsondateArray.put(jObj2);
                jsondateArray.put(jObj3);
            }

            if (!LeadFilterFragment.followFromDate.equals("") && !LeadFilterFragment.followToDate.equals("")) {
                jObj4.put("column", "follow_date");
                jObj4.put("operator", ">=");
                jObj4.put("value", LeadFilterFragment.followFromDate);
                jObj5.put("column", "follow_date");
                jObj5.put("operator", "<=");
                jObj5.put("value", LeadFilterFragment.followToDate);
                jsondateArray.put(jObj4);
                jsondateArray.put(jObj5);
                NotificationInstance.getInstance().setTodayfollowdate("");
            }

            if (!LeadFilterFragment.postTodYesLastFromDate.equals("") && !LeadFilterFragment.postTodYesLastToDate.equals("")) {
                jObj6.put("column", "created_at");
                jObj6.put("operator", ">=");
                jObj6.put("value", LeadFilterFragment.postTodYesLastFromDate);
                jObj7.put("column", "created_at");
                if (!mInstance.getPchoosecustomdatefrom().equals("") && !mInstance.getPchoosecustomdateto().equals("")) {
                    jObj7.put("operator", "<=");
                } else {
                    jObj7.put("operator", "<");
                }
                jObj7.put("value", LeadFilterFragment.postTodYesLastToDate);
                jsondateArray.put(jObj6);
                jsondateArray.put(jObj7);


            }

            if (!LeadFilterFragment.followTodYesLastFromDate.equals("") && !LeadFilterFragment.followTodYesLastToDate.equals("")) {
                jObj8.put("column", "follow_date");
                jObj8.put("operator", ">=");
                jObj8.put("value", LeadFilterFragment.followTodYesLastFromDate);
                jObj9.put("column", "follow_date");
                //if (!FilterInstance.getInstance().getFchoosecustomdatefrom().equals("") && !FilterInstance.getInstance().getFchoosecustomdateto().equals("")) {
                //changes this line
                if (!mInstance.getFchoosecustomdatefrom().equals("") && !mInstance.getFchoosecustomdateto().equals("")) {
                    jObj9.put("operator", "<=");
                } else {
                    jObj9.put("operator", "<");
                }

                jObj9.put("value", LeadFilterFragment.followTodYesLastToDate);
                jsondateArray.put(jObj8);
                jsondateArray.put(jObj9);

                NotificationInstance.getInstance().setTodayfollowdate("");
            }

            if (!followTodayDate.equals("") && !followTomorowdate.equals("")) {
                jObj12.put("column", "follow_date");
                jObj12.put("operator", ">");
                jObj12.put("value", LeadFilterFragment.followTomorowdate);
                jObj13.put("column", "follow_date");
                jObj13.put("operator", "<=");
                jObj13.put("value", LeadFilterFragment.followTodayDate);
                jsondateArray.put(jObj12);
                jsondateArray.put(jObj13);

            }

            if (!NotificationInstance.getInstance().getTodayfollowdate().equals("")) {
                String[] splitnotDate = NotificationInstance.getInstance().getTodayfollowdate().split("@", 10);

                jObj8.put("column", "follow_date");
                jObj8.put("operator", ">=");
                jObj8.put("value", splitnotDate[0]);
                jObj9.put("column", "follow_date");
                jObj9.put("operator", "<");
                jObj9.put("value", splitnotDate[1]);
                jsondateArray.put(jObj8);
                jsondateArray.put(jObj9);


            }


            if (whichLeadsPrivate.equals("followleads")) {
                Date d = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(d);
                jObj11.put("column", "follow_date");
                jObj11.put("operator", "<=");
                jObj11.put("value", formattedDate);
                jsondateArray.put(jObj11);
            }


            if (!searchVal.equals("")) {
                jObjmake.put("column", "make");
                jObjmake.put("operator", "like");
                jObjmake.put("value", searchVal.trim());
                jsonSearchVal.put(jObjmake);

                jObjmodel.put("column", "model");
                jObjmodel.put("operator", "like");
                jObjmodel.put("value", searchVal.trim());
                jsonSearchVal.put(jObjmodel);

                jObjvariant.put("column", "variant");
                jObjvariant.put("operator", "like");
                jObjvariant.put("value", searchVal.trim());
                jsonSearchVal.put(jObjvariant);

                jObjname.put("column", "customer_name");
                jObjname.put("operator", "like");
                jObjname.put("value", searchVal.trim());
                jsonSearchVal.put(jObjname);

                jObjmobile.put("column", "customer_mobile");
                jObjmobile.put("operator", "like");
                jObjmobile.put("value", searchVal.trim());
                jsonSearchVal.put(jObjmobile);
            }

            jObj.put("where_or", jsonSearchVal);


            jObj.put("where", jsondateArray);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("ParseleadstorePrivate ", "Request " + jObj.toString());

        return jObj;
    }*/


    public static void Parseprivateleadstore(JSONObject jObj, Activity activity) {

        swipeRefreshLayoutPrivate.setRefreshing(false);
        Log.e("ParseleadstorePrivate ", "Response " + jObj.toString());

        if (CommonMethods.getstringvaluefromkey(activity, "loadPri").equalsIgnoreCase("true")) {
            mPrivateLeadsAdapter.notifyItemInserted(myPriList.size() - 1);

        }

        if (CommonMethods.getstringvaluefromkey(activity, "loadPri").equalsIgnoreCase("false")) {
            myPriList = new ArrayList<>();
        } else {
            if ((myPriList.size() - 1) > 0) {
                // myJobList.remove(myJobList.size()-1);
            }

        }


        try {

            JSONArray arrayList = jObj.getJSONArray("data");
            for (int i = 0; i < arrayList.length(); i++) {


                JSONObject jsonobject = new JSONObject(arrayList.getString(i));

                if (whichLeadsPrivate.equals("allleads")) {
                    PrivateLeadModel normalLeadModel = new PrivateLeadModel(jsonobject.getString("id"),
                            jsonobject.getString("lead_type"),
                            jsonobject.getString("lead_title"),
                            jsonobject.getString("lead_date"),
                            jsonobject.getString("lead_category"),
                            jsonobject.getString("lead_tags"),
                            jsonobject.getString("lead_flag"),
                            jsonobject.getString("customer_id"),
                            jsonobject.getString("customer_prefix"),
                            jsonobject.getString("customer_name"),
                            jsonobject.getString("customer_mobile"),
                            jsonobject.getString("customer_email"),
                            jsonobject.getString("customer_comments"),
                            jsonobject.getString("customer_xtras"),
                            jsonobject.getString("customer_prefs_communication"),
                            jsonobject.getString("area_code"),
                            jsonobject.getString("city"),
                            jsonobject.getString("state"),
                            jsonobject.getString("area_mfc_branch"),
                            jsonobject.getString("area_pincode"),
                            jsonobject.getString("stock_id"),
                            jsonobject.getString("stock_type"),
                            jsonobject.getString("make"),
                            jsonobject.getString("model"),
                            jsonobject.getString("variant"),
                            jsonobject.getString("stock_model_year"),
                            jsonobject.getString("register_month"),
                            jsonobject.getString("register_year"),
                            jsonobject.getString("color"),
                            jsonobject.getString("kms"),
                            jsonobject.getString("owner"),
                            jsonobject.getString("register_no"),
                            jsonobject.getString("register_city"),
                            jsonobject.getString("stock_vinno"),
                            jsonobject.getString("insurance_type"),
                            jsonobject.getString("stock_insurance"),
                            jsonobject.getString("stock_expected_price"),
                            jsonobject.getString("dealer_id"),
                            jsonobject.getString("demand_for_dealer"),
                            jsonobject.getString("dealer_quote"),
                            jsonobject.getString("dealer_comments"),
                            jsonobject.getString("lead_stage"),
                            jsonobject.getString("lead_priority"),
                            jsonobject.getString("lead_status"),
                            jsonobject.getString("lead_source"),
                            jsonobject.getString("marketting_source"),
                            jsonobject.getString("marketting_campaign"),
                            jsonobject.getString("marketting_device"),
                            jsonobject.getString("client_ip"),
                            jsonobject.getString("follow_date"),
                            jsonobject.getString("remark"),
                            jsonobject.getString("associated_lead"),
                            jsonobject.getString("ref_id"),
                            jsonobject.getString("ref_src"),
                            jsonobject.getString("status"),
                            jsonobject.getString("comments"),
                            jsonobject.getString("created_by"),
                            jsonobject.getString("updated_by"),
                            jsonobject.getString("deleted_at"),
                            jsonobject.getString("created_at"),
                            jsonobject.getString("updated_at"),
                            jsonobject.getString("manufacturing_month"),
                            jsonobject.getString("executive_name"),
                            jsonobject.getString("executive_id"),
                            jsonobject.getString("Vehicle_Type"),
                            jsonobject.getString("lead_date2"),
                            jsonobject.getString("created_by_device"),
                            jsonobject.getString("updated_by_device"));

                    myPriList.add(normalLeadModel);

                } else {

                    PrivateLeadModel normalLeadModel = new PrivateLeadModel(jsonobject.getString("id"),
                            jsonobject.getString("lead_type"),
                            jsonobject.getString("lead_title"),
                            jsonobject.getString("lead_date"),
                            jsonobject.getString("lead_category"),
                            jsonobject.getString("lead_tags"),
                            jsonobject.getString("lead_flag"),
                            jsonobject.getString("customer_id"),
                            jsonobject.getString("customer_prefix"),
                            jsonobject.getString("customer_name"),
                            jsonobject.getString("customer_mobile"),
                            jsonobject.getString("customer_email"),
                            jsonobject.getString("customer_comments"),
                            jsonobject.getString("customer_xtras"),
                            jsonobject.getString("customer_prefs_communication"),
                            jsonobject.getString("area_code"),
                            jsonobject.getString("city"),
                            jsonobject.getString("state"),
                            jsonobject.getString("area_mfc_branch"),
                            jsonobject.getString("area_pincode"),
                            jsonobject.getString("stock_id"),
                            jsonobject.getString("stock_type"),
                            jsonobject.getString("make"),
                            jsonobject.getString("model"),
                            jsonobject.getString("variant"),
                            jsonobject.getString("stock_model_year"),
                            jsonobject.getString("register_month"),
                            jsonobject.getString("register_year"),
                            jsonobject.getString("color"),
                            jsonobject.getString("kms"),
                            jsonobject.getString("owner"),
                            jsonobject.getString("register_no"),
                            jsonobject.getString("register_city"),
                            jsonobject.getString("stock_vinno"),
                            jsonobject.getString("insurance_type"),
                            jsonobject.getString("stock_insurance"),
                            jsonobject.getString("stock_expected_price"),
                            jsonobject.getString("dealer_id"),
                            jsonobject.getString("demand_for_dealer"),
                            jsonobject.getString("dealer_quote"),
                            jsonobject.getString("dealer_comments"),
                            jsonobject.getString("lead_stage"),
                            jsonobject.getString("lead_priority"),
                            jsonobject.getString("lead_status"),
                            jsonobject.getString("lead_source"),
                            jsonobject.getString("marketting_source"),
                            jsonobject.getString("marketting_campaign"),
                            jsonobject.getString("marketting_device"),
                            jsonobject.getString("client_ip"),
                            jsonobject.getString("follow_date"),
                            jsonobject.getString("remark"),
                            jsonobject.getString("associated_lead"),
                            jsonobject.getString("ref_id"),
                            jsonobject.getString("ref_src"),
                            jsonobject.getString("status"),
                            jsonobject.getString("comments"),
                            jsonobject.getString("created_by"),
                            jsonobject.getString("updated_by"),
                            jsonobject.getString("deleted_at"),
                            jsonobject.getString("created_at"),
                            jsonobject.getString("updated_at"),
                            jsonobject.getString("manufacturing_month"),
                            jsonobject.getString("executive_name"),
                            jsonobject.getString("executive_id"),
                            jsonobject.getString("Vehicle_Type"),
                            jsonobject.getString("lead_date2"),
                            jsonobject.getString("created_by_device"),
                            jsonobject.getString("updated_by_device"));
                    myPriList.add(normalLeadModel);

                }

            }

            negotiationLeadCountPrivate = Integer.parseInt(jObj.getString("total").toString());

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("ParseleadstorePrivate ", "JSONException " + e.toString());
        }

        CommonMethods.setvalueAgainstKey(activity, "moredata", "moredata");

        if (CommonMethods.getstringvaluefromkey(activity, "loadPri").equalsIgnoreCase("true")) {
            mPrivateLeadsAdapter.notifyDataChanged();

        } else {
            CommonMethods.setvalueAgainstKey(activity, "loadPri", "true");
            reloadData();
        }

        if (negotiationLeadCountPrivate == 0) {
            noresults.setVisibility(View.VISIBLE);
            swipeRefreshLayoutPrivate.setVisibility(View.GONE);
        } else {
            noresults.setVisibility(View.GONE);
            swipeRefreshLayoutPrivate.setVisibility(View.VISIBLE);
        }


    }

    public static void reloadData() {

        mPrivateLeadsAdapter = new PrivateLeadsAdapter(c, myPriList, activity);
        privateLeadsRecy.setHasFixedSize(false);
        linearLayoutManager = new LinearLayoutManager(c);
        privateLeadsRecy.setLayoutManager(linearLayoutManager);
        privateLeadsRecy.setAdapter(mPrivateLeadsAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();

        Application.getInstance().trackScreenView(getActivity(),GlobalText.PrivateLead_Fragment);

        if (whichLeadsPrivate.equals("allleads")) {
            btnAllLeads.setTextColor(Color.BLACK);
            btnAllLeads.setBackgroundResource(R.drawable.my_button);
            btnFollowUpLeads.setBackgroundResource(R.drawable.my_buttom_gray_light);
            btnFollowUpLeads.setTextColor(Color.parseColor("#b9b9b9"));
        } else {
            btnFollowUpLeads.setTextColor(Color.BLACK);
            btnFollowUpLeads.setBackgroundResource(R.drawable.my_button);
            btnAllLeads.setBackgroundResource(R.drawable.my_buttom_gray_light);
            btnAllLeads.setTextColor(Color.parseColor("#b9b9b9"));
        }

        if (mPrivateLeadsAdapter != null) {
            privateLeadsRecy.getRecycledViewPool().clear();
            privateLeadsRecy.stopScroll();
            mPrivateLeadsAdapter.notifyDataChanged();
        }


        if (flagRes == true) {

            if (!mPrivate) {

                if (LeadsInstance.getInstance().getWhichleads().equals("PrivateLeads")) {


                    countsPrivate = 1;
                    negotiationLeadCountPrivate = 0;
                    pageItemPrivate = 100;
                    page_noPrivate = "1";

                    WebServicesCall.webCall(activity, activity, jsonMake(), "PrivateLeadStore", GlobalText.POST);

                }
            } else {
                mPrivate = false;
                //reloadData();
                if (mPrivateLeadsAdapter != null) {
                    mPrivateLeadsAdapter.notifyDataChanged();
                }
            }
            flagRes = false;
        }
    }


    public static void SortByPrivateLeads() {

        if (myPriList != null) {
            myPriList.clear();
        }
        countsPrivate = 1;
        negotiationLeadCountPrivate = 0;
        pageItemPrivate = 100;
        page_noPrivate = "1";

        WebServicesCall.webCall(activity, activity, jsonMake(), "PrivateLeadStore", GlobalText.POST);

    }


    public class WrapContentLinearLayoutManager extends LinearLayoutManager {
        public WrapContentLinearLayoutManager(Activity activity, int horizontal, boolean b) {
            super(activity);
        }

        //... constructor
        @Override
        public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
            try {
                super.onLayoutChildren(recycler, state);
            } catch (IndexOutOfBoundsException e) {
            }
        }
    }

    public static void onSerarchResultUpdate(String query) {
        if (mPrivateLeadsAdapter != null) {
            mPrivateLeadsAdapter.filter(query);
        }
        searchVal = query;
        if (searchVal.equals("")) {
            CommonMethods.setvalueAgainstKey(activity, "loadPri", "false");
            SearchingByPrivateLeads();
        }
        if (myPriList.size() <= 2) {
            if (oneTimeSearch == true) {
                oneTimeSearch = false;
                CommonMethods.setvalueAgainstKey(activity, "loadPri", "false");
                SearchingByPrivateLeads();
            }
        }

    }


    public static void SearchingByPrivateLeads() {

        if (myPriList != null) {
            myPriList.clear();
        }
        countsPrivate = 1;
        negotiationLeadCountPrivate = 0;
        if (!searchVal.equals("")) {
            pageItemPrivate = pageItemPrivate + 10;
        } else {
            pageItemPrivate = 100;
        }
        page_noPrivate = "1";

        WebServicesCall.webCall(activity, activity, jsonMake(), "PrivateLeadStore", GlobalText.POST);

    }


}


