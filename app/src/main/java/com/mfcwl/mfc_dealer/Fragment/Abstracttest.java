package com.mfcwl.mfc_dealer.Fragment;

import android.content.Context;
import android.util.Log;

import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter;
import com.mfcwl.mfc_dealer.videoAppSpecific.SqlAdapterForDML;

import java.util.List;

abstract class Abstracttest {

    static final String NO_INTERNET_MESSAGE = "No Internet Connection Detected... giving up.";
    private static final String TAG = Abstracttest.class.getSimpleName();

    protected final AISQLLiteAdapter dbAdapter = AISQLLiteAdapter.getInstance();
    protected final SqlAdapterForDML dbForDML = SqlAdapterForDML.getInstance();

    protected final Context context;

    Abstracttest() {
        if(null == Application.getInstance()) {
            new Application();
        }
        context = Application.getInstance().getApplicationContext();
    }

        public void uploadAllPendingMedia() {
        try {
            if (CommonMethods.isConnectingToInternet(null)) {
                final List<String> pending = getAllLeadsForPendingMediaUpload();
                Log.d(TAG, "Number of pending media to upload =" + pending.size());
                for (final String lead : pending) {
                    uploadMediaForSpecificLead(lead);
                }
            } else {
                Log.d(TAG, NO_INTERNET_MESSAGE);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception in uploadAllPendingMedia " + e.getMessage());
        }
    }

    abstract protected List<String> getAllLeadsForPendingMediaUpload();
    abstract protected void uploadMediaForSpecificLead(final String lead);

}
