package com.mfcwl.mfc_dealer.Fragment;


import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

/**
 * Created by sowmya on 4/5/18.
 */

public class ToolIbbPrivate extends Fragment {
    String tradeinFairprice, tradeinBestprice, tradeinMarketprice;

    String TAG = getClass().getSimpleName();
    public ToolIbbPrivate() {
    }


    @SuppressLint("ValidFragment")
    public ToolIbbPrivate(String tradeinFairprice, String tradeinBestprice, String tradeinMarketprice) {
        this.tradeinFairprice = tradeinFairprice;
        this.tradeinBestprice = tradeinBestprice;
        this.tradeinMarketprice = tradeinMarketprice;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tool_trade_in_price, container, false);


        TextView fairprice = view.findViewById(R.id.fairprice);
        fairprice.setText(tradeinFairprice);

        TextView marketprice = view.findViewById(R.id.marketprice);
        marketprice.setText(tradeinBestprice);

        TextView bestprice = view.findViewById(R.id.bestprice);
        bestprice.setText(tradeinMarketprice);
        Log.e("test view", tradeinFairprice + tradeinBestprice + tradeinMarketprice);
        return view;


    }

    @Override
    public void onResume() {
        super.onResume();
        // put your code here...

        Application.getInstance().trackScreenView(getActivity(),GlobalText.ToolIbbPrivate_Fragment);

    }
    
}
