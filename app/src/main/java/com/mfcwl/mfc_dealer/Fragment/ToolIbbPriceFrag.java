package com.mfcwl.mfc_dealer.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.Activity.IbbPriceActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.IbbMarketTradeModel.Cpo;
import com.mfcwl.mfc_dealer.IbbMarketTradeModel.Private;
import com.mfcwl.mfc_dealer.IbbMarketTradeModel.ResponseIbbTradeData;
import com.mfcwl.mfc_dealer.IbbMarketTradeModel.Retail;
import com.mfcwl.mfc_dealer.IbbMarketTradeModel.Tradein;
import com.mfcwl.mfc_dealer.IbbMarketTradeService.MarketTradeService;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.tv_header_title;

public class ToolIbbPriceFrag extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


    public static View view;
    public static TextView ibbYear, ibbMonth, ibbMake, ibbModel, ibbVariant, ibbOwner, ibbColor, ibbCity;

    public static EditText ibbKms;
    public static Button checkPrice;
    public static Activity activity;
    public static String tradeinFairprice, tradeinMarketprice, tradeinBestprice, private1Fairprice, private1Marketprice, private1Bestprice, retailFairprice, retailMarketprice, retailBestprice, cpoFairprice, cpoMarketprice, cpoBestprice;

    public String star = "<font color='#B40404'>*</font>";
    public TextView yearStar, monthStar, makeStar, modelStar, variantStar, cityStar, ownerStar, colorStar, kmStar;
    public static TextView yearError, monthError, makeError, modelError, variantError, cityError, ownerError, colorError, kmError;
    public static String monthValue = "";
    String[] yearAry;
    String TAG = getClass().getSimpleName();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_tool_ibb_price, container, false);
        //remove bell
        setHasOptionsMenu(true);
        if (tv_header_title != null) {
            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText("Tools");
        }
        yearStar = view.findViewById(R.id.yearstar);
        makeStar = view.findViewById(R.id.makestar);
        variantStar = view.findViewById(R.id.variantstar);
        cityStar = view.findViewById(R.id.citystar);
        ownerStar = view.findViewById(R.id.ownerstar);
        colorStar = view.findViewById(R.id.colorstar);
        kmStar = view.findViewById(R.id.kmstar);
        monthStar = view.findViewById(R.id.monthstar);
        modelStar = view.findViewById(R.id.modelstar);
        yearStar.setText(Html.fromHtml("YEAR " + star));
        monthStar.setText(Html.fromHtml("MONTH " + star));
        makeStar.setText(Html.fromHtml("MAKE " + star));
        modelStar.setText(Html.fromHtml("MODEL " + star));
        variantStar.setText(Html.fromHtml("VARIANT " + star));
        cityStar.setText(Html.fromHtml("CITY " + star));
        ownerStar.setText(Html.fromHtml("OWNER " + star));
        colorStar.setText(Html.fromHtml("COLOR " + star));
        kmStar.setText(Html.fromHtml("KM " + star));

        yearError = view.findViewById(R.id.yearerror);
        monthError = view.findViewById(R.id.montherroe);
        makeError = view.findViewById(R.id.makeerror);
        modelError = view.findViewById(R.id.modelerror);
        variantError = view.findViewById(R.id.varianterror);
        cityError = view.findViewById(R.id.cityerror);
        ownerError = view.findViewById(R.id.ownererror);
        colorError = view.findViewById(R.id.colorerror);
        kmError = view.findViewById(R.id.kmserror);

        activity = getActivity();
        ibbYear = view.findViewById(R.id.ibb_year);
        ibbYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MethodYearLoadData();


                //IBB year

                if (CommonMethods.isInternetWorking(activity)) {
                    getIbbYearData(getContext(), "year", "app", CommonMethods.getstringvaluefromkey(activity, "IBB_access_token"));

                } else {
                    CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
                }
                //reena close year

                ibbMonth.setText("");
                ibbMake.setText("");
                ibbModel.setText("");
                ibbVariant.setText("");

            }
        });
        ibbMonth = view.findViewById(R.id.ibb_month);
        ibbMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ibbYear.getText().toString().equals("")) {

                    Toast.makeText(activity, "Please Select Year ", Toast.LENGTH_LONG).show();
                } else {

                    //MethodMonthLoadData();

                    //=============Sonali ===========================================
                    ibbMake.setText("");
                    ibbModel.setText("");
                    ibbVariant.setText("");

                    if (CommonMethods.isInternetWorking(activity)) {
                        getIbbMonthData(getContext(), "month", ibbYear.getText().toString(), "app", CommonMethods.getstringvaluefromkey(activity, "IBB_access_token"));
                    } else {
                        CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
                    }


                }
            }
        });
        ibbMake = view.findViewById(R.id.ibb_make);
        ibbMake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ibbYear.getText().toString().equals("")) {
                    Toast.makeText(activity, "Please Select Year ", Toast.LENGTH_LONG).show();
                } else if (ibbMonth.getText().toString().equals("") || ibbMonth.getText().toString().equals("Select")) {

                    Toast.makeText(activity, "Please Select Month ", Toast.LENGTH_LONG).show();

                } else {
                    // MethodMokeLoadData();

                    //ibb make close json
                    if (ibbMonth.getText().toString().equalsIgnoreCase("January")) {
                        monthValue = "1";
                    } else if (ibbMonth.getText().toString().equalsIgnoreCase("February")) {
                        monthValue = "2";
                    } else if (ibbMonth.getText().toString().equalsIgnoreCase("March")) {
                        monthValue = "3";
                    } else if (ibbMonth.getText().toString().equalsIgnoreCase("April")) {
                        monthValue = "4";
                    } else if (ibbMonth.getText().toString().equalsIgnoreCase("May")) {
                        monthValue = "5";
                    } else if (ibbMonth.getText().toString().equalsIgnoreCase("June")) {
                        monthValue = "6";
                    } else if (ibbMonth.getText().toString().equalsIgnoreCase("July")) {
                        monthValue = "7";
                    } else if (ibbMonth.getText().toString().equalsIgnoreCase("August")) {
                        monthValue = "8";
                    } else if (ibbMonth.getText().toString().equalsIgnoreCase("September")) {
                        monthValue = "9";
                    } else if (ibbMonth.getText().toString().equalsIgnoreCase("October")) {
                        monthValue = "10";
                    } else if (ibbMonth.getText().toString().equalsIgnoreCase("November")) {
                        monthValue = "11";
                    } else if (ibbMonth.getText().toString().equalsIgnoreCase("December")) {
                        monthValue = "12";
                    }

                    if (CommonMethods.isInternetWorking(activity)) {
                        getIbbMakeData(getContext(), "make",
                                ibbYear.getText().toString(), monthValue, "app", CommonMethods.getstringvaluefromkey(activity, "IBB_access_token"));

                    } else {
                        CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
                    }

                    ibbVariant.setText("");
                }

            }
        });
        ibbModel = view.findViewById(R.id.ibb_model);
        ibbModel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ibbMake.getText().toString().equals("") || ibbMake.getText().toString().equals("Select Make")) {
                    Toast.makeText(activity, "Please Select Make ", Toast.LENGTH_LONG).show();
                } else {
                    //MethodModelLoadData();


                    if (CommonMethods.isInternetWorking(activity)) {
                        //ibb model close json
                        getIbbModelData(getContext(), "model",
                                ibbMake.getText().toString(), ibbYear.getText().toString().trim(), monthValue, "app", CommonMethods.getstringvaluefromkey(activity, "IBB_access_token"));
                        ibbVariant.setText("");
                    } else {
                        CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
                    }


                }

            }
        });
        ibbVariant = view.findViewById(R.id.ibb_variant);
        ibbVariant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  MethodVariantLoadData();
                if (ibbYear.getText().toString().equals("")) {
                    Toast.makeText(activity, "Please Select Year", Toast.LENGTH_LONG).show();
                } else if (ibbMonth.getText().toString().equals("") || ibbMonth.getText().toString().equals("Select")) {
                    Toast.makeText(activity, "Please Select Month", Toast.LENGTH_LONG).show();
                } else if (ibbMake.getText().toString().equals("") || ibbMake.getText().toString().equals("Select Make")) {
                    Toast.makeText(activity, "Please Select Make", Toast.LENGTH_LONG).show();
                } else if (ibbModel.getText().toString().equals("") || ibbModel.getText().toString().equals("Select Model")) {
                    Toast.makeText(activity, "Please Select Model ", Toast.LENGTH_LONG).show();
                }/* else if (ibbVariant.getText().toString().equals("") || ibbVariant.getText().toString().equals("Select Variant")) {
                    Toast.makeText(activity, "Please Select Variant ", Toast.LENGTH_LONG).show();
                } else if (ibbCity.getText().toString().equals("")) {
                    Toast.makeText(activity, "Please Select City ", Toast.LENGTH_LONG).show();
                } else if (ibbOwner.getText().toString().equals("")) {
                    Toast.makeText(activity, "Please Select Owner ", Toast.LENGTH_LONG).show();
                } else if (ibbColor.getText().toString().equals("")) {
                    Toast.makeText(activity, "Please Select Color", Toast.LENGTH_LONG).show();
                } else if (ibbKms.getText().toString().equals("")) {
                    Toast.makeText(activity, "Please Select Km ", Toast.LENGTH_LONG).show();
                }*/ else {
                    // MethodVariantLoadData();

                    //ibb variant closed
                    getIbbvariantData(getContext(), "variant", ibbYear.getText().toString(), "2", ibbMake.getText().toString(), ibbModel.getText().toString(), "app", CommonMethods.getstringvaluefromkey(activity, "IBB_access_token"));
                }
            }
        });
        ibbOwner = view.findViewById(R.id.ibb_owner);
        ibbOwner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // MethodOwnerLoadData();


                if (CommonMethods.isInternetWorking(activity)) {
                    getIbbOwnerData(getContext(), "owner", "app", CommonMethods.getstringvaluefromkey(activity, "IBB_access_token"));

                } else {
                    CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
                }
            }
        });
        ibbColor = view.findViewById(R.id.ibb_color);
        ibbColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MethodColorLoadData();


                if (CommonMethods.isInternetWorking(activity)) {
                    //closed color json
                    getIbbColorData(getContext(), "color", "app", CommonMethods.getstringvaluefromkey(activity, "IBB_access_token"));

                } else {
                    CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
                }
            }
        });
        ibbCity = view.findViewById(R.id.ibb_city);
        ibbCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // MethodCityLoadData();

                if (CommonMethods.isInternetWorking(activity)) {
                    //closed city json
                    getIbbCityData(getContext(), "city", "app", CommonMethods.getstringvaluefromkey(activity, "IBB_access_token"));

                } else {
                    CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
                }
                //closed city json
                //  getIbbCityData(getContext(), "city", "app", CommonMethods.getstringvaluefromkey(activity, "IBB_access_token"));
            }
        });
        ibbKms = view.findViewById(R.id.ibb_kms);
        checkPrice = view.findViewById(R.id.check_ibb_price);
        checkPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.ibb_check_price, GlobalText.android);

                errorMessageShow();

                Log.e("test ibbmodel", ibbModel.getText().toString());
                Log.e("test ibbYear", ibbYear.getText().toString() + " " + ibbColor.getText().toString());
                Log.e("test ibbmonth", ibbMonth.getText().toString() + " " + ibbOwner.getText().toString() + " " + ibbKms.getText().toString());

                if (ibbYear.getText().toString().equals("")) {

                } else if (ibbMonth.getText().toString().equals("") || ibbMonth.getText().toString().equals("Select")) {

                } else if (ibbMake.getText().toString().equals("") || ibbMake.getText().toString().equals("Select Make")) {

                } else if (ibbModel.getText().toString().equals("") || ibbModel.getText().toString().equals("Select Model")) {

                } else if (ibbVariant.getText().toString().equals("")) {

                } else if (ibbCity.getText().toString().equals("")) {

                } else if (ibbOwner.getText().toString().equals("")) {

                } else if (ibbColor.getText().toString().equals("")) {

                } else if (ibbKms.getText().toString().equals("")) {

                } else {
                    ibbYear.setError(null);
                    ibbMonth.setError(null);
                    ibbMake.setError(null);
                    ibbModel.setError(null);
                    ibbVariant.setError(null);
                    ibbCity.setError(null);
                    ibbOwner.setError(null);
                    ibbColor.setError(null);
                    ibbKms.setError(null);

                    // WebServicesCall.webCall(activity, activity, jsonMakeIBBcheckPrice(), "IBBcheckPrice", GlobalText.POST);


                    //comprensive closed json
                    getIbbComprehensivePriceData(getContext(), "comprehensivePrice", ibbYear.getText().toString(), monthValue, ibbMake.getText().toString(), ibbModel.getText().toString(), ibbVariant.getText().toString(), ibbCity.getText().toString(), ibbColor.getText().toString(), ibbOwner.getText().toString(), ibbKms.getText().toString(), 0, CommonMethods.getstringvaluefromkey(activity, "usernameforIbb"), "App", CommonMethods.getstringvaluefromkey(activity, "IBB_access_token"));

                    //price
                }

            }
        });


        return view;
    }

    private JSONObject jsonMakeIBBcheckPrice() {
        JSONObject jObj = new JSONObject();
        try {
            jObj.put("for", "comprehensivePrice");
            jObj.put("year", ibbYear.getText().toString());
            jObj.put("month", monthValue);
            jObj.put("make", ibbMake.getText().toString());
            jObj.put("model", ibbModel.getText().toString());
            jObj.put("variant", ibbVariant.getText().toString());
            jObj.put("location", ibbCity.getText().toString());
            jObj.put("color", ibbColor.getText().toString());
            jObj.put("owner", ibbOwner.getText().toString());
            jObj.put("kilometer", ibbKms.getText().toString());
            jObj.put("tag", "App");
            jObj.put("pricefor", 0);
            jObj.put("partuserid", CommonMethods.getstringvaluefromkey(activity, "usernameforIbb"));
            jObj.put("access_token", CommonMethods.getstringvaluefromkey(activity, "IBB_access_token"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jObj;
    }

    private void MethodCityLoadData() {
        WebServicesCall.webCall(activity, activity, jsonMakeIBBcity(), "IBBcity", GlobalText.POST);
    }

    @Override
    public void onResume() {
        super.onResume();
        // put your code here...
        if (tv_header_title != null) {
            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText("Tools");
        }
        Application.getInstance().trackScreenView(activity,GlobalText.ToolIbbPrice_Fragment);

    }

    private JSONObject jsonMakeIBBcity() {
        JSONObject jObj = new JSONObject();
        try {
            jObj.put("for", "city");
            jObj.put("tag", "app");
            jObj.put("access_token", CommonMethods.getstringvaluefromkey(activity, "IBB_access_token"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jObj;
    }

    private void MethodColorLoadData() {
        WebServicesCall.webCall(activity, activity, jsonMakeIBBcolor(), "IBBcolor", GlobalText.POST);
    }

    private JSONObject jsonMakeIBBcolor() {
        JSONObject jObj = new JSONObject();
        try {
            jObj.put("for", "color");
            jObj.put("tag", "app");
            jObj.put("access_token", CommonMethods.getstringvaluefromkey(activity, "IBB_access_token"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jObj;
    }

    private void MethodOwnerLoadData() {
        WebServicesCall.webCall(activity, activity, jsonMakeIBBowner(), "IBBowner", GlobalText.POST);
    }

    private JSONObject jsonMakeIBBowner() {
        JSONObject jObj = new JSONObject();
        try {
            jObj.put("for", "owner");
            jObj.put("tag", "app");
            jObj.put("access_token", CommonMethods.getstringvaluefromkey(activity, "IBB_access_token"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jObj;
    }

    private void MethodVariantLoadData() {
        WebServicesCall.webCall(activity, activity, jsonMakeIBBvariant(), "IBBvariant", GlobalText.POST);
    }

    private JSONObject jsonMakeIBBvariant() {

        JSONObject jObj = new JSONObject();
        try {
            jObj.put("for", "variant");
            jObj.put("year", ibbYear.getText().toString());

            if (ibbMonth.getText().toString().equalsIgnoreCase("January")) {
                monthValue = "1";
            } else if (ibbMonth.getText().toString().equalsIgnoreCase("February")) {
                monthValue = "2";
            } else if (ibbMonth.getText().toString().equalsIgnoreCase("March")) {
                monthValue = "3";
            } else if (ibbMonth.getText().toString().equalsIgnoreCase("April")) {
                monthValue = "4";
            } else if (ibbMonth.getText().toString().equalsIgnoreCase("May")) {
                monthValue = "5";
            } else if (ibbMonth.getText().toString().equalsIgnoreCase("June")) {
                monthValue = "6";
            } else if (ibbMonth.getText().toString().equalsIgnoreCase("July")) {
                monthValue = "7";
            } else if (ibbMonth.getText().toString().equalsIgnoreCase("August")) {
                monthValue = "8";
            } else if (ibbMonth.getText().toString().equalsIgnoreCase("September")) {
                monthValue = "9";
            } else if (ibbMonth.getText().toString().equalsIgnoreCase("October")) {
                monthValue = "10";
            } else if (ibbMonth.getText().toString().equalsIgnoreCase("November")) {
                monthValue = "11";
            } else if (ibbMonth.getText().toString().equalsIgnoreCase("December")) {
                monthValue = "12";
            }
            Log.e("Post month ibbprice", monthValue);
            jObj.put("month", monthValue);
            jObj.put("make", ibbMake.getText().toString());
            jObj.put("model", ibbModel.getText().toString());
            jObj.put("tag", "app");
            jObj.put("access_token", CommonMethods.getstringvaluefromkey(activity, "IBB_access_token"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jObj;
    }

    private void MethodModelLoadData() {
        WebServicesCall.webCall(activity, activity, jsonMakeIBBmodel(), "IBBmodel", GlobalText.POST);
    }

    private JSONObject jsonMakeIBBmodel() {


        JSONObject jObj = new JSONObject();
        try {
            jObj.put("for", "model");
            jObj.put("year", ibbYear.getText().toString().trim());
            jObj.put("month", monthValue);
            jObj.put("make", ibbMake.getText().toString());
            jObj.put("tag", "app");
            jObj.put("access_token", CommonMethods.getstringvaluefromkey(activity, "IBB_access_token"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jObj;

    }

    private void MethodMokeLoadData() {
        WebServicesCall.webCall(activity, activity, jsonMakeIBBmake(), "IBBmake", GlobalText.POST);
    }

    private JSONObject jsonMakeIBBmake() {

        JSONObject jObj = new JSONObject();
        try {
            jObj.put("for", "make");
            jObj.put("year", ibbYear.getText().toString());


            if (ibbMonth.getText().toString().equalsIgnoreCase("January")) {
                monthValue = "1";
            } else if (ibbMonth.getText().toString().equalsIgnoreCase("February")) {
                monthValue = "2";
            } else if (ibbMonth.getText().toString().equalsIgnoreCase("March")) {
                monthValue = "3";
            } else if (ibbMonth.getText().toString().equalsIgnoreCase("April")) {
                monthValue = "4";
            } else if (ibbMonth.getText().toString().equalsIgnoreCase("May")) {
                monthValue = "5";
            } else if (ibbMonth.getText().toString().equalsIgnoreCase("June")) {
                monthValue = "6";
            } else if (ibbMonth.getText().toString().equalsIgnoreCase("July")) {
                monthValue = "7";
            } else if (ibbMonth.getText().toString().equalsIgnoreCase("August")) {
                monthValue = "8";
            } else if (ibbMonth.getText().toString().equalsIgnoreCase("September")) {
                monthValue = "9";
            } else if (ibbMonth.getText().toString().equalsIgnoreCase("October")) {
                monthValue = "10";
            } else if (ibbMonth.getText().toString().equalsIgnoreCase("November")) {
                monthValue = "11";
            } else if (ibbMonth.getText().toString().equalsIgnoreCase("December")) {
                monthValue = "12";
            }

            jObj.put("month", monthValue);
            jObj.put("tag", "app");
            jObj.put("access_token", CommonMethods.getstringvaluefromkey(activity, "IBB_access_token"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jObj;

    }

    private void MethodMonthLoadData() {
        WebServicesCall.webCall(activity, activity, jsonMakeIBBMonth(), "IBBmonth", GlobalText.POST);
    }

    private JSONObject jsonMakeIBBMonth() {
        JSONObject jObj = new JSONObject();
        try {
            jObj.put("for", "month");
            jObj.put("year", ibbYear.getText().toString());
            jObj.put("tag", "app");
            jObj.put("access_token", CommonMethods.getstringvaluefromkey(activity, "IBB_access_token"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jObj;

    }

    private void MethodYearLoadData() {


        WebServicesCall.webCall(activity, activity, jsonMakeIBBYear(), "IBByear", GlobalText.POST);


    }

    private JSONObject jsonMakeIBBYear() {
        JSONObject jObj = new JSONObject();
        try {
            jObj.put("for", "year");
            jObj.put("tag", "app");
            jObj.put("access_token", CommonMethods.getstringvaluefromkey(activity, "IBB_access_token"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jObj;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
       /* getMenuInflater().inflate(R.menu.menu_item, menu);
        MenuItem item = menu.findItem(R.id.share);
        item.setVisible(true);*/
        final MenuItem item = menu.findItem(R.id.notification_bell);
        item.setVisible(false);

        super.onCreateOptionsMenu(menu, inflater);

    }


    public static void ParseIBByear(JSONObject jObj, Activity activity) {
        Log.e("IBByear ", "is " + jObj.toString());
        try {
            // Log.e("IBBaccess_token ", "is " + jObj.getString("status"));


            if (jObj.getString("message").equalsIgnoreCase("success")) {
                JSONArray ArrayState = jObj.getJSONArray("year");
                String[] strArrTemp = new String[ArrayState.length()];
                for (int i = 0; i < ArrayState.length(); i++) {
                    strArrTemp[i] = ArrayState.getString(i);

                }
               /* setAlertDialog(view, activity, "Year",
                        strArrTemp);*/
                MethodYearListPopup(strArrTemp);
            } else {
                // CommonMethods.alertMessage(activity,"server data not found");
            }


        } catch (JSONException e) {
            Log.e("test year ibb", e.getMessage());
            e.printStackTrace();
        }
    }


    private static void setAlertDialog(View v, Activity a, final String strTitle, final String[] arrVal) {

        AlertDialog.Builder alert = new AlertDialog.Builder(a);
        alert.setTitle(strTitle);
        alert.setItems(arrVal, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (strTitle.equals("Year")) {

                    ibbYear.setText(arrVal[which]);

                } else if (strTitle.equals("Month")) {
                    ibbMonth.setText(arrVal[which]);
                } else if (strTitle.equals("Make")) {
                    ibbMake.setText(arrVal[which]);
                } else if (strTitle.equals("Model")) {
                    ibbModel.setText(arrVal[which]);
                } else if (strTitle.equals("Variant")) {
                    ibbVariant.setText(arrVal[which]);
                } else if (strTitle.equals("Owner")) {
                    ibbOwner.setText(arrVal[which]);
                } else if (strTitle.equals("Color")) {
                    ibbColor.setText(arrVal[which]);
                } else if (strTitle.equals("City")) {
                    ibbCity.setText(arrVal[which]);
                }

            }
        });
        alert.create();
        alert.show();

    }

    public static void ParseIBBmonth(JSONObject jObj, Activity activity) {
        Log.e("IBBmonth ", "is " + jObj.toString());
        try {
            // Log.e("IBBaccess_token ", "is " + jObj.getString("status"));


            if (jObj.getString("message").equalsIgnoreCase("success")) {
                JSONArray ArrayState = jObj.getJSONArray("month");
                String[] strArrTemp = new String[ArrayState.length()];
                String[] ibbmonthlist = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
                for (int i = 0; i < ArrayState.length(); i++) {
                    //strArrTemp[i] = ArrayState.getString(i);
                    for (int j = 0; j < ibbmonthlist.length; j++) {
                        strArrTemp[i] = ibbmonthlist[i];
                    }

                }
                MethodMonthListPopup(strArrTemp);
             /*  setAlertDialog(view, activity, "Month",
                        strArrTemp);*/
            } else {
                //  CommonMethods.alertMessage(activity,"server data not found");
            }


        } catch (JSONException e) {
            Log.e("test year ibb", e.getMessage());
            e.printStackTrace();
        }
    }


    public static void ParseIBBmake(JSONObject jObj, Activity activity) {
        Log.e("IBBmake ", "is " + jObj.toString());
        try {
            // Log.e("IBBaccess_token ", "is " + jObj.getString("status"));


            if (jObj.getString("message").equalsIgnoreCase("success")) {
                JSONArray ArrayState = jObj.getJSONArray("make");
                String[] strArrTemp = new String[ArrayState.length()];
                for (int i = 0; i < ArrayState.length(); i++) {
                    strArrTemp[i] = ArrayState.getString(i);

                }

                MethodMakeListPopup(strArrTemp);
                /*setAlertDialog(view, activity, "Make",
                        strArrTemp);*/
            } else {
                // CommonMethods.alertMessage(activity,"server data not found");
            }


        } catch (JSONException e) {
            Log.e("test year ibb", e.getMessage());
            e.printStackTrace();
        }
    }

    public static void ParseIBBmodel(JSONObject jObj, Activity activity) {
        Log.e("IBBmodel ", "is " + jObj.toString());
        try {
            // Log.e("IBBaccess_token ", "is " + jObj.getString("status"));


            if (jObj.getString("message").equalsIgnoreCase("success")) {
                JSONArray ArrayState = jObj.getJSONArray("model");
                String[] strArrTemp = new String[ArrayState.length()];
                for (int i = 0; i < ArrayState.length(); i++) {
                    strArrTemp[i] = ArrayState.getString(i);

                }
                MethodModelListPopup(strArrTemp);
               /* setAlertDialog(view, activity, "Model",
                        strArrTemp);*/
            } else {
                //  CommonMethods.alertMessage(activity,"server data not found");
            }


        } catch (JSONException e) {
            Log.e("test year ibb", e.getMessage());
            e.printStackTrace();
        }
    }

    public static void ParseIBBvariant(JSONObject jObj, Activity activity) {
        Log.e("IBBvariant ", "is " + jObj.toString());
        try {
            // Log.e("IBBaccess_token ", "is " + jObj.getString("status"));


            if (jObj.getString("message").equalsIgnoreCase("success")) {
                JSONArray ArrayState = jObj.getJSONArray("variant");
                String[] strArrTemp = new String[ArrayState.length()];
                for (int i = 0; i < ArrayState.length(); i++) {
                    strArrTemp[i] = ArrayState.getString(i);

                }
                MethodVariantListPopup(strArrTemp);
               /* setAlertDialog(view, activity, "Variant",
                        strArrTemp);*/
            } else {
                // CommonMethods.alertMessage(activity,"server data not found");
            }


        } catch (JSONException e) {
            Log.e("test year ibb", e.getMessage());
            e.printStackTrace();
        }
    }

    public static void ParseIBBowner(JSONObject jObj, Activity activity) {
        Log.e("IBBowner ", "is " + jObj.toString());
        try {
            // Log.e("IBBaccess_token ", "is " + jObj.getString("status"));

            if (jObj.getString("message").equalsIgnoreCase("success")) {
                JSONArray ArrayState = jObj.getJSONArray("owner");
                String[] strArrTemp = new String[ArrayState.length()];
                for (int i = 0; i < ArrayState.length(); i++) {
                    strArrTemp[i] = ArrayState.getString(i);

                }

                setAlertDialog(view, activity, "Owner",
                        strArrTemp);
            } else {
                // CommonMethods.alertMessage(activity,"server data not found");
            }


        } catch (JSONException e) {
            Log.e("test year ibb", e.getMessage());
            e.printStackTrace();
        }
    }

    public static void ParseIBBcolor(JSONObject jObj, Activity activity) {
        Log.e("IBBcolor ", "is " + jObj.toString());
        try {
            // Log.e("IBBaccess_token ", "is " + jObj.getString("status"));

            if (jObj.getString("message").equalsIgnoreCase("success")) {
                JSONArray ArrayState = jObj.getJSONArray("color");
                String[] strArrTemp = new String[ArrayState.length()];
                for (int i = 0; i < ArrayState.length(); i++) {
                    strArrTemp[i] = ArrayState.getString(i);

                }

                MethodColourListPopup(strArrTemp);
               /* setAlertDialog(view, activity, "Color",
                        strArrTemp);*/
            } else {
                // CommonMethods.alertMessage(activity,"server data not found");
            }


        } catch (JSONException e) {
            Log.e("test year ibb", e.getMessage());
            e.printStackTrace();
        }
    }

    public static void ParseIBBcity(JSONObject jObj, Activity activity) {
        Log.e("IBBcity ", "is " + jObj.toString());
        try {
            // Log.e("IBBaccess_token ", "is " + jObj.getString("status"));


            if (jObj.getString("message").equalsIgnoreCase("success")) {
                JSONArray ArrayState = jObj.getJSONArray("city");
                String[] strArrTemp = new String[ArrayState.length()];
                for (int i = 0; i < ArrayState.length(); i++) {
                    strArrTemp[i] = ArrayState.getString(i);

                }

                MethodCityListPopup(strArrTemp);
                /*setAlertDialog(view, activity, "City",
                        strArrTemp);*/
            } else {
                // CommonMethods.alertMessage(activity,"server data not found");
            }


        } catch (JSONException e) {
            Log.e("test year ibb", e.getMessage());
            e.printStackTrace();
        }
    }

    public static void ParseIBBcheckPrice(JSONObject jObj, Activity activity) {
        Log.e("IBBcheckPrice ", "is " + jObj.toString());

        try {
            if (jObj.getString("message").equalsIgnoreCase("success")) {
                JSONObject tradein = jObj.getJSONObject("tradein");
                tradeinFairprice = tradein.getString("fairprice");
                tradeinMarketprice = tradein.getString("marketprice");
                tradeinBestprice = tradein.getString("bestprice");

                JSONObject private1 = jObj.getJSONObject("private");
                private1Fairprice = private1.getString("fairprice");
                private1Marketprice = private1.getString("marketprice");
                private1Bestprice = private1.getString("bestprice");
                JSONObject retail = jObj.getJSONObject("retail");
                retailFairprice = retail.getString("fairprice");
                retailMarketprice = retail.getString("marketprice");
                retailBestprice = retail.getString("bestprice");
                JSONObject cpo = jObj.getJSONObject("cpo");
                cpoFairprice = cpo.getString("fairprice");
                cpoMarketprice = cpo.getString("marketprice");
                cpoBestprice = cpo.getString("bestprice");
                Intent in_main = new Intent(activity, IbbPriceActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("tradeinFairprice", tradeinFairprice);
                bundle.putString("tradeinMarketprice", tradeinMarketprice);
                bundle.putString("tradeinBestprice", tradeinBestprice);

                Log.e("test trand", tradeinMarketprice + " " + tradeinBestprice);
                bundle.putString("private1Fairprice", private1Fairprice);
                bundle.putString("private1Marketprice", private1Marketprice);
                bundle.putString("private1Bestprice", private1Bestprice);

                bundle.putString("retailFairprice", retailFairprice);
                bundle.putString("retailMarketprice", retailMarketprice);
                bundle.putString("retailBestprice", retailBestprice);

                bundle.putString("cpoFairprice", cpoFairprice);
                bundle.putString("cpoMarketprice", cpoMarketprice);
                bundle.putString("cpoBestprice", cpoBestprice);

                bundle.putString("make", ibbMake.getText().toString());
                bundle.putString("model", ibbModel.getText().toString());
                bundle.putString("variant", ibbVariant.getText().toString());
                bundle.putString("city", ibbCity.getText().toString());
                bundle.putString("kms", ibbKms.getText().toString());
                bundle.putString("year", ibbYear.getText().toString());
                bundle.putString("color", ibbColor.getText().toString());

                //in_main.putExtra("source", stocktype);
                in_main.putExtras(bundle);

                activity.startActivity(in_main);

                // activity.finish();
            } else {
                // CommonMethods.alertMessage(activity,"server data not found");
            }


        } catch (JSONException e) {

            e.printStackTrace();
        }
    }

    public static void MethodYearListPopup(final String[] yearList) {

        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        TextView alertdialog_edittext = (EditText) dialog_data.findViewById(R.id.alertdialog_edittext);
        searchtittle.setText("Year List");
        alertdialog_edittext.setHint("Search Year");
        alertdialog_edittext.setInputType(InputType.TYPE_CLASS_NUMBER);

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

        ImageView backicon = dialog_data.findViewById(R.id.backicon);
        backicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }

            }
        });
        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }

            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        alertdialog_edittext.setInputType(InputType.TYPE_CLASS_NUMBER);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, yearList);
        alertdialog_Listview.setAdapter(adapter);
        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                String yearName = a.getAdapter().getItem(position).toString();

                Log.e("yearName ", "yearName " + yearName);


                TextView textview = v.findViewById(android.R.id.text1);
                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);


                String stryr = "";
                ibbYear.setText(yearName);
                if (!ibbYear.getText().toString().equals("")) {
                    yearError.setVisibility(View.INVISIBLE);
                }
                if (stryr.equals("manufactureyear")) {

                }

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });

        dialog_data.show();
    }

    public static void MethodCityListPopup(final String[] citylist) {

        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        TextView alertdialog_edittext = (EditText) dialog_data.findViewById(R.id.alertdialog_edittext);
        searchtittle.setText("City List");
        alertdialog_edittext.setHint("Search City");
        alertdialog_edittext.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

   /*     TextView alertdialog_textview = (TextView) dialog_data.findViewById(R.id.alertdialog_textview);
        alertdialog_textview.setText("City List");
        alertdialog_textview.setHint("search city");*/
        ImageView backicon = dialog_data.findViewById(R.id.backicon);
        backicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }
            }
        });
        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }

            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        alertdialog_edittext.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, citylist);
        alertdialog_Listview.setAdapter(adapter);
        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                String cityName = a.getAdapter().getItem(position).toString();

                Log.e("cityName ", "cityName " + cityName);

                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                ibbCity.setText(cityName);
                if (!ibbCity.getText().toString().equals("Select City") || !ibbCity.getText().toString().equals("")) {

                    cityError.setVisibility(View.INVISIBLE);
                }

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });


        dialog_data.show();
    }

    public static void MethodMakeListPopup(final String[] makeList) {


        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        TextView alertdialog_edittext = (EditText) dialog_data.findViewById(R.id.alertdialog_edittext);
        searchtittle.setText("Make List");
        alertdialog_edittext.setHint("Search Make");
        alertdialog_edittext.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

        ImageView backicon = dialog_data.findViewById(R.id.backicon);
        backicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }

            }
        });
        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }
            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        alertdialog_edittext.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, makeList);
        alertdialog_Listview.setAdapter(adapter);
        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                String makeName = a.getAdapter().getItem(position).toString();


                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                ibbMake.setText(makeName);
                if (!ibbMake.getText().toString().equals("") || !ibbMake.getText().toString().equals("Select Make")) {
                    makeError.setVisibility(View.INVISIBLE);
                }

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });


        dialog_data.show();
    }

    public static void MethodColourListPopup(final String[] colorList) {


        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        TextView alertdialog_edittext = (EditText) dialog_data.findViewById(R.id.alertdialog_edittext);
        searchtittle.setText("Color List");
        alertdialog_edittext.setHint("Search Color");
        alertdialog_edittext.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

        ImageView backicon = dialog_data.findViewById(R.id.backicon);
        backicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }

            }
        });
        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }

            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        alertdialog_edittext.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, colorList);
        alertdialog_Listview.setAdapter(adapter);
        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                String colourName = a.getAdapter().getItem(position).toString();

                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                ibbColor.setText(colourName);
                if (!ibbColor.getText().toString().equals("") || !ibbColor.getText().toString().equals("Select Color")) {
                    colorError.setVisibility(View.INVISIBLE);
                }

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });


        dialog_data.show();
    }

    public static void MethodModelListPopup(final String[] modelList) {

        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        TextView alertdialog_edittext = (EditText) dialog_data.findViewById(R.id.alertdialog_edittext);
        searchtittle.setText("Model List");
        alertdialog_edittext.setHint("Search Model");
        alertdialog_edittext.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

        ImageView backicon = dialog_data.findViewById(R.id.backicon);
        backicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }
            }
        });
        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }

            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        alertdialog_edittext.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, modelList);
        alertdialog_Listview.setAdapter(adapter);
        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                String modelName = a.getAdapter().getItem(position).toString();

                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                ibbModel.setText(modelName);
                if (!ibbModel.getText().toString().equals("") || !ibbModel.getText().toString().equals("Select Model")) {
                    modelError.setVisibility(View.INVISIBLE);
                }

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });


        dialog_data.show();
    }

    public static void MethodVariantListPopup(final String[] variantList) {

        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        TextView alertdialog_edittext = (EditText) dialog_data.findViewById(R.id.alertdialog_edittext);
        searchtittle.setText("Variant List");
        alertdialog_edittext.setHint("Search Variant");
        alertdialog_edittext.setInputType(InputType.TYPE_CLASS_TEXT);

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);


        ImageView backicon = dialog_data.findViewById(R.id.backicon);
        backicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }

            }
        });

        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }
            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        alertdialog_edittext.setInputType(InputType.TYPE_CLASS_TEXT);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, variantList);
        alertdialog_Listview.setAdapter(adapter);
        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                String modelName = a.getAdapter().getItem(position).toString();

                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                ibbVariant.setText(modelName);
                if (!ibbVariant.getText().toString().equals("") || !ibbVariant.getText().toString().equals("Select Variant")) {
                    variantError.setVisibility(View.INVISIBLE);
                }

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });


        dialog_data.show();
    }

    public void errorMessageShow() {
        if (ibbYear.getText().toString().equals("")) {
            yearError.setVisibility(View.VISIBLE);
        } else {
            yearError.setVisibility(View.INVISIBLE);
        }
        if (ibbMonth.getText().toString().equals("") || ibbMonth.getText().toString().equals("Select")) {
            monthError.setVisibility(View.VISIBLE);
        } else {
            monthError.setVisibility(View.INVISIBLE);
        }
        if (ibbMake.getText().toString().equals("") || ibbMake.getText().toString().equals("Select Make")) {
            makeError.setVisibility(View.VISIBLE);
        } else {
            makeError.setVisibility(View.INVISIBLE);
        }
        if (ibbModel.getText().toString().equals("") || ibbModel.getText().toString().equals("Select Model")) {
            modelError.setVisibility(View.VISIBLE);
        } else {
            modelError.setVisibility(View.INVISIBLE);
        }
        if (ibbVariant.getText().toString().equals("") || ibbVariant.getText().toString().equals("Select Variant")) {
            variantError.setVisibility(View.VISIBLE);
        } else {
            variantError.setVisibility(View.INVISIBLE);
        }
        if (ibbCity.getText().toString().equals("")) {
            cityError.setVisibility(View.VISIBLE);
        } else {
            cityError.setVisibility(View.INVISIBLE);
        }
        if (ibbOwner.getText().toString().equals("")) {
            ownerError.setVisibility(View.VISIBLE);
        } else {
            ownerError.setVisibility(View.INVISIBLE);
        }
        if (ibbColor.getText().toString().equals("")) {
            colorError.setVisibility(View.VISIBLE);
        } else {
            colorError.setVisibility(View.INVISIBLE);
        }
        if (ibbKms.getText().toString().equals("")) {
            kmError.setVisibility(View.VISIBLE);
        } else {
            kmError.setVisibility(View.INVISIBLE);
        }
    }

    private void errordisables() {
        yearError.setVisibility(View.INVISIBLE);
        monthError.setVisibility(View.INVISIBLE);
        makeError.setVisibility(View.INVISIBLE);
        modelError.setVisibility(View.INVISIBLE);
        variantError.setVisibility(View.INVISIBLE);
        cityError.setVisibility(View.INVISIBLE);
        ownerError.setVisibility(View.INVISIBLE);
        colorError.setVisibility(View.INVISIBLE);
        kmError.setVisibility(View.INVISIBLE);
    }

    public static void MethodMonthListPopup(final String[] monthList) {

        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        TextView alertdialog_edittext = (EditText) dialog_data.findViewById(R.id.alertdialog_edittext);
        searchtittle.setText("Month List");
        alertdialog_edittext.setHint("Search Month");
        alertdialog_edittext.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);


        ImageView backicon = dialog_data.findViewById(R.id.backicon);
        backicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }
                if (ibbMonth.getText().toString().equals("") || ibbMonth.getText().toString().equals("Select")) {

                } else {
                    ibbMonth.setError(null);

                }
            }
        });
        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }

            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        alertdialog_edittext.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, monthList);
        alertdialog_Listview.setAdapter(adapter);
        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                String monthName = a.getAdapter().getItem(position).toString();

                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                ibbMonth.setText(monthName);
                if (ibbMonth.getText().toString().equals("") || ibbMonth.getText().toString().equals("Select")) {

                } else {
                    monthError.setVisibility(View.INVISIBLE);
                    ibbMonth.setError(null);
                    if (ibbMonth.getText().toString().equalsIgnoreCase("January")) {
                        monthValue = "1";
                    } else if (ibbMonth.getText().toString().equalsIgnoreCase("February")) {
                        monthValue = "2";
                    } else if (ibbMonth.getText().toString().equalsIgnoreCase("March")) {
                        monthValue = "3";
                    } else if (ibbMonth.getText().toString().equalsIgnoreCase("April")) {
                        monthValue = "4";
                    } else if (ibbMonth.getText().toString().equalsIgnoreCase("May")) {
                        monthValue = "5";
                    } else if (ibbMonth.getText().toString().equalsIgnoreCase("June")) {
                        monthValue = "6";
                    } else if (ibbMonth.getText().toString().equalsIgnoreCase("July")) {
                        monthValue = "7";
                    } else if (ibbMonth.getText().toString().equalsIgnoreCase("August")) {
                        monthValue = "8";
                    } else if (ibbMonth.getText().toString().equalsIgnoreCase("September")) {
                        monthValue = "9";
                    } else if (ibbMonth.getText().toString().equalsIgnoreCase("October")) {
                        monthValue = "10";
                    } else if (ibbMonth.getText().toString().equalsIgnoreCase("November")) {
                        monthValue = "11";
                    } else if (ibbMonth.getText().toString().equalsIgnoreCase("December")) {
                        monthValue = "12";
                    }
                }

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }

                Log.e("Month ibbprice", ibbMonth.getText().toString().trim());
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });


        dialog_data.show();
    }


    public void getIbbYearData(Context context, String year, String app, String token) {
        SpinnerManager.showSpinner(context);
        MarketTradeService.IddMrkTradeData(context,year, app, token, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                Response<ResponseIbbTradeData> mRes = (Response<ResponseIbbTradeData>) obj;

                ResponseIbbTradeData data = mRes.body();
                Log.e("test: ", data.getMessage() + " " + data.getStatus());
                if (data.getStatus() == 200) {
                    List<String> mYear = data.getYear();
                    yearAry = new String[mYear.size()];
                    yearAry = mYear.toArray(yearAry);
                    for (int i = 0; i < mYear.size(); i++) {
                        Log.e("Test year: ", mYear.get(i));
                    }

                    MethodYearListPopup(yearAry);
                }else{
                    if (data.getStatus()==401) {
                        WebServicesCall.error_popup_retrofit(401, data.getMessage(), context);
                    } else {
                        CommonMethods.alertMessage((Activity) context, data.getMessage());
                    }
                }

                SpinnerManager.hideSpinner(context);
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(context);
            }
        });
    }

    //ibb month
    public void getIbbMonthData(Context context, String forValue, String year, String app, String token) {
        SpinnerManager.showSpinner(context);
        MarketTradeService.IddMrkTradeDataCity(forValue, year, app, token, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                Response<ResponseIbbTradeData> mRes = (Response<ResponseIbbTradeData>) obj;

                ResponseIbbTradeData data = mRes.body();

                if (data.getStatus() == 200) {
                    Log.e("test: ", data.getMessage() + " " + data.getStatus());
                    List<String> mCity = data.getMonth();

                    String[] strArrTemp = new String[mCity.size()];
                    strArrTemp = mCity.toArray(strArrTemp);
                    String[] ibbmonthlist = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
                    for (int i = 0; i < mCity.size(); i++) {
                        //strArrTemp[i] = ArrayState.getString(i);
                        for (int j = 0; j < ibbmonthlist.length; j++) {
                            strArrTemp[i] = ibbmonthlist[i];
                        }

                    }

                    MethodMonthListPopup(strArrTemp);
                } else {
                    if (data.getStatus()==401) {
                        WebServicesCall.error_popup_retrofit(401, data.getMessage(), context);
                    } else {
                        CommonMethods.alertMessage((Activity) context, data.getMessage());
                    }
                }
                SpinnerManager.hideSpinner(context);
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(context);
            }
        });
    }

    //ibb make
    public void getIbbMakeData(Context context, String forValue, String year, String month, String app, String token) {
        SpinnerManager.showSpinner(context);

        MarketTradeService.IddMrkTradeDataMake(forValue, year, month, app, token, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                Response<ResponseIbbTradeData> mRes = (Response<ResponseIbbTradeData>) obj;

                ResponseIbbTradeData data = mRes.body();
                if (data.getStatus() == 200) {
                    List<String> mMake = data.getMake();
                    String[] makeAry = new String[mMake.size()];
                    makeAry = mMake.toArray(makeAry);
                    MethodMakeListPopup(makeAry);
                    ibbModel.setText("");

                }else{
                    if (data.getStatus()==401) {
                        WebServicesCall.error_popup_retrofit(401, data.getMessage(), context);
                    } else {
                        CommonMethods.alertMessage((Activity) context, data.getMessage());
                    }
                }
                SpinnerManager.hideSpinner(context);
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(context);
            }

        });
    }

    //ibb model
    public void getIbbModelData(Context context, String forValue, String make, String year, String month, String app, String token) {
        SpinnerManager.showSpinner(context);

        MarketTradeService.IddMrkTradeDataModel(forValue, make, year, month, app, token, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                Response<ResponseIbbTradeData> mRes = (Response<ResponseIbbTradeData>) obj;

                ResponseIbbTradeData data = mRes.body();
                if (data.getStatus() == 200) {
                    Log.e("test model ", data.getMessage());
                    List<String> mModel = data.getModel();
                    String[] modelAry = new String[mModel.size()];
                    modelAry = mModel.toArray(modelAry);
                    for (int i = 0; i < mModel.size(); i++) {
                        Log.e("test model", mModel.get(i));
                    }
                    MethodModelListPopup(modelAry);
                    ibbVariant.setText("");
                } else {
                    if (data.getStatus()==401) {
                        WebServicesCall.error_popup_retrofit(401, data.getMessage(), context);
                    } else {
                        CommonMethods.alertMessage((Activity) context, data.getMessage());
                    }
                }


                SpinnerManager.hideSpinner(context);
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(context);

                Log.e("model error", mThrowable.getMessage());
            }
        });
    }

    //ibb variant
    public void getIbbvariantData(Context context, String forValue, String year, String month, String make, String model, String app, String token) {
        SpinnerManager.showSpinner(context);

        MarketTradeService.IddMrkTradeDataVariant(forValue, year, month, make, model, app, token, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                Response<ResponseIbbTradeData> mRes = (Response<ResponseIbbTradeData>) obj;

                ResponseIbbTradeData data = mRes.body();
                if (data.getStatus() == 200) {
                    List<String> mVariant = data.getVariant();
                    String[] variantAry = new String[mVariant.size()];
                    variantAry = mVariant.toArray(variantAry);
                    MethodVariantListPopup(variantAry);
                } else {
                    if (data.getStatus()==401) {
                        WebServicesCall.error_popup_retrofit(401, data.getMessage(), context);
                    } else {
                        CommonMethods.alertMessage((Activity) context, data.getMessage());
                    }
                }

                SpinnerManager.hideSpinner(context);
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(context);

                Log.e("model error", mThrowable.getMessage());
            }
        });
    }

    //ibb city

    public void getIbbCityData(Context context, String forValue, String app, String token) {
        SpinnerManager.showSpinner(context);

        MarketTradeService.IddMrkTradeDataCity(forValue, app, token, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                Response<ResponseIbbTradeData> mRes = (Response<ResponseIbbTradeData>) obj;

                ResponseIbbTradeData data = mRes.body();
                if (data.getStatus() == 200) {
                    List<String> mVariant = data.getCity();
                    String[] variantAry = new String[mVariant.size()];
                    variantAry = mVariant.toArray(variantAry);
                    MethodCityListPopup(variantAry);
                } else {
                    if (data.getStatus()==401) {
                        WebServicesCall.error_popup_retrofit(401, data.getMessage(), context);
                    } else {
                        CommonMethods.alertMessage((Activity) context, data.getMessage());
                    }
                }

                SpinnerManager.hideSpinner(context);
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(context);

                Log.e("model error", mThrowable.getMessage());
            }
        });
    }

    //ibb color
    public void getIbbColorData(Context context, String forValue, String app, String token) {
        SpinnerManager.showSpinner(context);

        MarketTradeService.IddMrkTradeDataColor(forValue, app, token, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                Response<ResponseIbbTradeData> mRes = (Response<ResponseIbbTradeData>) obj;

                ResponseIbbTradeData data = mRes.body();
                if (data.getStatus() == 200) {
                    List<String> mVariant = data.getColor();
                    String[] variantAry = new String[mVariant.size()];
                    variantAry = mVariant.toArray(variantAry);
                    MethodColourListPopup(variantAry);
                } else {
                    if (data.getStatus()==401) {
                        WebServicesCall.error_popup_retrofit(401, data.getMessage(), context);
                    } else {
                        CommonMethods.alertMessage((Activity) context, data.getMessage());
                    }
                }

                SpinnerManager.hideSpinner(context);
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(context);

                Log.e("model error", mThrowable.getMessage());
            }
        });
    }

    //ibb owner
    public void getIbbOwnerData(Context context, String forValue, String app, String token) {
        SpinnerManager.showSpinner(context);

        MarketTradeService.IddMrkTradeDataOwner(forValue, app, token, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                Response<ResponseIbbTradeData> mRes = (Response<ResponseIbbTradeData>) obj;

                ResponseIbbTradeData data = mRes.body();
                if (data.getStatus() == 200) {
                    List<String> mVariant = data.getOwner();
                    String[] variantAry = new String[mVariant.size()];
                    variantAry = mVariant.toArray(variantAry);
                    setAlertDialog(view, activity, "Owner",
                            variantAry);
                } else {

                }

                SpinnerManager.hideSpinner(context);
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(context);

                Log.e("model error", mThrowable.getMessage());
            }
        });
    }

    //IddMrkTradeDataComprehensivePrice
    public void getIbbComprehensivePriceData(Context context, String forValue, String year, String month, String make, String model, String variant, String location, String color, String owner, String kilometer, int priceFor, String partUserId, String app, String token) {
        SpinnerManager.showSpinner(context);

        MarketTradeService.IddMrkTradeDataComprehensivePrice(forValue, year, month, make, model, variant, location, color, owner, kilometer, priceFor, partUserId, app, token, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                Response<ResponseIbbTradeData> mRes = (Response<ResponseIbbTradeData>) obj;

                ResponseIbbTradeData data = mRes.body();
                if (data.getStatus() == 200) {

                    List<Integer> comPriceId = data.getComPriceID();
                    Cpo cpo = data.getCpo();
                    Retail retail = data.getRetail();
                    Private p = data.get_private();
                    Tradein tradein = data.getTradein();


                    Intent in_main = new Intent(activity, IbbPriceActivity.class);

                    Bundle bundle = new Bundle();
                    bundle.putString("make", ibbMake.getText().toString());
                    bundle.putString("model", ibbModel.getText().toString());
                    bundle.putString("variant", ibbVariant.getText().toString());
                    bundle.putString("city", ibbCity.getText().toString());
                    bundle.putString("kms", ibbKms.getText().toString());
                    bundle.putString("year", ibbYear.getText().toString());
                    bundle.putString("color", ibbColor.getText().toString());

                    in_main.putExtras(bundle);
                    in_main.putExtra("CPO", cpo);
                    in_main.putExtra("RETAIL", retail);
                    in_main.putExtra("PRIVATE", p);
                    in_main.putExtra("TRADEIN", tradein);
                    activity.startActivity(in_main);

                } else {
                    Log.e("test compresive data1: ", mRes.body().getMessage());
                }

                SpinnerManager.hideSpinner(context);
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(context);

                Log.e("model error", mThrowable.getMessage());
            }
        });
    }
}
