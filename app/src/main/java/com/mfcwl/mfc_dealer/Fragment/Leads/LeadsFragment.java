package com.mfcwl.mfc_dealer.Fragment.Leads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mfcwl.mfc_dealer.Activity.Leads.FilterInstance;
import com.mfcwl.mfc_dealer.Activity.Leads.LeadConstants;
import com.mfcwl.mfc_dealer.Activity.Leads.LeadFilterActivityNew;
import com.mfcwl.mfc_dealer.Activity.Leads.LeadsInstance;
import com.mfcwl.mfc_dealer.Activity.Leads.SortInstance;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Activity.SplashActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Fragment.ToolFragment;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import org.json.JSONArray;

import java.lang.ref.WeakReference;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchicon;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.leadstatusJsonArray;
import static com.mfcwl.mfc_dealer.Fragment.Leads.NormalLeadsFragment.normalLeadsRecy;
import static com.mfcwl.mfc_dealer.Fragment.Leads.PrivateLeadsFragment.privateLeadsRecy;
import static com.mfcwl.mfc_dealer.Utility.LeadsUtils.ClearFollowDataFilter;
import static com.mfcwl.mfc_dealer.Utility.LeadsUtils.ClearLeadStatusFilter;
import static com.mfcwl.mfc_dealer.Utility.LeadsUtils.ClearPostDataFilter;


public class LeadsFragment extends Fragment {

    public static ImageView lead_filter, lead_sort;
    LinearLayout lead_filter_linear, lead_sort_linear;
    public static boolean normalLead = false, privateLead = false;
    public static AppBarLayout appheader;
    public static Activity activity;
    public static LinearLayout headerdataLeads;
    private final SparseArray<WeakReference<Fragment>> instantiatedFragments = new SparseArray<>();
    private static SharedPreferences mSharedPreferences = null;
    String TAG = getClass().getSimpleName();
    public LeadsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        // Inflate the layout for this fragment
        mSharedPreferences = getActivity().getSharedPreferences("MFC", Context.MODE_PRIVATE);

        searchicon.setVisibility(View.VISIBLE);

        View view = inflater.inflate(R.layout.lead_fragment, container, false);
        //  Log.e("howmany ", "LeadsFragment onCreateView");
        setHasOptionsMenu(true);
        activity = getActivity();
        appheader = view.findViewById(R.id.appheader);
        TabLayout tabLayout = view.findViewById(R.id.leads_tabs);
        lead_filter = view.findViewById(R.id.lead_filter);
        lead_sort = view.findViewById(R.id.lead_sort);

        lead_filter_linear = view.findViewById(R.id.lead_filter_linear);
        lead_sort_linear = view.findViewById(R.id.lead_sort_linear);
        headerdataLeads = view.findViewById(R.id.headerdataLeads);
        final ViewPager viewPager = view.findViewById(R.id.leads_viewpager);
        viewPager.setAdapter(new LeadsPagerAdapter(getFragmentManager(), tabLayout.getTabCount()));

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(Color.parseColor("#aaaaaa"), Color.parseColor("#2d2d2d"));
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#e5e5e5"));

        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));

        LeadsInstance.getInstance().setWhichleads("WebLeads");
        CommonMethods.setvalueAgainstKey(getActivity(), "status", "WebLeads");
        CommonMethods.setvalueAgainstKey(getActivity(), "Notification_Switch", "");

        try {
            if (NormalLeadsFragment.myJobList != null) {
                NormalLeadsFragment.myJobList.clear();
            }
            if (PrivateLeadsFragment.myPriList != null) {
                PrivateLeadsFragment.myPriList.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            MainActivity.searchClose();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (LeadsInstance.getInstance().getLeaddirection().equals("PrivateLeadsPage")) {
            LeadsInstance.getInstance().setLeaddirection("");
            LeadsInstance.getInstance().setWhichleads("PrivateLeads");
            viewPager.setCurrentItem(1);

        }

        if (LeadsInstance.getInstance().getAddlead().equals("RedirectLeadPage")) {
            LeadsInstance.getInstance().setAddlead("");
            LeadsInstance.getInstance().setWhichleads("PrivateLeads");
            viewPager.setCurrentItem(1);
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.e("howmany ", "LeadsFragment tab");
                Log.e("LeadsFragment ", "tab ");
                Log.e("LeadsFragment ", "tab " + tab.getPosition());

                if (tab.getPosition() == 0) {
                    Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.web_leads, GlobalText.android);

                    // SplashActivity.progress = true;
                    LeadsInstance.getInstance().setWhichleads("WebLeads");
                    CommonMethods.setvalueAgainstKey(getActivity(), "status", "WebLeads");
                    NormalLeadsFragment.SortByLeads();

                }
                if (tab.getPosition() == 1) {
                    Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.private_leads, GlobalText.android);

                    SplashActivity.progress = true;
                    LeadsInstance.getInstance().setWhichleads("PrivateLeads");
                    CommonMethods.setvalueAgainstKey(getActivity(), "status", "PrivateLeads");
                    PrivateLeadsFragment.SortByPrivateLeads();
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        lead_filter_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.lead_filter, GlobalText.android);
                normalLeadsRecy.stopScroll();
                privateLeadsRecy.stopScroll();
                Intent intent = new Intent(getActivity(), LeadFilterActivityNew.class);
                startActivity(intent);
            }
        });

        lead_sort_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.lead_sort_by, GlobalText.android);
                normalLeadsRecy.stopScroll();
                privateLeadsRecy.stopScroll();
                LeadSortCustomDialogClass sortCustomDialogClass = new LeadSortCustomDialogClass(getContext(), getActivity());
                sortCustomDialogClass.show();
            }
        });

      /*  if(LeadsInstance.getInstance().getAddlead().equals("RedirectLeadPage"))
        {
            LeadsInstance.getInstance().setAddlead("");
            viewPager.setCurrentItem(1);
        }


*/
        return view;

    }

    public class LeadsPagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        // tab titles
        private String[] tabTitles = new String[]{" Web Leads ", "Private Leads"};

        // overriding getPageTitle()
        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }

        public LeadsPagerAdapter(FragmentManager fm, int mNumOfTabs) {
            super(fm);
            this.mNumOfTabs = mNumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {

            Log.e("position ", "position " + position);
            switch (position) {
                case 0:
                    return new NormalLeadsFragment();
                case 1:
                    return new PrivateLeadsFragment();
                case 2:
                    return new ToolFragment();

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return tabTitles.length;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
        /*Object obj=super.instantiateItem(container,position);
        if(obj instanceof Fragment)
        {
            Fragment f=(Fragment)obj;
            String tag=f.getTag();
        }
        return obj;*/

            final Fragment fragment = (Fragment) super.instantiateItem(container, position);

            instantiatedFragments.put(position, new WeakReference<>(fragment));
            return fragment;
        }

        @Nullable
        public Fragment getFragment(final int position) {
            final WeakReference<Fragment> wr = instantiatedFragments.get(position);
            if (wr != null) {
                return wr.get();
            } else {
                return null;
            }
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
       /* getMenuInflater().inflate(R.menu.menu_item, menu);
        MenuItem item = menu.findItem(R.id.share);
        item.setVisible(true);*/
        final MenuItem item = menu.findItem(R.id.notification_bell);
        item.setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);

    }

    public static void top_bottomAnimation() {
        Animation hide = AnimationUtils.loadAnimation(activity, R.anim.top_bottom);
        appheader.startAnimation(hide);
    }

    public static void bottom_topAnimation() {
        Animation hide = AnimationUtils.loadAnimation(activity, R.anim.bottom_top);
        appheader.startAnimation(hide);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("howmany ", "LeadsFragment onResume");

        Application.getInstance().trackScreenView(getActivity(),GlobalText.LeadFragment_Fragment);

        if (FilterInstance.getInstance().getNotification().equals("new lead")) {

            SortInstance.getInstance().setSortby("");
            LeadSortCustomDialogClass.leadsortbystatus = "posted_date_l_o";
            ClearPostDataFilter();

            ClearFollowDataFilter();

            ClearLeadStatusFilter();

            leadstatusJsonArray = new JSONArray();

        }

        Log.e("LeadsFrag ", "Detail " + mSharedPreferences.getString(LeadConstants.LEAD_STATUS_INFO, ""));

        if (!mSharedPreferences.getString(LeadConstants.LEAD_POST_TODAY, "").equals("") ||
                !mSharedPreferences.getString(LeadConstants.LEAD_POST_YESTER, "").equals("") ||
                !mSharedPreferences.getString(LeadConstants.LEAD_POST_7DAYS, "").equals("") ||
                !mSharedPreferences.getString(LeadConstants.LEAD_POST_15DAYS, "").equals("") ||
                !mSharedPreferences.getString(LeadConstants.LEAD_POST_CUSTDATE, "").equals("") ||

                !mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_TMRW, "").equals("") ||
                !mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_TODAY, "").equals("") ||
                !mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_YESTER, "").equals("") ||
                !mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_7DAYS, "").equals("") ||
                !mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_CUSTDATE, "").equals("") ||

                !mSharedPreferences.getString(LeadConstants.LEAD_STATUS_INFO, "").equals("")
                ) {

            lead_filter.setBackgroundResource(R.drawable.filter_icon_yellow);

        } else {
            lead_filter.setBackgroundResource(R.drawable.filter_48x18);

        }


    }

}
