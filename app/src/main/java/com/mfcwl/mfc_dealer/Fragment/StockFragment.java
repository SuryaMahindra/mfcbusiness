package com.mfcwl.mfc_dealer.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.graphics.drawable.DrawerArrowDrawable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.mfcwl.mfc_dealer.ASMModel.stockWhere;
import com.mfcwl.mfc_dealer.Activity.Leads.FilterInstance;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Activity.SplashActivity;
import com.mfcwl.mfc_dealer.Activity.StockFilterActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.AppConstants;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GAConstants;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import butterknife.ButterKnife;
import imageeditor.base.BaseFragment;
import rdm.PowerBIFragment;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.iv_toggle_back_btn;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.layout_back_btn;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.navigation;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchImage;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchicon;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toggle;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toolbar;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.tv_header_title;
import static com.mfcwl.mfc_dealer.Activity.StockFilterActivity.endPriceValues;
import static com.mfcwl.mfc_dealer.Activity.StockFilterActivity.kilometerValues;
import static com.mfcwl.mfc_dealer.Activity.StockFilterActivity.startPriceValues;
import static com.mfcwl.mfc_dealer.Activity.StockFilterActivity.yearValues;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ASM;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_STATEHEAD;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ZONALHEAD;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_DEALER_CRE;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_PROCUREMENT;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_SALES;

public class StockFragment extends BaseFragment {

    LinearLayout filter,ll_asm_stock_back_btn;
    public static LinearLayout headerdata;
    public static Activity activity;
    public static ImageView stock_filter_icon,iv_back_asm_tab_stock_fragment;
    public static TextView stock_filter_icon_red;
    public static TabLayout tabLayout;
    RelativeLayout pricesort;
    RelativeLayout postdatesort;
    RelativeLayout kmssport;
    RelativeLayout yearsort;

    ImageView priceimg;
    ImageView postedimg;
    ImageView kmsimg;
    ImageView yearimg;

    public boolean priceFlag = false;
    public boolean posteddateFlag = false;
    public boolean kmsFlag = false;
    public boolean yearFlag = false;
    public int tpostionCur = 0;

    public static boolean onBack = false;
    public static String stock = "";
    public static  stockWhere where;


    public static String TAG= StockFragment.class.getSimpleName();
    private String dealerName, dealerCode;

    public StockFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dealerCode = getArguments().getString("DCODE");
            dealerName = getArguments().getString("DNAME");
        }
    }

    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.i(TAG, "onCreateView: ");

        View view = inflater.inflate(R.layout.asm_tab_stock_fragment, container, false);
        tabLayout = view.findViewById(R.id.tabs);
        toolbar.setBackgroundColor(getResources().getColor(R.color.black));
        ll_asm_stock_back_btn=view.findViewById(R.id.ll_asm_stock_back_btn);
        iv_back_asm_tab_stock_fragment=view.findViewById(R.id.iv_back_asm_tab_stock_fragment);
        String userType = CommonMethods.getstringvaluefromkey(activity, "user_type");

        if (userType.equalsIgnoreCase("dealer") ||
                userType.equalsIgnoreCase(USER_TYPE_DEALER_CRE) ||
                userType.equalsIgnoreCase(USER_TYPE_SALES) ||
                userType.equalsIgnoreCase(USER_TYPE_PROCUREMENT)) {

            ll_asm_stock_back_btn.setVisibility(View.GONE);
            iv_back_asm_tab_stock_fragment.setVisibility(View.GONE);
        }
        else if(userType.equalsIgnoreCase(USERTYPE_ASM))
        {
            ll_asm_stock_back_btn.setVisibility(View.VISIBLE);
            iv_back_asm_tab_stock_fragment.setVisibility(View.VISIBLE);
            toggle.setDrawerIndicatorEnabled(false);
            searchicon.setVisibility(View.VISIBLE);
            searchImage.setVisibility(View.VISIBLE);
            toggle.setHomeAsUpIndicator(R.drawable.ic_new_hamburger_menu);
            navigation.setVisibility(View.GONE);
        }

        else {

            ll_asm_stock_back_btn.setVisibility(View.GONE);
            iv_back_asm_tab_stock_fragment.setVisibility(View.GONE);
            navigation.setVisibility(View.VISIBLE);
        }


        try {
            CommonMethods.MemoryClears();
            setHasOptionsMenu(true);
            SplashActivity.progress = true;
            StockFragment.onBack = false;
            tabLayout.getTag();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //  Log.e("tag", tabLayout.getTag() + " ");

        ButterKnife.bind(view);
        activity = getActivity();

        stock_filter_icon = view.findViewById(R.id.stock_filter_icon);

       /* Log.e("certifiedval", "certifiedval=" + CommonMethods.getstringvaluefromkey(activity, "certifiedval"));
        Log.e("price_valuesval", "price_valuesval=" + CommonMethods.getstringvaluefromkey(activity, "price_valuesval"));
        Log.e("yearval", "yearval=" + CommonMethods.getstringvaluefromkey(activity, "yearval"));
        Log.e("kmsval", "kmsval=" + CommonMethods.getstringvaluefromkey(activity, "kmsval"));
*/
        if (CommonMethods.getstringvaluefromkey(activity, "certifiedval").equalsIgnoreCase("true")
                || CommonMethods.getstringvaluefromkey(activity, "price_valuesval").equalsIgnoreCase("true")
                || CommonMethods.getstringvaluefromkey(activity, "yearval").equalsIgnoreCase("true")
                || CommonMethods.getstringvaluefromkey(activity, "kmsval").equalsIgnoreCase("true")
                || CommonMethods.getstringvaluefromkey(activity, "thirtydaycheckbox").equalsIgnoreCase("true")
                || CommonMethods.getstringvaluefromkey(activity, "photocounts1").equalsIgnoreCase("true")) {

            if (yearValues.equalsIgnoreCase("2002") && kilometerValues.equalsIgnoreCase("1000000")
                    && startPriceValues.equalsIgnoreCase("0") && endPriceValues.equalsIgnoreCase("10000000")) {

                if (CommonMethods.getstringvaluefromkey(activity, "thirtydaycheckbox").equalsIgnoreCase("true")
                        || CommonMethods.getstringvaluefromkey(activity, "photocounts1").equalsIgnoreCase("true")
                        || CommonMethods.getstringvaluefromkey(activity, "certifiedval").equalsIgnoreCase("true")) {

                    stock_filter_icon.setBackgroundResource(R.drawable.filter_48x18);
                 //   Log.e("filter_icon_yellow", "filter_icon_yellow=2s1s");
                }
                if (CommonMethods.getstringvaluefromkey(activity, "thirtydaycheckbox").equalsIgnoreCase("true")) {
                    stock_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);
                  //  Log.e("filter_icon_yellow", "filter_icon_yellow=2s1s");
                }
            } else {

                if (!FilterInstance.getInstance().getNotification().equals("")) {
                    stock_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);
                  //  Log.e("filter_icon_yellow", "filter_icon_yellow=1ss");

                } else {
                    if (!yearValues.equalsIgnoreCase("2002")
                            || !kilometerValues.equalsIgnoreCase("1000000")
                            || !startPriceValues.equalsIgnoreCase("0")
                            || !endPriceValues.equalsIgnoreCase("10000000")) {
                        stock_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);
                     //   Log.e("filter_icon_yellow", "filter_icon_yellow=2sss");

                    } else {
                        stock_filter_icon.setBackgroundResource(R.drawable.filter_48x18);

                    }

                }
             //   Log.e("filter_icon_yellow", "filter_icon_yellow=6");
            }
            //testing
            if (CommonMethods.getstringvaluefromkey(activity, "certifiedval").equalsIgnoreCase("true")) {
                stock_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);
              //  Log.e("filter_icon_yellow", "filter_icon_yellow=1asf");
            }

            if (CommonMethods.getstringvaluefromkey(activity, "thirtydaycheckbox").equalsIgnoreCase("true")) {
                stock_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);
             //   Log.e("filter_icon_yellow", "filter_icon_yellow=2s1s");
            }

        } else {
            if (!FilterInstance.getInstance().getNotification().equals("")
                /*&&!FilterInstance.getInstance().getNotification().equalsIgnoreCase("total")*/) {
                if (FilterInstance.getInstance().getNotification().equalsIgnoreCase("total")) {
                    FilterInstance.getInstance().setNotification("");
                } else {
                    stock_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);
                    //stock_filter_icon.setBackgroundResource(R.drawable.filter_48x18);
                 //   Log.e("filter_icon_yellow", "filter_icon_yellow=5s");
                }

            } else {

               /* Log.e("yearValues", "yearValues=" + yearValues);
                Log.e("kilometerValues", "kilometerValues=" + kilometerValues);
                Log.e("startPriceValues", "startPriceValues=" + startPriceValues);
                Log.e("endPriceValues", "endPriceValues=" + endPriceValues);
*/

                if (!yearValues.equalsIgnoreCase("2002")
                        || !kilometerValues.equalsIgnoreCase("1000000")
                        || !startPriceValues.equalsIgnoreCase("0")
                        || !endPriceValues.equalsIgnoreCase("10000000")) {

                    if (!kilometerValues.equalsIgnoreCase("200000")) {
                        stock_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);
                     //   Log.e("filter_icon_yellow", "filter_icon_yellow=2s");
                    } else {
                        stock_filter_icon.setBackgroundResource(R.drawable.filter_48x18);
                    }

                } else {
                    stock_filter_icon.setBackgroundResource(R.drawable.filter_48x18);

                }

            }

            if (CommonMethods.getstringvaluefromkey(activity, "thirtydaycheckbox").equalsIgnoreCase("true")) {
                stock_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);
             //   Log.e("filter_icon_yellow", "filter_icon_yellow=2s1s");
            }

        }
        headerdata = view.findViewById(R.id.headerdata);
        //  bottom_topAnimation();
        if(iv_back_asm_tab_stock_fragment.getVisibility()==View.VISIBLE)
        {
            iv_back_asm_tab_stock_fragment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i(TAG, "onClick: "+"clicked on image view");
                    loadFragment(new PowerBIFragment(activity));
                }
            });

        }

        filter = view.findViewById(R.id.stock_filter);
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

           /*     CommonMethods.setvalueAgainstKey(activity, "stocklist", "false");

                Intent in_main = new Intent(activity, ImageUploadActivity1.class);
                in_main.putExtra("stock_id","");
                ImageLibraryInstance.getInstance().setUploadstatus("uploadstatus");
                CommonMethods.setvalueAgainstKey(activity,"stock_id","191942");
                activity.startActivity(in_main);*/
                if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
                    Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.stock_filter, GlobalText.android);
                }else{
                    Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.asm_stock_filter, GlobalText.android);
                }

                Intent intent = new Intent(getContext(), StockFilterActivity.class);
                startActivity(intent);
                // bottom_topAnimation();


            }
        });

        LinearLayout sort = view.findViewById(R.id.stock_sort);
        sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
                    Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.stock_short, GlobalText.android);
                }else{
                    Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.asm_stock_sortby, GlobalText.android);
                }


                SortCustomDialogClass sortCustomDialogClass = new SortCustomDialogClass(getContext());
                sortCustomDialogClass.show();
                // top_bottomAnimation();
            }
        });

        final ViewPager viewPager = view.findViewById(R.id.viewpager);
        viewPager.setAdapter(new PagerAdapter(getFragmentManager(), tabLayout.getTabCount()));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(Color.parseColor("#aaaaaa"), Color.parseColor("#2d2d2d"));
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#e5e5e5"));

        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));
        //CommonMethods.setvalueAgainstKey(activity,"storelistfilter","true");

        /*if (CommonMethods.getstringvaluefromkey(activity, "booknowbtn").equalsIgnoreCase("true")) {
            Log.e("set", "set1");
            viewPager.setCurrentItem(1);
            stock = "Booked_Stocks";
        } else */
        if (CommonMethods.getstringvaluefromkey(activity, "bookfilter").equalsIgnoreCase("true")
        ||CommonMethods.getstringvaluefromkey(activity, "statuslist").equalsIgnoreCase("asmbook")) {
            Log.e("set", "set2");
            viewPager.setCurrentItem(1);
            stock = "Booked_Stocks";
        }/* else if (Stocklisting.getInstance().getStockstatus().equals("BookedStockFrag")) {
            viewPager.setCurrentItem(1);
            stock = "Booked_Stocks";
        }*/ else {
         //   Log.e("set", "set3");
            viewPager.setCurrentItem(0);
            stock = "Store_Stocks";
        }


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if (tab.getPosition() == 0) {
                    Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.store_stock, GlobalText.android);
                    stock = "Store_Stocks";
                    StockStoreFrag.sortbyapply();
                    // StockStoreFrag.swipeRefresh_s_applyapply();

                }
                if (tab.getPosition() == 1) {
                    String user_type = CommonMethods.getstringvaluefromkey(getActivity(), "user_type");
                    if (user_type.equalsIgnoreCase(USER_TYPE_DEALER_CRE) ||
                            user_type.equalsIgnoreCase(USER_TYPE_SALES)) {
                        Toast.makeText(getActivity(), "You are not authorized", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.booked_stock, GlobalText.android);

                    stock = "Booked_Stocks";
                    BookedStockFrag.sortbyapply();

                }
                tpostionCur = tab.getPosition();

              //  Log.e("onTabSelected=", "onTabSelected=" + tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                // Log.e("onTabUnselected=","onTabUnselected=" +tab.getPosition());

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                // Log.e("onTabReselected=","onTabReselected=" +tab.getPosition());

            }
        });

        return view;
    }


    public static void top_bottomAnimation() {
        Animation hide = AnimationUtils.loadAnimation(activity, R.anim.top_bottom);
        headerdata.startAnimation(hide);
    }

    public static void bottom_topAnimation() {
        Animation hide = AnimationUtils.loadAnimation(activity, R.anim.bottom_top);
        headerdata.startAnimation(hide);
    }


    public class PagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        // tab titles
        private String[] tabTitles = new String[]{"Store Stocks", "Booked Stocks"};

        // overriding getPageTitle()
        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }

        public PagerAdapter(FragmentManager fm, int mNumOfTabs) {
            super(fm);
            this.mNumOfTabs = mNumOfTabs;
        }


        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    // stock = "Store_Stocks";
                    return new StockStoreFrag();
                case 1:
                    // stock = "Booked_Stocks";
                    return new BookedStockFrag();

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return tabTitles.length;
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
            Application.getInstance().trackScreenView(getActivity(),GlobalText.Stock_Fragment);

            if (tv_header_title != null) {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Stock");
            }

        }
        if (
                CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(AppConstants.LOGINTYPE_DEALERCRE) ||
                CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(AppConstants.LOGINTYPE_PROCUREMENT) ||
                CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(AppConstants.LOGINTYPE_SLAES)||
                        CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ZONALHEAD) ||
                        CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_STATEHEAD)) {
            if (tv_header_title != null) {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Stock");
            }
        }

        if(CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ASM))
        {

            toolbar.setBackgroundColor(getResources().getColor(R.color.black));
            toggle.setDrawerIndicatorEnabled(false);
            toggle.setHomeAsUpIndicator(R.drawable.ic_new_hamburger_menu);
            if (tv_header_title != null) {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Stock");
            }

            Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_STOCK_LISTING,CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));

        }
        /*else if(CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ASM))
        {
            toggle.setDrawerIndicatorEnabled(false);
            toggle.setHomeAsUpIndicator(R.drawable.ic_new_hamburger_menu);
            toolbar.setBackgroundColor(getResources().getColor(R.color.all_dealers_fragment_bg));
            tv_header_title.setText("Stocks");
        }*/else{
            Application.getInstance().trackScreenView(getActivity(),GlobalText.asm_stock_listing);
        }
        searchicon.setVisibility(View.VISIBLE);
        try {
            if (tv_header_title != null) {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Stocks");

            }
        } catch (Exception e) {
            Log.e(TAG, "Exception: ");
        }

    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        getActivity().invalidateOptionsMenu();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }

    public class SortCustomDialogClass extends Dialog implements View.OnClickListener {

        public Context activity;

        public Activity act;
        public Dialog d;
        public Button yes;
        public TextView pricelbl, posteddatelbl, kmslbl, yearlbl;

        public SortCustomDialogClass(@NonNull Context context) {
            super(context);
            this.activity = context;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.stock_sort_apply:
                    Log.e("tpostionCur", "tpostionCur=" + tpostionCur);
                    if (tpostionCur == 0) {
                        StockStoreFrag.sortbyapply();
                    } else {
                        BookedStockFrag.sortbyapply();
                    }

                    dismiss();
                    break;
                default:
                    break;
            }
            dismiss();
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            act = getActivity();
            Log.i(TAG, "onCreate: "+act.getClass().getSimpleName());
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.stock_sort_dialog);
            if(tv_header_title!=null)
            {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Stocks");
            }
            searchicon.setVisibility(View.VISIBLE);
            yes = findViewById(R.id.stock_sort_apply);
            yes.setOnClickListener(this);

            pricesort = findViewById(R.id.pricesort);
            postdatesort = findViewById(R.id.postdatesort);
            kmssport = findViewById(R.id.kmssport);
            yearsort = findViewById(R.id.yearsort);

            pricelbl = findViewById(R.id.pricelbl);
            posteddatelbl = findViewById(R.id.posteddatelbl);
            kmslbl = findViewById(R.id.kmslbl);
            yearlbl = findViewById(R.id.yearlbl);

            priceimg = findViewById(R.id.priceimg);
            postedimg = findViewById(R.id.postedimg);
            kmsimg = findViewById(R.id.kmsimg);
            yearimg = findViewById(R.id.yearimg);

            priceimg.setVisibility(View.INVISIBLE);
            postedimg.setVisibility(View.INVISIBLE);
            kmsimg.setVisibility(View.INVISIBLE);
            yearimg.setVisibility(View.VISIBLE);
            yearFlag = true;
            yearlbl.setText("Year(Latest-Old)");

          //  Log.e("sortbystatus", "sortbystatus=1");


            if (CommonMethods.getstringvaluefromkey(act, "sortbystatus").equalsIgnoreCase("pricelowhigh")) {
                priceimg.setVisibility(View.VISIBLE);
                postedimg.setVisibility(View.INVISIBLE);
                kmsimg.setVisibility(View.INVISIBLE);
                yearimg.setVisibility(View.INVISIBLE);
                pricelbl.setText("Price(High-Low)");
                posteddatelbl.setText("Posted Date");
                kmslbl.setText("Kms");
                yearlbl.setText("Year");
                priceimg.animate().rotation(180).setDuration(180).start();
             //   Log.e("sortbystatus", "sortbystatus=3");
                priceFlag = false;


            } else if (CommonMethods.getstringvaluefromkey(act, "sortbystatus").equalsIgnoreCase("pricehighlow")) {
                priceFlag = true;
                priceimg.setVisibility(View.VISIBLE);
                postedimg.setVisibility(View.INVISIBLE);
                kmsimg.setVisibility(View.INVISIBLE);
                yearimg.setVisibility(View.INVISIBLE);
                pricelbl.setText("Price(Low-High)");
                posteddatelbl.setText("Posted Date");
                kmslbl.setText("Kms");
                yearlbl.setText("Year");
                priceimg.animate().rotation(0).setDuration(180).start();
             //   Log.e("sortbystatus", "sortbystatus=4");

            } else if (CommonMethods.getstringvaluefromkey(act, "sortbystatus").equalsIgnoreCase("postoldtonew")) {

                priceimg.setVisibility(View.INVISIBLE);
                postedimg.setVisibility(View.VISIBLE);
                kmsimg.setVisibility(View.INVISIBLE);
                yearimg.setVisibility(View.INVISIBLE);
             //   Log.e("sortbystatus", "sortbystatus=5");
                posteddatelbl.setText("Posted Date(Latest-Oldest)");
                pricelbl.setText("Price");
                kmslbl.setText("Kms");
                yearlbl.setText("Year");
                posteddateFlag = false;

                postedimg.animate().rotation(180).setDuration(180).start();

            } else if (CommonMethods.getstringvaluefromkey(act, "sortbystatus").equalsIgnoreCase("postnewtoold")) {
                priceimg.setVisibility(View.INVISIBLE);
                postedimg.setVisibility(View.VISIBLE);
                kmsimg.setVisibility(View.INVISIBLE);
                yearimg.setVisibility(View.INVISIBLE);
                posteddatelbl.setText("Posted Date(Oldest-Latest)");
                pricelbl.setText("Price");
                kmslbl.setText("Kms");
                yearlbl.setText("Year");
                posteddateFlag = true;

             //   Log.e("sortbystatus", "sortbystatus=6");

                postedimg.animate().rotation(0).setDuration(180).start();

            } else if (CommonMethods.getstringvaluefromkey(act, "sortbystatus").equalsIgnoreCase("kmshightolow")) {

                priceimg.setVisibility(View.INVISIBLE);
                postedimg.setVisibility(View.INVISIBLE);
                kmsimg.setVisibility(View.VISIBLE);
                yearimg.setVisibility(View.INVISIBLE);
                kmsFlag = true;
                kmslbl.setText("Kms(Low-High)");
                posteddatelbl.setText("Posted Date");
                pricelbl.setText("Price");
                yearlbl.setText("Year");
             //   Log.e("sortbystatus", "sortbystatus=7");

                kmsimg.animate().rotation(0).setDuration(180).start();


            } else if (CommonMethods.getstringvaluefromkey(act, "sortbystatus").equalsIgnoreCase("kmslowtohigh")) {
                priceimg.setVisibility(View.INVISIBLE);
                postedimg.setVisibility(View.INVISIBLE);
                kmsimg.setVisibility(View.VISIBLE);
                yearimg.setVisibility(View.INVISIBLE);
                kmslbl.setText("Kms(High-Low)");
                posteddatelbl.setText("Posted Date");
                pricelbl.setText("Price");
                yearlbl.setText("Year");
             //   Log.e("sortbystatus", "sortbystatus=8");

                kmsimg.animate().rotation(180).setDuration(180).start();

                kmsFlag = false;

            } else if (CommonMethods.getstringvaluefromkey(act, "sortbystatus").equalsIgnoreCase("yearnewtoold")) {

                priceimg.setVisibility(View.INVISIBLE);
                postedimg.setVisibility(View.INVISIBLE);
                kmsimg.setVisibility(View.INVISIBLE);
                yearimg.setVisibility(View.VISIBLE);
                yearFlag = true;
            //    Log.e("sortbystatus", "sortbystatus=9");
                yearlbl.setText("Year(Old-New)");
                kmslbl.setText("Kms");
                posteddatelbl.setText("Posted Date");
                pricelbl.setText("Price");

                yearimg.animate().rotation(0).setDuration(180).start();

            } else if (CommonMethods.getstringvaluefromkey(act, "sortbystatus").equalsIgnoreCase("yearoldtonew")) {
                priceimg.setVisibility(View.INVISIBLE);
                postedimg.setVisibility(View.INVISIBLE);
                kmsimg.setVisibility(View.INVISIBLE);
                yearimg.setVisibility(View.VISIBLE);
                yearFlag = false;
                yearlbl.setText("Year(New-Old)");
                kmslbl.setText("Kms");
                posteddatelbl.setText("Posted Date");
                pricelbl.setText("Price");
             //   Log.e("sortbystatus", "sortbystatus=10");

                yearimg.animate().rotation(180).setDuration(180).start();


            } else {
             //   Log.e("sortbystatus", "sortbystatus=11");

                priceimg.setVisibility(View.INVISIBLE);
                postedimg.setVisibility(View.VISIBLE);
                kmsimg.setVisibility(View.INVISIBLE);
                yearimg.setVisibility(View.INVISIBLE);
                posteddatelbl.setText("Posted Date(Latest-Oldest)");
                kmslbl.setText("Kms");
                pricelbl.setText("Price");
                yearlbl.setText("Year");

                postedimg.animate().rotation(180).setDuration(180).start();
                posteddateFlag = false;
            }
      /*      } else {
                priceimg.setVisibility(View.INVISIBLE);
                postedimg.setVisibility(View.VISIBLE);
                kmsimg.setVisibility(View.INVISIBLE);
                yearimg.setVisibility(View.INVISIBLE);
                Log.e("sortbystatus", "sortbystatus=5");
                posteddatelbl.setText("Posted Date(Latest-Oldest)");
                pricelbl.setText("Price");
                kmslbl.setText("Kms");
                yearlbl.setText("Year");
                posteddateFlag = false;

                postedimg.animate().rotation(180).setDuration(180).start();
            }*/
            /*}else{


                if (CommonMethods.getstringvaluefromkey(act, "sortbystatusbook").equalsIgnoreCase("pricelowhigh")) {
                    priceimg.setVisibility(View.VISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);
                    pricelbl.setText("Price(High-Low)");
                    kmslbl.setText("Kms");
                    yearlbl.setText("Year");
                    posteddatelbl.setText("Posted Date");

                    priceimg.animate().rotation(180).setDuration(180).start();
                    Log.e("sortbystatus","sortbystatus=3");
                    priceFlag = false;


                } else if (CommonMethods.getstringvaluefromkey(act, "sortbystatusbook").equalsIgnoreCase("pricehighlow")) {
                    priceFlag = true;
                    priceimg.setVisibility(View.VISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);
                    pricelbl.setText("Price(Low-High)");
                    kmslbl.setText("Kms");
                    yearlbl.setText("Year");
                    posteddatelbl.setText("Posted Date");
                    priceimg.animate().rotation(0).setDuration(180).start();
                    Log.e("sortbystatus","sortbystatus=4");

                } else if (CommonMethods.getstringvaluefromkey(act, "sortbystatusbook").equalsIgnoreCase("postoldtonew")) {

                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.VISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);
                    Log.e("sortbystatus","sortbystatus=5");
                    posteddatelbl.setText("Posted Date(Latest-Oldest)");
                    kmslbl.setText("Kms");
                    yearlbl.setText("Year");
                    pricelbl.setText("Price");

                    posteddateFlag = false;

                    postedimg.animate().rotation(180).setDuration(180).start();

                } else if (CommonMethods.getstringvaluefromkey(act, "sortbystatusbook").equalsIgnoreCase("postnewtoold")) {
                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.VISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);
                    posteddatelbl.setText("Posted Date(Oldest-Latest)");
                    kmslbl.setText("Kms");
                    yearlbl.setText("Year");
                    pricelbl.setText("Price");
                    posteddateFlag = true;

                    Log.e("sortbystatus","sortbystatus=6");

                    postedimg.animate().rotation(0).setDuration(180).start();

                } else if (CommonMethods.getstringvaluefromkey(act, "sortbystatusbook").equalsIgnoreCase("kmshightolow")) {

                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.VISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);
                    kmsFlag = true;
                    kmslbl.setText("Kms(Low-High)");
                    posteddatelbl.setText("Posted Date");
                    yearlbl.setText("Year");
                    pricelbl.setText("Price");
                    Log.e("sortbystatus","sortbystatus=7");

                    kmsimg.animate().rotation(0).setDuration(180).start();


                } else if (CommonMethods.getstringvaluefromkey(act, "sortbystatusbook").equalsIgnoreCase("kmslowtohigh")) {
                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.VISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);
                    kmslbl.setText("Kms(High-Low)");
                    posteddatelbl.setText("Posted Date");
                    yearlbl.setText("Year");
                    pricelbl.setText("Price");
                    Log.e("sortbystatus","sortbystatus=8");

                    kmsimg.animate().rotation(180).setDuration(180).start();

                    kmsFlag = false;

                } else if (CommonMethods.getstringvaluefromkey(act, "sortbystatusbook").equalsIgnoreCase("yearnewtoold")) {

                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.VISIBLE);
                    yearFlag = false;
                    Log.e("sortbystatus","sortbystatus=9");
                    yearlbl.setText("Year(Old-New)");
                    posteddatelbl.setText("Posted Date");
                    pricelbl.setText("Price");
                    kmslbl.setText("Kms");

                    yearimg.animate().rotation(0).setDuration(180).start();

                } else if (CommonMethods.getstringvaluefromkey(act, "sortbystatusbook").equalsIgnoreCase("yearoldtonew")) {
                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.VISIBLE);
                    yearFlag = false;
                    yearlbl.setText("Year(New-Old)");
                    posteddatelbl.setText("Posted Date");
                    pricelbl.setText("Price");
                    kmslbl.setText("Kms");


                    Log.e("sortbystatus","sortbystatus=10");

                    yearimg.animate().rotation(180).setDuration(180).start();


                } else {
                    Log.e("sortbystatus","sortbystatus=11");

                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.VISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);
                    posteddatelbl.setText("Posted Date(Latest-Oldest)");
                    pricelbl.setText("Price");
                    kmslbl.setText("Kms");
                    yearlbl.setText("Year");

                    postedimg.animate().rotation(180).setDuration(180).start();
                    posteddateFlag = false;
                }
            }*/


            pricesort.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    priceimg.setVisibility(View.VISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);


                    if (priceFlag) {
                        priceFlag = false;
                        pricelbl.setText("Price(High-Low)");
                        posteddatelbl.setText("Posted Date");
                        kmslbl.setText("Kms");
                        yearlbl.setText("Year");
                        priceimg.animate().rotation(180).setDuration(180).start();
                        // if (CommonMethods.getstringvaluefromkey(act, "status").equalsIgnoreCase("StockStore")) {
                        CommonMethods.setvalueAgainstKey(act, "sortbystatus", "pricelowhigh");
                       /* }else {
                            CommonMethods.setvalueAgainstKey(act, "sortbystatusbook", "pricelowhigh");
                        }*/

                    } else {
                        pricelbl.setText("Price(Low-High)");
                        posteddatelbl.setText("Posted Date");
                        kmslbl.setText("Kms");
                        yearlbl.setText("Year");
                        // if (CommonMethods.getstringvaluefromkey(act, "status").equalsIgnoreCase("StockStore")) {

                        CommonMethods.setvalueAgainstKey(act, "sortbystatus", "pricehighlow");
                        /*}else {
                            CommonMethods.setvalueAgainstKey(act, "sortbystatusbook", "pricehighlow");
                        }*/
                        priceimg.animate().rotation(0).setDuration(180).start();
                        priceFlag = true;
                    }
                }
            });


            postdatesort.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.VISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);

                    if (posteddateFlag) {
                        posteddateFlag = false;
                        posteddatelbl.setText("Posted Date(Latest-Oldest)");
                        pricelbl.setText("Price");
                        pricelbl.setText("Price");
                        kmslbl.setText("Kms");
                        yearlbl.setText("Year");

                        postedimg.animate().rotation(180).setDuration(180).start();
                        // if (CommonMethods.getstringvaluefromkey(act, "status").equalsIgnoreCase("StockStore")) {

                        CommonMethods.setvalueAgainstKey(act, "sortbystatus", "postoldtonew");
                        /*}else {
                            CommonMethods.setvalueAgainstKey(act, "sortbystatusbook", "postoldtonew");
                        }*/
                    } else {
                        //if (CommonMethods.getstringvaluefromkey(act, "status").equalsIgnoreCase("StockStore")) {

                        CommonMethods.setvalueAgainstKey(act, "sortbystatus", "postnewtoold");
                        /*}else {
                            CommonMethods.setvalueAgainstKey(act, "sortbystatusbook", "postnewtoold");
                        }*/
                        posteddatelbl.setText("Posted Date(Oldest-Latest)");
                        pricelbl.setText("Price");
                        kmslbl.setText("Kms");
                        yearlbl.setText("Year");
                        postedimg.animate().rotation(0).setDuration(180).start();
                        posteddateFlag = true;
                    }
                }

            });


            kmssport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.VISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);

                    if (kmsFlag) {
                        kmsFlag = false;
                        kmslbl.setText("Kms(High-Low)");
                        pricelbl.setText("Price");
                        yearlbl.setText("Year");
                        posteddatelbl.setText("Posted Date");

                        kmsimg.animate().rotation(180).setDuration(180).start();
                        //if (CommonMethods.getstringvaluefromkey(act, "status").equalsIgnoreCase("StockStore")) {

                        CommonMethods.setvalueAgainstKey(act, "sortbystatus", "kmslowtohigh");
                        /*}else {
                            CommonMethods.setvalueAgainstKey(act, "sortbystatusbook", "kmslowtohigh");
                        }*/
                    } else {
                        kmslbl.setText("Kms(Low-High)");
                        pricelbl.setText("Price");
                        yearlbl.setText("Year");
                        posteddatelbl.setText("Posted Date");
                        // if (CommonMethods.getstringvaluefromkey(act, "status").equalsIgnoreCase("StockStore")) {

                        CommonMethods.setvalueAgainstKey(act, "sortbystatus", "kmshightolow");
                        /*}else {
                            CommonMethods.setvalueAgainstKey(act, "sortbystatusbook", "kmshightolow");
                        }*/
                        kmsimg.animate().rotation(0).setDuration(180).start();
                        kmsFlag = true;
                    }

                }
            });
            yearsort.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.VISIBLE);

                    if (yearFlag) {
                        yearFlag = false;
                        yearlbl.setText("Year(New-Old)");
                        kmslbl.setText("Kms");
                        pricelbl.setText("Price");
                        posteddatelbl.setText("Posted Date");
                        yearimg.animate().rotation(180).setDuration(180).start();
                        // if (CommonMethods.getstringvaluefromkey(act, "status").equalsIgnoreCase("StockStore")) {

                        CommonMethods.setvalueAgainstKey(act, "sortbystatus", "yearoldtonew");
                        /*}else {
                            CommonMethods.setvalueAgainstKey(act, "sortbystatusbook", "yearoldtonew");
                        }*/
                    } else {
                        yearlbl.setText("Year(Old-New)");
                        kmslbl.setText("Kms");
                        pricelbl.setText("Price");
                        posteddatelbl.setText("Posted Date");
                        //if (CommonMethods.getstringvaluefromkey(act, "status").equalsIgnoreCase("StockStore")) {

                        CommonMethods.setvalueAgainstKey(act, "sortbystatus", "yearnewtoold");
                        /*}else{
                        CommonMethods.setvalueAgainstKey(act,"sortbystatusbook","yearnewtoold");
                        }*/
                        yearimg.animate().rotation(0).setDuration(180).start();
                        yearFlag = true;
                    }

                }
            });

        }
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        MenuItem asm_mail = menu.findItem(R.id.asm_mail);
        asm_mail.setVisible(true);

        super.onCreateOptionsMenu(menu, inflater);

    }

    private void loadFragment(Fragment mFragment) {
        Bundle mBundle = new Bundle();
        mBundle.putString("DNAME", dealerName);
        mBundle.putString("DCODE", dealerCode);
        mFragment.setArguments(mBundle);
        switchContent(R.id.fillLayout, mFragment);
    }

    public void switchContent(int id, Fragment fragment) {
        if (activity == null)
            return;
        if (activity instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) activity;
            Fragment frag = fragment;
            mainActivity.switchContent(id, frag);
        }

    }

}
