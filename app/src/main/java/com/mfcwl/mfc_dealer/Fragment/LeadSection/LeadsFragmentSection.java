package com.mfcwl.mfc_dealer.Fragment.LeadSection;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mfcwl.mfc_dealer.Activity.LeadSection.LeadFilterActivitySection;
import com.mfcwl.mfc_dealer.Activity.Leads.LeadsInstance;
import com.mfcwl.mfc_dealer.Activity.Leads.PrivateLeadsConstant;
import com.mfcwl.mfc_dealer.Activity.Leads.WebLeadsConstant;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Fragment.ToolFragment;
import com.mfcwl.mfc_dealer.InstanceCreate.LeadSection.LeadFilterSaveInstance;
import com.mfcwl.mfc_dealer.Interface.LeadSection.FilterApply;
import com.mfcwl.mfc_dealer.Popup.LeadSection.LeadSortCustomDialogClassSection;
import com.mfcwl.mfc_dealer.Procurement.Activity.ProcurementFilterBy;
import com.mfcwl.mfc_dealer.Procurement.Activity.ProcurementSortBy;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.AppConstants;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GAConstants;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import java.lang.ref.WeakReference;

import rdm.PowerBIFragment;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.activity;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.linearLayoutVisible;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.navigation;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchImage;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchicon;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toggle;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toolbar;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.tv_header_title;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ASM;


public class LeadsFragmentSection extends Fragment implements View.OnClickListener {

    private ImageView iv_back_asm_leads_tab,lead_filter, lead_sort;
    private LinearLayout lead_filter_linear, lead_sort_linear,top_layout;
    private TabLayout tabLayout;
    private AppBarLayout appheader;
    private Activity activity;
    private LinearLayout headerdataLeads;
    private final SparseArray<WeakReference<Fragment>> instantiatedFragments = new SparseArray<>();
    private FilterApply filterApply;
    private SharedPreferences mSharedPreferences = null;
    String TAG = getClass().getSimpleName();
    private String dealerName, dealerCode;

    public LeadsFragmentSection() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            dealerCode = getArguments().getString("DCODE");
            dealerName = getArguments().getString("DNAME");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.lead_fragment, container, false);
        toolbar.setBackgroundColor(getResources().getColor(R.color.black));
        if ( CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(AppConstants.LOGINTYPE_DEALER)||
                CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(AppConstants.LOGINTYPE_DEALERCRE) ||
                CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(AppConstants.LOGINTYPE_PROCUREMENT) ||
                CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(AppConstants.LOGINTYPE_SLAES)) {
            if (tv_header_title != null) {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Leads");
            }
        }
        if(CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(USERTYPE_ASM))
        {

            linearLayoutVisible.setVisibility(View.VISIBLE);
            searchicon.setVisibility(View.VISIBLE);
            searchImage.setVisibility(View.VISIBLE);
            if(tv_header_title!=null)
            {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Leads");
                toolbar.setBackgroundColor(getResources().getColor(R.color.black));
                tv_header_title.setTextColor(getResources().getColor(R.color.white));
            }
        }

        InitUI(view);
        onClick(view);

        return view;
    }

    private void InitUI(View view) {
        setHasOptionsMenu(true);

        CommonMethods.setvalueAgainstKey(getActivity(), "asm_pages", "leads_list");

        activity = getActivity();
        filterApply = (FilterApply) activity;
        appheader = view.findViewById(R.id.appheader);
        tabLayout = view.findViewById(R.id.leads_tabs);
        lead_filter = view.findViewById(R.id.lead_filter);
        top_layout=view.findViewById(R.id.top_layout);
        lead_sort = view.findViewById(R.id.lead_sort);
        lead_filter_linear = view.findViewById(R.id.lead_filter_linear);
        lead_sort_linear = view.findViewById(R.id.lead_sort_linear);
        headerdataLeads = view.findViewById(R.id.headerdataLeads);
        iv_back_asm_leads_tab=view.findViewById(R.id.iv_back_asm_leads_tab);

        final ViewPager viewPager = view.findViewById(R.id.leads_viewpager);

        viewPager.setAdapter(new LeadsPagerAdapter(getFragmentManager(), tabLayout.getTabCount()));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(Color.parseColor("#aaaaaa"), Color.parseColor("#2d2d2d"));
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#e5e5e5"));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));

        CommonMethods.setvalueAgainstKey(getActivity(), "PrivateLeadsP", "false");
        CommonMethods.setvalueAgainstKey(getActivity(), "WebLeadsP", "false");

        WebLeadsConstant.getInstance().setWhichleads("AllLeads");
        PrivateLeadsConstant.getInstance().setWhichleads("AllLeads");

        CommonMethods.setvalueAgainstKey(getActivity(), "status", "WebLeads");
        CommonMethods.setvalueAgainstKey(getActivity(), "Pstatus", "");

        if (LeadsInstance.getInstance().getLeaddirection().equals("PrivateLeadsPage")) {
            LeadsInstance.getInstance().setLeaddirection("");
            CommonMethods.setvalueAgainstKey(getActivity(), "status", "PrivateLeads");
            viewPager.setCurrentItem(1);
        }

        if(CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(USERTYPE_ASM))
        {
           // navigation.setVisibility(View.GONE);
            top_layout.setVisibility(View.VISIBLE);
            iv_back_asm_leads_tab.setOnClickListener(this);
        }
        else
        {
            top_layout.setVisibility(View.GONE);
        }

        //ASM //already filter data clear
        if (CommonMethods.getstringvaluefromkey(activity, "leadFilterClear").equalsIgnoreCase("changes")) {

            CommonMethods.setvalueAgainstKey(activity, "leadFilterClear", "no");
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    CommonMethods.setvalueAgainstKey(getActivity(), "status", "WebLeads");
                    filterApply.applyFilter("WebLeads");
                } else {
                    CommonMethods.setvalueAgainstKey(getActivity(), "status", "PrivateLeads");
                    filterApply.applyFilter("PrivateLeads");
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        lead_filter_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CommonMethods.getstringvaluefromkey(getActivity(), "WLEADS").equalsIgnoreCase("WebLeads")) {
                    CommonMethods.setvalueAgainstKey(getActivity(), "WebLeadsP", "false");
                }
                if (CommonMethods.getstringvaluefromkey(getActivity(), "WLEADS").equalsIgnoreCase("PrivateLeads")) {
                    CommonMethods.setvalueAgainstKey(getActivity(), "PrivateLeadsP", "false");
                }

                if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
                   Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.lead_filter, GlobalText.android);
                }else{
                    Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.asm_lead_filter, GlobalText.android);
                }


                Intent intent = new Intent(getActivity(), LeadFilterActivitySection.class);
                startActivityForResult(intent, 200);

            }
        });

        lead_sort_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (CommonMethods.getstringvaluefromkey(getActivity(), "WLEADS").equalsIgnoreCase("WebLeads")) {
                    CommonMethods.setvalueAgainstKey(getActivity(), "WebLeadsP", "false");
                }
                if (CommonMethods.getstringvaluefromkey(getActivity(), "WLEADS").equalsIgnoreCase("PrivateLeads")) {
                    CommonMethods.setvalueAgainstKey(getActivity(), "PrivateLeadsP", "false");
                }

                if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
                    Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.lead_sort_by, GlobalText.android);
                }else{
                    Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.asm_leads_sorts, GlobalText.android);
                }


                LeadSortCustomDialogClassSection sortCustomDialogClass = new LeadSortCustomDialogClassSection(getContext(), getActivity());
                sortCustomDialogClass.show();
            }
        });

        try {
            ((MainActivity) getActivity()).setFragmentRefreshListener(new MainActivity.FragmentRefreshListener() {
                @Override
                public void onRefresh() {
                    // Refresh Your Fragment
                    try {
                        if (CommonMethods.getstringvaluefromkey(getActivity(), "isLeadDetails").equalsIgnoreCase("true")) {
                            CommonMethods.setvalueAgainstKey(getActivity(), "isLeadDetails", "false");

                            if (CommonMethods.getstringvaluefromkey(getActivity(), "WLEADS").equalsIgnoreCase("WebLeads")) {
                                CommonMethods.setvalueAgainstKey(getActivity(), "WebLeadsP", "true");
                            }
                            if (CommonMethods.getstringvaluefromkey(getActivity(), "WLEADS").equalsIgnoreCase("PrivateLeads")) {
                                CommonMethods.setvalueAgainstKey(getActivity(), "PrivateLeadsP", "true");
                            }

                            final ViewPager viewPager = view.findViewById(R.id.leads_viewpager);
                            viewPager.setAdapter(new LeadsPagerAdapter(getFragmentManager(), tabLayout.getTabCount()));
                            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

                            if (CommonMethods.getstringvaluefromkey(getActivity(), "WLEADS").equalsIgnoreCase("WebLeads")) {
                                viewPager.setCurrentItem(0);
                            }
                            if (CommonMethods.getstringvaluefromkey(getActivity(), "WLEADS").equalsIgnoreCase("PrivateLeads")) {
                                viewPager.setCurrentItem(1);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        final MenuItem item = menu.findItem(R.id.notification_bell);
        item.setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back_asm_leads_tab:
                loadFragment(new PowerBIFragment(activity));
                break;
        }
    }

    public class LeadsPagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        private String[] tabTitles = new String[]{" Web Leads ", "Private Leads"};

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }

        public LeadsPagerAdapter(FragmentManager fm, int mNumOfTabs) {
            super(fm);
            this.mNumOfTabs = mNumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return new NormalLeadsFragmentSection();
                case 1:
                    return new PrivateLeadsFragmentSection();
                case 2:
                    return new ToolFragment();

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return tabTitles.length;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            final Fragment fragment = (Fragment) super.instantiateItem(container, position);
            instantiatedFragments.put(position, new WeakReference<>(fragment));
            return fragment;
        }

        @Nullable
        public Fragment getFragment(final int position) {
            final WeakReference<Fragment> wr = instantiatedFragments.get(position);
            if (wr != null) {
                return wr.get();
            } else {
                return null;
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();




        if (LeadFilterSaveInstance.getInstance().getSavedatahashmap().size() != 0 ||
                LeadFilterSaveInstance.getInstance().getStatusarray().length() != 0) {
            lead_filter.setBackgroundResource(R.drawable.filter_icon_yellow);
        } else {
           // lead_filter.setBackgroundResource(R.drawable.filter_48x18);

            if (CommonMethods.getstringvaluefromkey(getActivity(), "user_type").equalsIgnoreCase("dealer")) {
                lead_filter.setBackgroundResource(R.drawable.filter_48x18);
            } else {
                if (!CommonMethods.getstringvaluefromkey(getActivity(), "leads_status").equalsIgnoreCase("")
                ||CommonMethods.getstringvaluefromkey(getActivity(), "leadStatus").equalsIgnoreCase("true")) {
                    lead_filter.setBackgroundResource(R.drawable.filter_icon_yellow);
                }else{
                    lead_filter.setBackgroundResource(R.drawable.filter_48x18);
                }
            }
        }
        linearLayoutVisible.setVisibility(View.VISIBLE);
        searchicon.setVisibility(View.VISIBLE);

        if ( CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(AppConstants.LOGINTYPE_DEALER)||
                CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(AppConstants.LOGINTYPE_DEALERCRE) ||
                CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(AppConstants.LOGINTYPE_PROCUREMENT) ||
                CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(AppConstants.LOGINTYPE_SLAES)) {
            if (tv_header_title != null) {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Leads");
            }
        }
        else if(CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(USERTYPE_ASM))
        {
            toggle.setHomeAsUpIndicator(R.drawable.ic_new_hamburger_menu);
            searchicon.setVisibility(View.VISIBLE);
            toolbar.setBackgroundColor(getResources().getColor(R.color.black));
            searchImage.setVisibility(View.VISIBLE);
            if (tv_header_title != null) {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Leads");
            }
            Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_SALES_LEADS,CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));

        }

    }


    private void loadFragment(Fragment mFragment) {
        Bundle mBundle = new Bundle();
        mBundle.putString("DNAME", dealerName);
        mBundle.putString("DCODE", dealerCode);
        mFragment.setArguments(mBundle);
        switchContent(R.id.fillLayout, mFragment);
    }

    public void switchContent(int id, Fragment fragment) {
        if (activity == null)
            return;
        if (activity instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) activity;
            Fragment frag = fragment;
            mainActivity.switchContent(id, frag);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 100) {
            if (!CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase(""))
                filterApply.applyFilter(CommonMethods.getstringvaluefromkey(activity, "status"));

            Log.i(TAG, "interface -1: ");

        }

        Log.i(TAG, "interface -1.1: ");

    }


}
