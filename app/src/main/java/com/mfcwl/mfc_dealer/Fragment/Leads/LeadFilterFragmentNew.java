package com.mfcwl.mfc_dealer.Fragment.Leads;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.Activity.Leads.FilterInstance;
import com.mfcwl.mfc_dealer.Activity.Leads.LeadConstants;
import com.mfcwl.mfc_dealer.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class LeadFilterFragmentNew extends Fragment {

    public static TextView postdate, followup, leadstatus;
    public static TextView postcalendardate, followupcalendardate, PDfromdate, PDtodate, FWfromdate, FWtodate;
    public static CheckBox chopen, chhot, chwarm, chcold, chlost, chsold;
    public static CheckBox chfollowtmrw, chfollowtoday, chfollowyester, chfollowlast7days;
    public static CheckBox chposttoday, chpostyester, chpost7days, chpost15days;
    CardView card1, card3, card5;
    Activity activity;
    Context c;
    private SharedPreferences mSharedPreferences = null;
    private boolean isoktorepo = false;
    public static JSONArray leadstatusJsonArray = new JSONArray();

    public static String postCustomFromDate = "";
    public static String postCustomToDate = "";
    private String postcalendardates = "";
    private String chooseFromDate = "";
    private String regisMonth = "";

    public static String followCustomFromDate = "";
    public static String followCustomToDate = "";
    private String followcalendardates = "";
    private boolean flagpostdate = true, flagfollowup = true,flagstatus = true;;
    String TAG = getClass().getSimpleName();
    public static FilterInstance mInstance = null;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.layout_leads_filter2, container, false);



        InitUI(view);


        //--new
        mInstance = FilterInstance.getInstance();
        Gson gson = new Gson();
        String mJSon = mSharedPreferences.getString("leadfilter", "");
        if (mJSon != null && mJSon != "") {
            mInstance = gson.fromJson(mJSon, FilterInstance.class);
        }
        leadstatusJsonArray = new JSONArray();
        if (mInstance.getLeadstatusJsonArray() != null) {
            leadstatusJsonArray = mInstance.getLeadstatusJsonArray();
        }
        //--

        if(mSharedPreferences.getString(LeadConstants.LEAD_POST_TODAY,"") != ""){
            card1.setVisibility(View.VISIBLE);
            chposttoday.setChecked(true);

        }
        if(mSharedPreferences.getString(LeadConstants.LEAD_POST_YESTER,"") != ""){
            card1.setVisibility(View.VISIBLE);
            chpostyester.setChecked(true);

        }
        if(mSharedPreferences.getString(LeadConstants.LEAD_POST_7DAYS,"") != ""){
            card1.setVisibility(View.VISIBLE);
            chpost7days.setChecked(true);

        }
        if(mSharedPreferences.getString(LeadConstants.LEAD_POST_15DAYS,"") != ""){
            card1.setVisibility(View.VISIBLE);
            chpost15days.setChecked(true);

        }
        if(mSharedPreferences.getString(LeadConstants.LEAD_POST_CUSTDATE,"") != ""){
            card1.setVisibility(View.VISIBLE);
            String data = mSharedPreferences.getString(LeadConstants.LEAD_POST_CUSTDATE,"");
           String[] splitifonfrom = data.split("@");
            PDfromdate.setText(StringtoDateFromat(splitifonfrom[0]) + " - " + StringtoDateFromat(splitifonfrom[1]));
          //  PDfromdate.setText(splitifonfrom[2] + " " + MonthUtility.Month[Integer.parseInt(splitifonfrom[1])] + " " + splitifonfrom[0] + " -");
            //PDfromdate.setText(mSharedPreferences.getString(LeadConstants.LEAD_POST_CUSTDATE,""));
            //PDtodate.setText(mSharedPreferences.getString(LeadConstants.LEAD_POST_CUSTDATE,""));

        }

        //Follow-Up restore

        if(mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_TMRW,"") != "")
        {
            card3.setVisibility(View.VISIBLE);
            chfollowtmrw.setChecked(true);
        }
        if(mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_TODAY,"") != "")
        {
            card3.setVisibility(View.VISIBLE);
            chfollowtoday.setChecked(true);
        }
        if(mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_YESTER,"") != "")
        {
            card3.setVisibility(View.VISIBLE);
            chfollowyester.setChecked(true);
        }
        if(mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_7DAYS,"") != "")
        {
            card3.setVisibility(View.VISIBLE);
            chfollowlast7days.setChecked(true);
        }

        if(mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_CUSTDATE,"") != "")
        {
            card3.setVisibility(View.VISIBLE);
            String data = mSharedPreferences.getString(LeadConstants.LEAD_FOLUP_CUSTDATE,"");
            String[] splitifonfrom = data.split("@");
            FWfromdate.setText(StringtoDateFromat(splitifonfrom[0]) +" - " + StringtoDateFromat(splitifonfrom[1]));

        }

        //Lead Status

        if(mSharedPreferences.getString(LeadConstants.LEAD_STATUS_INFO,"").equals(""))
        {
            card5.setVisibility(View.GONE);


        }else{
            card5.setVisibility(View.VISIBLE);
            if(LeadFilterFragmentNew.leadstatusJsonArray != null){
                try {
                    for (int i = 0;i<LeadFilterFragmentNew.leadstatusJsonArray.length();i++){
                        String item = LeadFilterFragmentNew.leadstatusJsonArray.get(i).toString();
                        if(item.equalsIgnoreCase("open")){
                            chopen.setChecked(true);
                        }
                        if(item.equalsIgnoreCase("hot")){
                            chhot.setChecked(true);
                        }
                        if(item.equalsIgnoreCase("warm")){
                            chwarm.setChecked(true);
                        }
                        if(item.equalsIgnoreCase("cold")){
                            chcold.setChecked(true);
                        }
                        if(item.equalsIgnoreCase("lost")){
                            chlost.setChecked(true);
                        }
                        if(item.equalsIgnoreCase("sold")){
                            chsold.setChecked(true);
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();

                }
            }


        }



        postdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flagpostdate) {
                    card1.setVisibility(View.VISIBLE);
                    postdate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                    flagpostdate = false;
                } else {
                    postdate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
                    card1.setVisibility(View.GONE);
                    flagpostdate = true;
                }
            }
        });

        followup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flagfollowup) {
                    followup.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                    card3.setVisibility(View.VISIBLE);
                    flagfollowup = false;
                } else {
                    followup.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
                    card3.setVisibility(View.GONE);
                    flagfollowup = true;
                }
            }
        });

        leadstatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flagstatus) {
                    leadstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                    card5.setVisibility(View.VISIBLE);
                    flagstatus = false;
                } else {
                    leadstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
                    card5.setVisibility(View.GONE);
                    flagstatus = true;
                }
            }
        });


        chopen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                //remove if already presents in json arry
                try {
                    for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                        String mOpen = leadstatusJsonArray.getString(i);
                        if (mOpen.equalsIgnoreCase("open")) {
                            leadstatusJsonArray.remove(i);

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (isChecked) {

                    leadstatusJsonArray.put("open");


                }
            }
        });

        chhot.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                try {
                    for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                        String mhot = leadstatusJsonArray.getString(i);
                        if (mhot.equalsIgnoreCase("hot")) {
                            leadstatusJsonArray.remove(i);

                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (isChecked) {

                    leadstatusJsonArray.put("hot");

                }
            }
        });
        chwarm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                try {
                    for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                        String mWarm = leadstatusJsonArray.getString(i);
                        if (mWarm.equalsIgnoreCase("warm")) {
                            leadstatusJsonArray.remove(i);

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (isChecked) {

                    leadstatusJsonArray.put("warm");

                }
            }
        });
        chcold.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                try {
                    for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                        String mCold = leadstatusJsonArray.getString(i);
                        if (mCold.equalsIgnoreCase("cold")) {
                            leadstatusJsonArray.remove(i);

                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                if (isChecked) {

                    leadstatusJsonArray.put("cold");

                }
            }
        });
        chlost.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                try {
                    for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                        String mLost = leadstatusJsonArray.getString(i);
                        if (mLost.equalsIgnoreCase("lost")) {
                            leadstatusJsonArray.remove(i);

                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (isChecked) {

                    leadstatusJsonArray.put("lost");

                }
            }
        });

        chsold.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                try {
                    for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                        String mSold = leadstatusJsonArray.getString(i);
                        if (mSold.equalsIgnoreCase("sold")) {
                            leadstatusJsonArray.remove(i);

                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (isChecked) {

                    leadstatusJsonArray.put("sold");

                }
            }
        });


        //post date info

        chposttoday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                if(isChecked){
                    //clear other checkboxes
                    chpostyester.setChecked(false);
                    chpost7days.setChecked(false);
                    chpost15days.setChecked(false);
                    //clear custom date as well
                    PDfromdate.setText("");
                    PDtodate.setText("");
                }

            }
        });

        chpostyester.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    //clear other checkboxes
                    chposttoday.setChecked(false);
                    chpost7days.setChecked(false);
                    chpost15days.setChecked(false);
                    //clear custom date as well
                    PDfromdate.setText("");
                    PDtodate.setText("");

                }
            }
        });

        chpost7days.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    //clear other checkboxes
                    chposttoday.setChecked(false);
                    chpostyester.setChecked(false);
                    chpost15days.setChecked(false);
                    //clear custom date as well
                    PDfromdate.setText("");
                    PDtodate.setText("");

                }
            }
        });

        chpost15days.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    //clear other checkboxes
                    chposttoday.setChecked(false);
                    chpostyester.setChecked(false);
                    chpost7days.setChecked(false);
                    //clear custom date as well
                    PDfromdate.setText("");
                    PDtodate.setText("");

                }
            }
        });


        postcalendardate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                chposttoday.setChecked(false);
                chpostyester.setChecked(false);
                chpost7days.setChecked(false);
                chpost15days.setChecked(false);
                chooseFromDate = "fromdate";
                setPostCustomDateFrom(view);
            }
        });


         //End of Posted-up Filter

        //Start Follow-up Filter



        chfollowtmrw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    //clear other checkboxes
                    chfollowtoday.setChecked(false);
                    chfollowlast7days.setChecked(false);
                    chfollowyester.setChecked(false);
                    //clear custom date as well
                    FWfromdate.setText("");
                    FWtodate.setText("");
                }
            }
        });

        chfollowtoday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    //clear other checkboxes
                    chfollowtmrw.setChecked(false);
                    chfollowlast7days.setChecked(false);
                    chfollowyester.setChecked(false);
                    //clear custom date as well
                    FWfromdate.setText("");
                    FWtodate.setText("");
                }
            }
        });

        chfollowyester.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    //clear other checkboxes
                    chfollowtmrw.setChecked(false);
                    chfollowlast7days.setChecked(false);
                    chfollowtoday.setChecked(false);
                    //clear custom date as well
                    FWfromdate.setText("");
                    FWtodate.setText("");
                }
            }
        });

        chfollowlast7days.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    //clear other checkboxes
                    chfollowtmrw.setChecked(false);
                    chfollowyester.setChecked(false);
                    chfollowtoday.setChecked(false);
                    //clear custom date as well
                    FWfromdate.setText("");
                    FWtodate.setText("");
                }
            }
        });

        followupcalendardate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chfollowtmrw.setChecked(false);
                chfollowyester.setChecked(false);
                chfollowtoday.setChecked(false);
                chfollowlast7days.setChecked(false);


                setFollowCustomDateFrom(view);
            }
        });

        return view;
    }



    private void InitUI(View view) {
        mSharedPreferences = getActivity().getSharedPreferences("MFC", Context.MODE_PRIVATE);
        activity = getActivity();
        c = getContext();
        card1 = view.findViewById(R.id.card1);
        card3 = view.findViewById(R.id.card3);
        card5 = view.findViewById(R.id.card5);
        card1.setVisibility(View.GONE);
        card3.setVisibility(View.GONE);
        card5.setVisibility(View.GONE);
        postdate = view.findViewById(R.id.postdate);
        followup = view.findViewById(R.id.followup);
        leadstatus = view.findViewById(R.id.leadstatus);

        PDfromdate = view.findViewById(R.id.fromdate);
        PDtodate = view.findViewById(R.id.todate);

        FWfromdate = view.findViewById(R.id.fromdatefoll);
        FWtodate = view.findViewById(R.id.todatefoll);

        postcalendardate = view.findViewById(R.id.postcalendardate);
        followupcalendardate = view.findViewById(R.id.followupcalendardate);

        postdate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
        followup.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
        leadstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);

        chopen = view.findViewById(R.id.chopen);
        chhot = view.findViewById(R.id.chhot);
        chwarm = view.findViewById(R.id.chwarm);
        chcold = view.findViewById(R.id.chcold);
        chlost = view.findViewById(R.id.chlost);
        chsold = view.findViewById(R.id.chsold);

        chposttoday = view.findViewById(R.id.chposttoday);
        chpostyester = view.findViewById(R.id.chpostyester);
        chpost7days = view.findViewById(R.id.chpost7days);
        chpost15days = view.findViewById(R.id.chpost15days);

        chfollowtmrw = view.findViewById(R.id.chfollowtmrw);
        chfollowtoday = view.findViewById(R.id.chfollowtoday);
        chfollowyester = view.findViewById(R.id.chfollowyester);
        chfollowlast7days = view.findViewById(R.id.chfollowlast7days);

        card1 = view.findViewById(R.id.card1);
        card3 = view.findViewById(R.id.card3);
        card5 = view.findViewById(R.id.card5);
        card1.setVisibility(View.GONE);
        card3.setVisibility(View.GONE);
        card5.setVisibility(View.GONE);


    }


    public void setPostCustomDateFrom(View v) {


        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
            // when dialog box is closed, below method will be called.
            public void onDateSet(DatePicker view, int selectedYear,
                                  int selectedMonth, int selectedDay) {
                if (isoktorepo) {
                    myCalendar.set(Calendar.YEAR, selectedYear);
                    myCalendar.set(Calendar.MONTH, selectedMonth);
                    myCalendar.set(Calendar.DAY_OF_MONTH, selectedDay);

                    //System.out.println();
                    String strselectedDay = String.valueOf(selectedDay);
                    if (strselectedDay.length() == 1) {
                        strselectedDay = "0" + strselectedDay;
                    }
                    String strselectedMonth = String.valueOf(selectedMonth + 1);
                    if (strselectedMonth.length() == 1) {
                        strselectedMonth = "0" + strselectedMonth;
                    }

                    postcalendardates = selectedYear + "-" + strselectedMonth + "-"
                            + strselectedDay;
                    postCustomFromDate = postcalendardates;


                    MethodofMonth(strselectedMonth);


                        PDfromdate.setText(strselectedDay + " " + regisMonth + " " + selectedYear + " -");


                }
                isoktorepo = false;
                setPostCustomDateTo(view);
            }
        };


        final DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                datePickerListener, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            dialog.cancel();
                            isoktorepo = false;
                        }
                    }
                });

        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    @SuppressLint("NewApi")
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            isoktorepo = true;

                            DatePicker datePicker = datePickerDialog
                                    .getDatePicker();
                            datePickerListener.onDateSet(datePicker,
                                    datePicker.getYear(),
                                    datePicker.getMonth(),
                                    datePicker.getDayOfMonth());

                        }
                    }
                });

        long currentTime = System.currentTimeMillis();
        datePickerDialog.getDatePicker().setMaxDate(currentTime);

        datePickerDialog.setCancelable(false);
        datePickerDialog.show();


    }

    public void setPostCustomDateTo(View v) {

        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
            // when dialog box is closed, below method will be called.
            public void onDateSet(DatePicker view, int selectedYear,
                                  int selectedMonth, int selectedDay) {
                if (isoktorepo) {
                    myCalendar.set(Calendar.YEAR, selectedYear);
                    myCalendar.set(Calendar.MONTH, selectedMonth);
                    myCalendar.set(Calendar.DAY_OF_MONTH, selectedDay);

                   // System.out.println();
                    String strselectedDay = String.valueOf(selectedDay);
                    if (strselectedDay.length() == 1) {
                        strselectedDay = "0" + strselectedDay;
                    }
                    String strselectedMonth = String.valueOf(selectedMonth + 1);
                    if (strselectedMonth.length() == 1) {
                        strselectedMonth = "0" + strselectedMonth;
                    }

                    postcalendardates = selectedYear + "-" + strselectedMonth + "-"
                            + strselectedDay;

                    postCustomToDate = postcalendardates;
                    MethodofMonth(strselectedMonth);

                    PDtodate.setText(strselectedDay + " " + regisMonth + " " + selectedYear);

                }


                isoktorepo = false;
            }
        };


        final DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                datePickerListener, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                                PDfromdate.setText("");
                                PDtodate.setText("");

                            dialog.cancel();
                            isoktorepo = false;
                        }
                    }
                });

        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    @SuppressLint("NewApi")
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            isoktorepo = true;

                            DatePicker datePicker = datePickerDialog
                                    .getDatePicker();
                            datePickerListener.onDateSet(datePicker,
                                    datePicker.getYear(),
                                    datePicker.getMonth(),
                                    datePicker.getDayOfMonth());


                        }
                    }
                });
        long timeInMilliseconds = 0;
        String givenDateString = "";
        if (chooseFromDate.equals("fromdate")) {
            givenDateString = postcalendardates;
        } else {
            givenDateString = postcalendardates;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date mDate = sdf.parse(givenDateString);
            timeInMilliseconds = mDate.getTime();
            //Log.e("timeInMilliseconds ", "timeInMilliseconds " + timeInMilliseconds);
            //System.out.println("Date in milli :: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long currentTime = System.currentTimeMillis();
        datePickerDialog.getDatePicker().setMinDate(timeInMilliseconds);
        datePickerDialog.setCancelable(false);
        datePickerDialog.show();


    }

    private void MethodofMonth(String selMonth) {

        if (selMonth.equals("1")) {
            regisMonth = "January";
        }
        if (selMonth.equals("2")) {
            regisMonth = "February";
        }
        if (selMonth.equals("3")) {
            regisMonth = "March";
        }
        if (selMonth.equals("4")) {
            regisMonth = "April";
        }
        if (selMonth.equals("5")) {
            regisMonth = "May";
        }
        if (selMonth.equals("6")) {
            regisMonth = "June";
        }
        if (selMonth.equals("7")) {
            regisMonth = "July";
        }
        if (selMonth.equals("8")) {
            regisMonth = "August";
        }
        if (selMonth.equals("9")) {
            regisMonth = "September";
        }
        if (selMonth.equals("10")) {
            regisMonth = "October";
        }
        if (selMonth.equals("11")) {
            regisMonth = "November";
        }
        if (selMonth.equals("12")) {
            regisMonth = "December";
        }

    }


    public void setFollowCustomDateFrom(View v) {


        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
            // when dialog box is closed, below method will be called.
            public void onDateSet(DatePicker view, int selectedYear,
                                  int selectedMonth, int selectedDay) {
                if (isoktorepo) {
                    myCalendar.set(Calendar.YEAR, selectedYear);
                    myCalendar.set(Calendar.MONTH, selectedMonth);
                    myCalendar.set(Calendar.DAY_OF_MONTH, selectedDay);

                   // System.out.println();
                    String strselectedDay = String.valueOf(selectedDay);
                    if (strselectedDay.length() == 1) {
                        strselectedDay = "0" + strselectedDay;
                    }
                    String strselectedMonth = String.valueOf(selectedMonth + 1);
                    if (strselectedMonth.length() == 1) {
                        strselectedMonth = "0" + strselectedMonth;
                    }

                    followcalendardates = selectedYear + "-" + strselectedMonth + "-"
                            + strselectedDay;

                    followCustomFromDate = followcalendardates;

                    MethodofMonth(strselectedMonth);


                    FWfromdate.setText(strselectedDay + " " + regisMonth + " " + selectedYear + " -");

                }
                isoktorepo = false;
                setFollowCustomToFrom(view);
            }
        };


        final DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                datePickerListener, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            dialog.cancel();
                            isoktorepo = false;
                        }
                    }
                });

        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    @SuppressLint("NewApi")
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            isoktorepo = true;

                            DatePicker datePicker = datePickerDialog
                                    .getDatePicker();
                            datePickerListener.onDateSet(datePicker,
                                    datePicker.getYear(),
                                    datePicker.getMonth(),
                                    datePicker.getDayOfMonth());

                        }
                    }
                });

        long currentTime = System.currentTimeMillis();
        //datePickerDialog.getDatePicker().setMaxDate(currentTime);

        datePickerDialog.setCancelable(false);
        datePickerDialog.show();


    }


    public void setFollowCustomToFrom(View v) {

        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
            // when dialog box is closed, below method will be called.
            public void onDateSet(DatePicker view, int selectedYear,
                                  int selectedMonth, int selectedDay) {
                if (isoktorepo) {
                    myCalendar.set(Calendar.YEAR, selectedYear);
                    myCalendar.set(Calendar.MONTH, selectedMonth);
                    myCalendar.set(Calendar.DAY_OF_MONTH, selectedDay);

                   // System.out.println();
                    String strselectedDay = String.valueOf(selectedDay);
                    if (strselectedDay.length() == 1) {
                        strselectedDay = "0" + strselectedDay;
                    }
                    String strselectedMonth = String.valueOf(selectedMonth + 1);
                    if (strselectedMonth.length() == 1) {
                        strselectedMonth = "0" + strselectedMonth;
                    }

                    followcalendardates = selectedYear + "-" + strselectedMonth + "-"
                            + strselectedDay;

                    followCustomToDate = followcalendardates;
                    MethodofMonth(strselectedMonth);

                    FWtodate.setText(strselectedDay + " " + regisMonth + " " + selectedYear);

                }


                isoktorepo = false;
            }
        };


        final DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                datePickerListener, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {

                            FWfromdate.setText("");
                            FWtodate.setText("");

                            dialog.cancel();
                            isoktorepo = false;
                        }
                    }
                });

        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    @SuppressLint("NewApi")
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            isoktorepo = true;

                            DatePicker datePicker = datePickerDialog
                                    .getDatePicker();
                            datePickerListener.onDateSet(datePicker,
                                    datePicker.getYear(),
                                    datePicker.getMonth(),
                                    datePicker.getDayOfMonth());


                        }
                    }
                });
        long timeInMilliseconds = 0;
        String givenDateString = "";
        givenDateString = followcalendardates;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date mDate = sdf.parse(givenDateString);
            timeInMilliseconds = mDate.getTime();
           // Log.e("timeInMilliseconds ", "timeInMilliseconds " + timeInMilliseconds);
           // System.out.println("Date in milli :: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long currentTime = System.currentTimeMillis();
        datePickerDialog.getDatePicker().setMinDate(timeInMilliseconds);
        datePickerDialog.setCancelable(false);
        datePickerDialog.show();


    }

    private String StringtoDateFromat (String s){
        String strNewFormattedDate = "";
        try{
            SimpleDateFormat mSimpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd");
            Date date = mSimpleDateFormat.parse(s);

            SimpleDateFormat newFormat = new SimpleDateFormat("dd MMMM yyyy");
            strNewFormattedDate = newFormat.format(date);
        }catch ( Exception e){
            e.printStackTrace();
        }
        return strNewFormattedDate;
    }


}
