package com.mfcwl.mfc_dealer.Fragment.Leads;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mfcwl.mfc_dealer.Activity.SplashActivity;
import com.mfcwl.mfc_dealer.Adapter.Leads.FollowUpHistoryLeadsAdapter;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Model.Leads.FollowUpHistoryLeadModel;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP 240 G5 on 13-03-2018.
 */

public class FollowupHistoryFragment extends Fragment {

    public static RecyclerView followup_history_leads_recycler;
    public static FollowUpHistoryLeadsAdapter mFollowuphistoryAdapter;


    public static String ldid;
    public static String leadid;
    public static String leadtype;

    private static List<FollowUpHistoryLeadModel> myJobList = new ArrayList<>();

    @SuppressLint("ValidFragment")
    public FollowupHistoryFragment(String ldid) {
        // Required empty public constructor
        FollowupHistoryFragment.ldid = ldid;

    }

    @SuppressLint("ValidFragment")
    public FollowupHistoryFragment(String leadid, String leadtype) {
        // Required empty public constructor
        FollowupHistoryFragment.leadid = leadid;
        FollowupHistoryFragment.leadtype = leadtype;

    }

    public FollowupHistoryFragment() {
        // Required empty public constructor

    }

    static Activity activity;
    static Context c;
    private static String TAG = FollowupHistoryFragment.class.getSimpleName();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.layout_followup_history, container, false);

        activity = getActivity();
        c = getContext();

        SplashActivity.progress = true;
        followup_history_leads_recycler = view.findViewById(R.id.followup_history_leads_recycler);
        followup_history_leads_recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        followup_history_leads_recycler.setHasFixedSize(true);

        mFollowuphistoryAdapter = new FollowUpHistoryLeadsAdapter(c, myJobList, activity);

        DividerItemDecoration itemDecor = new DividerItemDecoration(getContext(), DividerItemDecoration.HORIZONTAL);
        followup_history_leads_recycler.addItemDecoration(itemDecor);
        followup_history_leads_recycler.setAdapter(mFollowuphistoryAdapter);

        if (CommonMethods.getstringvaluefromkey(activity, "WLEADS").equals("WebLeads")) {
            WebServicesCall.webCall(activity, getContext(), jsonMake(), "FollowUpHistory", GlobalText.POST);
        }
        if (CommonMethods.getstringvaluefromkey(activity, "WLEADS").equals("PrivateLeads")) {
            WebServicesCall.webCall(activity, getContext(), jsonMakePrivate(), "FollowUpHistoryPrivateLeads", GlobalText.POST);
        }


        Log.e(TAG, "callingAPI oncreateview" + ldid);

        return view;
    }

    public static JSONObject jsonMake() {
        JSONObject jObj = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        try {

            jObj.put("access_token", CommonMethods.getstringvaluefromkey(activity, "access_token"));
            jObj.put("dispatch_id", ldid);
            jObj.put("tag", "android");


        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("Parseleadstore ", "jObj " + jObj.toString());
        return jObj;
    }

    public static JSONObject jsonMakePrivate() {

        JSONObject jObj = new JSONObject();
        try {

            jObj.put("Type", "mfc");
            jObj.put("LeadId", leadid);
            jObj.put("LeadType", leadtype);

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("PrivateLeadHistory ", "excep " + e.toString());
        }
        Log.e("PrivateLeadHistory ", "jObj " + jObj.toString());
        return jObj;
    }


    public static void Parseupdatefollowuphistory(JSONObject jObj, Activity activity) {

        //   Log.e("ParseUpdate ", "followuphistory " + jObj.toString());

        try {
            if (jObj.getString("status").equals("failure")) {
                WebServicesCall.error_popup2(activity, Integer.parseInt(jObj.getString("status_code")), "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        myJobList = new ArrayList<>();

        try {
            JSONArray arrayList = jObj.getJSONArray("followup_history");

            for (int i = 0; i < arrayList.length(); i++) {


                JSONObject jsonobject = new JSONObject(arrayList.getString(i));

                FollowUpHistoryLeadModel historyLeadModel = new FollowUpHistoryLeadModel(jsonobject.getString("followup_date"),
                        jsonobject.getString("followup_status"),
                        jsonobject.getString("followup_comments"), jsonobject.getString("created_at"));

                myJobList.add(historyLeadModel);

            }
            reloadData();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public static void reloadData() {

        mFollowuphistoryAdapter = new FollowUpHistoryLeadsAdapter(c, myJobList, activity);
        followup_history_leads_recycler.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(c);
        followup_history_leads_recycler.setLayoutManager(mLayoutManager);

        followup_history_leads_recycler.setAdapter(mFollowuphistoryAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "callingAPI onresumeview" + ldid);
        Application.getInstance().trackScreenView(activity,GlobalText.FollowUpHistory_Fragment);

    }

    public static void reloadAPI() {
        Log.e("reloadAPI ", "reloadAPI " + CommonMethods.getstringvaluefromkey(activity, "WLEADS"));
        if (CommonMethods.getstringvaluefromkey(activity, "WLEADS").equals("WebLeads")) {
            WebServicesCall.webCall(activity, activity, jsonMake(), "FollowUpHistory", GlobalText.POST);
        }
        if (CommonMethods.getstringvaluefromkey(activity, "WLEADS").equals("PrivateLeads")) {
            WebServicesCall.webCall(activity, activity, jsonMakePrivate(), "FollowUpHistoryPrivateLeads", GlobalText.POST);
        }
        Log.e(TAG, "callingAPI oncreateview" + ldid);
    }

    public static void ParseUpdateFollowUpHistoryPrivateLeads(JSONObject jObj, Activity activity) {

        Log.e("PrivateLeadHistory ", "jobj " + jObj.toString());


        myJobList = new ArrayList<>();

        try {
            JSONArray arrayList = jObj.getJSONArray("FollowUps");

            for (int i = 0; i < arrayList.length(); i++) {


                JSONObject jsonobject = new JSONObject(arrayList.getString(i));

                FollowUpHistoryLeadModel historyLeadModel = new FollowUpHistoryLeadModel(jsonobject.getString("Follow_up"),
                        jsonobject.getString("Lead_Status"),
                        jsonobject.getString("Lead_remark"), jsonobject.getString("created_on"));

                myJobList.add(historyLeadModel);

            }
            reloadData();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
