package com.mfcwl.mfc_dealer.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.Activity.LeadSection.LeadConstantsSection;
import com.mfcwl.mfc_dealer.Activity.Leads.FilterInstance;
import com.mfcwl.mfc_dealer.Activity.Leads.LeadConstants;
import com.mfcwl.mfc_dealer.Activity.Leads.LeadsInstance;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Activity.OnlinePaymentActivity;
import com.mfcwl.mfc_dealer.Activity.SplashActivity;
import com.mfcwl.mfc_dealer.Activity.StockFilterActivity;
import com.mfcwl.mfc_dealer.DasBoardModels.CustomWhere;
import com.mfcwl.mfc_dealer.DasBoardModels.DashBoardResponse;
import com.mfcwl.mfc_dealer.DasBoardModels.WebLeadsResponse;
import com.mfcwl.mfc_dealer.DashBoardServices.DashBoardService;
import com.mfcwl.mfc_dealer.DashBoardServices.WebLeadsService;
import com.mfcwl.mfc_dealer.Fragment.LeadSection.LeadsFragmentSection;
import com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragmentNew;
import com.mfcwl.mfc_dealer.InstanceCreate.LeadSection.LeadFilterSaveInstance;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.PayUPayment.PayUPaymentResponse;
import com.mfcwl.mfc_dealer.PayUPayment.PaymentRequest;
import com.mfcwl.mfc_dealer.Procurement.Fragment.ProcurementTAB;
import com.mfcwl.mfc_dealer.Procurement.Instances.ProcLeadFilterSaveInstance;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.DashboardCustomWhere;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.DashboardWebLeadsRequest;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PWLCustomWhere;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PWLRequest;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PWLWhereIn;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PWLWhereor;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.ProcDashWebLeadResponse;
import com.mfcwl.mfc_dealer.Procurement.RetrofitConfig.ProcPrivateLeadService;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.RequestModel.WebLeadReq1;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.RetrofitServicesLeadSection.WebLeadService;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.tv_header_title;
import static com.mfcwl.mfc_dealer.Activity.StockFilterActivity.stock_photocountsFlag;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_DEALER_CRE;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_PROCUREMENT;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_SALES;


public class HomeFragment extends Fragment {

    String today = "2018-03-31";
    String TAG = getClass().getSimpleName();

    public static Activity activity;
    public static TextView warrantBal;
    public static TextView totalOldStockCount;
    public static TextView totalLeadPrivate, proc_totalLeadPrivate;
    public static TextView privateLeadsFollwupCount, proc_privateLeadsFollowupCount;
    public static TextView totalStoreStocks;
    public static TextView weblead_id, Proc_WebleadId;
    public LinearLayout clickStock, clickLead, thirtydays, listPrivate, listWebLeads, Proc_listWebLeads, proc_listPrivate, ll_procData;
    private WebLeadReq1 mLeadRequest;
    private SharedPreferences mSharedPreferences = null;
    private SharedPreferences mSharedPreferencesLead = null;
    private SharedPreferences mSharedPreferencesProcLead = null;


    private List<PWLCustomWhere> mWebLeadsCustomWheres;
    private List<String> mWebLeadsWherenotin;
    private List<PWLWhereor> mWhereorList;
    private List<PWLWhereIn> mWherein;
    private List<PWLCustomWhere> webLeadsCustomWheresList;
    private Button btn_update;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        MenuItem asmHome = menu.findItem(R.id.asmHome);
        MenuItem asm_mail = menu.findItem(R.id.asm_mail);
        MenuItem add_image = menu.findItem(R.id.add_image);
        MenuItem share = menu.findItem(R.id.share);
        MenuItem delete_fea = menu.findItem(R.id.delete_fea);
        MenuItem stock_fil_clear = menu.findItem(R.id.stock_fil_clear);
        MenuItem notification_bell = menu.findItem(R.id.notification_bell);
        MenuItem notification_cancel = menu.findItem(R.id.notification_cancel);

        asmHome.setVisible(false);
        asm_mail.setVisible(false);
        add_image.setVisible(false);
        share.setVisible(false);
        delete_fea.setVisible(false);
        stock_fil_clear.setVisible(false);
        notification_bell.setVisible(true);
        notification_cancel.setVisible(false);

    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        // Inflate the layout for this fragment
        mSharedPreferences = getActivity().getSharedPreferences("MFC", Context.MODE_PRIVATE);
        mSharedPreferencesLead = getActivity().getSharedPreferences("MFCB", Context.MODE_PRIVATE);
        mSharedPreferencesProcLead = getActivity().getSharedPreferences("MFCP", Context.MODE_PRIVATE);
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.home_fragment_proc, container, false);
        activity = getActivity();

       /* CustomDialogClass mCustomDialogClass = new CustomDialogClass(activity);
        //mCustomDialogClass.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mCustomDialogClass.show();*/

        warrantBal = view.findViewById(R.id.warrant_bal);
        totalOldStockCount = view.findViewById(R.id.total_old_stock_count);
        thirtydays = view.findViewById(R.id.thirtydays);
        listPrivate = view.findViewById(R.id.listPrivate);
        listWebLeads = view.findViewById(R.id.listWebLeads);
        Proc_listWebLeads = view.findViewById(R.id.Proc_listWebLeads);
        proc_listPrivate = view.findViewById(R.id.proc_listPrivate);
        btn_update = view.findViewById(R.id.btn_update);
        CommonMethods.setvalueAgainstKey(activity, "status", "Home");
        SplashActivity.progress = true;

        totalLeadPrivate = view.findViewById(R.id.total_lead_private);
        proc_totalLeadPrivate = view.findViewById(R.id.proc_totalLeadPrivate);
        privateLeadsFollwupCount = view.findViewById(R.id.private_leads_followup_count);
        proc_privateLeadsFollowupCount = view.findViewById(R.id.proc_privateLeadsFollowupCount);

        totalStoreStocks = view.findViewById(R.id.total_store_stocks);
        weblead_id = view.findViewById(R.id.weblead_id);
        Proc_WebleadId = view.findViewById(R.id.Proc_WebleadId);

        clickStock = view.findViewById(R.id.click_stock);
        clickStock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (StockStoreFrag.stock != null) {
                        StockStoreFrag.stock.clear();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                MainActivity.stockreset();
                StockFilterActivity.stockResetData(activity);
                FilterInstance.getInstance().setNotification("total");
                StockFragment stockFragment = new StockFragment();
                fragmentLoad(stockFragment);
            }
        });
        clickLead = view.findViewById(R.id.click_lead);
        clickLead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_PROCUREMENT)) {
                    Toast.makeText(activity, activity.getString(R.string.you_are_not_auth), Toast.LENGTH_SHORT).show();
                    return;
                }

                clearFilterLeads();
                saveTodayFollowupLeads();
                LeadsInstance.getInstance().setLeaddirection("WebLeadsPage");
                MainActivity.leadsreset("Sales Lead");
                LeadsFragmentSection leadsFragment = new LeadsFragmentSection();
                fragmentLoad(leadsFragment);
            }
        });

        listWebLeads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_PROCUREMENT)) {
                    Toast.makeText(activity, activity.getString(R.string.you_are_not_auth), Toast.LENGTH_SHORT).show();
                    return;
                }

                clearFilterLeads();
                saveTodayFollowupLeads();
                LeadsInstance.getInstance().setLeaddirection("WebLeadsPage");
                MainActivity.leadsreset("Sales Lead");
                LeadsFragmentSection leadsFragment = new LeadsFragmentSection();
                fragmentLoad(leadsFragment);
            }
        });

        listPrivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_PROCUREMENT)) {
                    Toast.makeText(activity, activity.getString(R.string.you_are_not_auth), Toast.LENGTH_SHORT).show();
                    return;
                }


                clearFilterLeads();
                saveTodayFollowupLeads();
                LeadsInstance.getInstance().setLeaddirection("PrivateLeadsPage");
                MainActivity.leadsreset("Sales Lead");
                LeadsFragmentSection leadsFragment = new LeadsFragmentSection();
                fragmentLoad(leadsFragment);
            }
        });

        // Procurement
        ll_procData = view.findViewById(R.id.ll_procData);
        ll_procData.setOnClickListener(v -> {

            if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_SALES) ||
                    CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_DEALER_CRE)) {
                Toast.makeText(activity, activity.getString(R.string.you_are_not_auth), Toast.LENGTH_SHORT).show();
                return;
            }

            ProcclearFilterLeads();
            ProcsaveTodayFollowupLeads();
            LeadsInstance.getInstance().setLeaddirection("ProcWebLeadsPage");
            MainActivity.leadsreset("Procurement Lead");
            ProcurementTAB leadsFragment = new ProcurementTAB();
            fragmentLoad(leadsFragment);

        });
        Proc_listWebLeads.setOnClickListener(view12 -> {

            if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_SALES) ||
                    CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_DEALER_CRE)) {
                Toast.makeText(activity, activity.getString(R.string.you_are_not_auth), Toast.LENGTH_SHORT).show();
                return;
            }
            ProcclearFilterLeads();
            ProcsaveTodayFollowupLeads();
            LeadsInstance.getInstance().setLeaddirection("ProcWebLeadsPage");
            MainActivity.leadsreset("Procurement Lead");
            ProcurementTAB leadsFragment = new ProcurementTAB();
            fragmentLoad(leadsFragment);
        });

        proc_listPrivate.setOnClickListener(view13 -> {

            if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_SALES)||
                    CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_DEALER_CRE)) {
                Toast.makeText(activity, activity.getString(R.string.you_are_not_auth), Toast.LENGTH_SHORT).show();
                return;
            }
            ProcclearFilterLeads();
            ProcsaveTodayFollowupLeads();
            LeadsInstance.getInstance().setLeaddirection("ProcPrivateLeadsPage");
            MainActivity.leadsreset("Procurement Lead");
            ProcurementTAB leadsFragment = new ProcurementTAB();
            fragmentLoad(leadsFragment);
        });

        thirtydays.setOnClickListener(v -> {

            MainActivity.stockreset();
            stock_photocountsFlag = false;
            //added by Surya to clear the previous stocks if any
            if (StockStoreFrag.stock != null) {
                StockStoreFrag.stock.clear();
            }
            CommonMethods.setvalueAgainstKey(activity, "photocounts", "false");
            CommonMethods.setvalueAgainstKey(activity, "Notification_Switch", "AgeingInventory");
            CommonMethods.setvalueAgainstKey(activity, "filterapply", "false");
            MainActivity.postdatelessthirty = true;
            FilterInstance.getInstance().setNotification("old");

            StockFragment stocks = new StockFragment();
            fragmentLoad(stocks);

        });
        btn_update.setOnClickListener(view1 -> {
            String userType = CommonMethods.getstringvaluefromkey(getActivity(), "user_type");
            if (userType.equalsIgnoreCase(USER_TYPE_DEALER_CRE) ||
                    userType.equalsIgnoreCase(USER_TYPE_PROCUREMENT) ||
                    userType.equalsIgnoreCase(USER_TYPE_SALES)) {
                Toast.makeText(getActivity(), R.string.you_are_not_auth, Toast.LENGTH_SHORT).show();
                return;
            }
            startActivity(new Intent(getActivity(), OnlinePaymentActivity.class).putExtra("warantybalance",
                    warrantBal.getText().toString().trim()));
        });

        try {
            if (CommonMethods.getstringvaluefromkey(getActivity(), "txnid") != null &&
                    !CommonMethods.getstringvaluefromkey(getActivity(), "txnid").equalsIgnoreCase("")) {

                PaymentRequest paymentRequest = new PaymentRequest();
                paymentRequest.setCode(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"));
                paymentRequest.setAmount(Integer.parseInt(CommonMethods.getstringvaluefromkey(getActivity(), "amount")));

                paymentRequest.setPg_transaction_id(CommonMethods.getstringvaluefromkey(getActivity(), "txnid"));

                //current date
                Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Kolkata"));
                Date currentLocalTime = cal.getTime();
                DateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                date.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                String localTime = date.format(currentLocalTime);

                paymentRequest.setPg_transaction_datetime_yyyymmddhhmmss(localTime);

                //paymentRequest.setRemark(CommonMethods.getstringvaluefromkey(getActivity(), "txnid"));

                CallingPayment(getActivity(), paymentRequest, CommonMethods.getstringvaluefromkey(getActivity(), "amount"),
                        CommonMethods.getstringvaluefromkey(getActivity(), "txnid"));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        MethodOfSubtractDate();
        MethodOfSubtractbyDays();
      //  CommonMethods.MemoryClears();

        if (CommonMethods.isInternetWorking(activity)) {
            getDashBoardInfo(getActivity());
        } else {
            CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
        }


        // leads filter clear
        LeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());
        LeadFilterSaveInstance.getInstance().setStatusarray(new JSONArray());
        // Log.i(TAG, "getSavedatahashmap: "+LeadFilterSaveInstance.getInstance().getSavedatahashmap());


        // WebServicesCall.webCall(activity, activity, jsonMake1(), "dashBroad", GlobalText.GET);

        return view;


    }


    @Override
    public void onResume() {
        super.onResume();
        // put your code here...

        if (tv_header_title != null) {
            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText("");
        }

     //   Application.getInstance().trackScreenView(activity,GlobalText.Home_Fragment);

    }

    public void prePareWebLeadData() {

        mLeadRequest = new WebLeadReq1();

        List<CustomWhere> mList = new ArrayList<>();

        try {
            mLeadRequest.setPerPage("2");
            mLeadRequest.setPage("1");
            mLeadRequest.setAccessToken(CommonMethods.getstringvaluefromkey(activity, "access_token"));

            mLeadRequest.setOrderBy("leads.created_at");
            mLeadRequest.setOrderByReverse("leads.created_at");
            mLeadRequest.setAliasFields("leads.created_at as PostedDate,leads.created_at as LeadDate");
            CustomWhere customWheretargetid = new CustomWhere();
            customWheretargetid.setColumn("dispatched.target_id");
            customWheretargetid.setOperator("=");
            customWheretargetid.setValue(CommonMethods.getstringvaluefromkey(activity, "dealer_code"));
            mList.add(customWheretargetid);
            CustomWhere customWheresource = new CustomWhere();
            customWheresource.setColumn("dispatched.target_source");
            customWheresource.setOperator("=");
            customWheresource.setValue("MFC");
            mList.add(customWheresource);
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, 0);
            Calendar cal1 = Calendar.getInstance();
            cal1.add(Calendar.DATE, 1);
            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
            String todayfromDate = s.format(new Date(cal.getTimeInMillis()));
            String todaytoDate = s.format(new Date(cal1.getTimeInMillis()));
            CustomWhere customWherefollowupdate1 = new CustomWhere();
            customWherefollowupdate1.setColumn("dispatched.followup_date");
            customWherefollowupdate1.setOperator(">=");
            customWherefollowupdate1.setValue(todayfromDate);
            /*if (CommonMethods.getstringvaluefromkey(MainActivity.activity, "user_type").equalsIgnoreCase(USER_TYPE_PROCUREMENT)) {
                mList.add(customWherefollowupdate1);
            }*/
            mList.add(customWherefollowupdate1);
            CustomWhere customWherefollowupdate2 = new CustomWhere();
            customWherefollowupdate2.setColumn("dispatched.followup_date");
            customWherefollowupdate2.setOperator("<");
            customWherefollowupdate2.setValue(todaytoDate);
            mList.add(customWherefollowupdate2);

            CustomWhere customewhere = new CustomWhere();
            customewhere.setColumn("leads.lead_type");
            customewhere.setOperator("=");
            customewhere.setValue("USEDCARSALES");
            mList.add(customewhere);
            mLeadRequest.setCustomWhere(mList);
            mLeadRequest.setWherenotin(new ArrayList<>());
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("ParseLead ", "exp " + e.toString());
        }
    }

    public void ProcWebLeadRequest() {

        DashboardWebLeadsRequest leadRequest = new DashboardWebLeadsRequest();
        List<DashboardCustomWhere> mlist = new ArrayList<>();
        try {
            leadRequest.setPerPage("2");
            leadRequest.setPage("1");
            leadRequest.setAccessToken(CommonMethods.getstringvaluefromkey(activity, "access_token"));

            leadRequest.setOrderBy("leads.created_at");
            leadRequest.setOrderByReverse("leads.created_at");
            leadRequest.setAliasFields("leads.created_at as PostedDate,leads.created_at as LeadDate");

            DashboardCustomWhere customWheretargetid = new DashboardCustomWhere();
            customWheretargetid.setColumn("dispatched.target_id");
            customWheretargetid.setOperator("=");
            customWheretargetid.setValue(CommonMethods.getstringvaluefromkey(activity, "dealer_code"));
            mlist.add(customWheretargetid);

            DashboardCustomWhere cuswhere = new DashboardCustomWhere();
            cuswhere.setColumn("leads.lead_type");
            cuswhere.setOperator("=");
            cuswhere.setValue("PROCUREMENT");
            mlist.add(cuswhere);

            DashboardCustomWhere customWheresource = new DashboardCustomWhere();
            customWheresource.setColumn("dispatched.target_source");
            customWheresource.setOperator("=");
            customWheresource.setValue("MFC");
            mlist.add(customWheresource);

            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, 0);
            Calendar cal1 = Calendar.getInstance();
            cal1.add(Calendar.DATE, 1);
            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
            String todayfromDate = s.format(new Date(cal.getTimeInMillis()));
            String todaytoDate = s.format(new Date(cal1.getTimeInMillis()));
            DashboardCustomWhere customWherefollowupdate1 = new DashboardCustomWhere();
            customWherefollowupdate1.setColumn("dispatched.followup_date");
            customWherefollowupdate1.setOperator(">=");
            customWherefollowupdate1.setValue(todayfromDate);
            mlist.add(customWherefollowupdate1);

            DashboardCustomWhere customWherefollowupdate2 = new DashboardCustomWhere();
            customWherefollowupdate2.setColumn("dispatched.followup_date");
            customWherefollowupdate2.setOperator("<");
            customWherefollowupdate2.setValue(todaytoDate);
            mlist.add(customWherefollowupdate2);

            leadRequest.setCustomWhere(mlist);
            leadRequest.setWherenotin(new ArrayList<>());

            sendWebLeadDetails(getContext(), leadRequest);

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("ProcParseLead ", "exp " + e.toString());
        }
    }

    public void prepareWebLeadRequest() {

        webLeadsCustomWheresList = new ArrayList<>();
        mWherein = new ArrayList<>();
        mWhereorList = new ArrayList<>();
        mWebLeadsWherenotin = new ArrayList<String>();
        mWebLeadsCustomWheres = new ArrayList<>();

        PWLRequest webLeadsModelRequest = new PWLRequest();

        webLeadsModelRequest.setFilterByFields("");
        webLeadsModelRequest.setPerPage(Integer.toString(2));
        webLeadsModelRequest.setPage("1");

        webLeadsModelRequest.setAccessToken(CommonMethods.getstringvaluefromkey(getActivity(), "access_token"));

        webLeadsModelRequest.setWhereOr(mWhereorList);

        webLeadsModelRequest.setAliasFields("leads.created_at as PostedDate,leads.created_at as LeadDate,leads.id as id,dispatched.id as dispatchid,leads.customer_name as Name,leads.customer_mobile as Mobile,leads.customer_email as Email,leads.primary_make,leads.primary_model,leads.primary_variant,leads.secondary_make,leads.secondary_model,leads.secondary_variant,leads.register_no as regno,leads.register_month as regmonth,leads.register_year as regyear,leads.color,leads.kms,mfg_year,leads.owner as owners,leads.register_city,leads.stock_listed_price as price,dispatched.status,dispatched.followup_comments as remark,dispatched.followup_date as FollowUpDate,dispatched.employee_name as ExecutiveName");

        webLeadsModelRequest.setCustomWhere(mWebLeadsCustomWheres);
        webLeadsModelRequest.setWherenotin(mWebLeadsWherenotin);

        webLeadsModelRequest.setReportPrefix("Follow up Sales Lead Report");
        webLeadsModelRequest.setTag("android");

        PWLCustomWhere webLeadsCustomWhere = new PWLCustomWhere();
        setCustomWhereWebLeads(webLeadsCustomWhere);

        //Posted Followup Date Filter

        webLeadsModelRequest.setWhereIn(mWherein);
        webLeadsModelRequest.setCustomWhere(webLeadsCustomWheresList);

        //  sendWebLeadDetails(getContext(), webLeadsModelRequest);
    }

    private void sendWebLeadDetails(final Context mContext, DashboardWebLeadsRequest request) {
        WebLeadService.procDashboardWebLead(mContext, request, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                Response<ProcDashWebLeadResponse> mRes = (Response<ProcDashWebLeadResponse>) obj;
                ProcDashWebLeadResponse mData = mRes.body();

                String str = new Gson().toJson(mData, ProcDashWebLeadResponse.class);
                Log.i(TAG, "OnSuccess: " + str);

                if (mData != null) {
                    if (!mData.getStatus().equals("failure")) {
                        if (mData.getTotal() != null) {
                            Proc_WebleadId.setText("" + mData.getTotal());
                        } else {
                            Proc_WebleadId.setText("0");
                        }
                    } else {
                        WebServicesCall.error_popup2(getActivity(), Integer.parseInt(mData.getStatusCode().toString()), "");
                    }
                }
                try {
                    int proc_total = Integer.parseInt(Proc_WebleadId.getText().toString().trim()) + Integer.parseInt(proc_privateLeadsFollowupCount.getText().toString().trim());
                    proc_totalLeadPrivate.setText("" + proc_total);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void OnFailure(Throwable t) {
                t.printStackTrace();
            }
        });

    }


    private void setCustomWhereWebLeads(PWLCustomWhere webLeadsCustomWhere) {
        //Add Dealer Id into DashboardCustomWhere
        webLeadsCustomWhere.setColumn("dispatched.target_id");
        webLeadsCustomWhere.setOperator("=");
        webLeadsCustomWhere.setValue(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"));
        webLeadsCustomWheresList.add(webLeadsCustomWhere);
        //End

        //Add Target Source into DashboardCustomWhere
        webLeadsCustomWhere = new PWLCustomWhere();
        webLeadsCustomWhere.setColumn("leads.lead_type");
        webLeadsCustomWhere.setOperator("=");
        webLeadsCustomWhere.setValue("PROCUREMENT");
        webLeadsCustomWheresList.add(webLeadsCustomWhere);
        //End
    }


    public static JSONObject jsonMake1() {
        JSONObject jObj = new JSONObject();
        //   Log.e("jsonMake1 ", "jsonMake1" + jObj.toString());
        return jObj;
    }


    private void MethodOfSubtractbyDays() {
        Calendar calendar = Calendar.getInstance();
//rollback 90 days
        calendar.add(Calendar.DAY_OF_YEAR, -45);
//now the date is 90 days back

        Log.e("MyApp ", " years " + Calendar.DAY_OF_YEAR);
        Log.e("MyApp", "90 days ago:" + calendar.getTime().toString());
    }

    private void MethodOfSubtractDate() {

        // DateTimeFormatter obj = new DateTimeFormatter();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");

        try {
            Date date1 = simpleDateFormat.parse("10/2/2013 11:30:10");
            Date date2 = simpleDateFormat.parse("8/3/2013 20:35:55");

            printDifference(date1, date2);

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

       // System.out.println("startDate : " + startDate);
        //System.out.println("endDate : " + endDate);
        //System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

       /* System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

        Log.e("elapsedDays ", "elapsedDays " + elapsedDays);*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


    public static void ParseDashBroad(JSONObject jObj, String strMethod, Activity activity) {

        try {
            String warranty_balance = jObj.getString("warranty_balance");
            String private_leads_followup_count = jObj.getString("private_leads_followup_count");
            String total_stock_count = jObj.getString("total_stock_count");
            String total_old_stock_count = jObj.getString("total_old_stock_count");

            warrantBal.setText(warranty_balance);

            CommonMethods.setvalueAgainstKey(activity, "warrantybalance", warrantBal.getText().toString());

            totalOldStockCount.setText(total_old_stock_count);
            privateLeadsFollwupCount.setText(private_leads_followup_count);
            totalStoreStocks.setText(total_stock_count);

            //   Log.e("test bashbroad data", warranty_balance + " " + private_leads_followup_count + " " + total_old_stock_count + " " + total_stock_count);
            // WebServicesCall.webCall(activity, activity, jsonMake(), "LeadStoreCount", GlobalText.POST);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public static void ParseleadstoreCount(JSONObject jObj, Activity activity) {
        Log.e("ParseLead ", "Counts" + jObj.toString());
        try {
            weblead_id.setText(jObj.getString("total"));
            int total = Integer.parseInt(weblead_id.getText().toString()) + Integer.parseInt(privateLeadsFollwupCount.getText().toString());

            CommonMethods.setvalueAgainstKey(activity, "leadfollowup_total", String.valueOf(total));

            /*Log.e("weblead_id=", "weblead_id" + weblead_id.getText().toString());
            Log.e("total=", "total" + total);*/

            totalLeadPrivate.setText("" + total);


        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("JSONException=", "JSONException" + weblead_id.getText().toString());

        }
        //total
    }

    public void fragmentLoad(Fragment fragment) {

        try {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fillLayout, fragment);
            fragmentTransaction.commit();

            CommonMethods.setvalueAgainstKey(activity, "stockcreated", "false");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    String todayfromDate = "", todaytoDate = "";

    public String CalendarTodayDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);

        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, 1);
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        todayfromDate = s.format(new Date(cal.getTimeInMillis()));
        todaytoDate = s.format(new Date(cal1.getTimeInMillis()));
        return todayfromDate + "@" + todaytoDate;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        final MenuItem item = menu.findItem(R.id.notification_bell);
        item.setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void getDashBoardInfo(final Context mContext) {
        SpinnerManager.showSpinner(mContext);
        DashBoardService.fetchDashBoardInfo(mContext, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                Response<DashBoardResponse> mRes = (Response<DashBoardResponse>) obj;
                DashBoardResponse mData = mRes.body();

                String str = new Gson().toJson(mData, DashBoardResponse.class);
                Log.i(TAG, "OnSuccess: " + str);

                SpinnerManager.hideSpinner(mContext);

                warrantBal.setText(String.valueOf(mData.getWarrantyBalance()));

                CommonMethods.setvalueAgainstKey(activity, "warrantybalance", warrantBal.getText().toString());


                totalOldStockCount.setText(String.valueOf(mData.getTotalOldStockCount()));
                privateLeadsFollwupCount.setText(String.valueOf(mData.getPrivateLeadsFollowupCountSales()));
                proc_privateLeadsFollowupCount.setText(String.valueOf(mData.getPrivateLeadsFollowupCountProcurement()));
                totalStoreStocks.setText(String.valueOf(mData.getTotalStockCount()));

                if (CommonMethods.isInternetWorking(activity)) {
                    prePareWebLeadData();
                    fetchWebleadsData(getActivity(), mLeadRequest);

                    // prepareWebLeadRequest();
                    ProcWebLeadRequest();
                } else {
                    CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
                }

                // WebServicesCall.webCall(activity, activity, jsonMake(), "LeadStoreCount", GlobalText.POST);
                // }
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
                Toast.makeText(mContext, mThrowable.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void fetchWebleadsData(final Context mContext, final WebLeadReq1 mWebLeadRequest) {
        SpinnerManager.showSpinner(mContext);
        WebLeadsService.fetchWebLeads(mContext, mWebLeadRequest, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                Response<WebLeadsResponse> mRes = (Response<WebLeadsResponse>) obj;
                WebLeadsResponse mData = mRes.body();

                String str = new Gson().toJson(mData, WebLeadsResponse.class);
                Log.i(TAG, "OnSuccess: " + str);

                CommonMethods.setvalueAgainstKey(activity, "leadfollowup_total", String.valueOf(mData.getTotal()));
                // totalLeadPrivate.setText("" + String.valueOf(mData.getTotal()));

                if (mData.getStatus().equalsIgnoreCase("failure")) {
                    error_popup(getActivity());
                } else {
                    if (mData.getTotal() != null) {
                        weblead_id.setText(String.valueOf(mData.getTotal()));
                    } else {
                        weblead_id.setText("0");
                    }
                    try {
                        int total = Integer.parseInt(weblead_id.getText().toString()) + Integer.parseInt(privateLeadsFollwupCount.getText().toString());
                        totalLeadPrivate.setText("" + total);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                try {
                    SpinnerManager.hideSpinner(mContext);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //  Toast.makeText(mContext, mThrowable.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public static void error_popup(FragmentActivity activity) {
        if (activity != null) {
           try {
               final Dialog dialog = new Dialog(activity);
               dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
               dialog.setCancelable(false);
               dialog.setContentView(R.layout.alert_error);
               TextView Message, Confirm;
               Message = dialog.findViewById(R.id.Message);
               Confirm = dialog.findViewById(R.id.ok_error);

               Message.setText(GlobalText.AUTH_ERROR);

               Confirm.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {

                       dialog.dismiss();
                       MainActivity.logOut();
                   }
               });
               dialog.show();
           }catch (Exception e){
                     e.printStackTrace();
           }
        }
    }

    private void ClearOtherFilterData() {
        SharedPreferences.Editor mEditor0 = mSharedPreferences.edit();
        mEditor0.putString(LeadConstants.LEAD_POST_TODAY, "");
        //clear others
        mEditor0.putString(LeadConstants.LEAD_POST_CUSTDATE, "");
        mEditor0.putString(LeadConstants.LEAD_POST_YESTER, "");
        mEditor0.putString(LeadConstants.LEAD_POST_7DAYS, "");
        mEditor0.putString(LeadConstants.LEAD_POST_15DAYS, "");

        mEditor0.putString(LeadConstants.LEAD_FOLUP_TMRW, "");
        //mEditor0.putString(LeadConstants.LEAD_FOLUP_TODAY, "");//because set Follow-Up date other Filter
        mEditor0.putString(LeadConstants.LEAD_FOLUP_YESTER, "");
        mEditor0.putString(LeadConstants.LEAD_FOLUP_7DAYS, "");
        mEditor0.putString(LeadConstants.LEAD_FOLUP_CUSTDATE, "");
        mEditor0.putString(LeadConstants.LEAD_STATUS_INFO, "");

        mEditor0.commit();
        try {
            LeadFilterFragmentNew.leadstatusJsonArray = new JSONArray();
        } catch (Exception e) {

        }
    }

    private void clearFilterLeads() {
        SharedPreferences.Editor mEditor = mSharedPreferencesLead.edit();
        mEditor.putString(LeadConstantsSection.LEAD_SAVE_DATE, "");
        mEditor.putString(LeadConstantsSection.LEAD_SAVE_STATUS, "");
        LeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());
        LeadFilterSaveInstance.getInstance().setStatusarray(new JSONArray());
        mEditor.clear();
        mEditor.commit();
    }


    private void ProcclearFilterLeads() {
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();
        mEditor.putString(LeadConstantsSection.LEAD_SAVE_DATE, "");
        mEditor.putString(LeadConstantsSection.LEAD_SAVE_STATUS, "");
        ProcLeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());
        ProcLeadFilterSaveInstance.getInstance().setStatusarray(new JSONArray());
        mEditor.clear();
        mEditor.commit();
    }

    private void saveTodayFollowupLeads() {

        LeadConstantsSection leadConstantsSection = new LeadConstantsSection();
        HashMap<String, String> followupTodayDate = new HashMap<String, String>();

        followupTodayDate.put(LeadConstantsSection.LEAD_FOLUP_TODAY, leadConstantsSection.getFollowtodayDate());
        LeadFilterSaveInstance.getInstance().setSavedatahashmap(followupTodayDate);


        SharedPreferences.Editor mEditor = mSharedPreferencesLead.edit();
        Gson gson = new Gson();
        String follupDate = gson.toJson(followupTodayDate);
        mEditor.putString(LeadConstantsSection.LEAD_SAVE_DATE, follupDate);
        mEditor.commit();
    }

    // Procurement

    private void ProcsaveTodayFollowupLeads() {

        LeadConstantsSection leadConstantsSection = new LeadConstantsSection();
        HashMap<String, String> followupTodayDate = new HashMap<String, String>();

        followupTodayDate.put(LeadConstantsSection.LEAD_FOLUP_TODAY, leadConstantsSection.getFollowtodayDate());
        ProcLeadFilterSaveInstance.getInstance().setSavedatahashmap(followupTodayDate);

        SharedPreferences.Editor mEditor = mSharedPreferencesProcLead.edit();
        Gson gson = new Gson();
        String follupDate = gson.toJson(followupTodayDate);
        mEditor.putString(LeadConstantsSection.LEAD_SAVE_DATE, follupDate);
        mEditor.commit();
       /* SSConstantsSection procleadConstantsSection = new SSConstantsSection();
        HashMap<String, String> followupTodayDate = new HashMap<String, String>();

        followupTodayDate.put(SSConstantsSection.LEAD_FOLUP_TODAY, procleadConstantsSection.getFollowtodayDate());
        SSFilterSaveInstance.getInstance().setSavedatahashmap(followupTodayDate);

        SharedPreferences.Editor mEditor = mSharedPreferencesLead.edit();
        Gson gson = new Gson();
        String follupDate = gson.toJson(followupTodayDate);
        mEditor.putString(SSConstantsSection.LEAD_SAVE_DATE, follupDate);
        mEditor.commit();*/
    }

    private void CallingPayment(Activity activity, PaymentRequest request, String amount, String txnid) {
        SpinnerManager.showSpinner(activity);
        ProcPrivateLeadService.CallingPayment(activity, request, amount, txnid, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(activity);
                Response<PayUPaymentResponse> mRes = (Response<PayUPaymentResponse>) obj;
                PayUPaymentResponse mData = mRes.body();

                String str = new Gson().toJson(mData, PayUPaymentResponse.class);
                Log.i(TAG, "OnSuccess: " + str);

                try {
                    if (mRes.code() == 200) {
                        CommonMethods.setvalueAgainstKey(activity, "amount", "");
                        CommonMethods.setvalueAgainstKey(activity, "txnid", "");
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e.getMessage());
                }
            }

            @Override
            public void OnFailure(Throwable t) {
                SpinnerManager.hideSpinner(activity);
                t.printStackTrace();
            }
        });
    }

}


