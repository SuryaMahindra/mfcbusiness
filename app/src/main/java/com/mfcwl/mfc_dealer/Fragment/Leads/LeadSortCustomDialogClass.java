package com.mfcwl.mfc_dealer.Fragment.Leads;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.Activity.Leads.FilterInstance;
import com.mfcwl.mfc_dealer.Activity.Leads.LeadsInstance;
import com.mfcwl.mfc_dealer.Activity.Leads.SortInstance;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

/**
 * Created by HP 240 G5 on 16-03-2018.
 */

public class LeadSortCustomDialogClass extends Dialog implements View.OnClickListener {

    public Context activity;
    public static Activity a;
    public Dialog d;
    public Button yes;
    public static TextView leads_posted_date_l_to_o, leads_followup_date_l_to_o;
    public static ImageView posteddateimg, folowimg;
    public static RelativeLayout lsrl1, lsrl2;
    public static String leadsortbystatus = "";
    public boolean postedFlag = false;
    public boolean followFlag = false;

    public LeadSortCustomDialogClass(@NonNull Context context, Activity a) {
        super(context);
        this.activity = context;
        LeadSortCustomDialogClass.a = a;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.stock_sort_apply:
                dismiss();
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(a, "dealer_code"), GlobalText.lead_sortby_apply, GlobalText.android);

                if (LeadsInstance.getInstance().getWhichleads().equals("WebLeads")) {
                    Log.e("LeadsInstance ", "WebLeads " + LeadsInstance.getInstance().getWhichleads());
                    NormalLeadsFragment.SortByLeads();
                } else {
                    Log.e("LeadsInstance ", "PrivateLeads " + LeadsInstance.getInstance().getWhichleads());
                    PrivateLeadsFragment.SortByPrivateLeads();
                }
                break;
            default:
                break;
        }
        dismiss();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.lead_sort_dialog);

        leads_posted_date_l_to_o = findViewById(R.id.leads_posted_date_l_to_o);
        leads_followup_date_l_to_o = findViewById(R.id.leads_followup_date_l_to_o);

        posteddateimg = findViewById(R.id.posteddateimg);
        folowimg = findViewById(R.id.folowimg);
        posteddateimg.setVisibility(View.VISIBLE);
        posteddateimg.animate().rotation(180).setDuration(180).start();
        folowimg.setVisibility(View.INVISIBLE);
        lsrl1 = findViewById(R.id.lsrl1);
        lsrl2 = findViewById(R.id.lsrl2);
        if (!FilterInstance.getInstance().getNotification().equals("new lead")) {
            if (SortInstance.getInstance().getSortby().equals("posted_date_l_o")) {
                posteddateimg.setVisibility(View.VISIBLE);
                folowimg.setVisibility(View.INVISIBLE);
                postedFlag = false;
                SortInstance.getInstance().setSortby("posted_date_l_o");
                posteddateimg.animate().rotation(180).setDuration(180).start();
                leads_posted_date_l_to_o.setText("Posted date(Latest to Oldest)");
                leadsortbystatus = "posted_date_l_o";
            } else if (SortInstance.getInstance().getSortby().equals("posted_date_o_l")) {
                posteddateimg.setVisibility(View.VISIBLE);
                folowimg.setVisibility(View.INVISIBLE);
                postedFlag = true;
                SortInstance.getInstance().setSortby("posted_date_o_l");
                posteddateimg.animate().rotation(0).setDuration(180).start();
                leads_posted_date_l_to_o.setText("Posted date(Oldest to Latest)");
                leadsortbystatus = "posted_date_o_l";
            } else if (SortInstance.getInstance().getSortby().equals("folowup_date_l_o")) {
                posteddateimg.setVisibility(View.INVISIBLE);
                folowimg.setVisibility(View.VISIBLE);
                followFlag = false;
                SortInstance.getInstance().setSortby("folowup_date_l_o");
                folowimg.animate().rotation(180).setDuration(180).start();
                leads_followup_date_l_to_o.setText("Follow up date(Latest to Oldest)");
                leadsortbystatus = "folowup_date_l_o";
            } else if (SortInstance.getInstance().getSortby().equals("folowup_date_o_l")) {
                posteddateimg.setVisibility(View.INVISIBLE);
                folowimg.setVisibility(View.VISIBLE);
                followFlag = true;
                SortInstance.getInstance().setSortby("folowup_date_o_l");
                folowimg.animate().rotation(0).setDuration(180).start();
                leads_followup_date_l_to_o.setText("Follow up date(Oldest to Latest)");
                leadsortbystatus = "folowup_date_o_l";
            }
        } else {
            postedFlag = false;
            SortInstance.getInstance().setSortby("posted_date_l_o");
            posteddateimg.animate().rotation(180).setDuration(180).start();
            leads_posted_date_l_to_o.setText("Posted date(Latest to Oldest)");
            leadsortbystatus = "posted_date_l_o";
        }


        lsrl1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                posteddateimg.setVisibility(View.VISIBLE);
                folowimg.setVisibility(View.INVISIBLE);
                if (postedFlag) {
                    postedFlag = false;
                    SortInstance.getInstance().setSortby("posted_date_l_o");
                    posteddateimg.animate().rotation(180).setDuration(180).start();
                    leads_posted_date_l_to_o.setText("Posted date(Latest to Oldest)");
                    leadsortbystatus = "posted_date_l_o";
                } else {
                    postedFlag = true;
                    SortInstance.getInstance().setSortby("posted_date_o_l");
                    posteddateimg.animate().rotation(0).setDuration(180).start();
                    leads_posted_date_l_to_o.setText("Posted date(Oldest to Latest)");
                    leadsortbystatus = "posted_date_o_l";
                }


            }
        });
        lsrl2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                folowimg.setVisibility(View.VISIBLE);
                posteddateimg.setVisibility(View.INVISIBLE);
                if (followFlag) {
                    followFlag = false;
                    SortInstance.getInstance().setSortby("folowup_date_l_o");
                    folowimg.animate().rotation(180).setDuration(180).start();
                    leads_followup_date_l_to_o.setText("Follow up date(Latest to Oldest)");
                    leadsortbystatus = "folowup_date_l_o";
                } else {
                    followFlag = true;
                    SortInstance.getInstance().setSortby("folowup_date_o_l");
                    folowimg.animate().rotation(0).setDuration(180).start();
                    leads_followup_date_l_to_o.setText("Follow up date(Oldest to Latest)");
                    leadsortbystatus = "folowup_date_o_l";
                }
            }
        });

        yes = findViewById(R.id.stock_sort_apply);
        yes.setOnClickListener(this);

    }
}