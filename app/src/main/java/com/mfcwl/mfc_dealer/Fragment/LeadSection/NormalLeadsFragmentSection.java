package com.mfcwl.mfc_dealer.Fragment.LeadSection;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.Activity.LeadSection.LeadConstantsSection;
import com.mfcwl.mfc_dealer.Activity.Leads.LeadDetailsActivity;
import com.mfcwl.mfc_dealer.Activity.Leads.WebLeadsConstant;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Adapter.LeadSection.NormalLeadsAdapterSection;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.InstanceCreate.LeadSection.LeadFilterSaveInstance;
import com.mfcwl.mfc_dealer.InstanceCreate.LeadSection.SortInstanceLeadSection;
import com.mfcwl.mfc_dealer.Interface.LeadSection.FilterApply;
import com.mfcwl.mfc_dealer.Interface.LeadSection.LazyloaderWebLeads;
import com.mfcwl.mfc_dealer.Interface.LeadSection.SearchbyWebLeads;
import com.mfcwl.mfc_dealer.Model.LeadSection.WebLeadsCustomWhere;
import com.mfcwl.mfc_dealer.Model.LeadSection.WebLeadsDatum;
import com.mfcwl.mfc_dealer.Model.LeadSection.WebLeadsModelRequest;
import com.mfcwl.mfc_dealer.Model.LeadSection.WebLeadsModelResponse;
import com.mfcwl.mfc_dealer.Model.LeadSection.WebLeadsWhereor;
import com.mfcwl.mfc_dealer.Model.LeadSection.WebleadsWhereIn;
import com.mfcwl.mfc_dealer.Model.LeadSection.WhereNotIn;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.RetrofitServicesLeadSection.PrivateLeadService;
import com.mfcwl.mfc_dealer.RetrofitServicesLeadSection.WebLeadService;
import com.mfcwl.mfc_dealer.Utility.AppConstants;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GAConstants;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import rdm.NotificationActivity;
import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.activity;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.linearLayoutVisible;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchImage;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchicon;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toggle;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toolbar;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.tv_header_title;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ASM;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_STATEHEAD;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ZONALHEAD;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_DEALER_CRE;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_PROCUREMENT;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_SALES;

public class NormalLeadsFragmentSection extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    public boolean flag = false;
    public boolean emailFlag = false;
    public String TAG = getClass().getSimpleName();
    int verticalOffset;
    boolean scrollingUp;
    int i = 0;
    LeadConstantsSection leadConstantsSection;
    private RecyclerView mRecyclerView;
    private TextView mNoresults;
    private Button mbtnAllLeads;
    private Button mbtnFollowUpLeads;
    private LinearLayoutManager mLinearLayoutManager;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private Activity mActivity;
    private Context mContext;

    //  public static List<WebLeadsDatum> normalList;
    private List<WebLeadsCustomWhere> mWebLeadsCustomWheres;
    private List<WhereNotIn> mWebLeadsWherenotin;
    private List<WebLeadsWhereor> mWhereorList;
    private List<WebleadsWhereIn> mWherein;
    private List<WebLeadsCustomWhere> webLeadsCustomWheresList;
    private List<WebLeadsDatum> mFollowuplist;
    private List<WebLeadsDatum> mLeadsDatumList;
    //private String whichLeads = "AllLeads";
    private int pageItem = 100;
    private String page_no = "1";
    private int counts = 1;
    private int negotiationLeadCount = 0;
    private boolean isLoadmore;
    private String searchQuery = "";
    private NormalLeadsAdapterSection mNormalLeadsAdapterSection;
    private HashMap<String, String> postedfollDateHashMap = new HashMap<String, String>();

    /*WebLeadsCustomWhere webLeadsCustomWhere;*/

    // WebLeadsModelRequest webLeadsModelRequest;
    private JSONArray leadstatusJsonArray = new JSONArray();

    public NormalLeadsFragmentSection() {
        // Required empty public constructor
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        String uType = CommonMethods.getstringvaluefromkey(activity, "user_type");
        searchImage.setImageResource(R.drawable.search);
        // menu.clear();
        MenuItem asm_mail = menu.findItem(R.id.asm_mail);

        //new ASM
        if (uType.equalsIgnoreCase("dealer")) {
            asm_mail.setVisible(false);
        } else if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_DEALER_CRE) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_SALES) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_PROCUREMENT)) {
            asm_mail.setVisible(false);
        } else {
            asm_mail.setVisible(true);
        }
        MenuItem asmHome = menu.findItem(R.id.asmHome);
        MenuItem add_image = menu.findItem(R.id.add_image);
        MenuItem share = menu.findItem(R.id.share);
        MenuItem delete_fea = menu.findItem(R.id.delete_fea);
        MenuItem stock_fil_clear = menu.findItem(R.id.stock_fil_clear);
        MenuItem notification_bell = menu.findItem(R.id.notification_bell);
        MenuItem notification_cancel = menu.findItem(R.id.notification_cancel);

        asmHome.setVisible(false);
        add_image.setVisible(false);
        share.setVisible(false);
        delete_fea.setVisible(false);
        stock_fil_clear.setVisible(false);
        notification_bell.setVisible(false);
        notification_cancel.setVisible(false);


        asm_mail.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                //  Toast.makeText(getActivity(),"web leads sending email...",Toast.LENGTH_LONG).show();
                if (CommonMethods.isInternetWorking(getActivity())) {

                    // custom dialog
                    Dialog dialog = new Dialog(activity);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setGravity(Gravity.TOP | Gravity.RIGHT);
                    dialog.setContentView(R.layout.common_cus_dialog);
                    TextView Message, Message2;
                    Button cancel, Confirm;
                    Message = dialog.findViewById(R.id.Message);
                    Message2 = dialog.findViewById(R.id.Message2);
                    cancel = dialog.findViewById(R.id.cancel);
                    Confirm = dialog.findViewById(R.id.Confirm);
                    Message.setText(GlobalText.are_you_sure_to_send_mail);
                    //Message2.setText("Go ahead and Submit ?");
                    Confirm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            //  sendEmailWebLeadDetails(getActivity(), webLeadsModelRequest);
                            emailFlag = true;
                            prepareWebLeadRequest();

                            dialog.dismiss();
                        }
                    });
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();


                } else {
                    CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);

                }
                return true;
            }
        });


        //return true;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.layout_normal_leads, container, false);
        mActivity = getActivity();
        mContext = getContext();
        setHasOptionsMenu(true);
        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ASM)) {
            linearLayoutVisible.setVisibility(View.VISIBLE);
            searchicon.setVisibility(View.VISIBLE);
            searchImage.setVisibility(View.VISIBLE);
        }
        if (tv_header_title != null) {
            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText("Sales Leads");
            tv_header_title.setTextColor(getResources().getColor(R.color.white));
        }
        InitView(view);
        return view;
    }

    private void InitView(View view) {

        searchicon.setVisibility(View.VISIBLE);
        linearLayoutVisible.setVisibility(View.VISIBLE);
        emailFlag = false;

        leadConstantsSection = new LeadConstantsSection();

        mRecyclerView = view.findViewById(R.id.normal_leads_recycler);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        mNoresults = view.findViewById(R.id.noresults);
        mbtnAllLeads = view.findViewById(R.id.btnAllLeads);
        mbtnFollowUpLeads = view.findViewById(R.id.btnFollowUpLeads);

        mSwipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.CYAN);

        mbtnAllLeads.setOnClickListener(this);
        mbtnFollowUpLeads.setOnClickListener(this);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        isLoadmore = false;

        if (WebLeadsConstant.getInstance().getWhichleads().equalsIgnoreCase("AllLeads")) {
            mbtnAllLeads.setTextColor(Color.BLACK);
            mbtnAllLeads.setBackgroundResource(R.drawable.my_button);
            mbtnFollowUpLeads.setTextColor(Color.parseColor("#b9b9b9"));
            mbtnFollowUpLeads.setBackgroundResource(R.drawable.my_buttom_gray_light);
        } else if (WebLeadsConstant.getInstance().getWhichleads().equalsIgnoreCase("FollowupLeads")) {
            mbtnFollowUpLeads.setTextColor(Color.BLACK);
            mbtnFollowUpLeads.setBackgroundResource(R.drawable.my_button);
            mbtnAllLeads.setTextColor(Color.parseColor("#b9b9b9"));
            mbtnAllLeads.setBackgroundResource(R.drawable.my_buttom_gray_light);
        }

        if (CommonMethods.getstringvaluefromkey(getActivity(), "WebLeadsP").equalsIgnoreCase("true")) {
            //   CommonMethods.setvalueAgainstKey(getActivity(), "WebLeadsP", "false");
            try {
                i = Integer.parseInt(LeadDetailsActivity.position);
            } catch (Exception e) {
                i = 0;
                // e.printStackTrace();
            }
        } else {
            i = 0;
        }

        onCallbackEvents();
        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
            bottomTabScrolling();
        }

        prepareWebLeadRequest();
        int margin = getResources().getDimensionPixelSize(R.dimen.recyclepadding);

        //new ASM
        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
        } else {
            ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) mSwipeRefreshLayout.getLayoutParams();
            mlp.setMargins(0, 0, 0, margin);
        }

    }

    private void bottomTabScrolling() {
        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {

            // Determines the scroll UP/DOWN direction
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                verticalOffset += dy;
                scrollingUp = dy > 0;

                Log.i(TAG, "onScrolled: ==" + scrollingUp);
                Log.i(TAG, "onScrolled: = dy = " + dy);


                if (scrollingUp) {

                    if (flag) {
                        MainActivity.bottom_topAnimation();
                        flag = false;
                    }
                } else {
                    if (!flag) {
                        MainActivity.top_bottomAnimation();
                        flag = true;
                    }
                }
            }
        });
    }

    private void prepareWebLeadRequest() {

        webLeadsCustomWheresList = new ArrayList<>();
        mWherein = new ArrayList<>();
        mWhereorList = new ArrayList<>();
        mWebLeadsWherenotin = new ArrayList<>();
        mWebLeadsCustomWheres = new ArrayList<>();

        WebLeadsModelRequest webLeadsModelRequest = new WebLeadsModelRequest();

        webLeadsModelRequest.setFilterByFields("");
        webLeadsModelRequest.setPerPage(pageItem);
        webLeadsModelRequest.setPage(Integer.parseInt(page_no));

        webLeadsModelRequest.setAccessToken(CommonMethods.getstringvaluefromkey(mActivity, "access_token"));

        //Search input Values
        searchbyWebLeads(mWhereorList);

        webLeadsModelRequest.setWhereOr(mWhereorList);

        webLeadsModelRequest.setAliasFields("leads.created_at as PostedDate,leads.created_at as LeadDate,leads.id as id,dispatched.id as dispatchid,leads.customer_name as Name,leads.customer_mobile as Mobile,leads.customer_email as Email,leads.primary_make,leads.primary_model,leads.primary_variant,leads.secondary_make,leads.secondary_model,leads.secondary_variant,leads.register_no as regno,leads.register_month as regmonth,leads.register_year as regyear,leads.color,leads.kms,mfg_year,leads.owner as owners,leads.register_city,leads.stock_listed_price as price,dispatched.status,dispatched.followup_comments as remark,dispatched.followup_date as FollowUpDate,dispatched.employee_name as ExecutiveName,leads.lead_type");

        sortbyWebLeads(webLeadsModelRequest);

        webLeadsModelRequest.setCustomWhere(mWebLeadsCustomWheres);
        webLeadsModelRequest.setWherenotin(mWebLeadsWherenotin);

        webLeadsModelRequest.setReportPrefix("Follow up Sales Lead Report");
        webLeadsModelRequest.setTag("android");

        //new changes
        WebLeadsCustomWhere webLeadsCustomWhere;
        webLeadsCustomWhere = new WebLeadsCustomWhere();


        setCustomWhereWebLeads(webLeadsCustomWhere);

        //Posted Followup Date Filter
        setPostedFollowupDateInfo();

        //Update Lead Status
        WebleadsWhereIn webleadsWhereIn = new WebleadsWhereIn();
        setLeadStatus(webleadsWhereIn);

        webLeadsModelRequest.setWhereIn(mWherein);
        webLeadsModelRequest.setCustomWhere(webLeadsCustomWheresList);
        //Log.e("request#####", webLeadsModelRequest)

        // postedfollDateHashMap = LeadFilterSaveInstance.getInstance().getSavedatahashmap();

        if (emailFlag) {
            sendEmailWebLeadDetails(mContext, webLeadsModelRequest);
            emailFlag = false;
        } else {
            sendWebLeadDetails(mContext, webLeadsModelRequest);
        }

    }

    private void sendWebLeadDetails(final Context mContext, WebLeadsModelRequest webLeadsModelRequest) {
        mSwipeRefreshLayout.setRefreshing(false);
        SpinnerManager.showSpinner(mContext);

        WebLeadService.postWebLead(webLeadsModelRequest, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                Response<WebLeadsModelResponse> mRes = (Response<WebLeadsModelResponse>) obj;
                WebLeadsModelResponse mData = mRes.body();

                String str = new Gson().toJson(mData, WebLeadsModelResponse.class);
                Log.i(TAG, "OnSuccess: " + str);

                if (!mData.getStatus().equals("failure")) {
                    negotiationLeadCount = mData.getTotal();
                    List<WebLeadsDatum> mlist = mData.getData();
                    Log.i(TAG, "OnSuccess: " + mlist.size());
                    if (isLoadmore) {
                        mNormalLeadsAdapterSection.notifyItemInserted(mLeadsDatumList.size() - 1);
                        mLeadsDatumList.addAll(mlist);
                        mNormalLeadsAdapterSection.notifyDataSetChanged();
                    } else {
                        mLeadsDatumList = new ArrayList<>();
                        mLeadsDatumList.addAll(mlist);
                        attachtoAdapter(mLeadsDatumList);
                    }
                    isLeadsAvailable(negotiationLeadCount);

                } else {
                    WebServicesCall.error_popup2(getActivity(), Integer.parseInt(mData.getStatusCode().toString()), "");
                }
            }

            @Override
            public void OnFailure(Throwable t) {
                SpinnerManager.hideSpinner(mContext);
                mSwipeRefreshLayout.setRefreshing(false);
                t.printStackTrace();
            }
        });

    }


    private void sendEmailWebLeadDetails(final Context mContext, WebLeadsModelRequest webLeadsModelRequest) {
        mSwipeRefreshLayout.setRefreshing(false);
        SpinnerManager.showSpinner(mContext);

        PrivateLeadService.sendEmailpostWebLead(webLeadsModelRequest, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                Response<String> mRes = (Response<String>) obj;
                String mData = mRes.body();
                CommonMethods.alertMessage(getActivity(), "Mail sent" + "\n" + mData.toString());


            }

            @Override
            public void OnFailure(Throwable t) {
                SpinnerManager.hideSpinner(mContext);
                mSwipeRefreshLayout.setRefreshing(false);
                t.printStackTrace();
            }
        });

    }


    private void preparefollowupWebLeadRequest() {

        webLeadsCustomWheresList = new ArrayList<>();
        mWherein = new ArrayList<>();
        mWhereorList = new ArrayList<>();
        mFollowuplist = new ArrayList<>();
        mWebLeadsWherenotin = new ArrayList<>();
        mWebLeadsCustomWheres = new ArrayList<>();

        WebLeadsModelRequest webLeadsModelRequest = new WebLeadsModelRequest();

        webLeadsModelRequest.setPerPage(pageItem);
        webLeadsModelRequest.setPage(Integer.parseInt(page_no));

        webLeadsModelRequest.setAccessToken(CommonMethods.getstringvaluefromkey(mActivity, "access_token"));

        //Search input Values
        searchbyWebLeads(mWhereorList);

        webLeadsModelRequest.setWhereOr(mWhereorList);
        webLeadsModelRequest.setAliasFields("leads.created_at as PostedDate,leads.created_at as LeadDate,leads.id as id,dispatched.id as dispatchid,leads.customer_name as Name,leads.customer_mobile as Mobile,leads.customer_email as Email,leads.primary_make,leads.primary_model,leads.primary_variant,leads.secondary_make,leads.secondary_model,leads.secondary_variant,leads.register_no as regno,leads.register_month as regmonth,leads.register_year as regyear,leads.color,leads.kms,mfg_year,leads.owner as owners,leads.register_city,leads.stock_listed_price as price,dispatched.status,dispatched.followup_comments as remark,dispatched.followup_date as FollowUpDate,dispatched.employee_name as ExecutiveName,leads.lead_type");

        sortbyWebLeads(webLeadsModelRequest);

        webLeadsModelRequest.setCustomWhere(mWebLeadsCustomWheres);
        webLeadsModelRequest.setWherenotin(mWebLeadsWherenotin);

        webLeadsModelRequest.setReportPrefix("Follow up Sales Lead Report");
        webLeadsModelRequest.setTag("android");


        //follow-up date (apply future date over here)
        WebLeadsCustomWhere webLeadsCustomWhere = new WebLeadsCustomWhere();
       /* Date d = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(d);
        webLeadsCustomWhere.setColumn("dispatched.followup_date");
        webLeadsCustomWhere.setOperator("<=");
        webLeadsCustomWhere.setValue(formattedDate + " 23:59:59");
        webLeadsCustomWheresList.add(webLeadsCustomWhere);
        //End
*/
        //new add class //
        setCustomWhereWebLeadsFollwup(webLeadsCustomWhere);


        //Posted Followup Date Filter
        setPostedFollowupDateInfo();

        //Update Lead Status
        WebleadsWhereIn webleadsWhereIn = new WebleadsWhereIn();
        setLeadStatus(webleadsWhereIn);
        webLeadsModelRequest.setWhereIn(mWherein);

        webLeadsModelRequest.setCustomWhere(webLeadsCustomWheresList);

        sendWebLeadFollowupDetails(mContext, webLeadsModelRequest);

    }

    private void sendWebLeadFollowupDetails(final Context mContext, WebLeadsModelRequest webLeadsModelRequest) {
        mSwipeRefreshLayout.setRefreshing(false);
        SpinnerManager.showSpinner(mContext);
        WebLeadService.postWebLead(webLeadsModelRequest, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);

                Response<WebLeadsModelResponse> mRes = (Response<WebLeadsModelResponse>) obj;
                WebLeadsModelResponse mData = mRes.body();

                String str = new Gson().toJson(mData, WebLeadsModelResponse.class);
                Log.i(TAG, "OnSuccess: " + str);

                if (!mData.getStatus().equals("failure")) {
                    negotiationLeadCount = mData.getTotal();
                    List<WebLeadsDatum> mlist = mData.getData();
                    Log.i(TAG, "OnSuccess: " + mlist.size());
                    WebLeadsDatum mWebLeadsDatum = null;
                    for (int i = 0; i < mlist.size(); i++) {
                        mWebLeadsDatum = mlist.get(i);
                        if (mWebLeadsDatum.getStatus() != null && !mWebLeadsDatum.getStatus().equalsIgnoreCase("Sold") &&
                                !mWebLeadsDatum.getStatus().equalsIgnoreCase("Lost") &&
                                !mWebLeadsDatum.getStatus().equalsIgnoreCase("Closed") && !mWebLeadsDatum.getStatus().equalsIgnoreCase("Not-Interested")
                                && !mWebLeadsDatum.getStatus().equalsIgnoreCase("Finalised")) {
                            mFollowuplist.add(mWebLeadsDatum);
                        }

                    }
                    if (isLoadmore) {
                        mNormalLeadsAdapterSection.notifyItemInserted(mLeadsDatumList.size() - 1);
                        mLeadsDatumList.addAll(mFollowuplist);
                        mNormalLeadsAdapterSection.notifyDataSetChanged();
                    } else {
                        mLeadsDatumList = new ArrayList<>();
                        mLeadsDatumList.addAll(mFollowuplist);
                        attachtoAdapter(mLeadsDatumList);
                    }
                    isLeadsAvailable(negotiationLeadCount);
                } else {
                    WebServicesCall.error_popup2(getActivity(), Integer.parseInt(mData.getStatusCode().toString()), "");
                }

            }

            @Override
            public void OnFailure(Throwable t) {
                SpinnerManager.hideSpinner(mContext);
                mSwipeRefreshLayout.setRefreshing(false);
                t.printStackTrace();
            }
        });

    }

    private void isLeadsAvailable(int negotiationLeadCount) {
        if (negotiationLeadCount == 0) {
            mNoresults.setVisibility(View.VISIBLE);
            mSwipeRefreshLayout.setVisibility(View.GONE);
        } else {
            mNoresults.setVisibility(View.GONE);
            mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        }
    }

    private void attachtoAdapter(List<WebLeadsDatum> mlist) {

        //   Log.e("position######", i + "");
        mNormalLeadsAdapterSection = new NormalLeadsAdapterSection(mContext, mlist, mActivity);
        mRecyclerView.setHasFixedSize(true);
        mLinearLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setAdapter(mNormalLeadsAdapterSection);

        mRecyclerView.scrollToPosition(i);

    }

    @Override
    public void onClick(View v) {
        restoreInputValues();
        i = 0;
        switch (v.getId()) {
            case R.id.btnAllLeads:
                //  whichLeads = "AllLeads";
                WebLeadsConstant.getInstance().setWhichleads("AllLeads");
                mbtnAllLeads.setTextColor(Color.BLACK);
                mbtnAllLeads.setBackgroundResource(R.drawable.my_button);
                mbtnFollowUpLeads.setTextColor(Color.parseColor("#b9b9b9"));
                mbtnFollowUpLeads.setBackgroundResource(R.drawable.my_buttom_gray_light);
                prepareWebLeadRequest();
                break;
            case R.id.btnFollowUpLeads:
                //  whichLeads = "FollowupLeads";
                WebLeadsConstant.getInstance().setWhichleads("FollowupLeads");
                mbtnFollowUpLeads.setTextColor(Color.BLACK);
                mbtnFollowUpLeads.setBackgroundResource(R.drawable.my_button);
                mbtnAllLeads.setTextColor(Color.parseColor("#b9b9b9"));
                mbtnAllLeads.setBackgroundResource(R.drawable.my_buttom_gray_light);
                preparefollowupWebLeadRequest();
                break;
        }

    }

    @Override
    public void onRefresh() {
        restoreInputValues();
        i = 0;
        mSwipeRefreshLayout.setRefreshing(true);
        if (WebLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
            prepareWebLeadRequest();
        } else {
            preparefollowupWebLeadRequest();
        }
    }

    private void sortbyWebLeads(WebLeadsModelRequest webLeadsModelRequest) {

        if (SortInstanceLeadSection.getInstance().getSortby().equals("posted_date_l_o")) {
            webLeadsModelRequest.setOrderBy("leads.created_at");
            webLeadsModelRequest.setOrderByReverse("true");
        } else if (SortInstanceLeadSection.getInstance().getSortby().equals("posted_date_o_l")) {
            webLeadsModelRequest.setOrderBy("leads.created_at");
            webLeadsModelRequest.setOrderByReverse("false");
        } else if (SortInstanceLeadSection.getInstance().getSortby().equals("folowup_date_l_o")) {
            webLeadsModelRequest.setOrderBy("dispatched.followup_date");
            webLeadsModelRequest.setOrderByReverse("true");

        } else if (SortInstanceLeadSection.getInstance().getSortby().equals("folowup_date_o_l")) {
            webLeadsModelRequest.setOrderBy("dispatched.followup_date");
            webLeadsModelRequest.setOrderByReverse("false");
        } else {
            webLeadsModelRequest.setOrderBy("leads.created_at");
            webLeadsModelRequest.setOrderByReverse("true");
        }

    }


    private void setCustomWhereWebLeads(WebLeadsCustomWhere webLeadsCustomWhere) {
        //Add Dealer Id into DashboardCustomWhere
        webLeadsCustomWhere.setColumn("dispatched.target_id");
        webLeadsCustomWhere.setOperator("=");
        webLeadsCustomWhere.setValue(CommonMethods.getstringvaluefromkey(mActivity, "dealer_code"));
        webLeadsCustomWheresList.add(webLeadsCustomWhere);
        //End

        //Add Target Source into DashboardCustomWhere
        webLeadsCustomWhere = new WebLeadsCustomWhere();
        webLeadsCustomWhere.setColumn("dispatched.target_source");
        webLeadsCustomWhere.setOperator("=");
        webLeadsCustomWhere.setValue("MFC");
        webLeadsCustomWheresList.add(webLeadsCustomWhere);
        //End

        //Add USEDCARSALES
        webLeadsCustomWhere = new WebLeadsCustomWhere();
        webLeadsCustomWhere.setColumn("leads.lead_type");
        webLeadsCustomWhere.setOperator("=");
        webLeadsCustomWhere.setValue("USEDCARSALES");
        webLeadsCustomWheresList.add(webLeadsCustomWhere);

        String usertype = CommonMethods.getstringvaluefromkey(activity, "user_type");
        if (usertype.equalsIgnoreCase(USER_TYPE_SALES) ||
                usertype.equalsIgnoreCase(USER_TYPE_PROCUREMENT)) {
            webLeadsCustomWhere = new WebLeadsCustomWhere();
            webLeadsCustomWhere.setColumn("dispatched.employee_id");
            webLeadsCustomWhere.setOperator("=");
            webLeadsCustomWhere.setValue(CommonMethods.getstringvaluefromkey(mActivity, "user_id"));
            webLeadsCustomWheresList.add(webLeadsCustomWhere);
        }
    }

    private void setCustomWhereWebLeadsFollwup(WebLeadsCustomWhere webLeadsCustomWhere) {
        //Add Dealer Id into DashboardCustomWhere
        if(!webLeadsCustomWheresList.isEmpty())
        {
            webLeadsCustomWheresList.clear();
        }
        webLeadsCustomWhere.setColumn("dispatched.target_id");
        webLeadsCustomWhere.setOperator("=");
        webLeadsCustomWhere.setValue(CommonMethods.getstringvaluefromkey(mActivity, "dealer_code"));
        webLeadsCustomWheresList.add(webLeadsCustomWhere);


        Date d = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(d);
        webLeadsCustomWhere = new WebLeadsCustomWhere();
        webLeadsCustomWhere.setColumn("dispatched.followup_date");
        webLeadsCustomWhere.setOperator("<=");
        webLeadsCustomWhere.setValue(formattedDate + " 23:59:59");
        webLeadsCustomWheresList.add(webLeadsCustomWhere);

        //.add(webLeadsCustomWhere);
        //End

        //End

        //Add Target Source into DashboardCustomWhere
        webLeadsCustomWhere = new WebLeadsCustomWhere();
        webLeadsCustomWhere.setColumn("dispatched.target_source");
        webLeadsCustomWhere.setOperator("=");
        webLeadsCustomWhere.setValue("MFC");
        webLeadsCustomWheresList.add(webLeadsCustomWhere);
        //End

        //Add USEDCARSALES
        webLeadsCustomWhere = new WebLeadsCustomWhere();
        webLeadsCustomWhere.setColumn("leads.lead_type");
        webLeadsCustomWhere.setOperator("=");
        webLeadsCustomWhere.setValue("USEDCARSALES");
        webLeadsCustomWheresList.add(webLeadsCustomWhere);

        String usertype = CommonMethods.getstringvaluefromkey(activity, "user_type");
        if (usertype.equalsIgnoreCase(USER_TYPE_SALES) ||
                usertype.equalsIgnoreCase(USER_TYPE_PROCUREMENT)) {
            webLeadsCustomWhere = new WebLeadsCustomWhere();
            webLeadsCustomWhere.setColumn("dispatched.employee_id");
            webLeadsCustomWhere.setOperator("=");
            webLeadsCustomWhere.setValue(CommonMethods.getstringvaluefromkey(mActivity, "user_id"));
            webLeadsCustomWheresList.add(webLeadsCustomWhere);
        }
    }

    private void setPostedFollowupDateInfo() {


        try {

            postedfollDateHashMap = LeadFilterSaveInstance.getInstance().getSavedatahashmap();

            String postdateinfo = "";

            if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_TODAY) != null) {
                postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_TODAY);
            }
            if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_YESTER) != null) {
                postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_YESTER);
            }
            if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_7DAYS) != null) {
                postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_7DAYS);
            }
            if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_15DAYS) != null) {
                postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_15DAYS);
            }
            if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE) != null) {
                postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE);
            }

            //   Log.e("postdateinfo ", "LEAD_POST_CUSTDATE " + postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE));
            WebLeadsCustomWhere postDateCustomWhereStart = new WebLeadsCustomWhere();
            WebLeadsCustomWhere postDateCustomWhereEnd = new WebLeadsCustomWhere();


            if (postdateinfo != null && postdateinfo.contains("@")) {
                //split the date and atach to query
                String[] createddates = postdateinfo.split("@");

                try {
                    postDateCustomWhereStart.setColumn("leads.created_at");
                    postDateCustomWhereEnd.setColumn("leads.created_at");

                    if (CommonMethods.getstringvaluefromkey(getActivity(), "user_type").equalsIgnoreCase("dealer")) {


                        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_TODAY) != null ||
                                postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_YESTER) != null ||
                                postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_7DAYS) != null ||
                                postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_15DAYS) != null) {

                            postDateCustomWhereStart.setOperator(">=");
                            postDateCustomWhereEnd.setOperator("<");
                            postDateCustomWhereStart.setValue(createddates[0]);
                            postDateCustomWhereEnd.setValue(createddates[1]);
                            webLeadsCustomWheresList.add(postDateCustomWhereStart);
                            webLeadsCustomWheresList.add(postDateCustomWhereEnd);
                        }

                        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE) != null) {
                            postDateCustomWhereStart.setOperator(">=");
                            postDateCustomWhereEnd.setOperator("<=");
                            postDateCustomWhereStart.setValue(createddates[0] + " 00:00:00");
                            postDateCustomWhereEnd.setValue(createddates[1] + " 23:59:59");

                            webLeadsCustomWheresList.add(postDateCustomWhereStart);
                            webLeadsCustomWheresList.add(postDateCustomWhereEnd);
                        }
                    } else {
                        if (CommonMethods.getstringvaluefromkey(getActivity(), "leadStatus").equalsIgnoreCase("true")) {

                            try {
                                // remove
                                LeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());


                                if (CommonMethods.getstringvaluefromkey(getActivity(), "leads_status")
                                        .equalsIgnoreCase("Used Car Sales Leads Created yesterday")) {

                                    postedfollDateHashMap.put(LeadConstantsSection.LEAD_POST_YESTER, leadConstantsSection.getYesterdayDate());
                                    postedfollDateHashMap.remove(LeadConstantsSection.LEAD_FOLUP_7DAYS);
                                    postedfollDateHashMap.remove(LeadConstantsSection.LEAD_POST_CUSTDATE);

                                    LeadFilterSaveInstance.getInstance().setSavedatahashmap(postedfollDateHashMap);
                                }


                            } catch (Exception e) {

                            }

                            if (CommonMethods.getstringvaluefromkey(getActivity(), "value2").equalsIgnoreCase("")
                                    && !CommonMethods.getstringvaluefromkey(getActivity(), "startDate").equalsIgnoreCase("")) {

                                String startDate = CommonMethods.getstringvaluefromkey(getActivity(), "startDate").toString();
                                String endDate = CommonMethods.getstringvaluefromkey(getActivity(), "endDate").toString();

                                postDateCustomWhereStart.setOperator(">=");
                                postDateCustomWhereEnd.setOperator("<");
                                postDateCustomWhereStart.setValue(startDate + " 00:00:00");
                                postDateCustomWhereEnd.setValue(endDate + " 23:59:59");

                                postDateCustomWhereStart.setOperator(">=");
                                postDateCustomWhereEnd.setOperator("<=");
                                postDateCustomWhereStart.setValue(startDate + " 00:00:00");
                                postDateCustomWhereEnd.setValue(endDate + " 23:59:59");

                                //12/12/19
                                webLeadsCustomWheresList.add(postDateCustomWhereStart);
                                webLeadsCustomWheresList.add(postDateCustomWhereEnd);
                            }


                        } else {

                            // After reset coditions data

                            if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_TODAY) != null ||
                                    postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_YESTER) != null ||
                                    postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_7DAYS) != null ||
                                    postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_15DAYS) != null) {

                                postDateCustomWhereStart.setOperator(">=");
                                postDateCustomWhereEnd.setOperator("<");
                                postDateCustomWhereStart.setValue(createddates[0]);
                                postDateCustomWhereEnd.setValue(createddates[1]);
                                webLeadsCustomWheresList.add(postDateCustomWhereStart);
                                webLeadsCustomWheresList.add(postDateCustomWhereEnd);
                            }

                            if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE) != null) {
                                postDateCustomWhereStart.setOperator(">=");
                                postDateCustomWhereEnd.setOperator("<=");
                                postDateCustomWhereStart.setValue(createddates[0] + " 00:00:00");
                                postDateCustomWhereEnd.setValue(createddates[1] + " 23:59:59");

                                webLeadsCustomWheresList.add(postDateCustomWhereStart);
                                webLeadsCustomWheresList.add(postDateCustomWhereEnd);
                            }

                        }

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {

                //ASM

                if (CommonMethods.getstringvaluefromkey(getActivity(), "user_type").equalsIgnoreCase("dealer")) {


                } else {

                    if (CommonMethods.getstringvaluefromkey(getActivity(), "value2").equalsIgnoreCase("")
                            && !CommonMethods.getstringvaluefromkey(getActivity(), "startDate").equalsIgnoreCase("")) {

                        postDateCustomWhereStart.setColumn("leads.created_at");
                        postDateCustomWhereEnd.setColumn("leads.created_at");


                        if (CommonMethods.getstringvaluefromkey(getActivity(), "leads_status")
                                .equalsIgnoreCase("Used Car Sales Leads Created yesterday")) {
                            postedfollDateHashMap.put(LeadConstantsSection.LEAD_POST_YESTER, leadConstantsSection.getYesterdayDate());
                            postedfollDateHashMap.remove(LeadConstantsSection.LEAD_POST_CUSTDATE);
                            postedfollDateHashMap.remove(LeadConstantsSection.LEAD_FOLUP_7DAYS);
                            LeadFilterSaveInstance.getInstance().setSavedatahashmap(postedfollDateHashMap);
                        }


                        String startDate = CommonMethods.getstringvaluefromkey(getActivity(), "startDate").toString();
                        String endDate = CommonMethods.getstringvaluefromkey(getActivity(), "endDate").toString();
                        if (!startDate.equalsIgnoreCase("")) {

                            postDateCustomWhereStart.setOperator(">=");
                            postDateCustomWhereEnd.setOperator("<");
                            postDateCustomWhereStart.setValue(startDate + " 00:00:00");
                            postDateCustomWhereEnd.setValue(endDate + " 23:59:59");

                            postDateCustomWhereStart.setOperator(">=");
                            postDateCustomWhereEnd.setOperator("<=");
                            postDateCustomWhereStart.setValue(startDate + " 00:00:00");
                            postDateCustomWhereEnd.setValue(endDate + " 23:59:59");

                            webLeadsCustomWheresList.add(postDateCustomWhereStart);
                            webLeadsCustomWheresList.add(postDateCustomWhereEnd);
                        }
                    }

                }


            }

            String followdateinfo = "";

            if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TMRW) != null) {
                followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TMRW);
            }
            if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TODAY) != null) {
                followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TODAY);
            }
            if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_YESTER) != null) {
                followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_YESTER);
            }
            if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_7DAYS) != null) {
                followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_7DAYS);
            }
            if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_CUSTDATE) != null) {
                followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_CUSTDATE);
            }

            WebLeadsCustomWhere followDateCustomWhereStart = new WebLeadsCustomWhere();
            WebLeadsCustomWhere followDateCustomWhereEnd = new WebLeadsCustomWhere();


            if (followdateinfo != null && followdateinfo.contains("@") &&
                    CommonMethods.getstringvaluefromkey(getActivity(), "Lvalue").equalsIgnoreCase("")) {

                String[] follow = followdateinfo.split("@");
                try {

                    followDateCustomWhereStart.setColumn("dispatched.followup_date");
                    followDateCustomWhereEnd.setColumn("dispatched.followup_date");

                    if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TMRW) != null) {
                        followDateCustomWhereStart.setOperator(">");
                        followDateCustomWhereEnd.setOperator("<=");
                        followDateCustomWhereStart.setValue(follow[1]);
                        followDateCustomWhereEnd.setValue(follow[0]);
                    }
                    if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TODAY) != null ||
                            postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_YESTER) != null ||
                            postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_7DAYS) != null) {
                        followDateCustomWhereStart.setOperator(">=");
                        followDateCustomWhereEnd.setOperator("<");
                        followDateCustomWhereStart.setValue(follow[0]);
                        followDateCustomWhereEnd.setValue(follow[1]);
                    }
                    if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_CUSTDATE) != null) {
                        followDateCustomWhereStart.setOperator(">=");
                        followDateCustomWhereEnd.setOperator("<=");
                        followDateCustomWhereStart.setValue(follow[0] + " 00:00:00");
                        followDateCustomWhereEnd.setValue(follow[1] + " 23:59:59");
                    }

                    webLeadsCustomWheresList.add(followDateCustomWhereStart);
                    webLeadsCustomWheresList.add(followDateCustomWhereEnd);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {

                if (CommonMethods.getstringvaluefromkey(getActivity(), "leadStatus").equalsIgnoreCase("true")
                        && !CommonMethods.getstringvaluefromkey(getActivity(), "value2").equalsIgnoreCase("")
                        || CommonMethods.getstringvaluefromkey(getActivity(), "withoutfollowup").equalsIgnoreCase("yes")) {


                    if (CommonMethods.getstringvaluefromkey(getActivity(), "leads_status")
                            .equalsIgnoreCase("Used Car Sales Leads Followup this week")) {

                        postedfollDateHashMap.remove(LeadConstantsSection.LEAD_POST_YESTER);
                        postedfollDateHashMap.put(LeadConstantsSection.LEAD_FOLUP_7DAYS, leadConstantsSection.getFollowlast7Date());
                        LeadFilterSaveInstance.getInstance().setSavedatahashmap(postedfollDateHashMap);
                    } else {


                    }


                    followDateCustomWhereStart.setColumn("dispatched.followup_date");
                    followDateCustomWhereEnd.setColumn("dispatched.followup_date");


                    followDateCustomWhereStart.setOperator(CommonMethods.getstringvaluefromkey(getActivity(), "operator"));
                    followDateCustomWhereEnd.setOperator(CommonMethods.getstringvaluefromkey(getActivity(), "operator2"));

                    followDateCustomWhereStart.setValue(CommonMethods.getstringvaluefromkey(getActivity(), "value"));
                    followDateCustomWhereEnd.setValue(CommonMethods.getstringvaluefromkey(getActivity(), "value2"));


                    webLeadsCustomWheresList.add(followDateCustomWhereStart);

                    if (!CommonMethods.getstringvaluefromkey(getActivity(), "operator").equalsIgnoreCase("=")) {
                        webLeadsCustomWheresList.add(followDateCustomWhereEnd);
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setLeadStatus(WebleadsWhereIn webleadsWhereIn) {

        ArrayList<String> listStatus = new ArrayList<String>();
        leadstatusJsonArray = LeadFilterSaveInstance.getInstance().getStatusarray();
        //   Log.e("setLeadStatus ", "json " + leadstatusJsonArray);

        // New ASM
        if (CommonMethods.getstringvaluefromkey(getActivity(), "user_type").equalsIgnoreCase("dealer")) {

            if (leadstatusJsonArray.length() != 0) {

                webleadsWhereIn.setColumn("dispatched.status");
                for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                    try {
                        listStatus.add(leadstatusJsonArray.getString(i));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                webleadsWhereIn.setValue(listStatus);
                mWherein.add(webleadsWhereIn);

            }

        } else {

            if (CommonMethods.getstringvaluefromkey(getActivity(), "leadStatus").equalsIgnoreCase("true")) {
                //remove if already presents in json arry

                if (!CommonMethods.getstringvaluefromkey(getActivity(), "Lvalue").equalsIgnoreCase("")
                        && CommonMethods.getstringvaluefromkey(getActivity(), "leads_status").equalsIgnoreCase("")
                        || CommonMethods.getstringvaluefromkey(getActivity(), "withoutfollowup").equalsIgnoreCase("yes")) {

                    // remove
                    if (leadstatusJsonArray.length() != 0) {
                        for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                            try {
                                leadstatusJsonArray.remove(i);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    webleadsWhereIn.setColumn("dispatched.status");

                    listStatus.add(CommonMethods.getstringvaluefromkey(getActivity(), "Lvalue"));

                    leadstatusJsonArray.put(CommonMethods.getstringvaluefromkey(getActivity(), "Lvalue"));

                    webleadsWhereIn.setValue(listStatus);
                    mWherein.add(webleadsWhereIn);

                }

            } else {

                if (leadstatusJsonArray.length() != 0) {

                    webleadsWhereIn.setColumn("dispatched.status");
                    for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                        try {
                            listStatus.add(leadstatusJsonArray.getString(i));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    webleadsWhereIn.setValue(listStatus);
                    mWherein.add(webleadsWhereIn);

                }

            }
        }

    }

    private void onCallbackEvents() {

        ((MainActivity) getActivity()).setFragmentSortListener(new MainActivity.FragmentSortListener() {

            @Override
            public void onSortWeb() {
                if (CommonMethods.getstringvaluefromkey(getActivity(), "WebLeadsP").equalsIgnoreCase("false")) {
                    i = 0;
                }
                restoreInputValues();
                if (WebLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
                    prepareWebLeadRequest();
                } else {
                    preparefollowupWebLeadRequest();
                }
            }
        });

        ((MainActivity) getActivity()).updateSearchWebLead(new SearchbyWebLeads() {
            @Override
            public void onSearchWebLead(String query) {
                searchQuery = query;
                //   Log.e("mLeads ", "size " + mLeadsDatumList.size());
                if (mLeadsDatumList.size() == 0) {
                    restoreInputValues();
                    i = 0;
                    if (WebLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
                        prepareWebLeadRequest();
                    } else {
                        preparefollowupWebLeadRequest();
                    }
                }
                mNormalLeadsAdapterSection.filter(query);
            }
        });

        ((MainActivity) getActivity()).setLazyloaderWebLeadsListener(new LazyloaderWebLeads() {
            @Override
            public void loadmore(int ItemCount) {

                counts = counts + 1;
                double maxPageNumber = Math.ceil((double) (negotiationLeadCount) / (double) pageItem);
                if (counts > maxPageNumber) {
                    return;
                }
                page_no = Integer.toString(counts);

                isLoadmore = true;

                if (WebLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
                    prepareWebLeadRequest();
                } else {
                    preparefollowupWebLeadRequest();
                }

            }

            @Override
            public void updatecountweb(int count) {
                //  Log.e("updatecount ", "size " + count);
                if (count <= 3) {
                    // mLeadsDatumList.clear();
                    if (CommonMethods.getstringvaluefromkey(getActivity(), "WebLeadsP").equalsIgnoreCase("false")) {
                        i = 0;
                    }
                    restoreInputValues();
                    if (WebLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
                        prepareWebLeadRequest();
                    } else {
                        preparefollowupWebLeadRequest();
                    }
                }
            }
        });

        //Apply Filter

        ((MainActivity) getActivity()).setApplyWebFilterListener(new FilterApply() {
            @Override
            public void applyFilter(String typeLead) {
                //Posted Date
                if (CommonMethods.getstringvaluefromkey(getActivity(), "WebLeadsP").equalsIgnoreCase("false")) {
                    i = 0;
                }
                restoreInputValues();
                if (WebLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
                    prepareWebLeadRequest();
                } else {
                    preparefollowupWebLeadRequest();
                }
                postedfollDateHashMap = LeadFilterSaveInstance.getInstance().getSavedatahashmap();
                leadstatusJsonArray = LeadFilterSaveInstance.getInstance().getStatusarray();

            }
        });

    }

    private void restoreInputValues() {
        page_no = "1";
        counts = 1;
        negotiationLeadCount = 0;
        isLoadmore = false;
    }


    private void searchbyWebLeads(List<WebLeadsWhereor> mWhereorList) {

        if (!searchQuery.equals("")) {
            //Make
            WebLeadsWhereor vehicle_make = new WebLeadsWhereor();
            vehicle_make.setColumn("leads.primary_make");
            vehicle_make.setOperator("like");
            vehicle_make.setValue("%" + searchQuery.trim() + "%");
            mWhereorList.add(vehicle_make);

            //Model
            WebLeadsWhereor vehicle_model = new WebLeadsWhereor();
            vehicle_model.setColumn("leads.primary_model");
            vehicle_model.setOperator("like");
            vehicle_model.setValue("%" + searchQuery.trim() + "%");
            mWhereorList.add(vehicle_model);

            //Variant
            WebLeadsWhereor vehicle_variant = new WebLeadsWhereor();
            vehicle_variant.setColumn("leads.primary_variant");
            vehicle_variant.setOperator("like");
            vehicle_variant.setValue("%" + searchQuery.trim() + "%");
            mWhereorList.add(vehicle_variant);

            //Name
            WebLeadsWhereor customer_name = new WebLeadsWhereor();
            customer_name.setColumn("leads.customer_name");
            customer_name.setOperator("like");
            customer_name.setValue("%" + searchQuery.trim() + "%");
            mWhereorList.add(customer_name);

            //Mobile
            WebLeadsWhereor customer_mobile = new WebLeadsWhereor();
            customer_mobile.setColumn("leads.customer_mobile");
            customer_mobile.setOperator("like");
            customer_mobile.setValue("%" + searchQuery.trim() + "%");
            mWhereorList.add(customer_mobile);

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //new ASM
        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
        } else {
            Application.getInstance().trackScreenView(activity, GlobalText.asm_webLeads);
        }
        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(AppConstants.LOGINTYPE_DEALER) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(AppConstants.LOGINTYPE_DEALERCRE) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(AppConstants.LOGINTYPE_PROCUREMENT) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(AppConstants.LOGINTYPE_SLAES) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ZONALHEAD) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_STATEHEAD)) {
            if (tv_header_title != null) {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Sales Leads");
            }
        }

        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ASM)) {

            linearLayoutVisible.setVisibility(View.VISIBLE);
            searchicon.setVisibility(View.VISIBLE);
            searchImage.setVisibility(View.VISIBLE);
            toggle.setHomeAsUpIndicator(R.drawable.ic_new_hamburger_menu);
            toolbar.setBackgroundColor(getResources().getColor(R.color.black));
            if (tv_header_title != null) {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Sales Leads");
                toolbar.setBackgroundColor(getResources().getColor(R.color.black));
                tv_header_title.setTextColor(getResources().getColor(R.color.white));
            }
            Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_SALES_LEADS, CommonMethods.getstringvaluefromkey(activity, "user_id") + System.currentTimeMillis() + "" + CommonMethods.getstringvaluefromkey(activity, "device_id"));

        }


    }

}
