package com.mfcwl.mfc_dealer.Fragment.Leads;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.Activity.Leads.FilterInstance;
import com.mfcwl.mfc_dealer.Activity.Leads.LeadConstants;
import com.mfcwl.mfc_dealer.Activity.Leads.LeadsInstance;
import com.mfcwl.mfc_dealer.Activity.Leads.NotificationInstance;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Activity.SplashActivity;
import com.mfcwl.mfc_dealer.Adapter.Leads.NormalLeadsAdapter;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.DasBoardModels.CustomWhere;
import com.mfcwl.mfc_dealer.DasBoardModels.WebLeadRequest;
import com.mfcwl.mfc_dealer.Model.Leads.NormalLeadModel;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchVal;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.followTodayDate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.followTomorowdate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.mInstance;
import static com.mfcwl.mfc_dealer.Utility.LeadsUtils.ClearFollowDataFilter;
import static com.mfcwl.mfc_dealer.Utility.LeadsUtils.ClearLeadStatusFilter;
import static com.mfcwl.mfc_dealer.Utility.LeadsUtils.ClearPostDataFilter;


/**
 * Created by HP 240 G5 on 12-03-2018.
 */

public class NormalLeadsFragmentNew extends Fragment {


    public static RecyclerView normalLeadsRecy;
    public static TextView noresults;
    public static NormalLeadsAdapter mNormalLeadsAdapter;

    static Activity activity;
    static Context c;
    public static List<NormalLeadModel> myJobList = new ArrayList<>();
    public static Button btnAllLeads;
    public static Button btnFollowUpLeads;
    private static String whichLeads = "allleads";

    public static int counts = 1;
    public static int negotiationLeadCount = 0;
    public static int pageItem = 100;
    public static String page_no = "1";
    public static LinearLayoutManager linearLayoutManager;
    int lastVisibleItemPosition = 0, totalItemCount = 0;
    public static boolean oneTimeSearch = true;
    public static SwipeRefreshLayout swipeRefreshLayoutWebLeads;
    public boolean Flag1 = false;

    public static boolean flagsetonetimecall;

    public static View viewswebLeads;
    private WebLeadRequest mLeadRequest;
    private static SharedPreferences mSharedPreferences = null;
    String TAG = getClass().getSimpleName();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        // Inflate the layout for this fragment
        mSharedPreferences = getActivity().getSharedPreferences("MFC", Context.MODE_PRIVATE);
        View view = inflater.inflate(R.layout.layout_normal_leads, container, false);
        activity = getActivity();
        c = getContext();

        counts = 1;
        negotiationLeadCount = 0;
        pageItem = 100;
        page_no = "1";
        flagsetonetimecall = true;
        normalLeadsRecy = view.findViewById(R.id.normal_leads_recycler);
        swipeRefreshLayoutWebLeads = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayoutWebLeads.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.CYAN);

        SplashActivity.progress = true;
        /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();*/


        swipeRefreshLayoutWebLeads.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                SplashActivity.progress = false;
                if (CommonMethods.isInternetWorking(activity)) {
                    if (whichLeads.equals("allleads")) {
                        normalLeadsRecy.getRecycledViewPool().clear();
                        mNormalLeadsAdapter.notifyDataChanged();
                        ShowAllLeads();
                    } else {
                        normalLeadsRecy.getRecycledViewPool().clear();
                        mNormalLeadsAdapter.notifyDataChanged();
                        ShowFollowUpLeads();
                    }
                }

            }
        });

        noresults = view.findViewById(R.id.noresults);
        noresults.setVisibility(View.GONE);
        normalLeadsRecy.setLayoutManager(new WrapContentLinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));

        btnAllLeads = view.findViewById(R.id.btnAllLeads);
        btnFollowUpLeads = view.findViewById(R.id.btnFollowUpLeads);

        CommonMethods.setvalueAgainstKey(activity, "loadWeb", "false");

        btnAllLeads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //NotificationInstance.getInstance().setTodayfollowdate("");
                SplashActivity.progress = true;
                normalLeadsRecy.getRecycledViewPool().clear();
                mNormalLeadsAdapter.notifyDataChanged();
                btnAllLeads.setTextColor(Color.BLACK);
                btnAllLeads.setBackgroundResource(R.drawable.my_button);
                btnFollowUpLeads.setBackgroundResource(R.drawable.my_buttom_gray_light);
                btnFollowUpLeads.setTextColor(Color.parseColor("#b9b9b9"));
                ShowAllLeads();

            }
        });

        btnFollowUpLeads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //NotificationInstance.getInstance().setTodayfollowdate("");
                SplashActivity.progress = true;
                normalLeadsRecy.getRecycledViewPool().clear();
                mNormalLeadsAdapter.notifyDataChanged();
                btnFollowUpLeads.setTextColor(Color.BLACK);
                btnFollowUpLeads.setBackgroundResource(R.drawable.my_button);
                btnAllLeads.setBackgroundResource(R.drawable.my_buttom_gray_light);
                btnAllLeads.setTextColor(Color.parseColor("#b9b9b9"));

                ShowFollowUpLeads();

            }
        });


        RecyclerView.OnScrollListener nRecyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == 2) {
                    try {
                        if (lastVisibleItemPosition + 1 == totalItemCount) {
                            //Write you code here
                            if (!searchVal.equals("")) {
                                oneTimeSearch = true;

                                counts = counts + 1;

                                double maxPageNumber = Math.ceil((double) (negotiationLeadCount) / (double) pageItem); // d = 3.0

                                if (counts > maxPageNumber) {
                                    return;
                                }
                                page_no = Integer.toString(counts);

                                SearchingByWebLeads();
                            } else {

                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();

            }
        };

        normalLeadsRecy.addOnScrollListener(nRecyclerViewOnScrollListener);


        // We need to detect scrolling changes in the RecyclerView
        normalLeadsRecy.setOnScrollListener(new RecyclerView.OnScrollListener() {

            // Keeps track of the overall vertical offset in the list
            int verticalOffset;

            // Determines the scroll UP/DOWN direction
            boolean scrollingUp;


            @Override
            public final void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                verticalOffset += dy;
                scrollingUp = dy > 0;
                viewswebLeads = recyclerView;
                if (scrollingUp) {

                    if (Flag1) {

                        MainActivity.bottom_topAnimation();

                        Flag1 = false;

                    }
                } else {

                    if (!Flag1) {

                        MainActivity.top_bottomAnimation();

                        Flag1 = true;

                    }

                }
            }
        });


        return view;

    }


    public static void loadmore(int count) {

        Log.e("ShowSnackBar ", "loadmore ");

        // LoginActivity.progress = false;
        counts = counts + 1;

        double maxPageNumber = Math.ceil((double) (negotiationLeadCount) / (double) pageItem); // d = 3.0

        if (counts > maxPageNumber) {
            //NormalLeadsAdapter.progress.setVisibility(View.GONE);
            /*Snackbar.make(viewswebLeads, "End of results", Snackbar.LENGTH_SHORT).setActionTextColor(Color.YELLOW)
                    .setAction("Action", null).show();*/
            //ShowSnackBar("End of results");
            return;
        }
        //NormalLeadsAdapter.progress.setVisibility(View.GONE);
        page_no = Integer.toString(counts);

        CommonMethods.setvalueAgainstKey(activity, "loadWeb", "true");
        if (CommonMethods.isInternetWorking(activity)) {

            if (CommonMethods.getstringvaluefromkey(activity, "moredata").equalsIgnoreCase("moredata")) {

                /*Snackbar.make(viewswebLeads, "Scroll down to see more...", Snackbar.LENGTH_SHORT).setActionTextColor(Color.YELLOW)
                        .setAction("Action", null).show();*/


                //ShowSnackBar("Scroll down to see more...");

                WebServicesCall.webCall(activity, activity, jsonMake1(), "LeadStore", GlobalText.POST);
            } else {

                Toast toast = Toast.makeText(activity, "No More Data.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }

    }

    private static void ShowSnackBar(String msg) {


        Snackbar snackbar = Snackbar
                .make(viewswebLeads, msg, Snackbar.LENGTH_SHORT)
                .setAction("", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });

        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        } else {
            textView.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        snackbar.show();// Changing message stock_fragment color

    }

    private void ShowAllLeads() {

        counts = 1;
        negotiationLeadCount = 0;
        pageItem = 100;
        page_no = "1";
        whichLeads = "allleads";

        flagsetonetimecall = true;

        onResume();

    }

    private void ShowFollowUpLeads() {

        counts = 1;
        negotiationLeadCount = 0;
        pageItem = 100;
        page_no = "1";
        whichLeads = "followleads";
        flagsetonetimecall = true;
        /*LeadFilterFragment.leadstatusJsonArray = new JSONArray();
        LeadFilterFragment.postTodYesLastFromDate="";
        LeadFilterFragment.postTodYesLastToDate="";
        LeadFilterFragment.followTodYesLastFromDate="";
        LeadFilterFragment.followTodYesLastToDate="";

        LeadFilterFragment.postFromDate="";
        LeadFilterFragment.postToDate="";
        LeadFilterFragment.followFromDate="";
        LeadFilterFragment.followToDate="";*/

        onResume();
    }


    public static void Parseleadstore(JSONObject jObj, Activity activity) {

        swipeRefreshLayoutWebLeads.setRefreshing(false);
        try {
            if (jObj.getString("status").equals("failure")) {
                WebServicesCall.error_popup2(activity,Integer.parseInt(jObj.getString("status_code")), "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (CommonMethods.getstringvaluefromkey(activity, "loadWeb").equalsIgnoreCase("true")) {
            mNormalLeadsAdapter.notifyItemInserted(myJobList.size() - 1);
        }

        if (CommonMethods.getstringvaluefromkey(activity, "loadWeb").equalsIgnoreCase("false")) {
            myJobList = new ArrayList<>();
        } else {
            if ((myJobList.size() - 1) > 0) {
                // myJobList.remove(myJobList.size()-1);
            }
        }

        try {

            JSONArray arrayList = jObj.getJSONArray("data");

            for (int i = 0; i < arrayList.length(); i++) {

                Log.e("allleads", "allleads===" + i);

                JSONObject jsonobject = new JSONObject(arrayList.getString(i));

                if (whichLeads.equals("allleads")) {
                    NormalLeadModel normalLeadModel = new NormalLeadModel(jsonobject.getString("PostedDate"),
                            jsonobject.getString("LeadDate"),
                            jsonobject.getString("Name"),
                            jsonobject.getString("Mobile"),
                            jsonobject.getString("Email"),
                            jsonobject.getString("primary_make"),
                            jsonobject.getString("primary_model"),
                            jsonobject.getString("primary_variant"),
                            jsonobject.getString("secondary_make"),

                            jsonobject.getString("secondary_model"),
                            jsonobject.getString("secondary_variant"),
                            jsonobject.getString("regno"),
                            jsonobject.getString("regmonth"),
                            jsonobject.getString("regyear"),
                            jsonobject.getString("color"),
                            jsonobject.getString("kms"),
                            jsonobject.getString("owners"),

                            jsonobject.getString("register_city"),
                            jsonobject.getString("price"),
                            jsonobject.getString("status"),
                            jsonobject.getString("remark"),
                            jsonobject.getString("FollowUpDate"),
                            jsonobject.getString("ExecutiveName"),
                            jsonobject.getString("dispatchid"), jsonobject.getString("mfg_year"), jsonobject.getString("id"));

                    myJobList.add(normalLeadModel);
                } else {
                    if (!jsonobject.getString("status").trim().equalsIgnoreCase("Sold") && !jsonobject.getString("status").trim().equalsIgnoreCase("Lost") && !jsonobject.getString("status").trim().equalsIgnoreCase("Closed")) {

                        NormalLeadModel normalLeadModel = new NormalLeadModel(jsonobject.getString("PostedDate"),
                                jsonobject.getString("LeadDate"),
                                jsonobject.getString("Name"),
                                jsonobject.getString("Mobile"),
                                jsonobject.getString("Email"),
                                jsonobject.getString("primary_make"),
                                jsonobject.getString("primary_model"),
                                jsonobject.getString("primary_variant"),
                                jsonobject.getString("secondary_make"),

                                jsonobject.getString("secondary_model"),
                                jsonobject.getString("secondary_variant"),
                                jsonobject.getString("regno"),
                                jsonobject.getString("regmonth"),
                                jsonobject.getString("regyear"),
                                jsonobject.getString("color"),
                                jsonobject.getString("kms"),
                                jsonobject.getString("owners"),

                                jsonobject.getString("register_city"),
                                jsonobject.getString("price"),
                                jsonobject.getString("status"),
                                jsonobject.getString("remark"),
                                jsonobject.getString("FollowUpDate"),
                                jsonobject.getString("ExecutiveName"),
                                jsonobject.getString("dispatchid"), jsonobject.getString("mfg_year"), jsonobject.getString("id"));

                        myJobList.add(normalLeadModel);

                    } else {

                    }
                }

            }
            negotiationLeadCount = Integer.parseInt(jObj.getString("total").toString());
            //reloadData();

        } catch (JSONException e) {
            e.printStackTrace();

        }

        CommonMethods.setvalueAgainstKey(activity, "moredata", "moredata");
        if (CommonMethods.getstringvaluefromkey(activity, "loadWeb").equalsIgnoreCase("true")) {
            mNormalLeadsAdapter.notifyDataChanged();


        } else {

            // CommonMethods.setvalueAgainstKey(activity, "loadWeb", "true");
            reloadData();
        }

        if (negotiationLeadCount == 0) {
            noresults.setVisibility(View.VISIBLE);
            swipeRefreshLayoutWebLeads.setVisibility(View.GONE);
        } else {
            noresults.setVisibility(View.GONE);
            swipeRefreshLayoutWebLeads.setVisibility(View.VISIBLE);
        }

    }

    public static void reloadData() {

        mNormalLeadsAdapter = new NormalLeadsAdapter(c, myJobList, activity);
        normalLeadsRecy.setHasFixedSize(false);
        linearLayoutManager = new LinearLayoutManager(c);
        normalLeadsRecy.setLayoutManager(linearLayoutManager);

        normalLeadsRecy.setAdapter(mNormalLeadsAdapter);

    }


    @Override
    public void onResume() {
        super.onResume();

        Application.getInstance().trackScreenView(activity,GlobalText.NormalLead_Fragment);
        Log.e("howmany ", "times");
        if (FilterInstance.getInstance().getNotification().equals("new lead")) {
            ClearPostDataFilter();

            ClearFollowDataFilter();

            ClearLeadStatusFilter();

        }

        if (mNormalLeadsAdapter != null) {
            normalLeadsRecy.getRecycledViewPool().clear();
            normalLeadsRecy.stopScroll();
            mNormalLeadsAdapter.notifyDataChanged();
        }

        if (flagsetonetimecall == true) {

            myJobList.size();
            if (!LeadsFragment.normalLead) {
                counts = 1;
                negotiationLeadCount = 0;
                pageItem = 100;
                page_no = "1";
                if (LeadsInstance.getInstance().getWhichleads().equals("WebLeads")) {

                    if (myJobList != null) {
                        myJobList.clear();
                    }


                    WebServicesCall.webCall(activity, getContext(), jsonMake1(), "LeadStore", GlobalText.POST);
                } else {
                    //Log.e("DontWorry ","not Calling");
                }
            } else {
                LeadsFragment.normalLead = false;
                //  reloadData();
                if (mNormalLeadsAdapter != null) {
                    mNormalLeadsAdapter.notifyDataChanged();
                }

            }
        }
        flagsetonetimecall = false;
    }

    public static JSONObject jsonMake1() {

        ArrayList<String> arrayListstatus = new ArrayList<String>();
        JSONObject jObj = new JSONObject();
        JSONObject jObj1 = new JSONObject();
        JSONObject jObj2 = new JSONObject();
        JSONObject jObj3 = new JSONObject();

        JSONObject jObj4 = new JSONObject();
        JSONObject jObj5 = new JSONObject();

        JSONObject jObj6 = new JSONObject();
        JSONObject jObj7 = new JSONObject();

        JSONObject jObj8 = new JSONObject();
        JSONObject jObj9 = new JSONObject();

        JSONObject jObj10 = new JSONObject();

        JSONObject jObj11 = new JSONObject();
        JSONObject jObj12 = new JSONObject();

        JSONObject jObjmake = new JSONObject();
        JSONObject jObjmodel = new JSONObject();
        JSONObject jObjvariant = new JSONObject();
        JSONObject jObjname = new JSONObject();
        JSONObject jObjmobile = new JSONObject();

        JSONObject jObjdealerid = new JSONObject();
        JSONObject jObjtargetsource = new JSONObject();

        JSONObject jObjLeadStatus = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONArray jsonArray_wherein = new JSONArray();
        JSONArray jsonArrayLeadStatus = new JSONArray();

        JSONArray jsonSearchVal = new JSONArray();

        if (!searchVal.equalsIgnoreCase("")) {
            swipeRefreshLayoutWebLeads.setEnabled(false);
        }

        String postdateinfo = "";

        if (mSharedPreferences.getString(LeadConstants.LEAD_POST_TODAY, "") != "") {
            postdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_POST_TODAY, "");
        }

        if (mSharedPreferences.getString(LeadConstants.LEAD_POST_YESTER, "") != "") {
            postdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_POST_YESTER, "");
        }
        if (mSharedPreferences.getString(LeadConstants.LEAD_POST_7DAYS, "") != "") {
            postdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_POST_7DAYS, "");
        }

        if (mSharedPreferences.getString(LeadConstants.LEAD_POST_15DAYS, "") != "") {
            postdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_POST_15DAYS, "");
        }

        if (mSharedPreferences.getString(LeadConstants.LEAD_POST_CUSTDATE, "") != "") {
            postdateinfo = mSharedPreferences.getString(LeadConstants.LEAD_POST_CUSTDATE, "");
        }

        if (postdateinfo != null && postdateinfo.contains("@")) {
            //split the date and atach to query
            String[] createddates = postdateinfo.split("@");

            try {
                if (!LeadFilterFragment.postFromDate.equals("") && !LeadFilterFragment.postToDate.equals("")) {
                    jObj2.put("column", "dispatched.created_at");
                    jObj2.put("operator", ">=");
                    jObj2.put("value", createddates[0] + " 00:00:00");
                    jObj3.put("column", "dispatched.created_at");
                    jObj3.put("operator", "<=");
                    jObj3.put("value", createddates[1] + " 23:59:59");
                    jsonArray.put(jObj2);
                    jsonArray.put(jObj3);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        try {

            if (!LeadFilterFragment.postFromDate.equals("") && !LeadFilterFragment.postToDate.equals("")) {
                jObj2.put("column", "dispatched.created_at");
                jObj2.put("operator", ">=");
                jObj2.put("value", LeadFilterFragment.postFromDate + " 00:00:00");
                jObj3.put("column", "dispatched.created_at");
                jObj3.put("operator", "<=");
                jObj3.put("value", LeadFilterFragment.postToDate + " 23:59:59");
                jsonArray.put(jObj2);
                jsonArray.put(jObj3);
            }

            if (!LeadFilterFragment.followFromDate.equals("") && !LeadFilterFragment.followToDate.equals("")) {
                jObj4.put("column", "dispatched.followup_date");
                jObj4.put("operator", ">=");
                jObj4.put("value", LeadFilterFragment.followFromDate + " 00:00:00");
                jObj5.put("column", "dispatched.followup_date");
                jObj5.put("operator", "<=");
                jObj5.put("value", LeadFilterFragment.followToDate + " 23:59:59");
                jsonArray.put(jObj4);
                jsonArray.put(jObj5);
                NotificationInstance.getInstance().setTodayfollowdate("");
            }

            if (!followTodayDate.equals("") && !followTomorowdate.equals("")) {
                jObj11.put("column", "dispatched.followup_date");
                jObj11.put("operator", ">=");
                jObj11.put("value", LeadFilterFragment.followTodayDate + " 00:00:00");
                jObj12.put("column", "dispatched.followup_date");
                jObj12.put("operator", "<=");
                jObj12.put("value", LeadFilterFragment.followTodayDate + " 23:59:59");
                jsonArray.put(jObj11);
                jsonArray.put(jObj12);

            }

            if (!LeadFilterFragment.postTodYesLastFromDate.equals("") && !LeadFilterFragment.postTodYesLastToDate.equals("")) {
                jObj6.put("column", "dispatched.created_at");
                jObj6.put("operator", ">=");
                jObj6.put("value", LeadFilterFragment.postTodYesLastFromDate + " 00:00:00");
                jObj7.put("column", "dispatched.created_at");
                jObj7.put("operator", "<=");
                if (!mInstance.getPtoday().equals("") ||
                        !mInstance.getPyesterday().equals("")) {
                    jObj7.put("value", LeadFilterFragment.postTodYesLastFromDate + " 23:59:59");
                } else {
                    jObj7.put("value", LeadFilterFragment.postTodYesLastToDate + " 23:59:59");
                }
                jsonArray.put(jObj6);
                jsonArray.put(jObj7);

            }
            if (FilterInstance.getInstance().getNotification().equals("new lead")) {
                LeadFilterFragment.followTodYesLastFromDate = "";
                LeadFilterFragment.followTodYesLastToDate = "";
            }
            if (!LeadFilterFragment.followTodYesLastFromDate.equals("") && !LeadFilterFragment.followTodYesLastToDate.equals("")) {

                jObj8.put("column", "dispatched.followup_date");
                jObj8.put("operator", ">=");
                jObj8.put("value", LeadFilterFragment.followTodYesLastFromDate + " 00:00:00");
                jObj9.put("column", "dispatched.followup_date");
                jObj9.put("operator", "<=");
                // Log.e("followTod ","followTod "+FilterInstance.getInstance().getFtoday());
                //FilterInstance.getInstance().setFtoday("");
                if (!mInstance.getFtoday().equals("") ||
                        !mInstance.getFyesterday().equals("")) {
                    jObj9.put("value", LeadFilterFragment.followTodYesLastFromDate + " 23:59:59");
                } else {
                    jObj9.put("value", LeadFilterFragment.followTodYesLastToDate + " 23:59:59");
                }

                //mInstance.setFtoday("");
                jsonArray.put(jObj8);
                jsonArray.put(jObj9);
                NotificationInstance.getInstance().setTodayfollowdate("");
            }

            if (!NotificationInstance.getInstance().getTodayfollowdate().equals("")) {
                String[] splitnotDate = NotificationInstance.getInstance().getTodayfollowdate().split("@", 10);
                jObj8.put("column", "dispatched.followup_date");
                jObj8.put("operator", ">=");
                jObj8.put("value", splitnotDate[0] + " 00:00:00");
                jObj9.put("column", "dispatched.followup_date");
                jObj9.put("operator", "<=");
                jObj9.put("value", splitnotDate[0] + " 23:59:59");
                jsonArray.put(jObj8);
                jsonArray.put(jObj9);

            }


            jObj.put("filter_by_fields", "");
            jObj.put("per_page", Integer.toString(pageItem));
            jObj.put("page", page_no);

            //dealer id
            jObjdealerid.put("column", "dispatched.target_id");
            jObjdealerid.put("operator", "=");
            jObjdealerid.put("value", CommonMethods.getstringvaluefromkey(activity, "dealer_code"));
            jsonArray.put(jObjdealerid);

            jObjtargetsource.put("column", "dispatched.target_source");
            jObjtargetsource.put("operator", "=");
            jObjtargetsource.put("value", "MFC");
            jsonArray.put(jObjtargetsource);

           /* if (FilterInstance.getInstance().getNotification().equals("new lead")) {

            }*/
            if (LeadSortCustomDialogClass.leadsortbystatus.equalsIgnoreCase("posted_date_l_o")) {

                jObj.put("order_by", "leads.created_at");
                jObj.put("order_by_reverse", "true");
            } else if (LeadSortCustomDialogClass.leadsortbystatus.equalsIgnoreCase("posted_date_o_l")) {
                jObj.put("order_by", "leads.created_at");
                jObj.put("order_by_reverse", "false");
            } else if (LeadSortCustomDialogClass.leadsortbystatus.equalsIgnoreCase("folowup_date_l_o")) {

                jObj.put("order_by", "dispatched.followup_date");
                jObj.put("order_by_reverse", "true");


            } else if (LeadSortCustomDialogClass.leadsortbystatus.equalsIgnoreCase("folowup_date_o_l")) {

                jObj.put("order_by", "dispatched.followup_date");
                jObj.put("order_by_reverse", "false");

            } else {

                jObj.put("order_by", "leads.created_at");
                jObj.put("order_by_reverse", "true");
            }
            jObj.put("alias_fields", "leads.created_at as PostedDate,leads.created_at as LeadDate,leads.id as id,dispatched.id as dispatchid,leads.customer_name as Name,leads.customer_mobile as Mobile,leads.customer_email as Email,leads.primary_make,leads.primary_model,leads.primary_variant,leads.secondary_make,leads.secondary_model,leads.secondary_variant,leads.register_no as regno,leads.register_month as regmonth,leads.register_year as regyear,leads.color,leads.kms,mfg_year,leads.owner as owners,leads.register_city,leads.stock_listed_price as price,dispatched.status,dispatched.followup_comments as remark,dispatched.followup_date as FollowUpDate,dispatched.employee_name as ExecutiveName");

            if (whichLeads.equals("followleads")) {
                Date d = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(d);
                jObj10.put("column", "dispatched.followup_date");
                jObj10.put("operator", "<=");
                jObj10.put("value", formattedDate + " 23:59:59");
                jsonArray.put(jObj10);
            }

            if (!searchVal.equals("")) {
                jObjmake.put("column", "leads.primary_make");
                jObjmake.put("operator", "like");
                jObjmake.put("value", "%" + searchVal.trim() + "%");
                jsonSearchVal.put(jObjmake);

                jObjmodel.put("column", "leads.primary_model");
                jObjmodel.put("operator", "like");
                jObjmodel.put("value", "%" + searchVal.trim() + "%");
                jsonSearchVal.put(jObjmodel);

                jObjvariant.put("column", "leads.primary_variant");
                jObjvariant.put("operator", "like");
                jObjvariant.put("value", "%" + searchVal.trim() + "%");
                jsonSearchVal.put(jObjvariant);

                jObjname.put("column", "leads.customer_name");
                jObjname.put("operator", "like");
                jObjname.put("value", "%" + searchVal.trim() + "%");
                jsonSearchVal.put(jObjname);

                jObjmobile.put("column", "leads.customer_mobile");
                jObjmobile.put("operator", "like");
                jObjmobile.put("value", "%" + searchVal.trim() + "%");
                jsonSearchVal.put(jObjmobile);
            }

            jObj.put("where_or", jsonSearchVal);

            jObj.put("custom_where", jsonArray);

            jObj.put("wherenotin", jsonArray_wherein);
            jObj.put("report_prefix", "Follow up Sales Lead Report");
            jObj.put("access_token", CommonMethods.getstringvaluefromkey(activity, "access_token"));

            jObjLeadStatus.put("column", "dispatched.status");
            jObjLeadStatus.put("value", LeadFilterFragment.leadstatusJsonArray);
            jsonArrayLeadStatus.put(jObjLeadStatus);

            if (LeadFilterFragment.leadstatusJsonArray.length() != 0) {
                jObj.put("where_in", jsonArrayLeadStatus);
            }

            jObj.put("tag", "android");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("LeadFilterpost Request", jObj.toString());
        return jObj;
    }

    public static void SortByLeads() {

        if (myJobList != null) {
            myJobList.clear();
        }

        counts = 1;
        negotiationLeadCount = 0;
        pageItem = 100;
        page_no = "1";

        WebServicesCall.webCall(activity, activity, jsonMake1(), "LeadStore", GlobalText.POST);

    }

    public static void onSerarchResultUpdate(String query) {
        mNormalLeadsAdapter.filter(query);
        searchVal = query;
        if (searchVal.equals("")) {
            CommonMethods.setvalueAgainstKey(activity, "loadWeb", "false");
            SearchingByWebLeads();
        }

        if (myJobList.size() <= 2) {
            if (oneTimeSearch == true) {
                oneTimeSearch = false;
                CommonMethods.setvalueAgainstKey(activity, "loadWeb", "false");
                SearchingByWebLeads();
            }
        }

    }

    public class WrapContentLinearLayoutManager extends LinearLayoutManager {
        public WrapContentLinearLayoutManager(Activity activity, int horizontal, boolean b) {
            super(activity);
        }

        //... constructor
        @Override
        public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
            try {
                super.onLayoutChildren(recycler, state);
            } catch (IndexOutOfBoundsException e) {

            }
        }
    }


    public static void SearchingByWebLeads() {

        if (myJobList != null) {
            myJobList.clear();
        }
        counts = 1;
        negotiationLeadCount = 0;
        if (!searchVal.equals("")) {
            pageItem = pageItem + 10;
        } else {
            pageItem = 100;
        }
        page_no = "1";

        WebServicesCall.webCall(activity, activity, jsonMake1(), "LeadStore", GlobalText.POST);

    }

    private void prePareWebLeadRequest() {
        mLeadRequest = new WebLeadRequest();
        List<CustomWhere> mList = new ArrayList<>();

        ArrayList<String> arrayListstatus = new ArrayList<String>();
        JSONObject jObj = new JSONObject();
        JSONObject jObj1 = new JSONObject();
        JSONObject jObj2 = new JSONObject();
        JSONObject jObj3 = new JSONObject();

        JSONObject jObj4 = new JSONObject();
        JSONObject jObj5 = new JSONObject();

        JSONObject jObj6 = new JSONObject();
        JSONObject jObj7 = new JSONObject();

        JSONObject jObj8 = new JSONObject();
        JSONObject jObj9 = new JSONObject();

        JSONObject jObj10 = new JSONObject();

        JSONObject jObj11 = new JSONObject();
        JSONObject jObj12 = new JSONObject();

        JSONObject jObjmake = new JSONObject();
        JSONObject jObjmodel = new JSONObject();
        JSONObject jObjvariant = new JSONObject();
        JSONObject jObjname = new JSONObject();
        JSONObject jObjmobile = new JSONObject();

        JSONObject jObjdealerid = new JSONObject();
        JSONObject jObjtargetsource = new JSONObject();

        JSONObject jObjLeadStatus = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONArray jsonArray_wherein = new JSONArray();
        JSONArray jsonArrayLeadStatus = new JSONArray();

        JSONArray jsonSearchVal = new JSONArray();

        if (!searchVal.equalsIgnoreCase("")) {
            swipeRefreshLayoutWebLeads.setEnabled(false);
        }


        try {

            if (!LeadFilterFragment.postFromDate.equals("") && !LeadFilterFragment.postToDate.equals("")) {
                CustomWhere createdat = new CustomWhere();
                createdat.setColumn("dispatched.created_at");
                createdat.setOperator(">=");
                createdat.setValue(LeadFilterFragment.postFromDate + " 00:00:00");
                mList.add(createdat);
                CustomWhere createdat1 = new CustomWhere();
                createdat1.setColumn("dispatched.created_at");
                createdat1.setOperator("<=");
                createdat1.setValue(LeadFilterFragment.postToDate + " 23:59:59");
                mList.add(createdat1);
            }

            if (!LeadFilterFragment.followFromDate.equals("") && !LeadFilterFragment.followToDate.equals("")) {

                CustomWhere followupdate = new CustomWhere();
                followupdate.setColumn("dispatched.followup_date");
                followupdate.setOperator(">=");
                followupdate.setValue(LeadFilterFragment.followFromDate + " 00:00:00");
                mList.add(followupdate);
                CustomWhere followupdate1 = new CustomWhere();
                followupdate1.setColumn("dispatched.followup_date");
                followupdate1.setOperator("<=");
                followupdate1.setValue(LeadFilterFragment.followToDate + " 23:59:59");
                mList.add(followupdate1);
                NotificationInstance.getInstance().setTodayfollowdate("");
            }

            if (!followTodayDate.equals("") && !followTomorowdate.equals("")) {


                CustomWhere dispatchedfollowup_date = new CustomWhere();
                dispatchedfollowup_date.setColumn("dispatched.followup_date");
                dispatchedfollowup_date.setOperator(">=");
                dispatchedfollowup_date.setValue(LeadFilterFragment.followTodayDate + " 00:00:00");
                mList.add(dispatchedfollowup_date);


                CustomWhere dispatchedfollowup_date1 = new CustomWhere();
                dispatchedfollowup_date1.setColumn("dispatched.followup_date");
                dispatchedfollowup_date1.setOperator("<=");
                dispatchedfollowup_date1.setValue(LeadFilterFragment.followTodayDate + " 23:59:59");

                mList.add(dispatchedfollowup_date1);

            }

            if (!LeadFilterFragment.postTodYesLastFromDate.equals("") && !LeadFilterFragment.postTodYesLastToDate.equals("")) {


                CustomWhere dispatchedcreated_at = new CustomWhere();
                dispatchedcreated_at.setColumn("dispatched.created_at");
                dispatchedcreated_at.setOperator(">=");
                dispatchedcreated_at.setValue(LeadFilterFragment.postTodYesLastFromDate + " 00:00:00");
                mList.add(dispatchedcreated_at);


                CustomWhere dispatchedcreated_at1 = new CustomWhere();
                dispatchedcreated_at1.setColumn("dispatched.created_at");
                dispatchedcreated_at1.setOperator("<=");


                if (!mInstance.getPtoday().equals("") ||
                        !mInstance.getPyesterday().equals("")) {
                    dispatchedcreated_at1.setValue(LeadFilterFragment.postTodYesLastFromDate + " 23:59:59");

                } else {
                    dispatchedcreated_at1.setValue(LeadFilterFragment.postTodYesLastToDate + " 23:59:59");

                }
                mList.add(dispatchedcreated_at1);


            }
            if (FilterInstance.getInstance().getNotification().equals("new lead")) {
                LeadFilterFragment.followTodYesLastFromDate = "";
                LeadFilterFragment.followTodYesLastToDate = "";
            }
            if (!LeadFilterFragment.followTodYesLastFromDate.equals("") && !LeadFilterFragment.followTodYesLastToDate.equals("")) {

                CustomWhere dispatchedfollowup_date = new CustomWhere();
                dispatchedfollowup_date.setColumn("dispatched.followup_date");
                dispatchedfollowup_date.setOperator(">=");
                dispatchedfollowup_date.setValue(LeadFilterFragment.followTodYesLastFromDate + " 00:00:00");
                mList.add(dispatchedfollowup_date);


                CustomWhere dispatchedfollowup_date1 = new CustomWhere();
                dispatchedfollowup_date1.setColumn("dispatched.followup_date");
                dispatchedfollowup_date1.setOperator("<=");


                if (!mInstance.getFtoday().equals("") ||
                        !mInstance.getFyesterday().equals("")) {
                    dispatchedfollowup_date1.setValue(LeadFilterFragment.followTodYesLastFromDate + " 23:59:59");

                } else {
                    dispatchedfollowup_date1.setValue(LeadFilterFragment.followTodYesLastToDate + " 23:59:59");

                }
                mList.add(dispatchedfollowup_date1);
                NotificationInstance.getInstance().setTodayfollowdate("");
            }

            if (!NotificationInstance.getInstance().getTodayfollowdate().equals("")) {
                String[] splitnotDate = NotificationInstance.getInstance().getTodayfollowdate().split("@", 10);

                CustomWhere dispatchedfollowup_date = new CustomWhere();
                dispatchedfollowup_date.setColumn("dispatched.followup_date");
                dispatchedfollowup_date.setOperator(">=");
                dispatchedfollowup_date.setValue(splitnotDate[0] + " 00:00:00");
                mList.add(dispatchedfollowup_date);


                CustomWhere dispatchedfollowup_date1 = new CustomWhere();
                dispatchedfollowup_date1.setColumn("dispatched.followup_date");
                dispatchedfollowup_date1.setOperator("<=");
                dispatchedfollowup_date1.setValue(splitnotDate[0] + " 23:59:59");
                mList.add(dispatchedfollowup_date1);


            }

            mLeadRequest.setFilterByFields("");
            mLeadRequest.setPerPage(String.valueOf(pageItem));
            mLeadRequest.setPage(page_no);


            //dealer id

            CustomWhere dispatchedtarget_id = new CustomWhere();
            dispatchedtarget_id.setColumn("dispatched.target_id");
            dispatchedtarget_id.setOperator("=");
            dispatchedtarget_id.setValue(CommonMethods.getstringvaluefromkey(activity, "dealer_code"));
            mList.add(dispatchedtarget_id);

//target source
            CustomWhere dispatchedtarget_source = new CustomWhere();
            dispatchedtarget_source.setColumn("dispatched.target_source");
            dispatchedtarget_source.setOperator("=");
            dispatchedtarget_source.setValue("MFC");
            mList.add(dispatchedtarget_source);


           /* if (FilterInstance.getInstance().getNotification().equals("new lead")) {

            }*/
            if (LeadSortCustomDialogClass.leadsortbystatus.equalsIgnoreCase("posted_date_l_o")) {


                mLeadRequest.setOrderBy("leads.created_at");
                mLeadRequest.setOrderByReverse("true");

            } else if (LeadSortCustomDialogClass.leadsortbystatus.equalsIgnoreCase("posted_date_o_l")) {
                mLeadRequest.setOrderBy("leads.created_at");
                mLeadRequest.setOrderByReverse("false");
            } else if (LeadSortCustomDialogClass.leadsortbystatus.equalsIgnoreCase("folowup_date_l_o")) {
                mLeadRequest.setOrderBy("dispatched.followup_date");
                mLeadRequest.setOrderByReverse("true");


            } else if (LeadSortCustomDialogClass.leadsortbystatus.equalsIgnoreCase("folowup_date_o_l")) {

                mLeadRequest.setOrderBy("dispatched.followup_date");
                mLeadRequest.setOrderByReverse("false");

            } else {

                mLeadRequest.setOrderBy("leads.created_at");
                mLeadRequest.setOrderByReverse("true");

            }
            mLeadRequest.setAliasFields("leads.created_at as PostedDate,leads.created_at as LeadDate,leads.id as id,dispatched.id as dispatchid,leads.customer_name as Name,leads.customer_mobile as Mobile,leads.customer_email as Email,leads.primary_make,leads.primary_model,leads.primary_variant,leads.secondary_make,leads.secondary_model,leads.secondary_variant,leads.register_no as regno,leads.register_month as regmonth,leads.register_year as regyear,leads.color,leads.kms,mfg_year,leads.owner as owners,leads.register_city,leads.stock_listed_price as price,dispatched.status,dispatched.followup_comments as remark,dispatched.followup_date as FollowUpDate,dispatched.employee_name as ExecutiveName");
            if (whichLeads.equals("followleads")) {
                Date d = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(d);
                jObj10.put("column", "dispatched.followup_date");
                jObj10.put("operator", "<=");
                jObj10.put("value", formattedDate + " 23:59:59");
                jsonArray.put(jObj10);
            }

            if (!searchVal.equals("")) {
                jObjmake.put("column", "leads.primary_make");
                jObjmake.put("operator", "like");
                jObjmake.put("value", "%" + searchVal.trim() + "%");
                jsonSearchVal.put(jObjmake);

                jObjmodel.put("column", "leads.primary_model");
                jObjmodel.put("operator", "like");
                jObjmodel.put("value", "%" + searchVal.trim() + "%");
                jsonSearchVal.put(jObjmodel);

                jObjvariant.put("column", "leads.primary_variant");
                jObjvariant.put("operator", "like");
                jObjvariant.put("value", "%" + searchVal.trim() + "%");
                jsonSearchVal.put(jObjvariant);

                jObjname.put("column", "leads.customer_name");
                jObjname.put("operator", "like");
                jObjname.put("value", "%" + searchVal.trim() + "%");
                jsonSearchVal.put(jObjname);

                jObjmobile.put("column", "leads.customer_mobile");
                jObjmobile.put("operator", "like");
                jObjmobile.put("value", "%" + searchVal.trim() + "%");
                jsonSearchVal.put(jObjmobile);
            }

            jObj.put("where_or", jsonSearchVal);

            jObj.put("custom_where", jsonArray);

            jObj.put("wherenotin", jsonArray_wherein);
            jObj.put("report_prefix", "Follow up Sales Lead Report");
            jObj.put("access_token", CommonMethods.getstringvaluefromkey(activity, "access_token"));

            jObjLeadStatus.put("column", "dispatched.status");
            jObjLeadStatus.put("value", LeadFilterFragment.leadstatusJsonArray);
            jsonArrayLeadStatus.put(jObjLeadStatus);

            if (LeadFilterFragment.leadstatusJsonArray.length() != 0) {
                jObj.put("where_in", jsonArrayLeadStatus);
            }

            jObj.put("tag", "android");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("LeadFilterpost Request", jObj.toString());

    }

}


