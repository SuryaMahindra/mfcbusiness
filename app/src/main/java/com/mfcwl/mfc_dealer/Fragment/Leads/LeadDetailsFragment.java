package com.mfcwl.mfc_dealer.Fragment.Leads;

import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mfcwl.mfc_dealer.Activity.Leads.LeadDetailsActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Fragment.ToolFragment;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import static com.mfcwl.mfc_dealer.Fragment.Leads.NormalLeadsDetailsFragment.lost;

public class LeadDetailsFragment extends Fragment {

    String Lname = "", Lmobile = "", Lemail = "", Lstatus = "", Lposteddate = "", Lmake = "", Lmodel = "", Lprice = "", Lregno = "", Lfollupdate = "", Lexename = "";

    String Lregcity = "", Lowner = "", Lregyear = "", Lcolor = "", Lkms = "", Ldid = "", Leadid = "", Ltype = "", remarks = "", Ldays = "", Lvariant = "", Lmfyear = "";

    public static ViewPager viewPager;
    String TAG = getClass().getSimpleName();
    public LeadDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Lname = getArguments().getString("paramsname");
            Lmobile = getArguments().getString("paramsmn");
            Lemail = getArguments().getString("paramsem");
            Lstatus = getArguments().getString("paramsstat");
            Lposteddate = getArguments().getString("paramspd");
            Lmake = getArguments().getString("paramsmk");
            Lmodel = getArguments().getString("paramsmd");
            Lprice = getArguments().getString("paramsprice");
            Lregno = getArguments().getString("paramsrn");

            Lfollupdate = getArguments().getString("paramsfollup");
            Lexename = getArguments().getString("paramsexenm");

            Lregcity = getArguments().getString("paramsrgcity");
            Lowner = getArguments().getString("paramsowner");
            // Log.e("Lownerd ", "Lowner " + Lowner);
            Lregyear = getArguments().getString("paramsrgyr");
            Lcolor = getArguments().getString("paramscolor");
            Lkms = getArguments().getString("paramskms");
            Ldid = getArguments().getString("paramsdid");
            Ldays = getArguments().getString("paramsldays");
            Lvariant = getArguments().getString("paramslvar");
            if (CommonMethods.getstringvaluefromkey(getActivity(), "WLEADS").equals("PrivateLeads")) {
                Leadid = getArguments().getString("paramsleadid");
                Ltype = getArguments().getString("paramsleadtype");
                remarks = getArguments().getString("paramsremark");
            } else {
                Lmfyear = getArguments().getString("paramsmfy");
                remarks = getArguments().getString("paramsremark");
            }

        } else {
            // Log.e("getArguments ", "getArguments null ");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.lead_details_fragment, container, false);
        TabLayout tabLayout = view.findViewById(R.id.leads_details_tabs);

        viewPager = view.findViewById(R.id.leads_details_viewpager);
        viewPager.setAdapter(new LeadsPagerAdapter(getFragmentManager(), tabLayout.getTabCount()));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(Color.parseColor("#aaaaaa"), Color.parseColor("#2d2d2d"));
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#e5e5e5"));

        /*tabLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("tabLayout ","tabLayout ");
            }
        });*/

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position == 0) {
                    if (Lstatus.equalsIgnoreCase("Lost") || Lstatus.equalsIgnoreCase("Sold") || Lstatus.equalsIgnoreCase("Close") || Lstatus.equalsIgnoreCase("Closed") || Lstatus.equalsIgnoreCase("Finalised") || Lstatus.equalsIgnoreCase("Not-Interested")) {
                        LeadDetailsActivity.Invisible();
                    } else {
                        LeadDetailsActivity.Visible();
                    }
                    if (lost == true) {
                        LeadDetailsActivity.Invisible();
                    }
                } else {
                    LeadDetailsActivity.Invisible();
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return view;
    }

    public class LeadsPagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        // tab titles
        private String[] tabTitles = new String[]{"About Lead", "Follow-up History"};

        // overriding getPageTitle()
        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }

        public LeadsPagerAdapter(FragmentManager fm, int mNumOfTabs) {
            super(fm);
            this.mNumOfTabs = mNumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                   /* Bundle bundle = new Bundle();
                    bundle.putString("paramsname",name);*/
                    if (CommonMethods.getstringvaluefromkey(getActivity(), "WLEADS").equals("PrivateLeads")) {
                        NormalLeadsDetailsFragment fragment = new NormalLeadsDetailsFragment(Lname, Lmobile, Lemail, Lstatus, Lposteddate, Lmake, Lmodel, Lprice, Lregno, Lfollupdate, Lexename, Lregcity, Lowner, Lregyear, Lcolor, Lkms, Ldid, remarks, Ldays, Lvariant);
                        return fragment;

                    } else {
                        NormalLeadsDetailsFragment fragment = new NormalLeadsDetailsFragment(Lname, Lmobile, Lemail, Lstatus, Lposteddate, Lmake, Lmodel, Lprice, Lregno, Lfollupdate, Lexename, Lregcity, Lowner, Lregyear, Lcolor, Lkms, Ldid, remarks, Ldays, Lvariant, Lmfyear);
                        return fragment;
                    }

                case 1:

                    if (CommonMethods.getstringvaluefromkey(getActivity(), "WLEADS").equals("PrivateLeads")) {
                        FollowupHistoryFragment fragment1 = new FollowupHistoryFragment(Leadid, Ltype);
                        return fragment1;
                    } else {
                        FollowupHistoryFragment fragment1 = new FollowupHistoryFragment(Ldid);
                        return fragment1;
                    }


                case 2:
                    return new ToolFragment();

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return tabTitles.length;
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        Application.getInstance().trackScreenView(getActivity(),GlobalText.LeadDetails_Fragment);

    }
}
