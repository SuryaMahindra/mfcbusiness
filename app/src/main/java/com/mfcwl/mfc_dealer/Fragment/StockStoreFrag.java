package com.mfcwl.mfc_dealer.Fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.ASMModel.stockWhere;
import com.mfcwl.mfc_dealer.Activity.Leads.FilterInstance;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Activity.SplashActivity;
import com.mfcwl.mfc_dealer.Activity.StockFilterActivity;
import com.mfcwl.mfc_dealer.Adapter.StockStoreAdapter;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.InstanceCreate.Stocklisting;
import com.mfcwl.mfc_dealer.Model.StockModel;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.RequestModel.WhereinASM;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.StockModels.StockData;
import com.mfcwl.mfc_dealer.StockModels.StockImages;
import com.mfcwl.mfc_dealer.StockModels.StockRequest;
import com.mfcwl.mfc_dealer.StockModels.StockResponse;
import com.mfcwl.mfc_dealer.StockModels.Where;
import com.mfcwl.mfc_dealer.StockServices.StockService;
import com.mfcwl.mfc_dealer.Utility.AppConstants;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GAConstants;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import rdm.NotificationActivity;
import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.activity;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchImage;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchVal;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchicon;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toggle;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toolbar;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.tv_header_title;
import static com.mfcwl.mfc_dealer.Activity.StockFilterActivity.stock_photocountsFlag;
import static com.mfcwl.mfc_dealer.Activity.StockFilterActivity.yearValues;
import static com.mfcwl.mfc_dealer.Adapter.StockStoreAdapter.progress;
import static com.mfcwl.mfc_dealer.Fragment.StockFragment.headerdata;
import static com.mfcwl.mfc_dealer.Fragment.StockFragment.stock_filter_icon;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ASM;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_STATEHEAD;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ZONALHEAD;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_DEALER_CRE;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_PROCUREMENT;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_SALES;

public class StockStoreFrag extends Fragment {


    public static RecyclerView stockStoreRecy;
    public static StockStoreAdapter mStockStoreAdapter;

    public static Activity activity;
    static Context context;

    public static List<StockModel> stock = new ArrayList<StockModel>();

    public static ArrayList<String> imageurls;
    public static ArrayList<String> imagename;
    public static int counts = 1;
    public static int negotiationLeadCount = 0;
    public static int pageItem = 100;
    public static String page_no = "1";
    public static SwipeRefreshLayout swipeRefreshLayout;

    public static Object[] objectList = null;
    public static String[] imageurlss;

    public static Object[] objectList2 = null;
    public static String[] imagenames;

    public static TextView nostock;
    public boolean Flag1 = false;

    private static StockRequest mStockRequest;
    private static Where mWhere;

    public static stockWhere where;

    public static String TAG = StockStoreFrag.class.getSimpleName();


    public StockStoreFrag() {

        setHasOptionsMenu(true);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        //menu.clear();

        String uType = CommonMethods.getstringvaluefromkey(activity, "user_type");

       MenuItem asm_mail = menu.findItem(R.id.asm_mail);
        searchImage.setImageResource(R.drawable.search);
        //new ASM
        if (uType.equalsIgnoreCase("dealer")) {
            asm_mail.setVisible(false);
        } else if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_DEALER_CRE) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_SALES) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_PROCUREMENT)) {
            asm_mail.setVisible(false);
        } else {
            asm_mail.setVisible(true);
        }


        MenuItem asmHome = menu.findItem(R.id.asmHome);
        MenuItem add_image = menu.findItem(R.id.add_image);
        MenuItem share = menu.findItem(R.id.share);
        MenuItem delete_fea = menu.findItem(R.id.delete_fea);
        MenuItem stock_fil_clear = menu.findItem(R.id.stock_fil_clear);
        MenuItem notification_bell = menu.findItem(R.id.notification_bell);
        MenuItem notification_cancel = menu.findItem(R.id.notification_cancel);

        asmHome.setVisible(false);
        add_image.setVisible(false);
        share.setVisible(false);
        delete_fea.setVisible(false);
        stock_fil_clear.setVisible(false);
        notification_bell.setVisible(false);
        notification_cancel.setVisible(false);



        asm_mail.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                //  Toast.makeText(getActivity(),"store stocks sending email...",Toast.LENGTH_LONG).show();


                if (CommonMethods.isInternetWorking(activity)) {


                    List<WhereinASM> wh = new ArrayList<>();

                    ArrayList<String> dealerCode = new ArrayList<String>();
                    dealerCode.add(CommonMethods.getstringvaluefromkey(activity, "dealer_code"));

                    WhereinASM code = new WhereinASM();
                    code.setColumn("dealer_code");
                    code.setValue(dealerCode);
                    code.setOperator("=");
                    wh.add(code);

                    WhereinASM email = new WhereinASM();

                    ArrayList<String> emailArray = new ArrayList<String>();
                    emailArray.add("true");

                    email.setColumn("isEmail");
                    email.setValue(emailArray);
                    email.setOperator("=");


                    String isEmail = "";
                    for (int i = 0; i < wh.size(); i++) {

                        if (wh.get(i).getColumn().equalsIgnoreCase("isEmail")) {
                            isEmail = "isEmail";
                        }
                    }

                    if (isEmail.equalsIgnoreCase("")) {
                        wh.add(email);
                        mStockRequest.setWherenotin(wh);
                    }


                    // custom dialog
                    Dialog dialog = new Dialog(activity);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setGravity(Gravity.TOP | Gravity.RIGHT);
                    dialog.setContentView(R.layout.common_cus_dialog);
                    TextView Message, Message2;
                    Button cancel, Confirm;
                    Message = dialog.findViewById(R.id.Message);
                    Message2 = dialog.findViewById(R.id.Message2);
                    cancel = dialog.findViewById(R.id.cancel);
                    Confirm = dialog.findViewById(R.id.Confirm);
                    Message.setText(GlobalText.are_you_sure_to_send_mail);
                    // Message2.setText("Go ahead and Submit ?");
                    Confirm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            //  if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("StockStore")) {
                            sendEmailDeatils(context, mStockRequest);
                            //   }

                            dialog.dismiss();
                        }
                    });
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();


                } else {
                    CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);

                }

                return true;
            }
        });


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.stock_store_fra, container, false);
        setHasOptionsMenu(true);

        if(tv_header_title!=null)
        {
            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText("Stocks");
            tv_header_title.setTextColor(getResources().getColor(R.color.white));
        }
        if(CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(USERTYPE_ASM))
        {
            searchicon.setVisibility(View.VISIBLE);
            searchImage.setVisibility(View.VISIBLE);
            toggle.setDrawerIndicatorEnabled(false);
            toggle.setHomeAsUpIndicator(R.drawable.ic_new_hamburger_menu);
        }

        try {

            stockStoreRecy = view.findViewById(R.id.stock_store_recycler);
            nostock = view.findViewById(R.id.nostock);
            context = getContext();
            activity = getActivity();
            imageurls = new ArrayList<String>();
            imagename = new ArrayList<String>();
            Calendar cal = Calendar.getInstance();
            searchicon.setVisibility(View.VISIBLE);

            imageurlss = new String[]{};
            imagenames = new String[]{};

            //ASM //already filter data clear
            if (CommonMethods.getstringvaluefromkey(activity, "stockFilterClear").equalsIgnoreCase("changes")) {
                stockFilterClear();
                CommonMethods.setvalueAgainstKey(activity, "stockFilterClear", "no");
            }


            // Subtract 30 days from the calendar
            cal.add(Calendar.DATE, -30);

            String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

            MainActivity.searchClose();
            MainActivity.test(activity.getResources().getString(R.string.stock_hint));

            if (nostock.getVisibility() == View.VISIBLE) {
                nostock.setVisibility(View.GONE);
            }
            page_no = "1";
            counts = 1;
            CommonMethods.setvalueAgainstKey(activity, "stockstore_load", "false");

            Log.e("booknowbtn", "booknowbtn==" + CommonMethods.getstringvaluefromkey(activity, "booknowbtn").toString());
            Log.e("bookfilter", "bookfilter=" + CommonMethods.getstringvaluefromkey(activity, "bookfilter").toString());
            Log.e("stockfilterOnclick", "stockfilterOnclick=" + CommonMethods.getstringvaluefromkey(activity, "stockfilterOnclick").toString());

            Log.i("Notification_Switch", "Notification_Switch:==1 " + CommonMethods.getstringvaluefromkey(activity, "Notification_Switch"));
            Log.i("photocounts", "photocounts:==1 " + CommonMethods.getstringvaluefromkey(activity, "photocounts"));

            Log.e("store coming...", "store coming...1");

            if (CommonMethods.getstringvaluefromkey(activity, "booknowbtn").equalsIgnoreCase("true")) {
                // Log.e("store server-in...","store coming...1");
            } else if (CommonMethods.getstringvaluefromkey(activity, "bookfilter").equalsIgnoreCase("true")) {
                //Log.e("store empty-in...","store coming...1");
            } else if (Stocklisting.getInstance().getStockstatus().equals("BookedStockFrag")) {
                //Log.e("store empty-in...","store coming...1");
            } else {


                if (CommonMethods.getstringvaluefromkey(activity, "stockfilterOnclick").equalsIgnoreCase("true")) {
                    CommonMethods.setvalueAgainstKey(activity, "stockfilterOnclick", "false");
                    if (CommonMethods.isInternetWorking(activity)) {
                        prepareStockRequestModel();
                    } else {
                        CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
                    }
                } else {
                    //  if (stock.size() <= 0) {
                    if (stock.isEmpty()) {

                        if (CommonMethods.isInternetWorking(activity)) {
                            prepareStockRequestModel();
                        } else {
                            CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
                        }

                    } else {
                        reloadData();
                    }
                }
            }

            swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);

            swipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.CYAN);

            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {

                    if (CommonMethods.isInternetWorking(activity)) {
                        swipeRefresh_s_applyapply();
                    } else {
                        swipeRefreshLayout.setEnabled(false);
                        CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
                    }
                }
            });

            // We need to detect scrolling changes in the RecyclerView
            /*stockStoreRecy.setOnScrollListener(new RecyclerView.OnScrollListener() {
                // Keeps track of the overall vertical offset in the list
                int verticalOffset;
                // Determines the scroll UP/DOWN direction
                boolean scrollingUp;

                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    *//*if (scrollingUp) {

                       // Log.e("scrollingUp","scrollingUp="+scrollingUp);
                        if (verticalOffset > headerdata.getHeight()) {
                            toolbarAnimateHide();
                        } else {
                            toolbarAnimateShow(verticalOffset);
                        }

                        if (verticalOffset > headermove.getHeight()) {
                            toolbarAnimateHide2();
                        } else {
                            toolbarAnimateShow2(verticalOffset);
                        }
                        spaceview.setVisibility(View.GONE);


                    } else {
                        //Log.e("scrollingDown","scrollingDown="+scrollingUp);
                        if (headerdata.getTranslationY() < headerdata.getHeight() * -0.6 && verticalOffset > headerdata.getHeight()) {
                            toolbarAnimateHide();

                        } else {
                            toolbarAnimateShow(verticalOffset); }

                        if (headermove.getTranslationY() < headermove.getHeight() * -0.6 && verticalOffset > headermove.getHeight()) {
                            toolbarAnimateHide2();
                        } else {
                            toolbarAnimateShow2(verticalOffset);
                        }
                    }*//*
                    }
                }


                @Override
                public final void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    verticalOffset += dy;
                    scrollingUp = dy > 0;
              *//*  int toolbarYOffset = (int) (dy - headerdata.getTranslationY());
                int toolbarYOffset2 = (int) (dy - headermove.getTranslationY());*//*
             *//* headerdata.animate().cancel();
                headermove.animate().cancel();*//*
                    if (scrollingUp) {
                   *//*if (toolbarYOffset < headerdata.getHeight()) {

                        headerdata.setTranslationY(-toolbarYOffset);
                       spaceview.setVisibility(View.GONE);

                   } else {
                        headerdata.setTranslationY(-headerdata.getHeight());

                    }

                    if (toolbarYOffset2 < headermove.getHeight()) {
                        headermove.setTranslationY(-toolbarYOffset2);
                    } else {
                        headermove.setTranslationY(-headermove.getHeight());

                    }
*//*

                        if (Flag1) {
                            MainActivity.bottom_topAnimation();
                            Flag1 = false;
                        }

                    } else {

                        if (!Flag1) {
                            MainActivity.top_bottomAnimation();
                            Flag1 = true;
                        }

                *//* if (toolbarYOffset < 0) {
                        headerdata.setTranslationY(0);
                     spaceview.setVisibility(View.VISIBLE);

                 } else {
                        headerdata.setTranslationY(-toolbarYOffset);
                    }

                    if (toolbarYOffset2 < 0) {
                        headermove.setTranslationY(0);
                    } else {
                        headermove.setTranslationY(-toolbarYOffset2);
                    }
*//*

                    }
                }
            });*/

            //R3 Preprod build bug fix added by Sangeetha

            stockStoreRecy.setOnScrollListener(new RecyclerView.OnScrollListener() {
                // Keeps track of the overall vertical offset in the list
                int verticalOffset;

                // Determines the scroll UP/DOWN direction
                boolean scrollingUp;
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    verticalOffset += dy;
                    scrollingUp = dy > 0;
                    int toolbarYOffset = (int) (dy - headerdata.getTranslationY());
                    int toolbarYOffset2 = (int) (dy - toolbar.getTranslationY());
                    headerdata.animate().cancel();
                    toolbar.animate().cancel();
                    if (scrollingUp) {
                   /* if (toolbarYOffset < headerdata.getHeight()) {
                        if (verticalOffset > headerdata.getHeight()) {
                            // toolbarSetElevation(TOOLBAR_ELEVATION);
                        }
                        ///headerdata.setTranslationY(-toolbarYOffset);
                        ///toolbar.setTranslationY(-toolbarYOffset2);
                    } else {
                        //toolbarSetElevation(0);
                        ////headerdata.setTranslationY(-toolbar.getHeight());
                        ////toolbar.setTranslationY(-toolbar.getHeight());
                    }
*/


                        if (Flag1) {

                            //  StockFragment.bottom_topAnimation();
                            MainActivity.bottom_topAnimation();

                            //Log.e("Flag1","scrollingUp-1="+Flag1);

                            Flag1 = false;

                        } else {

                        }
                        //Log.e("scrollingUp","scrollingUp-1="+scrollingUp);

                    } else {
                        //Log.e("scrollingDown","scrollingDown-1="+scrollingUp);


                        if (!Flag1) {

                            // Log.e("Flag1","scrollingDown-1="+Flag1);
                            //  StockFragment.top_bottomAnimation();
                            MainActivity.top_bottomAnimation();

                            Flag1 = true;

                        }
                    }
                }

            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        int margin = getResources().getDimensionPixelSize(R.dimen.recyclepadding);
        //new ASM
        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
        } else {
            ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) swipeRefreshLayout.getLayoutParams();
            mlp.setMargins(0, 0, 0, margin);
        }

        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        // put your code here...

        //new ASM
        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {

            if (tv_header_title != null) {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Stocks");
            }
            Application.getInstance().trackScreenView(getActivity(), GlobalText.StockStore_Fragment);

        }
        else if (
                CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(AppConstants.LOGINTYPE_DEALERCRE) ||
                CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(AppConstants.LOGINTYPE_PROCUREMENT) ||
                CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(AppConstants.LOGINTYPE_SLAES)||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ASM)||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ZONALHEAD) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_STATEHEAD))
        {
            if (tv_header_title != null) {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Stocks");
            }
        }
        else if(CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ASM))
        {
            toolbar.setBackgroundColor(getResources().getColor(R.color.black));
            toggle.setDrawerIndicatorEnabled(false);
            toggle.setHomeAsUpIndicator(R.drawable.ic_new_hamburger_menu);

            if (tv_header_title != null) {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Stock");
            }
        }else {

            if (tv_header_title != null) {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText(CommonMethods.getstringvaluefromkey(getActivity(), "dealerName"));
            }
            Application.getInstance().trackScreenView(getActivity(), GlobalText.asm_stock_listing);
            Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_STOCK_LISTING,CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));

        }



    }

    public static void swipeRefresh_s_applyapply() {

        page_no = "1";
        counts = 1;
        // Log.e("store sortby...", "store sortby...1");
        if (nostock.getVisibility() == View.VISIBLE) {
            nostock.setVisibility(View.GONE);
        }
        CommonMethods.setvalueAgainstKey(activity, "sortby", "true");
        SplashActivity.progress = false;
        CommonMethods.setvalueAgainstKey(activity, "stockstore_load", "false");
        // WebServicesCall.webCall(activity, activity, jsonMake1(), "StockStore", GlobalText.POST);

        if (CommonMethods.isInternetWorking(activity)) {
            prepareStockRequestModel();
        } else {
            CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
        }

    }

    public static void sortbyapply() {

        page_no = "1";
        counts = 1;
        //  Log.e("store sortby...", "store sortby...1");
        nostock.setVisibility(View.GONE);
        CommonMethods.setvalueAgainstKey(activity, "sortby", "true");
        SplashActivity.progress = true;
        CommonMethods.setvalueAgainstKey(activity, "stockstore_load", "false");
        // WebServicesCall.webCall(activity, activity, jsonMake1(), "StockStore", GlobalText.POST);

        if (CommonMethods.isInternetWorking(activity)) {
            prepareStockRequestModel();
        } else {
            CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
        }

    }


    public void toolbarAnimateShow(final int verticalOffset) {
        headerdata.animate()
                .translationY(0)
                .setInterpolator(new LinearInterpolator())
                .setDuration(180)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        // toolbarSetElevation(verticalOffset == 0 ? 0 : TOOLBAR_ELEVATION);
                    }
                });
    }

    private void toolbarAnimateHide() {
        headerdata.animate()
                .translationY(-headerdata.getHeight())
                .setInterpolator(new LinearInterpolator())
                .setDuration(180)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        // toolbarSetElevation(0);
                    }
                });

    }

    public static void loadmore(int count) {

        //   SplashActivity.progress = false;
        counts = counts + 1;

        double maxPageNumber = Math.ceil((double) (negotiationLeadCount) / (double) pageItem); // d = 3.0
        if (counts > maxPageNumber) {
            //  progress.setVisibility(View.GONE);
            return;
        }

        page_no = Integer.toString(counts);

       /* Log.e("maxPageNumber", "maxPageNumber=" + maxPageNumber);
        Log.e("page_no", "page_no=" + page_no);
        Log.e("counts", "counts=" + counts);*/

        CommonMethods.setvalueAgainstKey(activity, "stockstore_load", "true");
        CommonMethods.setvalueAgainstKey(activity, "bookedstore_load", "false");

        if (CommonMethods.isInternetWorking(activity)) {
            prepareStockRequestModel();
        } else {
            CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
        }

    }

    public static void onSerarchResultUpdate(String query) {
        searchVal = query;

        if (mStockStoreAdapter == null) {

        } else {
            mStockStoreAdapter.filter(query);
        }
        //System.out.println("Search Values>>>>>>>>>> "+query);

    }

    public static JSONObject jsonMake1() {
        JSONObject jObj = new JSONObject();
        JSONObject jObj1 = new JSONObject();
        JSONObject jObj2 = new JSONObject();
        JSONObject jObj3 = new JSONObject();
        JSONObject jObj4 = new JSONObject();
        JSONObject jObj5 = new JSONObject();
        JSONObject jObj6 = new JSONObject();
        JSONObject jObj7 = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        JSONObject jObjmake = new JSONObject();
        JSONObject jObjmodel = new JSONObject();
        JSONObject jObjvariant = new JSONObject();
        JSONObject jObjregno = new JSONObject();

        JSONArray jsonSearchVal = new JSONArray();

        if (!searchVal.equalsIgnoreCase("")) {

            if (swipeRefreshLayout != null) swipeRefreshLayout.setEnabled(false);
        }
        //searchClear();


        // Log.e("pageItem=","pageItem"+pageItem);

        try {
            jObj.put("Page", page_no);
            jObj.put("PageItems", Integer.toString(pageItem));

            if (CommonMethods.getstringvaluefromkey(activity, "sortbystatus").equalsIgnoreCase("pricelowhigh")) {
                jObj.put("OrderBy", "selling_price");
                jObj.put("OrderByReverse", "true");
            } else if (CommonMethods.getstringvaluefromkey(activity, "sortbystatus").equalsIgnoreCase("pricehighlow")) {
                jObj.put("OrderBy", "selling_price");
                jObj.put("OrderByReverse", "false");
            } else if (CommonMethods.getstringvaluefromkey(activity, "sortbystatus").equalsIgnoreCase("postoldtonew")) {
                jObj.put("OrderBy", "posted_date");
                jObj.put("OrderByReverse", "true");
            } else if (CommonMethods.getstringvaluefromkey(activity, "sortbystatus").equalsIgnoreCase("postnewtoold")) {
                jObj.put("OrderBy", "posted_date");
                jObj.put("OrderByReverse", "false");
            } else if (CommonMethods.getstringvaluefromkey(activity, "sortbystatus").equalsIgnoreCase("kmshightolow")) {
                jObj.put("OrderBy", "kilometer");
                jObj.put("OrderByReverse", "false");
            } else if (CommonMethods.getstringvaluefromkey(activity, "sortbystatus").equalsIgnoreCase("kmslowtohigh")) {
                jObj.put("OrderBy", "kilometer");
                jObj.put("OrderByReverse", "true");
            } else if (CommonMethods.getstringvaluefromkey(activity, "sortbystatus").equalsIgnoreCase("yearnewtoold")) {
                jObj.put("OrderBy", "manufacture_year");
                jObj.put("OrderByReverse", "false");
            } else if (CommonMethods.getstringvaluefromkey(activity, "sortbystatus").equalsIgnoreCase("yearoldtonew")) {
                jObj.put("OrderBy", "manufacture_year");
                jObj.put("OrderByReverse", "true");
            } else {
                jObj.put("OrderBy", "posted_date");
                jObj.put("OrderByReverse", "true");
            }


            if (CommonMethods.getstringvaluefromkey(activity, "filterapply").equalsIgnoreCase("true")) {

                //Log.e("====","====="+StockFilterActivity.startPriceValues.toString());
                //Log.e("====","====="+StockFilterActivity.endPriceValues.toString());

                jObj1.put("column", "selling_price");
                jObj1.put("operator", ">=");
                jObj1.put("value", StockFilterActivity.startPriceValues.toString());

                jObj2.put("column", "selling_price");
                jObj2.put("operator", "<=");
                jObj2.put("value", StockFilterActivity.endPriceValues.toString());

                jObj3.put("column", "kilometer");
                jObj3.put("operator", "<=");
                jObj3.put("value", StockFilterActivity.kilometerValues.toString());

                jObj4.put("column", "manufacture_year");
                jObj4.put("operator", ">=");
                jObj4.put("value", yearValues.toString());

                jsonArray.put(jObj1);
                jsonArray.put(jObj2);
                jsonArray.put(jObj3);
                jsonArray.put(jObj4);

                if (CommonMethods.getstringvaluefromkey(activity, "certifiedstatus").equalsIgnoreCase("certifiedcar")) {

                    jObj5.put("column", "is_certified");
                    jObj5.put("operator", "=");
                    jObj5.put("value", "true");
                    jsonArray.put(jObj5);
                }

                if (stock_photocountsFlag || CommonMethods.getstringvaluefromkey(activity, "Notification_Switch").equalsIgnoreCase("noStockImage")) {
                    jObj6.put("column", "photo_count");
                    jObj6.put("operator", "<=");
                    jObj6.put("value", "0");
                    jsonArray.put(jObj6);
                    //stock_photocountsFlag =false;
                    CommonMethods.setvalueAgainstKey(activity, "photocounts", "true");
                    CommonMethods.setvalueAgainstKey(activity, "Notification_Switch", "type");

                }

            }

            if (CommonMethods.getstringvaluefromkey(activity, "Notification_Switch").equalsIgnoreCase("noStockImage")
                    || CommonMethods.getstringvaluefromkey(activity, "photocounts").equalsIgnoreCase("true")) {
                jObj6.put("column", "photo_count");
                jObj6.put("operator", "<=");
                jObj6.put("value", "0");
                jsonArray.put(jObj6);
                //stock_photocountsFlag =false;

                CommonMethods.setvalueAgainstKey(activity, "Notification_Switch", "type");
                CommonMethods.setvalueAgainstKey(activity, "photocounts", "true");
                // stock_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);

            } else {
                CommonMethods.setvalueAgainstKey(activity, "photocounts", "false");

            }


            if (MainActivity.postdatelessthirty || CommonMethods.getstringvaluefromkey(activity, "thirtydaycheckbox").equalsIgnoreCase("true")) {

                jObj7.put("column", "posted_date");
                jObj7.put("operator", "<");
                jObj7.put("value", CalendarTodayDate());
                jsonArray.put(jObj7);

                CommonMethods.setvalueAgainstKey(activity, "thirtydaycheckbox", "true");

                MainActivity.postdatelessthirty = false;
                lastmonth = "";

                // MainActivity.postdatelessthirty = false;
                lastmonth = "";

            }

            if (!searchVal.equals("")) {
                if (progress.getVisibility() == View.VISIBLE)
                    progress.setVisibility(View.GONE);

                jObjmake.put("column", "vehicle_make");
                jObjmake.put("operator", "like");
                jObjmake.put("value", searchVal);
                jsonSearchVal.put(jObjmake);

                jObjmodel.put("column", "vehicle_model");
                jObjmodel.put("operator", "like");
                jObjmodel.put("value", searchVal);
                jsonSearchVal.put(jObjmodel);

                jObjvariant.put("column", "vehicle_variant");
                jObjvariant.put("operator", "like");
                jObjvariant.put("value", searchVal);
                jsonSearchVal.put(jObjvariant);

                jObjregno.put("column", "registration_number");
                jObjregno.put("operator", "like");
                jObjregno.put("value", searchVal);
                jsonSearchVal.put(jObjregno);
                jObj.put("where_or", jsonSearchVal);

            }

            jObj.put("where", jsonArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //    Log.e("jsonMake1 ", "jsonMake1" + jObj.toString());

        return jObj;
    }


    public static void reloadData() {
        mStockStoreAdapter = new StockStoreAdapter(activity, stock);
        stockStoreRecy.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(activity);
        stockStoreRecy.setLayoutManager(mLayoutManager);
        //stockStoreRecy.addItemDecoration(new DividerItemDecoration(activity, LinearLayoutManager.VERTICAL));
        stockStoreRecy.setItemAnimator(new DefaultItemAnimator());
        stockStoreRecy.setAdapter(mStockStoreAdapter);
        CommonMethods.setvalueAgainstKey(activity, "sortby", "false");
        CommonMethods.setvalueAgainstKey(activity, "status", "StockStore");

    }

    public static String lastmonth = "";

    public static String CalendarTodayDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -30);

        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        lastmonth = s.format(new Date(cal.getTimeInMillis()));
        return lastmonth;
    }

    // Retrofit
    private static void prepareStockRequestModel() {

        Log.i("prepareStockRequest", "prepareStockRequest:== store");

        Log.e("bNotification_Switch..", "Notification_Switcht==" + CommonMethods.getstringvaluefromkey(activity, "Notification_Switch"));


        mStockRequest = new StockRequest();
        mWhere = new Where();


        WhereinASM instock = new WhereinASM();

        List<WhereinASM> wh = new ArrayList<>();

        ArrayList<String> dealerCode = new ArrayList<String>();

        dealerCode.add(CommonMethods.getstringvaluefromkey(activity, "dealer_code"));

        List<Where> mWhereList = new ArrayList<>();

        instock.setColumn("dealer_code");
        instock.setValue(dealerCode);
        instock.setOperator("=");
        wh.add(instock);


        if (!searchVal.equalsIgnoreCase("")) {
            swipeRefreshLayout.setEnabled(false);
        }

        try {
            mStockRequest.setPage(page_no);
            mStockRequest.setPageItems(String.valueOf(pageItem));
            if (CommonMethods.getstringvaluefromkey(activity, "sortbystatus").equalsIgnoreCase("pricelowhigh")) {
                mStockRequest.setOrderBy("selling_price");
                mStockRequest.setOrderByReverse("true");

            } else if (CommonMethods.getstringvaluefromkey(activity, "sortbystatus").equalsIgnoreCase("pricehighlow")) {

                mStockRequest.setOrderBy("selling_price");
                mStockRequest.setOrderByReverse("false");
            } else if (CommonMethods.getstringvaluefromkey(activity, "sortbystatus").equalsIgnoreCase("postoldtonew")) {

                mStockRequest.setOrderBy("posted_date");
                mStockRequest.setOrderByReverse("true");
            } else if (CommonMethods.getstringvaluefromkey(activity, "sortbystatus").equalsIgnoreCase("postnewtoold")) {

                mStockRequest.setOrderBy("posted_date");
                mStockRequest.setOrderByReverse("false");
            } else if (CommonMethods.getstringvaluefromkey(activity, "sortbystatus").equalsIgnoreCase("kmshightolow")) {

                mStockRequest.setOrderBy("kilometer");
                mStockRequest.setOrderByReverse("false");
            } else if (CommonMethods.getstringvaluefromkey(activity, "sortbystatus").equalsIgnoreCase("kmslowtohigh")) {

                mStockRequest.setOrderBy("kilometer");
                mStockRequest.setOrderByReverse("true");
            } else if (CommonMethods.getstringvaluefromkey(activity, "sortbystatus").equalsIgnoreCase("yearnewtoold")) {

                mStockRequest.setOrderBy("manufacture_year");
                mStockRequest.setOrderByReverse("false");
            } else if (CommonMethods.getstringvaluefromkey(activity, "sortbystatus").equalsIgnoreCase("yearoldtonew")) {

                mStockRequest.setOrderBy("manufacture_year");
                mStockRequest.setOrderByReverse("true");
            } else {

                mStockRequest.setOrderBy("posted_date");
                mStockRequest.setOrderByReverse("true");
            }

            if (CommonMethods.getstringvaluefromkey(activity, "filterapply").equalsIgnoreCase("true")) {

                //Log.e("====","====="+StockFilterActivity.startPriceValues.toString());
                //Log.e("====","====="+StockFilterActivity.endPriceValues.toString());

                Where selling_price = new Where();
                selling_price.setColumn("selling_price");
                selling_price.setOperator(">=");
                selling_price.setValue(StockFilterActivity.startPriceValues.toString());
                mWhereList.add(selling_price);

                Where selling_price1 = new Where();
                selling_price1.setColumn("selling_price");
                selling_price1.setOperator("<=");
                selling_price1.setValue(StockFilterActivity.endPriceValues.toString());
                mWhereList.add(selling_price1);

                Where kilometer = new Where();
                kilometer.setColumn("kilometer");
                kilometer.setOperator("<=");
                kilometer.setValue(StockFilterActivity.kilometerValues.toString());
                mWhereList.add(kilometer);

                Where manufacture_year = new Where();
                manufacture_year.setColumn("manufacture_year");
                manufacture_year.setOperator(">=");
                manufacture_year.setValue(yearValues.toString());
                mWhereList.add(manufacture_year);

                mStockRequest.setWhere(mWhereList);

                if (CommonMethods.getstringvaluefromkey(activity, "certifiedstatus").equalsIgnoreCase("certifiedcar")) {

                    mWhere.setColumn("is_certified");
                    mWhere.setOperator("=");
                    mWhere.setValue("true");


                    mWhereList.add(mWhere);
                }

                if (stock_photocountsFlag || CommonMethods.getstringvaluefromkey(activity, "Notification_Switch").equalsIgnoreCase("noStockImage")) {

                    mWhere.setColumn("photo_count");
                    mWhere.setOperator("<=");
                    mWhere.setValue("0");
                    mWhereList.add(mWhere);
                    //stock_photocountsFlag =false;
                    CommonMethods.setvalueAgainstKey(activity, "photocounts", "true");
                    CommonMethods.setvalueAgainstKey(activity, "Notification_Switch", "type");
                    stock_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);

                }
            }

            Log.i("certifiedstatus", "certifiedstatus:== " + CommonMethods.getstringvaluefromkey(activity, "certifiedstatus"));

            if (CommonMethods.getstringvaluefromkey(activity, "certifiedstatus").equalsIgnoreCase("certifiedcar")) {

                Where is_certifiedWhere = new Where();

                is_certifiedWhere.setColumn("is_certified");
                is_certifiedWhere.setOperator("=");
                is_certifiedWhere.setValue("true");
                mWhereList.add(is_certifiedWhere);
            }


            if (CommonMethods.getstringvaluefromkey(activity, "Notification_Switch").equalsIgnoreCase("noStockImage")
                    || CommonMethods.getstringvaluefromkey(activity, "photocounts").equalsIgnoreCase("true")
                    || CommonMethods.getstringvaluefromkey(activity, "temp_Switch").equalsIgnoreCase("noStockImage")) {

                mWhere.setColumn("photo_count");
                mWhere.setOperator("<=");
                mWhere.setValue("0");
                mWhereList.add(mWhere);
                //stock_photocountsFlag =false;

                CommonMethods.setvalueAgainstKey(activity, "Notification_Switch", "type");
                CommonMethods.setvalueAgainstKey(activity, "temp_Switch", "type");
                CommonMethods.setvalueAgainstKey(activity, "photocounts", "true");
                stock_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);

            } else {
                CommonMethods.setvalueAgainstKey(activity, "photocounts", "false");

            }

            if (MainActivity.postdatelessthirty
                    || CommonMethods.getstringvaluefromkey(activity, "thirtydaycheckbox").equalsIgnoreCase("true")
                    || CommonMethods.getstringvaluefromkey(activity, "temp_Switch").equalsIgnoreCase("thirtydaycheckbox")) {

                Where posted_date = new Where();
                posted_date.setColumn("posted_date");
                posted_date.setOperator("<");
                posted_date.setValue(CalendarTodayDate());
                mWhereList.add(posted_date);
                CommonMethods.setvalueAgainstKey(activity, "temp_Switch", "type");
                CommonMethods.setvalueAgainstKey(activity, "thirtydaycheckbox", "true");
                stock_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);
                MainActivity.postdatelessthirty = false;
                lastmonth = "";

            }

            //new ASM
            if (!CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
                if (CommonMethods.getstringvaluefromkey(activity, "statuslist").equalsIgnoreCase("asmstore")) {

                    mWhere.setColumn(CommonMethods.getstringvaluefromkey(activity, "column"));
                    mWhere.setOperator(CommonMethods.getstringvaluefromkey(activity, "operator"));
                    mWhere.setValue(CommonMethods.getstringvaluefromkey(activity, "value"));
                    mWhereList.add(mWhere);
                    stock_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);

                    // auto fill
                    if (CommonMethods.getstringvaluefromkey(activity, "column").equalsIgnoreCase("photo_count")
                            && !CommonMethods.getstringvaluefromkey(activity, "30Name").equalsIgnoreCase("Stocks visible on MFC Website")
                            && CommonMethods.getstringvaluefromkey(activity, "value").equalsIgnoreCase("0")) {
                        CommonMethods.setvalueAgainstKey(activity, "photocounts", "true");
                    }
                    if (CommonMethods.getstringvaluefromkey(activity, "30Name").equalsIgnoreCase("Stocks >30days")
                            || CommonMethods.getstringvaluefromkey(activity, "value").equalsIgnoreCase("30")) {
                        CommonMethods.setvalueAgainstKey(activity, "thirtydaycheckbox", "true");
                    }

                    if (CommonMethods.getstringvaluefromkey(activity, "column").equalsIgnoreCase("is_certified")
                            && CommonMethods.getstringvaluefromkey(activity, "value").equalsIgnoreCase("true")) {
                        CommonMethods.setvalueAgainstKey(activity, "certifiedstatus", "certifiedcar");
                    }

                    //  CommonMethods.setvalueAgainstKey(activity, "m", "");

                }


                mStockRequest.setWherenotin(wh);
            }

            if (!searchVal.equals("")) {
                if (progress.getVisibility() == View.VISIBLE)
                    progress.setVisibility(View.GONE);

                Where vehicle_make = new Where();
                vehicle_make.setColumn("vehicle_make");
                vehicle_make.setOperator("like");
                vehicle_make.setValue(searchVal);
                mWhereList.add(vehicle_make);

                Where vehicle_model = new Where();
                vehicle_model.setColumn("vehicle_model");
                vehicle_model.setOperator("like");
                vehicle_model.setValue(searchVal);
                mWhereList.add(vehicle_model);

                Where vehicle_variant = new Where();
                vehicle_variant.setColumn("vehicle_variant");
                vehicle_variant.setOperator("like");
                vehicle_variant.setValue(searchVal);
                mWhereList.add(vehicle_variant);

                Where registration_number = new Where();
                registration_number.setColumn("registration_number");
                registration_number.setOperator("like");
                registration_number.setValue(searchVal);
                mWhereList.add(registration_number);

            }


            mStockRequest.setWhere(mWhereList);


            //retrofit call

            if (CommonMethods.isInternetWorking(activity)) {
                getStockDeatils(context, mStockRequest);
            } else {
                CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void getStockDeatils(final Context mContext, final StockRequest mStockRequest) {

        SpinnerManager.showSpinner(mContext);

        Log.i("getStockDeatils==", "getStockDeatils: ");

        StockService.fetchStock(mStockRequest, mContext, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                SpinnerManager.hideSpinner(mContext);


                try {
                    Response<StockResponse> mRes = (Response<StockResponse>) obj;

                    StockResponse mData = mRes.body();

                    List<StockData> mlist = mData.getData();

                    CommonMethods.setvalueAgainstKey(activity, "status", "StockStore");

                    if (CommonMethods.getstringvaluefromkey(activity, "sortby").equalsIgnoreCase("true")) {

                        if (CommonMethods.getstringvaluefromkey(activity, "stockstore_load").equalsIgnoreCase("true")) {

                            try {
                                mStockStoreAdapter.notifyItemInserted(stock.size() - 1);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    if (CommonMethods.getstringvaluefromkey(activity, "stockstore_load").equalsIgnoreCase("false")) {
                        stock = new ArrayList<>();
                    } else {
                        if ((stock.size() - 1) > 0) {
                            // myJobList.remove(myJobList.size()-1);
                        }
                    }

                    if (mlist.isEmpty()) {
                        nostock.setVisibility(View.VISIBLE);
                    } else {
                        if (nostock.getVisibility() == View.VISIBLE) {
                            nostock.setVisibility(View.GONE);
                        }
                    }
                    for (int i = 0; i < mlist.size(); i++) {
                        StockModel stockModel = new StockModel();

                        stockModel.setSource(mlist.get(i).getStockSource());
                        stockModel.setId(mlist.get(i).getStockId());
                        stockModel.setFeaturedCarText(/*jsonobject.getString("featured_car_text")*/"");
                        stockModel.setPostedDate(mlist.get(i).getPostedDate());
                        // stockModel.setStockAge(jsonobject.getInt("stock_age"));//
                        stockModel.setMake(mlist.get(i).getVehicleMake());
                        stockModel.setModel(mlist.get(i).getVehicleModel());
                        stockModel.setVariant(mlist.get(i).getVehicleVariant());
                        stockModel.setRegMonth(mlist.get(i).getRegMonth());
                        stockModel.setRegYear(Integer.parseInt(mlist.get(i).getRegYear()));

                        stockModel.setRegistraionCity(mlist.get(i).getRegistraionCity());
                        stockModel.setRegistrationNumber(mlist.get(i).getRegistrationNumber());
                        stockModel.setColour(mlist.get(i).getColour());
                        stockModel.setKilometer(mlist.get(i).getKilometer());
                        stockModel.setOwner(mlist.get(i).getOwner());
                        stockModel.setInsurance(mlist.get(i).getInsurance());
                        stockModel.setInsuranceExpDate(mlist.get(i).getInsuranceExpDate());
                        stockModel.setSellingPrice(mlist.get(i).getSellingPrice());
                        stockModel.setCertifiedText(mlist.get(i).getCertifiedText());
                        stockModel.setCertificationNumber(mlist.get(i).getCertificationNumber());
                        stockModel.setWarranty(mlist.get(i).getWarrantyRecommended());

                        stockModel.setPhotoCount(mlist.get(i).getPhotoCount());
                        //stockModel.setPhotoCount(1);

                        stockModel.setSurveyorCode(mlist.get(i).getSurveyorCode());
                        stockModel.setSurveyorModifiedDate(mlist.get(i).getSurveyorModifiedDate());

                       /* if (mlist.get(i).getSurveyorKilometer() != null) {
                            //reena retrofit model object type volley string SurveyorKilometer
                            stockModel.setSurveyorKilometer(String.valueOf(mlist.get(i).getSurveyorKilometer()));

                        }*/
                        stockModel.setSurveyorKilometer(String.valueOf(mlist.get(i).getSurveyorKilometer()));
                        stockModel.setSurveyorRemark(mlist.get(i).getSurveyorRemark());
                        stockModel.setDealerCode(mlist.get(i).getDealerCode());
                        stockModel.setIsbooked(mlist.get(i).getIsBooked());
                        //stockModel.setIssold(jsonobject.getString("is_sold"));
                        //stockModel.setIsDisplay(jsonobject.getString("is_display"));
                        stockModel.setIsOffload(mlist.get(i).getIsOffload());
                        stockModel.setFuel_type(mlist.get(i).getFuelType());

                        stockModel.setBought(String.valueOf(mlist.get(i).getBoughtPrice()));
                        //   if (mlist.get(i).getRefurbishmentCost() != null)
                        stockModel.setRefurbishment_cost(String.valueOf(mlist.get(i).getRefurbishmentCost()));
                        stockModel.setDealer_price(String.valueOf(mlist.get(i).getDealerPrice()));
                        stockModel.setProcurementExecId(String.valueOf(mlist.get(i).getProcurementExecutiveId()));
                        stockModel.setCng_kit(mlist.get(i).getCngKit());
                        stockModel.setChassis_number(mlist.get(i).getChassisNumber());
                        stockModel.setEngine_number(mlist.get(i).getEngineNumber());
                        stockModel.setIs_certified(mlist.get(i).getIsCertified());
                        stockModel.setIs_featured_car(mlist.get(i).getIsFeaturedCar());
                        //stockModel.setIs_featured_car_admin(jsonobject.getString("is_featured_car_admin"));
                        stockModel.setProcurement_executive_name(mlist.get(i).getProcurementExecutiveName());
                        stockModel.setComments(mlist.get(i).getComments());
                        stockModel.setFinance_required(mlist.get(i).getFinanceRequired());
                        stockModel.setManufacture_month(mlist.get(i).getManufactureMonth());
                        stockModel.setSales_executive_id(String.valueOf(mlist.get(i).getSalesExecutiveId()));
                        stockModel.setSales_executive_name(mlist.get(i).getSalesExecutiveName());
                        stockModel.setManufacture_year(mlist.get(i).getManufactureYear());
                        stockModel.setActual_selling_price(mlist.get(i).getActualSellingPrice());
                        stockModel.setCertifiedText(mlist.get(i).getCertifiedText());
                        //  stockModel.setIsDisplay(jsonobject.getString("IsDisplay"));
                        //stockModel.setTransmissionType(jsonobject.getString("TransmissionType"));
                        stockModel.setPrivate_vehicle(mlist.get(i).getPrivateVehicle());

                        List<StockImages> mStockImages = new ArrayList<>();
                        mStockImages = mlist.get(i).getImages();
                        JSONArray jsArray = new JSONArray(mStockImages);

                        stockModel.setImageUrl(jsArray);

                        if (mlist.get(i).getCoverImage().equalsIgnoreCase("")) {
                            if (jsArray != null && jsArray.length() > 0 && !jsArray.isNull(0)) {

                                if (imageurls != null) {
                                    imageurls.clear();
                                }
                                if (imagename != null) {
                                    imagename.clear();
                                }
                                if (objectList != null) {
                                    objectList = null;
                                }
                                if (objectList2 != null) {
                                    objectList2 = null;
                                }
                                if (imageurlss != null) {
                                    imageurlss = null;
                                }
                                if (imagenames != null) {
                                    imagenames = null;
                                }

                                // if (!jsArray.isNull(0)) {
                                for (int k = 0; k < jsArray.length(); k++) {
                                    try {
                                        JSONObject data = jsArray.getJSONObject(k);
                                        imageurls.add(data.getString("url"));
                                        imagename.add(data.getString("category_identifier"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                // }
                                objectList = imageurls.toArray();
                                imageurlss = Arrays.copyOf(objectList, objectList.length, String[].class);

                                objectList2 = imagename.toArray();
                                imagenames = Arrays.copyOf(objectList2, objectList2.length, String[].class);

                                stockModel.setCoverimage(imageurlss[0]);

                                for (int j = 0; j < imagenames.length; j++) {

                                    if (imagenames[j].equalsIgnoreCase("cover_image"))
                                        stockModel.setCoverimage(imageurlss[j]);
                                }

                            } else {
                                stockModel.setCoverimage("");
                            }

                        } else {
                            stockModel.setCoverimage(mlist.get(i).getCoverImage());
                        }

                        negotiationLeadCount = mRes.body().getTotal();
                        stock.add(stockModel);
                        // Log.e("report q", stock.get(i).getSource());
                    }

                } catch (Exception e) {

                }

                if (CommonMethods.getstringvaluefromkey(activity, "stockstore_load").equalsIgnoreCase("true")) {

                    mStockStoreAdapter.notifyDataChanged(stock);
                    if (progress.getVisibility() == View.VISIBLE)
                        progress.setVisibility(View.GONE);

                    // Log.e("notifyDataChanged", "notifyDataChanged=" + "coming");

                } else {
                    // Log.e("reloadData", "reloadData=" + "coming");

                    reloadData();
                }
                // try {
                if (swipeRefreshLayout != null) {
                    swipeRefreshLayout.setRefreshing(false);
                }
               /* }catch (Exception e){
                    if (swipeRefreshLayout != null) swipeRefreshLayout.setEnabled(false);
                }*/

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
                swipeRefreshLayout.setRefreshing(false);
                nostock.setVisibility(View.VISIBLE);
                // Toast.makeText(mContext, mThrowable.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
        });
    }


    private static void sendEmailDeatils(final Context mContext, final StockRequest mStockRequest) {

        SpinnerManager.showSpinner(mContext);

        StockService.sendEmail(mStockRequest, mContext, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                SpinnerManager.hideSpinner(mContext);

                try {
                    Response<String> mRes = (Response<String>) obj;

                    String mData = mRes.body();

                    CommonMethods.alertMessage(activity, "Mail sent" + "\n" + mData.toString());


                } catch (Exception e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    public static void stockFilterClear() {

        try {

            StockFilterActivity.stockResetData(activity);

            stock_filter_icon.setBackgroundResource(R.drawable.filter_48x18);

            FilterInstance.getInstance().setNotification("");
            CommonMethods.setvalueAgainstKey(activity, "thirtydaycheckbox", "false");
            CommonMethods.setvalueAgainstKey(activity, "Notification_Switch", "false");

            CommonMethods.setvalueAgainstKey(activity, "photocounts", "false");
            CommonMethods.setvalueAgainstKey(activity, "photocounts1", "false");
            CommonMethods.setvalueAgainstKey(activity, "photocounts2", "false");

            CommonMethods.setvalueAgainstKey(activity, "certifiedstatus", "allcar");

            CommonMethods.setvalueAgainstKey(activity, "certifiedval", "false");
            CommonMethods.setvalueAgainstKey(activity, "price_valuesval", "false");
            CommonMethods.setvalueAgainstKey(activity, "yearval", "false");
            CommonMethods.setvalueAgainstKey(activity, "kmsval", "false");
            CommonMethods.setvalueAgainstKey(activity, "clear", "false");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
