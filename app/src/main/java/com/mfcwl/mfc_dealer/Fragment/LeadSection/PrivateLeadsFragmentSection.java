package com.mfcwl.mfc_dealer.Fragment.LeadSection;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.Activity.LeadSection.LeadConstantsSection;
import com.mfcwl.mfc_dealer.Activity.Leads.LeadDetailsActivity;
import com.mfcwl.mfc_dealer.Activity.Leads.PrivateLeadsConstant;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Adapter.LeadSection.PrivateLeadsAdapterSection;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.InstanceCreate.LeadSection.LeadFilterSaveInstance;
import com.mfcwl.mfc_dealer.InstanceCreate.LeadSection.SortInstanceLeadSection;
import com.mfcwl.mfc_dealer.Interface.LeadSection.FilterApply;
import com.mfcwl.mfc_dealer.Interface.LeadSection.LazyloaderPrivateLeads;
import com.mfcwl.mfc_dealer.Interface.LeadSection.SearchbyPrivateLeads;
import com.mfcwl.mfc_dealer.Model.LeadSection.PrivateLeadDatum;
import com.mfcwl.mfc_dealer.Model.LeadSection.PrivateLeadWhere;
import com.mfcwl.mfc_dealer.Model.LeadSection.PrivateLeadWhereIn;
import com.mfcwl.mfc_dealer.Model.LeadSection.PrivateLeadsModelRequest;
import com.mfcwl.mfc_dealer.Model.LeadSection.PrivateLeadsModelResponse;
import com.mfcwl.mfc_dealer.Model.LeadSection.PrivateLeadsWhereor;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.RetrofitServicesLeadSection.PrivateLeadService;
import com.mfcwl.mfc_dealer.Utility.AppConstants;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GAConstants;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import rdm.NotificationActivity;
import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.activity;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.linearLayoutVisible;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchImage;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchicon;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toggle;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toolbar;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.tv_header_title;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ASM;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_STATEHEAD;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ZONALHEAD;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_DEALER_CRE;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_PROCUREMENT;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_SALES;

public class PrivateLeadsFragmentSection extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView mRecyclerView;
    private TextView mNoresults;
    private Button mbtnAllLeads;
    private Button mbtnFollowUpLeads;
    private LinearLayoutManager mLinearLayoutManager;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private Activity mActivity;
    private Context mContext;

    private List<PrivateLeadWhere> mwhereList;
    private List<PrivateLeadWhereIn> mwhereinList;
    private List<PrivateLeadsWhereor> mwhereorList;

    private List<PrivateLeadDatum> mFollowuplist;
    private List<PrivateLeadDatum> mLeadsDatumList;

    private List<String> mWhereList;

    private PrivateLeadsAdapterSection mPrivateLeadsAdapterSection;

    private int pageItem = 100;
    private String page_no = "1";
    private int counts = 1;
    private int negotiationLeadCount = 0;
    private boolean isLoadmore;

    // private String whichLeads = "AllLeads";
    private String searchQuery = "";

    private HashMap<String, String> postedfollDateHashMap = new HashMap<String, String>();
    private JSONArray leadstatusJsonArray = new JSONArray();

    public boolean flag = false;
    int i = 0;

    PrivateLeadsModelRequest privateLeadsModelRequest;
    public String TAG=getClass().getSimpleName();

    public PrivateLeadsFragmentSection() {
        // Required empty public constructor
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        searchImage.setImageResource(R.drawable.search);
        String uType = CommonMethods.getstringvaluefromkey(activity, "user_type");

                MenuItem asm_mail = menu.findItem(R.id.asm_mail);

                //new ASM
                if (uType.equalsIgnoreCase("dealer")) {
                    asm_mail.setVisible(false);
                } else if(CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_DEALER_CRE) ||
                        CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_SALES) ||
                        CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_PROCUREMENT)){
                    asm_mail.setVisible(false);
                } else{
                    asm_mail.setVisible(true);
                }
                MenuItem asmHome = menu.findItem(R.id.asmHome);
                MenuItem add_image = menu.findItem(R.id.add_image);
                MenuItem share = menu.findItem(R.id.share);
                MenuItem delete_fea = menu.findItem(R.id.delete_fea);
                MenuItem stock_fil_clear = menu.findItem(R.id.stock_fil_clear);
                MenuItem notification_bell = menu.findItem(R.id.notification_bell);
                MenuItem notification_cancel = menu.findItem(R.id.notification_cancel);

                asmHome.setVisible(false);
                add_image.setVisible(false);
                share.setVisible(false);
                delete_fea.setVisible(false);
                stock_fil_clear.setVisible(false);
                notification_bell.setVisible(false);
                notification_cancel.setVisible(false);




        asm_mail.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        //  Toast.makeText(getActivity(),"private leads sending email...",Toast.LENGTH_LONG).show();
                        if (CommonMethods.isInternetWorking(getActivity())) {


                            String isEmail="";

                            PrivateLeadWhereIn email =new PrivateLeadWhereIn();
                            ArrayList<String> emailArray = new ArrayList<String>();
                            emailArray.add("true");

                            email.setColumn("isEmail");
                            email.setValues(emailArray);

                            for(int i =0;i<mwhereinList.size();i++){

                                if(mwhereinList.get(i).getColumn().equalsIgnoreCase("isEmail")){
                                    isEmail="isEmail";
                                }
                            }

                            if(isEmail.equalsIgnoreCase("")) {
                                mwhereinList.add(email);
                                privateLeadsModelRequest.setWherein(mwhereinList);
                            }

                            // custom dialog
                            Dialog dialog = new Dialog(activity);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.getWindow().setGravity(Gravity.TOP|Gravity.RIGHT);
                            dialog.setContentView(R.layout.common_cus_dialog);
                            TextView Message, Message2;
                            Button cancel, Confirm;
                            Message = dialog.findViewById(R.id.Message);
                            Message2 = dialog.findViewById(R.id.Message2);
                            cancel = dialog.findViewById(R.id.cancel);
                            Confirm = dialog.findViewById(R.id.Confirm);
                            Message.setText(GlobalText.are_you_sure_to_send_mail);
                            //Message2.setText("Go ahead and Submit ?");
                            Confirm.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    sendEmailPrivateLeadDetails(getActivity(), privateLeadsModelRequest);
                                    dialog.dismiss();
                                }
                            });
                            cancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                            dialog.show();



                        } else {
                            CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);

                        }
                        return true;
                    }
                });


        //return true;

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.layout_private_leads, container, false);
        mActivity = getActivity();
        mContext = getContext();
        setHasOptionsMenu(true);

        if(CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(USERTYPE_ASM))
        {
            linearLayoutVisible.setVisibility(View.VISIBLE);
            searchicon.setVisibility(View.VISIBLE);
            searchImage.setVisibility(View.VISIBLE);
        }
        if(tv_header_title!=null && CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ZONALHEAD) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_STATEHEAD))
        {
            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText("Leads");
            tv_header_title.setTextColor(getResources().getColor(R.color.white));
        }
        else if(tv_header_title!=null)
        {
            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText("Sales Leads");
            tv_header_title.setTextColor(getResources().getColor(R.color.white));
        }

        InitView(view);
        return view;
    }

    private void InitView(View view) {

        searchicon.setVisibility(View.VISIBLE);
        linearLayoutVisible.setVisibility(View.VISIBLE);

        mRecyclerView = view.findViewById(R.id.normal_leads_recycler);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        mNoresults = view.findViewById(R.id.noresults);
        mbtnAllLeads = view.findViewById(R.id.btnAllLeads);
        mbtnFollowUpLeads = view.findViewById(R.id.btnFollowUpLeads);

        mSwipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.CYAN);

        mbtnAllLeads.setOnClickListener(this);
        mbtnFollowUpLeads.setOnClickListener(this);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        isLoadmore = false;

        if (PrivateLeadsConstant.getInstance().getWhichleads().equalsIgnoreCase("AllLeads")) {
            mbtnAllLeads.setTextColor(Color.BLACK);
            mbtnAllLeads.setBackgroundResource(R.drawable.my_button);
            mbtnFollowUpLeads.setTextColor(Color.parseColor("#b9b9b9"));
            mbtnFollowUpLeads.setBackgroundResource(R.drawable.my_buttom_gray_light);
        } else if (PrivateLeadsConstant.getInstance().getWhichleads().equalsIgnoreCase("FollowupLeads")) {
            mbtnFollowUpLeads.setTextColor(Color.BLACK);
            mbtnFollowUpLeads.setBackgroundResource(R.drawable.my_button);
            mbtnAllLeads.setTextColor(Color.parseColor("#b9b9b9"));
            mbtnAllLeads.setBackgroundResource(R.drawable.my_buttom_gray_light);
        }
        if (CommonMethods.getstringvaluefromkey(getActivity(), "PrivateLeadsP").equalsIgnoreCase("true")) {
            //  CommonMethods.setvalueAgainstKey(getActivity(), "PrivateLeadsP", "false");
            try {
                i = Integer.parseInt(LeadDetailsActivity.position);
            } catch (Exception e) {
                i = 0;
                // e.printStackTrace();
            }
        } else {
            i = 0;
        }
        if(CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase("dealer"))
        {
            bottomTabScrolling();
        }
        preparePrivateLeadRequest();
        onCallbackEvents();
        int margin = getResources().getDimensionPixelSize(R.dimen.recyclepadding);
        //new ASM
        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
        }else{
            ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) mSwipeRefreshLayout.getLayoutParams();
            mlp.setMargins(0,0,0,margin);
        }

    }

    private void bottomTabScrolling() {
        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            int verticalOffset;
            boolean scrollingUp;

            // Determines the scroll UP/DOWN direction
            @Override
            public void onScrollStateChanged(@NotNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                verticalOffset += dy;
                scrollingUp = dy > 0;
                if (scrollingUp) {
                    if (flag) {
                        MainActivity.bottom_topAnimation();
                        flag = false;
                    }
                } else {
                    if (!flag) {
                        MainActivity.top_bottomAnimation();
                        flag = true;
                    }
                }
            }
        });
    }


    private void preparePrivateLeadRequest() {

        mwhereList = new ArrayList<>();
        mwhereinList = new ArrayList<>();
        mwhereorList = new ArrayList<>();

        //new changes
         privateLeadsModelRequest = new PrivateLeadsModelRequest();

        privateLeadsModelRequest.setPageItems(Integer.toString(pageItem));
        privateLeadsModelRequest.setPage(page_no);
        sortbyPrivateLeads(privateLeadsModelRequest);

        searchbyPrivateLeads(mwhereorList);
        privateLeadsModelRequest.setWhereOr(mwhereorList);

        //Posted Followup Date Filter
        setPostedFollowupDateInfo();

        privateLeadsModelRequest.setWhere(mwhereList);

        //Update Lead Status
        PrivateLeadWhereIn privateLeadWhereIn = new PrivateLeadWhereIn();

        setLeadStatus(privateLeadWhereIn);

        privateLeadsModelRequest.setWherein(mwhereinList);

        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_SALES) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_PROCUREMENT)) {
            PrivateLeadWhere where = new PrivateLeadWhere();
            where.setColumn("executive_id");
            where.setOperator("=");
            where.setValue(CommonMethods.getstringvaluefromkey(activity, "user_id"));
            mwhereList.add(where);
            privateLeadsModelRequest.setWhere(mwhereList);
        }

        sendPrivateLeadDetails(mActivity, privateLeadsModelRequest);

    }


    private void sendPrivateLeadDetails(final Activity mActivity, PrivateLeadsModelRequest privateLeadsModelRequest) {
        mSwipeRefreshLayout.setRefreshing(false);
        SpinnerManager.showSpinner(mActivity);
        PrivateLeadService.postPrivateLead(privateLeadsModelRequest, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mActivity);

                Response<PrivateLeadsModelResponse> mRes = (Response<PrivateLeadsModelResponse>) obj;
                PrivateLeadsModelResponse mData = mRes.body();

                String str = new Gson().toJson(mData, PrivateLeadsModelResponse.class);
                Log.i(TAG, "OnSuccess: " + str);

                negotiationLeadCount = mData.getTotal();

                List<PrivateLeadDatum> mlist = mData.getData();
                Log.i(TAG, "OnSuccess: " + mlist.size());
                if (isLoadmore) {
                    mPrivateLeadsAdapterSection.notifyItemInserted(mLeadsDatumList.size() - 1);
                    mLeadsDatumList.addAll(mlist);
                    mPrivateLeadsAdapterSection.notifyDataSetChanged();
                } else {
                    mLeadsDatumList = new ArrayList<>();
                    mLeadsDatumList.addAll(mlist);
                    attachtoAdapter(mLeadsDatumList);
                }
                isLeadsAvailable(negotiationLeadCount);

            }

            @Override
            public void OnFailure(Throwable t) {
                SpinnerManager.hideSpinner(mActivity);
                mSwipeRefreshLayout.setRefreshing(false);
                t.printStackTrace();
            }
        });

    }


    private void sendEmailPrivateLeadDetails(final Activity mActivity, PrivateLeadsModelRequest privateLeadsModelRequest) {
        mSwipeRefreshLayout.setRefreshing(false);
        SpinnerManager.showSpinner(mActivity);
        PrivateLeadService.sendEmailpostPrivateLead(privateLeadsModelRequest, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mActivity);

                Response<String> mRes = (Response<String>) obj;
                String mData = mRes.body();
                CommonMethods.alertMessage(getActivity(),"Mail sent"+"\n"+mData.toString());

            }

            @Override
            public void OnFailure(Throwable t) {
                SpinnerManager.hideSpinner(mActivity);
                mSwipeRefreshLayout.setRefreshing(false);
                t.printStackTrace();
            }
        });

    }


    private void preparefollowupPrivateLeadRequest() {

        mFollowuplist = new ArrayList<>();
        mWhereList = new ArrayList<>();

        mwhereList = new ArrayList<>();
        mwhereinList = new ArrayList<>();
        mwhereorList = new ArrayList<>();

        PrivateLeadsModelRequest privateLeadsModelRequest = new PrivateLeadsModelRequest();

        privateLeadsModelRequest.setPageItems(Integer.toString(pageItem));
        privateLeadsModelRequest.setPage(page_no);

        sortbyPrivateLeads(privateLeadsModelRequest);

        searchbyPrivateLeads(mwhereorList);

        //Posted Followup Date Filter
        setPostedFollowupDateInfo();
        privateLeadsModelRequest.setWhere(mwhereList);

        //Update Lead Status
        PrivateLeadWhereIn privateLeadWhereIn = new PrivateLeadWhereIn();
        setLeadStatus(privateLeadWhereIn);
        privateLeadsModelRequest.setWherein(mwhereinList);

        privateLeadsModelRequest.setWhereOr(mwhereorList);

        //follow-up date (apply future date over here)
        PrivateLeadWhere privateLeadWhere = new PrivateLeadWhere();
        Date d = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(d);
        privateLeadWhere.setColumn("follow_date");
        privateLeadWhere.setOperator("<=");
        privateLeadWhere.setValue(formattedDate);
        mwhereList.add(privateLeadWhere);

        String usertype = CommonMethods.getstringvaluefromkey(activity, "user_type");
        if (usertype.equalsIgnoreCase(USER_TYPE_SALES) ||
                usertype.equalsIgnoreCase(USER_TYPE_PROCUREMENT)) {
            PrivateLeadWhere where2 = new PrivateLeadWhere();
            where2.setColumn("executive_id");
            where2.setOperator("=");
            where2.setValue(CommonMethods.getstringvaluefromkey(activity, "user_id"));
            mwhereList.add(where2);
        }

        privateLeadsModelRequest.setWhere(mwhereList);

        sendfollowupPrivateLeadDetails(mActivity, privateLeadsModelRequest);

    }

    private void sendfollowupPrivateLeadDetails(final Activity mActivity, PrivateLeadsModelRequest privateLeadsModelRequest) {
        mSwipeRefreshLayout.setRefreshing(false);
        SpinnerManager.showSpinner(mActivity);
        PrivateLeadService.postPrivateLead(privateLeadsModelRequest, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mActivity);
                Response<PrivateLeadsModelResponse> mRes = (Response<PrivateLeadsModelResponse>) obj;
                PrivateLeadsModelResponse mData = mRes.body();
                String str = new Gson().toJson(mData, PrivateLeadsModelResponse.class);
                Log.i(TAG, "OnSuccess: " + str);
                negotiationLeadCount = mData.getTotal();
                List<PrivateLeadDatum> mlist = mData.getData();
                Log.i(TAG, "OnSuccess: " + mlist.size());
                PrivateLeadDatum mPrivateLeadDatum = null;
                for (int i = 0; i < mlist.size(); i++) {
                    mPrivateLeadDatum = mlist.get(i);
                    if (mPrivateLeadDatum.getLeadStatus() != null && !mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Sold") &&
                            !mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Lost") &&
                            !mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Closed") &&
                            !mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Finalised") &&
                            !mPrivateLeadDatum.getLeadStatus().equalsIgnoreCase("Not-Interested")) {
                        mFollowuplist.add(mPrivateLeadDatum);
                    }

                }
                if (isLoadmore) {
                    mPrivateLeadsAdapterSection.notifyItemInserted(mLeadsDatumList.size() - 1);
                    mLeadsDatumList.addAll(mFollowuplist);
                    mPrivateLeadsAdapterSection.notifyDataSetChanged();
                } else {
                    mLeadsDatumList = new ArrayList<>();
                    mLeadsDatumList.addAll(mFollowuplist);
                    attachtoAdapter(mLeadsDatumList);
                }
                isLeadsAvailable(negotiationLeadCount);
                //attachtoAdapter(mFollowuplist);

            }

            @Override
            public void OnFailure(Throwable t) {
                SpinnerManager.hideSpinner(mActivity);
                mSwipeRefreshLayout.setRefreshing(false);
                t.printStackTrace();
            }
        });

    }

    private void isLeadsAvailable(int negotiationLeadCount) {
        if (negotiationLeadCount == 0) {
            mNoresults.setVisibility(View.VISIBLE);
            mSwipeRefreshLayout.setVisibility(View.GONE);
        } else {
            mNoresults.setVisibility(View.GONE);
            mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        }
    }

    private void attachtoAdapter(List<PrivateLeadDatum> mlist) {

        mPrivateLeadsAdapterSection = new PrivateLeadsAdapterSection(mContext, mlist, mActivity);
        mRecyclerView.setHasFixedSize(false);
        mLinearLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setAdapter(mPrivateLeadsAdapterSection);

        mRecyclerView.scrollToPosition(i);

    }

    @Override
    public void onClick(View v) {
        restoreInputValues();
        i = 0;
        switch (v.getId()) {
            case R.id.btnAllLeads:
                //  whichLeads = "AllLeads";
                PrivateLeadsConstant.getInstance().setWhichleads("AllLeads");
                mbtnAllLeads.setTextColor(Color.BLACK);
                mbtnAllLeads.setBackgroundResource(R.drawable.my_button);
                mbtnFollowUpLeads.setTextColor(Color.parseColor("#b9b9b9"));
                mbtnFollowUpLeads.setBackgroundResource(R.drawable.my_buttom_gray_light);
                preparePrivateLeadRequest();

                break;
            case R.id.btnFollowUpLeads:
                // whichLeads = "FollowupLeads";
                PrivateLeadsConstant.getInstance().setWhichleads("FollowupLeads");
                mbtnFollowUpLeads.setTextColor(Color.BLACK);
                mbtnFollowUpLeads.setBackgroundResource(R.drawable.my_button);
                mbtnAllLeads.setTextColor(Color.parseColor("#b9b9b9"));
                mbtnAllLeads.setBackgroundResource(R.drawable.my_buttom_gray_light);
                preparefollowupPrivateLeadRequest();

                break;
        }
    }

    @Override
    public void onRefresh() {
        restoreInputValues();
        i = 0;
        mSwipeRefreshLayout.setRefreshing(true);
        if (PrivateLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
            preparePrivateLeadRequest();
        } else {
            preparefollowupPrivateLeadRequest();
        }
    }

    private void sortbyPrivateLeads(PrivateLeadsModelRequest privateLeadsModelRequest) {

        if (SortInstanceLeadSection.getInstance().getSortby().equals("posted_date_l_o")) {
            privateLeadsModelRequest.setOrderBy("created_at");
            privateLeadsModelRequest.setOrderByReverse("true");
        } else if (SortInstanceLeadSection.getInstance().getSortby().equals("posted_date_o_l")) {
            privateLeadsModelRequest.setOrderBy("created_at");
            privateLeadsModelRequest.setOrderByReverse("false");
        } else if (SortInstanceLeadSection.getInstance().getSortby().equals("folowup_date_l_o")) {
            privateLeadsModelRequest.setOrderBy("follow_date");
            privateLeadsModelRequest.setOrderByReverse("true");
        } else if (SortInstanceLeadSection.getInstance().getSortby().equals("folowup_date_o_l")) {
            privateLeadsModelRequest.setOrderBy("follow_date");
            privateLeadsModelRequest.setOrderByReverse("false");
        } else {
            privateLeadsModelRequest.setOrderBy("created_at");
            privateLeadsModelRequest.setOrderByReverse("true");
        }

    }

    private void onCallbackEvents() {

        ((MainActivity) getActivity()).setFragmentSortPrivateListener(new MainActivity.FragmentPrivateSortListener() {
            @Override
            public void onSortPrivate() {
                if (CommonMethods.getstringvaluefromkey(getActivity(), "PrivateLeadsP").equalsIgnoreCase("false")) {
                    i = 0;
                }
                restoreInputValues();
                if (PrivateLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
                    preparePrivateLeadRequest();
                } else {
                    preparefollowupPrivateLeadRequest();
                }
            }
        });

        ((MainActivity) getActivity()).updateSearchPrivateLead(new SearchbyPrivateLeads() {
            @Override
            public void onSearchPrivateLead(String query) {

                searchQuery = query;
                //  Log.e("mLead ", "size " + mLeadsDatumList.size());
                if (mLeadsDatumList.isEmpty()) {
                    i = 0;
                    restoreInputValues();
                    if (PrivateLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
                        preparePrivateLeadRequest();
                    } else {
                        preparefollowupPrivateLeadRequest();
                    }
                }

                mPrivateLeadsAdapterSection.filter(query);

            }
        });

        ((MainActivity) getActivity()).setLazyloaderPrivateLeadsListener(new LazyloaderPrivateLeads() {
            @Override
            public void loadmorePrivate(int ItemCount) {
                counts = counts + 1;
                double maxPageNumber = Math.ceil((double) (negotiationLeadCount) / (double) pageItem);
                if (counts > maxPageNumber) {
                    return;
                }
                page_no = Integer.toString(counts);

                isLoadmore = true;

                if (PrivateLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
                    preparePrivateLeadRequest();
                } else {
                    preparefollowupPrivateLeadRequest();
                }

            }

            @Override
            public void updatecountPrivate(int count) {
                //   Log.e("updatecountPrivate ", "Filter Size " + count);
                if (count <= 3) {
                    mLeadsDatumList.clear();
                    if (CommonMethods.getstringvaluefromkey(getActivity(), "PrivateLeadsP").equalsIgnoreCase("false")) {
                        i = 0;
                    }
                    restoreInputValues();
                    if (PrivateLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
                        preparePrivateLeadRequest();
                    } else {
                        preparefollowupPrivateLeadRequest();
                    }
                }

            }
        });

        //Apply Filter

        ((MainActivity) getActivity()).setApplyPrivateFilterListener(new FilterApply() {
            @Override
            public void applyFilter(String typeLead) {
                //   Log.e("typeLead ", "Private " + typeLead);
                restoreInputValues();
                if (CommonMethods.getstringvaluefromkey(getActivity(), "PrivateLeadsP").equalsIgnoreCase("false")) {
                    i = 0;
                }
                if (PrivateLeadsConstant.getInstance().getWhichleads().equals("AllLeads")) {
                    preparePrivateLeadRequest();
                } else {
                    preparefollowupPrivateLeadRequest();
                }

            }
        });

    }

    private void restoreInputValues() {
        page_no = "1";
        counts = 1;
        negotiationLeadCount = 0;
        isLoadmore = false;
    }

    private void searchbyPrivateLeads(List<PrivateLeadsWhereor> mwhereorList) {

        if (!searchQuery.equals("")) {
            //Make
            PrivateLeadsWhereor vehicle_make = new PrivateLeadsWhereor();
            vehicle_make.setColumn("make");
            vehicle_make.setOperator("like");
            vehicle_make.setValue(searchQuery.trim());
            mwhereorList.add(vehicle_make);

            //Model
            PrivateLeadsWhereor vehicle_model = new PrivateLeadsWhereor();
            vehicle_model.setColumn("model");
            vehicle_model.setOperator("like");
            vehicle_model.setValue(searchQuery.trim());
            mwhereorList.add(vehicle_model);

            //Variant
            PrivateLeadsWhereor vehicle_variant = new PrivateLeadsWhereor();
            vehicle_variant.setColumn("variant");
            vehicle_variant.setOperator("like");
            vehicle_variant.setValue(searchQuery.trim());
            mwhereorList.add(vehicle_variant);

            //Name
            PrivateLeadsWhereor customer_name = new PrivateLeadsWhereor();
            customer_name.setColumn("customer_name");
            customer_name.setOperator("like");
            customer_name.setValue(searchQuery.trim());
            mwhereorList.add(customer_name);

            //Mobile
            PrivateLeadsWhereor customer_mobile = new PrivateLeadsWhereor();
            customer_mobile.setColumn("customer_mobile");
            customer_mobile.setOperator("like");
            customer_mobile.setValue(searchQuery.trim());
            mwhereorList.add(customer_mobile);

        }

    }

    private void setPostedFollowupDateInfo() {
        postedfollDateHashMap = LeadFilterSaveInstance.getInstance().getSavedatahashmap();
        String postdateinfo = "";

        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_TODAY) != null) {
            postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_TODAY);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_YESTER) != null) {
            postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_YESTER);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_7DAYS) != null) {
            postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_7DAYS);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_15DAYS) != null) {
            postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_15DAYS);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE) != null) {
            postdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE);
        }

        PrivateLeadWhere postDateCustomWhereStart = new PrivateLeadWhere();
        PrivateLeadWhere postDateCustomWhereEnd = new PrivateLeadWhere();
        if (postdateinfo != null && postdateinfo.contains("@")) {
            //split the date and atach to query
            String[] createddates = postdateinfo.split("@");

            try {


                if (CommonMethods.getstringvaluefromkey(getActivity(), "user_type").equalsIgnoreCase("dealer")) {


                    if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE) != null) {

                        postDateCustomWhereStart.setColumn("created_at");
                        postDateCustomWhereEnd.setColumn("created_at");

                        postDateCustomWhereStart.setOperator(">=");
                        postDateCustomWhereEnd.setOperator("<=");

                        postDateCustomWhereStart.setValue(createddates[0]);
                        postDateCustomWhereEnd.setValue(createddates[1]);

                        mwhereList.add(postDateCustomWhereStart);
                        mwhereList.add(postDateCustomWhereEnd);

                    } else {

                        postDateCustomWhereStart.setColumn("created_at");
                        postDateCustomWhereEnd.setColumn("created_at");

                        postDateCustomWhereStart.setOperator(">=");
                        postDateCustomWhereEnd.setOperator("<");

                        postDateCustomWhereStart.setValue(createddates[0]);
                        postDateCustomWhereEnd.setValue(createddates[1]);

                        mwhereList.add(postDateCustomWhereStart);
                        mwhereList.add(postDateCustomWhereEnd);
                    }


                } else {

                    if (CommonMethods.getstringvaluefromkey(getActivity(), "leadStatus").equalsIgnoreCase("true")) {
                        try {
                            // remove
                            if (!CommonMethods.getstringvaluefromkey(getActivity(), "leads_status")
                                    .equalsIgnoreCase("")) {

                            }else{
                                LeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());
                            }

                        } catch (Exception e) {
                        }


                            postDateCustomWhereStart.setColumn("created_at");
                            postDateCustomWhereEnd.setColumn("created_at");

                            String startDate = CommonMethods.getstringvaluefromkey(getActivity(), "startDate");
                            String endDate = CommonMethods.getstringvaluefromkey(getActivity(), "endDate");

                            if (!startDate.equalsIgnoreCase("")) {
                                postDateCustomWhereStart.setOperator(">=");
                                postDateCustomWhereEnd.setOperator("<=");

                                postDateCustomWhereStart.setValue(startDate);
                                postDateCustomWhereEnd.setValue(endDate);

                                mwhereList.add(postDateCustomWhereStart);
                                mwhereList.add(postDateCustomWhereEnd);
                            }


                    } else {

                        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_POST_CUSTDATE) != null) {

                            postDateCustomWhereStart.setColumn("created_at");
                            postDateCustomWhereEnd.setColumn("created_at");

                            postDateCustomWhereStart.setOperator(">=");
                            postDateCustomWhereEnd.setOperator("<=");

                            postDateCustomWhereStart.setValue(createddates[0]);
                            postDateCustomWhereEnd.setValue(createddates[1]);

                            mwhereList.add(postDateCustomWhereStart);
                            mwhereList.add(postDateCustomWhereEnd);

                        } else {

                            postDateCustomWhereStart.setColumn("created_at");
                            postDateCustomWhereEnd.setColumn("created_at");

                            postDateCustomWhereStart.setOperator(">=");
                            postDateCustomWhereEnd.setOperator("<");

                            postDateCustomWhereStart.setValue(createddates[0]);
                            postDateCustomWhereEnd.setValue(createddates[1]);

                            mwhereList.add(postDateCustomWhereStart);
                            mwhereList.add(postDateCustomWhereEnd);
                        }

                    }

                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {


            if (CommonMethods.getstringvaluefromkey(getActivity(), "user_type").equalsIgnoreCase("dealer")) {

            } else {

                postDateCustomWhereStart.setColumn("created_at");
                postDateCustomWhereEnd.setColumn("created_at");

                String startDate = CommonMethods.getstringvaluefromkey(getActivity(), "startDate");
                String endDate = CommonMethods.getstringvaluefromkey(getActivity(), "endDate");

                if(!startDate.equalsIgnoreCase("")) {

                    postDateCustomWhereStart.setOperator(">=");
                    postDateCustomWhereEnd.setOperator("<=");

                    postDateCustomWhereStart.setValue(startDate);
                    postDateCustomWhereEnd.setValue(endDate);

                    mwhereList.add(postDateCustomWhereStart);
                    mwhereList.add(postDateCustomWhereEnd);
                }

            }

        }

        String followdateinfo = "";

        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TMRW) != null) {
            followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TMRW);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TODAY) != null) {
            followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TODAY);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_YESTER) != null) {
            followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_YESTER);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_7DAYS) != null) {
            followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_7DAYS);
        }
        if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_CUSTDATE) != null) {
            followdateinfo = postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_CUSTDATE);
        }

        PrivateLeadWhere followDateCustomWhereStart = new PrivateLeadWhere();
        PrivateLeadWhere followDateCustomWhereEnd = new PrivateLeadWhere();
        if (followdateinfo != null && followdateinfo.contains("@")) {

            String[] follow = followdateinfo.split("@");
            try {
                followDateCustomWhereStart.setColumn("follow_date");
                followDateCustomWhereEnd.setColumn("follow_date");

                if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TMRW) != null) {
                    followDateCustomWhereStart.setOperator(">");
                    followDateCustomWhereEnd.setOperator("<=");
                    followDateCustomWhereStart.setValue(follow[1]);
                    followDateCustomWhereEnd.setValue(follow[0]);

                }
                if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_TODAY) != null || postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_YESTER) != null
                        || postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_7DAYS) != null) {
                    followDateCustomWhereStart.setOperator(">=");
                    followDateCustomWhereEnd.setOperator("<");
                    followDateCustomWhereStart.setValue(follow[0]);
                    followDateCustomWhereEnd.setValue(follow[1]);
                }
                if (postedfollDateHashMap.get(LeadConstantsSection.LEAD_FOLUP_CUSTDATE) != null) {
                    followDateCustomWhereStart.setOperator(">=");
                    followDateCustomWhereEnd.setOperator("<=");
                    followDateCustomWhereStart.setValue(follow[0]);
                    followDateCustomWhereEnd.setValue(follow[1]);
                }

                mwhereList.add(followDateCustomWhereStart);
                mwhereList.add(followDateCustomWhereEnd);


            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            if (CommonMethods.getstringvaluefromkey(getActivity(), "leadStatus").equalsIgnoreCase("true")
                    && !CommonMethods.getstringvaluefromkey(getActivity(), "value2").equalsIgnoreCase("")
                    && !CommonMethods.getstringvaluefromkey(getActivity(), "messageCenter").equalsIgnoreCase("true")) {


                followDateCustomWhereStart.setColumn("follow_date");
                followDateCustomWhereEnd.setColumn("follow_date");

                followDateCustomWhereStart.setOperator(CommonMethods.getstringvaluefromkey(getActivity(), "operator"));
                followDateCustomWhereEnd.setOperator(CommonMethods.getstringvaluefromkey(getActivity(), "operator2"));

                followDateCustomWhereStart.setValue(CommonMethods.getstringvaluefromkey(getActivity(), "value"));
                followDateCustomWhereEnd.setValue(CommonMethods.getstringvaluefromkey(getActivity(), "value2"));


                mwhereList.add(followDateCustomWhereStart);
                mwhereList.add(followDateCustomWhereEnd);

            }
        }
    }

    private void setLeadStatus(PrivateLeadWhereIn privateLeadWhereIn) {
        privateLeadWhereIn.setColumn("lead_type");
        privateLeadWhereIn.setValues(Collections.singletonList("Sales"));
        mwhereinList.add(privateLeadWhereIn);

        ArrayList<String> listStatus = new ArrayList<String>();
        ArrayList<String> dealercode = new ArrayList<String>();
        ArrayList<String> dealertype = new ArrayList<String>();


        leadstatusJsonArray = LeadFilterSaveInstance.getInstance().getStatusarray();
        // Log.e("setLeadStatus ", "json " + leadstatusJsonArray);


        // New ASM
        if (CommonMethods.getstringvaluefromkey(getActivity(), "user_type").equalsIgnoreCase("dealer")) {


            if (leadstatusJsonArray.length() != 0) {
                privateLeadWhereIn.setColumn("lead_status");
                for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                    try {
                        listStatus.add(leadstatusJsonArray.getString(i));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                privateLeadWhereIn.setValues(listStatus);
                mwhereinList.add(privateLeadWhereIn);
            }

        } else {

            if (CommonMethods.getstringvaluefromkey(getActivity(), "leadStatus").equalsIgnoreCase("true")) {
                //remove if already presents in json arry

                if (!CommonMethods.getstringvaluefromkey(getActivity(), "Lvalue").equalsIgnoreCase("")) {

                    // remove
                    if (leadstatusJsonArray.length() != 0) {
                        for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                            try {
                                leadstatusJsonArray.remove(i);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    listStatus.add(CommonMethods.getstringvaluefromkey(getActivity(), "Lvalue"));
                    leadstatusJsonArray.put(CommonMethods.getstringvaluefromkey(getActivity(), "Lvalue"));
                    privateLeadWhereIn.setColumn("lead_status");
                    privateLeadWhereIn.setValues(listStatus);
                    mwhereinList.add(privateLeadWhereIn);

                }

                PrivateLeadWhereIn codeWhereIn = new PrivateLeadWhereIn();
                PrivateLeadWhereIn typeWhereIn = new PrivateLeadWhereIn();

                dealertype.add("SALES");
                codeWhereIn.setColumn("lead_type");
                codeWhereIn.setValues(dealertype);
                mwhereinList.add(codeWhereIn);

                dealercode.add(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"));
                typeWhereIn.setColumn("dealer_id");
                typeWhereIn.setValues(dealercode);
                mwhereinList.add(typeWhereIn);


            } else {


                if (leadstatusJsonArray.length() != 0) {
                    privateLeadWhereIn.setColumn("lead_status");
                    for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                        try {
                            listStatus.add(leadstatusJsonArray.getString(i));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    privateLeadWhereIn.setValues(listStatus);
                    mwhereinList.add(privateLeadWhereIn);
                }


                //ASM

                PrivateLeadWhereIn codeWhereIn = new PrivateLeadWhereIn();
                PrivateLeadWhereIn typeWhereIn = new PrivateLeadWhereIn();

                dealertype.add("sales");
                codeWhereIn.setColumn("lead_type");
                codeWhereIn.setValues(dealertype);
                mwhereinList.add(codeWhereIn);

                dealercode.add(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"));
                typeWhereIn.setColumn("dealer_id");
                typeWhereIn.setValues(dealercode);
                mwhereinList.add(typeWhereIn);

            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //new ASM
        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
        }else{
            Application.getInstance().trackScreenView(activity,GlobalText.asm_privateLeads);
        }
        if ( CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(AppConstants.LOGINTYPE_DEALER)||
                CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(USERTYPE_ASM)||
                CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(AppConstants.LOGINTYPE_DEALERCRE) ||
                CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(AppConstants.LOGINTYPE_PROCUREMENT) ||
                CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(AppConstants.LOGINTYPE_SLAES)||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ZONALHEAD) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_STATEHEAD)) {
            if (tv_header_title != null) {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Sales Leads");
            }
        }

        if(CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(USERTYPE_ASM))
        {

            linearLayoutVisible.setVisibility(View.VISIBLE);
            searchicon.setVisibility(View.VISIBLE);
            searchImage.setVisibility(View.VISIBLE);
            toggle.setHomeAsUpIndicator(R.drawable.ic_new_hamburger_menu);
            toolbar.setBackgroundColor(getResources().getColor(R.color.black));
            if(tv_header_title!=null)
            {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Sales Leads");
                tv_header_title.setTextColor(getResources().getColor(R.color.white));
            }
            Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_SALES_LEADS,CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));

        }
    }
}
