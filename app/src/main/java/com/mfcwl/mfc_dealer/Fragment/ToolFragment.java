package com.mfcwl.mfc_dealer.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Activity.SplashActivity;
import com.mfcwl.mfc_dealer.Activity.StockFilterActivity;
import com.mfcwl.mfc_dealer.Activity.ToolsFilterActivity;
import com.mfcwl.mfc_dealer.Adapter.ToolsMarketTrandStockAdapter;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.InstanceCreate.ToolsFilterInstance;
import com.mfcwl.mfc_dealer.Model.ToolsModel;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.StockModels.StockRequest;
import com.mfcwl.mfc_dealer.StockModels.Where;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GAConstants;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.activity;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.linearLayoutVisible;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchImage;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchVal;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchicon;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toggle;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toolbar;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.tv_header_title;
import static com.mfcwl.mfc_dealer.Activity.ToolsFilterActivity.dealer_endPriceValues;
import static com.mfcwl.mfc_dealer.Activity.ToolsFilterActivity.dealer_startPriceValues;
import static com.mfcwl.mfc_dealer.Activity.ToolsFilterActivity.endPriceValues;
import static com.mfcwl.mfc_dealer.Activity.ToolsFilterActivity.kilometerValues;
import static com.mfcwl.mfc_dealer.Activity.ToolsFilterActivity.startPriceValues;
import static com.mfcwl.mfc_dealer.Activity.ToolsFilterActivity.yearValues;
import static com.mfcwl.mfc_dealer.Adapter.ToolsMarketTrandStockAdapter.progress;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ASM;

public class ToolFragment extends Fragment {

    public static Activity activity;
    public static RecyclerView toolsTrandMarketStockRcyer;
    public static ArrayList<String> timageurls;
    public static ArrayList<String> timagename;
    public static String[] timagenames;
    public static String[] timageurlss;
    public static Object[] objectList = null;
    public static Object[] objectList2 = null;
    public static int tcounts = 1;
    public static int tnegotiationLeadCount = 0;
    public static int tpageItem = 100;
    public static String tpage_no = "1";
    public static ToolsMarketTrandStockAdapter toolsMarketTrandStockAdapter;
    private static List<ToolsModel> stock = new ArrayList<>();
    public static SwipeRefreshLayout swipeRefreshLayout;
    public static LinearLayout tool_headerdata;
    public static TextView nostock;
    LinearLayout stock_sort, tools_filter;
    public boolean Flag1 = false;
    public static ImageView tools_filter_icon;
    //reena
    private StockRequest mStockRequest;
    private Where mWhere;
    public static int pageItem = 100;
    public static String page_no = "1";
    //end
    //Views
    ImageView priceimg;
    ImageView postedimg;
    ImageView kmsimg;
    ImageView yearimg;

    RelativeLayout pricesort;
    RelativeLayout postdatesort;
    RelativeLayout kmssport;
    RelativeLayout yearsort;

    public boolean priceFlag = false;
    public boolean posteddateFlag = false;
    public boolean kmsFlag = false;
    public boolean yearFlag = false;

    public int tpostionCur = 0;

    public static boolean oneTimeSearchTools = true;
    public static boolean dealer_price = false;
    String TAG = getClass().getSimpleName();
    public ToolFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.tool_fragment, container, false);
        activity = getActivity();
        //remove bell
        setHasOptionsMenu(true);
        SplashActivity.progress = true;
        timageurls = new ArrayList<String>();
        timagename = new ArrayList<String>();
        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ASM)) {
            linearLayoutVisible.setVisibility(View.VISIBLE);
            searchicon.setVisibility(View.VISIBLE);
            searchImage.setVisibility(View.VISIBLE);
        }
            if (tv_header_title != null) {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Tools");
                tv_header_title.setTextColor(getResources().getColor(R.color.white));

        }

        CommonMethods.setvalueAgainstKey(getActivity(), "status", "Tools");
        //reena for adapter change
        CommonMethods.setvalueAgainstKey(activity, "tstockstore_load", "false");
        toolsTrandMarketStockRcyer = view.findViewById(R.id.tools_recycler);

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        stock_sort = view.findViewById(R.id.stock_sort);
        tools_filter = view.findViewById(R.id.tools_filter);
        tool_headerdata = view.findViewById(R.id.tool_headerdata);
        swipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.CYAN);
        nostock = view.findViewById(R.id.tools_noresult);
        tools_filter_icon = view.findViewById(R.id.tools_filter_icon);


        /*toolsTrandMarketStockRcyer.setHasFixedSize(true);
        toolsMarketTrandStockAdapter = new ToolsMarketTrandStockAdapter(activity, stock);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(activity);
        toolsTrandMarketStockRcyer.setLayoutManager(mLayoutManager);
        //stockStoreRecy.addItemDecoration(new DividerItemDecoration(activity, LinearLayoutManager.VERTICAL));
        toolsTrandMarketStockRcyer.setItemAnimator(new DefaultItemAnimator());
        toolsTrandMarketStockRcyer.setAdapter(toolsMarketTrandStockAdapter);*/

        if (CommonMethods.getstringvaluefromkey(activity, "tcertifiedval").equalsIgnoreCase("true")
                || CommonMethods.getstringvaluefromkey(activity, "tselling").equalsIgnoreCase("true")
                || CommonMethods.getstringvaluefromkey(activity, "tdealer").equalsIgnoreCase("true")
                || CommonMethods.getstringvaluefromkey(activity, "kmsval").equalsIgnoreCase("true")
                || CommonMethods.getstringvaluefromkey(activity, "tyear").equalsIgnoreCase("true")
                || CommonMethods.getstringvaluefromkey(activity, "tkms").equalsIgnoreCase("true")
                || CommonMethods.getstringvaluefromkey(activity, "tcertifiedstatus").equalsIgnoreCase("certifiedcar")
                ) {

            Log.e("tcertifiedval", "tcertifiedval=0" + CommonMethods.getstringvaluefromkey(activity, "tcertifiedval").toString());

            if (CommonMethods.getstringvaluefromkey(activity, "tcertifiedval").equalsIgnoreCase("true")) {
                tools_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);
            }

            if (yearValues.equalsIgnoreCase("2002")
                    && kilometerValues.equalsIgnoreCase("200000")
                    && startPriceValues.equalsIgnoreCase("0")
                    && endPriceValues.equalsIgnoreCase("10000000")
                    && dealer_startPriceValues.equalsIgnoreCase("0")
                    && dealer_endPriceValues.equalsIgnoreCase("10000000")) {
                if (CommonMethods.getstringvaluefromkey(activity, "tcertifiedval").equalsIgnoreCase("true")) {
                    tools_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);
                }
            } else {
                tools_filter_icon.setBackgroundResource(R.drawable.filter_48x18);
                Log.e("filter_icon_yellow", "filter_icon_yellow=1");
            }

        } else {
            tools_filter_icon.setBackgroundResource(R.drawable.filter_48x18);
            Log.e("filter_icon_yellow", "filter_icon_yellow=2");

        }
        Log.e("tcertifiedval", "tcertifiedval=1" + CommonMethods.getstringvaluefromkey(activity, "tcertifiedval").toString());

        if (CommonMethods.getstringvaluefromkey(activity, "tcertifiedstatus").equalsIgnoreCase("certifiedcar")) {
            tools_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                SplashActivity.progress = false;
                if (CommonMethods.isInternetWorking(activity)) {
                    tsortbyapply();

                }
            }
        });

        toolsTrandMarketStockRcyer.setOnScrollListener(new RecyclerView.OnScrollListener() {

            int verticalOffset;

            boolean scrollingUp;

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {

                }
            }

            @Override
            public final void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                verticalOffset += dy;
                scrollingUp = dy > 0;
                if (scrollingUp) {

                    if (Flag1) {
                        MainActivity.bottom_topAnimation();
                        Flag1 = false;
                    }

                } else {
                    if (!Flag1) {
                        MainActivity.top_bottomAnimation();
                        Flag1 = true;
                    }
                }
            }
        });

        stock_sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.tool_shortby, GlobalText.android);

                SortCustomDialogClass sortCustomDialogClass = new SortCustomDialogClass(getContext());
                sortCustomDialogClass.show();
            }
        });

        tools_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.tool_filter, GlobalText.android);
                Intent intent = new Intent(getActivity(), ToolsFilterActivity.class);
                startActivity(intent);
            }
        });

        //prepareStockRequestModel();
        //getStockDeatils(getContext(),mStockRequest);

        WebServicesCall.webCall(activity, activity, jsonMake1(), "ToolsStockStore", GlobalText.POST);

        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
       /* getMenuInflater().inflate(R.menu.menu_item, menu);
        MenuItem item = menu.findItem(R.id.share);
        item.setVisible(true);*/
        final MenuItem item = menu.findItem(R.id.notification_bell);
        item.setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);

    }


    public static void loadmore(int count) {


        // SplashActivity.progress = false;
        tcounts = tcounts + 1;

        double maxPageNumber = Math.ceil((double) (tnegotiationLeadCount) / (double) tpageItem); // d = 3.0
        if (tcounts > maxPageNumber) {
            //  progress.setVisibility(View.GONE);
            return;
        }

        tpage_no = Integer.toString(tcounts);
        CommonMethods.setvalueAgainstKey(activity, "tstockstore_load", "true");

        if (CommonMethods.isInternetWorking(activity)) {

            WebServicesCall.webCall(activity, activity, jsonMake1(), "ToolsStockStore", GlobalText.POST);

        }

    }


    public static JSONObject jsonMake1() {

        Log.e("tsortbystatus ", "tsortbystatus " + CommonMethods.getstringvaluefromkey(activity, "tsortbystatus"));

        JSONObject jObj = new JSONObject();

        JSONObject jObj1 = new JSONObject();
        JSONObject jObj2 = new JSONObject();
        JSONObject jObj3 = new JSONObject();
        JSONObject jObj4 = new JSONObject();

        JSONObject jObj5 = new JSONObject();
        JSONObject jObj6 = new JSONObject();


        JSONObject jObj7 = new JSONObject();
        JSONObject jObj8 = new JSONObject();
        JSONArray jsonArray = new JSONArray();


        JSONObject jObjdeal = new JSONObject();
        JSONArray dealerArray = new JSONArray();
        JSONArray dealerwherein = new JSONArray();

        JSONObject jObjmake = new JSONObject();
        JSONObject jObjmodel = new JSONObject();
        JSONObject jObjvariant = new JSONObject();
        JSONObject jObjregno = new JSONObject();
        JSONArray jsonSearchVal = new JSONArray();

        String array = CommonMethods.getstringvaluefromkey(activity, "dealers_in_state");
        String replace1 = array.replace("[", "");
        String replace2 = replace1.replace("]", "");
        String listarray = replace2.replaceAll("\"", "");
        Log.e("Parsetoolsstockstore ", "array " + listarray);
        //dealerArray.put(listarray);
        String[] splitdealerCode = listarray.split(",", 100);

        for (int i = 0; i < splitdealerCode.length; i++) {
            dealerArray.put(splitdealerCode[i]);
        }

        try {
            jObj.put("Page", tpage_no);
            jObj.put("PageItems", Integer.toString(tpageItem));

            if (CommonMethods.getstringvaluefromkey(activity, "tsortbystatus").equalsIgnoreCase("pricelowhigh")) {
                jObj.put("OrderBy", "selling_price");
                jObj.put("OrderByReverse", "false");
            } else if (CommonMethods.getstringvaluefromkey(activity, "tsortbystatus").equalsIgnoreCase("pricehighlow")) {
                jObj.put("OrderBy", "selling_price");
                jObj.put("OrderByReverse", "true");
            } else if (CommonMethods.getstringvaluefromkey(activity, "tsortbystatus").equalsIgnoreCase("postoldtonew")) {
                jObj.put("OrderBy", "dealer_price");
                jObj.put("OrderByReverse", "false");
            } else if (CommonMethods.getstringvaluefromkey(activity, "tsortbystatus").equalsIgnoreCase("postnewtoold")) {
                jObj.put("OrderBy", "dealer_price");
                jObj.put("OrderByReverse", "true");
            } else if (CommonMethods.getstringvaluefromkey(activity, "tsortbystatus").equalsIgnoreCase("kmshightolow")) {
                jObj.put("OrderBy", "kilometer");
                jObj.put("OrderByReverse", "true");
            } else if (CommonMethods.getstringvaluefromkey(activity, "tsortbystatus").equalsIgnoreCase("kmslowtohigh")) {
                jObj.put("OrderBy", "kilometer");
                jObj.put("OrderByReverse", "false");
            } else if (CommonMethods.getstringvaluefromkey(activity, "tsortbystatus").equalsIgnoreCase("yearnewtoold")) {
                jObj.put("OrderBy", "manufacture_year");
                jObj.put("OrderByReverse", "false");
            } else if (CommonMethods.getstringvaluefromkey(activity, "tsortbystatus").equalsIgnoreCase("yearoldtonew")) {
                jObj.put("OrderBy", "manufacture_year");
                jObj.put("OrderByReverse", "true");
            } else {
                jObj.put("OrderBy", "posted_date");
                jObj.put("OrderByReverse", "true");
            }

            jObjdeal.put("column", "dealer_code");
            jObjdeal.put("values", dealerArray);

            dealerwherein.put(jObjdeal);

            jObj.put("wherein", dealerwherein);


            if (MainActivity.postdatelessthirty) {

                jObj7.put("column", "posted_date");
                jObj7.put("operator", "<=");
                jObj7.put("value",
                        CalendarTodayDate());
                jsonArray.put(jObj7);
                MainActivity.postdatelessthirty = false;
                lastmonth = "";
            }


            if (ToolsFilterInstance.getInstance().getToolsfilterby().equals("tfilterapply")) {

                jObj1.put("column", "selling_price");
                jObj1.put("operator", ">=");
                jObj1.put("value", ToolsFilterActivity.startPriceValues.toString());

                jObj2.put("column", "selling_price");
                jObj2.put("operator", "<=");
                jObj2.put("value", ToolsFilterActivity.endPriceValues.toString());


                jObj5.put("column", "dealer_price");
                jObj5.put("operator", ">=");
                jObj5.put("value", dealer_startPriceValues.toString());

                jObj6.put("column", "dealer_price");
                jObj6.put("operator", "<=");
                jObj6.put("value", dealer_endPriceValues.toString());

                jObj3.put("column", "kilometer");
                jObj3.put("operator", "<=");
                jObj3.put("value", ToolsFilterActivity.kilometerValues.toString());

                jObj4.put("column", "manufacture_year");
                jObj4.put("operator", ">=");
                jObj4.put("value", ToolsFilterActivity.yearValues.toString());

                jsonArray.put(jObj1);
                jsonArray.put(jObj2);
                jsonArray.put(jObj3);
                jsonArray.put(jObj4);

                jsonArray.put(jObj5);
                jsonArray.put(jObj6);
            }

            if (CommonMethods.getstringvaluefromkey(activity, "tcertifiedstatus").equalsIgnoreCase("certifiedcar")) {

                jObj8.put("column", "is_certified");
                jObj8.put("operator", "=");
                jObj8.put("value", "true");
                jsonArray.put(jObj8);
            }

            if (!searchVal.equals("")) {
                jObjmake.put("column", "vehicle_make");
                jObjmake.put("operator", "like");
                jObjmake.put("value", searchVal.trim());
                jsonSearchVal.put(jObjmake);

                jObjmodel.put("column", "vehicle_model");
                jObjmodel.put("operator", "like");
                jObjmodel.put("value", searchVal.trim());
                jsonSearchVal.put(jObjmodel);

                jObjvariant.put("column", "vehicle_variant");
                jObjvariant.put("operator", "like");
                jObjvariant.put("value", searchVal.trim());
                jsonSearchVal.put(jObjvariant);

                jObjregno.put("column", "registration_number");
                jObjregno.put("operator", "like");
                jObjregno.put("value", "%" + searchVal.trim() + "%");
                jsonSearchVal.put(jObjregno);


            }
            jObj.put("where_or", jsonSearchVal);

            jObj.put("where", jsonArray);

            //     Log.e("Requesttoolsstockstore ", "input " + jObj.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jObj;
    }

    public static String lastmonth = "";

    public static String CalendarTodayDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -30);

        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        lastmonth = s.format(new Date(cal.getTimeInMillis()));
        return lastmonth;
    }

    public static void Parsetoolsstockstore(JSONObject jObj, String strMethod) {
        //   Log.e("Parsetoolsstockstore ", "tools " + jObj.toString());
/*
        if(stock!=null)
        {
            stock.clear();
        }
*/
        CommonMethods.setvalueAgainstKey(activity, "tstatus", "StockStore");

        if (CommonMethods.getstringvaluefromkey(activity, "tsortby").equalsIgnoreCase("true")) {

            if (CommonMethods.getstringvaluefromkey(activity, "tstockstore_load").equalsIgnoreCase("true")) {
                if (toolsMarketTrandStockAdapter != null)
                    toolsMarketTrandStockAdapter.notifyItemInserted(stock.size() - 1);
                // Log.e("loading=", "Loading=" + (stock.size()));
            }
        }
        if (CommonMethods.getstringvaluefromkey(activity, "tstockstore_load").equalsIgnoreCase("false")) {
            stock = new ArrayList<>();
        } else {
            if ((stock.size() - 1) > 0) {
                // myJobList.remove(myJobList.size()-1);
            }
        }

        try {

            // stock = new ArrayList<>();
            JSONArray arrayList = jObj.getJSONArray("data");
            // Log.e("arrayList", "arrayList=" + arrayList.length());

            if (arrayList.length() == 0) {
                //  swipeRefreshLayout.setRefreshing(false);
                swipeRefreshLayout.setVisibility(View.GONE);
                nostock.setVisibility(View.VISIBLE);
            } else {
                swipeRefreshLayout.setVisibility(View.VISIBLE);
                nostock.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                for (int i = 0; i < arrayList.length(); i++) {
                    JSONObject jsonobject = new JSONObject(
                            arrayList.getString(i));

                    ToolsModel stockModel = new ToolsModel();

                    stockModel.setSource(jsonobject.getString("stock_source"));
                    stockModel.setId(jsonobject.getInt("stock_id"));
                    stockModel.setFeaturedCarText("");
                    stockModel.setPostedDate(jsonobject.getString("posted_date"));
                    // stockModel.setStockAge(jsonobject.getInt("stock_age"));//
                    stockModel.setMake(jsonobject.getString("vehicle_make"));
                    stockModel.setModel(jsonobject.getString("vehicle_model"));
                    stockModel.setVariant(jsonobject.getString("vehicle_variant"));
                    stockModel.setRegMonth(jsonobject.getString("reg_month"));
                    stockModel.setRegYear(jsonobject.getInt("reg_year"));

                    stockModel.setRegistraionCity(jsonobject.getString("registraion_city"));
                    stockModel.setRegistrationNumber(jsonobject.getString("registration_number"));
                    stockModel.setColour(jsonobject.getString("colour"));
                    stockModel.setKilometer(jsonobject.getInt("kilometer"));
                    stockModel.setOwner(jsonobject.getInt("owner"));
                    stockModel.setInsurance(jsonobject.getString("insurance"));
                    stockModel.setInsuranceExpDate(jsonobject.getString("insurance_exp_date"));
                    stockModel.setSellingPrice(jsonobject.getInt("selling_price"));
                    stockModel.setCertifiedText(jsonobject.getString("CertifiedText"));
                    stockModel.setCertificationNumber(jsonobject.getString("certification_number"));
                    stockModel.setWarranty(jsonobject.getString("warranty_recommended"));

                    stockModel.setPhotoCount(jsonobject.getInt("photo_count"));
                    //stockModel.setPhotoCount(1);

                    stockModel.setSurveyorCode(jsonobject.getString("surveyor_code"));
                    stockModel.setSurveyorModifiedDate(jsonobject.getString("surveyor_modified_date"));
                    stockModel.setSurveyorKilometer(jsonobject.getString("surveyor_kilometer"));
                    stockModel.setSurveyorRemark(jsonobject.getString("surveyor_remark"));
                    stockModel.setDealerCode(jsonobject.getString("dealer_code"));
                    stockModel.setIsbooked(jsonobject.getString("is_booked"));
                    // stockModel.setIssold(jsonobject.getString("is_sold"));
                    //  stockModel.setIsDisplay(jsonobject.getString("is_display"));
                    stockModel.setIsOffload(jsonobject.getString("is_offload"));
                    stockModel.setFuel_type(jsonobject.getString("fuel_type"));
                    stockModel.setBought(jsonobject.getString("bought_price"));
                    stockModel.setRefurbishment_cost(jsonobject.getString("refurbishment_cost"));
                    stockModel.setDealer_price(jsonobject.getString("dealer_price"));
                    stockModel.setProcurementExecId(jsonobject.getString("procurement_executive_id"));
                    stockModel.setCng_kit(jsonobject.getString("cng_kit"));
                    stockModel.setChassis_number(jsonobject.getString("chassis_number"));
                    stockModel.setEngine_number(jsonobject.getString("engine_number"));

                    stockModel.setIs_certified(jsonobject.getString("is_certified"));
                    stockModel.setIs_featured_car(jsonobject.getString("is_featured_car"));
                    //stockModel.setIs_featured_car_admin(jsonobject.getString("is_featured_car_admin"));
                    stockModel.setProcurement_executive_name(jsonobject.getString("procurement_executive_name"));
                    stockModel.setComments(jsonobject.getString("comments"));
                    stockModel.setFinance_required(jsonobject.getString("finance_required"));
                    stockModel.setManufacture_month(jsonobject.getString("manufacture_month"));
                    stockModel.setSales_executive_id(jsonobject.getString("sales_executive_id"));
                    stockModel.setSales_executive_name(jsonobject.getString("sales_executive_name"));
                    stockModel.setManufacture_year(jsonobject.getString("manufacture_year"));
                    stockModel.setActual_selling_price(jsonobject.getString("actual_selling_price"));
                    stockModel.setCertifiedText(jsonobject.getString("CertifiedText"));
                    //  stockModel.setIsDisplay(jsonobject.getString("IsDisplay"));
                    // stockModel.setTransmissionType(jsonobject.getString("TransmissionType"));
                    stockModel.setPrivate_vehicle(jsonobject.getString("private_vehicle"));
                    stockModel.setImageUrl(jsonobject.getJSONArray("images"));

                    final JSONArray images = jsonobject.getJSONArray("images");

                    if (images != null && images.length() > 0) {

                        if (timageurls != null) {
                            timageurls.clear();
                        }
                        if (timagename != null) {
                            timagename.clear();
                        }
                        if (objectList != null) {
                            objectList = null;
                        }
                        if (objectList2 != null) {
                            objectList2 = null;
                        }
                        if (timageurlss != null) {
                            timageurlss = null;
                        }
                        if (timagenames != null) {
                            timagenames = null;
                        }


                        for (int k = 0; k < images.length(); k++) {
                            try {
                                JSONObject data = images.getJSONObject(k);
                                timageurls.add(data.getString("url"));
                                timagename.add(data.getString("category_identifier"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        objectList = timageurls.toArray();
                        timageurlss = Arrays.copyOf(objectList, objectList.length, String[].class);

                        objectList2 = timagename.toArray();
                        timagenames = Arrays.copyOf(objectList2, objectList2.length, String[].class);

                        stockModel.setCoverimage(timageurlss[0]);
                        for (int j = 0; j < timagenames.length; j++) {

                            if (timagenames[j].equalsIgnoreCase("cover_image"))
                                stockModel.setCoverimage(timageurlss[j]);
                        }

                    } else {
                        stockModel.setCoverimage("");
                    }

                    tnegotiationLeadCount = Integer.parseInt(jObj.getString("total").toString());
                    stock.add(stockModel);

                }
            }
        } catch (Exception ex) {
            Log.e("Parsestockstore-Ex", ex.toString());
        }

        if (CommonMethods.getstringvaluefromkey(activity, "tstockstore_load").equalsIgnoreCase("true")) {
            if (toolsMarketTrandStockAdapter != null)
                toolsMarketTrandStockAdapter.notifyDataChanged();

            if (progress.getVisibility() == View.VISIBLE)
                progress.setVisibility(View.GONE);

            Log.e("notifyDataChanged", "notifyDataChanged=" + "coming");

        } else {
            Log.e("reloadData", "reloadData=" + "coming");
            reloadData();
        }

        SplashActivity.progress = true;
        // swipeRefreshLayout.setRefreshing(false);

    }

    public static void reloadData() {
        toolsMarketTrandStockAdapter = new ToolsMarketTrandStockAdapter(activity, stock);
        toolsTrandMarketStockRcyer.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(activity);
        toolsTrandMarketStockRcyer.setLayoutManager(mLayoutManager);
        toolsTrandMarketStockRcyer.setItemAnimator(new DefaultItemAnimator());
        toolsTrandMarketStockRcyer.setAdapter(toolsMarketTrandStockAdapter);
        CommonMethods.setvalueAgainstKey(activity, "tsortby", "false");

    }

    public static void tsortbyapply() {

        tpage_no = "1";
        tcounts = 1;
        Log.e("store sortby...", "store sortby...1");

        CommonMethods.setvalueAgainstKey(activity, "tsortby", "true");

        CommonMethods.setvalueAgainstKey(activity, "tstockstore_load", "false");
        CommonMethods.setvalueAgainstKey(activity, "stockstore_load", "false");
        WebServicesCall.webCall(activity, activity, jsonMake1(), "ToolsStockStore", GlobalText.POST);
    }

    public class SortCustomDialogClass extends Dialog implements View.OnClickListener {

        public Context activity;

        public Activity act;
        public Dialog d;
        public Button yes;
        public TextView pricelbl, posteddatelbl, kmslbl, yearlbl;


        public SortCustomDialogClass(@NonNull Context context) {
            super(context);
            this.activity = context;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.stock_sort_apply:
                    Log.e("tpostionCur", "tpostionCur=" + tpostionCur);
                    Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"), GlobalText.tool_apply_shortby, GlobalText.android);

                    tsortbyapply();

                    dismiss();

                    break;
                default:
                    break;
            }
            dismiss();
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            act = getActivity();
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.tools_sort_dialog);
            yes = findViewById(R.id.stock_sort_apply);
            yes.setOnClickListener(this);

            pricesort = findViewById(R.id.pricesort);
            postdatesort = findViewById(R.id.postdatesort);
            kmssport = findViewById(R.id.kmssport);
            yearsort = findViewById(R.id.yearsort);

            pricelbl = findViewById(R.id.pricelbl);
            posteddatelbl = findViewById(R.id.posteddatelbl);
            kmslbl = findViewById(R.id.kmslbl);
            yearlbl = findViewById(R.id.yearlbl);

            priceimg = findViewById(R.id.priceimg);
            postedimg = findViewById(R.id.postedimg);
            kmsimg = findViewById(R.id.kmsimg);
            yearimg = findViewById(R.id.yearimg);

            priceimg.setVisibility(View.INVISIBLE);
            postedimg.setVisibility(View.INVISIBLE);
            kmsimg.setVisibility(View.INVISIBLE);
            yearimg.setVisibility(View.VISIBLE);
            yearFlag = true;
            yearlbl.setText("Year(Latest-Old)");

            Log.e("sortbystatus", "sortbystatus=1");

            if (CommonMethods.getstringvaluefromkey(act, "tstatus").equalsIgnoreCase("StockStore")) {
                Log.e("sortbystatus", "sortbystatus=2");

                if (CommonMethods.getstringvaluefromkey(act, "tsortbystatus").equalsIgnoreCase("pricelowhigh")) {

                    priceimg.setVisibility(View.VISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);
                    pricelbl.setText("Selling Price(Low-High)");
                    posteddatelbl.setText("Dealer Price");
                    kmslbl.setText("Kms");
                    yearlbl.setText("Year");
                    priceimg.animate().rotation(0).setDuration(180).start();
                    Log.e("sortbystatus", "sortbystatus=3");
                    priceFlag = false;


                } else if (CommonMethods.getstringvaluefromkey(act, "tsortbystatus").equalsIgnoreCase("pricehighlow")) {
                    priceFlag = true;
                    priceimg.setVisibility(View.VISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);
                    pricelbl.setText("Selling Price(High-Low)");
                    posteddatelbl.setText("Dealer Price");
                    kmslbl.setText("Kms");
                    yearlbl.setText("Year");
                    priceimg.animate().rotation(180).setDuration(180).start();
                    Log.e("sortbystatus", "sortbystatus=4");

                } else if (CommonMethods.getstringvaluefromkey(act, "tsortbystatus").equalsIgnoreCase("postoldtonew")) {

                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.VISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);
                    Log.e("sortbystatus", "sortbystatus=5");
                    posteddatelbl.setText("Dealer Price(Low-High)");
                    pricelbl.setText("Selling Price");
                    kmslbl.setText("Kms");
                    yearlbl.setText("Year");
                    posteddateFlag = false;

                    postedimg.animate().rotation(0).setDuration(180).start();

                } else if (CommonMethods.getstringvaluefromkey(act, "tsortbystatus").equalsIgnoreCase("postnewtoold")) {
                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.VISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);
                    posteddatelbl.setText("Dealer Price(High-Low)");
                    pricelbl.setText("Selling Price");
                    kmslbl.setText("Kms");
                    yearlbl.setText("Year");
                    posteddateFlag = true;

                    Log.e("sortbystatus", "sortbystatus=6");

                    postedimg.animate().rotation(180).setDuration(180).start();

                } else if (CommonMethods.getstringvaluefromkey(act, "tsortbystatus").equalsIgnoreCase("kmshightolow")) {

                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.VISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);
                    kmsFlag = true;
                    kmslbl.setText("Kms(High-Low)");
                    posteddatelbl.setText("Dealer Price");
                    pricelbl.setText("Selling Price");
                    yearlbl.setText("Year");
                    Log.e("sortbystatus", "sortbystatus=7");

                    kmsimg.animate().rotation(180).setDuration(180).start();
                } else if (CommonMethods.getstringvaluefromkey(act, "tsortbystatus").equalsIgnoreCase("kmslowtohigh")) {
                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.VISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);
                    kmslbl.setText("Kms(Low-High)");
                    posteddatelbl.setText("Dealer Price");
                    pricelbl.setText("Selling Price");
                    yearlbl.setText("Year");
                    Log.e("sortbystatus", "sortbystatus=8");

                    kmsimg.animate().rotation(0).setDuration(180).start();
                    kmsFlag = false;

                } else if (CommonMethods.getstringvaluefromkey(act, "tsortbystatus").equalsIgnoreCase("yearnewtoold")) {

                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.VISIBLE);
                    yearFlag = true;
                    Log.e("sortbystatus", "sortbystatus=9");
                    yearlbl.setText("Year(Old-New)");
                    kmslbl.setText("Kms");
                    posteddatelbl.setText("Dealer Price");
                    pricelbl.setText("Selling Price");

                    yearimg.animate().rotation(0).setDuration(180).start();

                } else if (CommonMethods.getstringvaluefromkey(act, "tsortbystatus").equalsIgnoreCase("yearoldtonew")) {
                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.VISIBLE);
                    yearFlag = false;
                    yearlbl.setText("Year(New-Old)");
                    kmslbl.setText("Kms");
                    posteddatelbl.setText("Dealer Price");
                    pricelbl.setText("Selling Price");
                    Log.e("sortbystatus", "sortbystatus=10");

                    yearimg.animate().rotation(180).setDuration(180).start();

                } else {
                    Log.e("sortbystatus", "sortbystatus=11");

                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);
                    posteddatelbl.setText("Dealer Price");
                    kmslbl.setText("Kms");
                    pricelbl.setText("Selling Price");
                    yearlbl.setText("Year");

                    postedimg.animate().rotation(180).setDuration(180).start();
                    posteddateFlag = false;
                }
            } else {

                if (CommonMethods.getstringvaluefromkey(act, "tsortbystatusbook").equalsIgnoreCase("pricelowhigh")) {
                    priceimg.setVisibility(View.VISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);
                    pricelbl.setText("Selling Price(Low-High)");
                    kmslbl.setText("Kms");
                    yearlbl.setText("Year");
                    posteddatelbl.setText("Dealer Price");
                    priceimg.animate().rotation(0).setDuration(180).start();

                    Log.e("sortbystatus", "sortbystatus=3");
                    priceFlag = false;

                } else if (CommonMethods.getstringvaluefromkey(act, "tsortbystatusbook").equalsIgnoreCase("pricehighlow")) {
                    priceFlag = true;
                    priceimg.setVisibility(View.VISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);
                    pricelbl.setText("Selling Price(High-Low)");
                    kmslbl.setText("Kms");
                    yearlbl.setText("Year");
                    posteddatelbl.setText("Dealer Price");
                    priceimg.animate().rotation(180).setDuration(180).start();
                    Log.e("sortbystatus", "sortbystatus=4");

                } else if (CommonMethods.getstringvaluefromkey(act, "tsortbystatusbook").equalsIgnoreCase("postoldtonew")) {

                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);
                    Log.e("sortbystatus", "sortbystatus=5");
                    posteddatelbl.setText("Dealer Price(Low-High)");
                    kmslbl.setText("Kms");
                    yearlbl.setText("Year");
                    pricelbl.setText("Selling Price");

                    posteddateFlag = false;
                    postedimg.animate().rotation(0).setDuration(180).start();

                } else if (CommonMethods.getstringvaluefromkey(act, "tsortbystatusbook").equalsIgnoreCase("postnewtoold")) {
                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);
                    posteddatelbl.setText("Dealer Price(High-Low)");
                    kmslbl.setText("Kms");
                    yearlbl.setText("Year");
                    pricelbl.setText("Selling Price");
                    posteddateFlag = true;

                    Log.e("sortbystatus", "sortbystatus=6");
                    postedimg.animate().rotation(180).setDuration(180).start();

                } else if (CommonMethods.getstringvaluefromkey(act, "tsortbystatusbook").equalsIgnoreCase("kmshightolow")) {

                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.VISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);
                    kmsFlag = true;
                    kmslbl.setText("Kms(High-Low)");
                    posteddatelbl.setText("Dealer Price");
                    yearlbl.setText("Year");
                    pricelbl.setText("Selling Price");
                    Log.e("sortbystatus", "sortbystatus=7");
                    kmsimg.animate().rotation(180).setDuration(180).start();

                } else if (CommonMethods.getstringvaluefromkey(act, "tsortbystatusbook").equalsIgnoreCase("kmslowtohigh")) {
                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.VISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);
                    kmslbl.setText("Kms(Low-High)");
                    posteddatelbl.setText("Dealer Price");
                    yearlbl.setText("Year");
                    pricelbl.setText("Selling Price");
                    Log.e("sortbystatus", "sortbystatus=8");

                    kmsimg.animate().rotation(0).setDuration(180).start();
                    kmsFlag = false;

                } else if (CommonMethods.getstringvaluefromkey(act, "tsortbystatusbook").equalsIgnoreCase("yearnewtoold")) {

                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.VISIBLE);
                    yearFlag = false;
                    Log.e("sortbystatus", "sortbystatus=9");
                    yearlbl.setText("Year(Old-New)");
                    posteddatelbl.setText("Dealer Price");
                    pricelbl.setText("Selling Price");
                    kmslbl.setText("Kms");

                    yearimg.animate().rotation(0).setDuration(180).start();

                } else if (CommonMethods.getstringvaluefromkey(act, "tsortbystatusbook").equalsIgnoreCase("yearoldtonew")) {
                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.VISIBLE);
                    yearFlag = false;
                    yearlbl.setText("Year(New-Old)");
                    posteddatelbl.setText("Dealer Price");
                    pricelbl.setText("Selling Price");
                    kmslbl.setText("Kms");

                    Log.e("sortbystatus", "sortbystatus=10");

                    yearimg.animate().rotation(180).setDuration(180).start();

                } else {
                    Log.e("sortbystatus", "sortbystatus=11");

                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);
                    posteddatelbl.setText("Dealer Price");
                    pricelbl.setText("Selling Price");
                    kmslbl.setText("Kms");
                    yearlbl.setText("Year");

                    postedimg.animate().rotation(180).setDuration(180).start();
                    posteddateFlag = false;
                }
            }


            pricesort.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    priceimg.setVisibility(View.VISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);

                    if (priceFlag) {
                        priceFlag = false;
                        pricelbl.setText("Selling Price(Low-High)");
                        posteddatelbl.setText("Dealer Price");
                        kmslbl.setText("Kms");
                        yearlbl.setText("Year");
                        if (CommonMethods.getstringvaluefromkey(act, "tstatus").equalsIgnoreCase("StockStore")) {
                            CommonMethods.setvalueAgainstKey(act, "tsortbystatus", "pricelowhigh");
                        } else {
                            CommonMethods.setvalueAgainstKey(act, "tsortbystatusbook", "pricelowhigh");
                        }
                        priceimg.animate().rotation(0).setDuration(180).start();
                    } else {
                        pricelbl.setText("Selling Price(High-Low)");
                        posteddatelbl.setText("Dealer Price");
                        kmslbl.setText("Kms");
                        yearlbl.setText("Year");
                        priceimg.animate().rotation(180).setDuration(180).start();
                        if (CommonMethods.getstringvaluefromkey(act, "tstatus").equalsIgnoreCase("StockStore")) {

                            CommonMethods.setvalueAgainstKey(act, "tsortbystatus", "pricehighlow");
                        } else {
                            CommonMethods.setvalueAgainstKey(act, "tsortbystatusbook", "pricehighlow");
                        }

                        priceFlag = true;
                    }
                }
            });


            postdatesort.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.VISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);

                    if (posteddateFlag) {
                        posteddateFlag = false;
                        posteddatelbl.setText("Dealer Price(Low-High)");
                        pricelbl.setText("Selling Price");
                        pricelbl.setText("Selling Price");
                        kmslbl.setText("Kms");
                        yearlbl.setText("Year");
                        postedimg.animate().rotation(0).setDuration(180).start();

                        if (CommonMethods.getstringvaluefromkey(act, "tstatus").equalsIgnoreCase("StockStore")) {

                            CommonMethods.setvalueAgainstKey(act, "tsortbystatus", "postoldtonew");
                        } else {
                            CommonMethods.setvalueAgainstKey(act, "tsortbystatusbook", "postoldtonew");
                        }
                    } else {
                        if (CommonMethods.getstringvaluefromkey(act, "tstatus").equalsIgnoreCase("StockStore")) {

                            CommonMethods.setvalueAgainstKey(act, "tsortbystatus", "postnewtoold");
                        } else {
                            CommonMethods.setvalueAgainstKey(act, "tsortbystatusbook", "postnewtoold");
                        }
                        posteddatelbl.setText("Dealer Price(High-Low)");
                        pricelbl.setText("Selling Price");
                        kmslbl.setText("Kms");
                        yearlbl.setText("Year");
                        postedimg.animate().rotation(180).setDuration(180).start();
                        posteddateFlag = true;
                    }
                }

            });


            kmssport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.VISIBLE);
                    yearimg.setVisibility(View.INVISIBLE);

                    if (kmsFlag) {
                        kmsFlag = false;
                        kmslbl.setText("Kms(Low-High)");
                        pricelbl.setText("Selling Price");
                        yearlbl.setText("Year");
                        posteddatelbl.setText("Dealer Price");
                        kmsimg.animate().rotation(0).setDuration(180).start();

                        if (CommonMethods.getstringvaluefromkey(act, "tstatus").equalsIgnoreCase("StockStore")) {

                            CommonMethods.setvalueAgainstKey(act, "tsortbystatus", "kmslowtohigh");
                        } else {
                            CommonMethods.setvalueAgainstKey(act, "tsortbystatusbook", "kmslowtohigh");
                        }
                    } else {
                        kmslbl.setText("Kms(High-Low)");
                        pricelbl.setText("Selling Price");
                        yearlbl.setText("Year");
                        posteddatelbl.setText("Dealer Price");
                        if (CommonMethods.getstringvaluefromkey(act, "tstatus").equalsIgnoreCase("StockStore")) {

                            CommonMethods.setvalueAgainstKey(act, "tsortbystatus", "kmshightolow");
                        } else {
                            CommonMethods.setvalueAgainstKey(act, "tsortbystatusbook", "kmshightolow");
                        }
                        kmsimg.animate().rotation(180).setDuration(180).start();
                        kmsFlag = true;
                    }

                }
            });
            yearsort.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    priceimg.setVisibility(View.INVISIBLE);
                    postedimg.setVisibility(View.INVISIBLE);
                    kmsimg.setVisibility(View.INVISIBLE);
                    yearimg.setVisibility(View.VISIBLE);

                    if (yearFlag) {
                        yearFlag = false;
                        yearlbl.setText("Year(New-Old)");
                        kmslbl.setText("Kms");
                        pricelbl.setText("Selling Price");
                        posteddatelbl.setText("Dealer Price");
                        yearimg.animate().rotation(180).setDuration(180).start();
                        if (CommonMethods.getstringvaluefromkey(act, "tstatus").equalsIgnoreCase("StockStore")) {

                            CommonMethods.setvalueAgainstKey(act, "tsortbystatus", "yearoldtonew");
                        } else {
                            CommonMethods.setvalueAgainstKey(act, "tsortbystatusbook", "yearoldtonew");
                        }
                    } else {
                        yearlbl.setText("Year(Old-New)");
                        kmslbl.setText("Kms");
                        pricelbl.setText("Selling Price");
                        posteddatelbl.setText("Dealer Price");
                        if (CommonMethods.getstringvaluefromkey(act, "tstatus").equalsIgnoreCase("StockStore")) {

                            CommonMethods.setvalueAgainstKey(act, "tsortbystatus", "yearnewtoold");
                        } else {
                            CommonMethods.setvalueAgainstKey(act, "tsortbystatusbook", "yearnewtoold");
                        }
                        yearimg.animate().rotation(0).setDuration(180).start();
                        yearFlag = true;
                    }

                }
            });

        }
    }

    @Override
    public void onResume() {
        super.onResume();

        Application.getInstance().trackScreenView(activity,GlobalText.ToolFragment_Fragment);

        if(CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(USERTYPE_ASM))
        {

            linearLayoutVisible.setVisibility(View.VISIBLE);
            searchicon.setVisibility(View.VISIBLE);
            searchImage.setVisibility(View.VISIBLE);
            toggle.setHomeAsUpIndicator(R.drawable.ic_new_hamburger_menu);
            toolbar.setBackgroundColor(getResources().getColor(R.color.black));
            if(tv_header_title!=null)
            {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Tools");
                tv_header_title.setTextColor(getResources().getColor(R.color.white));
            }
            Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_SALES_LEADS,CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));
        }

        tv_header_title.setText("Tools");
        if (ToolsFilterInstance.getInstance().getToolsfilterby().equals("tfilterapply")) {
            if (stock != null) {
                stock.clear();
                //   nostock.setVisibility(View.VISIBLE);
            } else {
                //  nostock.setVisibility(View.GONE);
            }
            WebServicesCall.webCall(activity, activity, jsonMake1(), "ToolsStockStore", GlobalText.POST);
        } else {
            ToolsFilterInstance.getInstance().setToolsfilterby("");
        }
    }


    public static void onSerarchResultUpdate(String query) {
        Log.e("ToolsQuery ", "Search " + query);
        toolsMarketTrandStockAdapter.filter(query);
        searchVal = query;
        if (searchVal.equals("")) {
            CommonMethods.setvalueAgainstKey(activity, "loadTools", "false");
            SearchingByToolsMarketStock();
        }

        if (stock.size() <= 2) {
            if (oneTimeSearchTools == true) {
                oneTimeSearchTools = false;
                CommonMethods.setvalueAgainstKey(activity, "loadTools", "false");
                SearchingByToolsMarketStock();
            }
        }
    }

    public static void SearchingByToolsMarketStock() {
        if (stock != null || !stock.isEmpty()) {
            stock.clear();
        }
        tcounts = 1;
        tnegotiationLeadCount = 0;
        if (!searchVal.equals("")) {
            //change in 10 to 100
            tpageItem = tpageItem + 100;
        } else {
            tpageItem = 100;
        }
        tpage_no = "1";

        WebServicesCall.webCall(activity, activity, jsonMake1(), "ToolsStockStore", GlobalText.POST);

    }

    private void prepareStockRequestModel() {
        mStockRequest = new StockRequest();
        mWhere = new Where();
        List<Where> mWhereList = new ArrayList<>();

        try {
            mStockRequest.setPage(page_no);
            mStockRequest.setPageItems(String.valueOf(pageItem));
            Log.e("Toolpage", String.valueOf(pageItem).toString());
            if (CommonMethods.getstringvaluefromkey(activity, "tsortbystatus").equalsIgnoreCase("pricelowhigh")) {
                mStockRequest.setOrderBy("selling_price");
                mStockRequest.setOrderByReverse("true");

            } else if (CommonMethods.getstringvaluefromkey(activity, "tsortbystatus").equalsIgnoreCase("pricehighlow")) {

                mStockRequest.setOrderBy("selling_price");
                mStockRequest.setOrderByReverse("false");
            } else if (CommonMethods.getstringvaluefromkey(activity, "tsortbystatus").equalsIgnoreCase("postoldtonew")) {

                mStockRequest.setOrderBy("posted_date");
                mStockRequest.setOrderByReverse("true");
            } else if (CommonMethods.getstringvaluefromkey(activity, "tsortbystatus").equalsIgnoreCase("postnewtoold")) {

                mStockRequest.setOrderBy("posted_date");
                mStockRequest.setOrderByReverse("false");
            } else if (CommonMethods.getstringvaluefromkey(activity, "tsortbystatus").equalsIgnoreCase("kmshightolow")) {

                mStockRequest.setOrderBy("kilometer");
                mStockRequest.setOrderByReverse("false");
            } else if (CommonMethods.getstringvaluefromkey(activity, "tsortbystatus").equalsIgnoreCase("kmslowtohigh")) {

                mStockRequest.setOrderBy("kilometer");
                mStockRequest.setOrderByReverse("true");
            } else if (CommonMethods.getstringvaluefromkey(activity, "tsortbystatus").equalsIgnoreCase("yearnewtoold")) {

                mStockRequest.setOrderBy("manufacture_year");
                mStockRequest.setOrderByReverse("false");
            } else if (CommonMethods.getstringvaluefromkey(activity, "tsortbystatus").equalsIgnoreCase("yearoldtonew")) {

                mStockRequest.setOrderBy("manufacture_year");
                mStockRequest.setOrderByReverse("true");
            } else {

                mStockRequest.setOrderBy("posted_date");
                mStockRequest.setOrderByReverse("true");
            }


            if (CommonMethods.getstringvaluefromkey(activity, "tfilterapply").equalsIgnoreCase("true")) {

                //Log.e("====","====="+StockFilterActivity.startPriceValues.toString());
                //Log.e("====","====="+StockFilterActivity.endPriceValues.toString());

                mWhere.setColumn("selling_price");
                mWhere.setOperator(">=");
                mWhere.setValue(StockFilterActivity.startPriceValues.toString());
                mWhereList.add(mWhere);


                mWhere.setColumn("selling_price");
                mWhere.setOperator("<=");
                mWhere.setValue(StockFilterActivity.endPriceValues.toString());
                mWhereList.add(mWhere);


                mWhere.setColumn("kilometer");
                mWhere.setOperator("<=");
                mWhere.setValue(StockFilterActivity.kilometerValues.toString());
                mWhereList.add(mWhere);


                mWhere.setColumn("manufacture_year");
                mWhere.setOperator(">=");
                mWhere.setValue(StockFilterActivity.yearValues.toString());
                mWhereList.add(mWhere);

                mStockRequest.setWhere(mWhereList);

                if (CommonMethods.getstringvaluefromkey(activity, "tcertifiedstatus").equalsIgnoreCase("certifiedcar")) {


                    mWhere.setColumn("is_certified");
                    mWhere.setOperator("=");
                    mWhere.setValue("true");
                    mWhereList.add(mWhere);
                }

                /*if (stock_photocountsFlag || CommonMethods.getstringvaluefromkey(activity, "Notification_Switch").equalsIgnoreCase("noStockImage")) {

                    mWhere.setColumn("photo_count");
                    mWhere.setOperator("<=");
                    mWhere.setValue("0");
                    mWhereList.add(mWhere);
                    //stock_photocountsFlag =false;
                    CommonMethods.setvalueAgainstKey(activity, "photocounts", "true");
                    CommonMethods.setvalueAgainstKey(activity, "Notification_Switch", "type");

                }*/

            }

           /* if (CommonMethods.getstringvaluefromkey(activity, "Notification_Switch").equalsIgnoreCase("noStockImage")
                    ||CommonMethods.getstringvaluefromkey(activity, "photocounts").equalsIgnoreCase("true")) {

                mWhere.setColumn("photo_count");
                mWhere.setOperator("<=");
                mWhere.setValue("0");
                mWhereList.add(mWhere);
                //stock_photocountsFlag =false;

                CommonMethods.setvalueAgainstKey(activity, "Notification_Switch", "type");
                CommonMethods.setvalueAgainstKey(activity, "photocounts", "true");
                stock_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);

            } else {
                CommonMethods.setvalueAgainstKey(activity, "photocounts", "false");

            }


            if (MainActivity.postdatelessthirty || CommonMethods.getstringvaluefromkey(activity, "thirtydaycheckbox").equalsIgnoreCase("true")) {

                mWhere.setColumn("posted_date");
                mWhere.setOperator("<");
                mWhere.setValue( CalendarTodayDate());
                mWhereList.add(mWhere);

                CommonMethods.setvalueAgainstKey(activity, "thirtydaycheckbox", "true");

                MainActivity.postdatelessthirty = false;
                lastmonth="";

                // MainActivity.postdatelessthirty = false;
                lastmonth = "";

            }*/

          /*  if (!searchVal.equals("")) {
                if (progress.getVisibility() == View.VISIBLE)
                    progress.setVisibility(View.GONE);

                jObjmake.put("column", "vehicle_make");
                jObjmake.put("operator", "like");
                jObjmake.put("value", searchVal);
                jsonSearchVal.put(jObjmake);

                jObjmodel.put("column", "vehicle_model");
                jObjmodel.put("operator", "like");
                jObjmodel.put("value", searchVal);
                jsonSearchVal.put(jObjmodel);

                jObjvariant.put("column", "vehicle_variant");
                jObjvariant.put("operator", "like");
                jObjvariant.put("value", searchVal);
                jsonSearchVal.put(jObjvariant);

                jObjregno.put("column", "registration_number");
                jObjregno.put("operator", "like");
                jObjregno.put("value", searchVal);
                jsonSearchVal.put(jObjregno);
                jObj.put("where_or", jsonSearchVal);


            }*/


            mStockRequest.setWhere(mWhereList);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
