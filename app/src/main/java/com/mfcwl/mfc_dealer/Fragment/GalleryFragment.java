package com.mfcwl.mfc_dealer.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mfcwl.mfc_dealer.Activity.GalleryActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import uk.co.senab.photoview.PhotoView;



/**
 * Created by Surya on 12/06/2018.
 */
public class GalleryFragment extends Fragment {

    public static final String LOG_TAG = GalleryFragment.class.getSimpleName();
    public static final String ARG_ITEM_POSITION = "item_position";
    public static final String ARG_NUMBER_ITEM = "number_item";


    private String mUrlImage, mNameArea;
    private int mPosition;
    private int mtotalCount;
    public int mThumbnailWidth;

    private PhotoView imageView;
    private TextView counterTxt, nameTxt;
    String TAG = getClass().getSimpleName();
    public GalleryFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

      try{
          Log.d(LOG_TAG, "GalleryFragment savedInstanceState : " + (savedInstanceState == null));

          if (getArguments() != null) {
              if (getArguments().containsKey(ARG_ITEM_POSITION))
//                Begin at 0
                  mPosition = getArguments().getInt(ARG_ITEM_POSITION);
              mtotalCount = getArguments().getInt(ARG_NUMBER_ITEM);
              mUrlImage = GalleryActivity.mListImages.get(mPosition);
          }

          //mNameArea = DialogCardInfoPOI.mAire.name;
      }catch (Exception e){
          e.printStackTrace();
      }

       // mThumbnailWidth = UITools.getPixelDensity(getActivity().getApplicationContext(), 120);

//        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        View rootView = inflater.inflate(R.layout.gallery_fragment_pager, container, false);

        imageView = rootView.findViewById(R.id.fullscreen_picture);


        int realPosition = mPosition + 1;
        String str = realPosition + "/" + mtotalCount;
        counterTxt = rootView.findViewById(R.id.counter_pictures);
        counterTxt.setText(str);



        Glide.with(getActivity()).load(mUrlImage)
                .thumbnail(0.5f)
                .into(imageView);

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
    @Override
    public void onResume(){
        super.onResume();
        // put your code here...
        Application.getInstance().trackScreenView(getActivity(),GlobalText.Gallery_Fragment);
    }

}
