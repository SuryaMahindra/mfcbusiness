package com.mfcwl.mfc_dealer.Fragment.Leads;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.mfcwl.mfc_dealer.R;

/**
 * Created by HP 240 G5 on 16-03-2018.
 */

public class LeadDeleteCustomDialogClass extends Dialog implements View.OnClickListener{

    public Context activity;
    public Dialog d;
    public Button btn_yes;
    public LeadDeleteCustomDialogClass(@NonNull Context context) {
        super(context);
        this.activity = context;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_yes:
                dismiss();
                break;
            default:
                break;
        }
        dismiss();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.lead_fea_cus_dialog);
        Window window = getWindow();
        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        btn_yes = findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(this);

    }
}