package com.mfcwl.mfc_dealer.Fragment.Leads;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.Activity.Leads.FilterInstance;
import com.mfcwl.mfc_dealer.Activity.Leads.NotificationInstance;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.LeadsUtils;
import com.mfcwl.mfc_dealer.Utility.MonthUtility;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static com.mfcwl.mfc_dealer.Utility.LeadsUtils.ClearFollowCheckBoxFilter;
import static com.mfcwl.mfc_dealer.Utility.LeadsUtils.ClearFollowCheckBoxFilter_vvv;
import static com.mfcwl.mfc_dealer.Utility.LeadsUtils.ClearFollowDataFilter;
import static com.mfcwl.mfc_dealer.Utility.LeadsUtils.ClearLeadStatusFilter;
import static com.mfcwl.mfc_dealer.Utility.LeadsUtils.ClearPostCheckBoxFilter;
import static com.mfcwl.mfc_dealer.Utility.LeadsUtils.ClearPostCheckBoxFilter_vvv;
import static com.mfcwl.mfc_dealer.Utility.LeadsUtils.ClearPostDataFilter;


public class LeadFilterFragment extends Fragment {

    public LeadFilterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    CardView card1, card3, card5;
    public static TextView postdate, followup, leadstatus;
    public static TextView postcalendardate, followupcalendardate, fromdate, todate, fromdatefoll, todatefoll;
    boolean flagpostdate = true, flagfollowup = true, flagstatus = true;
    public static CheckBox chopen, chhot, chwarm, chcold, chlost, chsold;
    public static CheckBox chfollowtmrw, chfollowtoday, chfollowyester, chfollowlast7days;
    public static CheckBox chposttoday, chpostyester, chpost7days, chpost15days;
    public static String chselectstatus = "";

    Activity activity;
    Context c;
    public static boolean isoktorepo = false;
    public static String strDate = "", strDatefu = "";
    int timeH, timeM, year, month, day, daysinmonth;
    String timeS = "", dateS = "", regisMonth = "";
    public static boolean setflagrange = true;
    public static int whichdate;
    public static String choosedate = "";
    public static JSONArray leadstatusJsonArray = new JSONArray();
    public static String postFromDate = "", postToDate = "";
    public static String followFromDate = "", followToDate = "";

    public static String postTodYesLastFromDate = "", postTodYesLastToDate = "";
    public static String followTodYesLastFromDate = "", followTodYesLastToDate = "";
    public static String followTomorowdate = "", followTodayDate = "";

    public static int postflag = 0, followflag = 0;
    String fromDate = "", toDate = "";
    public static FilterInstance mInstance = null;
    private SharedPreferences mSharedPreferences = null;
    String TAG = getClass().getSimpleName();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        // Inflate the layout for this fragment
        mSharedPreferences = getActivity().getSharedPreferences("MFC", Context.MODE_PRIVATE);
        mInstance = FilterInstance.getInstance();
        Gson gson = new Gson();
        String mJSon = mSharedPreferences.getString("leadfilter", "");
        if (mJSon != null && mJSon != "") {
            mInstance = gson.fromJson(mJSon, FilterInstance.class);
        }
        leadstatusJsonArray = new JSONArray();
        if (mInstance.getLeadstatusJsonArray() != null) {
            leadstatusJsonArray = mInstance.getLeadstatusJsonArray();
        }

        postTodYesLastFromDate = "";
        postTodYesLastToDate = "";
        followTodYesLastFromDate = "";
        followTodYesLastToDate = "";
        followTomorowdate = "";
        followTodayDate = "";

        postFromDate = "";
        postToDate = "";
        followFromDate = "";
        followToDate = "";


        View view = inflater.inflate(R.layout.layout_leads_filter2, container, false);
        activity = getActivity();
        c = getContext();
        card1 = view.findViewById(R.id.card1);
        card3 = view.findViewById(R.id.card3);
        card5 = view.findViewById(R.id.card5);
        card1.setVisibility(View.GONE);
        card3.setVisibility(View.GONE);
        card5.setVisibility(View.GONE);
        postdate = view.findViewById(R.id.postdate);
        followup = view.findViewById(R.id.followup);
        leadstatus = view.findViewById(R.id.leadstatus);

        fromdate = view.findViewById(R.id.fromdate);
        todate = view.findViewById(R.id.todate);

        fromdatefoll = view.findViewById(R.id.fromdatefoll);
        todatefoll = view.findViewById(R.id.todatefoll);

        postcalendardate = view.findViewById(R.id.postcalendardate);
        followupcalendardate = view.findViewById(R.id.followupcalendardate);

        postdate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
        followup.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
        leadstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);

        chopen = view.findViewById(R.id.chopen);
        chhot = view.findViewById(R.id.chhot);
        chwarm = view.findViewById(R.id.chwarm);
        chcold = view.findViewById(R.id.chcold);
        chlost = view.findViewById(R.id.chlost);
        chsold = view.findViewById(R.id.chsold);

        chposttoday = view.findViewById(R.id.chposttoday);
        chpostyester = view.findViewById(R.id.chpostyester);
        chpost7days = view.findViewById(R.id.chpost7days);
        chpost15days = view.findViewById(R.id.chpost15days);

        chfollowtmrw = view.findViewById(R.id.chfollowtmrw);
        chfollowtoday = view.findViewById(R.id.chfollowtoday);
        chfollowyester = view.findViewById(R.id.chfollowyester);
        chfollowlast7days = view.findViewById(R.id.chfollowlast7days);

        //yuvaraj changes this line
        Log.e("getNotification", "=======" + mInstance.getNotification().toString());
        //Log.e("getNotification", "=======" + FilterInstance.getInstance().getNotification().toString());

        if (mInstance.getNotification().equals("")) {

            if (!mInstance.getPtoday().equals("")) {
                String[] splitFilter = mInstance.getPtoday().split("@", 10);
                postTodYesLastFromDate = splitFilter[0];
                postTodYesLastToDate = splitFilter[1];
                postflag = 1;
                ClearPostCheckBoxFilter();
                card1.setVisibility(View.VISIBLE);
                postdate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                flagpostdate = false;
            } else if (!mInstance.getPyesterday().equals("")) {
                String[] splitFilter = mInstance.getPyesterday().split("@", 10);
                postTodYesLastFromDate = splitFilter[0];
                postTodYesLastToDate = splitFilter[1];
                postflag = 2;
                ClearPostCheckBoxFilter();
                card1.setVisibility(View.VISIBLE);
                postdate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                flagpostdate = false;
            } else if (!mInstance.getPlast7days().equals("")) {
                String[] splitFilter = mInstance.getPlast7days().split("@", 10);
                postFromDate = splitFilter[0];
                postToDate = splitFilter[1];
                postflag = 3;
                ClearPostCheckBoxFilter();
                card1.setVisibility(View.VISIBLE);
                postdate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                flagpostdate = false;
            } else if (!mInstance.getPlast15days().equals("")) {
                String[] splitFilter = mInstance.getPlast15days().split("@", 10);
                postFromDate = splitFilter[0];
                postToDate = splitFilter[1];
                postflag = 4;
                ClearPostCheckBoxFilter();
                card1.setVisibility(View.VISIBLE);
                postdate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                flagpostdate = false;
            } else if (!mInstance.getPchoosecustomdatefrom().equals("") && !mInstance.getPchoosecustomdateto().equals("")) {
                postTodYesLastFromDate = mInstance.getPchoosecustomdatefrom();
                postTodYesLastToDate = mInstance.getPchoosecustomdateto();
                String[] splitifonfrom = postTodYesLastFromDate.split("-", 10);
                fromdate.setText(postTodYesLastFromDate);
                fromdate.setText(splitifonfrom[2] + " " + MonthUtility.Month[Integer.parseInt(splitifonfrom[1])] + " " + splitifonfrom[0] + " -");
                String[] splitifonto = postTodYesLastToDate.split("-", 10);
                todate.setText(postTodYesLastToDate);
                todate.setText(" " + splitifonto[2] + " " + MonthUtility.Month[Integer.parseInt(splitifonto[1])] + " " + splitifonto[0]);
                postflag = 5;
                ClearPostCheckBoxFilter();
                card1.setVisibility(View.VISIBLE);
                postdate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                flagpostdate = false;
            }

            if (!mInstance.getFtomorrow().equals("")) {
                //yuvaraj changed this line
                //String[] splitFilter = FilterInstance.getInstance().getFtomorrow().split("@", 10);
                String[] splitFilter = mInstance.getFtomorrow().split("@", 10);
                followTodayDate = splitFilter[0];
                followTomorowdate = splitFilter[1];

            /*followTodYesLastFromDate = splitFilter[0];
            followTodYesLastToDate = splitFilter[1];*/

                followflag = 1;
                ClearFollowCheckBoxFilter();
                card3.setVisibility(View.VISIBLE);
                followup.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                flagfollowup = false;
            } else if (!mInstance.getFtoday().equals("")) {
                String[] splitFilter = mInstance.getFtoday().split("@", 10);
                followTodYesLastFromDate = splitFilter[0];
                followTodYesLastToDate = splitFilter[1];
                followflag = 2;
                ClearFollowCheckBoxFilter();
                card3.setVisibility(View.VISIBLE);
                followup.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                flagfollowup = false;
            } else if (!mInstance.getFyesterday().equals("")) {
                String[] splitFilter = mInstance.getFyesterday().split("@", 10);
                followTodYesLastFromDate = splitFilter[0];
                followTodYesLastToDate = splitFilter[1];
                followflag = 3;
                ClearFollowCheckBoxFilter();
                card3.setVisibility(View.VISIBLE);
                followup.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                flagfollowup = false;
            } else if (!mInstance.getFlast7days().equals("")) {
                String[] splitFilter = mInstance.getFlast7days().split("@", 10);
                followFromDate = splitFilter[0];
                followToDate = splitFilter[1];
                followflag = 4;
                ClearFollowCheckBoxFilter();
                card3.setVisibility(View.VISIBLE);
                followup.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                flagfollowup = false;
            }   //else if (!mInstance.getFchoosecustomdatefrom().equals("") && !FilterInstance.getInstance().getFchoosecustomdateto().equals("")) {
            //yuvaraj change this line
            else if (!mInstance.getFchoosecustomdatefrom().equals("") && !mInstance.getFchoosecustomdateto().equals("")) {
                followTodYesLastFromDate = mInstance.getFchoosecustomdatefrom();
                followTodYesLastToDate = mInstance.getFchoosecustomdateto();
                String[] splitifonfrom = followTodYesLastFromDate.split("-", 10);
                fromdatefoll.setText(followTodYesLastFromDate);
                fromdatefoll.setText(splitifonfrom[2] + " " + MonthUtility.Month[Integer.parseInt(splitifonfrom[1])] + " " + splitifonfrom[0] + " -");
                String[] splitifonto = followTodYesLastToDate.split("-", 10);
                todatefoll.setText(followTodYesLastToDate);
                todatefoll.setText(" " + splitifonto[2] + " " + MonthUtility.Month[Integer.parseInt(splitifonto[1])] + " " + splitifonto[0]);
                followflag = 5;
                ClearFollowCheckBoxFilter();
                card3.setVisibility(View.VISIBLE);
                followup.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                flagfollowup = false;
            }


            if (!mInstance.getLsopen().equals("")) {

                chopen.setChecked(true);
                card5.setVisibility(View.VISIBLE);
                leadstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                flagstatus = false;
            }
            if (!mInstance.getLshot().equals("")) {

                chhot.setChecked(true);
                card5.setVisibility(View.VISIBLE);
                leadstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                flagstatus = false;
            }
            if (!mInstance.getLswarm().equals("")) {

                chwarm.setChecked(true);
                card5.setVisibility(View.VISIBLE);
                leadstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                flagstatus = false;
            }

            if (!mInstance.getLscold().equals("")) {

                chcold.setChecked(true);
                card5.setVisibility(View.VISIBLE);
                leadstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                flagstatus = false;
            }

            if (!mInstance.getLslost().equals("")) {

                chlost.setChecked(true);
                card5.setVisibility(View.VISIBLE);
                leadstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                flagstatus = false;
            }
            if (!mInstance.getLssold().equals("")) {

                chsold.setChecked(true);
                card5.setVisibility(View.VISIBLE);
                leadstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                flagstatus = false;
            }

        } else {

            ClearPostDataFilter();

            ClearFollowDataFilter();

            ClearLeadStatusFilter();

            chposttoday.setChecked(false);
            chpostyester.setChecked(false);
            chpost7days.setChecked(false);
            chpost15days.setChecked(false);

            chfollowtmrw.setChecked(false);
            chfollowtoday.setChecked(false);
            chfollowyester.setChecked(false);
            chfollowlast7days.setChecked(false);

            chopen.setChecked(false);
            chhot.setChecked(false);
            chwarm.setChecked(false);
            chcold.setChecked(false);
            chlost.setChecked(false);
            chsold.setChecked(false);

            postFromDate = "";
            postToDate = "";

            followFromDate = "";
            followToDate = "";

            postTodYesLastFromDate = "";
            postTodYesLastToDate = "";

            followTodYesLastFromDate = "";
            followTodYesLastToDate = "";

            followTomorowdate = "";
            followTodayDate = "";

            leadstatusJsonArray = new JSONArray();

            fromdate.setText("");
            todate.setText("");

            fromdatefoll.setText("");
            todatefoll.setText("");
            if (mInstance.getNotification().equals("new lead")) {
                //yuvaraj changes this line
                Log.e("getNotification", "=con" + mInstance.getNotification().toString());
                //Log.e("getNotification", "=con" + FilterInstance.getInstance().getNotification().toString());

                // getActivity().finish();
            }
            if (mInstance.getNotification().equals("Follow")) {

                card3.setVisibility(View.VISIBLE);
                followup.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                flagfollowup = false;

                followflag = 2;
                ClearFollowCheckBoxFilter();

                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, 0);

                Calendar cal1 = Calendar.getInstance();
                cal1.add(Calendar.DATE, 1);

                SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

                Log.e("choosedate ", "today: " + s.format(new Date(cal.getTimeInMillis())));
                Log.e("choosedate ", "tomorrow: " + s.format(new Date(cal1.getTimeInMillis())));
                if (chfollowtoday.isChecked()) {
                    followTodYesLastFromDate = s.format(new Date(cal.getTimeInMillis()));
                    followTodYesLastToDate = s.format(new Date(cal1.getTimeInMillis()));
                    mInstance.setFtoday(followTodYesLastFromDate + "@" + followTodYesLastToDate);

                    mInstance.setFtomorrow("");
                    mInstance.setFyesterday("");
                    mInstance.setFlast7days("");
                } else {
                    followTodYesLastFromDate = "";
                    followTodYesLastToDate = "";
                    mInstance.setFtoday("");
                }
            }

            // FilterInstance.getInstance().setNotification("");
        }


        chopen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                //remove if already presents in json arry
                try {
                    for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                        String mOpen = leadstatusJsonArray.getString(i);
                        if (mOpen.equalsIgnoreCase("open")) {
                            leadstatusJsonArray.remove(i);
                            mInstance.setLsopen("");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (isChecked) {
                    chselectstatus = "OPEN";
                    leadstatusJsonArray.put("open");
                    mInstance.setLsopen(chselectstatus);

                }
            }
        });

        chhot.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                try {
                    for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                        String mhot = leadstatusJsonArray.getString(i);
                        if (mhot.equalsIgnoreCase("hot")) {
                            leadstatusJsonArray.remove(i);
                            mInstance.setLshot("");
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (isChecked) {
                    chselectstatus = "HOT";
                    leadstatusJsonArray.put("hot");
                    mInstance.setLshot(chselectstatus);
                }
            }
        });
        chwarm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                try {
                    for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                        String mWarm = leadstatusJsonArray.getString(i);
                        if (mWarm.equalsIgnoreCase("warm")) {
                            leadstatusJsonArray.remove(i);
                            mInstance.setLswarm("");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (isChecked) {
                    chselectstatus = "WARM";
                    leadstatusJsonArray.put("warm");
                    mInstance.setLswarm(chselectstatus);
                }
            }
        });
        chcold.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                try {
                    for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                        String mCold = leadstatusJsonArray.getString(i);
                        if (mCold.equalsIgnoreCase("cold")) {
                            leadstatusJsonArray.remove(i);
                            mInstance.setLscold("");
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                if (isChecked) {
                    chselectstatus = "COLD";
                    leadstatusJsonArray.put("cold");
                    mInstance.setLscold(chselectstatus);
                }
            }
        });
        chlost.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                try {
                    for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                        String mLost = leadstatusJsonArray.getString(i);
                        if (mLost.equalsIgnoreCase("lost")) {
                            leadstatusJsonArray.remove(i);
                            mInstance.setLslost("");
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (isChecked) {
                    chselectstatus = "LOST";
                    leadstatusJsonArray.put("lost");
                    mInstance.setLslost(chselectstatus);
                }
            }
        });

        chsold.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                try {
                    for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                        String mSold = leadstatusJsonArray.getString(i);
                        if (mSold.equalsIgnoreCase("sold")) {
                            leadstatusJsonArray.remove(i);
                            mInstance.setLssold("");
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (isChecked) {
                    chselectstatus = "SOLD";
                    leadstatusJsonArray.put("sold");
                    mInstance.setLssold(chselectstatus);
                }
            }
        });


        chposttoday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LeadsUtils.ClearPostDataFilter();
                postTodYesLastFromDate = "";
                postTodYesLastToDate = "";
                postFromDate = "";
                postToDate = "";
                fromdate.setText("");
                todate.setText("");

                postflag = 1;
                ClearPostCheckBoxFilter_vvv();

                choosedate = getDate();

                String[] splitdate = choosedate.split("-", 10);

                String year = splitdate[0];
                String month = splitdate[1];
                String date = splitdate[2];

                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, 0);

                Calendar cal1 = Calendar.getInstance();
                cal1.add(Calendar.DATE, 1);

                SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

                Log.e("choosedate ", "today: " + s.format(new Date(cal.getTimeInMillis())));
                Log.e("choosedate ", "tomorrow: " + s.format(new Date(cal1.getTimeInMillis())));

                if (chposttoday.isChecked()) {
                    postTodYesLastFromDate = s.format(new Date(cal.getTimeInMillis()));
                    postTodYesLastToDate = s.format(new Date(cal1.getTimeInMillis()));
                    mInstance.setPtoday(postTodYesLastFromDate + "@" + postTodYesLastToDate);

                    //set the other to empty
                    mInstance.setPyesterday("");
                    mInstance.setPlast7days("");
                    mInstance.setPlast15days("");


                } else {
                    postTodYesLastFromDate = "";
                    postTodYesLastToDate = "";
                    mInstance.setPtoday("");
                }
            }
        });
        chpostyester.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LeadsUtils.ClearPostDataFilter();
                postTodYesLastFromDate = "";
                postTodYesLastToDate = "";
                postFromDate = "";
                postToDate = "";

                fromdate.setText("");
                todate.setText("");

                postflag = 2;
                ClearPostCheckBoxFilter_vvv();


                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -1);

                Calendar cal1 = Calendar.getInstance();
                cal1.add(Calendar.DATE, 0);

                SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

                Log.e("choosedate ", "yesterday: " + s.format(new Date(cal.getTimeInMillis())));
                Log.e("choosedate ", "today: " + s.format(new Date(cal1.getTimeInMillis())));
                if (chpostyester.isChecked()) {
                    postTodYesLastFromDate = s.format(new Date(cal.getTimeInMillis()));
                    postTodYesLastToDate = s.format(new Date(cal1.getTimeInMillis()));
                    mInstance.setPyesterday(postTodYesLastFromDate + "@" + postTodYesLastToDate);
                    mInstance.setPtoday("");
                    mInstance.setPlast7days("");
                    mInstance.setPlast15days("");
                } else {
                    postTodYesLastFromDate = "";
                    postTodYesLastToDate = "";
                    mInstance.setPyesterday("");
                }

            }
        });
        chpost7days.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LeadsUtils.ClearPostDataFilter();
                postTodYesLastFromDate = "";
                postTodYesLastToDate = "";
                postFromDate = "";
                postToDate = "";
                fromdate.setText("");
                todate.setText("");
                postflag = 3;
                ClearPostCheckBoxFilter_vvv();

                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -7);

                Calendar cal1 = Calendar.getInstance();
                cal1.add(Calendar.DATE, 0);

                SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

                Log.e("choosedate ", "Last7days: " + s.format(new Date(cal.getTimeInMillis())));
                Log.e("choosedate ", "today: " + s.format(new Date(cal1.getTimeInMillis())));

                if (chpost7days.isChecked()) {
                    postFromDate = s.format(new Date(cal.getTimeInMillis()));
                    postToDate = s.format(new Date(cal1.getTimeInMillis()));
                    mInstance.setPlast7days(postFromDate + "@" + postToDate);

                    mInstance.setPyesterday("");
                    mInstance.setPtoday("");
                    mInstance.setPlast15days("");
                } else {
                    postFromDate = "";
                    postToDate = "";
                    mInstance.setPlast7days("");
                }
            }
        });
        chpost15days.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LeadsUtils.ClearPostDataFilter();
                postTodYesLastFromDate = "";
                postTodYesLastToDate = "";
                postFromDate = "";
                postToDate = "";
                fromdate.setText("");
                todate.setText("");
                postflag = 4;
                ClearPostCheckBoxFilter_vvv();

                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -15);

                Calendar cal1 = Calendar.getInstance();
                cal1.add(Calendar.DATE, 0);

                SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

                Log.e("choosedate ", "Last15days: " + s.format(new Date(cal.getTimeInMillis())));
                Log.e("choosedate ", "today: " + s.format(new Date(cal1.getTimeInMillis())));
                if (chpost15days.isChecked()) {
                    postFromDate = s.format(new Date(cal.getTimeInMillis()));
                    postToDate = s.format(new Date(cal1.getTimeInMillis()));
                    mInstance.setPlast15days(postFromDate + "@" + postToDate);

                    mInstance.setPyesterday("");
                    mInstance.setPlast7days("");
                    mInstance.setPtoday("");
                } else {
                    postFromDate = "";
                    postToDate = "";
                    mInstance.setPlast15days("");
                }
            }
        });


        chfollowtmrw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClearFollowDataFilter();
                followTodYesLastFromDate = "";
                followTodYesLastToDate = "";

                followTomorowdate = "";
                followTodayDate = "";

                followFromDate = "";
                followToDate = "";

                fromdatefoll.setText("");
                todatefoll.setText("");
                followflag = 1;
                ClearFollowCheckBoxFilter_vvv();

                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, 1);

                Calendar cal1 = Calendar.getInstance();
                cal1.add(Calendar.DATE, 0);

                SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

                Log.e("choosedate ", "tomorrow: " + s.format(new Date(cal.getTimeInMillis())));
                Log.e("choosedate ", "today: " + s.format(new Date(cal1.getTimeInMillis())));
                if (chfollowtmrw.isChecked()) {
                    /*followTodYesLastFromDate = s.format(new Date(cal.getTimeInMillis()));
                    followTodYesLastToDate = s.format(new Date(cal1.getTimeInMillis()));
                    FilterInstance.getInstance().setFtomorrow(followTodYesLastFromDate+"@"+followTodYesLastToDate);*/
                    followTodayDate = s.format(new Date(cal.getTimeInMillis()));
                    followTomorowdate = s.format(new Date(cal1.getTimeInMillis()));

                    mInstance.setFtomorrow(followTodayDate + "@" + followTomorowdate);

                    mInstance.setFtoday("");
                    mInstance.setFyesterday("");
                    mInstance.setFlast7days("");
                } else {
                    followTodYesLastFromDate = "";
                    followTodYesLastToDate = "";

                    followTomorowdate = "";
                    followTodayDate = "";
                    mInstance.setFtomorrow("");

                }
            }
        });


        if (!NotificationInstance.getInstance().getTodayfollowdate().equals("")) {
            chfollowtoday.setChecked(true);
        }

        chfollowtoday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClearFollowDataFilter();
                followTomorowdate = "";
                followTodayDate = "";

                followTodYesLastFromDate = "";
                followTodYesLastToDate = "";
                followFromDate = "";
                followToDate = "";

                fromdatefoll.setText("");
                todatefoll.setText("");
                followflag = 2;
                ClearFollowCheckBoxFilter_vvv();


                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, 0);

                Calendar cal1 = Calendar.getInstance();
                cal1.add(Calendar.DATE, 1);

                SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

                Log.e("choosedate ", "today: " + s.format(new Date(cal.getTimeInMillis())));
                Log.e("choosedate ", "tomorrow: " + s.format(new Date(cal1.getTimeInMillis())));
                if (chfollowtoday.isChecked()) {
                    followTodYesLastFromDate = s.format(new Date(cal.getTimeInMillis()));
                    followTodYesLastToDate = s.format(new Date(cal1.getTimeInMillis()));
                    mInstance.setFtoday(followTodYesLastFromDate + "@" + followTodYesLastToDate);

                    mInstance.setFtomorrow("");
                    mInstance.setFyesterday("");
                    mInstance.setFlast7days("");

                } else {
                    followTodYesLastFromDate = "";
                    followTodYesLastToDate = "";
                    mInstance.setFtoday("");

                }

            }
        });


        chfollowyester.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClearFollowDataFilter();
                followTomorowdate = "";
                followTodayDate = "";

                followTodYesLastFromDate = "";
                followTodYesLastToDate = "";
                followFromDate = "";
                followToDate = "";

                fromdatefoll.setText("");
                todatefoll.setText("");
                followflag = 3;
                ClearFollowCheckBoxFilter_vvv();

                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -1);

                Calendar cal1 = Calendar.getInstance();
                cal1.add(Calendar.DATE, 0);

                SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

                Log.e("choosedate ", "yesterday: " + s.format(new Date(cal.getTimeInMillis())));
                Log.e("choosedate ", "today: " + s.format(new Date(cal1.getTimeInMillis())));
                if (chfollowyester.isChecked()) {
                    followTodYesLastFromDate = s.format(new Date(cal.getTimeInMillis()));
                    followTodYesLastToDate = s.format(new Date(cal1.getTimeInMillis()));
                    mInstance.setFyesterday(followTodYesLastFromDate + "@" + followTodYesLastToDate);

                    mInstance.setFtoday("");
                    mInstance.setFtomorrow("");
                    mInstance.setFlast7days("");
                } else {
                    followTodYesLastFromDate = "";
                    followTodYesLastToDate = "";
                    mInstance.setFyesterday("");
                }
            }
        });


        chfollowlast7days.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClearFollowDataFilter();
                followTomorowdate = "";
                followTodayDate = "";

                followTodYesLastFromDate = "";
                followTodYesLastToDate = "";
                followFromDate = "";
                followToDate = "";

                fromdatefoll.setText("");
                todatefoll.setText("");
                followflag = 4;
                ClearFollowCheckBoxFilter_vvv();

                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -7);

                Calendar cal1 = Calendar.getInstance();
                cal1.add(Calendar.DATE, 0);

                SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

                Log.e("choosedate ", "last7days: " + s.format(new Date(cal.getTimeInMillis())));
                Log.e("choosedate ", "today: " + s.format(new Date(cal1.getTimeInMillis())));
                if (chfollowlast7days.isChecked()) {
                    followFromDate = s.format(new Date(cal.getTimeInMillis()));
                    followToDate = s.format(new Date(cal1.getTimeInMillis()));
                    mInstance.setFlast7days(followFromDate + "@" + followToDate);

                    mInstance.setFtoday("");
                    mInstance.setFyesterday("");
                    mInstance.setFtomorrow("");
                } else {
                    followFromDate = "";
                    followToDate = "";

                    mInstance.setFlast7days("");

                }
            }
        });

        postdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flagpostdate) {
                    card1.setVisibility(View.VISIBLE);
                    postdate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                    flagpostdate = false;
                } else {
                    postdate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
                    card1.setVisibility(View.GONE);
                    flagpostdate = true;
                }
            }
        });

        followup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flagfollowup) {
                    followup.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                    card3.setVisibility(View.VISIBLE);
                    flagfollowup = false;
                } else {
                    followup.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
                    card3.setVisibility(View.GONE);
                    flagfollowup = true;
                }
            }
        });

        leadstatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flagstatus) {
                    leadstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                    card5.setVisibility(View.VISIBLE);
                    flagstatus = false;
                } else {
                    leadstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
                    card5.setVisibility(View.GONE);
                    flagstatus = true;
                }
            }
        });


        postcalendardate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LeadsUtils.ClearPostDataFilter();

                postTodYesLastFromDate = "";
                postTodYesLastToDate = "";
                postFromDate = "";
                postToDate = "";


                whichdate = 0;
                chposttoday.setChecked(false);
                chpostyester.setChecked(false);
                chpost7days.setChecked(false);
                chpost15days.setChecked(false);
                fromDate = "fromdate";
                setDate2(view);
            }
        });

        followupcalendardate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LeadsUtils.ClearFollowDataFilter();

                followTomorowdate = "";
                followTodayDate = "";

                followTodYesLastFromDate = "";
                followTodYesLastToDate = "";


                whichdate = 1;
                chfollowtmrw.setChecked(false);
                chfollowtoday.setChecked(false);
                chfollowyester.setChecked(false);
                chfollowlast7days.setChecked(false);
                fromDate = "todate";
                setDate3(view);
            }
        });


        return view;

    }


    public String getTime() {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
        Date currentLocalTime = cal.getTime();
        DateFormat date = new SimpleDateFormat("HH:mm a");
        date.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
        String localTime = date.format(currentLocalTime);

        timeH = cal.get(Calendar.HOUR_OF_DAY);
        timeM = cal.get(Calendar.MINUTE);

        return localTime;
    }


    public void setDate(View v) {


        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
            // when dialog box is closed, below method will be called.
            public void onDateSet(DatePicker view, int selectedYear,
                                  int selectedMonth, int selectedDay) {
                if (isoktorepo) {
                    myCalendar.set(Calendar.YEAR, selectedYear);
                    myCalendar.set(Calendar.MONTH, selectedMonth);
                    myCalendar.set(Calendar.DAY_OF_MONTH, selectedDay);

                  //  System.out.println();
                    String strselectedDay = String.valueOf(selectedDay);
                    if (strselectedDay.length() == 1) {
                        strselectedDay = "0" + strselectedDay;
                    }
                    String strselectedMonth = String.valueOf(selectedMonth + 1);
                    if (strselectedMonth.length() == 1) {
                        strselectedMonth = "0" + strselectedMonth;
                    }

                    /*strDate = strselectedDay + "/" + strselectedMonth + "/"
                            + selectedYear;*/

                    strDate = strselectedDay + "-" + strselectedMonth + "-"
                            + selectedYear;
                    MethodofMonth(strselectedMonth);

                    //postcalendardate.append(strselectedDay + " "+regisMonth);
                    if (whichdate == 0) {
                        if (setflagrange) {
                            fromdate.setText(strselectedDay + " " + regisMonth);
                            postFromDate = selectedYear + "-" + (selectedMonth + 1) + "-" + selectedDay;
                            setflagrange = false;
                            setDate(view);
                        } else {
                            setDate1(view);
                        }
                    } else {
                        if (setflagrange) {
                            fromdatefoll.setText(strselectedDay + " " + regisMonth);
                            followFromDate = selectedYear + "-" + (selectedMonth + 1) + "-" + selectedDay;
                            setflagrange = false;
                            setDate(view);
                        } else {
                            todatefoll.setText("- " + strselectedDay + " " + regisMonth);
                            followToDate = selectedYear + "-" + (selectedMonth + 1) + "-" + selectedDay;
                            setflagrange = true;
                        }
                    }
                    SimpleDateFormat curFormater = new SimpleDateFormat("dd/MM/yyyy");
                    Date dateObj = null;
                    try {
                        dateObj = curFormater.parse(strDate);
                    } catch (Exception ex) {

                    }

                    dateS = selectedYear + "-" + selectedMonth + "-" + selectedDay;

                    getTime();

                    SimpleDateFormat postFormater = new SimpleDateFormat("MMMM dd, yyyy");
                    //postcalendardate.setText(strDate + " " );

                    final TimePickerDialog timePickerDialog = new TimePickerDialog(activity,
                            new TimePickerDialog.OnTimeSetListener() {

                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay,
                                                      int minute) {

                                    timeS = hourOfDay + ":" + minute;

                                    dateS = dateS + " " + timeS;

                                    Log.e("dateS ", "dateS " + dateS);
                                    postcalendardate.append(dateS + "     ");
                                    //setDate(view);
                                }
                            }, timeH, timeM, true);
                    //timePickerDialog.show();
                }


                isoktorepo = false;
            }
        };


        final DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                datePickerListener, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            dialog.cancel();
                            isoktorepo = false;
                        }
                        setflagrange = true;
                    }
                });

        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    @SuppressLint("NewApi")
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            isoktorepo = true;

                            DatePicker datePicker = datePickerDialog
                                    .getDatePicker();
                            datePickerListener.onDateSet(datePicker,
                                    datePicker.getYear(),
                                    datePicker.getMonth(),
                                    datePicker.getDayOfMonth());


                        }
                    }
                });
        datePickerDialog.setCancelable(false);
        datePickerDialog.show();


    }


    private void MethodofMonth(String selMonth) {

        if (selMonth.equals("1")) {
            regisMonth = "January";
        }
        if (selMonth.equals("2")) {
            regisMonth = "February";
        }
        if (selMonth.equals("3")) {
            regisMonth = "March";
        }
        if (selMonth.equals("4")) {
            regisMonth = "April";
        }
        if (selMonth.equals("5")) {
            regisMonth = "May";
        }
        if (selMonth.equals("6")) {
            regisMonth = "June";
        }
        if (selMonth.equals("7")) {
            regisMonth = "July";
        }
        if (selMonth.equals("8")) {
            regisMonth = "August";
        }
        if (selMonth.equals("9")) {
            regisMonth = "September";
        }
        if (selMonth.equals("10")) {
            regisMonth = "October";
        }
        if (selMonth.equals("11")) {
            regisMonth = "November";
        }
        if (selMonth.equals("12")) {
            regisMonth = "December";
        }

    }


    public String getDate() {
        Calendar c = Calendar.getInstance();
       // System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }
    // ---------SONALI ---------------------------------------------------


    public void setDate3(View v) {


        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
            // when dialog box is closed, below method will be called.
            public void onDateSet(DatePicker view, int selectedYear,
                                  int selectedMonth, int selectedDay) {
                if (isoktorepo) {
                    myCalendar.set(Calendar.YEAR, selectedYear);
                    myCalendar.set(Calendar.MONTH, selectedMonth);
                    myCalendar.set(Calendar.DAY_OF_MONTH, selectedDay);

                    //System.out.println();
                    String strselectedDay = String.valueOf(selectedDay);
                    if (strselectedDay.length() == 1) {
                        strselectedDay = "0" + strselectedDay;
                    }
                    String strselectedMonth = String.valueOf(selectedMonth + 1);
                    if (strselectedMonth.length() == 1) {
                        strselectedMonth = "0" + strselectedMonth;
                    }

                    strDatefu = selectedYear + "-" + strselectedMonth + "-"
                            + strselectedDay;

                    followTodYesLastFromDate = strDatefu;
                    mInstance.setFchoosecustomdatefrom(followTodYesLastFromDate);

                    MethodofMonth(strselectedMonth);

                    if (fromDate.equals("fromdate")) {
                        fromdate.setText(strselectedDay + " " + regisMonth + " " + selectedYear + " -");
                    } else {
                        fromdatefoll.setText(strselectedDay + " " + regisMonth + " " + selectedYear + " -");
                    }
                }
                isoktorepo = false;
                setDate4(view);
            }
        };


        final DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                datePickerListener, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            dialog.cancel();
                            isoktorepo = false;
                        }
                    }
                });

        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    @SuppressLint("NewApi")
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            isoktorepo = true;

                            DatePicker datePicker = datePickerDialog
                                    .getDatePicker();
                            datePickerListener.onDateSet(datePicker,
                                    datePicker.getYear(),
                                    datePicker.getMonth(),
                                    datePicker.getDayOfMonth());

                        }
                    }
                });

        long currentTime = System.currentTimeMillis();
        //datePickerDialog.getDatePicker().setMaxDate(currentTime);

        datePickerDialog.setCancelable(false);
        datePickerDialog.show();


    }


    public void setDate2(View v) {


        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
            // when dialog box is closed, below method will be called.
            public void onDateSet(DatePicker view, int selectedYear,
                                  int selectedMonth, int selectedDay) {
                if (isoktorepo) {
                    myCalendar.set(Calendar.YEAR, selectedYear);
                    myCalendar.set(Calendar.MONTH, selectedMonth);
                    myCalendar.set(Calendar.DAY_OF_MONTH, selectedDay);

                    //System.out.println();
                    String strselectedDay = String.valueOf(selectedDay);
                    if (strselectedDay.length() == 1) {
                        strselectedDay = "0" + strselectedDay;
                    }
                    String strselectedMonth = String.valueOf(selectedMonth + 1);
                    if (strselectedMonth.length() == 1) {
                        strselectedMonth = "0" + strselectedMonth;
                    }

                    strDatefu = selectedYear + "-" + strselectedMonth + "-"
                            + strselectedDay;
                    postTodYesLastFromDate = strDatefu;
                    mInstance.setPchoosecustomdatefrom(postTodYesLastFromDate);

                    MethodofMonth(strselectedMonth);

                    if (fromDate.equals("fromdate")) {
                        fromdate.setText(strselectedDay + " " + regisMonth + " " + selectedYear + " -");
                    } else {
                        fromdatefoll.setText(strselectedDay + " " + regisMonth + " " + selectedYear + " -");
                    }
                }
                isoktorepo = false;
                setDate1(view);
            }
        };


        final DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                datePickerListener, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            dialog.cancel();
                            isoktorepo = false;
                        }
                    }
                });

        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    @SuppressLint("NewApi")
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            isoktorepo = true;

                            DatePicker datePicker = datePickerDialog
                                    .getDatePicker();
                            datePickerListener.onDateSet(datePicker,
                                    datePicker.getYear(),
                                    datePicker.getMonth(),
                                    datePicker.getDayOfMonth());

                        }
                    }
                });

        long currentTime = System.currentTimeMillis();
        datePickerDialog.getDatePicker().setMaxDate(currentTime);

        datePickerDialog.setCancelable(false);
        datePickerDialog.show();


    }


    public void setDate4(View v) {

        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
            // when dialog box is closed, below method will be called.
            public void onDateSet(DatePicker view, int selectedYear,
                                  int selectedMonth, int selectedDay) {
                if (isoktorepo) {
                    myCalendar.set(Calendar.YEAR, selectedYear);
                    myCalendar.set(Calendar.MONTH, selectedMonth);
                    myCalendar.set(Calendar.DAY_OF_MONTH, selectedDay);

                    //System.out.println();
                    String strselectedDay = String.valueOf(selectedDay);
                    if (strselectedDay.length() == 1) {
                        strselectedDay = "0" + strselectedDay;
                    }
                    String strselectedMonth = String.valueOf(selectedMonth + 1);
                    if (strselectedMonth.length() == 1) {
                        strselectedMonth = "0" + strselectedMonth;
                    }

                    strDate = selectedYear + "-" + strselectedMonth + "-"
                            + strselectedDay;

                    followTodYesLastToDate = strDate;
                    mInstance.setFchoosecustomdateto(followTodYesLastToDate);
                    MethodofMonth(strselectedMonth);
                    /*if (WhichDate.equals("LeadDate")) {
                        leaddatetv.setText(strDate);
                    } else {
                        follwdatetv.setText(strDate);
                    }*/
                    /*todate.setText("- " + strselectedDay + " " + strselectedMonth);
                    postToDate = selectedYear + "-" + (selectedMonth + 1) + "-" + selectedDay;
                    setflagrange = true;*/

                    if (fromDate.equals("fromdate")) {
                        todate.setText(strselectedDay + " " + regisMonth + " " + selectedYear);
                    } else {
                        todatefoll.setText(strselectedDay + " " + regisMonth + " " + selectedYear);
                    }
                }


                isoktorepo = false;
            }
        };


        final DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                datePickerListener, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            if (fromDate.equals("fromdate")) {
                                fromdate.setText("");
                            } else {
                                fromdatefoll.setText("");
                            }
                            if (fromDate.equals("fromdate")) {
                                todate.setText("");
                            } else {
                                todatefoll.setText("");
                            }
                            dialog.cancel();
                            isoktorepo = false;
                        }
                    }
                });

        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    @SuppressLint("NewApi")
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            isoktorepo = true;

                            DatePicker datePicker = datePickerDialog
                                    .getDatePicker();
                            datePickerListener.onDateSet(datePicker,
                                    datePicker.getYear(),
                                    datePicker.getMonth(),
                                    datePicker.getDayOfMonth());


                        }
                    }
                });
        long timeInMilliseconds = 0;
        String givenDateString = "";
        if (fromDate.equals("fromdate")) {
            givenDateString = strDatefu;
        } else {
            givenDateString = strDatefu;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date mDate = sdf.parse(givenDateString);
            timeInMilliseconds = mDate.getTime();
           // Log.e("timeInMilliseconds ", "timeInMilliseconds " + timeInMilliseconds);
            //System.out.println("Date in milli :: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long currentTime = System.currentTimeMillis();
        datePickerDialog.getDatePicker().setMinDate(timeInMilliseconds);
        datePickerDialog.setCancelable(false);
        datePickerDialog.show();


    }


    public void setDate1(View v) {

        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
            // when dialog box is closed, below method will be called.
            public void onDateSet(DatePicker view, int selectedYear,
                                  int selectedMonth, int selectedDay) {
                if (isoktorepo) {
                    myCalendar.set(Calendar.YEAR, selectedYear);
                    myCalendar.set(Calendar.MONTH, selectedMonth);
                    myCalendar.set(Calendar.DAY_OF_MONTH, selectedDay);

                    //System.out.println();
                    String strselectedDay = String.valueOf(selectedDay);
                    if (strselectedDay.length() == 1) {
                        strselectedDay = "0" + strselectedDay;
                    }
                    String strselectedMonth = String.valueOf(selectedMonth + 1);
                    if (strselectedMonth.length() == 1) {
                        strselectedMonth = "0" + strselectedMonth;
                    }

                    strDate = selectedYear + "-" + strselectedMonth + "-"
                            + strselectedDay;
                    postTodYesLastToDate = strDate;
                    mInstance.setPchoosecustomdateto(postTodYesLastToDate);

                    MethodofMonth(strselectedMonth);
                    /*if (WhichDate.equals("LeadDate")) {
                        leaddatetv.setText(strDate);
                    } else {
                        follwdatetv.setText(strDate);
                    }*/
                    /*todate.setText("- " + strselectedDay + " " + strselectedMonth);
                    postToDate = selectedYear + "-" + (selectedMonth + 1) + "-" + selectedDay;
                    setflagrange = true;*/

                    if (fromDate.equals("fromdate")) {
                        todate.setText(strselectedDay + " " + regisMonth + " " + selectedYear);
                    } else {
                        todatefoll.setText(strselectedDay + " " + regisMonth + " " + selectedYear);
                    }
                }


                isoktorepo = false;
            }
        };


        final DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                datePickerListener, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            if (fromDate.equals("fromdate")) {
                                fromdate.setText("");
                            } else {
                                fromdatefoll.setText("");
                            }
                            if (fromDate.equals("fromdate")) {
                                todate.setText("");
                            } else {
                                todatefoll.setText("");
                            }
                            dialog.cancel();
                            isoktorepo = false;
                        }
                    }
                });

        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    @SuppressLint("NewApi")
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            isoktorepo = true;

                            DatePicker datePicker = datePickerDialog
                                    .getDatePicker();
                            datePickerListener.onDateSet(datePicker,
                                    datePicker.getYear(),
                                    datePicker.getMonth(),
                                    datePicker.getDayOfMonth());


                        }
                    }
                });
        long timeInMilliseconds = 0;
        String givenDateString = "";
        if (fromDate.equals("fromdate")) {
            givenDateString = strDatefu;
        } else {
            givenDateString = strDatefu;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date mDate = sdf.parse(givenDateString);
            timeInMilliseconds = mDate.getTime();
           // Log.e("timeInMilliseconds ", "timeInMilliseconds " + timeInMilliseconds);
            //System.out.println("Date in milli :: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long currentTime = System.currentTimeMillis();
        datePickerDialog.getDatePicker().setMinDate(timeInMilliseconds);
        datePickerDialog.setCancelable(false);
        datePickerDialog.show();


    }

    @Override
    public void onResume() {
        super.onResume();
        Application.getInstance().trackScreenView(getActivity(),GlobalText.LeadFilter_Fragment);

    }
}
