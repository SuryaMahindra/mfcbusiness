package com.mfcwl.mfc_dealer.AddStockServices;

import android.content.Context;

import com.mfcwl.mfc_dealer.AddStockModel.CityMaster;
import com.mfcwl.mfc_dealer.AddStockModel.ColourMaster;
import com.mfcwl.mfc_dealer.AddStockModel.CommercialMakelist;
import com.mfcwl.mfc_dealer.AddStockModel.ModelDetails;
import com.mfcwl.mfc_dealer.AddStockModel.ModelValue;
import com.mfcwl.mfc_dealer.AddStockModel.Procurement;
import com.mfcwl.mfc_dealer.AddStockModel.ReqUpdateStock;
import com.mfcwl.mfc_dealer.AddStockModel.RequestCreateStock;
import com.mfcwl.mfc_dealer.AddStockModel.ResponseCreateStock;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.retrofitconfig.BaseService;

import org.json.JSONArray;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.PUT;
import retrofit2.http.Path;

import static com.mfcwl.mfc_dealer.Activity.AddStockActivity.activity;
import static com.mfcwl.mfc_dealer.Activity.AddStockActivity.error_popup2;

public class YearService extends BaseService {


    public static void getYearsFromServer(final HttpCallResponse mHttpCallResponse) {
        YearInterface mYearInterface = retrofit.create(YearInterface.class);

        Call<Object> mCall = mYearInterface.getYears();
        mCall.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });
    }

    //color
    public static void getColorFromServer(String color, final HttpCallResponse mHttpCallResponse) {
        YearInterface mYearInterface = retrofit.create(YearInterface.class);

        Call<List<ColourMaster>> mCall = mYearInterface.getColor(color);

        mCall.enqueue(new Callback<List<ColourMaster>>() {
            @Override
            public void onResponse(Call<List<ColourMaster>> call, Response<List<ColourMaster>> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<List<ColourMaster>> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });


    }

    //city
    public static void getCityFromServer(String city, final HttpCallResponse mHttpCallResponse) {
        YearInterface mYearInterface = retrofit.create(YearInterface.class);

        Call<List<CityMaster>> mCall = mYearInterface.getCityMaster(city);

        mCall.enqueue(new Callback<List<CityMaster>>() {
            @Override
            public void onResponse(Call<List<CityMaster>> call, Response<List<CityMaster>> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<List<CityMaster>> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });

    }

    //CommercialMakelist
    public static void getCommercialMakelistFromServer(final HttpCallResponse mHttpCallResponse) {
        YearInterface mYearInterface = retrofit.create(YearInterface.class);

        Call<List<CommercialMakelist>> mCall = mYearInterface.getCommercialMakelistMaster();

        mCall.enqueue(new Callback<List<CommercialMakelist>>() {
            @Override
            public void onResponse(Call<List<CommercialMakelist>> call, Response<List<CommercialMakelist>> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<List<CommercialMakelist>> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });
    }

    //makelist
    public static void getMakelistFromServer(final HttpCallResponse mHttpCallResponse) {
        YearInterface mYearInterface = retrofit.create(YearInterface.class);

        Call<List<CommercialMakelist>> mCall = mYearInterface.getMakelistMaster();

        mCall.enqueue(new Callback<List<CommercialMakelist>>() {
            @Override
            public void onResponse(Call<List<CommercialMakelist>> call, Response<List<CommercialMakelist>> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<List<CommercialMakelist>> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });
    }

    //model details
    public static void getModelDetailsFromServer(String make, final HttpCallResponse mHttpCallResponse) {
        YearInterface mYearInterface = retrofit.create(YearInterface.class);
        Call<ModelDetails> mCall = null;
        if (CommonMethods.getstringvaluefromkey(activity, "Commercial").equalsIgnoreCase("true")) {
            mCall = mYearInterface.getCommercialModelDetailMaster(make);
        } else {
            mCall = mYearInterface.getModelDetailMaster(make);
        }


        mCall.enqueue(new Callback<ModelDetails>() {
            @Override
            public void onResponse(Call<ModelDetails> call, Response<ModelDetails> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<ModelDetails> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });

    }

    //create stock
    public static void getCreateStockFromServer(String stock_source,
                                                String posted_date,
                                                String vehicle_make,
                                                String vehicle_model,
                                                String vehicle_variant,
                                                String reg_month,
                                                String reg_year,
                                                String registraion_city,
                                                String registration_number,
                                                String colour, String kilometer,
                                                String owner,
                                                String insurance,
                                                String insurance_exp_date,
                                                String selling_price,
                                                String dealer_code,
                                                String is_display,
                                                String bought_price,
                                                String refurbishment_cost,
                                                String procurement_executive_id,
                                                String procurement_executive_name,
                                                String cng_kit,
                                                String chassis_number,
                                                String engine_number,
                                                String private_vehicle,
                                                String comments,
                                                String manufacture_month,
                                                String dealer_price,
                                                String is_offload,
                                                String manufacture_year,
                                                String is_featured_car,
                                                String created_by_device
            , final HttpCallResponse mHttpCallResponse) {


    }

    //Procurement
    public static void getProcurementFromServer(final HttpCallResponse mHttpCallResponse) {
        YearInterface mYearInterface = retrofit.create(YearInterface.class);
        Call<List<Procurement>> mCall = mYearInterface.getProcurement();
        mCall.enqueue(new Callback<List<Procurement>>() {
            @Override
            public void onResponse(Call<List<Procurement>> call, Response<List<Procurement>> response) {

                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<List<Procurement>> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });

    }

    //update stock
    public static void getUpdateStockFromServer(ReqUpdateStock reqUpdateStock, Context mContext, final HttpCallResponse mHttpCallResponse) {
        YearInterface mYearInterface = retrofit.create(YearInterface.class);

        Call<ResponseCreateStock> mCall = mYearInterface.getStockUpdate(reqUpdateStock);
        mCall.enqueue(new Callback<ResponseCreateStock>() {
            @Override
            public void onResponse(Call<ResponseCreateStock> call, Response<ResponseCreateStock> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
                if (response.code() == 400) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 401) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 500) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 404) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 304) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 503) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 405) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                }
            }

            @Override
            public void onFailure(Call<ResponseCreateStock> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });

    }

    public interface YearInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @GET("master/years")
        Call<Object> getYears();

        @Headers("Content-Type: application/json; charset=utf-8")
        @GET("master/{colour}")
        Call<List<ColourMaster>> getColor(@Path("colour") String colour);


        @Headers("Content-Type: application/json; charset=utf-8")
        @GET("master/{citylist}")
        Call<List<CityMaster>> getCityMaster(@Path("citylist") String colour);

        @Headers("Content-Type: application/json; charset=utf-8")
        @GET("master/commercial-makelist")
        Call<List<CommercialMakelist>> getCommercialMakelistMaster();

        @Headers("Content-Type: application/json; charset=utf-8")
        @GET("master/makelist")
        Call<List<CommercialMakelist>> getMakelistMaster();

        @Headers("Content-Type: application/json; charset=utf-8")
        @GET("master/modeldetails/{AUDI}")
        Call<ModelDetails> getModelDetailMaster(@Path("AUDI") String make);

        @Headers("Content-Type: application/json; charset=utf-8")
        @GET("master/commercial-modeldetails/{AUDI}")
        Call<ModelDetails> getCommercialModelDetailMaster(@Path("AUDI") String make);

        @Headers("Content-Type: application/json; charset=utf-8")
        @GET("dealer/stocks/create")
        Call<ModelDetails> createStocks(@Body RequestCreateStock mRequestCreateStock);

        @Headers("Content-Type: application/json; charset=utf-8")
        @GET("dealer/active-executive-list/procurement")
        Call<List<Procurement>> getProcurement();

        //update stock
        @Headers("Content-Type: application/json; charset=utf-8")
        @PUT("dealer/stocks/update")
        Call<ResponseCreateStock> getStockUpdate(@Body ReqUpdateStock mReqUpdateStock);
    }
}
