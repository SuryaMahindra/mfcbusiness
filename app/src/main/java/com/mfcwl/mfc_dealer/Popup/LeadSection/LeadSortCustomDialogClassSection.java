package com.mfcwl.mfc_dealer.Popup.LeadSection;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.InstanceCreate.LeadSection.SortInstanceLeadSection;
import com.mfcwl.mfc_dealer.Interface.LeadSection.SortedbyLead;
import com.mfcwl.mfc_dealer.R;

public class LeadSortCustomDialogClassSection extends Dialog implements View.OnClickListener {

    private Context mContext;
    private Activity mActivity;

    private TextView leads_posted_date_l_to_o, leads_followup_date_l_to_o;
    private ImageView posteddateimg, folowimg;
    private RelativeLayout mRelativeLayoutLeadPostedDate, mRelativeLayoutLeadFollowupDate;
    private Button mApply;

    private String leadsortbystatus = "";

    private boolean postedFlag = false;
    private boolean followFlag = false;

    private SortedbyLead mSortedbyLead;

    public LeadSortCustomDialogClassSection(@NonNull Context mContext, Activity mActivity) {
        super(mContext);
        this.mContext = mContext;
        this.mActivity = mActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lead_sort_dialog);
        mSortedbyLead =(SortedbyLead) mActivity;
        InitUI();
    }

    private void InitUI() {
        leads_posted_date_l_to_o = findViewById(R.id.leads_posted_date_l_to_o);
        leads_followup_date_l_to_o = findViewById(R.id.leads_followup_date_l_to_o);

        mApply = findViewById(R.id.stock_sort_apply);

        posteddateimg = findViewById(R.id.posteddateimg);
        folowimg = findViewById(R.id.folowimg);

        mRelativeLayoutLeadPostedDate = findViewById(R.id.lsrl1);
        mRelativeLayoutLeadFollowupDate = findViewById(R.id.lsrl2);

        posteddateimg.setVisibility(View.VISIBLE);
        posteddateimg.animate().rotation(180).setDuration(180).start();
        folowimg.setVisibility(View.INVISIBLE);

        mRelativeLayoutLeadPostedDate.setOnClickListener(this);
        mRelativeLayoutLeadFollowupDate.setOnClickListener(this);
        mApply.setOnClickListener(this);

        isSortingApplied();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.lsrl1:
                posteddateimg.setVisibility(View.VISIBLE);
                folowimg.setVisibility(View.INVISIBLE);
                if (postedFlag) {
                    postedFlag = false;
                    leadsortbystatus = "posted_date_l_o";
                    posteddateimg.animate().rotation(180).setDuration(180).start();
                    leads_posted_date_l_to_o.setText("Posted date(Latest to Oldest)");

                } else {
                    postedFlag = true;
                    leadsortbystatus = "posted_date_o_l";
                    posteddateimg.animate().rotation(0).setDuration(180).start();
                    leads_posted_date_l_to_o.setText("Posted date(Oldest to Latest)");

                }
                break;
            case R.id.lsrl2:
                folowimg.setVisibility(View.VISIBLE);
                posteddateimg.setVisibility(View.INVISIBLE);
                if (followFlag) {
                    followFlag = false;
                    leadsortbystatus = "folowup_date_l_o";
                    folowimg.animate().rotation(180).setDuration(180).start();
                    leads_followup_date_l_to_o.setText("Follow up date(Latest to Oldest)");
                } else {
                    followFlag = true;
                    leadsortbystatus = "folowup_date_o_l";
                    folowimg.animate().rotation(0).setDuration(180).start();
                    leads_followup_date_l_to_o.setText("Follow up date(Oldest to Latest)");

                }
                break;

            case R.id.stock_sort_apply:
                dismiss();
                SortInstanceLeadSection.getInstance().setSortby(leadsortbystatus);
                mSortedbyLead.onSortSelection();
                break;
            default:
                break;
        }

    }

    private void isSortingApplied() {

        if (SortInstanceLeadSection.getInstance().getSortby().equals("posted_date_l_o")) {
            posteddateimg.setVisibility(View.VISIBLE);
            folowimg.setVisibility(View.INVISIBLE);
            posteddateimg.animate().rotation(180).setDuration(180).start();
            leads_posted_date_l_to_o.setText("Posted date(Latest to Oldest)");
            leadsortbystatus = "posted_date_l_o";
        } else if (SortInstanceLeadSection.getInstance().getSortby().equals("posted_date_o_l")) {
            posteddateimg.setVisibility(View.VISIBLE);
            folowimg.setVisibility(View.INVISIBLE);
            posteddateimg.animate().rotation(0).setDuration(180).start();
            leads_posted_date_l_to_o.setText("Posted date(Oldest to Latest)");
            leadsortbystatus = "posted_date_o_l";
        } else if (SortInstanceLeadSection.getInstance().getSortby().equals("folowup_date_l_o")) {
            posteddateimg.setVisibility(View.INVISIBLE);
            folowimg.setVisibility(View.VISIBLE);
            folowimg.animate().rotation(180).setDuration(180).start();
            leads_followup_date_l_to_o.setText("Follow up date(Latest to Oldest)");
            leadsortbystatus = "folowup_date_l_o";
        } else if (SortInstanceLeadSection.getInstance().getSortby().equals("folowup_date_o_l")) {
            posteddateimg.setVisibility(View.INVISIBLE);
            folowimg.setVisibility(View.VISIBLE);
            folowimg.animate().rotation(0).setDuration(180).start();
            leads_followup_date_l_to_o.setText("Follow up date(Oldest to Latest)");
            leadsortbystatus = "folowup_date_o_l";
        }
    }
}
