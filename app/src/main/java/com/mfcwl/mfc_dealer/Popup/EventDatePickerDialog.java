package com.mfcwl.mfc_dealer.Popup;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.mfcwl.mfc_dealer.Interface.LeadSection.FilterSelected;
import com.mfcwl.mfc_dealer.R;


public class EventDatePickerDialog extends Dialog implements View.OnClickListener{

    Activity activity;
    private String startDateString="",endDateString="";
    private String dateType;
    private Button mYes;
    private FilterSelected mFilterSelected;

    private String TAG=EventDatePickerDialog.class.getSimpleName();
    public EventDatePickerDialog(Activity activity)
    {
        super(activity);
        this.activity=activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_event_dialog);

    }


    @Override
    public void onClick(View v) {

    }
}
