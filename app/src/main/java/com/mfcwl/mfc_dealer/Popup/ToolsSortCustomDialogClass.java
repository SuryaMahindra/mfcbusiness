package com.mfcwl.mfc_dealer.Popup;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.R;

public class ToolsSortCustomDialogClass  extends Dialog implements View.OnClickListener{

    public Context activity;
    public static Activity a;
    public Dialog d;
    public Button yes;
    public static TextView leads_posted_date_l_to_o,leads_followup_date_l_to_o;
    public static ImageView posteddateimg,folowimg;
    public static RelativeLayout lsrl1,lsrl2;
    public static String leadsortbystatus = "";
    public boolean postedFlag=false;
    public boolean followFlag=false;
    public ToolsSortCustomDialogClass(@NonNull Context context, Activity a) {
        super(context);
        this.activity = context;
        ToolsSortCustomDialogClass.a = a;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.stock_sort_apply:
                dismiss();

                break;
            default:
                break;
        }
        dismiss();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.lead_sort_dialog);

        leads_posted_date_l_to_o = findViewById(R.id.leads_posted_date_l_to_o);
        leads_followup_date_l_to_o = findViewById(R.id.leads_followup_date_l_to_o);

        posteddateimg = findViewById(R.id.posteddateimg);
        folowimg = findViewById(R.id.folowimg);
        posteddateimg.setVisibility(View.VISIBLE);
        posteddateimg.animate().rotation(180).setDuration(180).start();
        folowimg.setVisibility(View.INVISIBLE);
        lsrl1 = findViewById(R.id.lsrl1);
        lsrl2 = findViewById(R.id.lsrl2);




        lsrl1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                posteddateimg.setVisibility(View.VISIBLE);
                folowimg.setVisibility(View.INVISIBLE);



            }
        });
        lsrl2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                folowimg.setVisibility(View.VISIBLE);
                posteddateimg.setVisibility(View.INVISIBLE);

            }
        });

        yes = findViewById(R.id.stock_sort_apply);
        yes.setOnClickListener(this);

    }

}
