package com.mfcwl.mfc_dealer.Popup.LeadSection;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.archit.calendardaterangepicker.customviews.DateRangeCalendarView;
import com.mfcwl.mfc_dealer.Interface.LeadSection.FilterSelected;
import com.mfcwl.mfc_dealer.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class CalendarFilterDialog extends Dialog implements View.OnClickListener{


    Activity activity;
    private DateRangeCalendarView mCalendar;
    private String startDateString="",endDateString="";
    private String dateType;
    private Button mYes,mCancel;
    private FilterSelected mFilterSelected;

    Context mContext;
    public static CalendarFilterDialog mFilterCon;
    public CalendarFilterDialog(Activity activity,String dateType) {
        super(activity);
        this.activity= activity;
        mFilterSelected =(FilterSelected) activity;
        this.dateType = dateType;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_view_leads_dialog);
        mContext = getContext();
        mFilterCon = CalendarFilterDialog.this;
        startDateString="";
        endDateString="";
        initView();

    }

    private void initView() {

        mCalendar = (DateRangeCalendarView) findViewById(R.id.calendar);
        mCalendar.setCalendarListener(mListener);
        //mCalendar.setFonts(CustomFonts.getNexaBold(activity));
        mYes = (Button)findViewById(R.id.buttonok);
        mYes.setOnClickListener(this);
        mCancel=findViewById(R.id.btncancel);
        mCancel.setOnClickListener(this);
    }

    private DateRangeCalendarView.CalendarListener  mListener = new DateRangeCalendarView.CalendarListener() {
        @Override
        public void onFirstDateSelected(Calendar startDate) {
            //Toast.makeText(getActivity(), "Start Date: " + startDate.getTime().toString(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onDateRangeSelected(Calendar startDate, Calendar endDate) {


            Date dateStart = new Date(startDate.getTimeInMillis());
            Date dateEnd = new Date(endDate.getTimeInMillis());
            SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
            startDateString = dateformat.format(dateStart);
            endDateString = dateformat.format(dateEnd);
            Log.e("DateRange ","dateStart "+dateformat.format(dateStart)+" dateEnd "+dateformat.format(dateEnd));

            // Toast.makeText(getActivity(), "Start Date: " + startDate.getTime().toString() + " End date: " + endDate.getTime().toString(), Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonok:
                mFilterSelected.onFilterSelection(startDateString,endDateString,dateType);
                dismiss();
                break;

            case R.id.btncancel:
                dismiss();
                break;

            default:
                break;
        }
        //dismiss();
    }


}
