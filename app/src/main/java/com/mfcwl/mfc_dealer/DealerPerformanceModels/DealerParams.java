package com.mfcwl.mfc_dealer.DealerPerformanceModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DealerParams {

    @SerializedName("Opening_Stock")
    @Expose
    public List<Integer> openingStock = null;
    @SerializedName("Sales_Plan")
    @Expose
    public List<Integer> salesPlan = null;
    @SerializedName("Procurement_Plan")
    @Expose
    public List<Integer> procurementPlan = null;
    @SerializedName("Closing_Stock")
    @Expose
    public List<Integer> closingStock = null;
    @SerializedName("Warranty_Units")
    @Expose
    public List<Integer> warrantyUnits = null;
    @SerializedName("Warranty_Value")
    @Expose
    public List<Integer> warrantyValue = null;
    @SerializedName("Procurement_from_IEP")
    @Expose
    public List<Integer> procurementFromIEP = null;
    @SerializedName("Procurement_from_CPT")
    @Expose
    public List<Integer> procurementFromCPT = null;
    @SerializedName("Walk_ins")
    @Expose
    public List<Integer> walkIns = null;
    @SerializedName("Sales_Conversion")
    @Expose
    public List<Integer> salesConversion = null;
    @SerializedName("OMS_Leads")
    @Expose
    public List<Integer> oMSLeads = null;
    @SerializedName("Conversion_through_OMS_Leads")
    @Expose
    public List<Integer> conversionThroughOMSLeads = null;

}
