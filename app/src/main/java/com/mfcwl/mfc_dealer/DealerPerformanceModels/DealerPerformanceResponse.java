package com.mfcwl.mfc_dealer.DealerPerformanceModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DealerPerformanceResponse {

    @SerializedName("Status")
    @Expose
    public String status;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;

}
