package com.mfcwl.mfc_dealer.DealerPerformanceModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("Dealer_name")
    @Expose
    public String dealerName;
    @SerializedName("dealer_code")
    @Expose
    public String dealerCode;
    @SerializedName("Parameters")
    @Expose
    public DealerParams parameters;

}
