package com.mfcwl.mfc_dealer.PayUPayment;

import com.mfcwl.mfc_dealer.BuildConfig;

public class PayUPaymentConfig {
    public static final String MESSENGER_INTENT_KEY
            = BuildConfig.APPLICATION_ID + ".MESSENGER_INTENT_KEY";
    public static final String WORK_DURATION_KEY =
            BuildConfig.APPLICATION_ID + ".WORK_DURATION_KEY";
}
