package com.mfcwl.mfc_dealer.PayUPayment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentRequest {
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("remark")
    @Expose
    private String remark;

    public String getPg_transaction_id() {
        return pg_transaction_id;
    }

    public void setPg_transaction_id(String pg_transaction_id) {
        this.pg_transaction_id = pg_transaction_id;
    }

    @SerializedName("pg_transaction_id")
    @Expose
    private String pg_transaction_id;

    public String getPg_transaction_datetime_yyyymmddhhmmss() {
        return pg_transaction_datetime_yyyymmddhhmmss;
    }

    public void setPg_transaction_datetime_yyyymmddhhmmss(String pg_transaction_datetime_yyyymmddhhmmss) {
        this.pg_transaction_datetime_yyyymmddhhmmss = pg_transaction_datetime_yyyymmddhhmmss;
    }

    @SerializedName("pg_transaction_datetime_yyyymmddhhmmss")
    @Expose
    private String pg_transaction_datetime_yyyymmddhhmmss;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
