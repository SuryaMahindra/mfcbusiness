package com.mfcwl.mfc_dealer.PayUPayment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PayUPaymentResponse {
    @SerializedName("ErrorStatus")
    @Expose
    private Object errorStatus;
    @SerializedName("ErrorShortMessage")
    @Expose
    private String errorShortMessage;
    @SerializedName("DateCreated")
    @Expose
    private String dateCreated;
    @SerializedName("ErrorMessage")
    @Expose
    private Object errorMessage;
    @SerializedName("ErrorStatusCode")
    @Expose
    private String errorStatusCode;

    public Object getErrorStatus() {
        return errorStatus;
    }

    public void setErrorStatus(Object errorStatus) {
        this.errorStatus = errorStatus;
    }

    public String getErrorShortMessage() {
        return errorShortMessage;
    }

    public void setErrorShortMessage(String errorShortMessage) {
        this.errorShortMessage = errorShortMessage;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Object getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(Object errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorStatusCode() {
        return errorStatusCode;
    }

    public void setErrorStatusCode(String errorStatusCode) {
        this.errorStatusCode = errorStatusCode;
    }
}
