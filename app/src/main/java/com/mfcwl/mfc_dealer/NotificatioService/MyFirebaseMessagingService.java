/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.mfcwl.mfc_dealer.NotificatioService;


import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.activity;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.hideOption;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    public static int id = 0;
    public static String notificationsStatus = "";

    public static String noStockImage = "";
    public static String lead = "";
    public static String leadFollowUp = "";
    public static String AgeingInventory = "";
    Bitmap d;
    Map data;
    Bitmap lanucherbit;
    RemoteMessage remoteMessage;
    PendingIntent resultPendingIntent;
    String notification, description, external_uri, SocietyId, eventId, eventType, imageurl;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        data = remoteMessage.getData();
        this.remoteMessage = remoteMessage;

        Log.d(TAG, "From-----: " + remoteMessage.getFrom() + "======" + data.get("event") + "=====" + remoteMessage.getData().get("actionevent") + "---" + remoteMessage.toString() + "--" + data.get("event") + "--" + remoteMessage.getData().toString());
        Log.d(TAG, "NotificationApp Message Body: " + remoteMessage.getNotification().getBody() + "---" + remoteMessage.getNotification().getClickAction());

        notificationsStatus = remoteMessage.getData().get("actionevent");

        Log.e("notificationsStatus", "notificationsStatus" + notificationsStatus);

        if (notificationsStatus != null) {
            if (notificationsStatus.equalsIgnoreCase("noStockImage")) {
                noStockImage = notificationsStatus;
            }
            if (notificationsStatus.equalsIgnoreCase("lead")) {
                lead = notificationsStatus;
            }

            if (notificationsStatus.equalsIgnoreCase("leadFollowUp")) {
                leadFollowUp = notificationsStatus;
            }
            if (notificationsStatus.equalsIgnoreCase("AgeingInventory")) {
                AgeingInventory = notificationsStatus;

            }
        }

        //  if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {

        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
        notificationUtils.playNotificationSound();

        if (MainActivity.activity != null) {

            CommonMethods.setvalueAgainstKey(activity, "nofication_status", "true");
            if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("Home")) {
                hideOption(R.id.notification_bell);
            }
        }

        try {
            if (CommonMethods.getstringvaluefromkey(activity, "token").equals("")) {
                //  Log.e("notification_acesstoken", CommonMethods.getstringvaluefromkey(MainActivity.activity, "access_token"));
            } else {
                sendNotifications(remoteMessage.getNotification().getBody(), remoteMessage.getData().get("actionevent")
                        , remoteMessage.getNotification().getTitle());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void sendNotifications(String notify, String lead, String title) {

        notificationsStatus = lead;
        Bitmap icon = BitmapFactory.decodeResource(activity.getResources(),
                R.drawable.ic_launcher);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setLargeIcon(icon)
                        .setContentTitle(title)
                        .setAutoCancel(true)
                        .setContentText(notify);

//============================== for multiple cases use switch case here
     //   Log.e("PendingIntent", "PendingIntent-1=" + lead);
        //Log.e("Notification text", lead);
        if (lead != null) {
          //  Log.e("PendingIntent", "PendingIntent-2");

            // Log.e("Notification text", lead);
            if (lead.equalsIgnoreCase("lead")) {
             //   Log.e("PendingIntent", "PendingIntent-3");
                this.lead = lead;

                PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                        new Intent(this, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
                builder.setContentIntent(contentIntent);

                // Add as notification
                NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                manager.notify(1, builder.build());

            } else if (lead.equalsIgnoreCase("noStockImage")) {
              //  Log.e("PendingIntent", "PendingIntent-3");
                noStockImage = lead;

                PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                        new Intent(this, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
                builder.setContentIntent(contentIntent);

                // Add as notification
                NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                manager.notify(2, builder.build());

            } else if (lead.equalsIgnoreCase("leadFollowUp")) {
             //   Log.e("PendingIntent", "PendingIntent-3");
                leadFollowUp = lead;

                PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                        new Intent(this, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
                builder.setContentIntent(contentIntent);

                // Add as notification
                NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                manager.notify(3, builder.build());

            } else if (lead.equalsIgnoreCase("AgeingInventory")) {
             //   Log.e("PendingIntent", "PendingIntent-3");
                AgeingInventory = lead;
                PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                        new Intent(this, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
                builder.setContentIntent(contentIntent);

                // Add as notification
                NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                manager.notify(4, builder.build());

            } else {

                PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                        new Intent(this, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
                builder.setContentIntent(contentIntent);

                // Add as notification
                NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                manager.notify(id, builder.build());

            }
        } else {

            PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                    new Intent(this, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(contentIntent);

            // Add as notification
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            manager.notify(id, builder.build());
        }

    }


    @Nullable
    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }
}
