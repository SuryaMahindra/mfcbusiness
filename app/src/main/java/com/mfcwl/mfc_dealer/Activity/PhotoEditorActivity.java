package com.mfcwl.mfc_dealer.Activity;

import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;

import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import java.io.IOException;

import ja.burhanrashid52.photoeditor.PhotoEditor;
import ja.burhanrashid52.photoeditor.PhotoEditorView;


public class PhotoEditorActivity extends AppCompatActivity {

    PhotoEditor mPhotoEditor;
    Uri imageUri;
    Bitmap bmpimg;
    private PhotoEditorView mPhotoEditorView;
    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_editor);
        Log.i(TAG, "onCreate: ");
        mPhotoEditorView = findViewById(R.id.photoEditorView);
        mPhotoEditorView.getSource().setImageResource(R.drawable.no_car_small);
        imageUri = ImageUploadActivity1.image1;/*Uri.parse(getIntent().getExtras().getString("imageUri"));*/

        try {
            bmpimg = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
        } catch (IOException e) {

            Log.e("IOException", "IOException=" + e);
        }

        //Use custom font using latest support library
        //Typeface mTextRobotoTf = ResourcesCompat.(this, R.);

        Typeface font = Typeface.createFromAsset(getAssets(), "lato_semibold.ttf");

        //loading font from assest
        Typeface mEmojiTypeFace = Typeface.createFromAsset(getAssets(), "lato_semibold.ttf");

        mPhotoEditorView.getSource().setImageBitmap(bmpimg);

        try {
            mPhotoEditor = new PhotoEditor.Builder(this, mPhotoEditorView)
                    .setPinchTextScalable(true)
                    .setDefaultTextTypeface(font)
                    .setDefaultEmojiTypeface(mEmojiTypeFace)
                    .build();
        } catch (Exception ee) {
            Log.e("mPhotoEditor", "mPhotoEditor=" + ee);

        }


    }

    @Override
    public void onResume() {
        super.onResume();
        // put your code here...

        Application.getInstance().trackScreenView(this,GlobalText.PhotoEditor_Activity);

    }
}
