package com.mfcwl.mfc_dealer.Activity.RDR;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.AppConstants;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.DealerReportRequest;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import imageeditor.base.BaseActivity;
import mteams.MTeamsLogin;
import rdm.RDRAddMeetingAdapter;
import rdm.model.EventData;
import rdm.model.EventResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static rdm.retrofit_services.RetroBaseService.URL_END_SCHEDULE_EVENT;
import static rdm.retrofit_services.RetroBaseService.retrofitInterface;

public class DiscussionPoint10 extends BaseActivity implements Callback<Object> {

    TextView tvMonthAndYearLbl;
    Button btnScheduleAnotherRDR, btnDPAddMeetingSubmit;
    RecyclerView rv_rgm_add_meeting;
    String startDate = "", endDate = "";
    private String startDateVal = "2020-07-13";
    private String format = "yyyy-MM-dd";
    private String formatExpected = "dd MMM";

    final Calendar cal = Calendar.getInstance();
    private SimpleDateFormat sdf = new SimpleDateFormat(format);

    private Activitykiller mKiller;
    private RDRAddMeetingAdapter addMeetingAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discussion_point9);
        Log.i(TAG, "onCreate: ");
        initView();

        setListeners();

        mKiller = new Activitykiller();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(mKiller, intentFilter);
    }

    public void initView() {

        tvMonthAndYearLbl = findViewById(R.id.tv_add_meeting_monthAndYearLbl);
        btnScheduleAnotherRDR = findViewById(R.id.btnScheduleAnotherRDR);
        btnDPAddMeetingSubmit = findViewById(R.id.btnDPAddMeetingSubmit);
        rv_rgm_add_meeting = findViewById(R.id.rgm_add_meeting);
        tvMonthAndYearLbl.setText(CommonMethods.getMonthNameAndYear(this,CommonMethods.getTodayDate(this)));
        startDateVal = sdf.format(cal.getTime());
        startDate = CommonMethods.getTodayAndTime(this);
        endDate = "2024-08-07 01:10:39";

        retrofitInterface.getFromWeb(URL_END_SCHEDULE_EVENT, startDate, endDate).enqueue(this);
        SpinnerManager.showSpinner(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        rv_rgm_add_meeting.setLayoutManager(layoutManager);
    }

    public void setListeners() {
        btnScheduleAnotherRDR.setOnClickListener(this);
        btnDPAddMeetingSubmit.setOnClickListener(this);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mKiller);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            case R.id.btnScheduleAnotherRDR:
                navigateToMTeamsLogin();
                break;

            case R.id.btnDPAddMeetingSubmit:
                submitToWeb(getRequest());

                break;
        }
    }

    private void navigateToMTeamsLogin()
    {
        Intent intent=new Intent(DiscussionPoint10.this, MTeamsLogin.class);
        intent.putExtra(AppConstants.DEALER_CODE,dealerId);
        intent.putExtra(AppConstants.DEALER_NAME,dealerName);
        intent.putExtra(AppConstants.MEETING_TYPE,"ScheduleAnotherRDR");
        startActivity(intent);
    }


    private DealerReportRequest getRequest() {
        DealerReportRequest request = new DealerReportRequest();
        request.dealerCode = dealerReportResponse.dealerCode;
        request.followUpDate = getTimeMMddyyy();
        if(DiscussionPoint1.meetinglink.isEmpty())
        {
            request.videoUrl="";
        }
        else
        {
            request.videoUrl = DiscussionPoint1.meetinglink;
        }

        request.points = discussionPoints;
        request.isOperational="Yes";

        return request;
    }

    @Override
    public void onResponse(Call<Object> call, Response<Object> response) {
        try {
            SpinnerManager.hideSpinner(this);
            String strRes = new Gson().toJson(response.body());
            Log.i(TAG, "onResponse: " + strRes);
            EventResponse eventResponse = new Gson().fromJson(strRes, EventResponse.class);
            if (eventResponse.getIsSuccess()) {
                Log.i(TAG, "onResponse: " + eventResponse.getIsSuccess());
                if(eventResponse.getData().size()>0)
                {
                    btnScheduleAnotherRDR.setText(getResources().getString(R.string.schedule_another_rdr));
                }else
                {
                    btnScheduleAnotherRDR.setText(getResources().getString(R.string.schedule_rdr));
                }

                setToAddMeetingAdapter(eventResponse.getData());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void onFailure(Call<Object> call, Throwable t) {

    }

    private void setToAddMeetingAdapter(List<EventData> data) {
        try{
            if(!data.isEmpty())
            {
                addMeetingAdapter = new RDRAddMeetingAdapter(DiscussionPoint10.this, data);
                rv_rgm_add_meeting.setAdapter(addMeetingAdapter);
            }
            else
            {
                CommonMethods.alertMessage(this,getResources().getString(R.string.no_data_found));
            }
        }catch(Exception exception){exception.printStackTrace();}

    }

}
