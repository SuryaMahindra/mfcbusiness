package com.mfcwl.mfc_dealer.Activity.RDR;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ScreenDataResponse {

    @SerializedName("dealer_code")
    @Expose
    public String dealerCode = "";
    @SerializedName("sales_target")
    @Expose
    public String salesTarget = "";
    @SerializedName("stocks_target")
    @Expose
    public String stocksTarget = "";
    @SerializedName("store_space_occupied")
    @Expose
    public String storeSpaceOccupied = "";
    @SerializedName("store_space_occupied_percent")
    @Expose
    public String storeSpaceOccupiedPercent = "";
    @SerializedName("paid_up_stock")
    @Expose
    public String paidUpStock = "";
    @SerializedName("paid_up_stock_percentage")
    @Expose
    public String paidUpStockPercentage = "";
    @SerializedName("park_and_sell_stock")
    @Expose
    public String parkAndSellStock = "";
    @SerializedName("park_and_sell_stock_percentage")
    @Expose
    public String parkAndSellStockPercentage = "";
    @SerializedName("free_space")
    @Expose
    public String freeSpace = "";
    @SerializedName("free_space_percentage")
    @Expose
    public String freeSpacePercentage = "";
    @SerializedName("procurement_target_stocks")
    @Expose
    public String procurementTargetStocks = "";
    @SerializedName("procurement_target_iep")
    @Expose
    public String procurementTargetIep = "";
    @SerializedName("procurement_target_cpt")
    @Expose
    public String procurementTargetCpt = "";
    @SerializedName("stocks_added")
    @Expose
    public String stocksAdded = "";
    @SerializedName("bids_won")
    @Expose
    public String bidsWon = "";
    @SerializedName("stocks_in_refurbishment")
    @Expose
    public String stocksInRefurbishment = "";
    @SerializedName("live_auctions_iep")
    @Expose
    public String liveAuctionsIep = "";
    @SerializedName("auctions_bids_on_iep")
    @Expose
    public String auctionsBidsOnIep = "";
    @SerializedName("auctions_bids_won_iep")
    @Expose
    public String auctionsBidsWonIep = "";
    @SerializedName("leads_oms_iep")
    @Expose
    public String leadsOmsIep = "";
    @SerializedName("leads_actual_iep")
    @Expose
    public String leadsActualIep = "";
    @SerializedName("procured_oms_iep")
    @Expose
    public String procuredOmsIep = "";
    @SerializedName("procured_actual_iep")
    @Expose
    public String procuredActualIep = "";
    @SerializedName("live_auctions_cpt")
    @Expose
    public String liveAuctionsCpt = "";
    @SerializedName("auctions_bids_on_cpt")
    @Expose
    public String auctionsBidsOnCpt = "";
    @SerializedName("auctions_bids_won_cpt")
    @Expose
    public String auctionsBidsWonCpt = "";
    @SerializedName("leads_oms_cpt")
    @Expose
    public String leadsOmsCpt = "";
    @SerializedName("leads_actual_cpt")
    @Expose
    public String leadsActualCpt = "";
    @SerializedName("procured_oms_cpt")
    @Expose
    public String procuredOmsCpt = "";
    @SerializedName("procured_actual_cpt")
    @Expose
    public String procuredActualCpt = "";
    @SerializedName("meetings")
    @Expose
    public String meetings = "";
    @SerializedName("commercial_closed")
    @Expose
    public String commercialClosed = "";
    @SerializedName("vehicles_procured_partnership")
    @Expose
    public String vehiclesProcuredPartnership = "";
    @SerializedName("leads_target")
    @Expose
    public String leadsTarget = "";
    @SerializedName("leads_generated")
    @Expose
    public String leadsGenerated = "";
    @SerializedName("leads_walkin")
    @Expose
    public String leadsWalkin = "";
    @SerializedName("leads_walkin_cold")
    @Expose
    public String leadsWalkinCold = "";
    @SerializedName("warranty_units_target")
    @Expose
    public String warrantyUnitsTarget = "";
    @SerializedName("warranty_units_sold")
    @Expose
    public String warrantyUnitsSold = "";
    @SerializedName("warranty_units_sold_price")
    @Expose
    public String warrantyUnitsSoldPrice = "";
    @SerializedName("warranty_na_cases")
    @Expose
    public String warrantyNaCases = "";
    @SerializedName("warranty_na_cases_followed_up")
    @Expose
    public String warrantyNaCasesFollowedUp = "";
    @SerializedName("responsibilities")
    @Expose
    public List<String> responsibilities = null;
    @SerializedName("StockAdequacy")
    @Expose
    public List<String> stockAdequacy = null;
    @SerializedName("StocksInProgress")
    @Expose
    public List<String> stocksInProgress = null;
    @SerializedName("ProcurementIEP")
    @Expose
    public List<String> procurementIEP = null;
    @SerializedName("ProcurementCPT")
    @Expose
    public List<String> procurementCPT = null;
    @SerializedName("LocalPartnership")
    @Expose
    public List<String> localPartnership = null;
    @SerializedName("WarrantySales")
    @Expose
    public List<String> warrantySales = null;
    @SerializedName("LeadsAdequacy")
    @Expose
    public List<String> leadsAdequacy = null;

    @SerializedName("bookings_in_hand")
    @Expose
    public String bookingsInHand;
    @SerializedName("finance_cases")
    @Expose
    public String financeCases;
    @SerializedName("finance_case_penetration")
    @Expose
    public Object financeCasePenetration;
    @SerializedName("update_on_finance_cases")
    @Expose
    public Object updateOnFinanceCases;
    @SerializedName("pending_RC_90days")
    @Expose
    public Object pendingRC90days;
    @SerializedName("mibl_cases")
    @Expose
    public Object miblCases;

    @SerializedName("RetailSales")
    @Expose
    public List<String> retailSales = null;

    @SerializedName("stocks_certified")
    @Expose
    public String stockscertified;

    @SerializedName("stocks_without_image")
    @Expose
    public String stockswithoutimage;

    @SerializedName("royalty_collected")
    @Expose
    public String royaltycollected;

    @SerializedName("retail_sales_target")
    @Expose
    public String retailsalestarget;

    //R4

    @SerializedName("current_stock")
    @Expose
    public Object currentStock;
    @SerializedName("procurement_Xmart")
    @Expose
    public Object procurementXmart;
    @SerializedName("self_procurement")
    @Expose
    public Object selfProcurement;
    @SerializedName("vehicle_not_per_requirement_iep")
    @Expose
    public Object vehicleNotPerRequirementIep;
    @SerializedName("negotiations_iep")
    @Expose
    public String negotiationsIep;
    @SerializedName("vehicle_not_per_requirement_cpt")
    @Expose
    public Object vehicleNotPerRequirementCpt;
    @SerializedName("negotiations_cpt")
    @Expose
    public String negotiationsCpt;
    @SerializedName("qualilified_leads")
    @Expose
    public Object qualilifiedLeads;
    @SerializedName("vehicles_closed")
    @Expose
    public Object vehiclesClosed;
    @SerializedName("repeat_leads")
    @Expose
    public Object repeatLeads;
    @SerializedName("walk_ins")
    @Expose
    public String walkIns;
    @SerializedName("booking_OMS")
    @Expose
    public String bookingOMS;

    @SerializedName("retail_sales")
    @Expose
    private String retailSalesVal;

    public String getRetailSalesVal() {
        return retailSalesVal;
    }

    public void setRetailSales(String retailSalesVal) {
        this.retailSalesVal = retailSalesVal;
    }

    @SerializedName("total_sales")
    @Expose
    public String totalSales;
    @SerializedName("conversion_OMS")
    @Expose
    public String conversionOMS;
    @SerializedName("royalty_amount")
    @Expose
    public Object royaltyAmount;
    @SerializedName("aggregator_cases")
    @Expose
    public Object aggregatorCases;


}