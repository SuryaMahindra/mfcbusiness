package com.mfcwl.mfc_dealer.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.Global;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import at.grabner.circleprogress.CircleProgressView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.mfcwl.mfc_dealer.Utility.CommonMethods.CAMERA_RESCALE_VALUE;
import static com.mfcwl.mfc_dealer.Utility.CommonMethods.GALLERY_RESCALE_VALUE;

public class ImageUploadActivity extends AppCompatActivity {

    @BindView(R.id.imagedonebtn)
    public Button imagedonebtn;

    @BindView(R.id.exteriorTab)
    public TextView exteriorTab;
    @BindView(R.id.interiorTab)
    public TextView interiorTab;
    @BindView(R.id.exteriorItem)
    public LinearLayout exteriorItem;
    @BindView(R.id.interiorItem)
    public LinearLayout interiorItem;

    @BindView(R.id.samplesbtn)
    public Button samplesbtn;


    @BindView(R.id.coverimg)
    public ImageView coverimg;
    @BindView(R.id.coverimgTrans)
    public ImageView coverimgTrans;


    @BindView(R.id.frontview)
    public ImageView frontview;
    @BindView(R.id.frontviewTrans)
    public ImageView frontviewTrans;


    @BindView(R.id.rearview)
    public ImageView rearview;
    @BindView(R.id.rearviewTrans)
    public ImageView rearviewTrans;


    @BindView(R.id.leftsideview)
    public ImageView leftsideview;

    @BindView(R.id.rightsideview)
    public ImageView rightsideview;


    @BindView(R.id.rearleftview)
    public ImageView rearleftview;


    @BindView(R.id.frontright)
    public ImageView frontright;

    @BindView(R.id.rearright)
    public ImageView rearright;

    @BindView(R.id.wheelimg)
    public ImageView wheelimg;

    @BindView(R.id.frontseat)
    public ImageView frontseat;

    @BindView(R.id.backseat)
    public ImageView backseat;

    @BindView(R.id.odometer)
    public ImageView odometer;

    @BindView(R.id.dasborad)
    public ImageView dasborad;

    @BindView(R.id.steering)
    public ImageView steering;

    @BindView(R.id.frontrowseat)
    public ImageView frontrowseat;

    @BindView(R.id.backrowseat)
    public ImageView backrowseat;

    @BindView(R.id.doorimg)
    public ImageView doorimg;

    @BindView(R.id.backseatsideTrans)
    public ImageView backseatsideTrans;

    @BindView(R.id.frontseatsideTrans)
    public ImageView frontseatsideTrans;

    @BindView(R.id.frontseatTrans)
    public ImageView frontseatTrans;

    @BindView(R.id.steeringTrans)
    public ImageView steeringTrans;


    @BindView(R.id.dasboardTrans)
    public ImageView dasboardTrans;

    @BindView(R.id.odometerTrans)
    public ImageView odometerTrans;

    @BindView(R.id.backseatTrans)
    public ImageView backseatTrans;

    @BindView(R.id.doorTrans)
    public ImageView doorTrans;

    //reena
    @BindView(R.id.image_upload_count)
    public TextView imageUploadCountTxt;

    public ImageView coverimgClose;
    public ImageView frontviewClose;
    public ImageView rearviewClose;
    public ImageView leftsideview_close;
    public ImageView rightsideview_close;
    public ImageView rearleftview_close;
    public ImageView frontright_close;
    public ImageView rearright_close;
    public ImageView wheelimg_close;
    public ImageView frontseat_close;
    public ImageView backseat_close;
    public ImageView odometer_close;
    public ImageView dasborad_close;
    public ImageView steering_close;
    public ImageView frontrowseat_close;
    public ImageView backrowseat_close;
    public ImageView doorimg_close;


    static CircleProgressView mCircleView_front, mCircleView_rear, mCircleView_leftside, mCircleView_rightside, mCircleView_rearleft, mCircleView_frontright, mCircleView_rearright, mCircleView_wheels,
            mCircleView_frontseat,
            mCircleView_backseat,
            mCircleView_odameter,
            mCircleView_dasboard,
            mCircleView_steering,
            mCircleView_frontseatside,
            mCircleView_backseatside,
            mCircleView_door,
            mCircleView_cover;


    static String cover_image = "cover_image", front_image = "front_image", rear_image = "rear_image", leftside_image = "leftside_image", rightside_image = "rightside_image",
            rearleft_image = "rearleft_image", frontright_image = "frontright_image", rearright_image = "rear_right_view",
            front_seats_image = "front_seats",
            wheels_image = "wheels_tyres",
            back_seats_image = "back_seats",
            odometer_image = "odometer",
            dashboard_image = "dashboard",
            steering_image = "steering",
            front_row_side_image = "front_row_side",
            back_row_side_image = "back_row_side",
            trunk_door_open_view_image = "trunk_door_open_view";

    public boolean coverimgFlag,
            frontviewFlag,
            leftsideviewFlag,
            rightsideviewFlag,
            rearleftviewFlag,
            rearrightviewFlag,
            rearviewFlag,
            frontrightviewFlag,
            wheelstviewFlag,
            front_seatsFlag,
            back_seatsFlag,
            odometerFlag,
            dashboardFlag,
            steeringFlag,
            front_row_sideFlag,
            back_row_sideFlag,
            trunk_door_open_viewFlag;

    boolean front_image_flag = true,
            rear_image_flag = true,
            leftside_image_flag = true,
            right_image_flag = true,
            rearleft_image_flag = true,
            rearright_image_flag = true,
            frontright_image_flag = true;

    File file;
    Uri fileUri;

    private static final int MY_PERMISSIONS_READ_PHONE_STATE = 0;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 0;

    int width_x = 700, height_y = 700;
    boolean whichSource = false;
    public static Activity activity;
    //private ImageDataForSync imageDataForSync;

    LocationManager locationManager;
    String mprovider;
    LocationListener mlocListener;
    String[] imageTypeSending;
    JSONArray jsonArray;

    TextView coverimglbl,
            frontviewlbl,
            rearviewlbl,
            leftsidelbl,
            rightsidelbl,
            rearleftlbl,
            frontrightlbl,
            rearrightlbl,
            wheelslbl,
            frontseatlbl,
            backseatlbl,
            odameterlbl,
            dasboardlbl,
            steeringlbl,
            frontrowlbl,
            backrowlbl,
            doorlbl;

    int cover_camera_request = 2001,
            front_camera_request = 2002,
            rear_camera_request = 2003,
            leftside_camera_request = 2004,
            rightside_camera_request = 2005,
            rearleft_camera_request = 2006,
            frontright_camera_request = 2007,
            rearright_camera_request = 2008,
            wheels_camera_request = 2009,
            front_seats_camera_request = 2010,
            back_seats_camera_request = 2011,
            odometer_camera_request = 2012,
            dashboard_camera_request = 2013,
            steering_camera_request = 2014,
            front_row_side_camera_request = 2015,
            back_row_side_camera_request = 2016,
            trunk_door_open_view_camera_request = 2017,

    cover_gallery_request = 1000,
            front_gallery_request = 1001,
            rear_gallery_request = 1002,
            leftside_gallery_request = 1003,
            rightside_gallery_request = 1004,
            rearleft_gallery_request = 1005,
            frontright_gallery_request = 1006,
            rearright_gallery_request = 1007,
            wheels_gallery_request = 1008,

    front_seats_gallery_request = 1009,
            back_seats_gallery_request = 1010,
            odometer_gallery_request = 1011,
            dashboard_gallery_request = 1012,
            steering_gallery_request = 1013,
            front_row_side_gallery_request = 1014,
            back_row_side_gallery_request = 1015,
            trunk_door_open_view_gallery_request = 1016;


    public String[] imageurl;
    public String[] imagename;
    String stock_id;

    public static int imgComp = 4;
    public ImageView uploadback;
    public String photoCountForuploadimage;
    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CommonMethods.MemoryClears();
        setContentView(R.layout.activity_imageupload);
        Log.i(TAG, "onCreate: ");

        ButterKnife.bind(this);
        CommonMethods.MemoryClears();
        Log.i(TAG, "onCreate: ");
        activity = this;
        exteriorTab.setTextColor(getResources().getColor(R.color.yellow));
        interiorTab.setTextColor(getResources().getColor(R.color.White));

        exteriorItem.setVisibility(View.VISIBLE);
        interiorItem.setVisibility(View.GONE);
        uploadback = (ImageView) findViewById(R.id.uploadback);

        coverimgClose = (ImageView) findViewById(R.id.coverimgClose);
        frontviewClose = (ImageView) findViewById(R.id.frontviewClose);
        rearviewClose = (ImageView) findViewById(R.id.rearviewClose);
        leftsideview_close = (ImageView) findViewById(R.id.leftsideview_close);
        rightsideview_close = (ImageView) findViewById(R.id.rightsideview_close);
        rearleftview_close = (ImageView) findViewById(R.id.rearleftview_close);
        frontright_close = (ImageView) findViewById(R.id.frontright_close);
        rearright_close = (ImageView) findViewById(R.id.rearright_close);
        wheelimg_close = (ImageView) findViewById(R.id.wheelimg_close);
        frontseat_close = (ImageView) findViewById(R.id.frontseat_close);
        backseat_close = (ImageView) findViewById(R.id.backseat_close);
        odometer_close = (ImageView) findViewById(R.id.odometer_close);
        dasborad_close = (ImageView) findViewById(R.id.dasborad_close);
        steering_close = (ImageView) findViewById(R.id.steering_close);
        frontrowseat_close = (ImageView) findViewById(R.id.frontrowseat_close);
        backrowseat_close = (ImageView) findViewById(R.id.backrowseat_close);
        doorimg_close = (ImageView) findViewById(R.id.doorimg_close);

        coverimgClose.setVisibility(View.INVISIBLE);
        frontviewClose.setVisibility(View.INVISIBLE);
        rearviewClose.setVisibility(View.INVISIBLE);
        leftsideview_close.setVisibility(View.INVISIBLE);
        rightsideview_close.setVisibility(View.INVISIBLE);
        rearleftview_close.setVisibility(View.INVISIBLE);
        frontright_close.setVisibility(View.INVISIBLE);
        rearright_close.setVisibility(View.INVISIBLE);
        wheelimg_close.setVisibility(View.INVISIBLE);
        frontseat_close.setVisibility(View.INVISIBLE);
        backseat_close.setVisibility(View.INVISIBLE);
        odometer_close.setVisibility(View.INVISIBLE);
        dasborad_close.setVisibility(View.INVISIBLE);
        steering_close.setVisibility(View.INVISIBLE);
        frontrowseat_close.setVisibility(View.INVISIBLE);
        backrowseat_close.setVisibility(View.INVISIBLE);
        doorimg_close.setVisibility(View.INVISIBLE);


        //reena photocount
        photoCountForuploadimage = getIntent().getStringExtra("imageCount");

        imageUploadCountTxt.setText("Upload image " + "(" + photoCountForuploadimage + "/" + "17" + ")");

        if (CommonMethods.getstringvaluefromkey(this, "stocklist").equalsIgnoreCase("true")) {
            CommonMethods.setvalueAgainstKey(this, "stocklist", "false");
            AutoFillMethods();
        } else {

            stock_id = getIntent().getStringExtra("stock_id");
        }

        uploadback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        mCircleView_front = (CircleProgressView) findViewById(R.id.mCircleView_front);
        mCircleView_rear = (CircleProgressView) findViewById(R.id.mCircleView_rear);
        mCircleView_leftside = (CircleProgressView) findViewById(R.id.mCircleView_leftside);
        mCircleView_rightside = (CircleProgressView) findViewById(R.id.mCircleView_rightside);
        mCircleView_rearleft = (CircleProgressView) findViewById(R.id.mCircleView_rearleft);
        mCircleView_frontright = (CircleProgressView) findViewById(R.id.mCircleView_frontright);
        mCircleView_rearright = (CircleProgressView) findViewById(R.id.mCircleView_rearright);
        mCircleView_wheels = (CircleProgressView) findViewById(R.id.mCircleView_wheels);
        mCircleView_frontseat = (CircleProgressView) findViewById(R.id.mCircleView_frontseat);
        mCircleView_backseat = (CircleProgressView) findViewById(R.id.mCircleView_backseat);
        mCircleView_odameter = (CircleProgressView) findViewById(R.id.mCircleView_odameter);
        mCircleView_dasboard = (CircleProgressView) findViewById(R.id.mCircleView_dasboard);
        mCircleView_steering = (CircleProgressView) findViewById(R.id.mCircleView_steering);
        mCircleView_frontseatside = (CircleProgressView) findViewById(R.id.mCircleView_frontseatside);
        mCircleView_backseatside = (CircleProgressView) findViewById(R.id.mCircleView_backseatside);
        mCircleView_door = (CircleProgressView) findViewById(R.id.mCircleView_door);
        mCircleView_cover = (CircleProgressView) findViewById(R.id.mCircleView_cover);

        mCircleView_cover.setVisibility(View.GONE);
        mCircleView_front.setVisibility(View.GONE);
        mCircleView_rear.setVisibility(View.GONE);
        mCircleView_leftside.setVisibility(View.GONE);
        mCircleView_rightside.setVisibility(View.GONE);
        mCircleView_rearleft.setVisibility(View.GONE);
        mCircleView_frontright.setVisibility(View.GONE);
        mCircleView_rearright.setVisibility(View.GONE);
        mCircleView_wheels.setVisibility(View.GONE);
        mCircleView_frontseat.setVisibility(View.GONE);
        mCircleView_backseat.setVisibility(View.GONE);
        mCircleView_odameter.setVisibility(View.GONE);
        mCircleView_dasboard.setVisibility(View.GONE);
        mCircleView_steering.setVisibility(View.GONE);
        mCircleView_frontseatside.setVisibility(View.GONE);
        mCircleView_backseatside.setVisibility(View.GONE);
        mCircleView_door.setVisibility(View.GONE);


        coverimglbl = (TextView) findViewById(R.id.coverimglbl);
        frontviewlbl = (TextView) findViewById(R.id.frontviewlbl);
        rearviewlbl = (TextView) findViewById(R.id.rearviewlbl);
        leftsidelbl = (TextView) findViewById(R.id.leftsidelbl);
        rightsidelbl = (TextView) findViewById(R.id.rightsidelbl);
        rearleftlbl = (TextView) findViewById(R.id.rearleftlbl);
        frontrightlbl = (TextView) findViewById(R.id.frontrightlbl);
        rearrightlbl = (TextView) findViewById(R.id.rearrightlbl);
        wheelslbl = (TextView) findViewById(R.id.wheelslbl);
        frontseatlbl = (TextView) findViewById(R.id.frontseatlbl);
        backseatlbl = (TextView) findViewById(R.id.backseatlbl);
        odameterlbl = (TextView) findViewById(R.id.odameterlbl);
        dasboardlbl = (TextView) findViewById(R.id.dasboardlbl);
        steeringlbl = (TextView) findViewById(R.id.steeringlbl);
        frontrowlbl = (TextView) findViewById(R.id.frontrowlbl);
        backrowlbl = (TextView) findViewById(R.id.backrowlbl);
        doorlbl = (TextView) findViewById(R.id.doorlbl);


    }

    private void checkMPermissions() {

        ActivityCompat.requestPermissions(ImageUploadActivity.this,
                new String[]{Manifest.permission.INTERNET,
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_WIFI_STATE,
                        Manifest.permission.CALL_PHONE,
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,

                }, MY_PERMISSIONS_REQUEST_READ_CONTACTS);


    }

    boolean checkedPermissionsmarshmellow = false;

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    checkedPermissionsmarshmellow = true;

                } else {
                    checkMPermissions();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Application.getInstance().trackScreenView(this,GlobalText.ImageUpload_Activity);

    }

    public void AutoFillMethods() {

        CommonMethods.MemoryClears();

        imageurl = getIntent().getStringArrayExtra("imageurl");
        imagename = getIntent().getStringArrayExtra("imagename");
        stock_id = getIntent().getStringExtra("stock_id");

        String cover_image_url = "",
                front_view_url = "",
                rear_view_url = "",
                left_side_view_url = "",
                right_side_view_url = "",
                rear_left_view_url = "",
                front_right_view_url = "",
                rear_right_view_url = "",
                wheels_tyres_ur = "",
                front_seats_url = "",
                back_seats_url = "",
                odometer_url = "",
                dashboard_url = "",
                steering_url = "",
                front_row_side_url = "",
                back_row_side_url = "",
                trunk_door_open_view_url = "";


        for (int i = 0; i < imagename.length; i++) {

            if (imagename[i].equalsIgnoreCase("cover_image")) {
                cover_image_url = imageurl[i];
                coverimgFlag = true;
                coverimgClose.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("front_view")) {
                front_view_url = imageurl[i];
                frontviewFlag = true;
                frontviewClose.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("rear_view")) {
                rear_view_url = imageurl[i];
                rearviewFlag = true;
                rearviewClose.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("left_side_view")) {
                left_side_view_url = imageurl[i];
                leftsideviewFlag = true;
                leftsideview_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("rear_left_view")) {
                rear_left_view_url = imageurl[i];
                rearleftviewFlag = true;
                rightsideview_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("right_side_view")) {
                right_side_view_url = imageurl[i];
                rightsideviewFlag = true;
                rearleftview_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("front_right_view")) {
                front_right_view_url = imageurl[i];
                frontrightviewFlag = true;
                frontright_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("rear_right_view")) {
                rear_right_view_url = imageurl[i];
                rearrightviewFlag = true;
                rearright_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("wheels_tyres")) {
                wheels_tyres_ur = imageurl[i];
                wheelstviewFlag = true;
                wheelimg_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("front_seats")) {
                front_seats_url = imageurl[i];
                front_seatsFlag = true;
                frontseat_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("back_seats")) {
                back_seats_url = imageurl[i];
                back_seatsFlag = true;
                backseat_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("odometer")) {
                odometer_url = imageurl[i];
                odometerFlag = true;
                odometer_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("dashboard")) {
                dashboard_url = imageurl[i];
                dashboardFlag = true;
                dasborad_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("steering")) {
                steering_url = imageurl[i];
                steeringFlag = true;
                steering_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("front_row_side")) {
                front_row_side_url = imageurl[i];
                front_row_sideFlag = true;
                frontrowseat_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("back_row_side")) {
                back_row_side_url = imageurl[i];
                back_row_sideFlag = true;
                backrowseat_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("trunk_door_open_view")) {
                trunk_door_open_view_url = imageurl[i];
                trunk_door_open_viewFlag = true;
                doorimg_close.setVisibility(View.VISIBLE);
            }

        }

        if (!cover_image_url.equalsIgnoreCase("")) {
            Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build()
                    .load(cover_image_url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(coverimg);
        }
        if (!front_view_url.equalsIgnoreCase("")) {
            Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build().load(front_view_url).into(frontview);
        }
        if (!rear_view_url.equalsIgnoreCase("")) {
            Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build().load(rear_view_url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(rearview);
        }
        if (!left_side_view_url.equalsIgnoreCase("")) {
            Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build().load(left_side_view_url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(leftsideview);
        }
        if (!left_side_view_url.equalsIgnoreCase("")) {
            Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build().load(left_side_view_url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(leftsideview);
        }

        if (!rear_left_view_url.equalsIgnoreCase("")) {
            Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build().load(rear_left_view_url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(rearleftview);
        }
        if (!right_side_view_url.equalsIgnoreCase("")) {
            Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build().load(right_side_view_url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(rightsideview);
        }
        if (!front_right_view_url.equalsIgnoreCase("")) {
            Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build().load(front_right_view_url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(frontright);
        }

        if (!rear_right_view_url.equalsIgnoreCase("")) {
            Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build().load(rear_right_view_url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(rearright);

        }

        if (!wheels_tyres_ur.equalsIgnoreCase("")) {
            Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build().load(wheels_tyres_ur)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(wheelimg);
        }

        if (!front_seats_url.equalsIgnoreCase("")) {
            Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build().load(front_seats_url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(frontseat);
        }

        if (!back_seats_url.equalsIgnoreCase("")) {
            Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build().load(back_seats_url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(backseat);
        }


        if (!odometer_url.equalsIgnoreCase("")) {
            Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build().load(odometer_url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(odometer);
        }

        if (!dashboard_url.equalsIgnoreCase("")) {
            Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build().load(dashboard_url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(dasborad);
        }

        if (!steering_url.equalsIgnoreCase("")) {
            Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build().load(steering_url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(steering);
        }

        if (!front_row_side_url.equalsIgnoreCase("")) {
            Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build().load(front_row_side_url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(frontrowseat);
        }

        if (!back_row_side_url.equalsIgnoreCase("")) {
            Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build().load(back_row_side_url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(backrowseat);
        }

        if (!trunk_door_open_view_url.equalsIgnoreCase("")) {
            Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build().load(trunk_door_open_view_url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(doorimg);
        }


    }

    @OnClick(R.id.samplesbtn)
    public void samplesbtn(View view) {

        Intent in_main = new Intent(ImageUploadActivity.this, ViewSamplesActivity.class);
        startActivity(in_main);

    }

    @OnClick(R.id.coverimgClose)
    public void coverimgClose(View view) {
        if (coverimgFlag) {
            CloseAert("coverimgClose");
        }
    }

    @OnClick(R.id.frontviewClose)
    public void frontviewClose(View view) {
        if (frontviewFlag) {
            CloseAert("frontviewClose");
        }
    }

    @OnClick(R.id.rearviewClose)
    public void rearviewClose(View view) {
        if (rearviewFlag) {
            CloseAert("rearviewClose");
        }
    }

    @OnClick(R.id.leftsideview_close)
    public void leftsideview_close(View view) {
        if (leftsideviewFlag) {
            CloseAert("leftsideview_close");
        }
    }

    @OnClick(R.id.frontright_close)
    public void frontright_close(View view) {
        if (frontrightviewFlag) {
            CloseAert("frontright_close");
        }
    }

    @OnClick(R.id.rightsideview_close)
    public void rightsideview_close(View view) {
        if (rightsideviewFlag) {
            CloseAert("rightsideview_close");
        }
    }

    @OnClick(R.id.rearleftview_close)
    public void rearleftview_close(View view) {
        if (rearleftviewFlag) {
            CloseAert("rearleftview_close");
        }
    }

    @OnClick(R.id.rearright_close)
    public void rearright_close(View view) {
        if (rearrightviewFlag) {
            CloseAert("rearright_close");
        }
    }

    @OnClick(R.id.wheelimg_close)
    public void wheelimg_close(View view) {
        if (wheelstviewFlag) {
            CloseAert("wheelimg_close");
        }
    }

    @OnClick(R.id.backseat_close)
    public void backseat_close(View view) {

        CloseAert("backseat_close");
    }

    @OnClick(R.id.odometer_close)
    public void odometer_close(View view) {

        CloseAert("odometer_close");
    }

    @OnClick(R.id.dasborad_close)
    public void dasborad_close(View view) {

        CloseAert("dasborad_close");
    }

    @OnClick(R.id.frontseat_close)
    public void frontseat_close(View view) {

        //if(front_seatsFlag) {
        CloseAert("frontseat_close");
        //}
    }

    @OnClick(R.id.steering_close)
    public void steering_close(View view) {

        CloseAert("steering_close");
    }

    @OnClick(R.id.frontrowseat_close)
    public void frontrowseat_close(View view) {

        CloseAert("frontrowseat_close");
    }

    @OnClick(R.id.backrowseat_close)
    public void backrowseat_close(View view) {

        CloseAert("backrowseat_close");
    }

    @OnClick(R.id.doorimg_close)
    public void doorimg_close(View view) {

        CloseAert("doorimg_close");
    }


    @OnClick(R.id.coverimgTrans)
    public void coverimgTrans(View view) {

        selectImage("cover_image_request");
    }


    @OnClick(R.id.frontviewTrans)
    public void frontview(View view) {

        selectImage("front_image_request");
    }


    @OnClick(R.id.rearviewTrans)
    public void rearview(View view) {
        selectImage("rear_image_request");
    }


    @OnClick(R.id.leftsideviewTrans)
    public void leftsideviewTrans(View view) {
        selectImage("leftside_image_request");
    }

    @OnClick(R.id.rightsideviewTrans)
    public void rightsideviewTrans(View view) {
        selectImage("rightside_image_request");
    }

    @OnClick(R.id.rearleftviewTrans)
    public void rearleftviewTrans(View view) {
        selectImage("rearleft_image_request");
    }

    @OnClick(R.id.frontrightviewTrans)
    public void frontrightviewTrans(View view) {
        selectImage("frontright_image_request");
    }

    @OnClick(R.id.rearrightviewTrans)
    public void rearrightviewTrans(View view) {
        selectImage("rear_right_view");
    }

    @OnClick(R.id.wheelsTrans)
    public void wheelsTrans(View view) {
        selectImage("wheels_tyres");
    }

    @OnClick(R.id.frontseatTrans)
    public void frontseatTrans(View view) {
        selectImage("front_seats");
    }

    @OnClick(R.id.backseatTrans)
    public void backseatTrans(View view) {
        selectImage("back_seats");
    }


    @OnClick(R.id.odometerTrans)
    public void odometerTrans(View view) {
        selectImage("odometer");
    }


    @OnClick(R.id.dasboardTrans)
    public void dasboardTrans(View view) {
        selectImage("dashboard");
    }

    @OnClick(R.id.steeringTrans)
    public void steeringTrans(View view) {
        selectImage("steering");
    }

    @OnClick(R.id.frontseatsideTrans)
    public void frontseatsideTrans(View view) {
        selectImage("front_row_side");
    }

    @OnClick(R.id.backseatsideTrans)
    public void backseatsideTrans(View view) {
        selectImage("back_row_side");
    }

    @OnClick(R.id.doorTrans)
    public void doorTrans(View view) {
        selectImage("trunk_door_open_view");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item, menu);
        MenuItem item = menu.findItem(R.id.notification_bell);
        item.setVisible(false);

        return true;
    }


    @OnClick(R.id.imagedonebtn)
    public void imagedonebtn(View view) {

        if (CommonMethods.getstringvaluefromkey(this, "stockdetails").equalsIgnoreCase("stockdetails")) {
            StockStoreInfoActivity.imageload(stock_id, activity);
            finish();

        } else {
            Intent in_main = new Intent(ImageUploadActivity.this, MainActivity.class);
            startActivity(in_main);
            finish();
        }

    }


    @OnClick(R.id.exteriorTab)
    public void exteriorTab(View view) {

        exteriorTab.setTextColor(getResources().getColor(R.color.yellow));
        interiorTab.setTextColor(getResources().getColor(R.color.White));

        exteriorItem.setVisibility(View.VISIBLE);
        interiorItem.setVisibility(View.GONE);

    }

    @OnClick(R.id.interiorTab)
    public void interiorTab(View view) {

        exteriorTab.setTextColor(getResources().getColor(R.color.White));
        interiorTab.setTextColor(getResources().getColor(R.color.yellow));

        exteriorItem.setVisibility(View.GONE);
        interiorItem.setVisibility(View.VISIBLE);

    }


    private void selectImage(String imageStatus) {


        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.camera);
        dialog.setCancelable(true);


        TextView pictureType = dialog.findViewById(R.id.pictureType);
        LinearLayout select_camera = dialog.findViewById(R.id.select_camera);
        LinearLayout select_gallery = dialog.findViewById(R.id.select_gallery);
        CommonMethods.MemoryClears();

        select_camera.setOnClickListener(view2 -> {

            whichSource = false;

            if (imageStatus.equalsIgnoreCase("cover_image_request")) {
                openCamera(cover_camera_request);
                coverimgFlag = false;
            } else if (imageStatus.equalsIgnoreCase("front_image_request")) {
                openCamera(front_camera_request);
                frontviewFlag = false;

            } else if (imageStatus.equalsIgnoreCase("rear_image_request")) {
                rearviewFlag = false;
                openCamera(rear_camera_request);
            } else if (imageStatus.equalsIgnoreCase("leftside_image_request")) {
                leftsideviewFlag = false;

                openCamera(leftside_camera_request);
            } else if (imageStatus.equalsIgnoreCase("rightside_image_request")) {
                rightsideviewFlag = false;

                openCamera(rightside_camera_request);
            } else if (imageStatus.equalsIgnoreCase("rearleft_image_request")) {
                rearleftviewFlag = false;

                openCamera(rearleft_camera_request);
            } else if (imageStatus.equalsIgnoreCase("frontright_image_request")) {
                frontrightviewFlag = false;

                openCamera(frontright_camera_request);
            } else if (imageStatus.equalsIgnoreCase("rear_right_view")) {
                rearrightviewFlag = false;

                openCamera(rearright_camera_request);
            } else if (imageStatus.equalsIgnoreCase("wheels_tyres")) {
                wheelstviewFlag = false;

                openCamera(wheels_camera_request);
            } else if (imageStatus.equalsIgnoreCase("front_seats")) {
                front_seatsFlag = false;
                openCamera(front_seats_camera_request);
            } else if (imageStatus.equalsIgnoreCase("back_seats")) {
                back_seatsFlag = false;
                openCamera(back_seats_camera_request);
            } else if (imageStatus.equalsIgnoreCase("odometer")) {
                odometerFlag = false;
                openCamera(odometer_camera_request);
            } else if (imageStatus.equalsIgnoreCase("dashboard")) {
                dashboardFlag = false;
                openCamera(dashboard_camera_request);
            } else if (imageStatus.equalsIgnoreCase("steering")) {
                steeringFlag = false;
                openCamera(steering_camera_request);
            } else if (imageStatus.equalsIgnoreCase("front_row_side")) {
                front_row_sideFlag = false;
                openCamera(front_row_side_camera_request);
            } else if (imageStatus.equalsIgnoreCase("back_row_side")) {
                back_row_sideFlag = false;
                openCamera(back_row_side_camera_request);
            } else if (imageStatus.equalsIgnoreCase("trunk_door_open_view")) {
                trunk_door_open_viewFlag = false;
                openCamera(trunk_door_open_view_camera_request);
            }

            dialog.dismiss();

        });

        select_gallery.setOnClickListener(view2 -> {

            whichSource = false;
            if (imageStatus.equalsIgnoreCase("cover_image_request")) {
                openGallery(cover_gallery_request);
            } else if (imageStatus.equalsIgnoreCase("front_image_request")) {
                openGallery(front_gallery_request);
            } else if (imageStatus.equalsIgnoreCase("rear_image_request")) {
                openGallery(rear_gallery_request);
            } else if (imageStatus.equalsIgnoreCase("leftside_image_request")) {
                openGallery(leftside_gallery_request);
            } else if (imageStatus.equalsIgnoreCase("rightside_image_request")) {
                openGallery(rightside_gallery_request);
            } else if (imageStatus.equalsIgnoreCase("rearleft_image_request")) {
                openGallery(rearleft_gallery_request);
            } else if (imageStatus.equalsIgnoreCase("frontright_image_request")) {
                openGallery(frontright_gallery_request);
            } else if (imageStatus.equalsIgnoreCase("rear_right_view")) {
                openGallery(rearright_gallery_request);
            } else if (imageStatus.equalsIgnoreCase("wheels_tyres")) {
                openGallery(wheels_gallery_request);
            } else if (imageStatus.equalsIgnoreCase("front_seats")) {
                openGallery(front_seats_gallery_request);
            } else if (imageStatus.equalsIgnoreCase("back_seats")) {
                openGallery(back_seats_gallery_request);
            } else if (imageStatus.equalsIgnoreCase("odometer")) {
                openGallery(odometer_gallery_request);
            } else if (imageStatus.equalsIgnoreCase("dashboard")) {
                openGallery(dashboard_gallery_request);
            } else if (imageStatus.equalsIgnoreCase("steering")) {
                openGallery(steering_gallery_request);
            } else if (imageStatus.equalsIgnoreCase("front_row_side")) {
                openGallery(front_row_side_gallery_request);
            } else if (imageStatus.equalsIgnoreCase("back_row_side")) {
                openGallery(back_row_side_gallery_request);
            } else if (imageStatus.equalsIgnoreCase("trunk_door_open_view")) {
                openGallery(trunk_door_open_view_gallery_request);
            }
            dialog.dismiss();

        });

        dialog.show();

    }

    private void openCamera(int requestType) {

        checkMPermissions();

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            if (intent.resolveActivity(this.getPackageManager()) != null) {
                file = new File(getExternalCacheDir(),
                        String.valueOf(System.currentTimeMillis()) + ".jpg");
                fileUri = Uri.fromFile(file);
                fileUri = FileProvider.getUriForFile(this, this.getPackageName() + ".provider", file);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                startActivityForResult(intent, requestType);
                overridePendingTransition(0, 0);

                cameraRequirement(intent, fileUri);
            }
        } catch (Exception e) {

        }
    }

    private void openGallery(int requestType) {

        whichSource = true;
        Log.e("requestType", "requestType=" + requestType);
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Choose File"), requestType);


    }

    public void CloseAert(String close) {


        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.alert);
        //dialog.setTitle("Custom Dialog");

        TextView Message, cancel, Confirm;
        Message = dialog.findViewById(R.id.Message);
        cancel = dialog.findViewById(R.id.cancel);
        Confirm = dialog.findViewById(R.id.Confirm);

        CommonMethods.MemoryClears();

            /* Window window = dialog.getWindow();
            window.setLayout(W, 1000);*/

        Message.setText("Are you sure you want to Delete this image?");

        Confirm.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {


                if (close.equalsIgnoreCase("coverimgClose")) {
                    startSendingImageToServerCounter("", cover_image, "cover_image", "coverimgClose");
                } else if (close.equalsIgnoreCase("frontviewClose")) {
                    startSendingImageToServerCounter("", front_image, "front_view", "frontviewClose");
                } else if (close.equalsIgnoreCase("rearviewClose")) {
                    startSendingImageToServerCounter("", rear_image, "rear_view", "rearviewClose");
                } else if (close.equalsIgnoreCase("leftsideview_close")) {
                    startSendingImageToServerCounter("", leftside_image, "leftside_image", "leftsideview_close");
                } else if (close.equalsIgnoreCase("rightsideview_close")) {
                    startSendingImageToServerCounter("", rightside_image, "right_side_view", "rightsideview_close");
                } else if (close.equalsIgnoreCase("rearleftview_close")) {
                    startSendingImageToServerCounter("", rearleft_image, "rear_left_view", "rearleftview_close");
                } else if (close.equalsIgnoreCase("rearright_close")) {
                    startSendingImageToServerCounter("", rearright_image, "rear_right_view", "rearright_close");
                } else if (close.equalsIgnoreCase("wheelimg_close")) {
                    startSendingImageToServerCounter("", wheels_image, "wheels_tyres", "wheelimg_close");
                } else if (close.equalsIgnoreCase("frontright_close")) {
                    startSendingImageToServerCounter("", frontright_image, "front_right_view", "frontright_close");
                } else if (close.equalsIgnoreCase("frontseat_close")) {
                    startSendingImageToServerCounter("", front_seats_image, "front_seats", "frontseat_close");
                } else if (close.equalsIgnoreCase("backseat_close")) {
                    startSendingImageToServerCounter("", back_seats_image, "back_seats", "backseat_close");
                } else if (close.equalsIgnoreCase("odometer_close")) {
                    startSendingImageToServerCounter("", odometer_image, "odometer", "odometer_close");
                } else if (close.equalsIgnoreCase("dasborad_close")) {
                    startSendingImageToServerCounter("", dashboard_image, "dashboard", "dasborad_close");
                } else if (close.equalsIgnoreCase("steering_close")) {
                    startSendingImageToServerCounter("", steering_image, "steering", "steering_close");
                } else if (close.equalsIgnoreCase("frontrowseat_close")) {
                    startSendingImageToServerCounter("", front_row_side_image, "front_row_side", "frontrowseat_close");
                } else if (close.equalsIgnoreCase("backrowseat_close")) {
                    startSendingImageToServerCounter("", back_row_side_image, "back_row_side", "backrowseat_close");
                } else if (close.equalsIgnoreCase("doorimg_close")) {
                    startSendingImageToServerCounter("", back_row_side_image, "trunk_door_open_view", "doorimg_close");
                }

                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    void cameraRequirement(Intent intent, Uri fileUri) {
        List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo resolveInfo : resInfoList) {
            String packageName = resolveInfo.activityInfo.packageName;
            grantUriPermission(packageName, fileUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
    }

    private Bitmap loadImageFromStorage(File f) {
        Bitmap b = null;

        CommonMethods.MemoryClears();

        try {
            b = BitmapFactory.decodeStream(new FileInputStream(f));


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return b;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Bitmap bm = null;
        CommonMethods.MemoryClears();

        if (resultCode != Activity.RESULT_CANCELED) {
            Log.e("data", "data=1");

            if (resultCode == Activity.RESULT_OK || file.length() != 0) {
                Log.e("data", "data=2");

                if (whichSource == false) {
                    Log.e("data", "data=3");

                    try {
                        if (file.length() != 0) {
                            Log.e("data", "data=4");

                            Bitmap rotatedBitmap = null;
                            ExifInterface exif = new ExifInterface(file.getAbsolutePath());

                            DisplayMetrics displayMetrics = new DisplayMetrics();
                            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

                            Double w = displayMetrics.widthPixels * (CAMERA_RESCALE_VALUE);
                            width_x = w.intValue();
                            Double h = displayMetrics.heightPixels * (CAMERA_RESCALE_VALUE);
                            height_y = h.intValue();

                            Integer tempInt = width_x;

                            /*width_x = loadImageFromStorage(file).getHeight()/2;
                            height_y = loadImageFromStorage(file).getWidth()/2;*/

                            if (loadImageFromStorage(file).getHeight() > loadImageFromStorage(file).getWidth()) {

                                rotatedBitmap = rotateImage(loadImageFromStorage(file), 0);

                            } else if (loadImageFromStorage(file).getWidth() > loadImageFromStorage(file).getHeight()) {

                                rotatedBitmap = rotateImage(loadImageFromStorage(file), 0);
                                width_x = height_y;
                                height_y = tempInt;

                            }

                            if (requestCode == cover_camera_request) {

                                coverimg.setImageBitmap(Bitmap.createScaledBitmap(rotatedBitmap, rotatedBitmap.getWidth() / imgComp, rotatedBitmap.getHeight() / imgComp, false));
                                coverimgFlag = true;
                                //deleteImageFile(file);
                                startSendingImageToServerCounter(imageToString(cover_image), cover_image, "cover_image", "");

                            } else if (requestCode == front_camera_request) {
                                frontview.setImageBitmap(Bitmap.createScaledBitmap(rotatedBitmap, rotatedBitmap.getWidth() / imgComp, rotatedBitmap.getHeight() / imgComp, false));
                                frontviewFlag = true;
                                //deleteImageFile(file);
                                startSendingImageToServerCounter(imageToString(front_image), front_image, "front_view", "");

                            } else if (requestCode == rear_camera_request) {
                                rearview.setImageBitmap(Bitmap.createScaledBitmap(rotatedBitmap, rotatedBitmap.getWidth() / imgComp, rotatedBitmap.getHeight() / imgComp, false));
                                rearviewFlag = true;
                                //deleteImageFile(file);
                                startSendingImageToServerCounter(imageToString(rear_image), rear_image, "rear_view", "");
                            } else if (requestCode == leftside_camera_request) {
                                leftsideview.setImageBitmap(Bitmap.createScaledBitmap(rotatedBitmap, rotatedBitmap.getWidth() / imgComp, rotatedBitmap.getHeight() / imgComp, false));
                                leftsideviewFlag = true;
                                //deleteImageFile(file);
                                startSendingImageToServerCounter(imageToString(leftside_image), leftside_image, "left_side_view", "");
                            } else if (requestCode == rightside_camera_request) {
                                rightsideview.setImageBitmap(Bitmap.createScaledBitmap(rotatedBitmap, rotatedBitmap.getWidth() / imgComp, rotatedBitmap.getHeight() / imgComp, false));
                                rightsideviewFlag = true;
                                //deleteImageFile(file);

                                startSendingImageToServerCounter(imageToString(rightside_image), rightside_image, "right_side_view", "");
                            } else if (requestCode == rearleft_camera_request) {
                                rearleftview.setImageBitmap(Bitmap.createScaledBitmap(rotatedBitmap, rotatedBitmap.getWidth() / imgComp, rotatedBitmap.getHeight() / imgComp, false));
                                rearleftviewFlag = true;
                                //deleteImageFile(file);

                                startSendingImageToServerCounter(imageToString(rearleft_image), rearleft_image, "rear_left_view", "");
                            } else if (requestCode == frontright_camera_request) {
                                frontright.setImageBitmap(Bitmap.createScaledBitmap(rotatedBitmap, rotatedBitmap.getWidth() / imgComp, rotatedBitmap.getHeight() / imgComp, false));
                                frontrightviewFlag = true;
                                //deleteImageFile(file);

                                startSendingImageToServerCounter(imageToString(frontright_image), frontright_image, "front_right_view", "");
                            } else if (requestCode == rearright_camera_request) {
                                rearright.setImageBitmap(Bitmap.createScaledBitmap(rotatedBitmap, rotatedBitmap.getWidth() / imgComp, rotatedBitmap.getHeight() / imgComp, false));
                                rearrightviewFlag = true;
                                //deleteImageFile(file);

                                startSendingImageToServerCounter(imageToString(rearright_image), rearright_image, "rear_right_view", "");
                            } else if (requestCode == wheels_camera_request) {
                                wheelimg.setImageBitmap(Bitmap.createScaledBitmap(rotatedBitmap, rotatedBitmap.getWidth() / imgComp, rotatedBitmap.getHeight() / imgComp, false));
                                wheelstviewFlag = true;
                                //deleteImageFile(file);

                                startSendingImageToServerCounter(imageToString(wheels_image), wheels_image, "wheels_tyres", "");
                            } else if (requestCode == front_seats_camera_request) {
                                frontseat.setImageBitmap(Bitmap.createScaledBitmap(rotatedBitmap, rotatedBitmap.getWidth() / imgComp, rotatedBitmap.getHeight() / imgComp, false));
                                front_seatsFlag = true;
                                //deleteImageFile(file);

                                startSendingImageToServerCounter(imageToString(front_seats_image), front_seats_image, "front_seats", "");
                            } else if (requestCode == back_seats_camera_request) {

                                backseat.setImageBitmap(Bitmap.createScaledBitmap(rotatedBitmap, rotatedBitmap.getWidth() / imgComp, rotatedBitmap.getHeight() / imgComp, false));
                                back_seatsFlag = true;
                                //deleteImageFile(file);

                                startSendingImageToServerCounter(imageToString(back_seats_image), back_seats_image, "back_seats", "");
                            } else if (requestCode == odometer_camera_request) {
                                odometer.setImageBitmap(Bitmap.createScaledBitmap(rotatedBitmap, rotatedBitmap.getWidth() / imgComp, rotatedBitmap.getHeight() / imgComp, false));
                                odometerFlag = true;
                                //deleteImageFile(file);

                                startSendingImageToServerCounter(imageToString(odometer_image), odometer_image, "odometer", "");
                            } else if (requestCode == dashboard_camera_request) {
                                dasborad.setImageBitmap(Bitmap.createScaledBitmap(rotatedBitmap, rotatedBitmap.getWidth() / imgComp, rotatedBitmap.getHeight() / imgComp, false));
                                dashboardFlag = true;
                                //deleteImageFile(file);

                                startSendingImageToServerCounter(imageToString(dashboard_image), dashboard_image, "dashboard", "");
                            } else if (requestCode == steering_camera_request) {
                                steering.setImageBitmap(Bitmap.createScaledBitmap(rotatedBitmap, rotatedBitmap.getWidth() / imgComp, rotatedBitmap.getHeight() / imgComp, false));
                                steeringFlag = true;
                                //deleteImageFile(file);

                                startSendingImageToServerCounter(imageToString(steering_image), steering_image, "steering", "");
                            } else if (requestCode == front_row_side_camera_request) {
                                frontrowseat.setImageBitmap(Bitmap.createScaledBitmap(rotatedBitmap, rotatedBitmap.getWidth() / imgComp, rotatedBitmap.getHeight() / imgComp, false));
                                front_row_sideFlag = true;
                                //deleteImageFile(file);

                                startSendingImageToServerCounter(imageToString(front_row_side_image), front_row_side_image, "front_row_side", "");
                            } else if (requestCode == back_row_side_camera_request) {
                                backrowseat.setImageBitmap(Bitmap.createScaledBitmap(rotatedBitmap, rotatedBitmap.getWidth() / imgComp, rotatedBitmap.getHeight() / imgComp, false));
                                back_row_sideFlag = true;
                                //deleteImageFile(file);

                                startSendingImageToServerCounter(imageToString(back_row_side_image), back_row_side_image, "back_row_side", "");
                            } else if (requestCode == trunk_door_open_view_camera_request) {
                                doorimg.setImageBitmap(Bitmap.createScaledBitmap(rotatedBitmap, rotatedBitmap.getWidth() / imgComp, rotatedBitmap.getHeight() / imgComp, false));
                                trunk_door_open_viewFlag = true;
                                //deleteImageFile(file);

                                startSendingImageToServerCounter(imageToString(trunk_door_open_view_image), trunk_door_open_view_image, "trunk_door_open_view", "");
                            }
                        }
                    } catch (Exception e) {
                    }
                } else {
                    Log.e("requestCode", "requestCode" + requestCode);
                    if (data != null) {
                        try {
                            bm = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());

                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            bm.compress(Bitmap.CompressFormat.JPEG, 70, baos);
                            byte[] b = baos.toByteArray();

                            if (b.length >= 1.3 * 1024 * 1024) {

                                Double w = bm.getWidth() * (GALLERY_RESCALE_VALUE);
                                width_x = w.intValue();
                                Double h = bm.getHeight() * (GALLERY_RESCALE_VALUE);
                                height_y = h.intValue();

                                width_x = w.intValue();
                                height_y = h.intValue();

                                Bitmap bitmapAfterRescaleImage = Bitmap.createScaledBitmap(bm, width_x, height_y, false);

                                bm = bitmapAfterRescaleImage;

                            }

                            if (requestCode == cover_gallery_request) {
                                coverimg.setImageBitmap(bm);
                                coverimgFlag = true;

                                startSendingImageToServerCounter(imageToString(cover_image), cover_image, "cover_image", "");

                            } else if (requestCode == front_gallery_request) {
                                frontview.setImageBitmap(bm);
                                frontviewFlag = true;


                                startSendingImageToServerCounter(imageToString(front_image), front_image, "front_view", "");

                            } else if (requestCode == rear_gallery_request) {
                                rearview.setImageBitmap(bm);
                                rearviewFlag = true;


                                startSendingImageToServerCounter(imageToString(rear_image), rear_image, "rear_view", "");
                            } else if (requestCode == leftside_gallery_request) {
                                leftsideview.setImageBitmap(bm);
                                leftsideviewFlag = true;


                                startSendingImageToServerCounter(imageToString(leftside_image), leftside_image, "left_side_view", "");
                            } else if (requestCode == rightside_gallery_request) {
                                rightsideview.setImageBitmap(bm);
                                rightsideviewFlag = true;
                                if (bm != null) bm = null;

                                startSendingImageToServerCounter(imageToString(rightside_image), rightside_image, "right_side_view", "");
                            } else if (requestCode == rearleft_gallery_request) {
                                rearleftview.setImageBitmap(bm);
                                rearleftviewFlag = true;
                                if (bm != null) bm = null;

                                startSendingImageToServerCounter(imageToString(rearleft_image), rearleft_image, "rear_left_view", "");
                            } else if (requestCode == frontright_gallery_request) {
                                frontright.setImageBitmap(bm);
                                frontrightviewFlag = true;
                                if (bm != null) bm = null;

                                startSendingImageToServerCounter(imageToString(frontright_image), frontright_image, "front_right_view", "");
                            } else if (requestCode == rearright_gallery_request) {
                                rearright.setImageBitmap(bm);
                                rearrightviewFlag = true;
                                if (bm != null) bm = null;

                                startSendingImageToServerCounter(imageToString(rearright_image), rearright_image, "rear_right_view", "");
                            } else if (requestCode == wheels_gallery_request) {
                                wheelimg.setImageBitmap(bm);
                                wheelstviewFlag = true;
                                if (bm != null) bm = null;

                                startSendingImageToServerCounter(imageToString(wheels_image), wheels_image, "wheels_tyres", "");
                            } else if (requestCode == front_seats_gallery_request) {
                                frontseat.setImageBitmap(bm);
                                front_seatsFlag = true;
                                if (bm != null) bm = null;

                                startSendingImageToServerCounter(imageToString(front_seats_image), front_seats_image, "front_seats", "");
                            } else if (requestCode == back_seats_camera_request) {
                                backseat.setImageBitmap(bm);
                                back_seatsFlag = true;
                                if (bm != null) bm = null;

                                startSendingImageToServerCounter(imageToString(back_seats_image), back_seats_image, "back_seats", "");
                            } else if (requestCode == odometer_gallery_request) {
                                odometer.setImageBitmap(bm);
                                odometerFlag = true;
                                if (bm != null) bm = null;

                                startSendingImageToServerCounter(imageToString(odometer_image), odometer_image, "odometer", "");
                            } else if (requestCode == dashboard_gallery_request) {
                                dasborad.setImageBitmap(bm);
                                dashboardFlag = true;
                                if (bm != null) bm = null;

                                startSendingImageToServerCounter(imageToString(dashboard_image), dashboard_image, "dashboard", "");
                            } else if (requestCode == steering_gallery_request) {
                                steering.setImageBitmap(bm);
                                steeringFlag = true;
                                if (bm != null) bm = null;

                                startSendingImageToServerCounter(imageToString(steering_image), steering_image, "steering", "");
                            } else if (requestCode == front_row_side_gallery_request) {
                                frontrowseat.setImageBitmap(bm);
                                front_row_sideFlag = true;
                                if (bm != null) bm = null;

                                startSendingImageToServerCounter(imageToString(front_row_side_image), front_row_side_image, "front_row_side", "");
                            } else if (requestCode == back_row_side_gallery_request) {
                                backrowseat.setImageBitmap(bm);
                                back_row_sideFlag = true;
                                if (bm != null) bm = null;

                                startSendingImageToServerCounter(imageToString(back_row_side_image), back_row_side_image, "back_row_side", "");
                            } else if (requestCode == trunk_door_open_view_gallery_request) {
                                doorimg.setImageBitmap(bm);
                                trunk_door_open_viewFlag = true;
                                if (bm != null) bm = null;

                                startSendingImageToServerCounter(imageToString(trunk_door_open_view_image), trunk_door_open_view_image, "trunk_door_open_view", "");
                            }
                        } catch (IOException e) {
                            e.printStackTrace();

                            Log.e("gallery", "gallery=null-0");
                            Log.e("gallery", "gallery=null" + e);

                        }
                    } else {
                        Log.e("gallery", "gallery=null-1");
                    }
                }
            }

        }
    }

    private void deleteImageFile(File fdelete) {  // this function is to delete the image file //

        if (fdelete.exists()) {
            if (fdelete.delete()) {
                Log.e(" File is delete -->", fdelete.toString());
            } else {
                Log.e(" File is not delete -->", fdelete.toString());
            }
        }

    }

    private void startSendingImageToServerCounter(String imageString, String imageType, String imageName, String deleteimg) {

        try {

            JSONObject jObj = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject1 = new JSONObject();
            jObj.put("stock_id", stock_id.toString());
            jObj.put("dealer_code", CommonMethods.getstringvaluefromkey(this, "dealer_code"));
            jObj.put("IsNonMFC", "false");

            jsonObject1.put("mdate", CommonMethods.getTime());
            jsonObject1.put("type", "jpg");
            jsonObject1.put("category_identifier", imageName);
            if (deleteimg.equalsIgnoreCase("")) {
                jsonObject1.put("img", imageString);

            } else {
                jsonObject1.put("img", "");

            }

            imageUploadedProgressBar(imageType);

            jsonArray.put(jsonObject1);
            jObj.put("images", jsonArray);

            serverCallForImage(imageType, jObj, deleteimg);


            Log.e("serverCallForImage", "serverCallForImage" + imageType);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void serverCallForImage(String imageUpload, JSONObject jsonObject, String deleteimg) {

        Log.e("serverCallForImage", "serverCallForImage" + imageUpload);

        if (imageUpload.equals(cover_image)) {
            CoverImageWebServices parser = new CoverImageWebServices(activity, cover_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(front_image)) {
            FrontImageWebServices parser = new FrontImageWebServices(activity, front_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(rear_image)) {
            RearImageWebServices parser = new RearImageWebServices(activity, rear_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(leftside_image)) {
            LeftSideImageWebServices parser = new LeftSideImageWebServices(activity, leftside_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(rightside_image)) {
            RightSideImageWebServices parser = new RightSideImageWebServices(activity, rightside_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(rearleft_image)) {
            RearLeftImageWebServices parser = new RearLeftImageWebServices(activity, rearleft_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(frontright_image)) {
            FrontRightImageWebServices parser = new FrontRightImageWebServices(activity, frontright_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(rearright_image)) {
            RearRightImageWebServices parser = new RearRightImageWebServices(activity, rearright_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(wheels_image)) {
            WheelImageWebServices parser = new WheelImageWebServices(activity, wheels_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(front_seats_image)) {
            FrontSeatImageWebServices parser = new FrontSeatImageWebServices(activity, front_seats_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(back_seats_image)) {
            BackSeatImageWebServices parser = new BackSeatImageWebServices(activity, back_seats_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(odometer_image)) {
            OdometerImageWebServices parser = new OdometerImageWebServices(activity, odometer_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(dashboard_image)) {
            DasboardImageWebServices parser = new DasboardImageWebServices(activity, dashboard_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(steering_image)) {
            SteeringImageWebServices parser = new SteeringImageWebServices(activity, steering_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(front_row_side_image)) {
            FrontRowSideImageWebServices parser = new FrontRowSideImageWebServices(activity, front_row_side_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(back_row_side_image)) {
            BackRowSideImageWebServices parser = new BackRowSideImageWebServices(activity, back_row_side_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(trunk_door_open_view_image)) {
            DoorImageWebServices parser = new DoorImageWebServices(activity, trunk_door_open_view_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        }


    }


    private void imageUploadedProgressBar(String imageName) {

        if (imageName.equals(cover_image)) {
            mCircleView_cover.setVisibility(View.VISIBLE);
            mCircleView_cover.spin();
        } else if (imageName.equals(front_image)) {
            mCircleView_front.setVisibility(View.VISIBLE);
            mCircleView_front.spin();
        } else if (imageName.equals(rear_image)) {
            mCircleView_rear.setVisibility(View.VISIBLE);
            mCircleView_rear.spin();
        } else if (imageName.equals(leftside_image)) {
            mCircleView_leftside.setVisibility(View.VISIBLE);
            mCircleView_leftside.spin();
        } else if (imageName.equals(rightside_image)) {
            mCircleView_rightside.setVisibility(View.VISIBLE);
            mCircleView_rightside.spin();
        } else if (imageName.equals(rearleft_image)) {
            mCircleView_rearleft.setVisibility(View.VISIBLE);
            mCircleView_rearleft.spin();
        } else if (imageName.equals(frontright_image)) {
            mCircleView_frontright.setVisibility(View.VISIBLE);
            mCircleView_frontright.spin();
        } else if (imageName.equals(rearright_image)) {
            mCircleView_rearright.setVisibility(View.VISIBLE);
            mCircleView_rearright.spin();
        } else if (imageName.equals(wheels_image)) {
            mCircleView_wheels.setVisibility(View.VISIBLE);
            mCircleView_wheels.spin();
        } else if (imageName.equals(front_seats_image)) {
            mCircleView_frontseat.setVisibility(View.VISIBLE);
            mCircleView_frontseat.spin();
        } else if (imageName.equals(back_seats_image)) {
            mCircleView_backseat.setVisibility(View.VISIBLE);
            mCircleView_backseat.spin();
        } else if (imageName.equals(odometer_image)) {
            mCircleView_odameter.setVisibility(View.VISIBLE);
            mCircleView_odameter.spin();
        } else if (imageName.equals(dashboard_image)) {
            mCircleView_dasboard.setVisibility(View.VISIBLE);
            mCircleView_dasboard.spin();
        } else if (imageName.equals(steering_image)) {
            mCircleView_steering.setVisibility(View.VISIBLE);
            mCircleView_steering.spin();
        } else if (imageName.equals(front_row_side_image)) {
            mCircleView_frontseatside.setVisibility(View.VISIBLE);
            mCircleView_frontseatside.spin();
        } else if (imageName.equals(back_row_side_image)) {
            mCircleView_backseatside.setVisibility(View.VISIBLE);
            mCircleView_backseatside.spin();
        } else if (imageName.equals(trunk_door_open_view_image)) {
            mCircleView_door.setVisibility(View.VISIBLE);
            mCircleView_door.spin();
        }
    }

    private static void imageUploadedProgressBarClosed(String imageName) {
        Log.e("mCircleView_cover", "mCircleView_cover-1");
        if (imageName.equals(cover_image)) {
            Log.e("mCircleView_cover", "mCircleView_cover-2");

            mCircleView_cover.stopSpinning();
            mCircleView_cover.setVisibility(View.GONE);
        } else if (imageName.equals(front_image)) {
            mCircleView_front.stopSpinning();
            mCircleView_front.setVisibility(View.GONE);
        } else if (imageName.equals(rear_image)) {
            mCircleView_rear.stopSpinning();
            mCircleView_rear.setVisibility(View.GONE);
        } else if (imageName.equals(leftside_image)) {
            mCircleView_leftside.stopSpinning();
            mCircleView_leftside.setVisibility(View.GONE);
        } else if (imageName.equals(rightside_image)) {
            mCircleView_rightside.stopSpinning();
            mCircleView_rightside.setVisibility(View.GONE);
        } else if (imageName.equals(rearleft_image)) {
            mCircleView_rearleft.stopSpinning();
            mCircleView_rearleft.setVisibility(View.GONE);
        } else if (imageName.equals(frontright_image)) {
            mCircleView_frontright.stopSpinning();
            mCircleView_frontright.setVisibility(View.GONE);
        } else if (imageName.equals(rearright_image)) {
            mCircleView_rearright.stopSpinning();
            mCircleView_rearright.setVisibility(View.GONE);
        } else if (imageName.equals(wheels_image)) {
            mCircleView_wheels.stopSpinning();
            mCircleView_wheels.setVisibility(View.GONE);
        } else if (imageName.equals(front_seats_image)) {
            mCircleView_frontseat.setVisibility(View.GONE);
            mCircleView_frontseat.spin();
        } else if (imageName.equals(back_seats_image)) {
            mCircleView_backseat.setVisibility(View.GONE);
            mCircleView_backseat.stopSpinning();
        } else if (imageName.equals(odometer_image)) {
            mCircleView_odameter.setVisibility(View.GONE);
            mCircleView_odameter.stopSpinning();
        } else if (imageName.equals(dashboard_image)) {
            mCircleView_dasboard.setVisibility(View.GONE);
            mCircleView_dasboard.stopSpinning();
        } else if (imageName.equals(steering_image)) {
            mCircleView_steering.setVisibility(View.GONE);
            mCircleView_steering.stopSpinning();
        } else if (imageName.equals(front_row_side_image)) {
            mCircleView_frontseatside.setVisibility(View.GONE);
            mCircleView_frontseatside.stopSpinning();
        } else if (imageName.equals(back_row_side_image)) {
            mCircleView_backseatside.setVisibility(View.GONE);
            mCircleView_backseatside.stopSpinning();
        } else if (imageName.equals(trunk_door_open_view_image)) {
            mCircleView_door.setVisibility(View.GONE);
            mCircleView_door.stopSpinning();
        }


    }

    private void imageUploadProgressBarClosedOnly(String imageName) {

        if (imageName.equals(front_image)) {
            mCircleView_front.stopSpinning();
            mCircleView_front.setVisibility(View.GONE);
            /*front_sent_indicator.setVisibility(View.INVISIBLE);
            Resend_Front.setVisibility(View.INVISIBLE);
            front_imageview.setImageDrawable(ContextCompat.getDrawable(activity, R.mipmap.failed_image));*/
        }
    }

    public String imageToString(String pictureOf) {

        Bitmap bm = null;

        Log.e("front_image", "front_image=3.0");

        if (pictureOf.equals(cover_image))
            bm = ((BitmapDrawable) coverimg.getDrawable()).getBitmap();
        else if (pictureOf.equals(front_image))
            bm = ((BitmapDrawable) frontview.getDrawable()).getBitmap();
        else if (pictureOf.equals(rear_image))
            bm = ((BitmapDrawable) rearview.getDrawable()).getBitmap();
        else if (pictureOf.equals(leftside_image))
            bm = ((BitmapDrawable) leftsideview.getDrawable()).getBitmap();
        else if (pictureOf.equals(rightside_image))
            bm = ((BitmapDrawable) rightsideview.getDrawable()).getBitmap();
        else if (pictureOf.equals(rearleft_image))
            bm = ((BitmapDrawable) rearleftview.getDrawable()).getBitmap();
        else if (pictureOf.equals(frontright_image))
            bm = ((BitmapDrawable) frontright.getDrawable()).getBitmap();
        else if (pictureOf.equals(rearright_image))
            bm = ((BitmapDrawable) rearright.getDrawable()).getBitmap();
        else if (pictureOf.equals(wheels_image))
            bm = ((BitmapDrawable) wheelimg.getDrawable()).getBitmap();
        else if (pictureOf.equals(front_seats_image))
            bm = ((BitmapDrawable) frontseat.getDrawable()).getBitmap();
        else if (pictureOf.equals(back_seats_image))
            bm = ((BitmapDrawable) backseat.getDrawable()).getBitmap();
        else if (pictureOf.equals(odometer_image))
            bm = ((BitmapDrawable) odometer.getDrawable()).getBitmap();
        else if (pictureOf.equals(dashboard_image))
            bm = ((BitmapDrawable) dasborad.getDrawable()).getBitmap();
        else if (pictureOf.equals(steering_image))
            bm = ((BitmapDrawable) steering.getDrawable()).getBitmap();
        else if (pictureOf.equals(front_row_side_image))
            bm = ((BitmapDrawable) frontrowseat.getDrawable()).getBitmap();
        else if (pictureOf.equals(back_row_side_image))
            bm = ((BitmapDrawable) backrowseat.getDrawable()).getBitmap();
        else if (pictureOf.equals(trunk_door_open_view_image))
            bm = ((BitmapDrawable) doorimg.getDrawable()).getBitmap();


        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 60, baos);
        byte[] b = baos.toByteArray();

        return globalImageCompression(b.length, bm);
    }

    private String globalImageCompression(int imageSize, Bitmap bm) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        if (imageSize <= (200 * 1024)) {                            // <= 200 KB
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] b = baos.toByteArray();
            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            return encodedImage;
        } else if (imageSize <= (500 * 1024)) {                      // <= 500 KB
            bm.compress(Bitmap.CompressFormat.JPEG, 80, baos);
            byte[] b = baos.toByteArray();
            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            return encodedImage;
        } else if (imageSize <= (1 * 1024 * 1024)) {                 // <= 1 MB
            bm.compress(Bitmap.CompressFormat.JPEG, 80, baos);
            byte[] b = baos.toByteArray();
            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            return encodedImage;
        } else if (imageSize <= (2 * 1024 * 1024)) {                 // <= 2 MB
            bm.compress(Bitmap.CompressFormat.JPEG, 55, baos);
            byte[] b = baos.toByteArray();
            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            return encodedImage;
        } else if (imageSize > (2 * 1024 * 1024)) {                  //  >  2 MB

            Double w = bm.getWidth() * (GALLERY_RESCALE_VALUE);
            width_x = w.intValue();
            Double h = bm.getHeight() * (GALLERY_RESCALE_VALUE);
            height_y = h.intValue();

            Bitmap bitmapAfterRescaleImage = Bitmap.createScaledBitmap(bm, width_x, height_y, false);

            bitmapAfterRescaleImage.compress(Bitmap.CompressFormat.JPEG, 80, baos);
            byte[] b = baos.toByteArray();
            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            return encodedImage;
        }
        return "";
    }


    private class CoverImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String delete;

        public CoverImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            delete = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS-1");


                    if (delete.equalsIgnoreCase("coverimgClose")) {
                        coverimg.setImageResource(R.drawable.front_left_view);
                        coverimglbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        coverimgClose.setVisibility(View.INVISIBLE);
                    } else {
                        coverimglbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        coverimgClose.setVisibility(View.VISIBLE);
                    }


                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    coverimglbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);
                    coverimgClose.setVisibility(View.INVISIBLE);

                }
                Log.e("mCircleView_cover", "mCircleView_cover-0");

                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
                //Log.e("mCircleView_cover","mCircleView_cover-0");
                Log.e("mCircleView_cover", "mCircleView_cover-0" + e);


            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here ---------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        jsonObject = null;

                        try {
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }


    private class FrontImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public FrontImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");
                    if (deleteimg.equalsIgnoreCase("frontviewClose")) {

                        frontview.setImageResource(R.drawable.front_view);
                        frontviewlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        frontviewClose.setVisibility(View.INVISIBLE);

                    } else {
                        frontviewlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        frontviewClose.setVisibility(View.VISIBLE);

                    }

                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    frontviewlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);
                    frontviewClose.setVisibility(View.INVISIBLE);

                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here ---------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        jsonObject = null;
                        try {
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class RearImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public RearImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");

                    if (deleteimg.equalsIgnoreCase("rearviewClose")) {

                        rearview.setImageResource(R.drawable.rear_view);
                        rearviewlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        rearviewClose.setVisibility(View.INVISIBLE);

                    } else {
                        rearviewlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        rearviewClose.setVisibility(View.VISIBLE);
                    }


                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    rearviewlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);
                    rearviewClose.setVisibility(View.VISIBLE);

                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here ---------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        jsonObject = null;
                        try {
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class LeftSideImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public LeftSideImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");
                    if (deleteimg.equalsIgnoreCase("leftsideview_close")) {
                        leftsideview.setImageResource(R.drawable.left_side_view);
                        leftsidelbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        leftsideview_close.setVisibility(View.INVISIBLE);
                    } else {
                        leftsidelbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        leftsideview_close.setVisibility(View.VISIBLE);

                    }


                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    leftsidelbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);
                    leftsideview_close.setVisibility(View.INVISIBLE);

                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here ---------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        jsonObject = null;
                        try {
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class RightSideImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public RightSideImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    if (deleteimg.equalsIgnoreCase("rightsideview_close")) {
                        rightsideview.setImageResource(R.drawable.right_side_view);
                        rightsidelbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        rightsideview_close.setVisibility(View.INVISIBLE);

                    } else {
                        rightsidelbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        rightsideview_close.setVisibility(View.VISIBLE);

                    }


                } else {
                    // Log.e("SUCCESS -->> ", "SUCCESS-2");
                    rightsideview_close.setVisibility(View.INVISIBLE);
                    rightsidelbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);


                }

                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here ---------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        jsonObject = null;
                        try {
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class RearLeftImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public RearLeftImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");
                    if (deleteimg.equalsIgnoreCase("rearleftview_close")) {
                        rearleftview.setImageResource(R.drawable.rear_left_view);
                        rearleftlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        rearleftview_close.setVisibility(View.INVISIBLE);

                    } else {
                        rearleftlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        rearleftview_close.setVisibility(View.VISIBLE);

                    }


                } else {
                    rearleftview_close.setVisibility(View.VISIBLE);
                    rearleftlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);

                    Log.e("SUCCESS -->> ", "SUCCESS-2");

                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here ---------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        jsonObject = null;
                        try {
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }


    private class FrontRightImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public FrontRightImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");

                    if (deleteimg.equalsIgnoreCase("frontright_close")) {
                        frontright.setImageResource(R.drawable.front_right_view);
                        frontrightlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        frontright_close.setVisibility(View.INVISIBLE);
                    } else {
                        frontrightlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        frontright_close.setVisibility(View.VISIBLE);
                    }

                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    frontright_close.setVisibility(View.INVISIBLE);
                    frontrightlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);
                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here ---------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        jsonObject = null;
                        try {
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }


    private class RearRightImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public RearRightImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");

                    if (deleteimg.equalsIgnoreCase("rearright_close")) {
                        rearright.setImageResource(R.drawable.rear_right_view);
                        rearrightlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        rearright_close.setVisibility(View.VISIBLE);
                    } else {
                        rearrightlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                    }

                } else {
                    rearrightlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);

                    Log.e("SUCCESS -->> ", "SUCCESS-2");

                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here ---------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        jsonObject = null;
                        try {
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class WheelImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public WheelImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");

                    if (deleteimg.equalsIgnoreCase("wheelimg_close")) {
                        wheelimg.setImageResource(R.drawable.wheel);
                        wheelslbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        wheelimg_close.setVisibility(View.INVISIBLE);
                    } else {
                        wheelslbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        wheelimg_close.setVisibility(View.VISIBLE);
                    }

                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    wheelslbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);

                    wheelimg_close.setVisibility(View.INVISIBLE);

                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here ---------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        jsonObject = null;
                        try {
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }


    private class FrontSeatImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public FrontSeatImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");
                    if (deleteimg.equalsIgnoreCase("frontseat_close")) {

                        frontseat.setImageResource(R.drawable.front_seat);
                        frontseatlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        frontseat_close.setVisibility(View.INVISIBLE);
                    } else {
                        frontseatlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        frontseat_close.setVisibility(View.VISIBLE);
                    }

                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    frontseat_close.setVisibility(View.VISIBLE);
                    frontseatlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);


                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here ---------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        jsonObject = null;
                        try {
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class BackSeatImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public BackSeatImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");

                    if (deleteimg.equalsIgnoreCase("backseat_close")) {

                        backseat.setImageResource(R.drawable.back_seat);
                        backseatlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        backseat_close.setVisibility(View.INVISIBLE);
                    } else {
                        backseatlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        backseat_close.setVisibility(View.VISIBLE);
                    }

                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    backseat_close.setVisibility(View.INVISIBLE);
                    backseatlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);


                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here ---------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        jsonObject = null;
                        try {
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class OdometerImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public OdometerImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");

                    if (deleteimg.equalsIgnoreCase("odometer_close")) {

                        odometer.setImageResource(R.drawable.odometer);
                        odameterlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        odometer_close.setVisibility(View.INVISIBLE);
                    } else {
                        odameterlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        odometer_close.setVisibility(View.VISIBLE);
                    }

                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    odometer_close.setVisibility(View.INVISIBLE);
                    odameterlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);


                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here ---------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        jsonObject = null;
                        try {
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class DasboardImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public DasboardImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");
                    if (deleteimg.equalsIgnoreCase("dasborad_close")) {

                        dasborad.setImageResource(R.drawable.dashboard);
                        dasboardlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        dasborad_close.setVisibility(View.INVISIBLE);
                    } else {
                        dasboardlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        dasborad_close.setVisibility(View.VISIBLE);
                    }
                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    dasborad_close.setVisibility(View.INVISIBLE);
                    dasboardlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);


                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here ---------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        jsonObject = null;
                        try {
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class SteeringImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public SteeringImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");

                    if (deleteimg.equalsIgnoreCase("steering_close")) {

                        steering.setImageResource(R.drawable.steering);
                        steeringlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        steering_close.setVisibility(View.INVISIBLE);
                    } else {
                        steeringlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        steering_close.setVisibility(View.VISIBLE);
                    }

                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    steering_close.setVisibility(View.INVISIBLE);
                    steeringlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);


                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here -->> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        jsonObject = null;
                        try {
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class FrontRowSideImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public FrontRowSideImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");

                    if (deleteimg.equalsIgnoreCase("frontrowseat_close")) {

                        frontrowseat.setImageResource(R.drawable.front_row_from_side);
                        frontrowlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        frontrowseat_close.setVisibility(View.INVISIBLE);
                    } else {
                        frontrowlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        frontrowseat_close.setVisibility(View.VISIBLE);
                    }

                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    frontrowseat_close.setVisibility(View.INVISIBLE);
                    frontrowlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);


                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here ---------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        jsonObject = null;
                        try {
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class BackRowSideImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public BackRowSideImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");

                    if (deleteimg.equalsIgnoreCase("backrowseat_close")) {

                        backrowseat.setImageResource(R.drawable.back_row_from_side);
                        backrowlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        backrowseat_close.setVisibility(View.INVISIBLE);
                    } else {
                        backrowlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        backrowseat_close.setVisibility(View.VISIBLE);
                    }

                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    backrowseat_close.setVisibility(View.INVISIBLE);
                    backrowlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);


                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here ---------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        jsonObject = null;
                        try {
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class DoorImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public DoorImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");

                    if (deleteimg.equalsIgnoreCase("doorimg_close")) {

                        doorimg.setImageResource(R.drawable.trunk_door_open_view);
                        doorlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        doorimg_close.setVisibility(View.INVISIBLE);
                    } else {
                        doorlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        doorimg_close.setVisibility(View.VISIBLE);
                    }

                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    doorlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);

                    doorimg_close.setVisibility(View.INVISIBLE);

                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here ---------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        jsonObject = null;
                        try {
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }


    public static void error_popup2(int str) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        dialog.setContentView(R.layout.alert_error);
        TextView Message, Confirm;
        Message = dialog.findViewById(R.id.Message);
        Confirm = dialog.findViewById(R.id.ok_error);
        dialog.setCancelable(false);
        String strI = Integer.toString(str);
        Message.setText(GlobalText.AUTH_ERROR);

        Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainActivity.logOut();
                dialog.dismiss();

            }
        });

        dialog.show();
    }

}

