package com.mfcwl.mfc_dealer.Activity.ASM_Reports;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.ASMModel.AutoFillResModel;
import com.mfcwl.mfc_dealer.ASMModel.CWRAutoFillModel;
import com.mfcwl.mfc_dealer.ASMModel.DealerName;
import com.mfcwl.mfc_dealer.ASMModel.monthlyTagetRes;
import com.mfcwl.mfc_dealer.ASMModel.monthlyTargetReq;
import com.mfcwl.mfc_dealer.ASMReportsServices.monthlySubmitService;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.retrofitconfig.CWRAutoFillService;
import com.mfcwl.mfc_dealer.retrofitconfig.DasboardStatusServices;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

public class MonthlyTargetActivity extends AppCompatActivity {

    @BindView(R.id.dealer_tv)
    public TextView dealer_tv;
    @BindView(R.id.zone_tv)
    public TextView zone_tv;
    @BindView(R.id.areaM_tv)
    public TextView areaM_tv;
    @BindView(R.id.areaM_loc_tv)
    public TextView areaM_loc_tv;
    @BindView(R.id.areaM_sapcode_tv)
    public TextView areaM_sapcode_tv;
    @BindView(R.id.areaM_state_tv)
    public TextView areaM_state_tv;
    @BindView(R.id.areaM_category_tv)
    public TextView areaM_category_tv;
    @BindView(R.id.closing_stock_tv)
    public TextView closing_stock_tv;
    @BindView(R.id.sales_conv_tv)
    public TextView sales_conv_tv;

    @BindView(R.id.omsleadsconv_tv)
    public EditText omsleadsconv_tv;

    @BindView(R.id.monthly_reset)
    public TextView monthly_reset;

    @BindView(R.id.openStocks_ev)
    public EditText openStocks_ev;
    @BindView(R.id.salesplan_ev)
    public EditText salesplan_ev;
    @BindView(R.id.proc_plan_ev)
    public EditText proc_plan_ev;
    @BindView(R.id.warranty_unit_ev)
    public EditText warranty_unit_ev;
    @BindView(R.id.warranty_values_ev)
    public EditText warranty_values_ev;
    @BindView(R.id.walkins_ev)
    public EditText walkins_ev;
    @BindView(R.id.debtor_warranty_unit_ev)
    public EditText debtor_warranty_unit_ev;
    @BindView(R.id.debtor_roi_ev)
    public EditText debtor_roi_ev;
    @BindView(R.id.proc_iep_ev)
    public EditText proc_iep_ev;
    @BindView(R.id.proc_ncd_ev)
    public EditText proc_ncd_ev;
    @BindView(R.id.proc_cpt_ev)
    public EditText proc_cpt_ev;
    @BindView(R.id.month_subcategory_ev)
    public EditText month_subcategory_ev;

    @BindView(R.id.omslead_ev)
    public TextView omslead_ev;

    @BindView(R.id.oms_conv)
    public TextView oms_conv;

    @BindView(R.id.monthly_back)
    public ImageView monthly_back;

    @BindView(R.id.submitBtn)
    public Button submitBtn;

    private Timer timer = new Timer();
    private final long DELAY = 1200;

    public String TAG = getClass().getSimpleName();
    public String dealerCatVal = "", tDate = "";
    CommonMethods db = new CommonMethods();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_monthly_target);
        Log.i(TAG, "onCreate: ");
        ButterKnife.bind(this);

        String star = "<font color='#B40404'>*</font>";
        oms_conv.setText(Html.fromHtml("Conversion through OMS leads"+star));

        setValues();

        setListeners();

        dataCalcluationSetData();




    }

    private void setListeners() {

        dealer_tv.setOnClickListener(v -> {

            if (db.isInternetWorking(MonthlyTargetActivity.this)) {
                getDealerName(v);
            } else {
                db.alertMessage(MonthlyTargetActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
            }

        });

        monthly_back.setOnClickListener(v -> {
            finish();
        });

        monthly_reset.setOnClickListener(v -> {
            resetData();
        });

        submitBtn.setOnClickListener(v -> {
            if (db.isInternetWorking(MonthlyTargetActivity.this)) {

                monthlyTargetReq req = new monthlyTargetReq();

                if (emptyCheck()) {
                    setData(req);
                    submitReportDetails(req);
                }

            } else {
                db.alertMessage(MonthlyTargetActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
            }
        });

    }

    public void setData(monthlyTargetReq req) {

        try {
            req.setDealerCode(dealerCatVal);
            req.setZone(zone_tv.getText().toString());
            req.setAreaManager(areaM_tv.getText().toString());
            req.setLocation(areaM_loc_tv.getText().toString());
            req.setSapCode(areaM_sapcode_tv.getText().toString());
            req.setState(areaM_state_tv.getText().toString());
            req.setCategory(areaM_category_tv.getText().toString());
            req.setSubCategory(month_subcategory_ev.getText().toString());
            req.setOpeningStock(openStocks_ev.getText().toString());
            req.setSalesPaln(salesplan_ev.getText().toString());
            req.setCloseingStock(closing_stock_tv.getText().toString());
            req.setProcurementPlan(proc_plan_ev.getText().toString());
            req.setWarrantyUnits(warranty_unit_ev.getText().toString());
            req.setWarrantyValue(warranty_values_ev.getText().toString());
            req.setWalkIns(walkins_ev.getText().toString());
            req.setSalesConversion(sales_conv_tv.getText().toString());
            req.setOmsLead(omslead_ev.getText().toString());
            req.setOmsLeadsConversion(omsleadsconv_tv.getText().toString());
            req.setDebitorMonth(debtor_warranty_unit_ev.getText().toString());
            req.setRoi(debtor_roi_ev.getText().toString());
            req.setProcurementThroughIep(proc_iep_ev.getText().toString());
            req.setProcurementThroughNcd(proc_ncd_ev.getText().toString());
            req.setProcurementThroughCpt(proc_cpt_ev.getText().toString());


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    int stockTotal = 0, salesconv = 0, omsconvTotal = 0;

    private void dataCalcluationSetData() {

        try {

            openStocks_ev.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {

                    int stockData = 0;
                    if (s.toString().equals("")) {
                    } else {
                        stockData = Integer.parseInt(s.toString());
                    }

                    int salesplan = 0, procplan = 0;

                    if (!salesplan_ev.getText().toString().trim().equalsIgnoreCase("")) {
                        salesplan = Integer.parseInt(salesplan_ev.getText().toString());
                    }
                    if (!proc_plan_ev.getText().toString().trim().equalsIgnoreCase("")) {
                        procplan = Integer.parseInt(proc_plan_ev.getText().toString());
                    }

                    stockTotal = (stockData + procplan) - salesplan;
                    if (stockTotal < 0) stockTotal = 0;

                    closing_stock_tv.setText(String.valueOf(stockTotal));


                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    if (s.length() > 0) {
                        int number = Integer.parseInt(s.toString());
                        stockTotal -= number;
                    }

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });


            salesplan_ev.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {

                    Log.i(TAG, "afterTextChanged: " + s);

                    int salesplan = 0;
                    if (s.toString().equals("")) {
                    } else {
                        salesplan = Integer.parseInt(s.toString());
                    }

                    int stockData = 0, procplan = 0;

                    if (!openStocks_ev.getText().toString().trim().equalsIgnoreCase("")) {
                        stockData = Integer.parseInt(openStocks_ev.getText().toString());
                    }
                    if (!proc_plan_ev.getText().toString().trim().equalsIgnoreCase("")) {
                        procplan = Integer.parseInt(proc_plan_ev.getText().toString());
                    }

                    stockTotal = (stockData + procplan) - salesplan;
                    if (stockTotal < 0) stockTotal = 0;

                    closing_stock_tv.setText(String.valueOf(stockTotal));

                    if (stockData <= salesplan && !s.toString().equals("")) {

                        timer = new Timer();
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                runOnUiThread(() -> {

                                    salesplan_ev.setText("");
                                    db.alertMessage(MonthlyTargetActivity.this,
                                            "Sales-month value should be less than addition of open-stock and procurement-plan");

                                });
                            }
                        }, DELAY);

                    }
                }


                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    if (s.length() > 0) {
                        int number = Integer.parseInt(s.toString());
                        stockTotal -= number;
                    }

                    Log.i(TAG, "beforeTextChanged: " + s);

                }


                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    if (timer != null)
                        timer.cancel();
                    Log.i(TAG, "onTextChanged: " + s);
                }
            });

            proc_plan_ev.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {

                    int procplan = 0;
                    if (s.toString().equals("")) {
                    } else {
                        procplan = Integer.parseInt(s.toString());
                    }

                    int stockData = 0, salesplan = 0;

                    if (!openStocks_ev.getText().toString().trim().equalsIgnoreCase("")) {
                        stockData = Integer.parseInt(openStocks_ev.getText().toString());
                    }
                    if (!salesplan_ev.getText().toString().trim().equalsIgnoreCase("")) {
                        salesplan = Integer.parseInt(salesplan_ev.getText().toString());
                    }

                    stockTotal = (stockData + procplan) - salesplan;
                    if (stockTotal < 0) stockTotal = 0;
                    closing_stock_tv.setText(String.valueOf(stockTotal));

                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    if (s.length() > 0) {
                        int number = Integer.parseInt(s.toString());
                        stockTotal -= number;
                    }

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });


            walkins_ev.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {

                    Log.i(TAG, "afterTextChanged: " + s);
                    float val = 1;
                    int walkins = 0;
                    if (s.toString().equals("")) {
                        walkins = -1;
                    } else {
                        walkins = Integer.parseInt(s.toString());
                    }

                    int salesplan = 0;

                    if (!salesplan_ev.getText().toString().trim().equalsIgnoreCase("")) {
                        salesplan = Integer.parseInt(salesplan_ev.getText().toString());
                    }

                    try {

                        if (walkins != -1) {
                            Float f1 = new Float(salesplan);
                            Float f2 = new Float(walkins);

                            val = (float) (f1 / f2);

                            val = val * 100;

                            Log.i(TAG, "afterTextChanged: " + val);

                            salesconv = (int) Math.round(val);

                            if (salesconv < 0) salesconv = 0;
                        } else {
                            salesconv = 0;
                        }

                    } catch (ArithmeticException e) {
                        salesconv = 0;
                    }

                    sales_conv_tv.setText(String.valueOf(salesconv));

                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    Log.i(TAG, "beforeTextChanged: " + s);

                    if (s.length() > 0) {
                        int number = Integer.parseInt(s.toString());
                        salesconv -= number;
                    }

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    Log.i(TAG, "onTextChanged: " + s);
                }
            });


            omsleadsconv_tv.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {

                    int omslead = 0;
                    if (s.toString().equals("")) {
                    } else {
                        omslead = Integer.parseInt(s.toString());
                    }
                    omsconvTotal = omslead * 20;
                    omslead_ev.setText(String.valueOf(omsconvTotal));

                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    if (s.length() > 0) {
                        int number = Integer.parseInt(s.toString());
                        omsconvTotal -= number;
                    }

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });


        } catch (Exception e) {
        }
    }

    private void setValues() {


        zone_tv.setText(db.getstringvaluefromkey(MonthlyTargetActivity.this, "user_zone"));

        Date cDate = new Date();
        tDate = new SimpleDateFormat("yyyy-MM-dd").format(cDate);
    }


    private void getDealerName(View v) {

        String zone =db.getstringvaluefromkey(MonthlyTargetActivity.this, "user_zone");

        DasboardStatusServices dash =new DasboardStatusServices();

        SpinnerManager.showSpinner(this);
        dash.getDealerNames(this, zone,new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(MonthlyTargetActivity.this);
                Response<List<DealerName>> mRes = (Response<List<DealerName>>) obj;
                List<DealerName> mData = mRes.body();
                clearData();
                displayData(mData, v);

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(MonthlyTargetActivity.this);
            }


        });
    }

    private void displayData(List<DealerName> mData, View v) {

        try {
            List<String> dealerName = new ArrayList<String>();
            List<String> dealerCode = new ArrayList<String>();

            for (int i = 0; i < mData.size(); i++) {
                dealerName.add(i, mData.get(i).getDealername());
                dealerCode.add(i, mData.get(i).getDealercode());
            }

            String[] namesArr = dealerName.toArray(new String[dealerName.size()]);
            String[] codeArr = dealerCode.toArray(new String[dealerCode.size()]);
            setAlertDialog(v, MonthlyTargetActivity.this, "Dealer Category", namesArr, codeArr);

        } catch (Exception e) {

        }

    }

    private void setAlertDialog(View v, Activity a, final String strTitle,
                                final String[] arrVal, final String[] codeVal) {

        AlertDialog.Builder alert = new AlertDialog.Builder(a);
        alert.setTitle(strTitle);
        alert.setItems(arrVal, (dialog, which) -> {

            dealer_tv.setText(arrVal[which]);
            dealerCatVal = codeVal[which];

            autoFillServiceCall(dealerCatVal, "AM", tDate, tDate);

        });
        alert.create();
        alert.show();
    }

    private void autoFillServiceCall(String dealerid, String dealerType, String startDate, String endDate) {
        SpinnerManager.showSpinner(this);
        CWRAutoFillModel cwrAutoFillModel = new CWRAutoFillModel(dealerid, dealerType, startDate, endDate);

        CWRAutoFillService cwrAutoFillService = new CWRAutoFillService();
        cwrAutoFillService.getAutoFillData(cwrAutoFillModel, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                SpinnerManager.hideSpinner(MonthlyTargetActivity.this);

                Response<AutoFillResModel> sData = (Response<AutoFillResModel>) obj;
                AutoFillResModel resModel = sData.body();

                autoFillSetData(resModel);
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(MonthlyTargetActivity.this);
            }
        });

    }

    private void autoFillSetData(AutoFillResModel resModel) {

        try {

            areaM_tv.setText(resModel.getAmName());
            areaM_loc_tv.setText(resModel.getCity());
            areaM_sapcode_tv.setText(resModel.getAccountSapCode());
            areaM_state_tv.setText(resModel.getState());
            areaM_category_tv.setText(resModel.getDealerCategory());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void submitReportDetails(monthlyTargetReq req) {

        SpinnerManager.showSpinner(this);

        monthlySubmitService monthlySubmitService = new monthlySubmitService();

        monthlySubmitService.pushMultipleData(req, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(MonthlyTargetActivity.this);
                Response<monthlyTagetRes> mRes = (Response<monthlyTagetRes>) obj;
                monthlyTagetRes mData = mRes.body();

                if (mData.getStatus().equalsIgnoreCase("SUCCESS")) {

                    db.alertMessage2(MonthlyTargetActivity.this, mData.getMessage());

                } else {
                    Toast.makeText(getApplicationContext(), mData.getMessage().toString(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(MonthlyTargetActivity.this);
            }
        });
    }

    private void clearData()
    {

        dealerCatVal = "";
        dealer_tv.setText("");
        areaM_tv.setText("");
        areaM_loc_tv.setText("");
        areaM_sapcode_tv.setText("");
        areaM_state_tv.setText("");
        areaM_category_tv.setText("");
        month_subcategory_ev.setText("");
        openStocks_ev.setText("");
        salesplan_ev.setText("");

        proc_plan_ev.setText("");
        warranty_unit_ev.setText("");
        warranty_values_ev.setText("");
        walkins_ev.setText("");

        omslead_ev.setText("");

        debtor_warranty_unit_ev.setText("");
        debtor_roi_ev.setText("");
        proc_iep_ev.setText("");
        proc_ncd_ev.setText("");
        proc_cpt_ev.setText("");

        closing_stock_tv.setText("0");
        sales_conv_tv.setText("0");
        omsleadsconv_tv.setText("");
        omslead_ev.setText("0");
        month_subcategory_ev.setText("");

    }
    private void resetData() {

        dealerCatVal = "";
        dealer_tv.setText("");
        areaM_tv.setText("");
        areaM_loc_tv.setText("");
        areaM_sapcode_tv.setText("");
        areaM_state_tv.setText("");
        areaM_category_tv.setText("");
        month_subcategory_ev.setText("");
        openStocks_ev.setText("");
        salesplan_ev.setText("");

        proc_plan_ev.setText("");
        warranty_unit_ev.setText("");
        warranty_values_ev.setText("");
        walkins_ev.setText("");

        omslead_ev.setText("");

        debtor_warranty_unit_ev.setText("");
        debtor_roi_ev.setText("");
        proc_iep_ev.setText("");
        proc_ncd_ev.setText("");
        proc_cpt_ev.setText("");

        closing_stock_tv.setText("0");
        sales_conv_tv.setText("0");
        omsleadsconv_tv.setText("0");
        omslead_ev.setText("0");
        month_subcategory_ev.setText("");

        InputMethodManager imm=(InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

    }


    private boolean emptyCheck() {

        if (areaM_tv.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please select Area Manager", Toast.LENGTH_SHORT).show();
            return false;
        } else if (dealer_tv.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please select dealer", Toast.LENGTH_SHORT).show();
            return false;
        } else if (month_subcategory_ev.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter sub category", Toast.LENGTH_SHORT).show();
            return false;
        } else if (openStocks_ev.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter opening stocks", Toast.LENGTH_SHORT).show();
            return false;
        } else if (salesplan_ev.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter sales plan for the month", Toast.LENGTH_SHORT).show();
            return false;
        } else if (proc_plan_ev.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter procurement plan", Toast.LENGTH_SHORT).show();
            return false;
        } else if (warranty_unit_ev.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter warranty unit", Toast.LENGTH_SHORT).show();
            return false;
        } else if (warranty_values_ev.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter warranty values", Toast.LENGTH_SHORT).show();
            return false;
        } else if (walkins_ev.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter walkins", Toast.LENGTH_SHORT).show();
            return false;
        } else if (omslead_ev.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter oms leads", Toast.LENGTH_SHORT).show();
            return false;
        } else if (omsleadsconv_tv.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Conversion through OMS Leads", Toast.LENGTH_SHORT).show();
            return false;
        } else if (debtor_warranty_unit_ev.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter debtor month", Toast.LENGTH_SHORT).show();
            return false;
        } else if (debtor_roi_ev.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter ROI", Toast.LENGTH_SHORT).show();
            return false;
        } else if (proc_iep_ev.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter procurement through IEP", Toast.LENGTH_SHORT).show();
            return false;
        } else if (proc_ncd_ev.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter procurement through NCD", Toast.LENGTH_SHORT).show();
            return false;
        } else if (proc_cpt_ev.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter procurement through CPT", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}
