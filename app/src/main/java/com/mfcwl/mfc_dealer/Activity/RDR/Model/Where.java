package com.mfcwl.mfc_dealer.Activity.RDR.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Where {

    @SerializedName("column")
    @Expose
    public String column;
    @SerializedName("operator")
    @Expose
    public String operator;
    @SerializedName("value")
    @Expose
    public String value;

}