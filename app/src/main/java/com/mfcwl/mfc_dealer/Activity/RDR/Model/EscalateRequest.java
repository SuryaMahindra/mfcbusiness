package com.mfcwl.mfc_dealer.Activity.RDR.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EscalateRequest {

    @SerializedName("action_id")
    @Expose
    private String actionId;
    @SerializedName("is_escalated")
    @Expose
    private String isEscalated;
    @SerializedName("comments")
    @Expose
    private String comments;

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public String getIsEscalated() {
        return isEscalated;
    }

    public void setIsEscalated(String isEscalated) {
        this.isEscalated = isEscalated;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

}