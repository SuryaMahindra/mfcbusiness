package com.mfcwl.mfc_dealer.Activity.RDR.Services;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.mfcwl.mfc_dealer.Activity.RDR.DiscussionPoint6;
import com.mfcwl.mfc_dealer.Activity.RDR.DiscussionPoint8;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GAConstants;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.DiscussionPoint;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.ScreenInformation;

import java.util.Arrays;

import imageeditor.base.BaseActivity;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.activity;

public class DiscussionPoint7 extends BaseActivity {

    TextView tvDateTime, tvDealerName, tvFocusArea, tvSubFA, tvRoyalty;
    ImageView createRepo_backbtn;
    TextView tvSalesTarget, tvBookingsInHand;
    TextView tvNoOfFinCases, tvNoOfFinCasesVal, tvFinCasePenetration,tvPendingRC,tvMIBLCases;
    TextView tvActionItems,tvResponsibilities, tvTargetDateCalendar;
    TextView tvPrevious, tvNext, tvFooterHeading;
    EditText etFinCasePenetration,etPendingRC,etMIBLCases;

    String  strActionItem ="", strResponsibility ="",strTargetDate ="",strRoyalty;
    String strBookingsInHand="", strNoOfFinanceCases="",strFinanceCasePenetration="",strUpdateOnFinCases="",strPendingRC="",strMIBLCases="";

// R4

    EditText etBookingsInHandVal,etBookingThroughOMSVal,etRetailSalesVal,etTotalSalesVal,etConversionThroughOMSVal,etFinCaseIntervention,etRoyaltyCollectedRSVal,etAggregatorCases;
    String strBookingThroughOMSVal="",strRetailSalesVal="",strTotalSalesVal="",strConversionThroughOMSVal="",strFinCaseIntervention="",strRoyaltyCollectedRSVal="",strAggregatorCases="";
LinearLayout ll_royalty_amount;
    Integer finCasesPen,updateOnFinCases,pendingRC,MIBCases;
    private Activitykiller mKiller;
     String[] royaltyArr = {"Yes","No"};
    private LinearLayout llresandtarget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discussion_point7);
        initView();

        setListeners();

        mKiller = new Activitykiller();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(mKiller,intentFilter);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mKiller);
    }

    public void initView() {

        createRepo_backbtn = findViewById(R.id.createRepo_backbtn);

        tvDateTime = findViewById(R.id.tvDateTime);
        tvDealerName = findViewById(R.id.tvDealer);
        tvFocusArea = findViewById(R.id.tvFocusArea);
        tvSubFA = findViewById(R.id.tvSubFA);
        tvRoyalty = findViewById(R.id.rayalty);


        tvSalesTarget = findViewById(R.id.tvSalesTarget);
        tvBookingsInHand=findViewById(R.id.tvBookingsInHand);

        tvFinCasePenetration=findViewById(R.id.tvFinCasePenetration);
        tvPendingRC=findViewById(R.id.tvPendingRC);
        tvMIBLCases=findViewById(R.id.tvMIBLCases);
/*
        tvUpdateOnFinCases=findViewById(R.id.tvUpdateOnFinCases);
*/
        tvNoOfFinCases=findViewById(R.id.tvNoOfFinCases);

        tvNoOfFinCasesVal = findViewById(R.id.tvNoOfFinCasesVal);
        tvActionItems=findViewById(R.id.tvActionItemList);
        tvResponsibilities = findViewById(R.id.tvResponsibilityList);
        tvTargetDateCalendar = findViewById(R.id.tvTargetDateCalendar);
        tvPrevious = findViewById(R.id.tvPrevious);
        tvNext = findViewById(R.id.tvNext);
        tvFooterHeading = findViewById(R.id.tvFooterHeading);
        tvResponsibilities.setMovementMethod(new ScrollingMovementMethod());

        etFinCasePenetration=findViewById(R.id.etFinCasePenetration);
        etPendingRC=findViewById(R.id.etPendingRC);
        etMIBLCases=findViewById(R.id.etMIBLCases);
        llresandtarget = findViewById(R.id.llresandtarget);
        ll_royalty_amount=findViewById(R.id.ll_royalty_amount);
        etBookingsInHandVal=findViewById(R.id.etBookingsInHandVal);
        etBookingThroughOMSVal= findViewById(R.id.etBookingThroughOMSVal);
        etRetailSalesVal=findViewById(R.id.etRetailSalesVal);
        etTotalSalesVal=findViewById(R.id.etTotalSalesVal);
        etConversionThroughOMSVal=findViewById(R.id.etConversionThroughOMSVal);
        etFinCaseIntervention=findViewById(R.id.etFinCaseIntervention);
        etRoyaltyCollectedRSVal=findViewById(R.id.etRoyaltyCollectedRSVal);
        etAggregatorCases=findViewById(R.id.etAggregatorCases);

    }
    public void setListeners() {

        try{
            tvPrevious.setOnClickListener(this);
            tvNext.setOnClickListener(this);
            tvActionItems.setOnClickListener(this);
            tvResponsibilities.setOnClickListener(this);
            tvRoyalty.setOnClickListener(this);
            tvTargetDateCalendar.setOnClickListener(this);
            tvDateTime.setText(getDateTime());
            tvDealerName.setText(dealerName + "(" + dealerId + ")");
            tvFocusArea.setText("Sales");
            tvSubFA.setText("Retail Sales");
            tvFooterHeading.setText("Discussion point 7");
            createRepo_backbtn.setOnClickListener(this);

            if(dealerReportResponse.retailsalestarget!=null && !dealerReportResponse.retailsalestarget.isEmpty())
            {
                tvSalesTarget.setText("Sales Target: " + dealerReportResponse.retailsalestarget);
            }
            else
            {
                tvSalesTarget.setText("Sales Target: 0");

            }


            Log.i(TAG, "setListeners: Sales Target :"+dealerReportResponse.retailsalestarget );
            etBookingsInHandVal.setText(String.valueOf(dealerReportResponse.bookingsInHand));
            tvNoOfFinCasesVal.setText(String.valueOf(dealerReportResponse.financeCases));
           /* if(dealerReportResponse.financeCasePenetration != null){
                etFinCasePenetration.setText(String.valueOf(dealerReportResponse.financeCasePenetration));

            }else {
                etFinCasePenetration.setText("");
            }*/

           /* if(dealerReportResponse.pendingRC90days != null){
                etPendingRC.setText(String.valueOf(dealerReportResponse.pendingRC90days));
            }else {
                etPendingRC.setText("");
            }
*/
           /* if(dealerReportResponse.miblCases != null){
                etMIBLCases.setText(String.valueOf(dealerReportResponse.miblCases));
            }else {
                etMIBLCases.setText("");
            }
*/
            if(dealerReportResponse.bookingsInHand!=null)
            {
                etBookingsInHandVal.setText(dealerReportResponse.bookingsInHand);
            }else
            {
                etBookingsInHandVal.setText("");
            }

            if(dealerReportResponse.bookingOMS!=null)
            {
                etBookingThroughOMSVal.setText(dealerReportResponse.bookingOMS);
            }else
            {
                etBookingThroughOMSVal.setText("");
            }

            if(dealerReportResponse.retailSales!=null)
            {
                etRetailSalesVal.setText(dealerReportResponse.getRetailSalesVal().toString());

            }else
            {
                etRetailSalesVal.setText("");
            }

            if(dealerReportResponse.totalSales!=null)
            {
                etTotalSalesVal.setText(dealerReportResponse.totalSales.toString());
            }else
            {
                etTotalSalesVal.setText("");
            }

            if(dealerReportResponse.conversionOMS!=null)
            {
                etConversionThroughOMSVal.setText(dealerReportResponse.conversionOMS.toString());
            }else
            {
                etConversionThroughOMSVal.setText("");
            }

            /*if(dealerReportResponse.updateOnFinanceCases!=null)
            {
                etFinCaseIntervention.setText(dealerReportResponse.updateOnFinanceCases.toString());
            }else
            {
                etFinCaseIntervention.setText("");
            }*/

            /*if(dealerReportResponse.royaltyAmount!=null)
            {
                etRoyaltyCollectedRSVal.setText(dealerReportResponse.royaltyAmount.toString());
            }else
            {
                etRoyaltyCollectedRSVal.setText("");
            }*/
            /*if(dealerReportResponse.aggregatorCases!=null)
            {
                etAggregatorCases.setText(dealerReportResponse.aggregatorCases.toString());
            }else
            {
                etAggregatorCases.setText("");
            }*/

        }catch (Exception e){
            e.printStackTrace();
        }
        //tvNonWarrantyCasesFollowup.setText("Non-warranty cases follow-up: " + dealerReportResponse.warrantyNaCasesFollowedUp);


        tvActionItems.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String data = s.toString();
                if(data.equalsIgnoreCase("No action required")){
                    llresandtarget.setVisibility(View.GONE);
                    strResponsibility = "NA";
                    strTargetDate = "07/17/2020";
                    Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_ACTION_ITEM, CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));

                }else {
                    llresandtarget.setVisibility(View.VISIBLE);
                    strResponsibility = "";
                    strTargetDate = "";
                    Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_ACTION_ITEM, CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            case R.id.rayalty:
                setAlertDialog(tvRoyalty,"Royalty Collected", royaltyArr);
                break;

            case R.id.createRepo_backbtn:
            case R.id.tvPrevious:
                startActivity(new Intent(this, DiscussionPoint6.class));
                break;
            case R.id.tvNext:
                if (validate()) {
                    removeAnyDuplicates(tvSubFA.getText().toString());
                    discussionPoints.add(getDiscussionPoint());
                   startActivity(new Intent(this, DiscussionPoint8.class));
                }
                break;

            case R.id.tvActionItemList:
                if (dealerReportResponse.retailSales != null){
                    setDialogMultiSelectionCheck(R.id.tvActionItemList, "Action Items", getStringArray(dealerReportResponse.retailSales));

                }else {
                    Toast.makeText(DiscussionPoint7.this,"No data found",Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    // Need to be replaced with Discussion Point 7 Once Screen information available

    public DiscussionPoint getDiscussionPoint() {
        DiscussionPoint discussionPoint = new DiscussionPoint();
        discussionPoint.setActionItem(Arrays.asList(strActionItem.split(("\\|"))));
        discussionPoint.setResponsibility(strResponsibility);
        discussionPoint.setTargetDate(strTargetDate);
        discussionPoint.setFocusArea(tvFocusArea.getText().toString());
        discussionPoint.setSubFocusArea(tvSubFA.getText().toString());

        ScreenInformation info = new ScreenInformation();
        info.setRetailsalestarget(dealerReportResponse.retailsalestarget);
        info.setBookingsInHand(strBookingsInHand);
        info.setBookingOMS(strBookingThroughOMSVal);
        info.setRetailSales(strRetailSalesVal);
        info.setTotalSales(strTotalSalesVal);
        info.setConversionOMS(strConversionThroughOMSVal);
        info.setFinanceCases(strNoOfFinanceCases);
        info.setRoyaltycollected(strRoyalty);
        info.setRoyaltyAmount(strRoyaltyCollectedRSVal);
        info.setAggregatorCases(strAggregatorCases);

        try
        {
            info.setFinanceCasePenetration(Integer.parseInt(strFinanceCasePenetration));
            info.setUpdateOnFinanceCases(Integer.parseInt(strUpdateOnFinCases));
            info.setPendingRC90days(Integer.parseInt(strPendingRC));
            info.setMiblCases(Integer.parseInt(strMIBLCases));

        }
        catch(NumberFormatException nFE)
        {

            info.setFinanceCasePenetration(0);
            info.setUpdateOnFinanceCases(0);
            info.setPendingRC90days(0);
            info.setMiblCases(0);
            nFE.printStackTrace();

        }

        discussionPoint.setScreenInformation(info);
        return discussionPoint;
    }

    private boolean validate() {
        strActionItem = tvActionItems.getText().toString();
        if(!strActionItem.equalsIgnoreCase("No action required")){
            strResponsibility = tvResponsibilities.getText().toString();
            strTargetDate = tvTargetDateCalendar.getText().toString();
        }
        strBookingsInHand=etBookingsInHandVal.getText().toString();
        strNoOfFinanceCases=tvNoOfFinCasesVal.getText().toString();
        strFinanceCasePenetration=etFinCasePenetration.getText().toString();
        /*strUpdateOnFinCases=etUpdateOnFinCases.getText().toString();*/
        strPendingRC=etPendingRC.getText().toString();
        strMIBLCases=etMIBLCases.getText().toString();
        strRoyalty = tvRoyalty.getText().toString();
        strRoyaltyCollectedRSVal=etRoyaltyCollectedRSVal.getText().toString();
        strBookingThroughOMSVal=etBookingThroughOMSVal.getText().toString();
        strBookingsInHand=etBookingsInHandVal.getText().toString();
        strRetailSalesVal=etRetailSalesVal.getText().toString();
        strTotalSalesVal=etTotalSalesVal.getText().toString();
        strConversionThroughOMSVal=etConversionThroughOMSVal.getText().toString();
        strFinCaseIntervention=etFinCaseIntervention.getText().toString();
        strAggregatorCases=etAggregatorCases.getText().toString();

        boolean allSelected = true;
        if( strActionItem.equals("") || strResponsibility.equals("") || strTargetDate.equals("") ||
                strBookingsInHand.equals("") || strNoOfFinanceCases.equals("") || strFinanceCasePenetration.equals("") ||
                strPendingRC.equals("") || strMIBLCases.equals("")|| strRoyalty.equals("") ||
                strBookingThroughOMSVal.equals("") || strRetailSalesVal.equals("") ||strTotalSalesVal.equals("")||
                strConversionThroughOMSVal.equals("")||strFinCaseIntervention.equals("")
                ||strRoyaltyCollectedRSVal.equals("")||strAggregatorCases.equals(""))
        {
            allSelected=false;
            Toast.makeText(this, "Please select all fields", Toast.LENGTH_SHORT).show();
        }
        return allSelected;
    }


    private void setAlertDialog(TextView textView,final String strTitle, final String[] arrVal) {

        AlertDialog.Builder alert = new AlertDialog.Builder(DiscussionPoint7.this);
        alert.setTitle(strTitle);
        alert.setItems(arrVal, (dialog, which) -> {

                textView.setText(arrVal[which]);

          setRoyaltyAmountEnabled();
        });
        alert.create();
        alert.show();
    }

    private void setRoyaltyAmountEnabled()
    {
        /*if(tvRoyalty.getText().toString().equalsIgnoreCase("Yes"))
        {
            etRoyaltyCollectedRSVal.setEnabled(true);
            etRoyaltyCollectedRSVal.setFocusable(true);
        }
        else */if(tvRoyalty.getText().toString().equalsIgnoreCase("No"))
        {
            etRoyaltyCollectedRSVal.setText("0");/*
            etRoyaltyCollectedRSVal.setEnabled(false);
            etRoyaltyCollectedRSVal.setFocusable(false);*/
        }
       /* else
    {
        etRoyaltyCollectedRSVal.setText("20");

        etRoyaltyCollectedRSVal.setEnabled(true);
        etRoyaltyCollectedRSVal.setFocusable(true);*/
    }


}