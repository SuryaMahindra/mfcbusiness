package com.mfcwl.mfc_dealer.Activity.Leads;

public class SortInstance {

    private static SortInstance mInstance = null;
    private String whichsort;
    private String sortby;

    private SortInstance(){
        whichsort = "";
        sortby = "";

    }

    public static SortInstance getInstance(){
        if(mInstance == null)
        {
            mInstance = new SortInstance();
        }
        return mInstance;
    }

    public String getWhichsort() {
        return whichsort;
    }

    public void setWhichsort(String whichsort) {
        this.whichsort = whichsort;
    }

    public String getSortby() {
        return sortby;
    }

    public void setSortby(String sortby) {
        this.sortby = sortby;
    }
}
