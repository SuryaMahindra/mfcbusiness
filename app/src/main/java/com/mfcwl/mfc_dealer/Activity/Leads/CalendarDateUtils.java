package com.mfcwl.mfc_dealer.Activity.Leads;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CalendarDateUtils {

    public static String todayfromDate="",todaytoDate="";

    public static String CalendarTodayDate()
    {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);

        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, 1);
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        todayfromDate = s.format(new Date(cal.getTimeInMillis()));
        todaytoDate = s.format(new Date(cal1.getTimeInMillis()));
        return todayfromDate+"@"+todaytoDate;
    }

}
