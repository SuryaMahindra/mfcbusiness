package com.mfcwl.mfc_dealer.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.mfcwl.mfc_dealer.AddStockModel.CityMaster;
import com.mfcwl.mfc_dealer.AddStockModel.ColourMaster;
import com.mfcwl.mfc_dealer.AddStockModel.CommercialMakelist;
import com.mfcwl.mfc_dealer.AddStockModel.ModelDetails;
import com.mfcwl.mfc_dealer.AddStockModel.ModelValue;
import com.mfcwl.mfc_dealer.AddStockServices.YearService;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.Global;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.Global_Urls;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddStockActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    @BindView(R.id.primarydetails)
    public LinearLayout primarydetails;
    @BindView(R.id.additionaldetails)
    public LinearLayout additionaldetails;
    @BindView(R.id.addstock_nextbtn)
    public Button addstock_nextbtn;
    @BindView(R.id.addstockbackbtn)
    public Button addstockbackbtn;
    @BindView(R.id.primarydetails_status)
    public TextView primarydetails_status;
    @BindView(R.id.additionaldetails_status)
    public TextView additionaldetails_status;

    public static TextView modelandvariant;
    @BindView(R.id.vediclelbl)
    public TextView vediclelbl;
    @BindView(R.id.stock_categorylbl)
    public TextView stock_categorylbl;
    public static TextView stockcategory_tv;
    @BindView(R.id.addstock_yesrtv)
    public TextView addstock_yesrtv;
    @BindView(R.id.addstock_monthtv)
    public TextView addstock_monthtv;
    @BindView(R.id.regmonthtv)
    public TextView regmonthtv;

    @BindView(R.id.makelbl)
    public TextView makelbl;
    @BindView(R.id.maketv)
    public TextView maketv;

    @BindView(R.id.reglbl)
    public TextView reglbl;
    //@BindView(R.id.regtv)
    public static EditText regtv;
    @BindView(R.id.kmslbl)
    public TextView kmslbl;
    @BindView(R.id.kmstv)
    public EditText kmstv;
    @BindView(R.id.colorlbl)
    public TextView colorlbl;
    @BindView(R.id.colortv)
    public TextView colortv;

    @BindView(R.id.ownerlbl)
    public TextView ownerlbl;
    public static TextView ownertv;
    @BindView(R.id.regcitylbl)
    public TextView regcitylbl;
    @BindView(R.id.regcitytv)
    public TextView regcitytv;

    public static TextView cngkittv;

    @BindView(R.id.manufacturedonlbl)
    public TextView manufacturedonlbl;
    @BindView(R.id.makevariantlbl)
    public TextView makevariantlbl;

    @BindView(R.id.chassisnolbl)
    public TextView chassisnolbl;
    @BindView(R.id.chassisnotv)
    public EditText chassisnotv;
    @BindView(R.id.enginenolbl)
    public TextView enginenolbl;
    @BindView(R.id.enginenotv)
    public EditText enginenotv;
    @BindView(R.id.boughtlbl)
    public TextView boughtlbl;
    @BindView(R.id.boughttv)
    public EditText boughttv;
    @BindView(R.id.refurelbl)
    public TextView refurelbl;
    @BindView(R.id.refuretv)

    public EditText refuretv;
    @BindView(R.id.procuretv)
    public TextView procuretv;
    @BindView(R.id.cngkitlbl)
    public TextView cngkitlbl;
    @BindView(R.id.commenttv)
    public TextView commenttv;
    @BindView(R.id.commentlentv)
    public TextView commentlentv;
    @BindView(R.id.procurexe)
    public TextView procurexe;
    @BindView(R.id.greenline_1)
    public TextView greenline_1;
    @BindView(R.id.greenline_2)
    public TextView greenline_2;

    @BindView(R.id.dealerplbl)
    public TextView dealerplbl;
    @BindView(R.id.sellingplbl)
    public TextView sellingplbl;
    //@BindView(R.id.insexpdlbl)
    public static TextView insexpdlbl;
    @BindView(R.id.inslbl)
    public TextView inslbl;
    @BindView(R.id.regonlbl)
    public TextView regonlbl;

    @BindView(R.id.sellingptv)
    public EditText sellingptv;
    @BindView(R.id.dealerptv)
    public EditText dealerptv;
    @BindView(R.id.regontv)
    public TextView regontv;
    public static TextView instv;
    //@BindView(R.id.insexpdtv)
    public static TextView insexpdtv;
    public static TextView vehicletv;

    @BindView(R.id.editcmts)
    public EditText editcmts;

    @BindView(R.id.stockcategory_error_tv)
    public TextView stockcategory_error_tv;
    @BindView(R.id.vehicle_error_tv)
    public TextView vehicle_error_tv;
    @BindView(R.id.month_error_tv)
    public TextView month_error_tv;
    @BindView(R.id.year_error_tv)
    public TextView year_error_tv;
    @BindView(R.id.make_error)
    public TextView make_error;
    @BindView(R.id.modelv_error)
    public TextView modelv_error;
    @BindView(R.id.color_error)
    public TextView color_error;
    @BindView(R.id.reg_error)
    public TextView reg_error;
    @BindView(R.id.dealerprice_error)
    public TextView dealerprice_error;
    @BindView(R.id.sellerprice_error)
    public TextView sellerprice_error;
    @BindView(R.id.owner_error)
    public TextView owner_error;
    @BindView(R.id.kms_error)
    public TextView kms_error;
    @BindView(R.id.regcity_error)
    public TextView regcity_error;
    @BindView(R.id.regon_error)
    public TextView regon_error;
    @BindView(R.id.regmonth_error)
    public TextView regmonth_error;
    //@BindView(R.id.insexp_error)
    public static TextView insexp_error;
    @BindView(R.id.ins_error)
    public TextView ins_error;
    @BindView(R.id.chassis_error)
    public TextView chassis_error;
    @BindView(R.id.engine_error)
    public TextView engine_error;
    @BindView(R.id.bought_error)
    public TextView bought_error;
    @BindView(R.id.proexe_error)
    public TextView proexe_error;
    @BindView(R.id.addstockback)
    public ImageView addstockback;
    @BindView(R.id.addstockclose)
    public ImageView addstockclose;

    //@BindView(R.id.insExline)
    public static TextView insExline;

    boolean onlyOnce = true, onlyOnceCheck = true;

    boolean nextFlag = false;
    public static Activity activity;
    public Context context;
    public String star = "<font color='#B40404'>*</font>";

    ArrayList<String> cityList;
    ArrayList<String> cityCode;
    ArrayList<String> makeList;
    ArrayList<String> yearList;
    ArrayList<String> colorIdList;
    ArrayList<String> colorList;

    //reena retrofit

    ArrayList<Integer> colorIdListRet;
    ArrayList<ColourMaster> colorListRet;
    ArrayList<String> list1;

    //

    ArrayList<String> procurementTextList;
    ArrayList<String> procurementValueList;
    ArrayList<String> procurementTextValueList;
    static ArrayList<String> modalVariant;
    static ArrayList<String> modelArr;
    static ArrayList<String> variantArr;

    static HashMap<String, String> modelhm;
    static HashMap<String, String> varianthm;

    public boolean reg, reg1;


    String cityName = "", makeName = "", yearName = "", yearregName = "", stryr = "", colourName;

    public static String vehicleType = "", vehicleCat = "", manufacYear = "", manufacMonth = "", vehicleMake = "";
    public static String vehicleModel = "", vehicleVaraiant = "", registrationNumber = "", vehicleColor = "", vehicleOwners = "", vehicleKms = "";
    public static String regYear = "", regCity = "", insurType = "", insurExpDate = "", vehicleChasisNumb = "", boughtPrice = "", refurbisCost = "", dealerPrice = "";
    public static String sellingPrice = "", createprocureExecId = "", cngKit = "", engineNumber = "", regisMonth = "", comments = "";
    public static String IsOffLoad = "false", stocktype = "";
    String timeS = "", dateS = "";
    int timeH, timeM, year, month, day, daysinmonth;
    long thismondatetime, nextmondatetime;
    long currentDate;
    ProgressDialog pd;

    public String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate: ");
        setContentView(R.layout.activity_addstock_info);
        CommonMethods.setvalueAgainstKey(this, "stockdetails", "empty");
        ButterKnife.bind(this);
        CommonMethods.MemoryClears();
        activity = this;
        context = this;
        cityList = new ArrayList<String>();
        cityCode = new ArrayList<String>();
        modalVariant = new ArrayList<String>();
        modelArr = new ArrayList<String>();
        variantArr = new ArrayList<String>();
        makeList = new ArrayList<String>();
        yearList = new ArrayList<String>();

        modelhm = new HashMap<String, String>();
        varianthm = new HashMap<String, String>();

        regtv = (EditText) findViewById(R.id.regtv);

        /*regtv.setText("ASdfasdfasdfasdf");
        regtv.setInputType(InputType.TYPE_NULL);*/

        regtv.setAllCaps(true);

        colorIdList = new ArrayList<String>();
        colorList = new ArrayList<String>();


        procurementTextList = new ArrayList<String>();
        procurementValueList = new ArrayList<String>();
        procurementTextValueList = new ArrayList<String>();
        modelandvariant = (TextView) findViewById(R.id.modelandvariant);
        vehicletv = (TextView) findViewById(R.id.vehicletv);
        stockcategory_tv = (TextView) findViewById(R.id.stockcategory_tv);
        stockcategory_tv.setInputType(InputType.TYPE_NULL);
        ownertv = (TextView) findViewById(R.id.ownertv);
        instv = (TextView) findViewById(R.id.instv);
        cngkittv = (TextView) findViewById(R.id.cngkittv);

        insexpdlbl = (TextView) findViewById(R.id.insexpdlbl);
        insexpdtv = (TextView) findViewById(R.id.insexpdtv);
        insexp_error = (TextView) findViewById(R.id.insexp_error);
        insExline = (TextView) findViewById(R.id.insExline);


        primarydetails.setVisibility(View.VISIBLE);


        additionaldetails.setVisibility(View.GONE);
        addstockbackbtn.setVisibility(View.INVISIBLE);

        primarydetails_status.setBackgroundResource(R.drawable.bottom_grayline);
        additionaldetails_status.setBackgroundResource(R.drawable.bottom_grayline);

        greenline_1.setBackgroundResource(R.color.lgreen);
        greenline_2.setBackgroundResource(R.color.lgreen);


        vediclelbl.setText(Html.fromHtml("VEHICLE " + star));
        stock_categorylbl.setText(Html.fromHtml("STOCK CATEGORY " + star));
        manufacturedonlbl.setText(Html.fromHtml("MANUFACTURED ON " + star));
        makelbl.setText(Html.fromHtml("MAKE " + star));
        makevariantlbl.setText(Html.fromHtml("MODEL AND VARIANT " + star));
        reglbl.setText(Html.fromHtml("REGISTRATION NUMBER " + star));
        colorlbl.setText(Html.fromHtml("COLOR " + star));
        ownerlbl.setText(Html.fromHtml("OWNER " + star));
        kmslbl.setText(Html.fromHtml("KMS " + star));
        regcitylbl.setText(Html.fromHtml("REG CITY " + star));
        procurexe.setText(Html.fromHtml("PROCUREMENT EXECUTIVE " + star));
        procurexe.setText(Html.fromHtml("PROCUREMENT EXECUTIVE " + star));

        dealerplbl.setText(Html.fromHtml("DEALER PRICE " + star));
        sellingplbl.setText(Html.fromHtml("SELLING PRICE " + star));
        insexpdlbl.setText(Html.fromHtml("INS. EXPIRY DATE " + star));
        inslbl.setText(Html.fromHtml("INSURANCE " + star));
        regonlbl.setText(Html.fromHtml("REGISTERED ON " + star));

        //regtv.addTextChangedListener(new mTextEditorWatcher(regtv));

        //add
        chassisnolbl.setText(Html.fromHtml("CHASSIS NUMBER " + star));
        enginenolbl.setText(Html.fromHtml("ENGINE NUMBER " + star));
        boughtlbl.setText(Html.fromHtml("BOUGHT PRICE " + star));

        /*boughttv.setHint((getResources().getString(R.string.rs) + " 0"));
        refuretv.setHint((getResources().getString(R.string.rs) + " 0"));

        sellingptv.setHint((getResources().getString(R.string.rs) + " 0"));
        dealerptv.setHint((getResources().getString(R.string.rs) + " 0"));*/
        updateFontUI(context);

        nextFlag = false;

        editcmts.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String currentText = editable.toString();
                int currentLength = currentText.length();
                commentlentv.setText(currentLength + "/230");
            }
        });

        modelandvariant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!makeName.equals("")) {
                    modelandvariant(view);

                    Log.e(TAG, "master name" + makeName);

                    // fetchModelDetails(makeName, AddStockActivity.this);

                } else {
                    Toast.makeText(AddStockActivity.this, "Please Select Make ", Toast.LENGTH_LONG).show();
                }
            }
        });

        vehicletv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                maketv.setText("");
                modelandvariant.setText("");
                String[] vehlist = {"Private", "Commercial"};
                setAlertDialog(v, activity, "Vehicle", vehlist);
            }
        });

        stockcategory_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] stockcatlist = {"Retail", "OffLoad Vehicle"};
                setAlertDialog(v, activity, "Stock Category", stockcatlist);
            }
        });
        ownertv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] ownertvlist = {"1", "2", "3", "4", "5"};
                setAlertDialog(v, activity, "Owner", ownertvlist);
            }
        });

        cngkittv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String[] cngkittvlist = {"NA", "Company Fitted", "After Market"};
                setAlertDialog(v, activity, "CNG KIT", cngkittvlist);
            }
        });
        instv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insexpdtv.setText("");
                String[] instvlist = {"Comprehensive", "Third Party", "NA"};
                setAlertDialog(v, activity, "Insurance", instvlist);
            }
        });

        //public boolean reg,reg1;

        regtv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String value = s.toString();

                if (value.length() == 1) {
                    reg = value.substring(0).matches("\\d+(?:\\.\\d+)?");
                    if (reg) {

                        reg_error.setVisibility(View.VISIBLE);
                        reg_error.setText("Please enter correct format.");

                        regtv.setText("");

                        CommonMethods.alertMessage(activity, "Please enter correct format.");
                    } else {
                        reg_error.setVisibility(View.GONE);
                    }

                } else if (value.length() >= 1) {
                    reg = value.substring(1).matches("\\d+(?:\\.\\d+)?");
                    if (reg) {

                        reg_error.setVisibility(View.VISIBLE);
                        reg_error.setText("Please enter correct format.");
                        regtv.setText("");

                        CommonMethods.alertMessage(activity, "Please enter correct format.");
                    } else {
                        if (!reg)
                            reg_error.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item, menu);
        MenuItem item = menu.findItem(R.id.notification_bell);
        item.setVisible(false);

        return true;
    }


    @OnClick(R.id.addstock_nextbtn)
    public void addstock_nextbtn(View view) {

        if (!vehicletv.getText().toString().equals("")
                && !stockcategory_tv.getText().toString().equals("")
                && !vehicletv.getText().toString().equals("")
                && !stockcategory_tv.getText().toString().equals("")
                && !addstock_yesrtv.getText().toString().equals("")
                && !addstock_monthtv.getText().toString().equals("")
                && !maketv.getText().toString().equals("")
                && !modelandvariant.getText().toString().equals("")
                && !sellingptv.getText().toString().equals("")
                && !dealerptv.getText().toString().equals("")
                && !regtv.getText().toString().equals("")
                && !colortv.getText().toString().equals("")
                && !kmstv.getText().toString().equals("")
                && !ownertv.getText().toString().equals("")
                && !regcitytv.getText().toString().equals("")
                && !regontv.getText().toString().equals("")
                && !regmonthtv.getText().toString().equals("")
                && !instv.getText().toString().equals("")) {

            if (regtv.length() < 5) {
                if (regtv.length() < 5) {
                    reg_error.setVisibility(View.VISIBLE);
                    reg_error.setText("Invalid Registration No.");
                    return;
                } else {
                        reg_error.setVisibility(View.INVISIBLE);
                }
            }

            if(regtv.getText().toString().matches("[a-zA-Z ]+")){
                reg_error.setVisibility(View.VISIBLE);
                reg_error.setText("Invalid Registration No.");
                return;
            }else{
                reg_error.setVisibility(View.INVISIBLE);
            }


            if (!instv.getText().toString().equalsIgnoreCase("NA") && insexpdtv.getText().toString().equals("")) {
                insexp_error.setVisibility(View.VISIBLE);
            } else {

                errordisables();
                primarydetails.setVisibility(View.GONE);
                additionaldetails.setVisibility(View.VISIBLE);
                addstockbackbtn.setVisibility(View.VISIBLE);

                primarydetails_status.setBackgroundResource(R.drawable.bottom_greenline);
                additionaldetails_status.setBackgroundResource(R.drawable.bottom_grayline);

                greenline_1.setBackgroundResource(R.color.successbg);
                greenline_2.setBackgroundResource(R.color.lgreen);

                if (nextFlag) {
                    Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.add_additional_stoke, GlobalText.android);

                    if (!chassisnotv.getText().toString().equals("")
                            && !enginenotv.getText().toString().equals("")
                            && !boughttv.getText().toString().equals("")
                            && !procuretv.getText().toString().equals("")) {

                        if (chassisnotv.length() < 6 || enginenotv.length() < 6) {

                            if (chassisnotv.length() < 6) {
                                chassis_error.setVisibility(View.VISIBLE);
                                chassis_error.setText("Invalid Chassis No.");
                            } else {
                                chassis_error.setVisibility(View.INVISIBLE);

                            }
                            if (enginenotv.length() < 6) {
                                engine_error.setVisibility(View.VISIBLE);
                                engine_error.setText("Invalid Engine No.");
                            } else {
                                engine_error.setVisibility(View.INVISIBLE);
                            }

                        } else {

                            primarydetails_status.setBackgroundResource(R.drawable.bottom_greenline);
                            additionaldetails_status.setBackgroundResource(R.drawable.bottom_greenline);
                            greenline_1.setBackgroundResource(R.color.successbg);
                            greenline_2.setBackgroundResource(R.color.successbg);

                            chassis_error.setVisibility(View.INVISIBLE);
                            engine_error.setVisibility(View.INVISIBLE);
                            bought_error.setVisibility(View.INVISIBLE);
                            proexe_error.setVisibility(View.INVISIBLE);

                            selectImage();
                        }
                    } else {
                        errorshow();

                    }

                } else {
                    Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.add_primary_stoke, GlobalText.android);

                    nextFlag = true;
                }



            }



        } else {

            errorText();

        }

    }

    private void errorshow() {

        if (chassisnotv.getText().toString().equals("")) {
            chassis_error.setVisibility(View.VISIBLE);
        } else {
            chassis_error.setVisibility(View.INVISIBLE);
        }

        if (enginenotv.getText().toString().equals("")) {
            engine_error.setVisibility(View.VISIBLE);
        } else {
            engine_error.setVisibility(View.INVISIBLE);
        }

        if (boughttv.getText().toString().equals("")) {
            bought_error.setVisibility(View.VISIBLE);
        } else {
            bought_error.setVisibility(View.INVISIBLE);
        }

        if (procuretv.getText().toString().equals("")) {
            proexe_error.setVisibility(View.VISIBLE);
        } else {
            proexe_error.setVisibility(View.INVISIBLE);
        }


    }

    private void errordisables() {

        vehicle_error_tv.setVisibility(View.INVISIBLE);
        stockcategory_error_tv.setVisibility(View.INVISIBLE);
        year_error_tv.setVisibility(View.INVISIBLE);
        month_error_tv.setVisibility(View.INVISIBLE);
        make_error.setVisibility(View.INVISIBLE);
        modelv_error.setVisibility(View.INVISIBLE);
        sellerprice_error.setVisibility(View.INVISIBLE);
        dealerprice_error.setVisibility(View.INVISIBLE);
        reg_error.setVisibility(View.INVISIBLE);
        color_error.setVisibility(View.INVISIBLE);
        kms_error.setVisibility(View.INVISIBLE);
        owner_error.setVisibility(View.INVISIBLE);
        regcity_error.setVisibility(View.INVISIBLE);
        regon_error.setVisibility(View.INVISIBLE);
        regmonth_error.setVisibility(View.INVISIBLE);
        ins_error.setVisibility(View.INVISIBLE);

        if (!instv.getText().toString().equalsIgnoreCase("NA")) {
            insexp_error.setVisibility(View.INVISIBLE);
        }

    }

    private void errorText() {



        if (vehicletv.getText().toString().equals("")) {
            vehicle_error_tv.setVisibility(View.VISIBLE);
        } else {
            vehicle_error_tv.setVisibility(View.INVISIBLE);
        }

        if (stockcategory_tv.getText().toString().equals("")) {
            stockcategory_error_tv.setVisibility(View.VISIBLE);
        } else {
            stockcategory_error_tv.setVisibility(View.INVISIBLE);
        }

        if (addstock_yesrtv.getText().toString().equals("")) {
            year_error_tv.setVisibility(View.VISIBLE);
        } else {
            year_error_tv.setVisibility(View.INVISIBLE);
        }

        if (addstock_monthtv.getText().toString().equals("")) {
            month_error_tv.setVisibility(View.VISIBLE);
        } else {
            month_error_tv.setVisibility(View.INVISIBLE);
        }

        if (maketv.getText().toString().equals("")) {
            make_error.setVisibility(View.VISIBLE);
        } else {
            make_error.setVisibility(View.INVISIBLE);
        }

        if (modelandvariant.getText().toString().equals("")) {
            modelv_error.setVisibility(View.VISIBLE);
        } else {
            modelv_error.setVisibility(View.INVISIBLE);
        }

        if (sellingptv.getText().toString().equals("")) {
            sellerprice_error.setVisibility(View.VISIBLE);
        } else {
            sellerprice_error.setVisibility(View.INVISIBLE);
        }

        if (dealerptv.getText().toString().equals("")) {
            dealerprice_error.setVisibility(View.VISIBLE);
        } else {
            dealerprice_error.setVisibility(View.INVISIBLE);
        }

        if (regtv.getText().toString().equals("")) {
            reg_error.setVisibility(View.VISIBLE);
        } else {
            reg_error.setVisibility(View.INVISIBLE);
        }

        if (colortv.getText().toString().equals("")) {
            color_error.setVisibility(View.VISIBLE);
        } else {
            color_error.setVisibility(View.INVISIBLE);
        }

        if (kmstv.getText().toString().equals("")) {
            kms_error.setVisibility(View.VISIBLE);
        } else {
            kms_error.setVisibility(View.INVISIBLE);
        }

        if (ownertv.getText().toString().equals("")) {
            owner_error.setVisibility(View.VISIBLE);
        } else {
            owner_error.setVisibility(View.INVISIBLE);
        }

        if (regcitytv.getText().toString().equals("")) {
            regcity_error.setVisibility(View.VISIBLE);
        } else {
            regcity_error.setVisibility(View.INVISIBLE);
        }

        if (regontv.getText().toString().equals("")) {
            regon_error.setVisibility(View.VISIBLE);
        } else {
            regon_error.setVisibility(View.INVISIBLE);
        }

        if (regmonthtv.getText().toString().equals("")) {
            regmonth_error.setVisibility(View.VISIBLE);
        } else {
            regmonth_error.setVisibility(View.INVISIBLE);
        }

        if (instv.getText().toString().equals("")) {
            ins_error.setVisibility(View.VISIBLE);
        } else {
            ins_error.setVisibility(View.INVISIBLE);
        }

        if (!instv.getText().toString().equalsIgnoreCase("NA")) {

            if (insexpdtv.getText().toString().equals("")) {
                insexp_error.setVisibility(View.VISIBLE);
            } else {
                insexp_error.setVisibility(View.INVISIBLE);
            }
        }

        if (regtv.length() < 5) {
            if (regtv.length() < 5) {
                reg_error.setVisibility(View.VISIBLE);
                reg_error.setText("Invalid Registration No.");
                return;
            } else {
                reg_error.setVisibility(View.INVISIBLE);
            }
        }

        if(regtv.getText().toString().matches("[a-zA-Z ]+")){
            reg_error.setVisibility(View.VISIBLE);
            reg_error.setText("Invalid Registration No.");
            return;
        }else{
            reg_error.setVisibility(View.INVISIBLE);
        }
    }

    private void updateFontUI(Context ctx) {

        ArrayList<Integer> arl = new ArrayList<Integer>();

        arl.add(R.id.addstock_nextbtn);
        arl.add(R.id.addstockbackbtn);
        //arl.add(R.id.modelandvariant);
        arl.add(R.id.primarydetails_status);
        arl.add(R.id.additionaldetails_status);
        arl.add(R.id.vediclelbl);
        arl.add(R.id.stock_categorylbl);
        //  arl.add(R.id.stockcategory_tv);
        //arl.add(R.id.addstock_yesrtv);
        // arl.add(R.id.addstock_monthtv);
        // arl.add(R.id.regmonthtv);
        arl.add(R.id.makelbl);
        arl.add(R.id.reglbl);
        //  arl.add(R.id.regtv);
        arl.add(R.id.kmslbl);
        //arl.add(R.id.kmstv);
        arl.add(R.id.colorlbl);
        // arl.add(R.id.colortv);
        arl.add(R.id.ownerlbl);
        // arl.add(R.id.ownertv);
        arl.add(R.id.regcitylbl);
        // arl.add(R.id.regcitytv);
        arl.add(R.id.manufacturedonlbl);
        arl.add(R.id.makevariantlbl);
        arl.add(R.id.chassisnolbl);
        //arl.add(R.id.chassisnotv);
        arl.add(R.id.enginenolbl);
        //arl.add(R.id.enginenotv);
        arl.add(R.id.boughtlbl);
        // arl.add(R.id.boughttv);
        arl.add(R.id.refurelbl);
        // arl.add(R.id.refuretv);
        // arl.add(R.id.procuretv);
        arl.add(R.id.cngkitlbl);
        // arl.add(R.id.cngkittv);
        //arl.add(R.id.commenttv);
        //arl.add(R.id.commentlentv);
        //  arl.add(R.id.sellingptv);
        //  arl.add(R.id.dealerptv);
        //  arl.add(R.id.regontv);
        //  arl.add(R.id.instv);
        //   arl.add(R.id.insexpdtv);
        arl.add(R.id.dealerplbl);
        arl.add(R.id.sellingplbl);
        arl.add(R.id.insexpdlbl);
        arl.add(R.id.inslbl);
        arl.add(R.id.regonlbl);
        // arl.add(R.id.vehicletv);

        setFontStyle(arl, context);
    }

    public void setFontStyle(ArrayList<Integer> tv_list, Context ctx) {

        for (int i = 0; i < tv_list.size(); i++) {
            Typeface myTypeface = Typeface.createFromAsset(this.getAssets(), "lato_semibold.ttf");
            TextView myTextView = (TextView) findViewById(tv_list.get(i));
            myTextView.setTypeface(myTypeface);
        }

    }

    @OnClick(R.id.addstockback)
    public void addstockback(View view) {
        finish();
    }

    @OnClick(R.id.addstockclose)
    public void addstockclose(View view) {
        finish();
    }

    @OnClick(R.id.addstockbackbtn)
    public void addstockbackbtn(View view) {

        nextFlag = false;

        primarydetails.setVisibility(View.VISIBLE);
        additionaldetails.setVisibility(View.GONE);
        addstockbackbtn.setVisibility(View.INVISIBLE);
        primarydetails_status.setBackgroundResource(R.drawable.bottom_greenline);
        additionaldetails_status.setBackgroundResource(R.drawable.bottom_grayline);


    }

    public void getMake(View view) {

        WebServicesCall.webCall(this, this, jsonMake(), "GetMake", GlobalText.GET);


    }


    public void modelandvariant(View view) {

        String makewithyear =  makeName+"/" + addstock_yesrtv.getText().toString();
        Log.e(TAG, "makewithyear" + makewithyear);
        WebServicesCall.webCall1(this, this, jsonMake(), "GetModelVariant", makewithyear, GlobalText.GET);

    }

    @OnClick(R.id.maketv)
    public void setMaketv(View view) {

        modelandvariant.setText("");

        if (vehicletv.getText().toString().equalsIgnoreCase("Commercial")) {
            CommonMethods.setvalueAgainstKey(activity, "Commercial", "true");
        } else {
            CommonMethods.setvalueAgainstKey(activity, "Commercial", "false");
        }

        if (addstock_yesrtv.getText().toString().equals("")) {
            Toast.makeText(activity, "Please Select Year ", Toast.LENGTH_LONG).show();
        } else if (addstock_monthtv.getText().toString().equals("") || addstock_monthtv.getText().toString().equals("Select")) {
            Toast.makeText(activity, "Please Select Month ", Toast.LENGTH_LONG).show();
        } else {

            MethodOfMake();
        }

       /* if (makeList.size() > 0) {
            MethodMakeListPopup(makeList);
        } else {
            // fetchCommercialMakeList(AddStockActivity.this);

            MethodOfMake();
        }*/

    }

    public String strDate;
    public static boolean isoktorepo = false;

    public void setDate(View v) {
        //  Activity a = this;

        final Calendar myCalendar = Calendar.getInstance();


        final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
            // when dialog box is closed, below method will be called.

            public void onDateSet(DatePicker view, int selectedYear,
                                  int selectedMonth, int selectedDay) {

                if (isoktorepo) {
                    myCalendar.set(Calendar.YEAR, selectedYear);
                    myCalendar.set(Calendar.MONTH, selectedMonth);
                    myCalendar.set(Calendar.DAY_OF_MONTH, selectedDay);

                   // System.out.println();
                    String strselectedDay = String.valueOf(selectedDay);
                    if (strselectedDay.length() == 1) {
                        strselectedDay = "0" + strselectedDay;
                    }
                    String strselectedMonth = String.valueOf(selectedMonth + 1);
                    if (strselectedDay.length() == 1) {
                        strselectedMonth = "0" + strselectedMonth;
                    }

                    // repodatebtn.setText(strselectedDay+"/"+strselectedMonth+"/"+selectedYear);
                    strDate = strselectedDay + "/" + strselectedMonth + "/"
                            + selectedYear;
                    SimpleDateFormat curFormater = new SimpleDateFormat("dd/MM/yyyy");
                    Date dateObj = null;
                    try {
                        dateObj = curFormater.parse(strDate);
                    } catch (Exception ex) {

                    }
                    SimpleDateFormat postFormater = new SimpleDateFormat("MMMM dd, yyyy");

                    postFormater = new SimpleDateFormat("yyyy-MM-dd");

                    insexpdtv.setText(postFormater.format(dateObj));

                }
                isoktorepo = false;
            }
        };

        final DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                datePickerListener, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() /*+ 24 * 60 * 60 * 1000*/);

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            dialog.cancel();
                            isoktorepo = false;
                        }
                    }
                });

        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            isoktorepo = true;
                            DatePicker datePicker = datePickerDialog
                                    .getDatePicker();
                            datePickerListener.onDateSet(datePicker,
                                    datePicker.getYear(),
                                    datePicker.getMonth(),
                                    datePicker.getDayOfMonth());

                        }
                    }
                });
        datePickerDialog.setCancelable(false);
        datePickerDialog.show();
    }

    @OnClick(R.id.insexpdtv)
    public void calendarPick(View view) {

        setDate(view);
        /*onlyOnce = true;
        onlyOnceCheck = false;
        getDate();
        DatePickerDialog datePickerDialog = new DatePickerDialog(AddStockActivity.this, this, year, month, day);
        datePickerDialog.getDatePicker().setMaxDate(thismondatetime);
        datePickerDialog.getDatePicker().setMinDate(currentDate);
        datePickerDialog.show();
        datePickerDialog.setCancelable(false);*/

    }

    public String getDate() {
        Calendar c = Calendar.getInstance();
        Calendar cals = Calendar.getInstance();
        Calendar calsnextmonth = Calendar.getInstance();
        //System.out.println(" Current time => " + c.getTime());

        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        Log.e("year ", "year " + day + " : " + month + " : " + year);
        daysinmonth = c.getActualMaximum(Calendar.DAY_OF_MONTH);

        Date today = new Date();
        Date thismonth = new Date(year + "/" + (month + 1) + "/" + daysinmonth);
        Date nextmonth;
        if (month >= 11) {

            nextmonth = new Date((year + 1) + "/" + (1) + "/" + daysinmonth);
        } else {
            nextmonth = new Date(year + "/" + (month + 1) + "/" + daysinmonth);
        }

        c.setTime(today);
        cals.setTime(thismonth);
        calsnextmonth.setTime(nextmonth);
        thismondatetime = cals.getTime().getTime();
// Thu Dec 21 13:30:26 GMT+05:30 2017
//Wed Nov 29 00:00:00 GMT+05:30 2017
        currentDate = c.getTime().getTime();
        nextmondatetime = calsnextmonth.getTime().getTime();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public void ProgressDialogs() {
       /* pd = new ProgressDialog(this);
        pd.setTitle("Loading...");
        pd.setMessage("Please Wait...");
        pd.setCancelable(false);

        if (!pd.isShowing()) {
            pd.show();
        }*/

        pd = new ProgressDialog(this);
        try {
            if (SplashActivity.progress) {
                pd.show();
            }
        } catch (WindowManager.BadTokenException e) {

        }
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        pd.setContentView(R.layout.progressdialog);
    }

    public void ProgressDialogsdismiss() {

        if (pd.isShowing()) {
            pd.dismiss();
        }

    }


    private void MethodYearLoadData() {

        if (yearList != null) {
            yearList.clear();
        }

        ProgressDialogs();

        JsonArrayRequest strReq = new JsonArrayRequest(Request.Method.GET, Global_Urls.addstock + "years", null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                for (int i = 0; i < response.length(); i++) {
                    try {
                        Log.e("Year ", "Year " + response.getString(i));

                        yearList.add(response.getString(i));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                ProgressDialogsdismiss();
                MethodYearListPopup(yearList);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                /*if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }*/
                ProgressDialogsdismiss();
                try {
                    error_popup2(error.networkResponse.statusCode);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json; charset=utf-8");
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));
                Log.e("token ", "requestMethods " + CommonMethods.getstringvaluefromkey(activity, "token").toString());

                return headers;
            }
        };
        Application.getInstance().addToRequestQueue(strReq);

    }

    private void MethodOfMake() {
        String url;
        if (makeList != null) {
            makeList.clear();
        }
        ProgressDialogs();

        String month = MethodofMonth_Num(addstock_monthtv.getText().toString());// Integer.parseInt(MethodofMonth_Num(addstock_monthtv.getText().toString()));
        String year = addstock_yesrtv.getText().toString();//Integer.parseInt(addstock_yesrtv.getText().toString());

        if (CommonMethods.getstringvaluefromkey(this, "Commercial").equalsIgnoreCase("true")) {
            url = Global.addstockURL + "commercial-makelist/";
        } else {
            url = Global.addstockURL + "makelist/";
        }

        url = url + year + "/" + month;
        url = url.replace(" ", "%20");

        Log.e("makelist", "makelist=======>" + url);

        JsonArrayRequest strReq = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject data = response.getJSONObject(i);
                        makeList.add(data.getString("make"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                ProgressDialogsdismiss();
                MethodMakeListPopup(makeList);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
                try {
                    error_popup2(error.networkResponse.statusCode);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json; charset=utf-8");
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));
                Log.e("token ", "requestMethods " + CommonMethods.getstringvaluefromkey(activity, "token").toString());

                return headers;
            }
        };
        Application.getInstance().addToRequestQueue(strReq);


    }




/*    @OnClick(R.id.stockcategory_tv)
    public void stockcategoryMeth(View view) {

        String [] stockcatlist= {"Retail","OffLoad Vehicle"};

        VehicleList(stockcatlist,1);
    }*/


    @OnClick(R.id.addstock_monthtv)
    public void addstockmonthMeth(View view) {
        maketv.setText("");
        modelandvariant.setText("");
        String[] stockmonthlist = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

        VehicleList(stockmonthlist, 2);
    }

/*    @OnClick(R.id.ownertv)
    public void ownerMeth(View view) {

        String [] ownertvlist= {"1","2","3","4","5"};

        VehicleList(ownertvlist,3);
    }*/

    @OnClick(R.id.regmonthtv)
    public void regmonthMeth(View view) {

        if (!regontv.getText().toString().equalsIgnoreCase("")) {

            String[] regmonthlist = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
            VehicleList(regmonthlist, 4);

        } else {
            Toast.makeText(AddStockActivity.this, "Please fill registration date.", Toast.LENGTH_LONG).show();

        }


    }

/*
    @OnClick(R.id.instv)
    public void insMeth(View view) {

        String [] instvlist= {"Comprehensive","Third Party","NA"};

        VehicleList(instvlist,5);
    }*/

/*
    @OnClick(R.id.cngkittv)
    public void cngkitMeth(View view) {

        String[] cngkittvlist = {"Comprehensive", "Third Party", "NA"};

       // VehicleList(cngkittvlist, 6);
    }
*/


    @OnClick(R.id.procuretv)
    public void procureExecId(View view) {

        if (procurementValueList.size() > 0) {
            MethodProcurementListPopup(procurementTextList, procurementValueList, procurementTextValueList);
        } else {
            MethodProcurementLoadData();
        }

    }

    @OnClick(R.id.regcitytv)
    public void registrationCity(View view) {

        //  fetchCity("citylist", AddStockActivity.this);
        if (cityList.size() > 0) {
            MethodCityListPopup(cityList, cityCode);
        } else {
            MethodCityLoadData();
        }

    }

    @OnClick(R.id.colortv)
    public void colorstv(View view) {
        //fetchColor("colour", AddStockActivity.this);

        if (colorList.size() > 0) {
            MethodColourListPopup(colorIdList, colorList);
        } else {
            MethodColorLoadData();
        }
    }

    @OnClick(R.id.addstock_yesrtv)
    public void year(View view) {

        stryr = "manufactureyear";
        maketv.setText("");
        regontv.setText("");
        regmonthtv.setText("");
        modelandvariant.setText("");
        if (yearList.size() > 0) {
            MethodYearListPopup(yearList);
        } else {
            // fetchYears(AddStockActivity.this);
            MethodYearLoadData();
        }

        // MethodYearLoadData();
        //fetchYears(AddStockActivity.this);

    }

    @OnClick(R.id.regontv)
    public void yearReg(View view) {
        regmonthtv.setText("");

        if (!addstock_yesrtv.getText().toString().equalsIgnoreCase("") &&
                !addstock_monthtv.getText().toString().equalsIgnoreCase("")) {
            stryr = "registeryear";
            if (yearList.size() > 0) {
                MethodYearListPopup(yearList);
            } else {
                MethodYearLoadData();
            }
        } else {
            Toast.makeText(AddStockActivity.this, "Please fill manufacturing date.", Toast.LENGTH_LONG).show();

        }

    }


    private void MethodProcurementLoadData() {

        if (procurementTextList != null) {
            procurementTextList.clear();
        }

        if (procurementValueList != null) {
            procurementValueList.clear();
        }

        if (procurementTextValueList != null) {
            procurementTextValueList.clear();
        }
        ProgressDialogs();
        JsonArrayRequest strReq = new JsonArrayRequest(Request.Method.GET, Global.stock_dealerURL + "dealer/active-executive-list/procurement", null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                Log.e("Me", "data " + response.toString());

                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject data = response.getJSONObject(i);
                        procurementTextList.add(data.getString("Text"));
                        procurementValueList.add(data.getString("Value"));
                        procurementTextValueList.add(data.getString("Text") + "@" + data.getString("Value"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                ProgressDialogsdismiss();
                MethodProcurementListPopup(procurementTextList, procurementValueList, procurementTextValueList);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("Me ", "error " + error.toString());
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
                try {
                    error_popup2(error.networkResponse.statusCode);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json; charset=utf-8");
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));
                Log.e("token ", "requestMethods " + CommonMethods.getstringvaluefromkey(activity, "token").toString());

                return headers;
            }
        };
        Application.getInstance().addToRequestQueue(strReq);


    }


    private void MethodColorLoadData() {

        if (colorIdList != null) {
            colorIdList.clear();
        }

        if (colorList != null) {
            colorList.clear();
        }
        ProgressDialogs();
        JsonArrayRequest strReq = new JsonArrayRequest(Request.Method.GET, Global_Urls.addstock + "colour", null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject data = response.getJSONObject(i);
                        colorIdList.add(data.getString("id"));
                        colorList.add(data.getString("colour"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                ProgressDialogsdismiss();
                MethodColourListPopup(colorIdList, colorList);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ProgressDialogsdismiss();
                try {
                    error_popup2(error.networkResponse.statusCode);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json; charset=utf-8");
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));
                Log.e("token ", "requestMethods " + CommonMethods.getstringvaluefromkey(activity, "token").toString());

                return headers;
            }
        };
        Application.getInstance().addToRequestQueue(strReq);


    }


    private void MethodCityLoadData() {

        if (cityList != null) {
            cityList.clear();
        }

        if (cityCode != null) {
            cityCode.clear();
        }
        ProgressDialogs();
        JsonArrayRequest strReq = new JsonArrayRequest(Request.Method.GET, Global.addstockURL + "citylist", null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                Log.e("MethodCityLoadData ", "data " + response.toString());

                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject data = response.getJSONObject(i);
                        Log.e("city ", "name " + data.getString("cityname"));
                        cityList.add(data.getString("cityname"));
                        cityCode.add(data.getString("citycode"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                ProgressDialogsdismiss();
                MethodCityListPopup(cityList, cityCode);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("MethodCityLoadData ", "error " + error.toString());
                /*if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }*/
                ProgressDialogsdismiss();
                try {
                    error_popup2(error.networkResponse.statusCode);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json; charset=utf-8");
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));
                Log.e("token ", "requestMethods " + CommonMethods.getstringvaluefromkey(activity, "token").toString());

                return headers;
            }
        };
        Application.getInstance().addToRequestQueue(strReq);


    }


    public static JSONObject jsonMake() {
        JSONObject jObj = new JSONObject();
        try {
            jObj.put("", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jObj;
    }

    public JSONObject jsonMakeRegularStock() {
        JSONObject jObj = new JSONObject();


        try {
            jObj.put("stock_source", stocktype);
            jObj.put("posted_date", CommonMethods.getTime());
            jObj.put("vehicle_make", vehicleMake);
            jObj.put("vehicle_model", vehicleModel);
            jObj.put("vehicle_variant", vehicleVaraiant);
            jObj.put("reg_month", MethodofMonth_Num(regisMonth));
            jObj.put("reg_year", regYear);
            jObj.put("registraion_city", regCity);
            jObj.put("registration_number", registrationNumber);
            jObj.put("colour", colortv.getText().toString());
            jObj.put("kilometer", kmstv.getText().toString());
            jObj.put("owner", ownertv.getText().toString());

            jObj.put("insurance", insurType);

            if (insurExpDate.equalsIgnoreCase("")) {
                jObj.put("insurance_exp_date", "");

            } else {
                jObj.put("insurance_exp_date", insurExpDate);

            }

            jObj.put("selling_price", sellingPrice);
            jObj.put("dealer_code", CommonMethods.getstringvaluefromkey(this, "dealer_code"));
            jObj.put("is_display", "1");
            jObj.put("bought_price", boughtPrice);
            jObj.put("refurbishment_cost", refurbisCost);
            jObj.put("procurement_executive_id", createprocureExecId);
            jObj.put("procurement_executive_name", procuretv.getText().toString());
            jObj.put("cng_kit", cngKit);

            jObj.put("chassis_number", vehicleChasisNumb);
            jObj.put("engine_number", engineNumber);

            if (vehicletv.getText().toString().equalsIgnoreCase("Private")) {
                jObj.put("private_vehicle", "true");
            } else {
                jObj.put("private_vehicle", "false");
            }

            jObj.put("comments", comments);
            jObj.put("manufacture_month", MethodofMonth_Num(manufacMonth));
            jObj.put("dealer_price", dealerPrice);
            jObj.put("is_offload", IsOffLoad);

            jObj.put("manufacture_year", manufacYear);
            jObj.put("is_featured_car", "false");
            jObj.put("created_by_device", "MOBILE");


            /*jObj.put("VehicleType", vehicleType);
            jObj.put("VehicleCategory", vehicleCat);
            jObj.put("ManufacturingYear", manufacYear);
            jObj.put("ManufacturingMonth", manufacMonth);
            jObj.put("ManufacturingMonth", manufacMonth);
            jObj.put("VehicleMake", vehicleMake);
            jObj.put("VehicleModel", vehicleModel);
            jObj.put("VehicleVariant", vehicleVaraiant);
            jObj.put("RegistrationNumber", registrationNumber);
            jObj.put("VehicleColor", vehicleColor);
            jObj.put("VehicleOwners", vehicleOwners);
            jObj.put("VehicleKms", vehicleKms);



            jObj.put("RegistrationYear", regYear);
            jObj.put("RegistrationCity", regCity);
            jObj.put("InsuranceType", insurType);
            jObj.put("InsuranceExpiryDate", insurExpDate);
            jObj.put("VehicleChassisNumber", vehicleChasisNumb);
            jObj.put("BoughtPrice", boughtPrice);
            jObj.put("RefurbishmentCost", refurbisCost);
            jObj.put("DealerPrice", dealerPrice);


            jObj.put("SellingPrice", sellingPrice);
            jObj.put("procurement_executive_id", createprocureExecId);
            jObj.put("CngKit", cngKit);
            jObj.put("EngineNumber", engineNumber);
            jObj.put("RegistrationMonth", regisMonth);
            jObj.put("Comments", comments);
            jObj.put("stocktype", stocktype);
            jObj.put("IsOffLoad", IsOffLoad);

            jObj.put("DealerCode", CommonMethods.getstringvaluefromkey(this, "dealer_code"));*/


        } catch (JSONException e) {
            Log.e("datetestException", e.getMessage());
            e.printStackTrace();
        }

        Log.e("CreatedStock", " PostJson" + jObj.toString());
        return jObj;

    }

    private void selectImage() {
        final Dialog dialog = new Dialog(this);
        //Theme_DeviceDefault_Light_DarkActionBar
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.addstocksubmitpop);
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;

        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.BOTTOM | Gravity.CENTER;
        wmlp.x = 100;   //x position
        wmlp.y = 150;   //y position

        TextView parkAndSellStock = dialog.findViewById(R.id.parkiAndSellStock);
        TextView ibbstock = dialog.findViewById(R.id.ibbstock);
        TextView regularstock = dialog.findViewById(R.id.regularstock);

        parkAndSellStock.setOnClickListener(v -> {
            Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.c2c_ibb_stock, GlobalText.android);

            stocktype = "p&s";

            vehicleType = vehicletv.getText().toString();
            vehicleCat = stockcategory_tv.getText().toString();
            manufacYear = addstock_yesrtv.getText().toString();

            manufacMonth = addstock_monthtv.getText().toString();

            MethodofMonth();

            vehicleMake = maketv.getText().toString();
            registrationNumber = regtv.getText().toString();
            vehicleColor = colortv.getText().toString();
            vehicleOwners = ownertv.getText().toString();
            vehicleKms = kmstv.getText().toString();
            regYear = regontv.getText().toString();
            regCity = regcitytv.getText().toString();
            insurType = instv.getText().toString();
            insurExpDate = insexpdtv.getText().toString();
            vehicleChasisNumb = enginenotv.getText().toString();

            boughtPrice = boughttv.getText().toString();
            refurbisCost = refuretv.getText().toString();

            dealerPrice = dealerptv.getText().toString();

            sellingPrice = sellingptv.getText().toString();
            //procureExecId = procuretv.getText().toString();
            cngKit = cngkittv.getText().toString();
            engineNumber = enginenotv.getText().toString();
            regisMonth = regmonthtv.getText().toString();
            MethodofRegMonth();
            comments = editcmts.getText().toString();


            WebServicesCall.webCall(AddStockActivity.this, AddStockActivity.this, jsonMakeRegularStock(), "AddRegularStock", GlobalText.POST);
            dialog.dismiss();

        });

        ibbstock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.c2c_ibb_stock, GlobalText.android);

                stocktype = "c2c";

                vehicleType = vehicletv.getText().toString();
                vehicleCat = stockcategory_tv.getText().toString();
                manufacYear = addstock_yesrtv.getText().toString();

                manufacMonth = addstock_monthtv.getText().toString();

                MethodofMonth();

                vehicleMake = maketv.getText().toString();
                registrationNumber = regtv.getText().toString();
                vehicleColor = colortv.getText().toString();
                vehicleOwners = ownertv.getText().toString();
                vehicleKms = kmstv.getText().toString();
                regYear = regontv.getText().toString();
                regCity = regcitytv.getText().toString();
                insurType = instv.getText().toString();
                insurExpDate = insexpdtv.getText().toString();
                vehicleChasisNumb = enginenotv.getText().toString();

                boughtPrice = boughttv.getText().toString();
                refurbisCost = refuretv.getText().toString();

                dealerPrice = dealerptv.getText().toString();

                sellingPrice = sellingptv.getText().toString();
                //procureExecId = procuretv.getText().toString();
                cngKit = cngkittv.getText().toString();
                engineNumber = enginenotv.getText().toString();
                regisMonth = regmonthtv.getText().toString();
                MethodofRegMonth();
                comments = editcmts.getText().toString();

                if (!IsOffLoad.equalsIgnoreCase("true")) {
                    //retrofit call create stock

                    WebServicesCall.webCall(AddStockActivity.this, AddStockActivity.this, jsonMakeRegularStock(), "AddRegularStock", GlobalText.POST);
                    dialog.dismiss();
                } else {

                    CommonMethods.alertMessage(activity, "Stock category:Offload", "is invalid for C2C stocks");
                    dialog.dismiss();
                }
            }
        });


        regularstock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.add_regular_stock, GlobalText.android);

                stocktype = "mfc";
                vehicleType = vehicletv.getText().toString();
                vehicleCat = stockcategory_tv.getText().toString();
                manufacYear = addstock_yesrtv.getText().toString();

                manufacMonth = addstock_monthtv.getText().toString();

                MethodofMonth();

                vehicleMake = maketv.getText().toString();
                registrationNumber = regtv.getText().toString();
                vehicleColor = colortv.getText().toString();
                vehicleOwners = ownertv.getText().toString();
                vehicleKms = kmstv.getText().toString();
                regYear = regontv.getText().toString();
                regCity = regcitytv.getText().toString();
                insurType = instv.getText().toString();
                insurExpDate = insexpdtv.getText().toString();
                vehicleChasisNumb = chassisnotv.getText().toString();

                boughtPrice = boughttv.getText().toString();
                refurbisCost = refuretv.getText().toString();

                dealerPrice = dealerptv.getText().toString();

                sellingPrice = sellingptv.getText().toString();
                //procureExecId = procuretv.getText().toString();
                cngKit = cngkittv.getText().toString();
                engineNumber = enginenotv.getText().toString();
                regisMonth = regmonthtv.getText().toString();
                MethodofRegMonth();
                comments = editcmts.getText().toString();

                WebServicesCall.webCall(AddStockActivity.this, AddStockActivity.this, jsonMakeRegularStock(), "AddRegularStock", GlobalText.POST);

                dialog.dismiss();

            }
        });

        dialog.show();
    }


    private void VehicleList(String[] vehlist, int flag) {

        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);
        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_data.getWindow().setGravity(Gravity.CENTER);
        dialog_data.setContentView(R.layout.citylist);
        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        TextView alertdialog_edittext = (EditText) dialog_data.findViewById(R.id.alertdialog_edittext);
        searchtittle.setText("Month List");
        alertdialog_edittext.setHint("Search Month");

        ImageView dialog_back_btn = dialog_data.findViewById(R.id.backicon);
        dialog_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_data.dismiss();
                //   Log.e("test error", "resee");
                //   dialog_data.cancel();
            }
        });

        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    //   dialog_data.cancel();
                }
            }

        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        ArrayAdapter<String> adapter;
        Calendar now = Calendar.getInstance();

        if (addstock_yesrtv.getText().toString().trim().equals(now.get(Calendar.YEAR) + "") && flag == 2) {
            String[] myStringArray = new String[(now.get(Calendar.MONTH) + 1)];
            for (int i = 0; i < myStringArray.length; i++) {
                for (int j = 0; j < vehlist.length; j++) {
                    myStringArray[i] = vehlist[i];
                }
            }
            adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, myStringArray);
        } else {
            if (regontv.getText().toString().trim().equals(now.get(Calendar.YEAR) + "") && flag == 4) {
                String[] myStringArray = new String[(now.get(Calendar.MONTH) + 1)];
                for (int i = 0; i < myStringArray.length; i++) {
                    for (int j = 0; j < vehlist.length; j++) {
                        myStringArray[i] = vehlist[i];
                    }
                }
                adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, myStringArray);
            } else {
                adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, vehlist);
            }

        }

        alertdialog_Listview.setAdapter(adapter);

        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                String vehli = a.getAdapter().getItem(position).toString();

                //  Log.e("IsOffLoad=","IsOffLoad="+vehli);
                if (flag == 0) {
                    vehicletv.setText(vehli);
                }
                if (flag == 1) {
              /*      if (vehli.equals("Retail")) {
                        IsOffLoad = "false";
                    } else {
                        IsOffLoad = "true";
                    }*/
                    stockcategory_tv.setText(vehli);
                }
                if (flag == 2) {
                    addstock_monthtv.setText(vehli);
                }
                if (flag == 3) {
                    ownertv.setText(vehli);
                }
                if (flag == 4) {

                    String i = MethodofMonth_Num(addstock_monthtv.getText().toString());
                    String j = MethodofMonth_Num(vehli.toString());
                    int Mmonth = Integer.parseInt(i.toString());
                    int Rmonth = Integer.parseInt(j.toString());

                    int myear = Integer.parseInt(addstock_yesrtv.getText().toString());
                    int ryear = Integer.parseInt(regontv.getText().toString());

                    Log.e("Mmonth", "Mmonth" + Mmonth);
                    /*Log.e("Rmonth", "Rmonth" + Mmonth);
                    Log.e("Mmonth", "myear" + Mmonth);
                    Log.e("Mmonth", "ryear" + Mmonth);*/

                    if (Mmonth > Rmonth) {
                        //   Log.e("enter-1", "enter-1");
                        if (myear < ryear) {
                            //       Log.e("enter-1", "enter-2");
                            regmonthtv.setText(vehli);
                        } else {
                            //       Log.e("enter-1", "enter-3");
                            regmonthtv.setText("");
                            Toast.makeText(AddStockActivity.this, "Registration month cannot be before manufacturing month.", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        regmonthtv.setText(vehli);
                    }
                }
                if (flag == 5) {
                    instv.setText(vehli);
                }
                if (flag == 6) {
                    cngkittv.setText(vehli);
                }
                TextView textview = v.findViewById(android.R.id.text1);
                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                if (dialog_data != null) {
                    dialog_data.dismiss();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);

            }
        });
        dialog_data.show();

    }

    public void Citylist(final String[] arrVal) {

        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

   /*     TextView alertdialog_textview = (TextView) dialog_data.findViewById(R.id.alertdialog_textview);
        alertdialog_textview.setText("City List");
        alertdialog_textview.setHint("search city");*/

        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }

            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, arrVal);
        alertdialog_Listview.setAdapter(adapter);

        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                String city = a.getAdapter().getItem(position).toString();

                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                if (dialog_data != null) {
                    dialog_data.dismiss();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);

            }
        });


        dialog_data.show();
    }


    public void MethodProcurementListPopup(final ArrayList procurementTextList, final ArrayList procurementValueList, final ArrayList procurementTextValueList) {
        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);
        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_data.getWindow().setGravity(Gravity.CENTER);
        dialog_data.setContentView(R.layout.citylist);

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        TextView alertdialog_edittext = (EditText) dialog_data.findViewById(R.id.alertdialog_edittext);
        searchtittle.setText("Procurement Executive List");
        alertdialog_edittext.setHint("Search Pro Executive");

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

        //procurement back button
        ImageView dialog_back_btn = dialog_data.findViewById(R.id.backicon);
        dialog_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_data.dismiss();
            }
        });
        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                }
            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, procurementTextList);
        alertdialog_Listview.setAdapter(adapter);

        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                String procureName = a.getAdapter().getItem(position).toString();

                createprocureExecId = procurementValueList.get(position).toString();

                TextView textview = v.findViewById(android.R.id.text1);
                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
                procuretv.setText(procureName);
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    // dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);

            }
        });

        dialog_data.show();
    }


    public void MethodYearListPopup(final ArrayList yearList) {


        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        TextView alertdialog_edittext = (EditText) dialog_data.findViewById(R.id.alertdialog_edittext);
        searchtittle.setText("Year List");
        alertdialog_edittext.setHint("Search Year");

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

        //reena back button

        ImageView dialog_back_btn = dialog_data.findViewById(R.id.backicon);
        dialog_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_data.dismiss();
                //   Log.e("test error", "resee");
                //    dialog_data.cancel();
            }
        });


        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    //  dialog_data.cancel();
                }

            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, yearList);
        alertdialog_Listview.setAdapter(adapter);

        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                yearName = a.getAdapter().getItem(position).toString();

                //  Log.e("yearName ", "yearName " + yearName);

                TextView textview = v.findViewById(android.R.id.text1);
                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                if (stryr.equals("manufactureyear")) {
                    addstock_yesrtv.setText(yearName);
                } else {
                    int myear = Integer.parseInt(addstock_yesrtv.getText().toString());
                    int ryear = Integer.parseInt(yearName.toString());

                    if (myear > ryear) {
                        regontv.setText("");
                        Toast.makeText(AddStockActivity.this, "Registration year cannot be before manufacturing year.", Toast.LENGTH_LONG).show();

                    } else {
                        regontv.setText(yearName);
                    }
                }


                if (dialog_data != null) {
                    dialog_data.dismiss();
                    //  dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);

            }
        });

        dialog_data.show();
    }


    public void MethodMakeListPopup(final ArrayList makeList) {

        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        TextView alertdialog_edittext = (EditText) dialog_data.findViewById(R.id.alertdialog_edittext);
        searchtittle.setText("Make List");
        alertdialog_edittext.setHint("Search Make");

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

        ImageView dialog_back_btn = dialog_data.findViewById(R.id.backicon);
        dialog_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_data.dismiss();
                // Log.e("test error", "resee");
                dialog_data.cancel();
            }
        });

        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }
            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, makeList);
        alertdialog_Listview.setAdapter(adapter);

        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                makeName = a.getAdapter().getItem(position).toString();

                Log.e("cityName ", "cityName " + cityName);

                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                maketv.setText(makeName);

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    //  dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);

            }
        });


        dialog_data.show();
    }


    public void MethodColourListPopup(final ArrayList colorIdList, final ArrayList colorList) {

        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        TextView alertdialog_edittext = (EditText) dialog_data.findViewById(R.id.alertdialog_edittext);
        searchtittle.setText("Color List");
        alertdialog_edittext.setHint("Search Color");

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

        //color back button

        ImageView dialog_back_btn = dialog_data.findViewById(R.id.backicon);
        dialog_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_data.dismiss();
                Log.e("test error", "resee");
                dialog_data.cancel();
            }
        });

        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }
            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, colorList);
        alertdialog_Listview.setAdapter(adapter);

        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                colourName = a.getAdapter().getItem(position).toString();

                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                colortv.setText(colourName);

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    //   dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);

            }
        });

        dialog_data.show();
    }


    public void MethodCityListPopup(final ArrayList citylist, final ArrayList citycode) {

        Log.e("citycode ", "citycode " + citycode.toString());

        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        TextView alertdialog_edittext = (EditText) dialog_data.findViewById(R.id.alertdialog_edittext);
        searchtittle.setText("City List");
        alertdialog_edittext.setHint("Search City");

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

   /*     TextView alertdialog_textview = (TextView) dialog_data.findViewById(R.id.alertdialog_textview);
        alertdialog_textview.setText("City List");
        alertdialog_textview.setHint("search city");*/

        ImageView dialog_back_btn = dialog_data.findViewById(R.id.backicon);
        dialog_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //regcity year back button
                dialog_data.dismiss();
                //   Log.e("test error", "resee");
                //  dialog_data.cancel();
            }
        });

        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    //   dialog_data.cancel();
                }
            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, citylist);
        alertdialog_Listview.setAdapter(adapter);

        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                cityName = a.getAdapter().getItem(position).toString();

                Log.e("cityName ", "cityName " + cityName);

                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                regcitytv.setText(cityName);

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    //  dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);

            }
        });

        dialog_data.show();
    }


    public static void Parsegetcity(JSONObject jObj, String strMethod) {

        Log.e("Parsegetcity ", "Parsegetcity " + jObj.toString());

    }

    public static void Parsegetmodalvariant(JSONObject jObj, String strMethod, Activity a) {

        Log.e("Parsegetmodalvariant ", "Parsegetmodalvariant " + jObj.toString());

        if (modalVariant != null) {
            modalVariant.clear();
        }
        if (modelArr != null) {
            modelArr.clear();
        }
        if (variantArr != null) {
            variantArr.clear();
        }
        if (modelhm != null) {
            modelhm.clear();
        }
        if (varianthm != null) {
            varianthm.clear();
        }

        try {
            JSONArray array = jObj.getJSONArray("model_values");
            for (int i = 0; i < array.length(); i++) {
                try {
                    JSONObject data = array.getJSONObject(i);
                    modalVariant.add(data.getString("display"));
                    modelArr.add(data.getString("model"));
                    variantArr.add(data.getString("variant"));

                    modelhm.put(data.getString("display"), data.getString("model"));
                    varianthm.put(data.getString("display"), data.getString("variant"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            MethodCityListPopup(modalVariant, modelArr, variantArr, a);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private static void MethodCityListPopup(ArrayList<String> modalVariant, ArrayList<String> modelarr1, ArrayList<String> variantarr1, Activity activity) {

        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);
        /*   TextView alertdialog_textview = (TextView) dialog_data.findViewById(R.id.alertdialog_textview);
        alertdialog_textview.setText("City List");
        alertdialog_textview.setHint("search city");*/

        //reena variant back button

        ImageView dialog_back_btn = dialog_data.findViewById(R.id.backicon);
        dialog_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_data.dismiss();
                // Log.e("test error", "resee");
                // dialog_data.cancel();
            }
        });

        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    //  dialog_data.cancel();
                }
            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, modalVariant);
        alertdialog_Listview.setAdapter(adapter);

        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                String model = "";
                String variant = "";

                String modVar = a.getAdapter().getItem(position).toString();

                for (Map.Entry<String, String> entry : modelhm.entrySet()) {
                    if (modVar.equalsIgnoreCase(entry.getKey())) {
                        model = entry.getValue();
                    }
                }
                for (Map.Entry<String, String> entry : varianthm.entrySet()) {
                    if (modVar.equalsIgnoreCase(entry.getKey())) {
                        variant = entry.getValue();
                    }
                }

               /* Log.e("modVar", "modVar=" + modVar);
                Log.e("model", "model=" + model);
                Log.e("variant", "variant=" + variant);*/

                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                vehicleModel = model;
                vehicleVaraiant = variant;

                modelandvariant.setText(modVar);

                if (dialog_data != null) {
                    dialog_data.dismiss();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);


            }
        });

        dialog_data.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Application.getInstance().trackScreenView(this,GlobalText.AddstockActivity);
    }

    public static void Parseaddregularstock(JSONObject jObj, String strMethod, Activity
            activity) {

        //   Log.e("CreatedStock ", "Parseaddregularstock " + jObj.toString());

        try {
            if (jObj.getString("status").equalsIgnoreCase("FAILURE")) {

                // Toast.makeText(activity, " " + jObj.getString("Message"), Toast.LENGTH_LONG).show();

                CommonMethods.alertMessage(activity, jObj.getString("Message").toString());

            } else {
                // Toast.makeText(activity, " " + jObj.getString("Message"), Toast.LENGTH_LONG).show();
                Intent in_main = new Intent(activity, AddStockSucessActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("STCKID", jObj.getString("Message"));

                in_main.putExtra("source", stocktype);
                in_main.putExtra("regno", regtv.getText().toString());
                in_main.putExtras(bundle);
                activity.startActivity(in_main);
                activity.finish();
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    private void MethodofMonth() {

        if (manufacMonth.equals("January")) {
            manufacMonth = "01";
        }


        if (manufacMonth.equals("February")) {
            manufacMonth = "02";
        }

        if (manufacMonth.equals("March")) {
            manufacMonth = "03";
        }

        if (manufacMonth.equals("April")) {
            manufacMonth = "04";
        }


        if (manufacMonth.equals("May")) {
            manufacMonth = "05";
        }

        if (manufacMonth.equals("June")) {
            manufacMonth = "06";
        }

        if (manufacMonth.equals("July")) {
            manufacMonth = "07";
        }

        if (manufacMonth.equals("August")) {
            manufacMonth = "08";
        }


        if (manufacMonth.equals("September")) {
            manufacMonth = "09";
        }

        if (manufacMonth.equals("October")) {
            manufacMonth = "10";
        }

        if (manufacMonth.equals("November")) {
            manufacMonth = "11";
        }

        if (manufacMonth.equals("December")) {
            manufacMonth = "12";
        }

    }


    private void MethodofRegMonth() {

        if (regisMonth.equals("January")) {
            regisMonth = "1";
        }


        if (regisMonth.equals("February")) {
            regisMonth = "2";
        }

        if (regisMonth.equals("March")) {
            regisMonth = "3";
        }

        if (regisMonth.equals("April")) {
            regisMonth = "4";
        }


        if (regisMonth.equals("May")) {
            regisMonth = "5";
        }

        if (regisMonth.equals("June")) {
            regisMonth = "6";
        }

        if (regisMonth.equals("July")) {
            regisMonth = "7";
        }

        if (regisMonth.equals("August")) {
            regisMonth = "8";
        }


        if (regisMonth.equals("September")) {
            regisMonth = "9";
        }

        if (regisMonth.equals("October")) {
            regisMonth = "10";
        }

        if (regisMonth.equals("November")) {
            regisMonth = "11";
        }

        if (regisMonth.equals("December")) {
            regisMonth = "12";
        }

    }


    public static void Parsegetmake(JSONObject jObj, String strMethod, Activity activity) {
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

        monthOfYear++;

        dateS = year + "-" + monthOfYear + "-" + dayOfMonth;

        getTime();

        if (onlyOnce) {

            new TimePickerDialog(AddStockActivity.this, this, timeH, timeM, true).show();    // this is the way to get the time //
            onlyOnce = false;
        }
    }

    public String getTime() {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
        Date currentLocalTime = cal.getTime();
        DateFormat date = new SimpleDateFormat("HH:mm a");
        date.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
        String localTime = date.format(currentLocalTime);

        timeH = cal.get(Calendar.HOUR_OF_DAY);
        timeM = cal.get(Calendar.MINUTE);

        return localTime;
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        if (onlyOnceCheck) {

            onlyOnceCheck = false;

        }

        timeS = hourOfDay + ":" + minute;
        insexpdtv.setText(dateS);
        dateS = dateS + " " + timeS;

    }
    /*public static Spinner spinnerCaseCategory;
    private static void spinnerCaseCategory(View v, Activity a, final String strTitle, final String[] arrVal) {

            spinnerCaseCategory = (Spinner) findViewById(R.id.spinnerCaseCategory);
        spinnerCaseCategory.setAdapter(caseCategoryAdapter);
        ArrayAdapter caseCategoryAdapter = new ArrayAdapter(a,
            R.layout.simple_spinner,
            caseCategory);
        String[] caseCategory = {"Select the casecategories", "CAT A - GREEN", "CAT B - ORANGE", "CAT C - RED"};
        spinnerCaseCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()

    {
        @Override
        public void onItemSelected (AdapterView < ? > adapterView, View view,int i, long l){

        if (spinnerCaseCategory.getSelectedItem().toString().equals("Select the casecategories")) {
        }


    }

        @Override
        public void onNothingSelected (AdapterView < ? > adapterView){

    }
    });
}*/

    private static void setAlertDialog(View v, Activity a, final String strTitle,
                                       final String[] arrVal) {

        AlertDialog.Builder alert = new AlertDialog.Builder(a);
        alert.setTitle(strTitle);
        alert.setItems(arrVal, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (strTitle.equals("Vehicle")) {
                    vehicletv.setText(arrVal[which]);

                } else if (strTitle.equals("Stock Category")) {

                    stockcategory_tv.setText(arrVal[which]);

                    Log.e("stockcategory_tv", "stockcategory_tv=" + arrVal[which]);

                    if (arrVal[which].equalsIgnoreCase("Retail")) {
                        IsOffLoad = "false";
                    } else {
                        IsOffLoad = "true";
                    }


                } else if (strTitle.equals("Stock Category")) {
                    stockcategory_tv.setText(arrVal[which]);
                } else if (strTitle.equals("Owner")) {

                    ownertv.setText(arrVal[which]);

                } else if (strTitle.equals("Insurance")) {
                    instv.setText(arrVal[which]);

                    if (instv.getText().toString().equalsIgnoreCase("NA")) {
                        insexpdlbl.setVisibility(View.INVISIBLE);
                        insexpdtv.setVisibility(View.INVISIBLE);
                        insexp_error.setVisibility(View.INVISIBLE);
                        insExline.setVisibility(View.INVISIBLE);
                    } else {
                        insexpdlbl.setVisibility(View.VISIBLE);
                        insexpdtv.setVisibility(View.VISIBLE);
                        insExline.setVisibility(View.VISIBLE);
                    }

                } else if (strTitle.equals("CNG KIT")) {
                    cngkittv.setText(arrVal[which]);
                }
            }

        });
        alert.create();
        alert.show();

    }


    public String MethodofMonth_Num(String month) {

        if (month.equalsIgnoreCase("January") || month.equalsIgnoreCase("Jan")) {
            month = "1";
            return month;
        }


        if (month.equalsIgnoreCase("February") || month.equalsIgnoreCase("Feb")) {
            month = "2";
            return month;

        }

        if (month.equalsIgnoreCase("March") || month.equalsIgnoreCase("Mar")) {
            month = "3";
            return month;

        }

        if (month.equalsIgnoreCase("April") || month.equalsIgnoreCase("Apr")) {
            month = "4";
            return month;

        }


        if (month.equalsIgnoreCase("May") || month.equalsIgnoreCase("May")) {
            month = "5";
            return month;

        }

        if (month.equalsIgnoreCase("June") || month.equalsIgnoreCase("Jun")) {
            month = "6";
            return month;

        }

        if (month.equalsIgnoreCase("July") || month.equalsIgnoreCase("Jul")) {
            month = "7";
            return month;

        }

        if (month.equals("August") || month.equalsIgnoreCase("Aug")) {
            month = "8";
            return month;

        }


        if (month.equalsIgnoreCase("September") || month.equalsIgnoreCase("Sep")) {
            month = "9";
            return month;

        }

        if (month.equals("October") || month.equalsIgnoreCase("Oct")) {
            month = "10";
            return month;

        }

        if (month.equalsIgnoreCase("November") || month.equalsIgnoreCase("Nov")) {
            month = "11";
            return month;

        }

        if (month.equalsIgnoreCase("December") || month.equalsIgnoreCase("Dec")) {
            month = "12";
            return month;

        }

        return month;

    }

    public static void error_popup2(int str) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        dialog.setContentView(R.layout.alert_error);
        TextView Message, Confirm;
        Message = dialog.findViewById(R.id.Message);
        Confirm = dialog.findViewById(R.id.ok_error);
        dialog.setCancelable(false);
        String strI = Integer.toString(str);
        if (str == 405) {
            Message.setText(GlobalText.SERVER_ERROR);

        } else {
            Message.setText(GlobalText.AUTH_ERROR);
        }

        Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainActivity.logOut();
                dialog.dismiss();

            }
        });

        dialog.show();
    }


    //retrofit calls

    private void fetchYears(final Context mContext) {

        SpinnerManager.showSpinner(mContext);

        YearService.getYearsFromServer(new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                try {
                    retrofit2.Response<Object> mResponse = (retrofit2.Response<Object>) obj;
                    String mdata = mResponse.body().toString();
                    JSONArray mJson = new JSONArray(mdata);
                    for (int i = 0; i < mJson.length(); i++) {
                        yearList.add(mJson.getString(i));
                    }
                    MethodYearListPopup(yearList);

                } catch (Exception e) {
                    e.printStackTrace();
                    SpinnerManager.hideSpinner(mContext);
                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
                mThrowable.printStackTrace();
            }
        });

    }

    //color

    public void fetchColor(String color, final Context mContext) {
        SpinnerManager.showSpinner(mContext);
        YearService.getColorFromServer(color, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                try {
                    retrofit2.Response<List<ColourMaster>> mResponse = (retrofit2.Response<List<ColourMaster>>) obj;

                    List<ColourMaster> list = mResponse.body();
                    list1 = new ArrayList<>();

                    /*for (int i = 0; i < list.size(); i++) {
                        Log.e("mater id and color", list.get(i).getColour() + " " + list.get(i).getId());
                    }*/
                } catch (Exception e) {
                    SpinnerManager.hideSpinner(mContext);
                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                Log.e("master", mThrowable.getMessage());
                SpinnerManager.hideSpinner(mContext);
            }
        });
    }

    //city
    public void fetchCity(String city, final Context mContext) {
        SpinnerManager.showSpinner(mContext);
        YearService.getCityFromServer(city, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                try {
                    retrofit2.Response<List<CityMaster>> mResponse = (retrofit2.Response<List<CityMaster>>) obj;

                    List<CityMaster> cityList = mResponse.body();
                    for (int i = 0; i < cityList.size(); i++) {
                        Log.e(TAG, "Master city: " + cityList.get(i).getCityname() + " " + cityList.get(i).getCitycode());
                    }
                } catch (Exception e) {
                    SpinnerManager.hideSpinner(mContext);
                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
            }
        });
    }

    //makelist and commercial-makelist
    public void fetchCommercialMakeList(final Context mContext) {
        SpinnerManager.showSpinner(mContext);

        if (CommonMethods.getstringvaluefromkey(this, "Commercial").equalsIgnoreCase("true")) {
            YearService.getCommercialMakelistFromServer(new HttpCallResponse() {
                @Override
                public void OnSuccess(Object obj) {
                    SpinnerManager.hideSpinner(mContext);
                    try {
                        retrofit2.Response<List<CommercialMakelist>> mResponse = (retrofit2.Response<List<CommercialMakelist>>) obj;

                        List<CommercialMakelist> commercialList = mResponse.body();
                        for (int i = 0; i < commercialList.size(); i++) {
                            Log.e(TAG, "commercialList : " + commercialList.get(i).getMake());
                        }
                    } catch (Exception e) {
                        SpinnerManager.hideSpinner(mContext);
                    }

                }

                @Override
                public void OnFailure(Throwable mThrowable) {
                    SpinnerManager.hideSpinner(mContext);
                }
            });

        } else {
            YearService.getMakelistFromServer(new HttpCallResponse() {
                @Override
                public void OnSuccess(Object obj) {
                    SpinnerManager.hideSpinner(mContext);
                    try {
                        retrofit2.Response<List<CommercialMakelist>> mResponse = (retrofit2.Response<List<CommercialMakelist>>) obj;

                        List<CommercialMakelist> commercialList = mResponse.body();
                        for (int i = 0; i < commercialList.size(); i++) {
                            Log.e(TAG, "MakeList : " + commercialList.get(i).getMake());
                        }
                    } catch (Exception e) {
                        SpinnerManager.hideSpinner(mContext);
                    }

                }

                @Override
                public void OnFailure(Throwable mThrowable) {
                    SpinnerManager.hideSpinner(mContext);
                }
            });
        }

    }

    //model details and commercial
    public void fetchModelDetails(String make, Context mContext) {
        SpinnerManager.showSpinner(mContext);
        YearService.getModelDetailsFromServer(make, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                try {
                    retrofit2.Response<ModelDetails> mResponse = (retrofit2.Response<ModelDetails>) obj;
                    List<ModelValue> modelValues = new ArrayList<>();
                    ModelDetails list = mResponse.body();
                    modelValues = list.getModelValues();

                    for (int i = 0; i < modelValues.size(); i++) {
                        Log.e(TAG, "Model Details: " + modelValues.get(i).getModel() + " " + modelValues.get(i).getVariant());
                    }
                } catch (Exception e) {
                    SpinnerManager.hideSpinner(mContext);
                    Log.e(TAG, "model: " + e.getMessage());

                }


            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                Log.e(TAG, "model: " + mThrowable.getMessage());
                SpinnerManager.hideSpinner(mContext);

            }
        });
    }

    //create stocks
    public void CreateStocks(String stock_source,
                             String posted_date,
                             String vehicle_make,
                             String vehicle_model,
                             String vehicle_variant,
                             String reg_month,
                             String reg_year,
                             String registraion_city,
                             String registration_number,
                             String colour, String kilometer,
                             String owner,
                             String insurance,
                             String insurance_exp_date,
                             String selling_price,
                             String dealer_code,
                             String is_display,
                             String bought_price,
                             String refurbishment_cost,
                             String procurement_executive_id,
                             String procurement_executive_name,
                             String cng_kit,
                             String chassis_number,
                             String engine_number,
                             String private_vehicle,
                             String comments,
                             String manufacture_month,
                             String dealer_price,
                             String is_offload,
                             String manufacture_year,
                             String is_featured_car,
                             String created_by_device) {
        YearService.getCreateStockFromServer(stock_source, posted_date, vehicle_make, vehicle_model, vehicle_variant, reg_month, reg_year, registraion_city, registration_number, colour, kilometer, owner, insurance, insurance_exp_date, selling_price, dealer_code, is_display, bought_price, refurbishment_cost, procurement_executive_id, procurement_executive_name, cng_kit, chassis_number, engine_number, private_vehicle, comments, manufacture_month, dealer_price, is_offload, manufacture_year, is_featured_car, created_by_device, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

            }

            @Override
            public void OnFailure(Throwable mThrowable) {

            }
        });

    }
}