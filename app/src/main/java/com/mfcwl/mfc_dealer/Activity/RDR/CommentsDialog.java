package com.mfcwl.mfc_dealer.Activity.RDR;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.Activity.RDR.Model.EscalateRequest;
import com.mfcwl.mfc_dealer.Activity.RDR.Services.RDRDashboardService;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;

public class CommentsDialog extends Dialog implements
        android.view.View.OnClickListener {

    public Activity c;
    public Dialog d;
    public Button cancel, ok;
    private EditText mComments;
    private String actionid;


    public CommentsDialog(Activity a,String actionid) {
        super(a);
        this.c = a;
        this.actionid = actionid;


    }
    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.comments_dialog);
        Log.i(TAG, "onCreate: ");
        cancel = findViewById(R.id.btn_cancel);
        ok = findViewById(R.id.btn_ok);
        mComments = findViewById(R.id.etcomments);
        cancel.setOnClickListener(this);
        ok.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                dismiss();
                break;
            case R.id.btn_ok:
                String comments = mComments.getText().toString();
                if (comments.isEmpty()) {
                    Toast.makeText(c, "Please enter comments", Toast.LENGTH_SHORT).show();
                    return;
                }
                EscalateRequest mReq = new EscalateRequest();
                mReq.setActionId(actionid);
                mReq.setIsEscalated("true");
                mReq.setComments(comments);
                sendEscalation(mReq);
                dismiss();

            default:
                break;
        }

    }


    private void sendEscalation(EscalateRequest mRequest) {

        RDRDashboardService.postEscalation(mRequest, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                ViewDetailedListing.getReq("All","","");;
            }

            @Override
            public void OnFailure(Throwable mThrowable) {

            }
        });

    }


}