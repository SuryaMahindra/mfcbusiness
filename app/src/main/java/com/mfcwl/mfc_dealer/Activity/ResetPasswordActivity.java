package com.mfcwl.mfc_dealer.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.mfcwl.mfc_dealer.ASMModel.signupRes;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.retrofitconfig.forgotService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import rdm.EventEditActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static rdm.retrofit_services.RetroBaseService.URL_END_DEALER_PARTICIPANTS;
import static rdm.retrofit_services.RetroBaseService.retrofitInterface;

public class ResetPasswordActivity extends AppCompatActivity {

    public Button sign_up_button;
    public ImageView BackNavigation;
    public EditText currentpassword,newPassword,confirmpass;
    String TAG = getClass().getSimpleName();
    private String UserId = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.resetpassword);
        Log.i(TAG, "onCreate: ");
        sign_up_button = (Button) findViewById(R.id.sign_up_button);
        BackNavigation = (ImageView) findViewById(R.id.BackNavigation);
        currentpassword = (EditText) findViewById(R.id.currentpassword);
        newPassword = (EditText) findViewById(R.id.newpassword);
        confirmpass = (EditText) findViewById(R.id.confirmpass);
        UserId = getIntent().getStringExtra("uname");
        sign_up_button.setOnClickListener(v -> {
            String newpas = newPassword.getText().toString().trim();
            String confim = confirmpass.getText().toString().trim();
            String oldpass = currentpassword.getText().toString().trim();

            if (oldpass.equalsIgnoreCase("")) {
                Toast.makeText(ResetPasswordActivity.this, "Please enter old password", Toast.LENGTH_LONG).show();
                return;
            }
            if (newpas.equalsIgnoreCase("")) {
                Toast.makeText(ResetPasswordActivity.this, "Please enter new password", Toast.LENGTH_LONG).show();
                return;
            }

            if (confim.equalsIgnoreCase("")) {
                Toast.makeText(ResetPasswordActivity.this, "Please enter confirm password", Toast.LENGTH_LONG).show();
                return;
            }
            if(newpas.length() < 8){
                Toast.makeText(ResetPasswordActivity.this, "Password - Minimum Length 8 characters", Toast.LENGTH_LONG).show();
                return;
            }
            if(!confim.equalsIgnoreCase(newpas)){
                Toast.makeText(ResetPasswordActivity.this, "New password and confirm password not same", Toast.LENGTH_LONG).show();

                return;

            }
            if(isValidPassword(newpas)){
                //Call API
                ResetPassRequest mReq = new ResetPassRequest();
                mReq.setUserId(UserId);
                mReq.setOldPassword(oldpass);
                mReq.setNewPassword(newpas);
                doResetPassword(mReq);

            }else {
              passwordHintDialog();
            }




        });
        BackNavigation.setOnClickListener(v -> {
            finish();
        });

    }


    @Override
    public void onBackPressed() {
        finish();
    }

    public boolean isValidPassword(final String password) {
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();

    }

    private  void passwordHintDialog(){

        final AlertDialog.Builder alert = new AlertDialog.Builder(this,R.style.AlertDialogTheme);
        alert.setTitle("Password rules");
        alert.setMessage("New password should conform to the new password rules listed below\n" +
                "\u25CF Password - Minimum Length 8 characters\n" +
                "\u25CF Password Complexity (uppercase, lowercase, special character & numbers)\n" +
                "\u25CF Allowed special characters (!,@,#,$,%,^,*,? and ,)\n" +
                "\u25CF Space is not allowed\n" +
                "\u25CF Password History = 5 (cannot set the password if it is from the last 5 password set)\n" +
                "\u25CF 48 Hrs. must have passed since last successful password reset");
        alert.setPositiveButton("Ok",
                (dialog, which) -> {
                    dialog.cancel();
                });
        alert.show();
    }


    private void doResetPassword(ResetPassRequest mReq){
        SpinnerManager.showSpinner(ResetPasswordActivity.this);

        ResetPasswordService.passwordReset(mReq, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
              SpinnerManager.hideSpinner(ResetPasswordActivity.this);
              Response<ResetPassResponse> mRes = (Response<ResetPassResponse>)obj;
                ResetPassResponse mData = mRes.body();
                if(mData.getStatus().equalsIgnoreCase("SUCCESS")){
                     passwordResetStatus(mData.getMessage());
                }else {
                    passwordResetFailed(mData.getMessage());
                }
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(ResetPasswordActivity.this);
                passwordResetFailed("Password rest failed, please try again.");
            }
        });

    }


    private  void passwordResetStatus(String message){

        final AlertDialog.Builder alert = new AlertDialog.Builder(this,R.style.AlertDialogTheme);
        alert.setTitle("MFC Business");
        alert.setMessage(message);
        alert.setPositiveButton("Ok",
                (dialog, which) -> {
                    dialog.cancel();
                    CommonMethods.setvalueAgainstKey(this,"token","");

                    Intent restpass = new Intent(this,LoginActivity.class);
                    startActivity(restpass);
                });
        alert.show();
    }

    private  void passwordResetFailed(String message){

        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("MFC Business");
        alert.setMessage(message);
        alert.setPositiveButton("Ok",
                (dialog, which) -> {
                    dialog.cancel();
                    CommonMethods.setvalueAgainstKey(this,"token","");

                });
        alert.show();
    }



}

