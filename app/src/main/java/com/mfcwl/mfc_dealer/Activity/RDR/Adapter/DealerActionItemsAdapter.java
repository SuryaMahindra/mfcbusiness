package com.mfcwl.mfc_dealer.Activity.RDR.Adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.mfcwl.mfc_dealer.Activity.RDR.CommentsDialog;
import com.mfcwl.mfc_dealer.Activity.RDR.Model.ActionCompleteRequest;
import com.mfcwl.mfc_dealer.Activity.RDR.Model.RDRActionItemData;
import com.mfcwl.mfc_dealer.Activity.RDR.Services.RDRDashboardService;
import com.mfcwl.mfc_dealer.Activity.RDR.ViewDetailedListing;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.SalesStocks.Instances.SalesCalendarFilterDialog;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class DealerActionItemsAdapter extends RecyclerView.Adapter<DealerActionItemsAdapter.MyViewHolder> implements Filterable {

    private List<RDRActionItemData> dataSet;
    private List<RDRActionItemData> contactListFiltered;
    private Context mContext;
    private String TAG= SalesCalendarFilterDialog.class.getSimpleName();

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    contactListFiltered = dataSet;
                } else {
                    List<RDRActionItemData> filteredList = new ArrayList<>();
                    for (RDRActionItemData row : dataSet) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.dealerName.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    contactListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = contactListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                contactListFiltered = (ArrayList<RDRActionItemData>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView DealerName;
        TextView actiontype;
        TextView actiondate;
        TextView actionitem;
        TextView focusarea;
        TextView subfa;
        TextView targetdate;
        TextView responsibility;
        TextView daysago;
        LinearLayout headerforcolor;
        LinearLayout llcomplete;
        TextView mComplete, mEscalate;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.DealerName = (TextView) itemView.findViewById(R.id.dealerName);
            this.actiontype = (TextView) itemView.findViewById(R.id.actiontype);
            this.actiondate = (TextView) itemView.findViewById(R.id.actiondate);
            this.actionitem = (TextView) itemView.findViewById(R.id.actionitem);
            this.focusarea = (TextView) itemView.findViewById(R.id.focusarea);
            this.subfa = (TextView) itemView.findViewById(R.id.subfa);
            this.targetdate = (TextView) itemView.findViewById(R.id.targetdate);
            this.daysago = (TextView) itemView.findViewById(R.id.daysago);
            this.responsibility = (TextView) itemView.findViewById(R.id.responsibility);
            this.headerforcolor = (LinearLayout) itemView.findViewById(R.id.headerforcolor);
            this.llcomplete = (LinearLayout) itemView.findViewById(R.id.llcomplete);
            mComplete = itemView.findViewById(R.id.Complete);
            mEscalate = itemView.findViewById(R.id.Escalate);

        }
    }

    public DealerActionItemsAdapter(List<RDRActionItemData> data, Context mContext) {
        this.dataSet = data;
        contactListFiltered = data;
        this.mContext = mContext;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rdr_actionitem_card, parent, false);
        Log.i(TAG, "onCreateViewHolder: ");
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        if (CommonMethods.getstringvaluefromkey((Activity) mContext, "user_type").equalsIgnoreCase("dealer")) {
            holder.llcomplete.setVisibility(View.VISIBLE);
        } else {
            holder.llcomplete.setVisibility(View.GONE);
        }
        RDRActionItemData mData = contactListFiltered.get(listPosition);
        holder.DealerName.setText(mData.dealerName);
        holder.actiontype.setText("" + mData.actionStatus);
        if (mData.actionStatus.equalsIgnoreCase("Completed")) {
            holder.llcomplete.setVisibility(View.GONE);
            holder.headerforcolor.setBackgroundColor(mContext.getResources().getColor(R.color.completedcolor));
            holder.actiondate.setText("Submited: " + ChangeDateFormat(mData.rdrSubmittedDate));
            if (mData.completiondate != null) {
                holder.daysago.setText(" | " + getDaysAgo(mData.completiondate));
            } else {
                holder.daysago.setText(" | " + "NA");
            }

            //  holder.actiondate.setVisibility(View.INVISIBLE);

        } else if (mData.actionStatus.equalsIgnoreCase("Overdue")) {
            if(mData.isEscalated != null && mData.isEscalated){
                holder.mEscalate.setText("Escalated");
            }else {
                holder.mEscalate.setText("Escalate");
            }
            holder.headerforcolor.setBackgroundColor(mContext.getResources().getColor(R.color.overduecolor));
            holder.actiondate.setText("From: " + ChangeDateFormat(mData.rdrSubmittedDate));
            holder.daysago.setText(" | " + getDaysAgo(mData.rdrSubmittedDate));
            holder.daysago.setVisibility(View.VISIBLE);
        } else if (mData.actionStatus.equalsIgnoreCase("Pending")) {
            if(mData.isEscalated != null && mData.isEscalated){
                holder.mEscalate.setText("Escalated");
            }else {
                holder.mEscalate.setText("Escalate");
            }
            holder.headerforcolor.setBackgroundColor(mContext.getResources().getColor(R.color.pendingcolor));
            // holder.actiondate.setText("From: " +ChangeDateFormat(mData.rdrSubmittedDate));

            holder.daysago.setText("From: " + getDaysAgo(mData.rdrSubmittedDate));
            holder.actiondate.setVisibility(View.INVISIBLE);
        } else {

            holder.headerforcolor.setBackgroundColor(mContext.getResources().getColor(R.color.pendingcolor));
            holder.actiondate.setText("From : " + ChangeDateFormat(mData.rdrSubmittedDate));
            holder.daysago.setText(" | " + getDaysAgo(mData.rdrSubmittedDate));
            holder.daysago.setVisibility(View.VISIBLE);
        }
        holder.actionitem.setText("" + mData.actionItem);
        holder.focusarea.setText("" + mData.focusArea);
        holder.subfa.setText("" + mData.subFocusArea);
        holder.targetdate.setText("" + ChangeDateFormat(mData.targetDate));
        holder.responsibility.setText("" + mData.responsibility);

        holder.mComplete.setOnClickListener(v -> {
            if (!mData.actionStatus.equalsIgnoreCase("Completed")) {
                ActionCompleteRequest request = new ActionCompleteRequest();
                request.actionId = String.valueOf(mData.actionId);
                request.actionStatus = "completed";
                sendActionStatus(request);
            } else {
                Toast.makeText(mContext, "Your action has been recorded", Toast.LENGTH_SHORT).show();
            }


        });

        holder.mEscalate.setOnClickListener(v -> {
            String escalatetext = holder.mEscalate.getText().toString();
            if(escalatetext.equalsIgnoreCase("Escalate")){
                CommentsDialog mDialog = new CommentsDialog((Activity) mContext, String.valueOf(mData.actionId));
                mDialog.setCancelable(false);
                mDialog.show();
            }else {
                Toast.makeText(mContext,"Already escalated",Toast.LENGTH_LONG).show();
            }

        });

    }

    @Override
    public int getItemCount() {
        return contactListFiltered.size();
    }


    private String ChangeDateFormat(String input) {

        String formattedDate = "";
        try {
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            SimpleDateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
            Date date = inputFormat.parse(input);
            formattedDate = outputFormat.format(date);
            System.out.println(formattedDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return formattedDate;
    }


    private String getDaysAgo(String input) {
        String outputdata = "";
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date dt = formatter.parse(input);
            Date dt1 = new Date();
            long difference = Math.abs(dt1.getTime() - dt.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);
             outputdata = Long.toString(differenceDates);
             if(outputdata.equalsIgnoreCase("0")){
                 outputdata = "1" + " days ago";
             }else {
                 outputdata = outputdata + " days ago";
             }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return outputdata;
    }

    private void sendActionStatus(ActionCompleteRequest mRequest) {
        SpinnerManager.showSpinner(mContext);

        RDRDashboardService.postAction(mRequest, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                ViewDetailedListing.getReq(ViewDetailedListing.sortbytext.getText().toString(), "", "");
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);

            }
        });

    }


}



