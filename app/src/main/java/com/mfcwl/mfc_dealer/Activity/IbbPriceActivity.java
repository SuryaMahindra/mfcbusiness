package com.mfcwl.mfc_dealer.Activity;

import android.content.Intent;
import android.graphics.Color;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Fragment.ToolIbbCpo;
import com.mfcwl.mfc_dealer.Fragment.ToolIbbPrivate;
import com.mfcwl.mfc_dealer.Fragment.ToolIbbRetail;
import com.mfcwl.mfc_dealer.Fragment.ToolTradeInPrice;
import com.mfcwl.mfc_dealer.IbbMarketTradeModel.Cpo;
import com.mfcwl.mfc_dealer.IbbMarketTradeModel.Private;
import com.mfcwl.mfc_dealer.IbbMarketTradeModel.Retail;
import com.mfcwl.mfc_dealer.IbbMarketTradeModel.Tradein;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

public class IbbPriceActivity extends AppCompatActivity {

    public TabLayout tabLayout;
    public String tradeinFairprice, tradeinBestprice, tradeinMarketprice, private1Fairprice, private1Marketprice, private1Bestprice, retailFairprice, retailMarketprice, retailBestprice, cpoFairprice, cpoMarketprice, cpoBestprice;

    public String make, variant, color, kms, year, city, model;

    public String TAG = getClass().getSimpleName();
    private Cpo cpo;
    private Tradein tradein;
    private Retail retail;
    private Private aPrivate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ibb_price);
        Log.i(TAG, "onCreate: ");

        Intent intent = getIntent();
        cpo = (Cpo) intent.getExtras().getSerializable("CPO");
        aPrivate = (Private) intent.getExtras().getSerializable("PRIVATE");
        tradein = (Tradein) intent.getExtras().getSerializable("TRADEIN");
        retail = (Retail) intent.getExtras().getSerializable("RETAIL");

        Log.e(TAG, cpo.getBestprice() + "");

        tradeinFairprice = getIntent().getStringExtra("tradeinFairprice");
        tradeinBestprice = getIntent().getStringExtra("tradeinBestprice");
        tradeinMarketprice = getIntent().getStringExtra("tradeinMarketprice");

        private1Fairprice = getIntent().getStringExtra("private1Fairprice");
        private1Marketprice = getIntent().getStringExtra("private1Marketprice");
        private1Bestprice = getIntent().getStringExtra("private1Bestprice");


        retailFairprice = getIntent().getStringExtra("retailFairprice");
        retailMarketprice = getIntent().getStringExtra("retailMarketprice");
        retailBestprice = getIntent().getStringExtra("retailBestprice");

        cpoFairprice = getIntent().getStringExtra("cpoFairprice");
        cpoMarketprice = getIntent().getStringExtra("cpoMarketprice");
        cpoBestprice = getIntent().getStringExtra("cpoBestprice");


        make = getIntent().getStringExtra("make");
        variant = getIntent().getStringExtra("variant");
        color = getIntent().getStringExtra("color");
        city = getIntent().getStringExtra("city");
        kms = getIntent().getStringExtra("kms");
        year = getIntent().getStringExtra("year");
        model = getIntent().getStringExtra("model");

        TextView makeTxt = (TextView) findViewById(R.id.make);
        TextView cityTxt = (TextView) findViewById(R.id.city);
        TextView modelTxt = (TextView) findViewById(R.id.model);
        TextView kmsTxt = (TextView) findViewById(R.id.kms);
        TextView yearTxt = (TextView) findViewById(R.id.year);
        TextView colorTxt = (TextView) findViewById(R.id.color);
        makeTxt.setText(make);
        cityTxt.setText(city);
        modelTxt.setText(model + " " + variant);
        kmsTxt.setText(kms + " kms");
        yearTxt.setText(year);
        colorTxt.setText(color);

        Log.e("test error make varint", make + " " + variant + " " + color + " " + city + " " + kms + " " + year);

        Log.e("test tradevalue", tradeinFairprice + " " + tradeinBestprice + " " + tradeinMarketprice);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount()));
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(Color.parseColor("#aaaaaa"), Color.parseColor("#2d2d2d"));
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#ffdd00"));

    }

    public class PagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        // tab titles
        private String[] tabTitles = new String[]{"Trade-in Price", "Private Price", "Retail Price", "CPO Price"};

        // overriding getPageTitle()
        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }

        public PagerAdapter(FragmentManager fm, int mNumOfTabs) {
            super(fm);
            this.mNumOfTabs = mNumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new ToolTradeInPrice(String.valueOf(tradein.getFairprice()), String.valueOf(tradein.getBestprice()), String.valueOf(tradein.getMarketprice()));
                case 1:
                    return new ToolIbbPrivate(String.valueOf(aPrivate.getFairprice()), String.valueOf(aPrivate.getMarketprice()), String.valueOf(aPrivate.getBestprice()));
                case 2:
                    return new ToolIbbRetail(String.valueOf(retail.getFairprice()), String.valueOf(retail.getMarketprice()), String.valueOf(retail.getBestprice()));
                case 3:
                    return new ToolIbbCpo(String.valueOf(cpo.getFairprice()), String.valueOf(cpo.getMarketprice()), String.valueOf(cpo.getBestprice()));
                default:
                    return null;
            }

        }

        @Override
        public int getCount() {
            return tabTitles.length;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();

        Application.getInstance().trackScreenView(this,GlobalText.IbbPriceActivity);

    }
}
