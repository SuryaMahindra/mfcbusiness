package com.mfcwl.mfc_dealer.Activity.Leads;

public class LeadsInstance {

    private static LeadsInstance mInstance = null;
    private String whichleads;
    private String addlead;
    private String leaddirection;

    private LeadsInstance() {
        whichleads = "";
        addlead = "";
        leaddirection = "";
    }

    public static LeadsInstance getInstance() {
        if (mInstance == null) {
            mInstance = new LeadsInstance();
        }
        return mInstance;
    }

    public String getWhichleads() {
        return whichleads;
    }

    public void setWhichleads(String whichleads) {
        this.whichleads = whichleads;
    }

    public String getAddlead() {
        return addlead;
    }

    public void setAddlead(String addlead) {
        this.addlead = addlead;
    }

    public String getLeaddirection() {
        return leaddirection;
    }

    public void setLeaddirection(String leaddirection) {
        this.leaddirection = leaddirection;
    }
}
