package com.mfcwl.mfc_dealer.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.mfcwl.mfc_dealer.Adapter.CustomPagerAdapter;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Fragment.BookedStockFrag;
import com.mfcwl.mfc_dealer.Fragment.StockFragment;
import com.mfcwl.mfc_dealer.Fragment.StockStoreFrag;
import com.mfcwl.mfc_dealer.InstanceCreate.ImageLibraryInstance;
import com.mfcwl.mfc_dealer.InstanceCreate.Stocklisting;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.Global;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.MonthUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import imageeditor.base.BaseActivity;

public class StockStoreInfoActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    //@BindView(R.id.car_name)
    public static TextView carName;
    //@BindView(R.id.expedition)
    public static TextView modelVariant;
    //@BindView(R.id.stock_registration_no)
    public static TextView stock_registration_no;
    //@BindView(R.id.sellingprice)
    public static TextView sellingprice;
    public static ImageView iv_price_image;
    //@BindView(R.id.ownertv)
    public static TextView ownertv;
    //@BindView(R.id.yeartv)
    public static TextView yeartv;
    //@BindView(R.id.kmstv)
    public static TextView kmstv;
    //@BindView(R.id.fueltypetv)
    public static TextView fueltypetv;
    //@BindView(R.id.regcitytv)
    public static TextView regcitytv;
    //@BindView(R.id.stock_bought_price_val)
    public static TextView stock_bought_price_val;
    //@BindView(R.id.stock_registration_no_val)
    public static TextView stock_registration_no_val;
    //@BindView(R.id.stock_refurbishment_cost_val)
    public static TextView stock_refurbishment_cost_val;
    //@BindView(R.id.stock_certification_no_val)
    public static TextView stock_certification_no_val;
    //@BindView(R.id.stock_dealer_price_val)
    public static TextView stock_dealer_price_val;
    //@BindView(R.id.stock_insurance_val)
    public static TextView stock_insurance_val;
    //@BindView(R.id.stock_procurement_executive)
    public static TextView stock_procurement_executive;
    //@BindView(R.id.stock_insurance_expiry_date)
    public static TextView stock_insurance_expiry_date;
    //@BindView(R.id.stock_cng_kit)
    public static TextView stock_cng_kit;
    ///@BindView(R.id.stock_chassis_number)
    public static TextView stock_chassis_number;
    //@BindView(R.id.stock_enginenotv)
    public static TextView stock_enginenotv;
    // @BindView(R.id.stock_source)
    public static TextView stock_source;
    //@BindView(R.id.stock_warrantytv)
    public static TextView stock_warrantytv;
    //@BindView(R.id.stock_vehicletype)
    public static TextView stock_vehicletype;
    //@BindView(R.id.stock_cattv)
    public static TextView stock_cattv;
    //@BindView(R.id.stock_comments)
    public static TextView stock_comments;
    public static TextView stock_img_count;

    //@BindView(R.id.post_on_lab)
    public static TextView post_on_lab;
    //@BindView(R.id.post_on)
    public static TextView postOn;
    public static TextView days;

    // @BindView(R.id.viewpager)
    public static ViewPager viewPager;
    @BindView(R.id.left_nav)
    public ImageView leftNav;

    @BindView(R.id.bookstockbtn_lin)
    LinearLayout bookstockbtn_lin;
    @BindView(R.id.editstockbtn_lin)
    LinearLayout editstockbtn_lin;
    //@BindView(R.id.lldeletebtn)
    static LinearLayout lldeletebtn;
    //@BindView(R.id.bookstockbtn)
    public static TextView bookstockbtn;
    @BindView(R.id.editstockbtn)
    TextView editstockbtn;

    LinearLayout buttom_ll;

    ArrayList<String> salesexecIdList;
    ArrayList<String> salesexecList;

    public static Activity activity;
    public static Context context;
    public static String isCertifiedWhatsupdata;
    ImageView rightNav;
    @BindView(R.id.collapsing_toolbar)
    public CollapsingToolbarLayout collapsingToolbarLayout;
    public static String make, variantModel, variant, regno, selling_price, owner, Regyear, kms, registraionCity, certifiedNo, insurance = "", insuranceExp_date,
            source = "", warranty, surveyor_remark, photo_count, reg_month, fuel_type, bought, refurbishment_cost, dealer_price, procurement_executive_id, cng_kit, chassis_number, engine_number, is_display, is_certified, is_featured_car_admin, procurement_executive_name, private_vehicle, comments, finance_required, manufacture_month, sales_executive_id, sales_executive_name, manufacture_year, actual_selling_price, CertifiedText, IsDisplay, TransmissionType, colour, dealer_code, is_offload, posted_date, video_url = "", stock_id = "", is_booked = "", is_sold = "", coverimage;
    public static String is_featured_car;
    public static TextView stock_Certified_lbl;
    public static String[] imageurl;
    public static String[] imagename;
    public static String share_Content = "";
    public static String imageCount = "";
    public static int imageLenght = 0;
    public static MenuItem item;
    public static Menu menus;
    public int current = 0, last = 0;
    public static ProgressDialog progress;
    ProgressDialog pd;

    public Bitmap bitmap1 = null,
            bitmap2 = null,
            bitmap3 = null,
            bitmap4 = null,
            bitmap5 = null,
            bitmap6 = null,
            bitmap7 = null,
            bitmap8 = null,
            bitmap9 = null,
            bitmap10 = null,
            bitmap11 = null,
            bitmap12 = null,
            bitmap13 = null,
            bitmap14 = null,
            bitmap15 = null,
            bitmap16 = null,
            bitmap17 = null;

    public static boolean coverimgFlag = false,
            frontviewFlag = false,
            leftsideviewFlag = false,
            rightsideviewFlag = false,
            rearleftviewFlag = false,
            rearrightviewFlag = false,
            rearviewFlag = false,
            frontrightviewFlag = false,
            wheelstviewFlag = false,
            front_seatsFlag = false,
            back_seatsFlag = false,
            odometerFlag = false,
            dashboardFlag = false,
            steeringFlag = false,
            front_row_sideFlag = false,
            back_row_sideFlag = false,
            trunk_door_open_viewFlag = false;

    private ArrayList<Uri> imageUriArray;

    static String TAG = StockStoreInfoActivity.class.getSimpleName();

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate: ");
        setContentView(R.layout.activity_stock_store_info);
        ButterKnife.bind(this);
        activity = this;
        context = this;
        salesexecIdList = new ArrayList<String>();
        salesexecList = new ArrayList<String>();
        progress = new ProgressDialog(activity);

        lldeletebtn = (LinearLayout)findViewById(R.id.lldeletebtn);

        if (getIntent() != null) {

            if (getIntent().getStringExtra("source") != null)
                source = getIntent().getStringExtra("source");

            if (getIntent().getStringExtra("is_booked") != null)
                is_booked = getIntent().getStringExtra("is_booked");

            if (getIntent().getStringExtra("is_sold") != null)
                is_sold = getIntent().getStringExtra("is_sold");

            setDeleteButtonStatus();
        }
        //===========False
        // StockFragment.onBack = false;

        imageurl = new String[]{};
        imagename = new String[]{};
        setFalse();
        bitmap1 = null;
        bitmap2 = null;
        bitmap3 = null;
        bitmap4 = null;
        bitmap5 = null;
        bitmap6 = null;
        bitmap7 = null;
        bitmap8 = null;
        bitmap9 = null;
        bitmap10 = null;
        bitmap11 = null;
        bitmap12 = null;
        bitmap13 = null;
        bitmap14 = null;
        bitmap15 = null;
        bitmap16 = null;
        bitmap17 = null;

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        ViewCompat.setTransitionName(findViewById(R.id.app_bar_layout), null);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");


        collapsingToolbarLayout.setTitle("");
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        //slider image
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        leftNav = (ImageView) findViewById(R.id.left_nav);
        rightNav = (ImageView) findViewById(R.id.right_nav);
        bookstockbtn = (TextView) findViewById(R.id.bookstockbtn);
        stock_source = (TextView) findViewById(R.id.stock_source);
        stock_comments = (TextView) findViewById(R.id.stock_comments);

        stock_vehicletype = (TextView) findViewById(R.id.stock_vehicletype);
        stock_img_count = (TextView) findViewById(R.id.stock_img_count);
        postOn = (TextView) findViewById(R.id.post_on);
        stock_Certified_lbl = (TextView) findViewById(R.id.stock_Certified_lbl);
        days = (TextView) findViewById(R.id.days);
        post_on_lab = (TextView) findViewById(R.id.post_on_lab);
        stock_chassis_number = (TextView) findViewById(R.id.stock_chassis_number);
        stock_enginenotv = (TextView) findViewById(R.id.stock_enginenotv);
        carName = (TextView) findViewById(R.id.car_name);
        stock_procurement_executive = (TextView) findViewById(R.id.stock_procurement_executive);
        modelVariant = (TextView) findViewById(R.id.expedition);
        sellingprice = (TextView) findViewById(R.id.sellingprice);
        iv_price_image = (ImageView) findViewById(R.id.iv_price_image);
        ownertv = (TextView) findViewById(R.id.ownertv);
        stock_refurbishment_cost_val = (TextView) findViewById(R.id.stock_refurbishment_cost_val);
        yeartv = (TextView) findViewById(R.id.yeartv);
        kmstv = (TextView) findViewById(R.id.kmstv);
        fueltypetv = (TextView) findViewById(R.id.fueltypetv);
        regcitytv = (TextView) findViewById(R.id.regcitytv);
        stock_bought_price_val = (TextView) findViewById(R.id.stock_bought_price_val);
        stock_certification_no_val = (TextView) findViewById(R.id.stock_certification_no_val);
        stock_insurance_val = (TextView) findViewById(R.id.stock_insurance_val);
        stock_insurance_expiry_date = (TextView) findViewById(R.id.stock_insurance_expiry_date);
        stock_cng_kit = (TextView) findViewById(R.id.stock_cng_kit);
        stock_warrantytv = (TextView) findViewById(R.id.stock_warrantytv);
        stock_registration_no_val = (TextView) findViewById(R.id.stock_registration_no_val);
        stock_registration_no = (TextView) findViewById(R.id.stock_registration_no);
        stock_dealer_price_val = (TextView) findViewById(R.id.stock_dealer_price_val);
        stock_cattv = (TextView) findViewById(R.id.stock_cattv);
        buttom_ll = (LinearLayout) findViewById(R.id.buttom_ll);
        //status bar color change

        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {

        } else {
            buttom_ll.setVisibility(View.GONE);

        }

       /* posted_date = getIntent().getStringExtra("posted_date");

        String createdDate = posted_date.toString();
        // Log.e("getPostedDate ", "getPostedDate " + createdDate);
        String[] splitDateTime = createdDate.split(" ", 10);
        String[] splitDate = splitDateTime[0].split("-", 10);
        String finalDate = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");
        Calendar c = Calendar.getInstance();
        String formattedDate = simpleDateFormat.format(c.getTime());
        String[] splitSpace = formattedDate.split(" ", 10);


        try {
            Date date1 = simpleDateFormat.parse(finalDate + " 00:00:00");
            Date date2 = simpleDateFormat.parse(splitSpace[0] + " 00:00:00");

            Log.e("diff ", "diff " + date1 + " " + date2 + "==" + printDifference(date1, date2));

            String day = "" + printDifference(date1, date2);

            if (day.equalsIgnoreCase("0")) {
                days.setText("" + "Today");
            } else if (day.equalsIgnoreCase("1")) {
                days.setText("" + "Yesterday");

            } else {
                days.setText("" + printDifference(date1, date2) + " D ago");

            }

        } catch (ParseException e) {
            e.printStackTrace();
        }



        regno = getIntent().getStringExtra("registraionNo");
        stock_registration_no.setText(regno);
        make = getIntent().getStringExtra("make");
        carName.setText(make);
        variantModel = getIntent().getStringExtra("model");
        variant = getIntent().getStringExtra("variant");
        modelVariant.setText(variantModel + " " + variant);
        selling_price = getIntent().getStringExtra("sellingPrice");
        sellingprice.setText(selling_price);
        owner = getIntent().getStringExtra("owner");
        ownertv.setText(owner);
        Regyear = getIntent().getStringExtra("Regyear");
        reg_month = getIntent().getStringExtra("reg_month");
        stock_registration_no_val.setText(reg_month + " " + Regyear);
        kms = getIntent().getStringExtra("kms");
        kmstv.setText(kms);
        registraionCity = getIntent().getStringExtra("registraionCity");
        regcitytv.setText(registraionCity);
        fuel_type = getIntent().getStringExtra("fuel_type");
        if (fuel_type == null || fuel_type.equalsIgnoreCase("")
                || fuel_type.equalsIgnoreCase("null")) {
            fuel_type = "NA";
        }
        fueltypetv.setText(fuel_type);
        manufacture_month = getIntent().getStringExtra("manufacture_month");
        manufacture_year = getIntent().getStringExtra("manufacture_year");

        yeartv.setText(manufacture_month.substring(0, 3) + " " + manufacture_year);

*/

        CommonMethods.setvalueAgainstKey(activity, "stockdetails", "stockdetails");
        //Volley
        WebServicesCall.webCall(this, this, jsonMake1(), "PermissionDear", GlobalText.GET);

        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(activity, R.color.Black));


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


// Images left navigation
        leftNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewPager.getCurrentItem();

            /*    if (tab > 0) {
                    tab--;
                    viewPager.setCurrentItem(tab);
                } else if (tab == 0) {
                   // viewPager.setCurrentItem(tab);
                }*/

                // stock_img_count.setText(current + "/" + imageLenght);
                Log.e("imageCount", "imageCount=" + (tab));
                Log.e("test image count", current + " ");
                viewPager.arrowScroll(View.FOCUS_LEFT);
            }
        });

        // Images right navigatin
        rightNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewPager.getCurrentItem();

              /*  if (tab <= 0) {
                    tab++;

                    Log.e("test",tab++ + " ");
                    //viewPager.setCurrentItem(tab);
                } else if (tab == 0) {
                   // viewPager.setCurrentItem(tab);
                }*/
                //  stock_img_count.setText(tab + "/" + imageLenght);
                Log.e("imageCount", "imageCount=" + tab);
                viewPager.arrowScroll(View.FOCUS_RIGHT);
                Log.e("test count tab", tab + " ");
            }
        });

        // Images right navigatin
        viewPager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewPager.getCurrentItem();

                Log.e("getCurrentItem", "getCurrentItem=" + tab);

                Log.e("imageurl=", "imageurl=" + imageurl[tab]);

            }
        });

        viewPager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e("onPageSelected", "onPageSelected=" + view.getTransitionName());
            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                Log.e("onPageSelected", "onPageSelected=" + arg0);
                stock_img_count.setText(arg0 + 1 + "/" + imageLenght);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

                Log.e("onPageSelected", "onPageSelected=" + arg0 + "=" + arg1 + "=" + arg2);

            }

            @Override
            public void onPageScrollStateChanged(int num) {
                Log.e("onPageSelected", "onPageSelected=" + num);

            }

        });

/*
        share_Content =
                        "*" + make + " " + variantModel + variant + "*"+  "\n" +
                        "REG.CITY : " + registraionCity + "\n" +
                        "SELLING PRICE : " + getResources().getString(R.string.rs) +  selling_price + "\n" +
                        "FUEL TYPE : " + fuel_type + "\n" +
                        "KMS : " + kms + "\n" +
                        "OWNER : " + owner + "\n"+
        "MANUF. ON : " + manufacture_year + " " + manufacture_month+ "\n" +
        "CERTIFIED : " + isCertifiedWhatsupdata ;


        Log.e("test share content",share_Content);*/
        //call fond method
        updateFontUI(this);


        if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("BookedStock")) {
            bookstockbtn.setText("Unbook");
        } else {
            bookstockbtn.setText("Book Stock");
        }

    }

    public static void setFalse() {
        coverimgFlag = false;
        frontviewFlag = false;
        leftsideviewFlag = false;
        rightsideviewFlag = false;
        rearleftviewFlag = false;
        rearrightviewFlag = false;
        rearviewFlag = false;
        frontrightviewFlag = false;
        wheelstviewFlag = false;
        front_seatsFlag = false;
        back_seatsFlag = false;
        odometerFlag = false;
        dashboardFlag = false;
        steeringFlag = false;
        front_row_sideFlag = false;
        back_row_sideFlag = false;
        trunk_door_open_viewFlag = false;
    }

    public static void white_star() {
        is_featured_car = "false";
        post_on_lab.setText("Mark as featured");
        postOn.setBackgroundResource(R.drawable.star);

    }

    public static void yellow_star() {
        is_featured_car = "true";
        postOn.setBackgroundResource(R.drawable.star_y);
        post_on_lab.setText("Featured");

    }

    public static void imageload(String stock_id, Activity act) {

        progress = new ProgressDialog(activity);
        imageurl = new String[]{};
        imagename = new String[]{};
        setFalse();

        WebServicesCall.webCall(activity, activity, jsonMakestock(), "StockDetails", GlobalText.POST);

    }

    public static void stockdetailspage(JSONObject jObj, String strMethod, Activity activity) {

        try {

            // stock = new ArrayList<>();

            Log.e("StockDetails", jObj.toString());
            JSONArray arrayList = jObj.getJSONArray("data");
            Log.e("arrayList", "arrayList=" + arrayList.length());

            for (int i = 0; i < arrayList.length(); i++) {
                JSONObject jsonobject = new JSONObject(
                        arrayList.getString(i));

                //new ----------------

                posted_date = jsonobject.getString("posted_date");


                String createdDate = posted_date.toString();
                // Log.e("getPostedDate ", "getPostedDate " + createdDate);
                String[] splitDateTime = createdDate.split(" ", 10);
                String[] splitDate = splitDateTime[0].split("-", 10);
                String finalDate = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");
                Calendar c = Calendar.getInstance();
                String formattedDate = simpleDateFormat.format(c.getTime());
                String[] splitSpace = formattedDate.split(" ", 10);


                try {
                    Date date1 = simpleDateFormat.parse(finalDate + " 00:00:00");
                    Date date2 = simpleDateFormat.parse(splitSpace[0] + " 00:00:00");

                    Log.e("diff ", "diff " + date1 + " " + date2 + "==" + printDifference(date1, date2));

                    String day = "" + printDifference(date1, date2);

                    days.setVisibility(View.VISIBLE);
                    if (day.equalsIgnoreCase("0")) {
                        days.setText("" + "Today");
                    } else if (day.equalsIgnoreCase("1")) {
                        days.setText("" + "Yesterday");

                    } else {
                        days.setText("" + printDifference(date1, date2) + " D ago");

                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }


                regno = jsonobject.getString("registration_number");
                stock_registration_no.setText(regno);
                make = jsonobject.getString("vehicle_make");
                carName.setText(make);
                variantModel = jsonobject.getString("vehicle_model");
                variant = jsonobject.getString("vehicle_variant");
                modelVariant.setText(variantModel + " " + variant);
                selling_price = String.valueOf(jsonobject.getInt("selling_price"));
                iv_price_image.setVisibility(View.VISIBLE);
                sellingprice.setText(selling_price);
                owner = String.valueOf(jsonobject.getInt("owner"));
                ownertv.setText(owner);
                Regyear = String.valueOf(jsonobject.getInt("reg_year"));
                reg_month = jsonobject.getString("reg_month");
                stock_registration_no_val.setText(reg_month + " " + Regyear);
                kms = String.valueOf(jsonobject.getInt("kilometer"));
                kmstv.setText(kms);
                registraionCity = jsonobject.getString("registraion_city");
                regcitytv.setText(registraionCity);


                fuel_type = jsonobject.getString("fuel_type");

                video_url = jsonobject.getString("video_url");

                if(video_url.isEmpty()||video_url==null){

                    video_url="";
                }


                if (fuel_type == null || fuel_type.equalsIgnoreCase("")
                        || fuel_type.equalsIgnoreCase("null")) {
                    fuel_type = "NA";
                }
                fueltypetv.setText(fuel_type);

                //------------------------------------

                final JSONArray images = jsonobject.getJSONArray("images");

                ArrayList<String> imageurls;
                imageurls = new ArrayList<String>();
                ArrayList<String> imagenames;

                imagenames = new ArrayList<String>();


                if (images != null && images.length() > 0) {
                    for (int j = 0; j < images.length(); j++) {
                        try {
                            JSONObject data = images.getJSONObject(j);
                            imageurls.add(data.getString("url"));
                            imagenames.add(data.getString("category_identifier"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    imageurls.add("url");
                    imagenames.add("category_identifier");

                }


                Object[] objectList_imagenames = imagenames.toArray();
                imagename = Arrays.copyOf(objectList_imagenames, objectList_imagenames.length, String[].class);


                Object[] objectList = imageurls.toArray();

                imageurl = Arrays.copyOf(objectList, objectList.length, String[].class);

                if (imageurl[0].equalsIgnoreCase("url")) {
                    imageLenght = 0;
                } else {
                    imageLenght = imageurl.length;
                }
                stock_img_count.setVisibility(View.VISIBLE);
                if (imageLenght == 0) {
                    stock_img_count.setText("0" + "/" + imageLenght);
                } else {
                    stock_img_count.setText("1" + "/" + imageLenght);
                }


                CustomPagerAdapter mCustomPagerAdapter = new CustomPagerAdapter(activity, imageurl);
                viewPager.setAdapter(mCustomPagerAdapter);

                if (!jsonobject.getString("cover_image").equalsIgnoreCase("")) {
                    coverimage = jsonobject.getString("cover_image").toString();
                } else {
                    coverimage = imageurl[0];
                }

                imageCount = String.valueOf(jsonobject.getInt("photo_count"));


                certifiedNo = jsonobject.getString("certification_number");
                if (certifiedNo == null || certifiedNo.equalsIgnoreCase("")
                        || certifiedNo.equalsIgnoreCase("null")) {
                    certifiedNo = "NA";
                }

                insurance = jsonobject.getString("insurance");

                if (insurance == null || insurance.equalsIgnoreCase("")
                        || insurance.equalsIgnoreCase("null")) {
                    insurance = "NA";
                }
                insuranceExp_date = jsonobject.getString("insurance_exp_date");

                if (insuranceExp_date == null || insuranceExp_date.equalsIgnoreCase("")
                        || insuranceExp_date.equalsIgnoreCase("null")) {
                    insuranceExp_date = "NA";
                }

                source = jsonobject.getString("stock_source");
                if (source == null || source.equalsIgnoreCase("")
                        || source.equalsIgnoreCase("null")) {
                    source = "NA";
                }

                warranty = jsonobject.getString("warranty_recommended");

                if (warranty == null || warranty.equalsIgnoreCase("")
                        || warranty.equalsIgnoreCase("null")) {
                    warranty = "NA";
                }
                Log.e("warranty", "warranty=" + warranty);


                surveyor_remark = jsonobject.getString("surveyor_remark");

                if (surveyor_remark == null || surveyor_remark.equalsIgnoreCase("")
                        || surveyor_remark.equalsIgnoreCase("null")) {
                    surveyor_remark = "NA";
                }

                photo_count = String.valueOf(jsonobject.getInt("photo_count"));


                Log.e("reg_month", "reg_month->>" + jsonobject.getString("reg_month"));
                Log.e("reg_month", "reg_month->=" + reg_month);


                bought = jsonobject.getString("bought_price");
                refurbishment_cost = jsonobject.getString("refurbishment_cost");
                dealer_price = jsonobject.getString("dealer_price");
                procurement_executive_id = jsonobject.getString("procurement_executive_id");

                if (procurement_executive_id == null || procurement_executive_id.equalsIgnoreCase("")) {
                    procurement_executive_id = "0";
                }

                cng_kit = jsonobject.getString("cng_kit");

                if (cng_kit == null || cng_kit.equalsIgnoreCase("")
                        || cng_kit.equalsIgnoreCase("null")) {
                    cng_kit = "NA";
                }

                chassis_number = jsonobject.getString("chassis_number");
                engine_number = jsonobject.getString("engine_number");
                is_certified = jsonobject.getString("is_certified");
                is_featured_car = jsonobject.getString("is_featured_car");


                if (is_featured_car.equalsIgnoreCase("true") || is_featured_car.equalsIgnoreCase("1")) {
                    postOn.setBackgroundResource(R.drawable.star_y);
                    post_on_lab.setText("Featured");
                } else {
                    postOn.setBackgroundResource(R.drawable.star);
                }
                if (source.equalsIgnoreCase("mfc")) {
                    postOn.setVisibility(View.VISIBLE);
                } else {
                    postOn.setVisibility(View.INVISIBLE);
                }

                is_featured_car_admin = "";
                procurement_executive_name = jsonobject.getString("procurement_executive_name");
                private_vehicle = jsonobject.getString("private_vehicle");

                comments = jsonobject.getString("comments");

                if (comments == null || comments.equalsIgnoreCase("")
                        || comments.equalsIgnoreCase("null")) {
                    stock_comments.setText("NA");
                } else {
                    stock_comments.setText(comments);

                }

                finance_required = jsonobject.getString("finance_required");
                manufacture_month = jsonobject.getString("manufacture_month");
                sales_executive_id = jsonobject.getString("sales_executive_id");
                sales_executive_name = jsonobject.getString("sales_executive_name");
                manufacture_year = jsonobject.getString("manufacture_year");
                actual_selling_price = jsonobject.getString("actual_selling_price");
                CertifiedText = jsonobject.getString("CertifiedText");
                IsDisplay = "";
                TransmissionType = "";
                colour = jsonobject.getString("colour");
                dealer_code = jsonobject.getString("dealer_code");
                is_offload = jsonobject.getString("is_offload");

                stock_id = String.valueOf(jsonobject.getInt("stock_id"));

                CommonMethods.setvalueAgainstKey(activity, "stock_id", stock_id);
                is_booked = jsonobject.getString("is_booked");
                is_sold = jsonobject.getString("is_sold");

                setDeleteButtonStatus();

                //reena
                if (is_certified == null) {
                    stock_Certified_lbl.setVisibility(View.GONE);
                    isCertifiedWhatsupdata = "NO";
                } else {
                    if (is_certified.equals("1") || is_certified.equals("true")) {
                        stock_Certified_lbl.setText("Certified");
                        stock_Certified_lbl.setVisibility(View.VISIBLE);
                        isCertifiedWhatsupdata = "YES";
                    } else {
                        stock_Certified_lbl.setVisibility(View.GONE);
                        isCertifiedWhatsupdata = "NO";
                    }
                }

                if (chassis_number == null || chassis_number.equalsIgnoreCase("")
                        || chassis_number.equalsIgnoreCase("null")) {
                    chassis_number = "NA";

                }
                if (engine_number == null || engine_number.equalsIgnoreCase("")
                        || engine_number.equalsIgnoreCase("null")) {
                    engine_number = "NA";

                }

                stock_chassis_number.setText(chassis_number);
                stock_enginenotv.setText(engine_number);

                if (procurement_executive_name == null || procurement_executive_name.equalsIgnoreCase("")) {
                    procurement_executive_name = "";
                }
                stock_procurement_executive.setText(procurement_executive_name);

                if (refurbishment_cost == null || refurbishment_cost.equalsIgnoreCase("")) {
                    refurbishment_cost = "0";
                }

                stock_refurbishment_cost_val.setText(refurbishment_cost);
                yeartv.setText(manufacture_month.substring(0, 3) + " " + manufacture_year);

                if (bought == null || bought.equalsIgnoreCase("")) {
                    bought = "0";
                }
                stock_bought_price_val.setText(bought);

                stock_certification_no_val.setText(certifiedNo);
                stock_insurance_val.setText(insurance);


                if (insuranceExp_date == null || insuranceExp_date.equalsIgnoreCase("")
                        || insuranceExp_date.equalsIgnoreCase("null")) {
                    stock_insurance_expiry_date.setText("NA");

                } else {

                    if (insuranceExp_date.length() < 10) {
                        stock_insurance_expiry_date.setText(insuranceExp_date);

                    } else {

                        //reena
                        String insuranceExp_date1 = insuranceExp_date.substring(0, 10);
                        String CurrentString = insuranceExp_date1;
                        String[] separated = CurrentString.split("-");
                        String s1 = separated[0];
                        String s2 = separated[1];
                        String s3 = separated[2];

                        Log.e("test split", s1 + " " + s2 + " " + s3);

                        String insuranceExp_date = s3 + " " + MonthUtility.Month[Integer.parseInt(s2)] + " " + s1;

                        stock_insurance_expiry_date.setText(insuranceExp_date);
                    }
                }

                stock_cng_kit.setText(cng_kit);
                stock_source.setText(source);

                if (source.equalsIgnoreCase("mfc") || source.equalsIgnoreCase("p&s")) {
                    item = menus.findItem(R.id.add_image);
                    item.setVisible(true);

                    if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
                        item = menus.findItem(R.id.add_video);
                        item.setVisible(true);
                    }

                } else {
                    item = menus.findItem(R.id.add_image);
                    item.setVisible(false);
                }

                if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {

                } else {
                    item = menus.findItem(R.id.add_image);
                    item.setVisible(false);

                    item = menus.findItem(R.id.share);
                    item.setVisible(true);

                    item = menus.findItem(R.id.asmHome);
                    item.setVisible(false);

                    item = menus.findItem(R.id.asm_mail);
                    item.setVisible(false);
                }

                stock_warrantytv.setText(warranty);
                //stock_img_count.setText(photo_count + "/20");


                if (dealer_price == null || dealer_price.equalsIgnoreCase("")) {
                    dealer_price = "0";
                }

                stock_dealer_price_val.setText(dealer_price);

                if (is_offload.equalsIgnoreCase("0") || is_offload.equalsIgnoreCase("false")) {
                    stock_cattv.setText("Retail");
                } else {
                    stock_cattv.setText("OffLoad Vehicle");
                }
                if (private_vehicle.equalsIgnoreCase("1") || private_vehicle.equalsIgnoreCase("true")) {
                    stock_vehicletype.setText("Private");
                    post_on_lab.setVisibility(View.VISIBLE);

                } else {
                    post_on_lab.setVisibility(View.INVISIBLE);
                    stock_vehicletype.setText("Commercial");
                }


                if (CommonMethods.getstringvaluefromkey(activity, "featured_car").equalsIgnoreCase("true")
                        && stock_vehicletype.getText().toString().equalsIgnoreCase("Private")
                        && stock_source.getText().toString().equalsIgnoreCase("mfc")
                        && is_offload.equalsIgnoreCase("false")
                ) {
                    //postOnLab.setVisibility(View.VISIBLE);
                    post_on_lab.setVisibility(View.VISIBLE);
                    postOn.setVisibility(View.VISIBLE);
                    //Log.e("test feated",CommonMethods.getstringvaluefromkey(activity, "featured_car"));
                } else if (CommonMethods.getstringvaluefromkey(activity, "featured_car").equalsIgnoreCase("true")
                        && stock_vehicletype.getText().toString().equalsIgnoreCase("Private")
                        && stock_source.getText().toString().equalsIgnoreCase("p&s")
                        && is_offload.equalsIgnoreCase("false")) {
                    post_on_lab.setVisibility(View.VISIBLE);
                    postOn.setVisibility(View.VISIBLE);
                } else {
                    postOn.setVisibility(View.INVISIBLE);
                    post_on_lab.setVisibility(View.INVISIBLE);
                    // Log.e("test featewrwewrred",CommonMethods.getstringvaluefromkey(activity, "featured_car"));
                }


                String CERTIFIED = "", ADDRESS = "", URL = "";
                Double dealer_latitude = null, dealer_longitude = null;
                String newline = "\n";
                //val
                ADDRESS = CommonMethods.getstringvaluefromkey(activity, "dealer_address");

                try {
                    dealer_latitude = Double.parseDouble(CommonMethods.getstringvaluefromkey(activity, "dealer_latitude").toString());
                    dealer_longitude = Double.parseDouble(CommonMethods.getstringvaluefromkey(activity, "dealer_longitude").toString());
                } catch (Exception e) {

                }

                if (dealer_latitude != null
                        && dealer_longitude != null
                        && !dealer_latitude.isNaN()
                        && !dealer_longitude.isNaN()
                ) {

                    URL = "http://maps.google.com/maps?q=" + String.format("%f,%f", dealer_latitude, dealer_longitude);
                }

                if (is_certified.equals("1") || is_certified.equals("true")) {
                    CERTIFIED = "_Certified by Mahindra First Choice_";
                } else {
                    CERTIFIED = "";
                    newline = "";
                }

                share_Content =
                        "*" + make + " " + variantModel + " " + variant + "*" + "\n" +
                                "REG.CITY : " + registraionCity + "\n" +
                                "SELLING PRICE : " + activity.getResources().getString(R.string.rs) + selling_price + "\n" +
                                "FUEL TYPE : " + fuel_type + "\n" +
                                "KMS : " + kms + "\n" +
                                "OWNER : " + owner + "\n" +
                                "MFG. DATE : " + manufacture_month + " " + manufacture_year + newline +
                                CERTIFIED + "\n" +
                                "AVAILABLE AT : " + ADDRESS + "\n" +
                                URL;

                Log.e("test share content", share_Content);

            }


        } catch (Exception ex) {
            Log.e("Parsestockstore-Ex", ex.toString());
        }


    }


    public static void PermissionDear(JSONObject jObj, String strMethod, Activity activity) {


        try {

            JSONObject jsonObject = new JSONObject(jObj.getJSONObject("permission").toString());

            if (jsonObject.getBoolean("featured_car")) {
                CommonMethods.setvalueAgainstKey(activity, "featured_car", "true");

            } else {
                CommonMethods.setvalueAgainstKey(activity, "featured_car", "false");

            }

            if (CommonMethods.getstringvaluefromkey(activity, "featured_car").equalsIgnoreCase("true")
                    && stock_vehicletype.getText().toString().equalsIgnoreCase("Private")
                    && stock_source.getText().toString().equalsIgnoreCase("mfc")
                    && is_offload.toString().equalsIgnoreCase("false")
            ) {
                //postOnLab.setVisibility(View.VISIBLE);
                post_on_lab.setVisibility(View.VISIBLE);
                postOn.setVisibility(View.VISIBLE);
                //Log.e("test feated",CommonMethods.getstringvaluefromkey(activity, "featured_car"));
            } else if (CommonMethods.getstringvaluefromkey(activity, "featured_car").equalsIgnoreCase("true")
                    && stock_vehicletype.getText().toString().equalsIgnoreCase("Private")
                    && stock_source.getText().toString().equalsIgnoreCase("p&s")
                    && is_offload.toString().equalsIgnoreCase("false")
            ) {
                post_on_lab.setVisibility(View.VISIBLE);
                postOn.setVisibility(View.VISIBLE);
            } else {
                postOn.setVisibility(View.INVISIBLE);
                post_on_lab.setVisibility(View.INVISIBLE);
                // Log.e("test featewrwewrred",CommonMethods.getstringvaluefromkey(activity, "featured_car"));
            }




            //Log.e("test json",jsonObject.getString("featured_car"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        try {

            if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
                CommonMethods.setvalueAgainstKey(activity, "dealer_code", jObj.getString("dealer_code"));
            } else {
            }


        } catch (JSONException e) {
            Log.e("test featured", e.getMessage());
        }

        WebServicesCall.webCall(activity, activity, jsonMakestock(), "StockDetails", GlobalText.POST);

    }


    public static JSONObject jsonMakestock() {
        JSONObject jObj = new JSONObject();
        JSONObject jObj1 = new JSONObject();
        JSONObject jObj2 = new JSONObject();

        JSONObject jObjvalues = new JSONObject();
        JSONObject code = new JSONObject();


        JSONArray jsonArray = new JSONArray();
        JSONArray jsonArrayValues = new JSONArray();
        JSONArray jObjdealer = new JSONArray();

        try {

            jObj.put("Page", "1");
            jObj.put("PageItems", "10");

            jObj1.put("column", "stock_id");
            jObj1.put("operator", "=");
            jObj1.put("value", CommonMethods.getstringvaluefromkey(activity, "stock_id"));

            if (Stocklisting.getInstance().getStockstatus().equals("BookedStockFrag")) {
                jObj2.put("column", "is_booked");
                jObj2.put("operator", "=");
                jObj2.put("value", "true");
                jsonArray.put(jObj2);
            }

            if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {

            } else {

                jObjdealer.put(CommonMethods.getstringvaluefromkey(activity, "dealer_code"));

                jObjvalues.put("column", "dealer_code");
                jObjvalues.put("operator", "=");
                jObjvalues.put("values", jObjdealer);

                jsonArrayValues.put(jObjvalues);

                jObj.put("wherein", jsonArrayValues);

            }


            jsonArray.put(jObj1);


            jObj.put("where", jsonArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("jsonMake1 ", "jsonMake1" + jObj.toString());

        return jObj;
    }

    public static void postions(int pos) {

        stock_img_count.setText(pos + "/" + imageLenght);

    }


    public static JSONObject jsonMake1() {
        JSONObject jObj = new JSONObject();


        Log.e("jsonMake1 ", "jsonMake1" + jObj.toString());

        return jObj;
    }


    @OnClick(R.id.lldeletebtn)
    public void deleteStock() {
        Log.i(TAG, "deleteStock: ");
        Dialog popDialog = new Dialog(this);
        popDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        popDialog.setContentView(R.layout.common_cus_dialog);
        TextView message1 = popDialog.findViewById(R.id.Message);
        TextView message2 = popDialog.findViewById(R.id.Message2);
        message1.setText("Are you sure you want to");
        message2.setText("Delete?");
        Button cancel = popDialog.findViewById(R.id.cancel);
        Button confirm = popDialog.findViewById(R.id.Confirm);
        cancel.setOnClickListener(v -> {
            popDialog.cancel();
        });
        confirm.setOnClickListener(v -> {
            popDialog.cancel();
            StockFragment.onBack = true;
            WebServicesCall.webCall(activity, activity, getStockId(), "ParkAndSellDelete", GlobalText.POST);
        });
        popDialog.show();
    }

    public JSONObject getStockId() {
        JSONObject jObj = new JSONObject();
        try {
            jObj.put("stock_id", stock_id);
        } catch (Exception e) {
            Log.i(TAG, "getStokId: ");
        }
        return jObj;
    }

    @OnClick(R.id.bookstockbtn_lin)
    public void bookstockbtn_lin(View view) {

        if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("BookedStock")
                || bookstockbtn.getText().toString().equalsIgnoreCase("Unbook")) {

           Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.book_now_stock, GlobalText.android);

            UnbookeAert();

        } else {

            Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.book_stocks, GlobalText.android);

            exitPopUp();
        }

    }

    public void UnbookeAert() {

        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.common_cus_dialog);
        //dialog.setTitle("Custom Dialog");

        TextView Message, Message2;
        Button cancel, Confirm;
        Message = dialog.findViewById(R.id.Message);
        Message2 = dialog.findViewById(R.id.Message2);
        cancel = dialog.findViewById(R.id.cancel);
        Confirm = dialog.findViewById(R.id.Confirm);

        if (bookstockbtn.getText().toString().equalsIgnoreCase("Unbook")) {
            Message.setText("Are you sure you want to ");
            Message2.setText("un-book this stock?");
        } else {
            Message.setText("Are you sure you want to ");
            Message2.setText("book this stock?");
        }

        Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                WebServicesCall.webCall(activity, activity, jsonMakeBookNow(), "BoookNow", GlobalText.PUT);

                dialog.dismiss();
            }

        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }


    @OnClick(R.id.editstockbtn_lin)
    public void editstockbtn_lin(View view) {

        StockFragment.onBack = true;
        Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.edit_stocks, GlobalText.android);

        try {
            Intent intent = new Intent(StockStoreInfoActivity.this, EditStockActivity.class);

            intent.putExtra("make", make);
            intent.putExtra("model", variantModel);
            intent.putExtra("variant", variant);
            intent.putExtra("registraionCity", registraionCity);
            intent.putExtra("registraionNo", regno);
            intent.putExtra("reg_year", Regyear);

            intent.putExtra("color", colour);
            intent.putExtra("certified", is_certified);
            intent.putExtra("owner", owner);
            intent.putExtra("kms", kms);
            intent.putExtra("certifiedNo", certifiedNo);
            intent.putExtra("sellingPrice", selling_price);
            intent.putExtra("yearMonth", manufacture_month);
            intent.putExtra("insurance", insurance);
            intent.putExtra("insuranceExp_date", insuranceExp_date);
            intent.putExtra("selling_price", selling_price);
            intent.putExtra("source", source);
            intent.putExtra("warranty", warranty);
            intent.putExtra("surveyor_remark", surveyor_remark);
            intent.putExtra("photo_count", photo_count);
            intent.putExtra("reg_month", reg_month);
            intent.putExtra("fuel_type", fuel_type);
            intent.putExtra("bought", bought);
            intent.putExtra("refurbishment_cost", refurbishment_cost);
            intent.putExtra("dealer_price", dealer_price);
            intent.putExtra("ProcurementExecId", procurement_executive_id);
            intent.putExtra("cng_kit", cng_kit);
            intent.putExtra("chassis_number", chassis_number);
            intent.putExtra("engine_number", engine_number);

            intent.putExtra("is_certified", is_certified);
            intent.putExtra("is_featured_car", is_featured_car);
            intent.putExtra("is_featured_car_admin", is_featured_car_admin);
            intent.putExtra("procurement_executive_name", procurement_executive_name);
            intent.putExtra("private_vehicle", private_vehicle);
            intent.putExtra("comments", comments);
            intent.putExtra("finance_required", finance_required);
            intent.putExtra("manufacture_month", manufacture_month);
            intent.putExtra("sales_executive_id", sales_executive_id);
            intent.putExtra("sales_executive_name", sales_executive_name);
            intent.putExtra("manufacture_year", manufacture_year);
            intent.putExtra("actual_selling_price", actual_selling_price);
            intent.putExtra("CertifiedText", CertifiedText);
            //intent.putExtra("IsDisplay",IsDisplay);
            intent.putExtra("TransmissionType", TransmissionType);
            intent.putExtra("colour", colour);
            intent.putExtra("dealer_code", dealer_code);
            intent.putExtra("is_offload", is_offload);
            intent.putExtra("posted_date", posted_date);
            intent.putExtra("stock_id", stock_id);
            intent.putExtra("is_booked", is_booked);

            intent.putExtra("imageurl", imageurl);
            intent.putExtra("imagename", imagename);
            intent.putExtra("coverimage", coverimage);

            startActivity(intent);
            finish();
        } catch (NullPointerException err) {

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        menus = menu;
        getMenuInflater().inflate(R.menu.menu_item, menu);

        item = menu.findItem(R.id.share);
        item.setVisible(true);

        item = menu.findItem(R.id.asmHome);
        item.setVisible(false);

        item = menu.findItem(R.id.asm_mail);
        item.setVisible(false);

        /*if(source.equalsIgnoreCase("mfc")) {
            item = menu.findItem(R.id.add_image);
            item.setVisible(true);
        }else{
            item = menu.findItem(R.id.add_image);
            item.setVisible(false);
        }*/

        item = menu.findItem(R.id.delete_fea);
        item.setVisible(false);
        item = menu.findItem(R.id.stock_fil_clear);
        item.setVisible(false);
        item = menu.findItem(R.id.notification_bell);
        item.setVisible(false);
        item = menu.findItem(R.id.notification_cancel);
        item.setVisible(false);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();  // optional depending on your needs
        Log.e("boolean", StockFragment.onBack + "");
        Log.e("String", StockFragment.stock + "");
        if (StockFragment.stock.equals("Store_Stocks") && StockFragment.onBack) {
            StockStoreFrag.swipeRefresh_s_applyapply();
            finish();
        }
        if (StockFragment.stock.equals("Booked_Stocks") && StockFragment.onBack) {
            BookedStockFrag.swipeRefreshapply();
            finish();
        } else {
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:

                /*if(CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("StockStore")) {
                    StockStoreFrag.sortbyapply();
                    finish();
                }else{
                    BookedStockFrag.sortbyapply();
                    finish();
                }*/
               /* Intent intents = new Intent(StockStoreInfoActivity.this, MainActivity.class);
                startActivity(intents);*/

               /* Log.e("boolean", StockFragment.onBack + "");
                Log.e("String", StockFragment.stock + "");*/

                if (StockFragment.stock.equals("Store_Stocks") && StockFragment.onBack) {
                    StockStoreFrag.swipeRefresh_s_applyapply();
                    finish();
                }
                if (StockFragment.stock.equals("Booked_Stocks") && StockFragment.onBack) {
                    BookedStockFrag.swipeRefreshapply();
                    finish();
                } else {
                    finish();
                }

                return true;

            case R.id.add_image:

                StockFragment.onBack = true;

                CommonMethods.setvalueAgainstKey(activity, "stocklist", "true");
                CommonMethods.setvalueAgainstKey(activity, "imagestatus", "true");

                CommonMethods.setvalueAgainstKey(activity, "fromImage", "yes");

                CommonMethods.MemoryClears();
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.add_image, GlobalText.android);

                Intent intent = new Intent(StockStoreInfoActivity.this, ImageUploadActivity1.class);

                ImageLibraryInstance.getInstance().setUploadstatus("detailstatus");

                intent.putExtra("imagename", imagename);
                intent.putExtra("imageurl", imageurl);
                intent.putExtra("stock_id", stock_id);
                intent.putExtra("regno", regno);

                CommonMethods.setvalueAgainstKey(activity, "video_url", video_url);

                intent.putExtra("video_url", video_url);

                //  Log.e("Booknow", imageCount + "");

                if (imageCount.equalsIgnoreCase("-1")
                        || imageCount.equalsIgnoreCase("")
                        || imageCount.equalsIgnoreCase("null")
                        || imageCount == null) {
                    imageCount = "0";
                }

                intent.putExtra("imageCount", imageCount);

                /*Log.e("stockimage ", "imagename " + imagename);
                Log.e("stockimage ", "imageurl " + imageurl);
                Log.e("stockimage ", "stock_id " + stock_id);*/
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                //finish();

                return true;

            case R.id.share:

                //shareImageWhatsApp();
                try {
                    listpopupWhatup();
                } catch (Exception e) {

                }

                return true;

            case R.id.add_video:

                try {

                    Intent intents=new Intent(StockStoreInfoActivity.this, VideoActivity.class);
                    intents.putExtra("stock_id", stock_id);
                    intents.putExtra("video_url", video_url);
                    intents.putExtra("regno", regno);
                    CommonMethods.setvalueAgainstKey(activity, "fromImage", "no");
                    Log.i(TAG, "regno==: "+regno);

                    startActivity(intents);


                } catch (Exception e) {

                }

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public void multipleImageshare_wait() {

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                if (!progress.isShowing()) {
                    progress.setTitle("Loading...");
                    progress.setMessage("Please Wait...");
                    progress.setCancelable(false);
                    progress.show();
                }
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(final Void... params) {
                // something you know that will take a few seconds

                try {

                    //  Thread.sleep(2000);

                    Log.e("Thread", "Thread-1");
                    if (!coverimgFlag && !frontviewFlag && !leftsideviewFlag && !rightsideviewFlag && !rearleftviewFlag
                            && !rearrightviewFlag && !rearviewFlag && !frontrightviewFlag && !wheelstviewFlag
                            && !front_seatsFlag && !back_seatsFlag && !odometerFlag && !dashboardFlag
                            && !steeringFlag && !front_row_sideFlag && !back_row_sideFlag && !trunk_door_open_viewFlag) {

                        // progress.dismiss();
                        multipleImageshare();

                    } else {
                        multipleImageshare();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    progress.dismiss();
                }
                return null;
            }

            @Override
            protected void onPostExecute(final Void result) {

                if (progress.isShowing()) {
                    progress.dismiss();
                }

                try {
                    Intent intents = new Intent();
                    intents.setAction(Intent.ACTION_SEND_MULTIPLE);
                    intents.setType("image/jpeg");
                    // intents.putExtra(Intent.EXTRA_TEXT,share_Content);
                    intents.setPackage("com.whatsapp");

                    intents.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imageUriArray);
                    startActivity(intents);
                } catch (Exception e) {
                    if (progress.isShowing()) {
                        progress.dismiss();
                    }
                }

            }
        }.execute();
    }


    public void multipleImageshare() {

        imageUriArray = new ArrayList<Uri>();

        for (int i = 0; i < imagename.length; i++) {
            Bitmap mBitmap = null;
            try {
                mBitmap = getBitmapFromUrl(imageurl[i]);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                mBitmap.compress(Bitmap.CompressFormat.JPEG, 70, bytes);
                File f = new File(Environment.getExternalStorageDirectory() + File.separator + imagename[i] + ".png");
                try {
                    // deleteImageFile(f);
                    f.createNewFile();
                    new FileOutputStream(f).write(bytes.toByteArray());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Uri uri1 = Uri.parse(Environment.getExternalStorageDirectory() + File.separator + imagename[i] + ".png");
                imageUriArray.add(uri1);




            } catch (Exception e) {
                e.printStackTrace();
            }
        }



       for (int i  =0;i<imageUriArray.size();i++){
           String uri = imageUriArray.get(i).toString();
           if(uri.contains("back_row_side.png") || uri.contains("back_seats.png")|| uri.contains("cover_image.png")|| uri.contains("dashboard.png")){
               imageUriArray.add(imageUriArray.size(),imageUriArray.get(i));
               imageUriArray.remove(i);
           }
       }

        for (int i  =0;i<imageUriArray.size();i++){
            String uri = imageUriArray.get(i).toString();
            if(uri.contains("back_row_side.png") || uri.contains("back_seats.png")|| uri.contains("cover_image.png")|| uri.contains("dashboard.png")){
                imageUriArray.add(imageUriArray.size(),imageUriArray.get(i));
                imageUriArray.remove(i);
            }
        }

        for (int i  =0;i<imageUriArray.size();i++){
            String uri = imageUriArray.get(i).toString();
            if(uri.contains("dashboard.png")){
                imageUriArray.add(imageUriArray.size(),imageUriArray.get(i));
                imageUriArray.remove(i);
            }
        }
    }

    private void deleteImageFile(File fdelete) {  // this function is to delete the image file //

        if (fdelete.exists()) {
            if (fdelete.delete()) {
                Log.e(" File is delete -->", fdelete.toString());
            } else {
                Log.e(" File is not delete -->", fdelete.toString());
            }
        }

    }


    public void listpopupWhatup() {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Share");
        String[] animals;
        // add a list
        if (photo_count.equalsIgnoreCase("0")
                || photo_count.equalsIgnoreCase("-1")
                || photo_count == null
                || photo_count.equalsIgnoreCase("null")) {
            animals = new String[]{"Share Details"};

        } else {
            animals = new String[]{"Share Images", "Share Details"};

        }
        builder.setItems(animals, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: // horse

                        Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.share_image, GlobalText.android);

                        if (photo_count.equalsIgnoreCase("0")
                                || photo_count.equalsIgnoreCase("-1")
                                || photo_count == null
                                || photo_count.equalsIgnoreCase("null")) {

                            shareImageWhatsApp();
                        } else {

                            //imageload();
                            multipleImageshare_wait();

                        }


                        break;
                    case 1: // cow
                        Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.share_details, GlobalText.android);

                        shareImageWhatsApp();
                        //multipleImageshare();
                        break;
                }
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();

        dialog.getWindow().setGravity(Gravity.CENTER | Gravity.LEFT);

        dialog.show();
    }


    public static Bitmap getBitmapFromUrl(String urls) {
        Bitmap bitmap = null;

        try {
            URL url = new URL(urls);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream inputStream1 = connection.getInputStream();

            try {
                bitmap = BitmapFactory.decodeStream(inputStream1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            coverimgFlag = false;

            return bitmap;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;

    }


    public void shareImageWhatsApp() {

        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");

        Log.e("test share content", share_Content);

        share.putExtra(Intent.EXTRA_TEXT, share_Content);
        //sharingIntent.setType("*/*")

        startActivity(Intent.createChooser(share, "Share Details"));
      /*  share.setPackage("com.whatsapp");

        if (isPackageInstalled("com.whatsapp", this)) {
            share.setPackage("com.whatsapp");
            startActivity(Intent.createChooser(share, "Share Details"));

        } else {

            Toast.makeText(getApplicationContext(), "Please Install Whatsapp", Toast.LENGTH_LONG).show();
        }*/

    }

    private boolean isPackageInstalled(String packagename, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    @OnClick(R.id.post_on)
    public void postOn() {


        if(isNotAllowed())
            return;

        Intent intent = new Intent(StockStoreInfoActivity.this, StockFeaturedCarActivity.class);
        intent.putExtra("make", make);
        intent.putExtra("model", variantModel);
        intent.putExtra("variant", variant);
        intent.putExtra("registraionCity", registraionCity);
        intent.putExtra("registraionNo", regno);
        intent.putExtra("color", colour);
        intent.putExtra("certified", is_certified);
        intent.putExtra("owner", owner);
        intent.putExtra("kms", kms);
        intent.putExtra("Regyear", Regyear);
        intent.putExtra("certifiedNo", certifiedNo);
        intent.putExtra("sellingPrice", selling_price);
        intent.putExtra("yearMonth", manufacture_month);
        intent.putExtra("insurance", insurance);
        intent.putExtra("insuranceExp_date", insuranceExp_date);
        intent.putExtra("selling_price", selling_price);
        intent.putExtra("source", source);
        intent.putExtra("warranty", warranty);
        intent.putExtra("surveyor_remark", surveyor_remark);
        intent.putExtra("photo_count", photo_count);
        intent.putExtra("reg_month", reg_month);
        intent.putExtra("fuel_type", fuel_type);
        intent.putExtra("bought", bought);
        intent.putExtra("refurbishment_cost", refurbishment_cost);
        intent.putExtra("dealer_price", dealer_price);
        intent.putExtra("ProcurementExecId", procurement_executive_id);
        intent.putExtra("cng_kit", cng_kit);
        intent.putExtra("chassis_number", chassis_number);
        intent.putExtra("engine_number", engine_number);

        intent.putExtra("is_certified", is_certified);
        intent.putExtra("is_featured_car", is_featured_car);
        intent.putExtra("is_featured_car_admin", is_featured_car_admin);
        intent.putExtra("procurement_executive_name", procurement_executive_name);
        intent.putExtra("private_vehicle", private_vehicle);
        intent.putExtra("comments", comments);
        intent.putExtra("finance_required", finance_required);
        intent.putExtra("manufacture_month", manufacture_month);
        intent.putExtra("sales_executive_id", sales_executive_id);
        intent.putExtra("sales_executive_name", sales_executive_name);
        intent.putExtra("manufacture_year", manufacture_year);
        intent.putExtra("actual_selling_price", actual_selling_price);
        intent.putExtra("CertifiedText", CertifiedText);
        //intent.putExtra("IsDisplay",IsDisplay);
        intent.putExtra("TransmissionType", TransmissionType);
        intent.putExtra("colour", colour);
        intent.putExtra("dealer_code", dealer_code);
        intent.putExtra("is_offload", is_offload);
        intent.putExtra("posted_date", posted_date);
        intent.putExtra("stock_id", stock_id);
        intent.putExtra("is_booked", is_booked);
        intent.putExtra("imageurl", imageurl);
        intent.putExtra("imagename", imagename);
        intent.putExtra("coverimage", coverimage);

        startActivity(intent);
        //finish();


    }

    public boolean financereqFlag = false;
    public TextView saleexetv;
    public TextView storestocktv;
    EditText sellingpricetv;
    public CheckBox financereqtv;
    public static Dialog dialog2;

    public void exitPopUp() {

        dialog2 = new Dialog(activity, R.style.full_screen_dialog);

        dialog2.setContentView(R.layout.booknowpop);
        dialog2.setCancelable(false);
        String star = "<font color='#B40404'>*</font>";
        Button booknowbtn = dialog2.findViewById(R.id.booknowbtn);

        TextView storestocklbl = dialog2.findViewById(R.id.storestocklbl);
        TextView saleexelbl = dialog2.findViewById(R.id.saleexelbl);
        TextView financereqlbl = dialog2.findViewById(R.id.financereqlbl);
        TextView sellingpricelbl = dialog2.findViewById(R.id.sellingpricelbl);
        sellingpricetv = dialog2.findViewById(R.id.sellingpricetv);
        storestocktv = dialog2.findViewById(R.id.storestocktv);
        saleexetv = dialog2.findViewById(R.id.saleexetv);
        financereqtv = dialog2.findViewById(R.id.financereqtv);
        Button addleadcancel = dialog2.findViewById(R.id.addleadcancel);


        storestocktv.setText(regno);
        sellingpricetv.setText(selling_price);

        // updateFontUI(activity);

        storestocklbl.setText(Html.fromHtml("STORE STOCK "/*+star*/));
        storestocklbl.setVisibility(View.GONE);

        saleexelbl.setText(Html.fromHtml("SALES EXECUTIVE " + star));
        sellingpricelbl.setText(Html.fromHtml("ACTUAL SELLING PRICE " + star));
        financereqlbl.setText(Html.fromHtml("FINANCE REQURIED "));


        CommonMethods.setvalueAgainstKey(activity, "booknowbtn", "false");

        Window window = dialog2.getWindow();
        activity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        //dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.show();

        saleexetv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GetMethodSalesExecutiveLoadData();
            }
        });


        booknowbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                financereqFlag = financereqtv.isChecked();

                if (!saleexetv.getText().toString().equals("")
                        && !sellingpricetv.getText().toString().equals("")) {

                    WebServicesCall.webCall(activity, activity, jsonMakeBookNow(), "BoookNow", GlobalText.PUT);

                } else {
                    Toast.makeText(getApplicationContext(), "Please fill missing values", Toast.LENGTH_LONG).show();

                }

                //
            }
        });

        addleadcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });


    }

    private void GetMethodSalesExecutiveLoadData() {

        if (salesexecIdList != null) {
            salesexecIdList.clear();
        }

        if (salesexecList != null) {
            salesexecList.clear();
        }

        ProgressDialogs();

        JsonArrayRequest strReq = new JsonArrayRequest(Request.Method.GET, Global.stock_dealerURL + "dealer/active-executive-list/sales", null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                //   Log.e("Sales list", response.toString());

                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject data = response.getJSONObject(i);
                        salesexecIdList.add(data.getString("Value"));
                        salesexecList.add(data.getString("Text"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                ProgressDialogsdismiss();
                MethodsalesexecIdListPopup(salesexecIdList, salesexecList);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json; charset=utf-8");
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));
                Log.e("token ", "requestMethods " + CommonMethods.getstringvaluefromkey(activity, "token").toString());

                return headers;
            }
        };
        Application.getInstance().addToRequestQueue(strReq);


    }

    public void MethodsalesexecIdListPopup(final ArrayList salesexecIdList, final ArrayList salesexecList) {


        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        TextView alertdialog_edittext = (EditText) dialog_data.findViewById(R.id.alertdialog_edittext);
        searchtittle.setText("Sales Executive List");
        alertdialog_edittext.setHint("Search Sales Executive");


        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);


        //reena for back button
        ImageView dialog_back_btn = dialog_data.findViewById(R.id.backicon);
        dialog_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_data.dismiss();
                //  Log.e("test error", "resee");
                //  dialog_data.cancel();
            }
        });
        //end
        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    //   dialog_data.cancel();
                }

            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, salesexecList);
        alertdialog_Listview.setAdapter(adapter);
        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                sales_executive_name = a.getAdapter().getItem(position).toString();

                sales_executive_id = salesexecIdList.get(position).toString();

                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                saleexetv.setText(sales_executive_name);

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    // dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });


        dialog_data.show();
    }

    public JSONObject jsonMakeBookNow() {
        JSONObject jObj = new JSONObject();

        try {

            jObj.put("stock_id", stock_id);
            jObj.put("stock_source", source);
            jObj.put("posted_date", posted_date);
            jObj.put("vehicle_make", make);
            jObj.put("vehicle_model", variantModel);
            jObj.put("vehicle_variant", variant);

            //    Log.e("reg_month==", "reg_month===" + reg_month);

            jObj.put("reg_month", MethodofMonth_Num(reg_month));

            jObj.put("reg_year", Regyear);
            jObj.put("registraion_city", registraionCity);
            jObj.put("registration_number", regno);

            jObj.put("colour", colour);
            jObj.put("kilometer", kms);
            jObj.put("owner", owner);
            jObj.put("insurance", insurance);

            try {
                if (insurance.equalsIgnoreCase("NA")) {
                    jObj.put("insurance_exp_date", "");
                } else {
                    jObj.put("insurance_exp_date", insuranceExp_date);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            jObj.put("selling_price", selling_price);
            jObj.put("is_display", is_display);
            jObj.put("bought_price", bought);
            jObj.put("refurbishment_cost", refurbishment_cost);
            jObj.put("procurement_executive_id", procurement_executive_id);
            jObj.put("cng_kit", cng_kit);
            jObj.put("private_vehicle", private_vehicle);
            jObj.put("comments", comments);
            jObj.put("manufacture_month", MethodofMonth_Num(manufacture_month));
            jObj.put("is_offload", is_offload);
            jObj.put("manufacture_year", manufacture_year);
            jObj.put("updated_by_device", "MOBILE");

            if (is_featured_car.equalsIgnoreCase("true")) {
                jObj.put("is_featured_car", "false");

            } else {
                jObj.put("is_featured_car", is_featured_car);

            }

            if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("BookedStock")) {

                if (bookstockbtn.getText().toString().equalsIgnoreCase("Unbook")) {
                    jObj.put("is_booked", "false");
                    jObj.put("actual_selling_price", selling_price.toString());
                    jObj.put("sales_executive_id", sales_executive_id);
                    jObj.put("finance_required", "false");
                } else {
                    jObj.put("is_booked", "true");
                    jObj.put("actual_selling_price", selling_price.toString());
                    jObj.put("sales_executive_id", sales_executive_id);
                    jObj.put("finance_required", "false");
                }

            } else {

                if (bookstockbtn.getText().toString().equalsIgnoreCase("Unbook")) {
                    jObj.put("is_booked", "false");
                    jObj.put("actual_selling_price", selling_price.toString());
                    jObj.put("sales_executive_id", sales_executive_id);
                    jObj.put("finance_required", "false");

                } else {
                    jObj.put("is_booked", "true");

                    jObj.put("actual_selling_price", sellingpricetv.getText().toString());
                    jObj.put("sales_executive_id", sales_executive_id);
                    jObj.put("finance_required", financereqtv.isChecked());
                }
            }


            jObj.put("chassis_number", chassis_number);
            jObj.put("engine_number", engine_number);
            jObj.put("dealer_price", dealer_price);
            jObj.put("dealer_code", dealer_code);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;

    }


    public static void Parsebooknow(JSONObject jObj, String strMethod) {
        try {

            if (jObj.getString("status").equalsIgnoreCase("FAILURE")) {
                StockFragment.onBack = false;
                Toast.makeText(activity, " " + jObj.getString("Message"), Toast.LENGTH_LONG).show();
            } else {
                StockFragment.onBack = true;
                if (bookstockbtn.getText().toString().equalsIgnoreCase("Unbook")) {
                    bookstockbtn.setText("Book Stock");
                    Toast.makeText(activity, " " + "Unbooked", Toast.LENGTH_LONG).show();
                    is_booked = "false";
                } else {
                    bookstockbtn.setText("Unbook");
                    StockStoreInfoActivity.white_star();
                    Toast.makeText(activity, " " + "Booked", Toast.LENGTH_LONG).show();
                    is_booked = "true";
                }
                setDeleteButtonStatus();
                if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("BookedStock")) {

                } else {
                    dialog2.dismiss();
                }

            }


        } catch (Exception ex) {
            Log.e("Parsestockstore-Ex", ex.toString());
        }


    }

    //for stock_fragment font
    public void updateFontUI(Context context) {
        ArrayList<Integer> arl = new ArrayList<Integer>();
        arl.add(R.id.car_name);
        arl.add(R.id.expedition);
      /*  arl.add(R.id.stock_registration);
        arl.add(R.id.stock_certified);*/


        setFontStyle(arl, context);
    }

    private void setFontStyle(ArrayList<Integer> tv_list, Context context) {
        for (int i = 0; i < tv_list.size(); i++) {
            Typeface myTypeface = Typeface.createFromAsset(this.getAssets(), "lato_semibold.ttf");
            TextView myTextView = (TextView) findViewById(tv_list.get(i));
            myTextView.setTypeface(myTypeface);
        }
    }


    public static long printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        /*System.out.println("startDate : " + startDate);
        System.out.println("endDate : "+ endDate);
        System.out.println("different : " + different);*/

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

       /* System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

        Log.e("elapsedDays ","elapsedDays "+elapsedDays);*/
        return elapsedDays;
    }

    public static void error_popup2(int str) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        dialog.setContentView(R.layout.alert_error);
        TextView Message, Confirm;
        Message = dialog.findViewById(R.id.Message);
        Confirm = dialog.findViewById(R.id.ok_error);
        dialog.setCancelable(false);
        String strI = Integer.toString(str);
        Message.setText(GlobalText.AUTH_ERROR);

        Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        dialog.show();
    }


    public String MethodofMonth_Num(String month) {

        try {


            if (month.equalsIgnoreCase("January") || month.equalsIgnoreCase("Jan")) {
                month = "1";
                return month;
            }


            if (month.equalsIgnoreCase("February") || month.equalsIgnoreCase("Feb")) {
                month = "2";
                return month;

            }

            if (month.equalsIgnoreCase("March") || month.equalsIgnoreCase("Mar")) {
                month = "3";
                return month;

            }

            if (month.equalsIgnoreCase("April") || month.equalsIgnoreCase("Apr")) {
                month = "4";
                return month;

            }


            if (month.equalsIgnoreCase("May") || month.equalsIgnoreCase("May")) {
                month = "5";
                return month;

            }

            if (month.equalsIgnoreCase("June") || month.equalsIgnoreCase("Jun")) {
                month = "6";
                return month;

            }

            if (month.equalsIgnoreCase("July") || month.equalsIgnoreCase("Jul")) {
                month = "7";
                return month;

            }

            if (month.equalsIgnoreCase("August") || month.equalsIgnoreCase("Aug")) {
                month = "8";
                return month;

            }


            if (month.equalsIgnoreCase("September") || month.equalsIgnoreCase("Sep")) {
                month = "9";
                return month;

            }

            if (month.equalsIgnoreCase("October") || month.equalsIgnoreCase("Oct")) {
                month = "10";
                return month;

            }

            if (month.equalsIgnoreCase("November") || month.equalsIgnoreCase("Nov")) {
                month = "11";
                return month;

            }

            if (month.equalsIgnoreCase("December") || month.equalsIgnoreCase("Dec")) {
                month = "12";
                return month;

            }
        } catch (Exception e) {

            // FirebaseCrash.log(e.toString());
            //CommonMethods.alertMessage(activity,"Error="+e);
        }

        return month;


    }

    public void ProgressDialogs() {
        /*pd = new ProgressDialog(this);
        pd.setTitle("Loading...");
        pd.setMessage("Please Wait...");
        pd.setCancelable(false);
        if (!pd.isShowing()) {
            pd.show();
        }*/
        try {
            pd = new ProgressDialog(StockStoreInfoActivity.this);
            try {
                if (SplashActivity.progress) {
                    pd.show();
                }
            } catch (WindowManager.BadTokenException e) {

            }
            pd.setCancelable(false);
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            pd.setContentView(R.layout.progressdialog);

        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    public void ProgressDialogsdismiss() {

        if (pd.isShowing()) {
            pd.dismiss();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
            Application.getInstance().trackScreenView(this,GlobalText.StockStoreInfo_Activity);
        }else{
            Application.getInstance().trackScreenView(this,GlobalText.asm_stock_details);
        }


       /* SaveImageString.getInstance().setCoverimage(null);
        SaveImageString.getInstance().setFrontview(null);
        SaveImageString.getInstance().setRearview(null);
        SaveImageString.getInstance().setLeftsideview(null);
        SaveImageString.getInstance().setRightsideview(null);
        SaveImageString.getInstance().setRearleft(null);
        SaveImageString.getInstance().setRearright(null);
        SaveImageString.getInstance().setFrontright(null);
        SaveImageString.getInstance().setWheel(null);
        SaveImageString.getInstance().setFrontseats(null);
        SaveImageString.getInstance().setBackseats(null);
        SaveImageString.getInstance().setOdometer(null);
        SaveImageString.getInstance().setDashboard(null);
        SaveImageString.getInstance().setSteering(null);
        SaveImageString.getInstance().setFrontrowfromside(null);
        SaveImageString.getInstance().setBackrowfromside(null);
        SaveImageString.getInstance().setTrunkdooropenview(null);*/
    }

    public static void setDeleteButtonStatus() {
        if (source.equalsIgnoreCase("p&s")) {

            if (is_booked.equalsIgnoreCase("true") || is_sold.equalsIgnoreCase("1"))
                lldeletebtn.setVisibility(View.GONE);
            else
                lldeletebtn.setVisibility(View.VISIBLE);

        } else {
            lldeletebtn.setVisibility(View.GONE);
        }
    }

    public static void deleteSuccessPopup(JSONObject jsonObject) {

        String status = "";
        String message = "Stock deleted successfully";
        try {
            status = jsonObject.getString("status");

            if (!status.equalsIgnoreCase("SUCCESS")) {
                message = jsonObject.getString("Message");
            }
        } catch (Exception e) {
            Log.i(TAG, "deleteSuccessPopup: ");
        }

        AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
        alertDialog.setTitle(" ");
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                (dialog, which) -> {
                    dialog.cancel();
                    if (StockFragment.stock.equals("Store_Stocks")) {
                        StockStoreFrag.swipeRefresh_s_applyapply();
                    }
                    if (StockFragment.stock.equals("Booked_Stocks")) {
                        BookedStockFrag.swipeRefreshapply();
                    }
                    activity.finish();
                });
        alertDialog.show();
        /*Dialog popDialog = new Dialog(activity);
        popDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        popDialog.setContentView(R.layout.common_cus_dialog);
        TextView message1 = popDialog.findViewById(R.id.Message);
        TextView message2 = popDialog.findViewById(R.id.Message2);
        message1.setText(message);
        Button cancel = popDialog.findViewById(R.id.cancel);
        cancel.setVisibility(View.GONE);
        Button confirm = popDialog.findViewById(R.id.Confirm);
        confirm.setOnClickListener(v -> {
            popDialog.cancel();
            StockStoreFrag.swipeRefresh_s_applyapply();
            activity.finish();
        });
        popDialog.show();*/


    }
}
