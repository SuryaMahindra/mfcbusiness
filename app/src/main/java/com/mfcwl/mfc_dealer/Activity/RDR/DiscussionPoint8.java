package com.mfcwl.mfc_dealer.Activity.RDR;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.mfcwl.mfc_dealer.Activity.RDR.Services.DiscussionPoint7;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GAConstants;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.DealerReportRequest;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.DiscussionPoint;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.ScreenInformation;

import java.util.Arrays;

import imageeditor.base.BaseActivity;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.activity;

public class DiscussionPoint8 extends BaseActivity {

    TextView tvDateTime, tvDealerName, tvFocusArea, tvSubFA;
    ImageView createRepo_backbtn;
    TextView tvSalesTarget, tvWarrantyUnitsTarget;
    TextView tvUnits, tvNonWarrantyCases, tvNonWarrantyCasesFollowup;
    TextView tvActionItems, tvResponsibilities, tvTargetDateCalendar;
    TextView tvPrevious, tvNext, tvFooterHeading, tvDone;
    EditText etNonWarrantyCasesFollowup;
    String strNonWarrantyCasesFollowUp = "";

    private Activitykiller mKiller;
    private LinearLayout llresandtarget;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.discussion_point7);

        initView();

        setListeners();

        mKiller = new Activitykiller();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(mKiller,intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
      unregisterReceiver(mKiller);
    }

    public void initView() {

        createRepo_backbtn = findViewById(R.id.createRepo_backbtn);

        tvDateTime = findViewById(R.id.tvDateTime);
        tvDealerName = findViewById(R.id.tvDealer);
        tvFocusArea = findViewById(R.id.tvFocusArea);
        tvSubFA = findViewById(R.id.tvSubFA);

        tvSalesTarget = findViewById(R.id.tvSalesTarget);
        tvWarrantyUnitsTarget = findViewById(R.id.tvWarrantyUnitsTarget);

        tvUnits = findViewById(R.id.tvUnits);
        tvNonWarrantyCases = findViewById(R.id.tvNonWarrantyCases);
        tvNonWarrantyCasesFollowup = findViewById(R.id.tvNonWarrantyCasesFollowup);

        tvActionItems = findViewById(R.id.tvActionItemList);
        tvResponsibilities = findViewById(R.id.tvResponsibilityList);
        tvTargetDateCalendar = findViewById(R.id.tvTargetDateCalendar);
        etNonWarrantyCasesFollowup=findViewById(R.id.etNonWarrantyCasesFollowup);
        tvPrevious = findViewById(R.id.tvPrevious);
        tvNext = findViewById(R.id.tvNext);
        tvFooterHeading = findViewById(R.id.tvFooterHeading);
        tvDone = findViewById(R.id.tvDone);
        llresandtarget = findViewById(R.id.llresandtarget);

        tvActionItems.setMovementMethod(new ScrollingMovementMethod());
        tvResponsibilities.setMovementMethod(new ScrollingMovementMethod());

    }

    @SuppressLint("SetTextI18n")
    public void setListeners() {

      try{
          tvPrevious.setOnClickListener(this);
          tvNext.setOnClickListener(this);
          tvActionItems.setOnClickListener(this);
          tvResponsibilities.setOnClickListener(this);
          tvTargetDateCalendar.setOnClickListener(this);
          tvDone.setOnClickListener(this);
          etNonWarrantyCasesFollowup.setOnClickListener(this);
          tvDateTime.setText(getDateTime());
          tvDealerName.setText(dealerName + "(" + dealerId + ")");
          tvFocusArea.setText("Sales");
          tvSubFA.setText("Warranty Sales");
          tvFooterHeading.setText("Discussion point 8");
          createRepo_backbtn.setOnClickListener(this);

/*
          tvSalesTarget.setText("Sales Target: " + dealerReportResponse.salesTarget);
*/
        if(dealerReportResponse.warrantyUnitsTarget!=null)
        {
            tvWarrantyUnitsTarget.setText("Warranty Units Target: " + dealerReportResponse.warrantyUnitsTarget);
        }
        else
        {
            tvWarrantyUnitsTarget.setText("Warranty Units Target: 0" );
        }


        if(dealerReportResponse.warrantyUnitsSold!=null && dealerReportResponse.warrantyUnitsSoldPrice!=null)
        {
            tvUnits.setText(" " + dealerReportResponse.warrantyUnitsSold+ "("+dealerReportResponse.warrantyUnitsSoldPrice+")");
        }
        else if(dealerReportResponse.warrantyUnitsSold!=null && dealerReportResponse.warrantyUnitsSoldPrice==null)
        {
            tvUnits.setText(" " + dealerReportResponse.warrantyUnitsSold+ "(0)");
        }
        else if(dealerReportResponse.warrantyUnitsSoldPrice!=null && dealerReportResponse.warrantyUnitsSold==null )
            tvUnits.setText(" 0 (" + dealerReportResponse.warrantyUnitsSoldPrice + ")");
        else
        {
            tvUnits.setText("0 (0)");
        }

        if(dealerReportResponse.warrantyNaCases!=null)
        {
            tvNonWarrantyCases.setText("Non-warranty cases: " + dealerReportResponse.warrantyNaCases);
        }
        else
        {
            tvNonWarrantyCases.setText("Non-warranty cases: 0");
        }

      }catch (Exception e){
          e.printStackTrace();
      }
        //tvNonWarrantyCasesFollowup.setText("Non-warranty cases follow-up: " + dealerReportResponse.warrantyNaCasesFollowedUp);

        tvActionItems.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String data = s.toString();
                if(data.equalsIgnoreCase("No action required")){
                    llresandtarget.setVisibility(View.GONE);
                    strResponsibility = "NA";
                    strTargetDate = "07/17/2020";
                    Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_ACTION_ITEM, CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));

                }else {
                    llresandtarget.setVisibility(View.VISIBLE);
                    strResponsibility = "";
                    strTargetDate = "";
                    Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_ACTION_ITEM, CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));

                }
            }
        });
    }

    public DiscussionPoint getDiscussionPoint() {
        DiscussionPoint discussionPoint = new DiscussionPoint();
        discussionPoint.setActionItem(Arrays.asList(strActionItem.split(("\\|"))));
        discussionPoint.setResponsibility(strResponsibility);
        discussionPoint.setTargetDate(strTargetDate);
        discussionPoint.setFocusArea(tvFocusArea.getText().toString());
        discussionPoint.setSubFocusArea(tvSubFA.getText().toString());

        ScreenInformation info = new ScreenInformation();
        info.setSalesTarget(dealerReportResponse.salesTarget);
        info.setWarrantyUnitsTarget(dealerReportResponse.warrantyUnitsTarget);
        info.setWarrantyNaCases(dealerReportResponse.warrantyNaCases);
        info.setWarrantyNaCasesFollowedUp(strNonWarrantyCasesFollowUp);

        info.setWarrantyUnitsSold(dealerReportResponse.warrantyUnitsSold);
        info.setWarrantyUnitsSoldPrice(dealerReportResponse.warrantyUnitsSoldPrice);

        discussionPoint.setScreenInformation(info);
        return discussionPoint;
    }

    private boolean validate() {

        strActionItem = tvActionItems.getText().toString();
        if(!strActionItem.equalsIgnoreCase("No action required")){
            strResponsibility = tvResponsibilities.getText().toString();
            strTargetDate = tvTargetDateCalendar.getText().toString();
        }

        strNonWarrantyCasesFollowUp = etNonWarrantyCasesFollowup.getText().toString();

        boolean allSelected = true;

        if (strActionItem.equals("") ||
                strResponsibility.equals("") ||
                strTargetDate.equals("") ||
                strNonWarrantyCasesFollowUp.equals("")) {
            allSelected = false;
            Toast.makeText(this, "Please select all fields", Toast.LENGTH_SHORT).show();
        }
        return allSelected;
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            case R.id.createRepo_backbtn:
                startActivity(new Intent(this, DiscussionPoint7.class));
                break;
            case R.id.tvPrevious:
                startActivity(new Intent(this, DiscussionPoint7.class));
                break;
            case R.id.tvNext:
                if (validate()) {
                    removeAnyDuplicates(tvSubFA.getText().toString());
                    discussionPoints.add(getDiscussionPoint());
                    startActivity(new Intent(this, DiscussionPoint9.class));
                }
                break;

            case R.id.tvActionItemList:
                if (dealerReportResponse != null&& dealerReportResponse.warrantySales !=  null) {
                    setDialogMultiSelectionCheck(R.id.tvActionItemList, "Action Items", getStringArray(dealerReportResponse.warrantySales));
                }else {
                    Toast.makeText(DiscussionPoint8.this,"No data found",Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.tvDone:
                if (validate()) {
                    removeAnyDuplicates(tvSubFA.getText().toString());
                    discussionPoints.add(getDiscussionPoint());
                    startActivity(new Intent(DiscussionPoint8.this,DiscussionPoint10.class));
                   // submitToWeb(getRequest());
                }
                break;
        }
    }

    private DealerReportRequest getRequest() {
        DealerReportRequest request = new DealerReportRequest();
        request.dealerCode = dealerReportResponse.dealerCode;
        request.followUpDate = getTimeMMddyyy();
        request.videoUrl = "https://www.youtube.com/watch?v=u99AklNGpyc&list=RDu99AklNGpyc&start_radio=1";
        request.points = discussionPoints;
        request.isOperational="Yes";

        return request;
    }



}