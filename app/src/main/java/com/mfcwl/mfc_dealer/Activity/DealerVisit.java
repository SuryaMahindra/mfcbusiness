package com.mfcwl.mfc_dealer.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.ASMReportsServices.DvReport;
import com.mfcwl.mfc_dealer.AddStockModel.DVRReqModel;
import com.mfcwl.mfc_dealer.AddStockModel.dvrPointData;
import com.mfcwl.mfc_dealer.LocationService.GPSTracker;
import com.mfcwl.mfc_dealer.LocationService.LocationService;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.Helper;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DealerVisit extends AppCompatActivity {

    TextView focus_area_dd ,dealerCategorySelect, dateText, dvr_next_btn, dvr_sumbit_btn, point_tv, dvr_pre_btn;
    public static TextView target_date_set, target_date;
    String dealerCatVal = "select";
    public static Activity activity;
    EditText  points_discussed, actionPlanned, responsibility_tv;
    Button submitButon;
    static String curdate;
    public static Double latitude, longitude;
    public int level = 1;
    DVRReqModel dvrReq;
    ArrayList<dvrPointData> list;
    String[] paylist = {"Morning Meeting","Customer Complaint, if any","Stock Count","Walk-ins + Leads discussion","Sales Discussion",
            "Procurement Discussion","Finance Discussion","Warranty Penetration Discussion","Royalty + Variable Payment","Other Open Issues"};
    public String TAG = getClass().getSimpleName();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate: ");
        setContentView(R.layout.activity_dealer_visit_report);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        activity = this;

        dvrReq = new DVRReqModel();
        level = 1;
        list = new ArrayList<dvrPointData>();

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        ImageView wrBackIV = findViewById(R.id.createRepo_backbtn);
        dealerCategorySelect = findViewById(R.id.spinnerDealer);
        dateText = findViewById(R.id.textDate);
        points_discussed = findViewById(R.id.points_discussed);
        actionPlanned = findViewById(R.id.action_planned);
        target_date = findViewById(R.id.target_date);
        target_date_set = findViewById(R.id.target_date_set);

        dvr_next_btn = findViewById(R.id.dvr_next_btn);
        dvr_sumbit_btn = findViewById(R.id.dvr_sumbit_btn);
        point_tv = findViewById(R.id.point_tv);
        dvr_pre_btn = findViewById(R.id.dvr_pre_btn);

        point_tv.setText("Point Discussed " + String.valueOf(level));

        responsibility_tv = findViewById(R.id.responsibility_tv);
        focus_area_dd = findViewById(R.id.focus_area_dd);

        SimpleDateFormat sdf = new SimpleDateFormat("EEE d MMM HH:mm", Locale.getDefault());
        String currentDateandTime = sdf.format(new Date());


        dateText.setText(currentDateandTime.replace(","," "));

        curdate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());


        target_date_set.setText(curdate.toString());

        CursorClear();

        getLocation();


        wrBackIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DealerVisit.this, MainActivity.class);
                startActivity(intent);
                //finish();
            }
        });

        target_date.setOnClickListener(v -> {
            DatePickerFragment newFragment = new DatePickerFragment();
            newFragment.show(getSupportFragmentManager(), "datePicker");
        });

        focus_area_dd.setOnClickListener(v -> {

            setAlertDialog(v, DealerVisit.this, "Focus Area", paylist, focus_area_dd);
        });


        dealerCategorySelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {

                    if(level==1) {

                        List<String> dealerName = new ArrayList<String>();
                        dealerName.clear();

                        dealerName = Helper.dealerName;


                        List<String> dealerCode = new ArrayList<String>();
                        dealerCode.clear();
                        dealerCode = Helper.dealerCode;


                        String[] namesArr = dealerName.toArray(new String[dealerName.size()]);

                        String[] codeArr = dealerCode.toArray(new String[dealerCode.size()]);

                        setAlertDialog(v, DealerVisit.this, "Dealer Category", namesArr, codeArr);
                        //dealerCategory.setText(weekSelectItem(dealerCategoryArray));

                    }


                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        submitButon = findViewById(R.id.sub_dealer_visit);

        //  submitButon.setOnClickListener(v -> formSubmit());

        dvr_next_btn.setOnClickListener(v -> {

            if (emptyCheck()) {

                try {
                    getAllData();
                    resetData();
                    nextpreSetData();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        });

        dvr_sumbit_btn.setOnClickListener(v -> {

            if (emptyCheck()) {

                try {
                    getAllData();

                    if (CommonMethods.isInternetWorking(DealerVisit.this)) {
                        visitSubmit();
                    } else {
                        CommonMethods.alertMessage(DealerVisit.this, GlobalText.CHECK_NETWORK_CONNECTION);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

        });

        dvr_pre_btn.setOnClickListener(v -> {
            try {

                preSetData();
                CursorClear();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(level>1){
                dealerCategorySelect.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }else{
                dealerCategorySelect.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_down_black_24dp, 0);
            }
        });

        //
        getLocation();


       // dvr_sumbit_btn.setVisibility(View.VISIBLE);
    }

    private void CursorClear() {

        points_discussed.clearFocus();
        actionPlanned.clearFocus();
        responsibility_tv.clearFocus();
    }

    private void preSetData() {

        try {

            if(emptyCheck_pre()){

                dvrReq.setDealerCode(dealerCatVal);
                dvrReq.setLatitude(Double.toString(latitude));
                dvrReq.setLongitude(Double.toString(longitude));

                dvrPointData points = new dvrPointData();
                points.setActionPlan(actionPlanned.getText().toString());
                points.setFocusArea(focus_area_dd.getText().toString());
                points.setPointsDiscussed(points_discussed.getText().toString());
                points.setResponsibility(responsibility_tv.getText().toString());
                points.setTargetDate(target_date_set.getText().toString());

                if (list.size() >= level) {
                    list.remove(level - 1);
                    list.add(level - 1, points);
                } else {
                    list.add(level - 1, points);
                }
                dvrReq.setPoints(list);

            }

            level = level - 1;
            if (level <= 1) {
                dvr_pre_btn.setVisibility(View.INVISIBLE);
            }

            if (level >= 3) {
                dvr_sumbit_btn.setVisibility(View.VISIBLE);
            } else {
                dvr_sumbit_btn.setVisibility(View.INVISIBLE);
            }

            actionPlanned.setText(dvrReq.getPoints().get(level - 1).getActionPlan());
            focus_area_dd.setText(dvrReq.getPoints().get(level - 1).getFocusArea());
            points_discussed.setText(dvrReq.getPoints().get(level - 1).getPointsDiscussed());
            responsibility_tv.setText(dvrReq.getPoints().get(level - 1).getResponsibility());
            target_date_set.setText(dvrReq.getPoints().get(level - 1).getTargetDate());

            if(level>1){
                dealerCategorySelect.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }else{
                dealerCategorySelect.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_down_black_24dp, 0);
            }

            point_tv.setText("Point Discussed " + String.valueOf(level));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void nextpreSetData() {

        try {


            if (dvrReq.getPoints().size() >= level) {

                actionPlanned.setText(dvrReq.getPoints().get(level - 1).getActionPlan());
                focus_area_dd.setText(dvrReq.getPoints().get(level - 1).getFocusArea());
                points_discussed.setText(dvrReq.getPoints().get(level - 1).getPointsDiscussed());
                responsibility_tv.setText(dvrReq.getPoints().get(level - 1).getResponsibility());
                target_date_set.setText(dvrReq.getPoints().get(level - 1).getTargetDate());

                CursorClear();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Boolean emptyCheck() {
        if (dealerCategorySelect.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Select Dealer", Toast.LENGTH_LONG).show();
            return false;
        }else if (focus_area_dd.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Enter Focus Area", Toast.LENGTH_LONG).show();
            return false;
        } else if (points_discussed.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Enter Points discussed", Toast.LENGTH_LONG).show();
            return false;
        } else if (actionPlanned.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Enter Action planned", Toast.LENGTH_LONG).show();
            return false;
        } else if (responsibility_tv.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Enter Responsibility", Toast.LENGTH_LONG).show();
            return false;
        } else if (target_date_set.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Select target date", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    public Boolean emptyCheck_pre() {
        if (dealerCategorySelect.getText().toString().trim().isEmpty()) {
            return false;
        }else if (focus_area_dd.getText().toString().trim().isEmpty()) {
            return false;
        } else if (points_discussed.getText().toString().trim().isEmpty()) {
            return false;
        } else if (actionPlanned.getText().toString().trim().isEmpty()) {
            return false;
        } else if (responsibility_tv.getText().toString().trim().isEmpty()) {
            return false;
        } else if (target_date_set.getText().toString().trim().isEmpty()) {
            return false;
        }

        return true;
    }

    private void getAllData() {

        dvrReq.setDealerCode(dealerCatVal);
        dvrReq.setLatitude(Double.toString(latitude));
        dvrReq.setLongitude(Double.toString(longitude));

        dvrPointData points = new dvrPointData();
        points.setActionPlan(actionPlanned.getText().toString());
        points.setFocusArea(focus_area_dd.getText().toString());
        points.setPointsDiscussed(points_discussed.getText().toString());
        points.setResponsibility(responsibility_tv.getText().toString());
        points.setTargetDate(target_date_set.getText().toString());


        if (list.size() >= level) {

            list.remove(level - 1);
            list.add(level - 1, points);

            Toast.makeText(getApplicationContext(), "Point Discussed "+level+" is Updated", Toast.LENGTH_LONG).show();

        } else {
            list.add(level - 1, points);
            Toast.makeText(getApplicationContext(), "Point Discussed "+level+" is Completed", Toast.LENGTH_LONG).show();
        }

        dvrReq.setPoints(list);

        Gson gson = new Gson();
        String Json = gson.toJson(dvrReq);  //see firstly above

        Log.i(TAG, "getAllData: " + Json);

    }


    CalendarView calenderview;
    TextView displaydata;
    TextView popup_submit;
    ImageView createRepo_backbtn;

    int daydvr = 0, monthdvr = 0, yeardvr = 0;

    private void visitSubmit() {

        daydvr = 0;
        monthdvr = 0;
        yeardvr = 0;

        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);
        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_data.getWindow().setGravity(Gravity.CENTER);
        dialog_data.setContentView(R.layout.visit_list);
        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);


        calenderview = dialog_data.findViewById(R.id.calendarView1);
        displaydata = dialog_data.findViewById(R.id.textView1);
        createRepo_backbtn = dialog_data.findViewById(R.id.createRepo_backbtn);
        popup_submit = dialog_data.findViewById(R.id.popup_submit);


        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, 1);
        calenderview.setMinDate(cal.getTimeInMillis());
        calenderview.setDate (cal.getTimeInMillis(),true,true);


        calenderview.setOnDateChangeListener((view, year, month, dayOfMonth) -> {

            daydvr = dayOfMonth;
            monthdvr = month + 1;
            yeardvr = year;

            // displaydata.setText("Date: " + yeardvr + "-" +monthdvr + "-" + daydvr);
        });


        popup_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //displaydata.setText("Date: " + yeardvr + "-" +monthdvr + "-" + daydvr);

                if (yeardvr == 0) {
                    dvrReq.setFollowUpDate(curdate.toString());
                } else {
                    String fdate = yeardvr + "-" + monthdvr + "-" + daydvr;
                    dvrReq.setFollowUpDate(fdate.toString());
                }

                postData(DealerVisit.this, dvrReq);

            }
        });


        createRepo_backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_data.dismiss();
            }
        });


        dialog_data.show();

    }

    private void resetData() {

        point_tv.setText("");
        focus_area_dd.setText("");
        points_discussed.setText("");
        actionPlanned.setText("");
        responsibility_tv.setText("");

        CursorClear();

        target_date_set.setText(curdate.toString());

        level = level + 1;

        if (level >= 3) {
            dvr_sumbit_btn.setVisibility(View.VISIBLE);
        } else {
            dvr_sumbit_btn.setVisibility(View.INVISIBLE);
        }

        if (level >= 2) {
            dvr_pre_btn.setVisibility(View.VISIBLE);
        } else {
            dvr_pre_btn.setVisibility(View.INVISIBLE);
        }

        point_tv.setText("Point Discussed " + String.valueOf(level));

        if(level>1){
            dealerCategorySelect.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }else{
            dealerCategorySelect.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_down_black_24dp, 0);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        GPSTracker gps = new GPSTracker(this);
        if (!gps.isGPSEnabled()) {
            gps.showSettingsAlert();
        }

    }

    private void getLocation() {


        LocationService locationService = new LocationService(getApplicationContext());
        Location location = locationService.getLocation(LocationManager.GPS_PROVIDER, getApplicationContext());
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        } else {
            Location nwLocation = locationService.getLocation(LocationManager.NETWORK_PROVIDER, getApplicationContext());
            if (nwLocation != null) {
                latitude = nwLocation.getLatitude();
                longitude = nwLocation.getLongitude();
            } else {
                latitude = 0.0;
                longitude = 0.0;
            }
        }
    }


    public void confiirmAlert() {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(DealerVisit.this);
        alertDialog.setTitle("Done");
        alertDialog.setMessage("Data Inserted Successfully");

        alertDialog.setPositiveButton("OK", (dialog, which) -> {
            Intent intent = new Intent(DealerVisit.this, MainActivity.class);
            startActivity(intent);
        });
        alertDialog.show();
    }

    public void setAlertDialog(View v, Activity a, final String strTitle, final String[] arrVal, TextView text) {
        android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(a);
        alert.setTitle(strTitle);

        alert.setItems(arrVal, (dialog, which) -> text.setText(arrVal[which]));
        alert.create();
        alert.show();
    }

    private void setAlertDialog(View v, Activity a, final String strTitle, final String[] arrVal, final String[] codeVal) {

        AlertDialog.Builder alert = new AlertDialog.Builder(a);
        alert.setTitle(strTitle);
        alert.setItems(arrVal, (dialog, which) -> {


            if(arrVal[which].equalsIgnoreCase("Others")
            || arrVal[which].equalsIgnoreCase("Other")){

                dealerCategorySelect.setText("");
                dealerCatVal = "";
                Toast.makeText(getApplicationContext(), "please select valid dealer.", Toast.LENGTH_LONG).show();


            }else {
                dealerCategorySelect.setText(arrVal[which]);
                dealerCatVal = codeVal[which];
            }

        });
        alert.create();
        alert.show();

    }




    private void postData(final Context mContext, final DVRReqModel dvReportModel) {

        SpinnerManager.showSpinner(mContext);

        DvReport.pushdata(dvReportModel, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                SpinnerManager.hideSpinner(mContext);
                try {
                    confiirmAlert();

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Data inserted Successfully", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);

                Toast.makeText(getApplicationContext(), "cannot submit Please check your connection or Host is down ", Toast.LENGTH_LONG).show();

            }
        });
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        static int year, month, day;
        static Calendar cal = java.util.Calendar.getInstance();

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = cal.getInstance();
            int year = c.get(cal.YEAR);
            int month = c.get(cal.MONTH);
            int day;
            day = c.get(cal.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            this.year = year;
            this.month = month + 1;

            this.day = day;

            target_date_set.setText(showDate().toString());

        }


        public static StringBuilder showDate() {
            StringBuilder str = new StringBuilder();
            String m = Integer.toString(month);
            String d = Integer.toString(day);
            if (m.length() == 1 && d.length() == 1) {
                str.append(year).append("-").append("0")
                        .append(month).append("-").append("0").append(day);
            } else if (m.length() == 1) {
                str.append(year).append("-").append("0")
                        .append(month).append("-").append(day);
            } else if (d.length() == 1) {
                str.append(year).append("-")
                        .append(month).append("-").append("0").append(day);
            } else {
                str.append(year).append("-")
                        .append(month).append("-").append(day);
            }
            return str;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(DealerVisit.this, MainActivity.class);
        startActivity(intent);
    }
}
