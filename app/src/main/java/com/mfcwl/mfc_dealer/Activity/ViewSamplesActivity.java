package com.mfcwl.mfc_dealer.Activity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewSamplesActivity extends AppCompatActivity {

    @BindView(R.id.samplesclose)
    public ImageView samplesclose;
    @BindView(R.id.sampleback)
    public ImageView sampleback;
    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_samples);
        Log.i(TAG, "onCreate: ");
        ButterKnife.bind(this);
    }

    @OnClick(R.id.samplesclose)
    public void samplesclose(View view) {

        finish();
    }

    @OnClick(R.id.sampleback)
    public void sampleback(View view) {

        /*Intent in_main = new Intent(ViewSamplesActivity.this, ImageUploadActivity.class);
        startActivity(in_main);*/
        finish();

    }

    @Override
    public void onResume() {
        super.onResume();
        // put your code here...

        Application.getInstance().trackScreenView(this,GlobalText.ViewSamples_Activity);

    }
}
