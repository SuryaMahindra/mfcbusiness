package com.mfcwl.mfc_dealer.Activity.RDR;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.text.style.StrikethroughSpan;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GAConstants;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.DiscussionPoint;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.ScreenInformation;

import java.util.Arrays;

import imageeditor.base.BaseActivity;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.activity;

public class DiscussionPoint3 extends BaseActivity {

    TextView tvDateTime, tvDealerName, tvFocusArea, tvSubFA;
   public ImageView createRepo_backbtn;
    TextView tvProcurementTarget;
    TextView  tvLiveAuctions, tvActionsBidsOwn, tvAuctionsWon;
    // tvLeadsAsperOMS,tvLeadsActual,tvProcuredAsperOMS,tvProcuredActual,etActualProcured,, etActualLeads
    TextView tvActionItems, tvResponsibilities,tv_participation_lbl, tv_live_auctions_lbl,tvTargetDateCalendar, tvPrevious, tvNext, tvFooterHeading, tvAuctionsBidsOn, etAuctionsWon;
    EditText etlowparticipation;
    String strAuctionBidsOn = "", strAuctionsWon = "", strLowparticipation = "", strActualLeads = "", strActualProcured = "";
    //R4
    EditText et_veh_not_asp_req;
    TextView tvNegotiationsVal,tvProcuredVal;
    String vehNotAsPerReq="",strNegotiationsVal="",strProcuredVal="";
    //

    private Activitykiller mKiller;
    private LinearLayout llresandtarget;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.discussion_point3);
        Log.i(TAG, "onCreate: ");

        initView();

        setListeners();

        mKiller = new Activitykiller();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(mKiller,intentFilter);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mKiller);
    }

    public void initView() {

        createRepo_backbtn = findViewById(R.id.createRepo_backbtn);

        tvDateTime = findViewById(R.id.tvDateTime);
        tvDealerName = findViewById(R.id.tvDealer);
        tvFocusArea = findViewById(R.id.tvFocusArea);
        tvSubFA = findViewById(R.id.tvSubFA);

        tvProcurementTarget = findViewById(R.id.tvProcurementTarget);


        tvLiveAuctions = findViewById(R.id.tvLiveAuctions);
        tvActionsBidsOwn = findViewById(R.id.tvAuctionsBidsOn);
        tvAuctionsBidsOn = findViewById(R.id.etAuctionsBidsOn);
        tvAuctionsWon = findViewById(R.id.tvAuctionsWon);
        etlowparticipation = findViewById(R.id.etlowparticipation);
        etAuctionsWon = findViewById(R.id.etAuctionsWon);

        /*tvLeadsAsperOMS = findViewById(R.id.tvLeadsAsPerOMS);
        tvLeadsActual = findViewById(R.id.tvLeadsAsPerOMSActual);
        etActualLeads = findViewById(R.id.etActualLeads);
        tvProcuredAsperOMS = findViewById(R.id.tvProcuredAsPerOMS);
        tvProcuredActual = findViewById(R.id.tvProcuredAsPerOMSActual);
        etActualProcured = findViewById(R.id.etActualProcured);*/

        tvActionItems = findViewById(R.id.tvActionItemList);
        tvResponsibilities = findViewById(R.id.tvResponsibilityList);
        tvTargetDateCalendar = findViewById(R.id.tvTargetDateCalendar);
        tvPrevious = findViewById(R.id.tvPrevious);
        tv_live_auctions_lbl=findViewById(R.id.tv_live_auctions_lbl);
        tvNext = findViewById(R.id.tvNext);
        tvFooterHeading = findViewById(R.id.tvFooterHeading);
        tv_participation_lbl=findViewById(R.id.tv_participation_lbl);
        tvActionItems.setMovementMethod(new ScrollingMovementMethod());
        tvResponsibilities.setMovementMethod(new ScrollingMovementMethod());
        llresandtarget = findViewById(R.id.llresandtarget);

        //R4
        et_veh_not_asp_req=findViewById(R.id.et_veh_not_asp_req);
        tvNegotiationsVal=findViewById(R.id.tvNegotiationsVal);
        tvProcuredVal=findViewById(R.id.tvProcuredVal);

    }

    public void setListeners() {

        tvPrevious.setOnClickListener(this);
        tvNext.setOnClickListener(this);
        tvActionItems.setOnClickListener(this);
        tvResponsibilities.setOnClickListener(this);
        tvTargetDateCalendar.setOnClickListener(this);
        createRepo_backbtn.setOnClickListener(this);

        tvAuctionsBidsOn.setOnClickListener(this);
        etAuctionsWon.setOnClickListener(this);
        /*etActualLeads.setOnClickListener(this);*/
        /*etActualProcured.setOnClickListener(this);*/

        tvDateTime.setText(getDateTime());
        tvDealerName.setText(dealerName + "(" + dealerId + ")");
        tvFocusArea.setText("Procurement");
        tvSubFA.setText("Procurement through IEP");
        tvFooterHeading.setText("Discussion point 3");
        tvAuctionsBidsOn.setText(dealerReportResponse.auctionsBidsOnCpt);
        etAuctionsWon.setText(dealerReportResponse.auctionsBidsWonCpt);
        tvProcurementTarget.setText("Procurement Target: " + dealerReportResponse.procurementTargetStocks);
        if (dealerReportResponse.liveAuctionsIep != null) {
            tvLiveAuctions.setText("" + dealerReportResponse.liveAuctionsIep);
        } else {
            tvLiveAuctions.setText("0");
        }

        //R4
/*
        if(dealerReportResponse.vehicleNotPerRequirementIep!=null)
        {
            et_veh_not_asp_req.setText(dealerReportResponse.vehicleNotPerRequirementIep.toString());
        }
        else
        {
            et_veh_not_asp_req.setText("");

        }
*/

        if(dealerReportResponse.negotiationsIep!=null)
        {
            tvNegotiationsVal.setText(dealerReportResponse.negotiationsIep.toString());
            strNegotiationsVal=tvNegotiationsVal.getText().toString();
        }
        else
        {
            tvNegotiationsVal.setText("0");
            strNegotiationsVal=tvNegotiationsVal.getText().toString();
        }



        if(dealerReportResponse.procuredActualIep!=null)
        {
            tvProcuredVal.setText(dealerReportResponse.procuredActualIep);
            strProcuredVal=tvProcuredVal.getText().toString();
        }else
        {
            tvProcuredVal.setText("0");
            strProcuredVal=tvProcuredVal.getText().toString();
        }

        // tvAuctionsWon.setText("Auctions won: ");
        //  etAuctionsWon.setText(dealerReportResponse.auctionsBidsOnIep);
        //tvLeadsAsperOMS.setText("Leads as per OMS: " + dealerReportResponse.leadsOmsIep);
        // tvLeadsActual.setText("Leads (Actual): ");
        /*etActualLeads.setText(dealerReportResponse.leadsActualIep);*/
        /*tvProcuredAsperOMS.setText("Procured as per OMS: " + dealerReportResponse.procuredOmsIep);*/
        // tvProcuredActual.setText("Procured (Actual): ");
        /*etActualProcured.setText(dealerReportResponse.procuredActualIep);*/

        tvActionItems.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String data = s.toString();
                if(data.equalsIgnoreCase("No action required")){
                    llresandtarget.setVisibility(View.GONE);
                    strResponsibility = "NA";
                    strTargetDate = "07/17/2020";
                    Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_ACTION_ITEM, CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));

                }else {
                    llresandtarget.setVisibility(View.VISIBLE);
                    strResponsibility = "";
                    strTargetDate = "";
                    Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_ACTION_ITEM,CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));
                }
            }
        });

    }

    public DiscussionPoint getDiscussionPoint() {
        DiscussionPoint discussionPoint = new DiscussionPoint();
        discussionPoint.setActionItem(Arrays.asList(strActionItem.split(("\\|"))));
        discussionPoint.setResponsibility(strResponsibility);
        discussionPoint.setTargetDate(strTargetDate);
        discussionPoint.setFocusArea(tvFocusArea.getText().toString());
        discussionPoint.setSubFocusArea(tvSubFA.getText().toString());

        ScreenInformation screenInformation = new ScreenInformation();
        //screenInformation.setProcurementTargetStocks(dealerReportResponse.procurementTargetStocks);
        screenInformation.setProcurementTargetIep(dealerReportResponse.procurementTargetIep);
        screenInformation.setLiveAuctionsIep(dealerReportResponse.liveAuctionsIep);
        screenInformation.setVehicleNotPerRequirementIep(vehNotAsPerReq);
        screenInformation.setAuctionsBidsOnIep(strAuctionBidsOn);
        screenInformation.setAuctionsBidsWonIep(strAuctionsWon);
        screenInformation.setNegotiationsIep(strNegotiationsVal);
        screenInformation.setLeadsActualIep(strActualLeads);
        screenInformation.setReasonslowparticipationiep(strLowparticipation);
        screenInformation.setProcuredOmsIep(dealerReportResponse.procuredOmsIep);
        screenInformation.setLeadsOmsIep(dealerReportResponse.leadsOmsIep);

        screenInformation.setProcuredActualIep(strProcuredVal);

        discussionPoint.setScreenInformation(screenInformation);

        return discussionPoint;
    }

    private boolean validate() {

        strActionItem = tvActionItems.getText().toString();
        if(!strActionItem.equalsIgnoreCase("No action required")){
            strResponsibility = tvResponsibilities.getText().toString();
            strTargetDate = tvTargetDateCalendar.getText().toString();
        }

        strAuctionBidsOn = tvAuctionsBidsOn.getText().toString();
        strAuctionsWon = etAuctionsWon.getText().toString();
        strLowparticipation = etlowparticipation.getText().toString();
        vehNotAsPerReq=et_veh_not_asp_req.getText().toString();

        /*strActualLeads = etActualLeads.getText().toString();*/
/*
        strActualProcured = etActualProcured.getText().toString();
        strActualLeads.equals("") ||
*/

        boolean allSelected = true;

        if (strActionItem.equals("") ||
                strResponsibility.equals("") ||
                strTargetDate.equals("") ||
                strLowparticipation.equals("")||
                vehNotAsPerReq.equals(""))
        {
            allSelected = false;
            Toast.makeText(this, "Please select all fields", Toast.LENGTH_SHORT).show();
        }
        return allSelected;
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            case R.id.createRepo_backbtn:
                startActivity(new Intent(this, DiscussionPoint2.class));
                break;
            case R.id.tvPrevious:
                startActivity(new Intent(this,DiscussionPoint2.class));
                break;
            case R.id.tvNext:
                if (validate()) {
                    removeAnyDuplicates(tvSubFA.getText().toString());
                    discussionPoints.add(getDiscussionPoint());
                    startActivity(new Intent(this, DiscussionPoint4.class));
                }
                break;
            case R.id.tvActionItemList:
                if (dealerReportResponse != null && dealerReportResponse.procurementIEP != null) {
                    setDialogMultiSelectionCheck(R.id.tvActionItemList, "Action Items", getStringArray(dealerReportResponse.procurementIEP));
                }else {
                    Toast.makeText(DiscussionPoint3.this,"No data found",Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public void setStrikeThroughText(TextView textview,String stringVal,int startingLimit,int endingLimit)
    {
        SpannableString string = new SpannableString(stringVal);
        string.setSpan(new StrikethroughSpan(), startingLimit, endingLimit, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textview.setText(string);
    }


}