package com.mfcwl.mfc_dealer.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.Global;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditStockActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    @BindView(R.id.primarydetails)
    public LinearLayout primarydetails;
    @BindView(R.id.additionaldetails)
    public LinearLayout additionaldetails;
    @BindView(R.id.addstock_savebtn)
    public Button addstock_savebtn;
    @BindView(R.id.addstockcancelbtn)
    public Button addstockcancelbtn;
    @BindView(R.id.eadditionallbl)
    public TextView eadditionallbl;
    @BindView(R.id.eprimarylbl)
    public TextView eprimarylbl;


    public static TextView modelandvariant;
    @BindView(R.id.vediclelbl)
    public TextView vediclelbl;
    @BindView(R.id.stock_categorylbl)
    public TextView stock_categorylbl;
    public static TextView stockcategory_tv;
    @BindView(R.id.addstock_yesrtv)
    public TextView addstock_yesrtv;
    @BindView(R.id.addstock_monthtv)
    public TextView addstock_monthtv;
    @BindView(R.id.regmonthtv)
    public TextView regmonthtv;

    @BindView(R.id.makelbl)
    public TextView makelbl;
    @BindView(R.id.maketv)
    public TextView maketv;

    @BindView(R.id.reglbl)
    public TextView reglbl;
    @BindView(R.id.regtv)
    public EditText regtv;
    @BindView(R.id.kmslbl)
    public TextView kmslbl;
    @BindView(R.id.kmstv)
    public EditText kmstv;
    @BindView(R.id.colorlbl)
    public TextView colorlbl;
    @BindView(R.id.colortv)
    public TextView colortv;

    @BindView(R.id.ownerlbl)
    public TextView ownerlbl;
    public static TextView ownertv;
    @BindView(R.id.regcitylbl)
    public TextView regcitylbl;
    @BindView(R.id.regcitytv)
    public TextView regcitytv;

    public static TextView cngkittv;

    @BindView(R.id.manufacturedonlbl)
    public TextView manufacturedonlbl;
    @BindView(R.id.makevariantlbl)
    public TextView makevariantlbl;

    @BindView(R.id.chassisnolbl)
    public TextView chassisnolbl;
    @BindView(R.id.chassisnotv)
    public EditText chassisnotv;
    @BindView(R.id.enginenolbl)
    public TextView enginenolbl;
    @BindView(R.id.enginenotv)
    public EditText enginenotv;
    @BindView(R.id.boughtlbl)
    public TextView boughtlbl;
    @BindView(R.id.boughttv)
    public EditText boughttv;
    @BindView(R.id.refurelbl)
    public TextView refurelbl;
    @BindView(R.id.refuretv)
    public EditText refuretv;
    @BindView(R.id.procuretv)
    public TextView procuretv;
    @BindView(R.id.cngkitlbl)
    public TextView cngkitlbl;
    @BindView(R.id.commenttv)
    public TextView commenttv;
    @BindView(R.id.commentlentv)
    public TextView commentlentv;
    @BindView(R.id.procurexe)
    public TextView procurexe;
    @BindView(R.id.greenline_1)
    public TextView greenline_1;
    @BindView(R.id.greenline_2)
    public TextView greenline_2;

    @BindView(R.id.dealerplbl)
    public TextView dealerplbl;
    @BindView(R.id.sellingplbl)
    public TextView sellingplbl;
    //@BindView(R.id.insexpdlbl)
    public static TextView insexpdlbl;
    @BindView(R.id.inslbl)
    public TextView inslbl;
    @BindView(R.id.regonlbl)
    public TextView regonlbl;

    @BindView(R.id.sellingptv)
    public EditText sellingptv;
    @BindView(R.id.dealerptv)
    public EditText dealerptv;
    @BindView(R.id.regontv)
    public TextView regontv;
    public static TextView instv;
    //@BindView(R.id.insexpdtv)
    public static TextView insexpdtv;
    public static TextView insExline;
    public static TextView vehicletv;

    @BindView(R.id.editcmts)
    public EditText editcmts;

    @BindView(R.id.stockcategory_error_tv)
    public TextView stockcategory_error_tv;
    @BindView(R.id.vehicle_error_tv)
    public TextView vehicle_error_tv;
    @BindView(R.id.month_error_tv)
    public TextView month_error_tv;
    @BindView(R.id.year_error_tv)
    public TextView year_error_tv;
    @BindView(R.id.make_error)
    public TextView make_error;
    @BindView(R.id.modelv_error)
    public TextView modelv_error;
    @BindView(R.id.color_error)
    public TextView color_error;
    @BindView(R.id.reg_error)
    public TextView reg_error;
    @BindView(R.id.dealerprice_error)
    public TextView dealerprice_error;
    @BindView(R.id.sellerprice_error)
    public TextView sellerprice_error;
    @BindView(R.id.owner_error)
    public TextView owner_error;
    @BindView(R.id.kms_error)
    public TextView kms_error;
    @BindView(R.id.regcity_error)
    public TextView regcity_error;
    @BindView(R.id.regon_error)
    public TextView regon_error;
    @BindView(R.id.regmonth_error)
    public TextView regmonth_error;
    //@BindView(R.id.insexp_error)
    public static TextView insexp_error;
    @BindView(R.id.ins_error)
    public TextView ins_error;
    @BindView(R.id.chassis_error)
    public TextView chassis_error;
    @BindView(R.id.engine_error)
    public TextView engine_error;
    @BindView(R.id.bought_error)
    public TextView bought_error;
    @BindView(R.id.proexe_error)
    public TextView proexe_error;
    @BindView(R.id.editstockback)
    public ImageView editstockback;
    @BindView(R.id.editstockclose)
    public ImageView editstockclose;

    @BindView(R.id.editstockRegno)
    public TextView editstockRegno;


    boolean onlyOnce = true, onlyOnceCheck = true;

    ArrayList<String> cityList;
    ArrayList<String> cityCode;
    ArrayList<String> makeList;
    ArrayList<String> yearList;
    ArrayList<String> colorIdList;
    ArrayList<String> colorList;

    ArrayList<String> procurementTextList;
    ArrayList<String> procurementValueList;
    ArrayList<String> procurementTextValueList;
    static ArrayList<String> modalVariant;
    static ArrayList<String> modelArr;
    static ArrayList<String> variantArr;


    static HashMap<String, String> modelhm;
    static HashMap<String, String> varianthm;

    String cityName = "", makeName = "", yearName = "", yearregName = "", stryr = "", colourName;

   /* public static String vehicleType = "", vehicleCat = "", manufacYear = "", manufacMonth = "", vehicleMake = "";
    public static String vehicleModel = "", vehicleVaraiant = "", registrationNumber = "", vehicleColor = "", vehicleOwners = "", vehicleKms = "";
    public static String regYear = "", regCity = "", insurType = "", insurExpDate = "", vehicleChasisNumb = "", boughtPrice = "", refurbisCost = "", dealerPrice = "";
    public static String sellingPrice = "", createprocureExecId = "", cngKit = "", engineNumber = "", regisMonth = "", comments = "";
    public static String IsOffLoad = "false", stocktype = "";
    String timeS = "", dateS = "";*/

    int year, month, day, daysinmonth;
    long thismondatetime, nextmondatetime;
    long currentDate;

    ProgressDialog pd;

    public static boolean is_certified = false;
    public static String make, model, registraionNo, selling_price, owner, reg_year, kms, registraionCity, certifiedNo,
            insurance, insuranceExp_date,
            source, warranty, surveyor_remark, photo_count, reg_month, fuel_type, bought,
            refurbishment_cost, dealer_price, procurement_executive_id, cng_kit, chassis_number, engine_number,
            is_display, is_featured_car, is_featured_car_admin, procurement_executive_name,
            private_vehicle, comments, finance_required, manufacture_month, sales_executive_id,
            sales_executive_name, manufacture_year, actual_selling_price, CertifiedText, IsDisplay,
            TransmissionType, colour, dealer_code, is_offload, posted_date, stock_id, is_booked, variant, coverimage;


    public static String[] imageurl;
    public static String[] imagename;
    public boolean reg, reg1;
    boolean nextFlag = false;
    public static Activity activity;
    public Context context;
    public String star = "<font color='#B40404'>*</font>";
    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editstock_info);
        Log.i(TAG, "onCreate: ");
        ButterKnife.bind(this);
        CommonMethods.MemoryClears();
        activity = this;
        context = this;
        SplashActivity.progress = true;
        Log.i(TAG, "onCreate: ");
        greenline_1.setBackgroundResource(R.color.yellow);
        greenline_2.setBackgroundResource(R.color.Black);

        eprimarylbl.setTextColor(getResources().getColor(R.color.yellow));
        eadditionallbl.setTextColor(getResources().getColor(R.color.White));

        primarydetails.setVisibility(View.VISIBLE);
        additionaldetails.setVisibility(View.GONE);
        nextFlag = false;

        insexpdlbl = (TextView) findViewById(R.id.insexpdlbl);
        insexpdtv = (TextView) findViewById(R.id.insexpdtv);
        insexp_error = (TextView) findViewById(R.id.insexp_error);
        insExline = (TextView) findViewById(R.id.insExline);

        cityList = new ArrayList<String>();
        cityCode = new ArrayList<String>();
        modalVariant = new ArrayList<String>();
        modelArr = new ArrayList<String>();
        variantArr = new ArrayList<String>();
        makeList = new ArrayList<String>();
        yearList = new ArrayList<String>();


        modelhm = new HashMap<String, String>();
        varianthm = new HashMap<String, String>();
        /*regtv.setText("ASdfasdfasdfasdf");
        regtv.setInputType(InputType.TYPE_NULL);*/

        colorIdList = new ArrayList<String>();
        colorList = new ArrayList<String>();

        procurementTextList = new ArrayList<String>();
        procurementValueList = new ArrayList<String>();
        procurementTextValueList = new ArrayList<String>();
        modelandvariant = (TextView) findViewById(R.id.modelandvariant);
        vehicletv = (TextView) findViewById(R.id.vehicletv);
        stockcategory_tv = (TextView) findViewById(R.id.stockcategory_tv);
        stockcategory_tv.setInputType(InputType.TYPE_NULL);
        ownertv = (TextView) findViewById(R.id.ownertv);
        instv = (TextView) findViewById(R.id.instv);
        cngkittv = (TextView) findViewById(R.id.cngkittv);

        try {
            vediclelbl.setText(Html.fromHtml("VEHICLE " + star));
            stock_categorylbl.setText(Html.fromHtml("STOCK CATEGORY " + star));
            manufacturedonlbl.setText(Html.fromHtml("MANUFACTURED ON " + star));
            makelbl.setText(Html.fromHtml("MAKE " + star));
            makevariantlbl.setText(Html.fromHtml("MODEL AND VARIANT " + star));
            reglbl.setText(Html.fromHtml("REGISTRATION NUMBER " + star));
            colorlbl.setText(Html.fromHtml("COLOR " + star));
            ownerlbl.setText(Html.fromHtml("OWNER " + star));
            kmslbl.setText(Html.fromHtml("KMS " + star));
            regcitylbl.setText(Html.fromHtml("REG CITY " + star));
            procurexe.setText(Html.fromHtml("PROCUREMENT EXECUTIVE " + star));
            procurexe.setText(Html.fromHtml("PROCUREMENT EXECUTIVE " + star));

            dealerplbl.setText(Html.fromHtml("DEALER PRICE " + star));
            sellingplbl.setText(Html.fromHtml("SELLING PRICE " + star));
            insexpdlbl.setText(Html.fromHtml("INS. EXPIRY DATE " + star));
            inslbl.setText(Html.fromHtml("INSURANCE " + star));
            regonlbl.setText(Html.fromHtml("REGISTERED ON " + star));


            //add
            chassisnolbl.setText(Html.fromHtml("CHASSIS NUMBER " + star));
            enginenolbl.setText(Html.fromHtml("ENGINE NUMBER " + star));
            boughtlbl.setText(Html.fromHtml("BOUGHT PRICE " + star));


            make = getIntent().getStringExtra("make");
            model = getIntent().getStringExtra("model");
            //vehicleModel = variantModel;
            variant = getIntent().getStringExtra("variant");
            //vehicleVaraiant = variant;
            registraionNo = getIntent().getStringExtra("registraionNo");
            selling_price = getIntent().getStringExtra("selling_price");
            owner = getIntent().getStringExtra("owner");
            reg_year = getIntent().getStringExtra("reg_year");
            kms = getIntent().getStringExtra("kms");
            registraionCity = getIntent().getStringExtra("registraionCity");
            certifiedNo = getIntent().getStringExtra("certifiedNo");
            insurance = getIntent().getStringExtra("insurance");
            insuranceExp_date = getIntent().getStringExtra("insuranceExp_date");
            source = getIntent().getStringExtra("source");
            warranty = getIntent().getStringExtra("warranty");
            surveyor_remark = getIntent().getStringExtra("surveyor_remark");
            photo_count = getIntent().getStringExtra("photo_count");
            reg_month = getIntent().getStringExtra("reg_month");

            fuel_type = getIntent().getStringExtra("fuel_type");
            bought = getIntent().getStringExtra("bought");
            refurbishment_cost = getIntent().getStringExtra("refurbishment_cost");
            dealer_price = getIntent().getStringExtra("dealer_price");
            procurement_executive_id = getIntent().getStringExtra("ProcurementExecId");
            cng_kit = getIntent().getStringExtra("cng_kit");
            chassis_number = getIntent().getStringExtra("chassis_number");
            engine_number = getIntent().getStringExtra("engine_number");

            // is_display= getIntent().getStringExtra("is_display");
            String str = getIntent().getStringExtra("is_certified");
            if (str.equalsIgnoreCase("true")
                    || str == null) {
                is_certified = true;
            } else {
                is_certified = false;
            }


            is_featured_car = getIntent().getStringExtra("is_featured_car");
            is_featured_car_admin = getIntent().getStringExtra("is_featured_car_admin");
            procurement_executive_name = getIntent().getStringExtra("procurement_executive_name");
            private_vehicle = getIntent().getStringExtra("private_vehicle");
            comments = getIntent().getStringExtra("comments");//
            finance_required = getIntent().getStringExtra("finance_required");
            manufacture_month = getIntent().getStringExtra("manufacture_month");
            sales_executive_id = getIntent().getStringExtra("sales_executive_id");
            sales_executive_name = getIntent().getStringExtra("sales_executive_name");
            manufacture_year = getIntent().getStringExtra("manufacture_year");
            actual_selling_price = getIntent().getStringExtra("actual_selling_price");
            CertifiedText = getIntent().getStringExtra("CertifiedText");
            IsDisplay = getIntent().getStringExtra("IsDisplay");
            TransmissionType = getIntent().getStringExtra("TransmissionType");
            colour = getIntent().getStringExtra("colour");
            dealer_code = getIntent().getStringExtra("dealer_code");
            is_offload = getIntent().getStringExtra("is_offload");
            posted_date = getIntent().getStringExtra("posted_date");
            stock_id = getIntent().getStringExtra("stock_id");
            is_booked = getIntent().getStringExtra("is_booked");

            imageurl = getIntent().getStringArrayExtra("imageurl");
            imagename = getIntent().getStringArrayExtra("imagename");
            coverimage = getIntent().getStringExtra("coverimage");


            if (!is_certified) {
                regtv.setInputType(InputType.TYPE_CLASS_TEXT);
                kmstv.setInputType(InputType.TYPE_CLASS_NUMBER);
                chassisnotv.setInputType(InputType.TYPE_CLASS_TEXT);
                enginenotv.setInputType(InputType.TYPE_CLASS_TEXT);
            } else {
                regtv.setInputType(InputType.TYPE_NULL);
                kmstv.setInputType(InputType.TYPE_NULL);
                chassisnotv.setInputType(InputType.TYPE_NULL);
                enginenotv.setInputType(InputType.TYPE_NULL);
            }

            Log.e("private_vehicle", "private_vehicle" + private_vehicle);

            if (private_vehicle.equalsIgnoreCase("1") || private_vehicle.equalsIgnoreCase("true")) {
                vehicletv.setText("Private");
            } else {
                vehicletv.setText("Commercial");
            }

            if (is_offload.equalsIgnoreCase("1") || is_offload.equalsIgnoreCase("true")) {
                stockcategory_tv.setText("OffLoad Vehicle");
                is_offload = "1";
            } else {
                stockcategory_tv.setText("Retail");
                is_offload = "0";
            }

       /* if (is_offload.equals("Retail")) {
            is_offload = "0";
        } else {
            is_offload = "1";
        }*/


            addstock_yesrtv.setText(manufacture_year);
            addstock_monthtv.setText(manufacture_month);
            maketv.setText(make);
            modelandvariant.setText(model + " " + variant);
            regtv.setAllCaps(true);
            regtv.setText(registraionNo);
            colortv.setText(colour);
            kmstv.setText(kms);
            ownertv.setText(owner);
            regcitytv.setText(registraionCity);
            regontv.setText(reg_year);
            regmonthtv.setText(reg_month);
            instv.setText(insurance);

            if (insuranceExp_date == null || insuranceExp_date.equalsIgnoreCase("")
                    || insuranceExp_date.equalsIgnoreCase("null")) {


                insexpdtv.setText("NA");

            } else {

                if (insuranceExp_date.length() < 10) {
                    insexpdtv.setText(insuranceExp_date);

                } else {
                    //reena

                    String insuranceExp_date1 = insuranceExp_date.substring(0, 10);
                    insexpdtv.setText(insuranceExp_date1);
                }
            }


            // insexpdtv.setText(insuranceExp_date);


            chassisnotv.setText(chassis_number);
            enginenotv.setText(engine_number);
            procuretv.setText(procurement_executive_name);
            cngkittv.setText(cng_kit);

            sellingptv.setText(selling_price);
            dealerptv.setText(dealer_price);
            boughttv.setText(bought);
            refuretv.setText(refurbishment_cost);
            editcmts.setText(comments);
            editstockRegno.setText(registraionNo);

            if (instv.getText().toString().equalsIgnoreCase("NA")) {

                insexpdlbl.setVisibility(View.INVISIBLE);
                insexpdtv.setVisibility(View.INVISIBLE);
                insexp_error.setVisibility(View.INVISIBLE);
                insExline.setVisibility(View.INVISIBLE);
            } else {
                insexpdlbl.setVisibility(View.VISIBLE);
                insexpdtv.setVisibility(View.VISIBLE);
                insExline.setVisibility(View.VISIBLE);
            }

        } catch (NullPointerException e) {
            //nulls
        }
        updateFontUI(context);

        nextFlag = false;

        editcmts.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String currentText = editable.toString();
                int currentLength = currentText.length();
                commentlentv.setText(currentLength + "/230");
            }
        });

        modelandvariant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!is_certified) {

                    if (!maketv.getText().toString().equals("")) {
                        makeName = maketv.getText().toString();
                        modelandvariant(view);
                    } else {
                        Toast.makeText(EditStockActivity.this, "Please Select Make ", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        vehicletv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!is_certified) {
                    maketv.setText("");
                    modelandvariant.setText("");
                    String[] vehlist = {"Private", "Commercial"};
                    setAlertDialog(v, activity, "Vehicle", vehlist);
                }


            }
        });

        stockcategory_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!is_certified) {
                    String[] stockcatlist = {"Retail", "OffLoad Vehicle"};
                    setAlertDialog(v, activity, "Stock Category", stockcatlist);
                }
            }
        });
        ownertv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!is_certified) {

                    String[] ownertvlist = {"1", "2", "3", "4", "5"};
                    setAlertDialog(v, activity, "Owner", ownertvlist);

                }

            }
        });

        cngkittv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!is_certified) {

                    String[] cngkittvlist = {"NA", "Company Fitted", "After Market"};
                    setAlertDialog(v, activity, "CNG KIT", cngkittvlist);
                }
            }
        });
        instv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!is_certified) {

                    insexpdtv.setText("");
                    String[] instvlist = {"Comprehensive", "Third Party", "NA"};
                    setAlertDialog(v, activity, "Insurance", instvlist);
                }
            }
        });


        regtv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String value = s.toString();

                if (value.length() == 1) {
                    reg = value.substring(0).matches("\\d+(?:\\.\\d+)?");
                    if (reg) {

                        reg_error.setVisibility(View.VISIBLE);
                        reg_error.setText("Please enter correct format.");
                        CommonMethods.alertMessage(activity, "Please enter correct format.");
                    } else {
                        reg_error.setVisibility(View.GONE);
                    }

                } else if (value.length() >= 1) {
                    reg = value.substring(1).matches("\\d+(?:\\.\\d+)?");
                    if (reg) {

                        reg_error.setVisibility(View.VISIBLE);
                        reg_error.setText("Please enter correct format.");
                        CommonMethods.alertMessage(activity, "Please enter correct format.");
                    } else {
                        if (!reg)
                            reg_error.setVisibility(View.GONE);

                    }
                }


            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item, menu);
        MenuItem item = menu.findItem(R.id.notification_bell);
        item.setVisible(false);

        return true;
    }

    @OnClick(R.id.eprimarylbl)
    public void eprimarylbl(View view) {

        primarydetails.setVisibility(View.VISIBLE);
        additionaldetails.setVisibility(View.GONE);

        eprimarylbl.setTextColor(getResources().getColor(R.color.yellow));
        eadditionallbl.setTextColor(getResources().getColor(R.color.White));

        greenline_1.setBackgroundResource(R.color.yellow);
        greenline_2.setBackgroundResource(R.color.Black);
    }

    @OnClick(R.id.eadditionallbl)
    public void eadditionallbl(View view) {

        primarydetails.setVisibility(View.GONE);
        additionaldetails.setVisibility(View.VISIBLE);

        eprimarylbl.setTextColor(getResources().getColor(R.color.White));
        eadditionallbl.setTextColor(getResources().getColor(R.color.yellow));

        greenline_1.setBackgroundResource(R.color.Black);
        greenline_2.setBackgroundResource(R.color.yellow);

    }

    @OnClick(R.id.editstockback)
    public void editstockback(View view) {
        finish();
    }


    @OnClick(R.id.editstockclose)
    public void editstockclose(View view) {
        finish();
    }

    @OnClick(R.id.addstock_savebtn)
    public void addstock_savebtn(View view) {

        if (!vehicletv.getText().toString().equals("")
                && !stockcategory_tv.getText().toString().equals("")
                && !vehicletv.getText().toString().equals("")
                && !stockcategory_tv.getText().toString().equals("")
                && !addstock_yesrtv.getText().toString().equals("")
                && !addstock_monthtv.getText().toString().equals("")
                && !maketv.getText().toString().equals("")
                && !modelandvariant.getText().toString().equals("")
                && !sellingptv.getText().toString().equals("")
                && !dealerptv.getText().toString().equals("")
                && !regtv.getText().toString().equals("")
                && !colortv.getText().toString().equals("")
                && !kmstv.getText().toString().equals("")
                && !ownertv.getText().toString().equals("")
                && !regcitytv.getText().toString().equals("")
                && !regontv.getText().toString().equals("")
                && !regmonthtv.getText().toString().equals("")
                && !instv.getText().toString().equals("")
            /*&& !insexpdtv.getText().toString().equals("")*/
                ) {

            if (regtv.length() < 5) {

                if (regtv.length() < 5) {
                    reg_error.setVisibility(View.VISIBLE);
                    reg_error.setText("Invaild Reg.Number.");
                    return;
                } else {
                    reg_error.setVisibility(View.INVISIBLE);

                }
            }

            if (!instv.getText().toString().equalsIgnoreCase("NA") && insexpdtv.getText().toString().equals("")) {
                insexp_error.setVisibility(View.VISIBLE);
            } else {

                errordisables();
                primarydetails.setVisibility(View.GONE);
                additionaldetails.setVisibility(View.VISIBLE);

                greenline_1.setBackgroundResource(R.color.Black);
                greenline_2.setBackgroundResource(R.color.yellow);

                eprimarylbl.setTextColor(getResources().getColor(R.color.White));
                eadditionallbl.setTextColor(getResources().getColor(R.color.yellow));

                if (nextFlag) {

                    Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.edit_primary_stocks, GlobalText.android);

                    if (!chassisnotv.getText().toString().equals("")
                            && !enginenotv.getText().toString().equals("")
                            && !boughttv.getText().toString().equals("")
                            && !procuretv.getText().toString().equals("")) {

                        if (chassisnotv.length() < 6 || enginenotv.length() < 6) {

                            if (chassisnotv.length() < 6) {
                                chassis_error.setVisibility(View.VISIBLE);
                                chassis_error.setText("Invaild Chassis No.");
                            } else {
                                chassis_error.setVisibility(View.INVISIBLE);

                            }
                            if (enginenotv.length() < 6) {
                                engine_error.setVisibility(View.VISIBLE);
                                engine_error.setText("Invaild Engine No.");
                            } else {
                                engine_error.setVisibility(View.INVISIBLE);

                            }

                        } else {
                            chassis_error.setVisibility(View.INVISIBLE);
                            engine_error.setVisibility(View.INVISIBLE);
                            bought_error.setVisibility(View.INVISIBLE);
                            proexe_error.setVisibility(View.INVISIBLE);
                            selectImage();
                        }

                    } else {
                        errorshow();

                    }

                } else {
                    Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.edit_additional_stocks, GlobalText.android);

                    nextFlag = true;
                }
            }
        } else {

            errorText();

        }

    }

    @OnClick(R.id.addstockcancelbtn)
    public void addstockcancelbtn(View view) {
        //AddStockActivity
        /*Intent in_main = new Intent(EditStockActivity.this, StockStoreInfoActivity.class);
        startActivity(in_main);*/
        finish();
    }


    ///////////////////////////////////////////////////////


    private void errorshow() {

        if (chassisnotv.getText().toString().equals("")) {
            chassis_error.setVisibility(View.VISIBLE);
        } else {
            chassis_error.setVisibility(View.INVISIBLE);
        }

        if (enginenotv.getText().toString().equals("")) {
            engine_error.setVisibility(View.VISIBLE);
        } else {
            engine_error.setVisibility(View.INVISIBLE);
        }

        if (boughttv.getText().toString().equals("")) {
            bought_error.setVisibility(View.VISIBLE);
        } else {
            bought_error.setVisibility(View.INVISIBLE);
        }

        if (procuretv.getText().toString().equals("")) {
            proexe_error.setVisibility(View.VISIBLE);
        } else {
            proexe_error.setVisibility(View.INVISIBLE);
        }


    }

    private void errordisables() {

        vehicle_error_tv.setVisibility(View.INVISIBLE);
        stockcategory_error_tv.setVisibility(View.INVISIBLE);
        year_error_tv.setVisibility(View.INVISIBLE);
        month_error_tv.setVisibility(View.INVISIBLE);
        make_error.setVisibility(View.INVISIBLE);
        modelv_error.setVisibility(View.INVISIBLE);
        sellerprice_error.setVisibility(View.INVISIBLE);
        dealerprice_error.setVisibility(View.INVISIBLE);
        reg_error.setVisibility(View.INVISIBLE);
        color_error.setVisibility(View.INVISIBLE);
        kms_error.setVisibility(View.INVISIBLE);
        owner_error.setVisibility(View.INVISIBLE);
        regcity_error.setVisibility(View.INVISIBLE);
        regon_error.setVisibility(View.INVISIBLE);
        regmonth_error.setVisibility(View.INVISIBLE);
        ins_error.setVisibility(View.INVISIBLE);

      /*  if(!instv.getText().toString().equalsIgnoreCase("NA")) {
            insexp_error.setVisibility(View.INVISIBLE);
        }*/

        if (!instv.getText().toString().equalsIgnoreCase("NA")) {

            if (insexpdtv.getText().toString().equals("")) {
                insexp_error.setVisibility(View.VISIBLE);
            } else {
                insexp_error.setVisibility(View.INVISIBLE);
            }
        }

    }

    private void errorText() {

        if (vehicletv.getText().toString().equals("")) {
            vehicle_error_tv.setVisibility(View.VISIBLE);
        } else {
            vehicle_error_tv.setVisibility(View.INVISIBLE);
        }

        if (stockcategory_tv.getText().toString().equals("")) {
            stockcategory_error_tv.setVisibility(View.VISIBLE);
        } else {
            stockcategory_error_tv.setVisibility(View.INVISIBLE);
        }

        if (addstock_yesrtv.getText().toString().equals("")) {
            year_error_tv.setVisibility(View.VISIBLE);
        } else {
            year_error_tv.setVisibility(View.INVISIBLE);
        }

        if (addstock_monthtv.getText().toString().equals("")) {
            month_error_tv.setVisibility(View.VISIBLE);
        } else {
            month_error_tv.setVisibility(View.INVISIBLE);
        }


        if (maketv.getText().toString().equals("")) {
            make_error.setVisibility(View.VISIBLE);
        } else {
            make_error.setVisibility(View.INVISIBLE);
        }


        if (modelandvariant.getText().toString().equals("")) {
            modelv_error.setVisibility(View.VISIBLE);
        } else {
            modelv_error.setVisibility(View.INVISIBLE);
        }


        if (sellingptv.getText().toString().equals("")) {
            sellerprice_error.setVisibility(View.VISIBLE);
        } else {
            sellerprice_error.setVisibility(View.INVISIBLE);
        }


        if (dealerptv.getText().toString().equals("")) {
            dealerprice_error.setVisibility(View.VISIBLE);
        } else {
            dealerprice_error.setVisibility(View.INVISIBLE);
        }


        if (regtv.getText().toString().equals("")) {
            reg_error.setVisibility(View.VISIBLE);
        } else {
            reg_error.setVisibility(View.INVISIBLE);
        }

        if (colortv.getText().toString().equals("")) {
            color_error.setVisibility(View.VISIBLE);
        } else {
            color_error.setVisibility(View.INVISIBLE);
        }

        if (kmstv.getText().toString().equals("")) {
            kms_error.setVisibility(View.VISIBLE);
        } else {
            kms_error.setVisibility(View.INVISIBLE);
        }

        if (ownertv.getText().toString().equals("")) {
            owner_error.setVisibility(View.VISIBLE);
        } else {
            owner_error.setVisibility(View.INVISIBLE);
        }


        if (regcitytv.getText().toString().equals("")) {
            regcity_error.setVisibility(View.VISIBLE);
        } else {
            regcity_error.setVisibility(View.INVISIBLE);
        }


        if (regontv.getText().toString().equals("")) {
            regon_error.setVisibility(View.VISIBLE);
        } else {
            regon_error.setVisibility(View.INVISIBLE);
        }


        if (regmonthtv.getText().toString().equals("")) {
            regmonth_error.setVisibility(View.VISIBLE);
        } else {
            regmonth_error.setVisibility(View.INVISIBLE);
        }


        if (instv.getText().toString().equals("")) {
            ins_error.setVisibility(View.VISIBLE);
        } else {
            ins_error.setVisibility(View.INVISIBLE);
        }

        if (!instv.getText().toString().equalsIgnoreCase("NA")) {

            if (insexpdtv.getText().toString().equals("")) {
                insexp_error.setVisibility(View.VISIBLE);
            } else {
                insexp_error.setVisibility(View.INVISIBLE);
            }
        }


    }

    private void updateFontUI(Context ctx) {

        ArrayList<Integer> arl = new ArrayList<Integer>();

        arl.add(R.id.modelandvariant);
        arl.add(R.id.vediclelbl);
        arl.add(R.id.stock_categorylbl);
        arl.add(R.id.stockcategory_tv);
        arl.add(R.id.addstock_yesrtv);
        arl.add(R.id.addstock_monthtv);
        arl.add(R.id.regmonthtv);
        arl.add(R.id.makelbl);
        arl.add(R.id.reglbl);
        arl.add(R.id.regtv);
        arl.add(R.id.kmslbl);
        arl.add(R.id.kmstv);
        arl.add(R.id.colorlbl);
        arl.add(R.id.colortv);
        arl.add(R.id.ownerlbl);
        arl.add(R.id.ownertv);
        arl.add(R.id.regcitylbl);
        arl.add(R.id.regcitytv);
        arl.add(R.id.manufacturedonlbl);
        arl.add(R.id.makevariantlbl);
        arl.add(R.id.chassisnolbl);
        arl.add(R.id.chassisnotv);
        arl.add(R.id.enginenolbl);
        arl.add(R.id.enginenotv);
        arl.add(R.id.boughtlbl);
        arl.add(R.id.boughttv);
        arl.add(R.id.refurelbl);
        arl.add(R.id.refuretv);
        arl.add(R.id.procuretv);
        arl.add(R.id.cngkitlbl);
        arl.add(R.id.cngkittv);
        arl.add(R.id.commenttv);
        arl.add(R.id.commentlentv);
        arl.add(R.id.sellingptv);
        arl.add(R.id.dealerptv);
        arl.add(R.id.regontv);
        arl.add(R.id.instv);
        arl.add(R.id.insexpdtv);
        arl.add(R.id.dealerplbl);
        arl.add(R.id.sellingplbl);
        arl.add(R.id.insexpdlbl);
        arl.add(R.id.inslbl);
        arl.add(R.id.regonlbl);
        arl.add(R.id.vehicletv);


        setFontStyle(arl, context);
    }

    public void setFontStyle(ArrayList<Integer> tv_list, Context ctx) {

        for (int i = 0; i < tv_list.size(); i++) {
            Typeface myTypeface = Typeface.createFromAsset(this.getAssets(), "lato_semibold.ttf");
            TextView myTextView = (TextView) findViewById(tv_list.get(i));
            myTextView.setTypeface(myTypeface);
        }

    }


    public void getMake(View view) {

        WebServicesCall.webCall(this, this, jsonMake(), "EditStockGetMake", GlobalText.GET);


    }


    public void modelandvariant(View view) {


        WebServicesCall.webCall1(this, this, jsonMake(), "EditStockGetModelVariant", makeName, GlobalText.GET);

    }

    @OnClick(R.id.maketv)
    public void setMaketv(View view) {


        if (!is_certified) {
            modelandvariant.setText("");

            Log.e("Commercial", "Commercial=" + private_vehicle);
            Log.e("Commercial", "Commercial=" + vehicletv.getText().toString());


            if (vehicletv.getText().toString().equalsIgnoreCase("Commercial")) {
                CommonMethods.setvalueAgainstKey(activity, "Commercial", "true");
            } else {
                CommonMethods.setvalueAgainstKey(activity, "Commercial", "false");
            }

            MethodOfMake();
        }

    }

    public String strDate;
    public static boolean isoktorepo = false;

    public void setDate(View v) {
        //  Activity a = this;

        final Calendar myCalendar = Calendar.getInstance();


        final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
            // when dialog box is closed, below method will be called.

            public void onDateSet(DatePicker view, int selectedYear,
                                  int selectedMonth, int selectedDay) {
                if (isoktorepo) {
                    myCalendar.set(Calendar.YEAR, selectedYear);
                    myCalendar.set(Calendar.MONTH, selectedMonth);
                    myCalendar.set(Calendar.DAY_OF_MONTH, selectedDay);

                   // System.out.println();
                    String strselectedDay = String.valueOf(selectedDay);
                    if (strselectedDay.length() == 1) {
                        strselectedDay = "0" + strselectedDay;
                    }
                    String strselectedMonth = String.valueOf(selectedMonth + 1);
                    if (strselectedMonth.length() == 1) {
                        strselectedMonth = "0" + strselectedMonth;
                    }

                    // repodatebtn.setText(strselectedDay+"/"+strselectedMonth+"/"+selectedYear);
                    strDate = strselectedDay + "/" + strselectedMonth + "/"
                            + selectedYear;
                    SimpleDateFormat curFormater = new SimpleDateFormat("dd/MM/yyyy");
                    Date dateObj = null;
                    try {
                        dateObj = curFormater.parse(strDate);
                    } catch (Exception ex) {

                    }
                    SimpleDateFormat postFormater = new SimpleDateFormat("MMMM dd, yyyy");

                    postFormater = new SimpleDateFormat("yyyy-MM-dd");

                    insexpdtv.setText(postFormater.format(dateObj));

                    // WebServicesCall.webCall(activity,activity, jsonMake(postFormater.format(dateObj)), "Reshedule");

                }
                isoktorepo = false;
            }
        };

        final DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                datePickerListener, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() + 24 * 60 * 60 * 1000);

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            dialog.cancel();
                            isoktorepo = false;
                        }
                    }
                });

        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            isoktorepo = true;
                            DatePicker datePicker = datePickerDialog
                                    .getDatePicker();
                            datePickerListener.onDateSet(datePicker,
                                    datePicker.getYear(),
                                    datePicker.getMonth(),
                                    datePicker.getDayOfMonth());

                        }
                    }
                });
        datePickerDialog.setCancelable(false);
        datePickerDialog.show();
    }


    @OnClick(R.id.insexpdtv)
    public void calendarPick(View view) {
        if (!is_certified) {

            setDate(view);
        }

    }

    public String getDate() {
        Calendar c = Calendar.getInstance();
        Calendar cals = Calendar.getInstance();
        Calendar calsnextmonth = Calendar.getInstance();
        //System.out.println(" Current time => " + c.getTime());

        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        Log.e("year ", "year " + day + " : " + month + " : " + year);
        daysinmonth = c.getActualMaximum(Calendar.DAY_OF_MONTH);

        Date today = new Date();
        Date thismonth = new Date(year + "/" + (month + 1) + "/" + daysinmonth);
        Date nextmonth;
        if (month >= 11) {

            nextmonth = new Date((year + 1) + "/" + (1) + "/" + daysinmonth);
        } else {
            nextmonth = new Date(year + "/" + (month + 1) + "/" + daysinmonth);
        }

        c.setTime(today);
        cals.setTime(thismonth);
        calsnextmonth.setTime(nextmonth);
        thismondatetime = cals.getTime().getTime();
// Thu Dec 21 13:30:26 GMT+05:30 2017
//Wed Nov 29 00:00:00 GMT+05:30 2017
        currentDate = c.getTime().getTime();
        nextmondatetime = calsnextmonth.getTime().getTime();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public void ProgressDialogs() {
        pd = new ProgressDialog(this);
        pd.setTitle("Loading...");
        pd.setMessage("Please Wait...");
        pd.setCancelable(false);
        if (!pd.isShowing()) {
            pd.show();
        }
    }

    public void ProgressDialogsdismiss() {

        if (pd.isShowing()) {
            pd.dismiss();
        }

    }


    private void MethodYearLoadData() {

        if (yearList != null) {
            yearList.clear();
        }

        ProgressDialogs();

        JsonArrayRequest strReq = new JsonArrayRequest(Request.Method.GET, Global.addstockURL + "years", null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                for (int i = 0; i < response.length(); i++) {
                    try {
                        Log.e("Year ", "Year " + response.getString(i));

                        yearList.add(response.getString(i));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                ProgressDialogsdismiss();
                MethodYearListPopup(yearList);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
                try {
                    error_popup2(error.networkResponse.statusCode);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json; charset=utf-8");
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));
                Log.e("token ", "requestMethods " + CommonMethods.getstringvaluefromkey(activity, "token").toString());

                return headers;
            }
        };
        Application.getInstance().addToRequestQueue(strReq);


    }

    private void MethodOfMake() {
        String url;
        if (makeList != null) {
            makeList.clear();
        }
        ProgressDialogs();

        String month = MethodofMonth_Num(addstock_monthtv.getText().toString());// Integer.parseInt(MethodofMonth_Num(addstock_monthtv.getText().toString()));
        String year = addstock_yesrtv.getText().toString();//Integer.parseInt(addstock_yesrtv.getText().toString());

        Log.e("Commercial", "Commercial=" + CommonMethods.getstringvaluefromkey(this, "Commercial"));

        if (vehicletv.getText().toString().equalsIgnoreCase("Commercial")) {
            url = Global.addstockURL + "commercial-makelist/";
        } else {
            url = Global.addstockURL + "makelist/";
        }

        url = url + year + "/" + month;

        url = url.replace(" ", "%20");

        Log.e("Commercial", "Commercial=" + url);

        JsonArrayRequest strReq = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject data = response.getJSONObject(i);
                        makeList.add(data.getString("make"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                ProgressDialogsdismiss();
                MethodMakeListPopup(makeList);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
                try {
                    error_popup2(error.networkResponse.statusCode);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json; charset=utf-8");
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));
                Log.e("token ", "requestMethods " + CommonMethods.getstringvaluefromkey(activity, "token").toString());

                return headers;
            }
        };
        Application.getInstance().addToRequestQueue(strReq);


    }







/*    @OnClick(R.id.stockcategory_tv)
    public void stockcategoryMeth(View view) {

        String [] stockcatlist= {"Retail","OffLoad Vehicle"};

        VehicleList(stockcatlist,1);
    }*/

    @OnClick(R.id.addstock_monthtv)
    public void addstockmonthMeth(View view) {
        if (!is_certified) {
            maketv.setText("");
            modelandvariant.setText("");
            String[] stockmonthlist = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
            VehicleList(stockmonthlist, 2);
        }
    }


    @OnClick(R.id.regmonthtv)
    public void regmonthMeth(View view) {

        if (!is_certified) {
            String[] regmonthlist = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
            VehicleList(regmonthlist, 4);
        }
    }


    @OnClick(R.id.procuretv)
    public void procureExecId(View view) {
        if (!is_certified) {
            MethodProcurementLoadData();
        }

    }

    @OnClick(R.id.regcitytv)
    public void registrationCity(View view) {
        if (!is_certified) {
            MethodCityLoadData();
        }

    }

    @OnClick(R.id.colortv)
    public void colorstv(View view) {
        if (!is_certified) {
            MethodColorLoadData();

        }

    }

    @OnClick(R.id.addstock_yesrtv)
    public void year(View view) {

        if (!is_certified) {
            maketv.setText("");
            modelandvariant.setText("");
            stryr = "manufactureyear";
            MethodYearLoadData();
        }

    }

    @OnClick(R.id.regontv)
    public void yearReg(View view) {
        if (!is_certified) {
            stryr = "registeryear";
            MethodYearLoadData();
        }

    }


    private void MethodProcurementLoadData() {


        if (procurementTextList != null) {
            procurementTextList.clear();
        }

        if (procurementValueList != null) {
            procurementValueList.clear();
        }

        if (procurementTextValueList != null) {
            procurementTextValueList.clear();
        }
        ProgressDialogs();
        JsonArrayRequest strReq = new JsonArrayRequest(Request.Method.GET, Global.stock_dealerURL + "dealer/active-executive-list/procurement", null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                Log.e("MethodProcurementLoadData ", "data " + response.toString());

                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject data = response.getJSONObject(i);
                        procurementTextList.add(data.getString("Text"));
                        procurementValueList.add(data.getString("Value"));
                        procurementTextValueList.add(data.getString("Text") + "@" + data.getString("Value"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                ProgressDialogsdismiss();
                MethodProcurementListPopup(procurementTextList, procurementValueList, procurementTextValueList);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("MethodProcurementLoadData ", "error " + error.toString());

                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
                try {
                    error_popup2(error.networkResponse.statusCode);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json; charset=utf-8");
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));
                Log.e("token ", "requestMethods " + CommonMethods.getstringvaluefromkey(activity, "token").toString());

                return headers;
            }
        };
        Application.getInstance().addToRequestQueue(strReq);


    }


    private void MethodColorLoadData() {

        if (colorIdList != null) {
            colorIdList.clear();
        }

        if (colorList != null) {
            colorList.clear();
        }
        ProgressDialogs();
        JsonArrayRequest strReq = new JsonArrayRequest(Request.Method.GET, Global.addstockURL + "colour", null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {


                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject data = response.getJSONObject(i);
                        colorIdList.add(data.getString("id"));
                        colorList.add(data.getString("colour"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                ProgressDialogsdismiss();
                MethodColourListPopup(colorIdList, colorList);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
                try {
                    error_popup2(error.networkResponse.statusCode);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json; charset=utf-8");
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));
                Log.e("token ", "requestMethods " + CommonMethods.getstringvaluefromkey(activity, "token").toString());

                return headers;
            }
        };
        Application.getInstance().addToRequestQueue(strReq);


    }


    private void MethodCityLoadData() {

        if (cityList != null) {
            cityList.clear();
        }

        if (cityCode != null) {
            cityCode.clear();
        }
        ProgressDialogs();
        JsonArrayRequest strReq = new JsonArrayRequest(Request.Method.GET, Global.addstockURL + "citylist", null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                Log.e("MethodCityLoadData ", "data " + response.toString());

                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject data = response.getJSONObject(i);
                        Log.e("city ", "name " + data.getString("cityname"));
                        cityList.add(data.getString("cityname"));
                        cityCode.add(data.getString("citycode"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                ProgressDialogsdismiss();
                MethodCityListPopup(cityList, cityCode);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("MethodCityLoadData ", "error " + error.toString());
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
                try {
                    error_popup2(error.networkResponse.statusCode);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json; charset=utf-8");
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));
                Log.e("token ", "requestMethods " + CommonMethods.getstringvaluefromkey(activity, "token").toString());

                return headers;
            }
        };
        Application.getInstance().addToRequestQueue(strReq);


    }


    public static JSONObject jsonMake() {
        JSONObject jObj = new JSONObject();
        try {
            jObj.put("", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jObj;
    }

    public JSONObject jsonMakeRegularStock() {
        JSONObject jObj = new JSONObject();


        try {

            jObj.put("stock_id", stock_id);
            jObj.put("stock_source", source);
            jObj.put("posted_date", CommonMethods.getTime());
            jObj.put("vehicle_make", make);
            jObj.put("vehicle_model", model);
            jObj.put("vehicle_variant", variant);

            jObj.put("reg_month", MethodofMonth_Num(reg_month));

            jObj.put("reg_year", reg_year);
            jObj.put("registraion_city", registraionCity);
            jObj.put("registration_number", registraionNo);
            jObj.put("colour", colortv.getText().toString());
            jObj.put("kilometer", kmstv.getText().toString());
            jObj.put("owner", ownertv.getText().toString());

            jObj.put("insurance", insurance);

            if (insurance.equalsIgnoreCase("NA")) {
                jObj.put("insurance_exp_date", "");
            } else {
                jObj.put("insurance_exp_date", insuranceExp_date);
            }

            jObj.put("selling_price", selling_price);
            jObj.put("dealer_code", CommonMethods.getstringvaluefromkey(this, "dealer_code"));
            jObj.put("is_display", is_display);
            jObj.put("bought_price", bought);
            jObj.put("refurbishment_cost", refurbishment_cost);
            jObj.put("procurement_executive_id", procurement_executive_id);
            jObj.put("procurement_executive_name", procuretv.getText().toString());
            jObj.put("cng_kit", cng_kit);

            jObj.put("chassis_number", chassis_number);

            jObj.put("engine_number", engine_number);


            if (vehicletv.getText().toString().equalsIgnoreCase("Commercial")) {
                private_vehicle = "false";
            } else {
                private_vehicle = "true";
            }
            jObj.put("private_vehicle", private_vehicle);

            jObj.put("comments", comments);
            jObj.put("manufacture_month", MethodofMonth_Num(manufacture_month));
            jObj.put("dealer_price", dealer_price);
            jObj.put("is_offload", is_offload);
            jObj.put("manufacture_year", manufacture_year);

            if (is_featured_car == null || is_featured_car.equalsIgnoreCase("null")) {
                jObj.put("is_featured_car", "false");
            } else {
                jObj.put("is_featured_car", is_featured_car);
            }

            jObj.put("created_by_device", "MOBILE");


            if (is_booked == null || is_booked.equalsIgnoreCase("null")) {
                jObj.put("is_booked", "false");
            } else {
                jObj.put("is_booked", is_booked);
            }

            if (actual_selling_price == null || actual_selling_price.equalsIgnoreCase("null")) {
                jObj.put("actual_selling_price", selling_price);
            } else {
                jObj.put("actual_selling_price", actual_selling_price);
            }


            if (sales_executive_id == null || sales_executive_id.equalsIgnoreCase("null")) {
                jObj.put("sales_executive_id", 0);
            } else {
                jObj.put("sales_executive_id", sales_executive_id);
            }

            if (finance_required == null || finance_required.equalsIgnoreCase("null")) {
                jObj.put("finance_required", "false");
            } else {
                jObj.put("finance_required", finance_required);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.e("CreatedStock", " PostJson" + jObj.toString());

        return jObj;

    }


    private void selectImage() {

        make = maketv.getText().toString();
        model = model;
        variant = variant;
        registraionCity = regcitytv.getText().toString();
        registraionNo = regtv.getText().toString();
        reg_year = regontv.getText().toString();
        colour = colortv.getText().toString();
        is_certified = is_certified;
        owner = ownertv.getText().toString();
        kms = kmstv.getText().toString();
        certifiedNo = certifiedNo;
        selling_price = sellingptv.getText().toString();
        manufacture_month = addstock_monthtv.getText().toString();
        insurance = instv.getText().toString();
        if (insexpdtv.getText().toString().equalsIgnoreCase("")) {
            insuranceExp_date = "NA";
        } else {
            insuranceExp_date = insexpdtv.getText().toString();
        }
        selling_price = sellingptv.getText().toString();
        source = source;
        warranty = warranty;
        surveyor_remark = surveyor_remark;
        photo_count = photo_count;
        reg_month = regmonthtv.getText().toString();
        fuel_type = fuel_type;
        bought = boughttv.getText().toString();
        refurbishment_cost = refuretv.getText().toString();
        dealer_price = dealerptv.getText().toString();
        procurement_executive_id = procurement_executive_id;
        cng_kit = cngkittv.getText().toString();
        chassis_number = chassisnotv.getText().toString();
        engine_number = enginenotv.getText().toString();
        is_certified = is_certified;
        is_featured_car = is_featured_car;
        is_featured_car_admin = is_featured_car_admin;
        procurement_executive_name = procuretv.getText().toString();

        if (vehicletv.getText().toString().equalsIgnoreCase("Private")) {
            private_vehicle = "true";
        } else {
            private_vehicle = "false";
        }

        comments = editcmts.getText().toString();
        finance_required = finance_required;
        manufacture_month = addstock_monthtv.getText().toString();
        sales_executive_id = sales_executive_id;
        sales_executive_name = sales_executive_name;
        manufacture_year = addstock_yesrtv.getText().toString();
        actual_selling_price = actual_selling_price;
        CertifiedText = CertifiedText;
        TransmissionType = TransmissionType;
        colour = colortv.getText().toString();
        dealer_code = dealer_code;

     /*   if (is_offload.equals("Retail")) {
            is_offload = "0";
        } else {
            is_offload = "1";
        }*/

        posted_date = posted_date;
        stock_id = stock_id;
        is_booked = is_booked;

        Log.e("source", "source==" + source);
        Log.e("stockcategory_tv", "stockcategory_tv==" + stockcategory_tv.getText().toString());


        if (stockcategory_tv.getText().toString().equalsIgnoreCase("OffLoad Vehicle")
                && source.equalsIgnoreCase("C2C")
                ) {

            CommonMethods.alertMessage(activity, "Stock category:Offload", "is invalid for C2C stocks");

        } else {
            WebServicesCall.webCall(EditStockActivity.this, EditStockActivity.this, jsonMakeRegularStock(), "EditStock", GlobalText.PUT);

        }


    }


    private void VehicleList(String[] vehlist, int flag) {

        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        TextView alertdialog_edittext = (EditText) dialog_data.findViewById(R.id.alertdialog_edittext);
        ImageView searchClose = dialog_data.findViewById(R.id.searchClose);

        searchtittle.setText("Month List");
        alertdialog_edittext.setHint("Search Month");

        searchClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertdialog_edittext.setText("");
            }
        });


        //month back button

        ImageView dialog_back_btn = dialog_data.findViewById(R.id.backicon);
        dialog_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_data.dismiss();
                Log.e("test error", "resee");
                dialog_data.cancel();
            }
        });
        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }

            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));

        final ArrayAdapter<String> adapter;
        Calendar now = Calendar.getInstance();
        Log.e("Current Year is : ", now.get(Calendar.YEAR) + "");
        Log.e("selectYear is : ", addstock_yesrtv.getText().toString().trim());

        if (addstock_yesrtv.getText().toString().trim().equals(now.get(Calendar.YEAR) + "") && flag == 2) {
            String[] myStringArray = new String[(now.get(Calendar.MONTH) + 1)];
            for (int i = 0; i < myStringArray.length; i++) {
                for (int j = 0; j < vehlist.length; j++) {
                    myStringArray[i] = vehlist[i];
                }
            }


            adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, myStringArray);
        } else {
            if (regontv.getText().toString().trim().equals(now.get(Calendar.YEAR) + "") && flag == 4) {
                String[] myStringArray = new String[(now.get(Calendar.MONTH) + 1)];
                for (int i = 0; i < myStringArray.length; i++) {
                    for (int j = 0; j < vehlist.length; j++) {
                        myStringArray[i] = vehlist[i];
                    }
                }
                adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, myStringArray);
            } else {
                adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, vehlist);
            }

        }
        // final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, vehlist);
        alertdialog_Listview.setAdapter(adapter);
        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                String vehli = a.getAdapter().getItem(position).toString();
                if (flag == 0) {
                    vehicletv.setText(vehli);
                }
                if (flag == 1) {
                    if (vehli.equals("Retail")) {
                        is_offload = "0";
                    } else {
                        is_offload = "1";
                    }
                    stockcategory_tv.setText(vehli);

                }
                if (flag == 2) {


                    addstock_monthtv.setText(vehli);


                }
                if (flag == 3) {
                    ownertv.setText(vehli);
                }
                if (flag == 4) {

                    String i = MethodofMonth_Num(addstock_monthtv.getText().toString());
                    String j = MethodofMonth_Num(vehli.toString());
                    int Mmonth = Integer.parseInt(i.toString());
                    int Rmonth = Integer.parseInt(j.toString());

                    int myear = Integer.parseInt(addstock_yesrtv.getText().toString());
                    int ryear = Integer.parseInt(regontv.getText().toString());

                    Log.e("Mmonth", "Mmonth" + Mmonth);
                    Log.e("Rmonth", "Rmonth" + Mmonth);
                    Log.e("Mmonth", "myear" + Mmonth);
                    Log.e("Mmonth", "ryear" + Mmonth);

                    if (Mmonth > Rmonth) {
                        Log.e("enter-1", "enter-1");

                        if (myear < ryear) {
                            Log.e("enter-1", "enter-2");

                            regmonthtv.setText(vehli);

                        } else {
                            Log.e("enter-1", "enter-3");

                            regmonthtv.setText("");
                            Toast.makeText(EditStockActivity.this, "Registration month cannot be before manufacturing month.", Toast.LENGTH_LONG).show();

                        }

                    } else {
                        regmonthtv.setText(vehli);
                    }


                }
                if (flag == 5) {
                    instv.setText(vehli);
                }
                if (flag == 6) {
                    cngkittv.setText(vehli);
                }
                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });


        dialog_data.show();


    }


    public void Citylist(final String[] arrVal) {

        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

   /*     TextView alertdialog_textview = (TextView) dialog_data.findViewById(R.id.alertdialog_textview);
        alertdialog_textview.setText("City List");
        alertdialog_textview.setHint("search city");*/

        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }

            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, arrVal);
        alertdialog_Listview.setAdapter(adapter);
        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                String city = a.getAdapter().getItem(position).toString();

                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
                // Toast.makeText(activity, "  " + city , Toast.LENGTH_SHORT).show();

        /*        if(city.startsWith("-")){

                    vd_city_tv.setText("");
                    Toast.makeText(this,"Please Select Correct City...",Toast.LENGTH_SHORT).show();

                }else {
                    vd_city_tv.setText(city.toString());
                }
*/
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });


        dialog_data.show();
    }


    public void MethodProcurementListPopup(final ArrayList procurementTextList, final ArrayList procurementValueList, final ArrayList procurementTextValueList) {


        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        TextView alertdialog_edittext = (EditText) dialog_data.findViewById(R.id.alertdialog_edittext);
        ImageView searchClose = dialog_data.findViewById(R.id.searchClose);

        searchtittle.setText("Procurement Executive List");
        alertdialog_edittext.setHint("Search Proc Executive");


        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);



   /*     TextView alertdialog_textview = (TextView) dialog_data.findViewById(R.id.alertdialog_textview);
        alertdialog_textview.setText("City List");
        alertdialog_textview.setHint("search city");*/

        //procurement back button

        ImageView dialog_back_btn = dialog_data.findViewById(R.id.backicon);
        dialog_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_data.dismiss();
                Log.e("test error", "resee");
                dialog_data.cancel();
            }
        });


        searchClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertdialog_edittext.setText("");
            }
        });

        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }

            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, procurementTextList);
        alertdialog_Listview.setAdapter(adapter);
        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                String procureName = a.getAdapter().getItem(position).toString();

                procurement_executive_id = procurementValueList.get(position).toString();

                Log.e("createprocureExecId ", "createprocureExecId= " + procurement_executive_id);

                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                procuretv.setText(procureName);

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });


        dialog_data.show();
    }


    public void MethodYearListPopup(final ArrayList yearList) {


        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        TextView alertdialog_edittext = (EditText) dialog_data.findViewById(R.id.alertdialog_edittext);
        ImageView searchClose = dialog_data.findViewById(R.id.searchClose);
        searchtittle.setText("Year List");
        alertdialog_edittext.setHint("Search Year");

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

        //manuf no back button

        searchClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertdialog_edittext.setText("");
            }
        });

        ImageView dialog_back_btn = dialog_data.findViewById(R.id.backicon);
        dialog_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_data.dismiss();
                Log.e("test error", "resee");
                dialog_data.cancel();
            }
        });
        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }

            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, yearList);
        alertdialog_Listview.setAdapter(adapter);
        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                yearName = a.getAdapter().getItem(position).toString();

                Log.e("yearName ", "yearName " + yearName);


                TextView textview = v.findViewById(android.R.id.text1);
                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);


                if (stryr.equals("manufactureyear")) {
                    addstock_yesrtv.setText(yearName);
                } else {
                    int myear = Integer.parseInt(addstock_yesrtv.getText().toString());
                    int ryear = Integer.parseInt(yearName.toString());

                    if (myear > ryear) {
                        regontv.setText("");
                        Toast.makeText(EditStockActivity.this, "Registration year cannot be before manufacturing year.", Toast.LENGTH_LONG).show();

                    } else {
                        regontv.setText(yearName);
                    }

                }


                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });


        dialog_data.show();
    }


    public void MethodMakeListPopup(final ArrayList makeList) {


        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        TextView alertdialog_edittext = (EditText) dialog_data.findViewById(R.id.alertdialog_edittext);

        ImageView searchClose = dialog_data.findViewById(R.id.searchClose);
        searchtittle.setText("Make List");
        alertdialog_edittext.setHint("Search Make");

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

        //make back button

        searchClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertdialog_edittext.setText("");
            }
        });

        ImageView dialog_back_btn = dialog_data.findViewById(R.id.backicon);
        dialog_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_data.dismiss();
                Log.e("test error", "resee");
                dialog_data.cancel();
            }
        });

        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }

            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, makeList);
        alertdialog_Listview.setAdapter(adapter);
        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                makeName = a.getAdapter().getItem(position).toString();

                Log.e("cityName ", "cityName " + cityName);


                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                maketv.setText(makeName);

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });


        dialog_data.show();
    }


    public void MethodColourListPopup(final ArrayList colorIdList, final ArrayList colorList) {


        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        TextView alertdialog_edittext = (EditText) dialog_data.findViewById(R.id.alertdialog_edittext);
        ImageView searchClose = dialog_data.findViewById(R.id.searchClose);
        searchtittle.setText("Color List");
        alertdialog_edittext.setHint("Search Color");


        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

        //city back button

        searchClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertdialog_edittext.setText("");
            }
        });

        ImageView dialog_back_btn = dialog_data.findViewById(R.id.backicon);
        dialog_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_data.dismiss();
                Log.e("test error", "resee");
                dialog_data.cancel();
            }
        });

        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }

            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, colorList);
        alertdialog_Listview.setAdapter(adapter);
        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                colourName = a.getAdapter().getItem(position).toString();

                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                colortv.setText(colourName);

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });


        dialog_data.show();
    }


    public void MethodCityListPopup(final ArrayList citylist, final ArrayList citycode) {

        Log.e("citycode ", "citycode " + citycode.toString());

        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        TextView alertdialog_edittext = (EditText) dialog_data.findViewById(R.id.alertdialog_edittext);
        ImageView searchClose = dialog_data.findViewById(R.id.searchClose);

        searchtittle.setText("City List");
        alertdialog_edittext.setHint("Search City");


        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

   /*     TextView alertdialog_textview = (TextView) dialog_data.findViewById(R.id.alertdialog_textview);
        alertdialog_textview.setText("City List");
        alertdialog_textview.setHint("search city");*/

//reg city back button

        searchClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertdialog_edittext.setText("");
            }
        });

        ImageView dialog_back_btn = dialog_data.findViewById(R.id.backicon);
        dialog_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_data.dismiss();
                Log.e("test error", "resee");
                dialog_data.cancel();
            }
        });

        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }

            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, citylist);
        alertdialog_Listview.setAdapter(adapter);

        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                cityName = a.getAdapter().getItem(position).toString();

                Log.e("cityName ", "cityName " + cityName);


                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                regcitytv.setText(cityName);

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });


        dialog_data.show();
    }


    public static void Parsegetcity(JSONObject jObj, String strMethod) {

        Log.e("Parsegetcity ", "Parsegetcity " + jObj.toString());

    }

    public static void Parsegetmodalvariant(JSONObject jObj, String strMethod, Activity a) {

        Log.e("Parsegetmodalvariant ", "Parsegetmodalvariant " + jObj.toString());

        if (modalVariant != null) {
            modalVariant.clear();
        }
        if (modelArr != null) {
            modelArr.clear();
        }
        if (variantArr != null) {
            variantArr.clear();
        }

        if (modelhm != null) {
            modelhm.clear();
        }
        if (varianthm != null) {
            varianthm.clear();
        }
        try {
            JSONArray array = jObj.getJSONArray("model_values");
            for (int i = 0; i < array.length(); i++) {
                try {
                    JSONObject data = array.getJSONObject(i);
                    modalVariant.add(data.getString("display"));
                    modelArr.add(data.getString("model"));
                    variantArr.add(data.getString("variant"));

                    modelhm.put(data.getString("display"), data.getString("model"));
                    varianthm.put(data.getString("display"), data.getString("variant"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            MethodCityListPopup(modalVariant, modelArr, variantArr, a);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private static void MethodCityListPopup(ArrayList<String> modalVariant, ArrayList<String> modelarr, ArrayList<String> variantarr, Activity activity) {

        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

        ImageView searchClose = dialog_data.findViewById(R.id.searchClose);

   /*     TextView alertdialog_textview = (TextView) dialog_data.findViewById(R.id.alertdialog_textview);
        alertdialog_textview.setText("City List");
        alertdialog_textview.setHint("search city");*/

        //reena back button


        ImageView dialog_back_btn = dialog_data.findViewById(R.id.backicon);
        dialog_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_data.dismiss();
                Log.e("test error", "resee");
                dialog_data.cancel();
            }
        });

        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }

            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);

        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, modalVariant);
        alertdialog_Listview.setAdapter(adapter);

        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                String models = "";
                String variants = "";

                String modVar = a.getAdapter().getItem(position).toString();

                for (Map.Entry<String, String> entry : modelhm.entrySet()) {
                    if (modVar.equalsIgnoreCase(entry.getKey())) {
                        models = entry.getValue();
                    }
                }
                for (Map.Entry<String, String> entry : varianthm.entrySet()) {
                    if (modVar.equalsIgnoreCase(entry.getKey())) {
                        variants = entry.getValue();
                    }
                }

                Log.e("modVar", "modVar=" + modVar);
                Log.e("model", "model=" + models);
                Log.e("variant", "variant=" + variants.trim());

                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                model = models;
                variant = variants.trim();

                modelandvariant.setText(modVar);

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);

            }
        });

        dialog_data.show();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Application.getInstance().trackScreenView(activity,GlobalText.EditStockActivity);

    }

    public static void Parseaddregularstock(JSONObject jObj, String strMethod, Activity activity) {

        Log.e("CreatedStock ", "Parseaddregularstock " + jObj.toString());

        try {
            if (jObj.getString("status").equalsIgnoreCase("FAILURE")) {
                Toast.makeText(activity, " " + jObj.getString("Message"), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(activity, " " + jObj.getString("Message"), Toast.LENGTH_LONG).show();
                Intent in_main = new Intent(activity, AddStockSucessActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("STCKID", jObj.getString("Message"));
                in_main.putExtras(bundle);
                activity.startActivity(in_main);
                activity.finish();

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public String MethodofMonth_Num(String month) {

        if (month.equalsIgnoreCase("January") || month.equalsIgnoreCase("Jan")) {
            month = "1";
            return month;
        }


        if (month.equalsIgnoreCase("February") || month.equalsIgnoreCase("Feb")) {
            month = "2";
            return month;

        }

        if (month.equalsIgnoreCase("March") || month.equalsIgnoreCase("Mar")) {
            month = "3";
            return month;

        }

        if (month.equalsIgnoreCase("April") || month.equalsIgnoreCase("Apr")) {
            month = "4";
            return month;

        }


        if (month.equalsIgnoreCase("May") || month.equalsIgnoreCase("May")) {
            month = "5";
            return month;

        }

        if (month.equalsIgnoreCase("June") || month.equalsIgnoreCase("Jun")) {
            month = "6";
            return month;

        }

        if (month.equalsIgnoreCase("July") || month.equalsIgnoreCase("Jul")) {
            month = "7";
            return month;

        }

        if (month.equalsIgnoreCase("August") || month.equalsIgnoreCase("Aug")) {
            month = "8";
            return month;

        }


        if (month.equalsIgnoreCase("September") || month.equalsIgnoreCase("Sep")) {
            month = "9";
            return month;

        }

        if (month.equalsIgnoreCase("October") || month.equalsIgnoreCase("Oct")) {
            month = "10";
            return month;

        }

        if (month.equalsIgnoreCase("November") || month.equalsIgnoreCase("Nov")) {
            month = "11";
            return month;

        }

        if (month.equalsIgnoreCase("December") || month.equalsIgnoreCase("Dec")) {
            month = "12";
            return month;

        }

        return month;

    }


    public static void Parsegetmake(JSONObject jObj, String strMethod, Activity activity) {
    }


    private static void setAlertDialog(View v, Activity a, final String strTitle, final String[] arrVal) {

        AlertDialog.Builder alert = new AlertDialog.Builder(a);
        alert.setTitle(strTitle);
        alert.setItems(arrVal, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (strTitle.equals("Vehicle")) {
                    vehicletv.setText(arrVal[which]);
                } else if (strTitle.equals("Stock Category")) {
                    stockcategory_tv.setText(arrVal[which]);


                    if (arrVal[which].equalsIgnoreCase("Retail")) {
                        is_offload = "false";
                    } else {
                        is_offload = "true";
                    }


                } else if (strTitle.equals("Owner")) {

                    ownertv.setText(arrVal[which]);
                } else if (strTitle.equals("Insurance")) {

                    instv.setText(arrVal[which]);
                    if (instv.getText().toString().equalsIgnoreCase("NA")) {

                        insexpdlbl.setVisibility(View.INVISIBLE);
                        insexpdtv.setVisibility(View.INVISIBLE);
                        insexp_error.setVisibility(View.INVISIBLE);
                        insExline.setVisibility(View.INVISIBLE);
                    } else {
                        insexpdlbl.setVisibility(View.VISIBLE);
                        insexpdtv.setVisibility(View.VISIBLE);
                        insExline.setVisibility(View.VISIBLE);
                    }


                } else if (strTitle.equals("CNG KIT")) {
                    cngkittv.setText(arrVal[which]);
                }
            }


        });
        alert.create();
        alert.show();

    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    public static void Parseeditstock(JSONObject jObj, String strMethod) {
        try {


            if (jObj.get("status").equals("SUCCESS")) {
                alert(jObj.getString("Message").toString(), jObj.getString("status").toString());
            } else {
                alert(jObj.getString("Message").toString(), jObj.getString("status").toString());
            }

        } catch (Exception ex) {
            Log.e("Parseseditstock-Ex", ex.toString());
        }


    }


    public static void alert(String strMsg, String str) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.setContentView(R.layout.common_alert);
        //dialog.setTitle("Custom Dialog");
        dialog.setCancelable(false);

        TextView Message, cancel, Confirm;
        Message = dialog.findViewById(R.id.Message);
        Confirm = dialog.findViewById(R.id.Confirm);

        Message.setText(strMsg);

        Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                if (str.equalsIgnoreCase("SUCCESS")) {
                    if (!stockcategory_tv.getText().toString().equalsIgnoreCase("OffLoad Vehicle")) {

                        Intent intent = new Intent(activity, StockStoreInfoActivity.class);
                        intent.putExtra("make", make);
                        intent.putExtra("model", model);
                        intent.putExtra("variant", variant);
                        intent.putExtra("registraionCity", registraionCity);
                        intent.putExtra("registraionNo", registraionNo);
                        intent.putExtra("color", colour);
                        intent.putExtra("certified", is_certified);
                        intent.putExtra("owner", owner);
                        intent.putExtra("kms", kms);
                        intent.putExtra("Regyear", reg_year);
                        intent.putExtra("certifiedNo", certifiedNo);
                        intent.putExtra("sellingPrice", selling_price);
                        intent.putExtra("yearMonth", manufacture_month);
                        intent.putExtra("insurance", insurance);
                        intent.putExtra("insuranceExp_date", insuranceExp_date);
                        intent.putExtra("selling_price", selling_price);
                        intent.putExtra("source", source);
                        intent.putExtra("warranty", warranty);
                        intent.putExtra("surveyor_remark", surveyor_remark);
                        intent.putExtra("photo_count", photo_count);
                        intent.putExtra("reg_month", reg_month);
                        intent.putExtra("fuel_type", fuel_type);
                        intent.putExtra("bought", bought);
                        intent.putExtra("refurbishment_cost", refurbishment_cost);
                        intent.putExtra("dealer_price", dealer_price);
                        intent.putExtra("ProcurementExecId", procurement_executive_id);
                        intent.putExtra("cng_kit", cng_kit);
                        intent.putExtra("chassis_number", chassis_number);

                        intent.putExtra("engine_number", engine_number);

                        intent.putExtra("is_certified", is_certified);
                        intent.putExtra("is_featured_car", is_featured_car);
                        intent.putExtra("is_featured_car_admin", is_featured_car_admin);
                        intent.putExtra("procurement_executive_name", procurement_executive_name);
                        if (vehicletv.getText().toString().equalsIgnoreCase("Private")) {
                            intent.putExtra("private_vehicle", "1");
                        } else {
                            intent.putExtra("private_vehicle", "0");
                        }
                        intent.putExtra("comments", comments);
                        intent.putExtra("finance_required", finance_required);
                        intent.putExtra("manufacture_month", manufacture_month);
                        intent.putExtra("sales_executive_id", sales_executive_id);
                        intent.putExtra("sales_executive_name", sales_executive_name);
                        intent.putExtra("manufacture_year", manufacture_year);
                        intent.putExtra("actual_selling_price", actual_selling_price);
                        intent.putExtra("CertifiedText", CertifiedText);
                        //intent.putExtra("IsDisplay",IsDisplay);
                        intent.putExtra("TransmissionType", TransmissionType);
                        intent.putExtra("colour", colour);
                        intent.putExtra("dealer_code", dealer_code);
                        intent.putExtra("is_offload", is_offload);
                        intent.putExtra("posted_date", posted_date);
                        intent.putExtra("stock_id", stock_id);
                        intent.putExtra("is_booked", is_booked);


                        intent.putExtra("imageurl", imageurl);
                        intent.putExtra("imagename", imagename);
                        intent.putExtra("coverimage", coverimage);

                        activity.startActivity(intent);
                        activity.finish();

                    } else {
                        // StockFragment.onBack = false;
                        Intent intent = new Intent(activity, MainActivity.class);
                        activity.startActivity(intent);
                        activity.finish();

                    }
                }
            }
        });

        dialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

    }


    public static void error_popup2(int str) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        dialog.setContentView(R.layout.alert_error);
        TextView Message, Confirm;
        Message = dialog.findViewById(R.id.Message);
        Confirm = dialog.findViewById(R.id.ok_error);
        dialog.setCancelable(false);
        String strI = Integer.toString(str);
        Message.setText(GlobalText.AUTH_ERROR);

        Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainActivity.logOut();
                dialog.dismiss();

            }
        });

        dialog.show();
    }

}
