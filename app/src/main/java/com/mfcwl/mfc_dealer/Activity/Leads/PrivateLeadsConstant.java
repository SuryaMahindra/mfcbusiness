package com.mfcwl.mfc_dealer.Activity.Leads;

public class PrivateLeadsConstant {
    private static PrivateLeadsConstant mInstance = null;

    public String getWhichleads() {
        return whichleads;
    }

    public void setWhichleads(String whichleads) {
        this.whichleads = whichleads;
    }

    private String whichleads;

    private PrivateLeadsConstant() {
        whichleads = "";

    }

    public static PrivateLeadsConstant getInstance() {
        if (mInstance == null) {
            mInstance = new PrivateLeadsConstant();
        }
        return mInstance;
    }
}
