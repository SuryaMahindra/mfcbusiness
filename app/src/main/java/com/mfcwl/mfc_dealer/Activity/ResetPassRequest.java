package com.mfcwl.mfc_dealer.Activity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResetPassRequest {
    @SerializedName("User_Name")
    @Expose
    private String userId;
    @SerializedName("Old_Password")
    @Expose
    private String oldPassword;
    @SerializedName("New_Password")
    @Expose
    private String newPassword;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

}
