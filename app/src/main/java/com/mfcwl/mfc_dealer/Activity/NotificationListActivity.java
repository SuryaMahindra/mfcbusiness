package com.mfcwl.mfc_dealer.Activity;

import android.app.Activity;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.Adapter.NotificationListAdapter;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.ButterKnife;


public class NotificationListActivity extends AppCompatActivity {

    //@BindView(R.id.notification_recycler)
    public static RecyclerView notification_recycler;

    public static Activity activity;
    public static TextView nonotification;
    public static int myNum = 0;

    public static ArrayList<String> NotificationNames;
    public static ArrayList<String> datetime;
    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_list);
        Log.i(TAG, "onCreate: ");
        notification_recycler = (RecyclerView) findViewById(R.id.notification_recycler);
        nonotification = (TextView) findViewById(R.id.nonotification);
        nonotification.setVisibility(View.GONE);
        activity = this;
        ButterKnife.bind(this);
        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Log.i(TAG, "onCreate: ");
        NotificationNames = new ArrayList<>();
        datetime = new ArrayList<>();

        WebServicesCall.webCall(activity, activity, jsonMakeNotification(), "Notification_2", GlobalText.POST);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item, menu);
        MenuItem item = menu.findItem(R.id.share);
        item.setVisible(false);
        item = menu.findItem(R.id.add_image);
        item.setVisible(false);
        item = menu.findItem(R.id.delete_fea);
        item.setVisible(false);
        item = menu.findItem(R.id.stock_fil_clear);
        item.setVisible(false);
        item = menu.findItem(R.id.notification_bell);
        item.setVisible(false);
        item = menu.findItem(R.id.notification_cancel);
        item.setVisible(true);

        item = menu.findItem(R.id.asmHome);
        item.setVisible(false);

        item = menu.findItem(R.id.asm_mail);
        item.setVisible(false);

        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        // put your code here...

        Application.getInstance().trackScreenView(activity,GlobalText.Notification_list_Activity);

    }

    public static JSONObject jsonMakeNotification() {
        JSONObject jObj = new JSONObject();
        try {
            jObj.put("fromdate",CalendarTodayDate());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("jsonMake1 ", "jsonMake1" + jObj.toString());

        return jObj;
    }

    public static String lastmonth = "";

    public static String CalendarTodayDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -30);

        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        lastmonth = s.format(new Date(cal.getTimeInMillis()));
        return lastmonth;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (item.getItemId()) {
            case R.id.notification_cancel:

                finish();
            case android.R.id.home:

                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static void ParseNotification(JSONObject jObj, String strMethod) {

        Log.e("ParseNotification", "ParseNotification=" + jObj.toString());

        try {

            JSONObject json = new JSONObject(jObj.toString());    // create JSON obj from string
            JSONObject data = json.getJSONObject("data");

            if (data.isNull("AgeingStock")) {
                Log.e("AgeingStock", "null");
                CommonMethods.setvalueAgainstKey(activity, "nofication_status", "false");

                if (CommonMethods.getstringvaluefromkey(activity, "AgeingStockTrue").equalsIgnoreCase("true")
                        && !CommonMethods.getstringvaluefromkey(activity, "AgeingInventoryTime").toString().equalsIgnoreCase("")) {
                    NotificationNames.add(CommonMethods.getstringvaluefromkey(activity, "AgeingStockMessage").toString());

                    String date = Dateget_2(CommonMethods.getstringvaluefromkey(activity, "AgeingInventoryTime").toString());
                    if (!CommonMethods.getstringvaluefromkey(activity, "AgeingInventoryTime").equalsIgnoreCase("")) {

                        date = Dateget_2(CommonMethods.getstringvaluefromkey(activity, "AgeingInventoryTime").toString());
                    }
                    datetime.add(date);

                }

            } else {
                Log.e("AgeingStock", "data");
                JSONObject AgeingStock = data.getJSONObject("AgeingStock");
                Log.e("Message", "data=" + AgeingStock.getString("Message"));
                Log.e("sent", "data=" + AgeingStock.getString("sent"));
                Log.e("SentTime", "data=" + AgeingStock.getString("SentTime"));

                String date = "";
                if (!AgeingStock.getString("SentTime").equalsIgnoreCase("")) {
                    CommonMethods.setvalueAgainstKey(activity, "AgeingStockMessage", AgeingStock.getString("Message"));
                    CommonMethods.setvalueAgainstKey(activity, "AgeingStockTrue", "true");
                    CommonMethods.setvalueAgainstKey(activity, "AgeingInventoryTime", AgeingStock.getString("SentTime"));
                    date = Dateget_2(CommonMethods.getstringvaluefromkey(activity, "AgeingInventoryTime").toString());
                    NotificationNames.add(CommonMethods.getstringvaluefromkey(activity, "AgeingStockMessage").toString());
                    datetime.add(date);
                }

               /* if (!CommonMethods.getstringvaluefromkey(activity, "AgeingInventoryTime").equalsIgnoreCase("")) {
                    NotificationNames.add(CommonMethods.getstringvaluefromkey(activity, "AgeingStockMessage").toString());
                    date = Dateget_2(AgeingStock.getString("SentTime"));
                    datetime.add(date);
                }*/


                /*if (!AgeingStock.getString("SentTime").equalsIgnoreCase("")) {

                    date = Dateget_2(AgeingStock.getString("SentTime"));
                }
                if (!date.equalsIgnoreCase("")) {
                    datetime.add(date);
                }*/

            }
            if (data.isNull("LeadFollowUp")) {
                Log.e("LeadFollowUp", "null");
                String date = "";
                CommonMethods.setvalueAgainstKey(activity, "nofication_status", "false");

                if (CommonMethods.getstringvaluefromkey(activity, "LeadFollowUpTrue").equalsIgnoreCase("true")
                        && !CommonMethods.getstringvaluefromkey(activity, "leadFollowUpTime").toString().equalsIgnoreCase("")) {

                    NotificationNames.add(CommonMethods.getstringvaluefromkey(activity, "LeadFollowUpMessage").toString());

                    date = Dateget_2(CommonMethods.getstringvaluefromkey(activity, "leadFollowUpTime").toString());
                    if (!CommonMethods.getstringvaluefromkey(activity, "leadFollowUpTime").equalsIgnoreCase("")) {

                        date = Dateget_2(CommonMethods.getstringvaluefromkey(activity, "leadFollowUpTime").toString());
                    }
                    datetime.add(date);
                }
                datetime.add(date);

            } else {
                Log.e("LeadFollowUp", "data");
                JSONObject LeadFollowUp = data.getJSONObject("LeadFollowUp");
                Log.e("Message", "data=" + LeadFollowUp.getString("Message"));
                Log.e("sent", "data=" + LeadFollowUp.getString("sent"));
                Log.e("SentTime", "data=" + LeadFollowUp.getString("SentTime"));

                CommonMethods.setvalueAgainstKey(activity, "LeadFollowUpMessage", LeadFollowUp.getString("Message"));

                String date = "";// Dateget(CommonMethods.getstringvaluefromkey(activity, "leadFollowUpTime").toString());

                if (!LeadFollowUp.getString("SentTime").equalsIgnoreCase("")) {
                    CommonMethods.setvalueAgainstKey(activity, "LeadFollowUpTrue", "true");
                    if (!LeadFollowUp.getString("SentTime").equalsIgnoreCase("")) {
                        CommonMethods.setvalueAgainstKey(activity, "leadFollowUpTime", LeadFollowUp.getString("SentTime"));
                    }
                    NotificationNames.add(CommonMethods.getstringvaluefromkey(activity, "LeadFollowUpMessage").toString());
                    date = Dateget_2(CommonMethods.getstringvaluefromkey(activity, "leadFollowUpTime").toString());

                    Log.e("leadFollowUpTime", "leadFollowUpTime=1" + date);
                }

                /*if (!LeadFollowUp.getString("SentTime").equalsIgnoreCase("")) {

                    date = Dateget_2(LeadFollowUp.getString("SentTime"));
                }
                if (!date.equalsIgnoreCase("")) {
                    datetime.add(date);
                }*/
                Log.e("leadFollowUpTime", "leadFollowUpTime=2" + date);

                datetime.add(date);

            }
            if (data.isNull("NewLead")) {
                Log.e("NewLead", "null");
                CommonMethods.setvalueAgainstKey(activity, "nofication_status", "false");

                if (CommonMethods.getstringvaluefromkey(activity, "NewLeadTrue").equalsIgnoreCase("true")
                        && !CommonMethods.getstringvaluefromkey(activity, "leadTime").toString().equalsIgnoreCase("")) {
                    NotificationNames.add(CommonMethods.getstringvaluefromkey(activity, "NewLeadMessage").toString());

                    String date = Dateget_2(CommonMethods.getstringvaluefromkey(activity, "leadTime").toString());
                    if (!CommonMethods.getstringvaluefromkey(activity, "leadTime").toString().equalsIgnoreCase("")) {

                        date = Dateget_2(CommonMethods.getstringvaluefromkey(activity, "leadTime").toString());
                    }
                    datetime.add(date);
                }

            } else {
                Log.e("NewLead", "data");
                JSONObject NewLead = data.getJSONObject("NewLead");

                Log.e("Message", "data=" + NewLead.getString("Message"));
                Log.e("sent", "data=" + NewLead.getString("sent"));
                Log.e("SentTime", "data=" + NewLead.getString("SentTime"));

                CommonMethods.setvalueAgainstKey(activity, "NewLeadMessage", NewLead.getString("Message"));

                String date = "";// Dateget(CommonMethods.getstringvaluefromkey(activity, "leadTime").toString());

                if (!NewLead.getString("SentTime").equalsIgnoreCase("")) {
                    CommonMethods.setvalueAgainstKey(activity, "NewLeadTrue", "true");

                    if (!NewLead.getString("SentTime").equalsIgnoreCase("")) {
                        CommonMethods.setvalueAgainstKey(activity, "leadTime", NewLead.getString("SentTime"));
                    }
                    NotificationNames.add(CommonMethods.getstringvaluefromkey(activity, "NewLeadMessage").toString());

                    date = Dateget_2(CommonMethods.getstringvaluefromkey(activity, "leadTime").toString());
                    datetime.add(date);
                }
                /*if (!NewLead.getString("SentTime").equalsIgnoreCase("")) {

                    date = Dateget_2(NewLead.getString("SentTime"));
                }
                if (!date.equalsIgnoreCase("")) {
                    datetime.add(date);
                }*/

            }
            if (data.isNull("ImageUploadRemainder")) {
                Log.e("ImageUploadRemainder", "null");
                CommonMethods.setvalueAgainstKey(activity, "nofication_status", "false");

                if (CommonMethods.getstringvaluefromkey(activity, "ImageUploadRemainderTrue").equalsIgnoreCase("true")
                        && !CommonMethods.getstringvaluefromkey(activity, "noStockImageTime").toString().equalsIgnoreCase("")) {

                    NotificationNames.add(CommonMethods.getstringvaluefromkey(activity, "NewLeadMessage").toString());

                    String date = Dateget_2(CommonMethods.getstringvaluefromkey(activity, "noStockImageTime").toString());

                    if (!CommonMethods.getstringvaluefromkey(activity, "noStockImageTime").equalsIgnoreCase("")) {
                        date = Dateget_2(CommonMethods.getstringvaluefromkey(activity, "noStockImageTime").toString());
                    }
                    datetime.add(date);
                }

            } else {
                Log.e("ImageUploadRemainder", "data");
                JSONObject ImageUploadRemainder = data.getJSONObject("ImageUploadRemainder");
                Log.e("Message", "data=" + ImageUploadRemainder.getString("Message"));
                Log.e("sent", "data=" + ImageUploadRemainder.getString("sent"));
                Log.e("SentTime", "data=" + ImageUploadRemainder.getString("SentTime"));

                CommonMethods.setvalueAgainstKey(activity, "NewLeadMessage", ImageUploadRemainder.getString("Message"));

                String date = "";// Dateget(CommonMethods.getstringvaluefromkey(activity, "noStockImageTime").toString());

                if (!ImageUploadRemainder.getString("SentTime").equalsIgnoreCase("")) {
                    CommonMethods.setvalueAgainstKey(activity, "ImageUploadRemainderTrue", "true");

                    NotificationNames.add(CommonMethods.getstringvaluefromkey(activity, "NewLeadMessage").toString());
                    if (!ImageUploadRemainder.getString("SentTime").equalsIgnoreCase("")) {
                        CommonMethods.setvalueAgainstKey(activity, "noStockImageTime", ImageUploadRemainder.getString("SentTime"));
                    }

                    date = Dateget_2(CommonMethods.getstringvaluefromkey(activity, "noStockImageTime").toString());

                    datetime.add(date);
                }


               /* if (!ImageUploadRemainder.getString("SentTime").equalsIgnoreCase("")) {

                    date = Dateget_2(ImageUploadRemainder.getString("SentTime"));
                }
                if (!date.equalsIgnoreCase("")) {
                    datetime.add(date);
                }

                datetime.add(date);*/
            }


            if (data.isNull("AgeingStock")
                    && data.isNull("LeadFollowUp")
                    && data.isNull("NewLead")
                    && data.isNull("ImageUploadRemainder")
                    && CommonMethods.getstringvaluefromkey(activity, "AgeingStockTrue").equalsIgnoreCase("")
                    && CommonMethods.getstringvaluefromkey(activity, "LeadFollowUpTrue").equalsIgnoreCase("")
                    && CommonMethods.getstringvaluefromkey(activity, "NewLeadTrue").equalsIgnoreCase("")
                    && CommonMethods.getstringvaluefromkey(activity, "ImageUploadRemainderTrue").equalsIgnoreCase("")

                    ) {
                nonotification.setVisibility(View.VISIBLE);

            } else {
                nonotification.setVisibility(View.GONE);

            }

            NotificationListAdapter notificationListAdapter = new NotificationListAdapter(activity, NotificationNames, datetime);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(activity);
            notification_recycler.setLayoutManager(mLayoutManager);
            notification_recycler.setItemAnimator(new DefaultItemAnimator());
            notification_recycler.setAdapter(notificationListAdapter);
            //notificationListAdapter.notifyDataSetChanged();

        } catch (JSONException e1) {
            e1.printStackTrace();
        }

    }

    public static String Dateget_2(String date) {

        // DateTimeUtils obj = new DateTimeUtils();
        //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-M-dd HH:mm:ss");
        try {

            Date date1 = simpleDateFormat.parse(date);

            simpleDateFormat = new SimpleDateFormat("yyyy-M-dd HH:mm:ss");

            Calendar c = Calendar.getInstance();
            String formattedDate = simpleDateFormat.format(c.getTime());
            Date date2 = simpleDateFormat.parse(formattedDate);

            Log.e("date1", "date1=" + date1);
            Log.e("date2", "date2=" + date2);

            //date=printDifference_2(date1, date2);


            //milliseconds
            long different = date2.getTime() - date1.getTime();

            /*System.out.println("startDate : " + date1);
            System.out.println("endDate : " + date2);
            System.out.println("different : " + different);*/

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = different / daysInMilli;
            different = different % daysInMilli;

            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;
            different = different % minutesInMilli;

            long elapsedSeconds = different / secondsInMilli;

            if (elapsedDays != 0) {

                date = elapsedDays + " " + "d ago";

            } else if (elapsedHours != 0) {
                date = elapsedHours + " " + "h ago";

            } else {

                if (elapsedMinutes != 0) {
                    date = elapsedMinutes + " " + "m ago";
                } else {
                    date = "Now";
                }

            }

            /*System.out.printf(
                    "%d days, %d hours, %d minutes, %d seconds%n",
                    elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);*/


        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }


}
