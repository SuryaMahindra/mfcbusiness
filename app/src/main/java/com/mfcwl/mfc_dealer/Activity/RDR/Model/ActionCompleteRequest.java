package com.mfcwl.mfc_dealer.Activity.RDR.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ActionCompleteRequest {

    @SerializedName("action_id")
    @Expose
    public String actionId;
    @SerializedName("action_status")
    @Expose
    public String actionStatus;

}