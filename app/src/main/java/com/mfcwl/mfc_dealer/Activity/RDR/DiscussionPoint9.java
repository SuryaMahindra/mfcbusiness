package com.mfcwl.mfc_dealer.Activity.RDR;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.DealerReportRequest;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.DiscussionPoint;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.ScreenInformation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import imageeditor.base.BaseActivity;

public class DiscussionPoint9 extends BaseActivity {


    TextView tvDateTime, tvDealerName,focus_area_dd;
    ImageView createRepo_backbtn;
    EditText  etSubFA;
    EditText etPointsDiscussed;
    EditText etactionitems;
    TextView tvActionItems, tvResponsibilities, tvTargetDateCalendar;
    TextView tvPrevious, tvNext, tvFooterHeading, tvDone;
    String strFocusArea, strSubFocusArea, strPointsDiscussed;
    private LinkedHashMap<String, DiscussionPoint> map = new LinkedHashMap();
    public int level = 1;
    private LinearLayout llactionitems;
    private DiscussionPoint discussionPoint;

    String[] focusAreas = {"Morning Meeting","Customer Complaint, if any","Stock Count","Walk-ins + Leads discussion","Sales Discussion",
            "Procurement Discussion","Finance Discussion","Warranty Penetration Discussion","Royalty + Variable Payment","Other Open Issues"};

    private Activitykiller mKiller;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.discussion_point8);
        level = 9;
        initView();
        setListeners();
        mKiller = new Activitykiller();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(mKiller,intentFilter);
    }

    public void initView() {

        createRepo_backbtn = findViewById(R.id.createRepo_backbtn);

        tvDateTime = findViewById(R.id.tvDateTime);
        tvDealerName = findViewById(R.id.tvDealer);
        focus_area_dd = findViewById(R.id.focus_area_dd);
        etSubFA = findViewById(R.id.etSubFA);
        etPointsDiscussed = findViewById(R.id.etPointsDiscussed);
        tvActionItems = findViewById(R.id.tvActionItemList);
        tvResponsibilities = findViewById(R.id.tvResponsibilityList);
        tvTargetDateCalendar = findViewById(R.id.tvTargetDateCalendar);
        llactionitems = findViewById(R.id.llactionitems);
        llactionitems.setVisibility(View.GONE);

        etactionitems = findViewById(R.id.etactionitems);

        tvPrevious = findViewById(R.id.tvPrevious8);
        tvNext = findViewById(R.id.tvNext);
        tvDone = findViewById(R.id.tvDone);
        tvFooterHeading = findViewById(R.id.tvFooterHeading);

    }

    public void setListeners() {

        tvPrevious.setOnClickListener(this);
        tvNext.setOnClickListener(this);
        tvActionItems.setOnClickListener(this);
        tvResponsibilities.setOnClickListener(this);
        tvTargetDateCalendar.setOnClickListener(this);
        tvDone.setOnClickListener(this);
        focus_area_dd.setOnClickListener(this);
        etSubFA.setOnClickListener(this);
        createRepo_backbtn.setOnClickListener(this);

        tvDateTime.setText(getDateTime());
        tvDealerName.setText(dealerName + "(" + dealerId + ")");
        tvFooterHeading.setText("Discussion point 9");
        etPointsDiscussed.setText("");


    }

    public DiscussionPoint getDiscussionPoint() {
        discussionPoint = new DiscussionPoint();
        discussionPoint.setActionItem(Arrays.asList(etactionitems.getText().toString()));
        discussionPoint.setResponsibility(tvResponsibilities.getText().toString());
        discussionPoint.setTargetDate(tvTargetDateCalendar.getText().toString());
        discussionPoint.setFocusArea(focus_area_dd.getText().toString());
        discussionPoint.setSubFocusArea(etSubFA.getText().toString());

        ScreenInformation info = new ScreenInformation();
        info.setPointsDiscussed(etPointsDiscussed.getText().toString());
        discussionPoint.setScreenInformation(info);


        return discussionPoint;
    }

    private boolean validate() {

        strActionItem = etactionitems.getText().toString();
        strResponsibility = tvResponsibilities.getText().toString();
        strTargetDate = tvTargetDateCalendar.getText().toString();

        strFocusArea = focus_area_dd.getText().toString();
        strSubFocusArea = etSubFA.getText().toString();
        strPointsDiscussed = etPointsDiscussed.getText().toString();

        boolean allSelected = true;

        if (strActionItem.equals("") ||
                strResponsibility.equals("") ||
                strTargetDate.equals("") ||
                strFocusArea.equals("") ||
                strSubFocusArea.equals("") ||
                strPointsDiscussed.equals("")) {
            allSelected = false;
            Toast.makeText(this, "Please select all fields", Toast.LENGTH_SHORT).show();
        }

        return allSelected;
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.createRepo_backbtn:
                startActivity(new Intent(this, DiscussionPoint8.class));
                break;
            case R.id.tvNext:
                    if (validate()) {
                        map.put(String.valueOf(level), getDiscussionPoint());
                        resetData();
                        DiscussionPoint dp = map.get(String.valueOf(level));
                        if (dp != null){
                            preFillData(level);
                        }

                    }
                break;

            case R.id.tvPrevious8:
              if(level == 9){
                  startActivity(new Intent(DiscussionPoint9.this,DiscussionPoint8.class));
              }else {
                  preFillData(level - 1);
                  level--;
              }
                break;

            case R.id.tvActionItemList:
                if (dealerReportResponse != null)
                    setDialogMultiSelectionCheck(R.id.tvActionItemList, "Action Items", getStringArray(dealerReportResponse.stocksInProgress));
                break;

            case R.id.focus_area_dd:
                    setListDialog(focus_area_dd,"Focus Area",focusAreas);
                break;

            case R.id.tvDone:
                if (validate()) {
                    map.put(String.valueOf(level),getDiscussionPoint());
                    getMapData();
                    startActivity(new Intent(DiscussionPoint9.this,DiscussionPoint10.class));

                    // submitToWeb(getRequest());
                }
                break;
        }
    }

    private void getMapData(){
        List<DiscussionPoint> list = new ArrayList<>() ;
        list.clear();
        for(Map.Entry<String, DiscussionPoint> entry : map.entrySet()) {
            DiscussionPoint point = entry.getValue();
            list.add(point);
        }
        discussionPoints.addAll(list);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       unregisterReceiver(mKiller);
    }

    private void resetData() {

        strFocusArea = "";
        strSubFocusArea = "";
        strPointsDiscussed = "";
        strTargetDate = "";
        strResponsibility = "";
        strActionItem = "";
        discussionPoint = null;
        etactionitems.setFocusableInTouchMode(false);
        etactionitems.clearFocus();
        etactionitems.setFocusableInTouchMode(true);

        etSubFA.setText("");
        focus_area_dd.setText("");
        etPointsDiscussed.setText("");
        etactionitems.setText("");
        tvResponsibilities.setText("");
        tvTargetDateCalendar.setText("");
        level = level + 1;
        tvFooterHeading.setText("Point Discussed " + level);


    }

    private DealerReportRequest getRequest() {
        DealerReportRequest request = new DealerReportRequest();
        request.dealerCode = dealerReportResponse.dealerCode;
        request.followUpDate = getTimeMMddyyy();
        request.videoUrl = "https://www.youtube.com/watch?v=u99AklNGpyc&list=RDu99AklNGpyc&start_radio=1";
        request.points = discussionPoints;
        request.isOperational="Yes";

        return request;
    }



    private void preFillData(int l) {

        if (l >= 9) {
            DiscussionPoint dp = map.get(String.valueOf(l));
            if (dp != null) {
                ScreenInformation info = dp.getScreenInformation();
                etSubFA.setText(dp.getSubFocusArea());
                focus_area_dd.setText(dp.getFocusArea());
                etPointsDiscussed.setText(info.getPointsDiscussed());
                etactionitems.setText(dp.getActionItem().toString().replace("[", "").replace("]", ""));
                tvResponsibilities.setText(dp.getResponsibility());
                tvTargetDateCalendar.setText(dp.getTargetDate());
                tvFooterHeading.setText("Point Discussed " + l);

            }
            strFocusArea = "";
            strSubFocusArea = "";
            strPointsDiscussed = "";
            strTargetDate = "";
            strResponsibility = "";
            strActionItem = "";




        } else {
            startActivity(new Intent(this, DiscussionPoint8.class));
        }

    }
}