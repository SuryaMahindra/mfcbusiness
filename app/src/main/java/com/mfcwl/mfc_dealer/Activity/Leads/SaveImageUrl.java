package com.mfcwl.mfc_dealer.Activity.Leads;
public class SaveImageUrl {

    private static SaveImageUrl mInstance = null;

    private String coverimageurl;
    private String frontviewurl;
    private String rearviewurl;
    private String leftsideviewurl;
    private String rightsideviewurl;
    private String rearlefturl;
    private String frontrighturl;
    private String rearrighturl;
    private String wheelurl;
    private String frontseatsurl;
    private String backseatsurl;
    private String odometerurl;
    private String dashboardurl;
    private String steeringurl;
    private String frontrowfromsideurl;
    private String backrowfromsideurl;
    private String trunkdooropenviewurl;



    //Interior

    public static SaveImageUrl getInstance(){
        if(mInstance == null)
        {
            mInstance = new SaveImageUrl();
        }
        return mInstance;
    }

    private SaveImageUrl(){
        coverimageurl = "";
        frontviewurl ="";
        rearviewurl ="";
        leftsideviewurl ="";
        rightsideviewurl ="";
        rearlefturl ="";
        frontrighturl ="";
        rearrighturl ="";
        wheelurl ="";
        frontseatsurl ="";
        backseatsurl ="";
        odometerurl ="";
        dashboardurl ="";
        steeringurl ="";
        frontrowfromsideurl ="";
        backrowfromsideurl ="";
        trunkdooropenviewurl ="";
    }

    public String getCoverimageurl() {
        return coverimageurl;
    }

    public void setCoverimageurl(String coverimageurl) {
        this.coverimageurl = coverimageurl;
    }

    public String getFrontviewurl() {
        return frontviewurl;
    }

    public void setFrontviewurl(String frontviewurl) {
        this.frontviewurl = frontviewurl;
    }

    public String getRearviewurl() {
        return rearviewurl;
    }

    public void setRearviewurl(String rearviewurl) {
        this.rearviewurl = rearviewurl;
    }

    public String getLeftsideviewurl() {
        return leftsideviewurl;
    }

    public void setLeftsideviewurl(String leftsideviewurl) {
        this.leftsideviewurl = leftsideviewurl;
    }

    public String getRightsideviewurl() {
        return rightsideviewurl;
    }

    public void setRightsideviewurl(String rightsideviewurl) {
        this.rightsideviewurl = rightsideviewurl;
    }

    public String getRearlefturl() {
        return rearlefturl;
    }

    public void setRearlefturl(String rearlefturl) {
        this.rearlefturl = rearlefturl;
    }

    public String getFrontrighturl() {
        return frontrighturl;
    }

    public void setFrontrighturl(String frontrighturl) {
        this.frontrighturl = frontrighturl;
    }

    public String getRearrighturl() {
        return rearrighturl;
    }

    public void setRearrighturl(String rearrighturl) {
        this.rearrighturl = rearrighturl;
    }

    public String getWheelurl() {
        return wheelurl;
    }

    public void setWheelurl(String wheelurl) {
        this.wheelurl = wheelurl;
    }

    public String getFrontseatsurl() {
        return frontseatsurl;
    }

    public void setFrontseatsurl(String frontseatsurl) {
        this.frontseatsurl = frontseatsurl;
    }

    public String getBackseatsurl() {
        return backseatsurl;
    }

    public void setBackseatsurl(String backseatsurl) {
        this.backseatsurl = backseatsurl;
    }

    public String getOdometerurl() {
        return odometerurl;
    }

    public void setOdometerurl(String odometerurl) {
        this.odometerurl = odometerurl;
    }

    public String getDashboardurl() {
        return dashboardurl;
    }

    public void setDashboardurl(String dashboardurl) {
        this.dashboardurl = dashboardurl;
    }

    public String getSteeringurl() {
        return steeringurl;
    }

    public void setSteeringurl(String steeringurl) {
        this.steeringurl = steeringurl;
    }

    public String getFrontrowfromsideurl() {
        return frontrowfromsideurl;
    }

    public void setFrontrowfromsideurl(String frontrowfromsideurl) {
        this.frontrowfromsideurl = frontrowfromsideurl;
    }

    public String getBackrowfromsideurl() {
        return backrowfromsideurl;
    }

    public void setBackrowfromsideurl(String backrowfromsideurl) {
        this.backrowfromsideurl = backrowfromsideurl;
    }

    public String getTrunkdooropenviewurl() {
        return trunkdooropenviewurl;
    }

    public void setTrunkdooropenviewurl(String trunkdooropenviewurl) {
        this.trunkdooropenviewurl = trunkdooropenviewurl;
    }
}
