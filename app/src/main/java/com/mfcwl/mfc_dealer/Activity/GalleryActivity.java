package com.mfcwl.mfc_dealer.Activity;

import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;


import com.mfcwl.mfc_dealer.Adapter.GalleryPagerAdapter;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import java.util.ArrayList;


/**
 * Created by Surya on 12/06/2018.
 */
public class GalleryActivity extends FragmentActivity {


    public final static String LOG_TAG = GalleryActivity.class.getSimpleName();
    public static final String ARG_IMAGES = "images";
    private static final String STATE_ITEM = "STATE_ITEM";

    private ViewPager mPager;
    private GalleryPagerAdapter mAdapter;

    public static ArrayList<String> mListImages;

    public static String nameArea;
    private LinearLayout mBackNavigation;

    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setContentView(R.layout.gallery_activity_layout);
       // this.setupActionBar();
        Log.i(TAG, "onCreate: ");
        mBackNavigation = (LinearLayout)findViewById(R.id.BackLayout);

        mBackNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        try {
            mListImages = getIntent().getStringArrayListExtra(ARG_IMAGES);

            if (savedInstanceState != null)
                mListImages = savedInstanceState.getParcelable(STATE_ITEM);

            mAdapter = new GalleryPagerAdapter(getSupportFragmentManager(), mListImages.size());

            // Pager
            mPager = (ViewPager) findViewById(R.id.gallery_layout_pager);
            mPager.setAdapter(mAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onCreate(savedInstanceState);

    }

    @Override
    protected void onResume() {
        super.onResume();

        Application.getInstance().trackScreenView(this,GlobalText.GalleryActivity);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
