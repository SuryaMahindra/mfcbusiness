package com.mfcwl.mfc_dealer.Activity.ASM_Reports;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.ASMAdapter.ASMReportAdapter.WeeklyReportAdapter;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.WeekReportSubmitModel;
import com.mfcwl.mfc_dealer.ASMModel.amsCreateRes;
import com.mfcwl.mfc_dealer.ASMReportsServices.WRSubmitService;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Model.weekRes;
import com.mfcwl.mfc_dealer.Model.weekResData;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.Helper;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Utility.Helper.dealerNameList;

public class WeeklyReport extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    RecyclerView recyclerView;
    WeeklyReportAdapter adapter;
    TextView select_dealer_tv;
    String cNameValue;
    String dealerCatVal = "select";
    String[] dealerNameArray = new String[]{"Manyata", "Bagmane", "Other"};
    public Button wr_submitallrpts;
    SwipeRefreshLayout swipeRefresh;

    public static String dcode = "0";
    public JSONArray array_all = new JSONArray();

    public String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weekly_report);
        Log.i(TAG, "onCreate: ");
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ImageView wrBackIV = findViewById(R.id.createRepo_backbtn);
        Log.i(TAG, "onCreate: ");
        wr_submitallrpts = findViewById(R.id.wr_submitallrpts);

        swipeRefresh = findViewById(R.id.swipeRefresh);
        swipeRefresh.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.CYAN);
        swipeRefresh.setOnRefreshListener(this);


        wrBackIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WeeklyReport.this, MainActivity.class);
                startActivity(intent);
            }
        });
        select_dealer_tv = findViewById(R.id.select_dealer_tv);


        select_dealer_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<String> dealerName = new ArrayList<String>();
                dealerName.clear();

                dealerName = Helper.dealerName;

                if (!dealerName.isEmpty()) {

                    if (!dealerName.get(dealerName.size() - 1).equalsIgnoreCase("Others")) {
                        dealerName.add(dealerName.size(), "Others");
                    }
                } else {
                    dealerName.add(dealerName.size(), "Others");
                }

                List<String> dealerCode = new ArrayList<String>();
                dealerCode.clear();
                dealerCode = Helper.dealerCode;

                if (!dealerCode.isEmpty()) {
                    if (!dealerCode.get(dealerCode.size() - 1).equalsIgnoreCase("Others")) {
                        dealerCode.add(dealerCode.size(), "Others");
                    }
                } else {
                    dealerCode.add(dealerCode.size(), "Others");
                }

                String[] namesArr = dealerName.toArray(new String[dealerName.size()]);

                String[] codeArr = dealerCode.toArray(new String[dealerCode.size()]);

                setAlertDialog(v, WeeklyReport.this, "Dealer Category", namesArr, codeArr);

                // setAlertDialog(v,WeeklyReport.this,"Dealer Name",dealerNameArray);
            }
        });

        cNameValue = (String) select_dealer_tv.getText();

        Button createNew = findViewById(R.id.createnewbtn);
        createNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!select_dealer_tv.getText().toString().equalsIgnoreCase("")) {

                    Intent intent = new Intent(WeeklyReport.this, CreateReportActivity.class);
                    intent.putExtra("dealername", select_dealer_tv.getText().toString());
                    dcode = CommonMethods.getstringvaluefromkey(WeeklyReport.this, "dcode");
                    intent.putExtra("dealer_code", dcode);
                    intent.putExtra("tempdealer_code", "");
                    intent.putExtra("editJson", "");

                    startActivity(intent);
                } else {
                    emptyValuation(select_dealer_tv);
                }


            }
        });


        wr_submitallrpts.setOnClickListener(v -> {

            Log.i(TAG, "onCreate: -1");
            week_report_submit_all_data();

        });

        loadData();

    }

    public void loadData() {


        select_dealer_tv.setText(CommonMethods.getstringvaluefromkey(this, "dname"));

        dcode = CommonMethods.getstringvaluefromkey(this, "dcode");
        String commonJson = CommonMethods.getstringvaluefromkey(this, "jsonweek");

        Log.i(TAG, "commonJson= " + commonJson);

        weekRes data = new weekRes();
        Gson gson = new Gson();
        data = gson.fromJson(commonJson, weekRes.class);

        ArrayList<weekResData> lists = new ArrayList<weekResData>();


        if (!commonJson.equalsIgnoreCase("")) lists = (ArrayList<weekResData>) data.getData();


        if (!lists.isEmpty()) {

            Collections.sort(lists, new Comparator<weekResData>() {
                @Override
                public int compare(weekResData o1, weekResData o2) {
                    return o2.getToDate().compareTo(o1.getToDate());
                    //return o2.getFormDate().compareTo(o1.getFormDate());
                }
            });

            // Just for testing

           /* for(weekResData week:lists)
            {
                System.out.println(" *************** Array List ************* ");
                System.out.print(week);
            }*/

            week_report_save_data(commonJson);

        }


        recyclerView = (RecyclerView) findViewById(R.id.rv_weeklyreport);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        this.recyclerView.setLayoutManager(layoutManager);
        adapter = new WeeklyReportAdapter(this, lists);
        this.recyclerView.setAdapter(adapter);
        swipeRefresh.setRefreshing(false);
    }

    private void setAlertDialog(View v, Activity a, final String strTitle,
                                final String[] arrVal, final String[] codeVal) {

        AlertDialog.Builder alert = new AlertDialog.Builder(a);
        alert.setTitle(strTitle);
        alert.setItems(arrVal, (dialog, which) -> {

            select_dealer_tv.setText(arrVal[which]);
            dealerCatVal = codeVal[which];

            CommonMethods.setvalueAgainstKey(WeeklyReport.this, "dcode", dealerCatVal);
            CommonMethods.setvalueAgainstKey(WeeklyReport.this, "dname", select_dealer_tv.getText().toString());

            loadData();

        });
        alert.create();
        alert.show();

    }


    private void week_report_submit_all_data() {

        String commonJson = CommonMethods.getstringvaluefromkey(WeeklyReport.this, "jsonweek");
        JSONArray new_array = new JSONArray();
        JSONObject jsonObjMain = new JSONObject();

        Log.i(TAG, "week_report_submit_all_data: -1");

        try {
            Log.i(TAG, "week_report_submit_all_data: -2");

            if (!commonJson.equalsIgnoreCase("")) {
                jsonObjMain = new JSONObject(commonJson);

                new_array = jsonObjMain.getJSONArray("data");

                for (int i = 0; i < new_array.length(); i++) {
                    JSONObject updateJson = new JSONObject();
                    updateJson = new_array.getJSONObject(i);

                    if (ValitationCheck(updateJson)) {
                        array_all.put(updateJson);
                        new_array.remove(i);
                        i = i - 1;
                    }
                }
            }

            jsonObjMain.put("data", new_array);

        } catch (JSONException e) {
            int x = 0;
            e.printStackTrace();
        }

        String saveJsonNew = jsonObjMain.toString(); //http request

        CommonMethods.setvalueAgainstKey(WeeklyReport.this, "jsonweek", saveJsonNew);

        Log.d("saveJsonNew", "saveJsonNew= " + saveJsonNew.toString());


        if (array_all.length() != 0) {

            if (CommonMethods.isInternetWorking(WeeklyReport.this)) {
                submitReportDetails(array_all);
            } else {
                CommonMethods.alertMessage(WeeklyReport.this, GlobalText.CHECK_NETWORK_CONNECTION);
            }


        } else {
            if (adapter.getItemCount() <= 0) {
                Toast.makeText(WeeklyReport.this, "No Pending Report.", Toast.LENGTH_LONG).show();

            } else {
                Toast.makeText(WeeklyReport.this, "some field data missing.", Toast.LENGTH_LONG).show();

            }

        }

    }


    private Boolean ValitationCheck(JSONObject updateJson) {


        Iterator<String> keys = updateJson.keys();
        for (int i = 0; i < updateJson.length(); i++) {
            try {
                if (updateJson.get(keys.next()).equals("")) {
                    return false;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return true;
    }


    private void submitReportDetails(JSONArray array_all) {

        SpinnerManager.showSpinner(WeeklyReport.this);

        ArrayList<WeekReportSubmitModel> list = new ArrayList<>();

        try {
            for (int i = 0; i < array_all.length(); i++) {

                JSONObject updateJson = new JSONObject();
                updateJson = array_all.getJSONObject(i);

                WeekReportSubmitModel setData = new WeekReportSubmitModel();
                Gson gson = new Gson();
                setData = gson.fromJson(updateJson.toString(), WeekReportSubmitModel.class);


                setData.setTempdealer_code(null);
                setData.setDate(null);
                setData.setDay(null);
                setData.setHour(null);

                list.add(setData);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        WRSubmitService wrSubmitService = new WRSubmitService();
        wrSubmitService.pushMultipleData(list, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                Response<amsCreateRes> mRes = (Response<amsCreateRes>) obj;
                amsCreateRes mData = mRes.body();

                if (mData.getStatus().equalsIgnoreCase("SUCCESS")) {
                    Toast.makeText(getApplicationContext(), "Successfully Submited", Toast.LENGTH_LONG).show();

                    try {
                        for (int i = 0; i < array_all.length(); i++) {

                            array_all.remove(i);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), mData.getMessage().toString(), Toast.LENGTH_LONG).show();
                }


                SpinnerManager.hideSpinner(WeeklyReport.this);

                loadData();
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                Log.d("FailureMsg", "Try Again ");
                SpinnerManager.hideSpinner(WeeklyReport.this);
                Toast.makeText(WeeklyReport.this, "try again", Toast.LENGTH_LONG).show();
            }
        });

    }


    public void emptyValuation(EditText hint) {
        Toast.makeText(WeeklyReport.this, hint.getHint().toString(), Toast.LENGTH_LONG).show();
    }

    public void emptyValuation(TextView hint) {
        Toast.makeText(WeeklyReport.this, hint.getHint().toString(), Toast.LENGTH_LONG).show();
    }

    public void emptyValuation(String hint) {
        Toast.makeText(WeeklyReport.this, hint, Toast.LENGTH_LONG).show();
    }

    public void DeletetAert() {

        // custom dialog
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.common_cus_dialog);
        //dialog.setTitle("Custom Dialog");

        TextView Message, Message2;
        Button cancel, Confirm;
        Message = dialog.findViewById(R.id.Message);
        Message2 = dialog.findViewById(R.id.Message2);
        cancel = dialog.findViewById(R.id.cancel);
        Confirm = dialog.findViewById(R.id.Confirm);
        Message.setText("Are you sure you want to ");
        Message2.setText("Delete ?");


        Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void week_report_save_data(String commonJson) {

        JSONArray new_array = new JSONArray();
        JSONArray old_array = new JSONArray();
        JSONObject jsonObjMain = new JSONObject();

        Log.i(TAG, "commonJson-in: " + commonJson);

        try {

            if (!commonJson.equalsIgnoreCase("")) {

                dealerNameList.clear();

                jsonObjMain = new JSONObject(commonJson);
                old_array = jsonObjMain.getJSONArray("data");
                for (int i = 0; i < old_array.length(); i++) {
                    new_array.put(old_array.get(i));
                }
                for (int i = 0; i < new_array.length(); i++) {

                    JSONObject updateJson = new JSONObject();
                    updateJson = new_array.getJSONObject(i);

                    dealerNameList.put(updateJson.getString("to_date"), updateJson.getString("to_date"));

                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onRefresh() {
        swipeRefresh.setRefreshing(true);
        loadData();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(WeeklyReport.this, MainActivity.class);
        startActivity(intent);
    }
}
