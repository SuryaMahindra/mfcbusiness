package com.mfcwl.mfc_dealer.Activity.RDR.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RDRActionItemRequest {

    @SerializedName("Page")
    @Expose
    public String page;
    @SerializedName("PageItems")
    @Expose
    public String pageItems;
    @SerializedName("FilterByFields")
    @Expose
    public String filterByFields;
    @SerializedName("OrderBy")
    @Expose
    public String orderBy;
    @SerializedName("OrderByReverse")
    @Expose
    public String orderByReverse;
    @SerializedName("WhereIn")
    @Expose
    public List<Object> whereIn = null;
    @SerializedName("where")
    @Expose
    public List<Where> where = null;

}
