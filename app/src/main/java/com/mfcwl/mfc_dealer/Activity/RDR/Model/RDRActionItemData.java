package com.mfcwl.mfc_dealer.Activity.RDR.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RDRActionItemData {

    @SerializedName("action_id")
    @Expose
    public Integer actionId;
    @SerializedName("rdr_submitted_date")
    @Expose
    public String rdrSubmittedDate;
    @SerializedName("dealer_code")
    @Expose
    public String dealerCode;
    @SerializedName("dealer_name")
    @Expose
    public String dealerName;
    @SerializedName("action_status")
    @Expose
    public String actionStatus;
    @SerializedName("focus_area")
    @Expose
    public String focusArea;
    @SerializedName("sub_focus_area")
    @Expose
    public String subFocusArea;
    @SerializedName("target_date")
    @Expose
    public String targetDate;
    @SerializedName("responsibility")
    @Expose
    public String responsibility;
    @SerializedName("action_item")
    @Expose
    public String actionItem;

    @SerializedName("completion_date")
    @Expose
    public String completiondate;

    @SerializedName("is_escalated")
    @Expose
    public Boolean isEscalated;


}