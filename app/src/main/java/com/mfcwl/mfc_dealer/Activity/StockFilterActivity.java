package com.mfcwl.mfc_dealer.Activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.appyvet.materialrangebar.RangeBar;
import com.mfcwl.mfc_dealer.Activity.Leads.FilterInstance;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.mfcwl.mfc_dealer.Fragment.StockFragment.stock_filter_icon;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ASM;

public class StockFilterActivity extends AppCompatActivity {

    @BindView(R.id.filter_cetified_car)
    public Button filterCetifiedCar;
    @BindView(R.id.filter_all_cars)
    public Button filterAllCar;
    @BindView(R.id.filter_price_card)
    public CardView filterPrice;
    @BindView(R.id.year_card)
    public CardView filterYear;
    @BindView(R.id.certified_level)
    public TextView certifiedLevel;
    @BindView(R.id.linear_certified_car)
    public CardView certifiedValues;
    @BindView(R.id.kms_level)
    public TextView kmsLelel;
    @BindView(R.id.startprice)
    public TextView startprice;
    @BindView(R.id.endprice)
    public TextView endprice;
    @BindView(R.id.kmsvalues)
    public TextView kmsvalues;
    @BindView(R.id.year_change)
    public TextView year_change;
    @BindView(R.id.kms_values)
    public CardView kmsValues;
    @BindView(R.id.year_values)
    public CardView year_values;
    @BindView(R.id.price_values)
    public CardView price_values;
    @BindView(R.id.filter_seekbar)
    public SeekBar filter_seekbar;
    @BindView(R.id.year_seekbar)
    public SeekBar year_seekbar;

    @BindView(R.id.filterby_apply)
    public LinearLayout filterby_apply;
    @BindView(R.id.filterbycancelbtn)
    public LinearLayout filterbycancelbtn;

    @BindView(R.id.price_arrow)
    public ImageView price_arrow;
    @BindView(R.id.yearicon)
    public ImageView yearicon;

    public static CheckBox photocounts, thirtydaycheckbox;

    public static boolean stock_photocountsFlag;
    public static boolean book_photocountsFlag;

    boolean certified, kms, price = true, yearFlag = true;
    public static Activity activity;

    public static String yearValues = "2002";
    public static String kilometerValues = "1000000";
    public static String startPriceValues = "0";
    public static String endPriceValues = "10000000";

    RangeBar twoseekar;
    boolean certifiedFlag = false;
    //add seekbar value
    int start = 0, end = 100;

    int progval = 0;
    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_filter);
        Log.i(TAG, "onCreate: ");
        ButterKnife.bind(this);
        activity = this;
        CommonMethods.MemoryClears();
        photocounts = (CheckBox) findViewById(R.id.photocounts);
        thirtydaycheckbox = (CheckBox) findViewById(R.id.thirtydaycheckbox);
        Log.i(TAG, "onCreate: ");
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter("com.mfcwl.mfc_dealer.FMC"));

        if (CommonMethods.getstringvaluefromkey(activity, "bookkilometerdata").equalsIgnoreCase("") || CommonMethods.getstringvaluefromkey(activity, "bookkilometerdata") == null) {
            CommonMethods.setvalueAgainstKey(activity, "bookkilometerdata", kilometerValues);
        }

        if (CommonMethods.getstringvaluefromkey(activity, "bookstartpricedata").equalsIgnoreCase("") || CommonMethods.getstringvaluefromkey(activity, "bookstartpricedata") == null) {

            CommonMethods.setvalueAgainstKey(activity, "bookstartpricedata", startPriceValues);
        }
        if (CommonMethods.getstringvaluefromkey(activity, "bookendpricedata").equalsIgnoreCase("") || CommonMethods.getstringvaluefromkey(activity, "bookendpricedata") == null) {

            CommonMethods.setvalueAgainstKey(activity, "bookendpricedata", endPriceValues);

        }


        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        twoseekar = (RangeBar) findViewById(R.id.twoseekar);

        //added value progres

        certifiedValues.setVisibility(View.GONE);
        kmsValues.setVisibility(View.GONE);
        price_values.setVisibility(View.GONE);
        year_values.setVisibility(View.GONE);

        price_arrow.setBackgroundResource(R.drawable.down_arrow);
        yearicon.setBackgroundResource(R.drawable.down_arrow);

        Log.e("testtwo", twoseekar.getLeftPinValue() + " " + twoseekar.getRightPinValue());
        Log.e("testyear", year_seekbar.getProgress() + "");
        Log.e("testfilter", filter_seekbar.getProgress() + "");

        Log.e("getNotificationstring", FilterInstance.getInstance().getNotification());

        certifiedLevel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
        kmsLelel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);

        if (!FilterInstance.getInstance().getNotification().equals("")) {
            Log.e("tets", "test");
            stock_photocountsFlag = false;
            CommonMethods.setvalueAgainstKey(activity, "thirtydaycheckbox", "false");
            // CommonMethods.setvalueAgainstKey(activity, "thirtydaycheckbox", "false");
            CommonMethods.setvalueAgainstKey(activity, "Notification_Switch", "false");

            CommonMethods.setvalueAgainstKey(activity, "clear", "true");

            CommonMethods.setvalueAgainstKey(activity, "photocounts", "false");
            CommonMethods.setvalueAgainstKey(activity, "photocounts1", "false");
            CommonMethods.setvalueAgainstKey(activity, "photocounts2", "false");
            Log.e("test", filter_seekbar.getProgress() + "");
            kilometerValues = "1000000";
            filter_seekbar.setProgress(Integer.parseInt(kilometerValues));

            kmsvalues.setText(" " + kilometerValues + " Kms");

            yearValues = "2002";
            if (yearValues.equalsIgnoreCase("2002")) {
                year_seekbar.setProgress(0);
            }
            year_change.setText(yearValues + " and Newer");

            photocounts.setChecked(false);
            thirtydaycheckbox.setChecked(false);

            //twoseekbar
            start = 0;
            end = 100;
            twoseekar.setRangePinsByValue(start, end);

            //allcar
            certifiedFlag = false;
            filterAllCar.setBackgroundResource(R.drawable.my_button);
            filterAllCar.setTextColor(getResources().getColor(R.color.Black));
            CommonMethods.setvalueAgainstKey(activity, "certifiedstatus", "allcar");

            filterCetifiedCar.setBackgroundResource(R.drawable.my_buttom_gray_light);
            filterCetifiedCar.setTextColor(getResources().getColor(R.color.lgray));

            if (CommonMethods.getstringvaluefromkey(activity, "clear").equalsIgnoreCase("true")) {
                CommonMethods.setvalueAgainstKey(activity, "certifiedval", "false");
                CommonMethods.setvalueAgainstKey(activity, "price_valuesval", "false");
                CommonMethods.setvalueAgainstKey(activity, "yearval", "false");
                CommonMethods.setvalueAgainstKey(activity, "kmsval", "false");
                CommonMethods.setvalueAgainstKey(activity, "clear", "false");
            }
        } else {
            priceRefill();
        }





        if (!FilterInstance.getInstance().getNotification().equals("")) {
            if (FilterInstance.getInstance().getNotification().equals("missing images")) {
                photocounts.setChecked(true);
                thirtydaycheckbox.setChecked(false);
                //  FilterInstance.getInstance().setNotification("");
                CommonMethods.setvalueAgainstKey(activity, "photocounts", "true");
            }

            if (FilterInstance.getInstance().getNotification().equals("old")) {
                photocounts.setChecked(false);
                thirtydaycheckbox.setChecked(true);
                //  FilterInstance.getInstance().setNotification("");
                Log.e("thirtydaycheckbox-->", "thirtydaycheckbox-a1");
                // CommonMethods.setvalueAgainstKey(activity, "photocounts", "false");
                CommonMethods.setvalueAgainstKey(activity, "thirtydaycheckbox", "true");
            }
        } else {
            if (CommonMethods.getstringvaluefromkey(this, "photocounts").equalsIgnoreCase("true")) {
                photocounts.setChecked(true);
                CommonMethods.setvalueAgainstKey(activity, "photocounts", "false");
            }
            if (CommonMethods.getstringvaluefromkey(this, "photocounts1").equalsIgnoreCase("true")) {
                photocounts.setChecked(true);
                Log.e("testphotocount2", photocounts.isChecked() + "");
                CommonMethods.setvalueAgainstKey(activity, "photocounts", "false");
            }
            if (CommonMethods.getstringvaluefromkey(this, "photocounts2").equalsIgnoreCase("true")) {
                photocounts.setChecked(true);
                Log.e("testphotocount2", photocounts.isChecked() + "");
                CommonMethods.setvalueAgainstKey(activity, "photocounts2", "false");
            }
            if (CommonMethods.getstringvaluefromkey(this, "thirtydaycheckbox").equalsIgnoreCase("true")) {
                thirtydaycheckbox.setChecked(true);
                Log.e("thirtydaycheckbox", thirtydaycheckbox.isChecked() + "");
                CommonMethods.setvalueAgainstKey(activity, "thirtydaycheckbox", "true");
            }
        }

        /*if (CommonMethods.getstringvaluefromkey(this, "photocounts").equalsIgnoreCase("true")
                || CommonMethods.getstringvaluefromkey(this, "Notification_Switch").equalsIgnoreCase("noStockImage")) {
            photocounts.setChecked(true);
        }

        if (CommonMethods.getstringvaluefromkey(this, "thirtydaycheckbox").equalsIgnoreCase("true")) {
            thirtydaycheckbox.setChecked(true);
        }*/

        //reena
        Log.e("testphotocount112", CommonMethods.getstringvaluefromkey(this, "photocounts1").equalsIgnoreCase("true") + "");

        if (FilterInstance.getInstance().getNotification().equals("")) {
            if (CommonMethods.getstringvaluefromkey(this, "photocounts1").equalsIgnoreCase("true")) {
                photocounts.setChecked(true);
                Log.e("testphotocount2", photocounts.isChecked() + "");
                CommonMethods.setvalueAgainstKey(activity, "photocounts", "false");
            }
            if (CommonMethods.getstringvaluefromkey(this, "photocounts2").equalsIgnoreCase("true")) {
                photocounts.setChecked(true);
                Log.e("testphotocount2", photocounts.isChecked() + "");
                CommonMethods.setvalueAgainstKey(activity, "photocounts2", "false");
            }
        }
        if (CommonMethods.getstringvaluefromkey(this, "certifiedstatus").equalsIgnoreCase("certifiedcar")) {
            filterCetifiedCar.setBackgroundResource(R.drawable.my_button);
            filterCetifiedCar.setTextColor(getResources().getColor(R.color.Black));
            certifiedFlag = true;
            filterAllCar.setBackgroundResource(R.drawable.my_buttom_gray_light);
            filterAllCar.setTextColor(getResources().getColor(R.color.lgray));
        } else {
            certifiedFlag = false;
            filterAllCar.setBackgroundResource(R.drawable.my_button);
            filterAllCar.setTextColor(getResources().getColor(R.color.Black));
            CommonMethods.setvalueAgainstKey(activity, "certifiedstatus", "allcar");

            filterCetifiedCar.setBackgroundResource(R.drawable.my_buttom_gray_light);
            filterCetifiedCar.setTextColor(getResources().getColor(R.color.lgray));
        }

        if (CommonMethods.getstringvaluefromkey(activity, "kilometerdata").equals(kilometerValues)) {


        } else {

        }


        if (!CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
            if (CommonMethods.getstringvaluefromkey(activity, "30Name").equalsIgnoreCase("Stocks >30days")
                    ) {
                thirtydaycheckbox.setChecked(true);
                CommonMethods.setvalueAgainstKey(activity, "thirtydaycheckbox", "true");
            }
        }

       /* certifiedLevel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
        kmsLelel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);*/

        filterby_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FilterInstance.getInstance().setNotification("");

               /* Log.e("certifiedval", "certifiedval=" + CommonMethods.getstringvaluefromkey(activity, "certifiedval"));
                Log.e("price_valuesval", "price_valuesval=" + CommonMethods.getstringvaluefromkey(activity, "price_valuesval"));
                Log.e("yearval", "yearval=" + CommonMethods.getstringvaluefromkey(activity, "yearval"));
                Log.e("kmsval", "kmsval=" + CommonMethods.getstringvaluefromkey(activity, "kmsval"));
                Log.e("thirtydaycheckbox", "thirtydaycheckbox=" + CommonMethods.getstringvaluefromkey(activity, "thirtydaycheckbox"));
                Log.e("photocounts1", "photocounts1=" + CommonMethods.getstringvaluefromkey(activity, "photocounts1"));
                Log.e("tcertifiedstatus", "tcertifiedstatus=" + CommonMethods.getstringvaluefromkey(activity, "tcertifiedstatus"));


                Log.e("yearValues", "yearValues=" + yearValues);
                Log.e("kilometerValues", "kilometerValues=" + yearValues);
                Log.e("startPriceValues", "startPriceValues=" + yearValues);
                Log.e("endPriceValues", "endPriceValues=" + yearValues);*/

                CommonMethods.setvalueAgainstKey(activity, "stockfilterOnclick", "true");

                if (CommonMethods.getstringvaluefromkey(activity, "certifiedval").equalsIgnoreCase("true")
                        || CommonMethods.getstringvaluefromkey(activity, "price_valuesval").equalsIgnoreCase("true")
                        || CommonMethods.getstringvaluefromkey(activity, "yearval").equalsIgnoreCase("true")
                        || CommonMethods.getstringvaluefromkey(activity, "kmsval").equalsIgnoreCase("true")
                        || CommonMethods.getstringvaluefromkey(activity, "thirtydaycheckbox").equalsIgnoreCase("true")
                        || CommonMethods.getstringvaluefromkey(activity, "photocounts1").equalsIgnoreCase("true")
                        || CommonMethods.getstringvaluefromkey(activity, "tcertifiedstatus").equalsIgnoreCase("certifiedcar")
                        ) {

                    if (yearValues.equalsIgnoreCase("2002")
                            && kilometerValues.equalsIgnoreCase("1000000")
                            && startPriceValues.equalsIgnoreCase("0")
                            && endPriceValues.equalsIgnoreCase("10000000")) {

                        if (CommonMethods.getstringvaluefromkey(activity, "thirtydaycheckbox").equalsIgnoreCase("true")
                                || CommonMethods.getstringvaluefromkey(activity, "photocounts1").equalsIgnoreCase("true")
                                || CommonMethods.getstringvaluefromkey(activity, "certifiedval").equalsIgnoreCase("true")
                                ) {
                            stock_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);
                            //   Log.e("filter_icon_yellow", "filter_icon_yellow=1asf");
                        }

                    } else {
                        stock_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);
                        //   Log.e("filter_icon_yellow", "filter_icon_yellow=1as");
                    }
                    if (CommonMethods.getstringvaluefromkey(activity, "certifiedval").equalsIgnoreCase("true")) {
                        stock_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);
                        //   Log.e("filter_icon_yellow", "filter_icon_yellow=1asf");
                    }

                } else {
                    stock_filter_icon.setBackgroundResource(R.drawable.filter_48x18);
                    //   Log.e("filter_icon_yellow", "filter_icon_yellow=2");

                }


              /*  if (CommonMethods.getstringvaluefromkey(activity, "clear").equalsIgnoreCase("true")) {
                    CommonMethods.setvalueAgainstKey(activity, "certifiedval", "false");
                    CommonMethods.setvalueAgainstKey(activity, "price_valuesval", "false");
                    CommonMethods.setvalueAgainstKey(activity, "yearval", "false");
                    CommonMethods.setvalueAgainstKey(activity, "kmsval", "false");
                    CommonMethods.setvalueAgainstKey(activity, "clear", "false");

                }*/


                if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("StockStore")) {
                    //   Log.e("test variable", "stock");

                    if (certifiedFlag) {
                        CommonMethods.setvalueAgainstKey(activity, "certifiedstatus", "certifiedcar");
                    } else {
                        CommonMethods.setvalueAgainstKey(activity, "certifiedstatus", "allcar");
                    }

                    CommonMethods.setvalueAgainstKey(activity, "startpricedata", startPriceValues);
                    CommonMethods.setvalueAgainstKey(activity, "endpricedata", endPriceValues);
                    CommonMethods.setvalueAgainstKey(activity, "kilometerdata", kilometerValues);
                    CommonMethods.setvalueAgainstKey(activity, "yeardata", yearValues);

                } else {

                    Log.e("test variable", "book");

                    CommonMethods.setvalueAgainstKey(activity, "booklistfilter", "true");

                    /*if (certifiedFlag) {
                        CommonMethods.setvalueAgainstKey(activity, "bookcertifiedstatus", "certifiedcar");
                    } else {


                        CommonMethods.setvalueAgainstKey(activity, "bookcertifiedstatus", "allcar");
                    }

                    CommonMethods.setvalueAgainstKey(activity, "bookstartpricedata", startPriceValues);
                    CommonMethods.setvalueAgainstKey(activity, "bookendpricedata", endPriceValues);
                    CommonMethods.setvalueAgainstKey(activity, "bookkilometerdata", kilometerValues);
                    CommonMethods.setvalueAgainstKey(activity, "bookyeardata", yearValues);*/


                }
                if (certifiedFlag) {
                    CommonMethods.setvalueAgainstKey(activity, "certifiedstatus", "certifiedcar");
                } else {
                    CommonMethods.setvalueAgainstKey(activity, "certifiedstatus", "allcar");
                }

                CommonMethods.setvalueAgainstKey(activity, "startpricedata", startPriceValues);
                CommonMethods.setvalueAgainstKey(activity, "endpricedata", endPriceValues);
                CommonMethods.setvalueAgainstKey(activity, "kilometerdata", kilometerValues);
                CommonMethods.setvalueAgainstKey(activity, "yeardata", yearValues);


                if (thirtydaycheckbox.isChecked()) {
                    CommonMethods.setvalueAgainstKey(activity, "thirtydaycheckbox", "true");
                } else {
                    CommonMethods.setvalueAgainstKey(activity, "thirtydaycheckbox", "false");

                }

                //reena
                if (photocounts.isChecked()) {
                    CommonMethods.setvalueAgainstKey(activity, "photocounts1", "true");
                } else {
                    CommonMethods.setvalueAgainstKey(activity, "photocounts1", "false");
                }
                stock_photocountsFlag = photocounts.isChecked();

                Log.e("StockStore=", "StockStore=" + CommonMethods.getstringvaluefromkey(activity, "status"));

                if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("StockStore")) {
                    CommonMethods.setvalueAgainstKey(activity, "storelistfilter", "true");
                    CommonMethods.setvalueAgainstKey(activity, "bookfilter", "false");

                    //reena
                   /* if (photocounts.isChecked()){
                        CommonMethods.setvalueAgainstKey(activity, "photocounts1", "true");
                    }else {
                        CommonMethods.setvalueAgainstKey(activity, "photocounts1", "false");
                    }
                        if(photocounts.isChecked()){
                            stock_photocountsFlag=true;

                        }else{
                            stock_photocountsFlag=false;
                        }
*/
                } else {
                    CommonMethods.setvalueAgainstKey(activity, "storelistfilter", "false");
                    CommonMethods.setvalueAgainstKey(activity, "bookfilter", "true");

                    /*if (photocounts.isChecked()){
                        CommonMethods.setvalueAgainstKey(activity, "photocounts", "true");
                    }

                    CommonMethods.setvalueAgainstKey(activity,"bookfilter","true");

                    //reena
                    if (photocounts.isChecked()){
                        CommonMethods.setvalueAgainstKey(activity, "photocounts2", "true");
                    }else {
                        CommonMethods.setvalueAgainstKey(activity, "photocounts2", "false");
                    }

                    if(photocounts.isChecked()){
                        book_photocountsFlag=true;
                    }else{
                        book_photocountsFlag=false;
                    }*/
                }


                CommonMethods.setvalueAgainstKey(activity, "filterapply", "true");
                if(!CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ASM)){
                    Intent intent = new Intent(StockFilterActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }else {
                    finish();
                }



            }
        });

        filterbycancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.setvalueAgainstKey(activity, "clear", "false");

                finish();
            }
        });


        filterPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*
                Intent intent = new Intent(StockFilterActivity.this,FilterPriceActvity.class);
                startActivity(intent);*/


                if (price) {
                    price_values.setVisibility(View.VISIBLE);
                    price_arrow.setBackgroundResource(R.drawable.up_arrow);
                    price = false;
                } else {
                    price_values.setVisibility(View.GONE);
                    price_arrow.setBackgroundResource(R.drawable.down_arrow);
                    price = true;
                }


            }
        });

        filter_seekbar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    int progress = 0;


                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progressValue, boolean fromUser) {
                        progress = progressValue;
                        //Log.e("progress=","progress="+progress);

                        CommonMethods.setvalueAgainstKey(activity, "kmsval", "true");


                        if (progress == 0 || progress <= 10000) {
                            kmsvalues.setText("10,000" + " Kms");
                            progval = 10000;
                        } else if (10000 < progress && progress <= 20000) {
                            kmsvalues.setText("20,000" + " Kms");
                            progval = 20000;
                        } else if (20000 < progress && progress <= 30000) {
                            kmsvalues.setText("30,000" + " Kms");
                            progval = 30000;
                        } else if (30000 < progress && progress <= 40000) {
                            kmsvalues.setText("40,000" + " Kms");
                            progval = 40000;
                        } else if (40000 < progress && progress <= 50000) {
                            kmsvalues.setText("50,000" + " Kms");
                            progval = 50000;
                        } else if (50000 < progress && progress <= 60000) {
                            kmsvalues.setText("60,000" + " Kms");
                            progval = 60000;
                        } else if (60000 < progress && progress <= 70000) {
                            kmsvalues.setText("70,000" + " Kms");
                            progval = 70000;
                        } else if (70000 < progress && progress <= 80000) {
                            kmsvalues.setText("80,000" + " Kms");
                            progval = 80000;
                        } else if (80000 < progress && progress <= 90000) {
                            kmsvalues.setText("90,000" + " Kms");
                            progval = 90000;
                        } else if (90000 < progress && progress <= 100000) {
                            kmsvalues.setText("1,00,000" + " Kms");
                            progval = 100000;
                        } else if (100000 < progress && progress <= 200000) {
                            kmsvalues.setText("2,00,000" + " Kms");
                            progval = 200000;
                        } else if (200000 < progress && progress <= 300000) {
                            kmsvalues.setText("3,00,000" + " Kms");
                            progval = 300000;
                        } else if (300000 < progress && progress <= 400000) {
                            kmsvalues.setText("4,00,000" + " Kms");
                            progval = 400000;
                        } else if (400000 < progress && progress <= 500000) {
                            kmsvalues.setText("5,00,000" + " Kms");
                            progval = 500000;
                        } else if (500000 < progress && progress <= 600000) {
                            kmsvalues.setText("6,00,000" + " Kms");
                            progval = 600000;
                        } else if (600000 < progress && progress <= 700000) {
                            kmsvalues.setText("7,00,000" + " Kms");
                            progval = 700000;
                        } else if (700000 < progress && progress <= 800000) {
                            kmsvalues.setText("8,00,000" + " Kms");
                            progval = 800000;
                        } else if (800000 < progress && progress <= 900000) {
                            kmsvalues.setText("9,00,000" + " Kms");
                            progval = 900000;
                        } else if (900000 < progress && progress <= 1000000) {
                            kmsvalues.setText("10,00,000" + " Kms");
                            progval = 1000000;

                        }

                        kilometerValues = Integer.toString(progval);


                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });

        year_seekbar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    int progress = 0;

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progressValue, boolean fromUser) {
                        progress = progressValue;
                        CommonMethods.setvalueAgainstKey(activity, "yearval", "true");

                        //Log.e("progress=","progress="+progress);

                        if (progressValue == 0 || progressValue <= 5) {
                            year_change.setText("2002" + " and Newer");
                            yearValues = "2002";
                        } else if (5 < progressValue && progressValue <= 10) {
                            year_change.setText("2002" + " and Newer");
                            yearValues = "2002";
                        } else if (10 < progressValue && progressValue <= 15) {
                            year_change.setText("2003" + " and Newer");
                            yearValues = "2003";
                        } else if (15 < progressValue && progressValue <= 20) {
                            year_change.setText("2004" + " and Newer");
                            yearValues = "2004";
                        } else if (20 < progressValue && progressValue <= 25) {
                            year_change.setText("2005" + " and Newer");
                            yearValues = "2005";
                        } else if (25 < progressValue && progressValue <= 30) {
                            year_change.setText("2006" + " and Newer");
                            yearValues = "2006";
                        } else if (30 < progressValue && progressValue <= 35) {
                            year_change.setText("2007" + " and Newer");
                            yearValues = "2007";
                        } else if (35 < progressValue && progressValue <= 40) {
                            year_change.setText("2008" + " and Newer");
                            yearValues = "2008";
                        } else if (40 < progressValue && progressValue <= 45) {
                            year_change.setText("2009" + " and Newer");
                            yearValues = "2009";
                        } else if (45 < progressValue && progressValue <= 50) {
                            year_change.setText("2010" + " and Newer");
                            yearValues = "2010";
                        } else if (50 < progressValue && progressValue <= 55) {
                            year_change.setText("2010" + " and Newer");
                            yearValues = "2010";
                        } else if (55 < progressValue && progressValue <= 60) {
                            year_change.setText("2011" + " and Newer");
                            yearValues = "2011";
                        } else if (60 < progressValue && progressValue <= 65) {
                            year_change.setText("2012" + " and Newer");
                            yearValues = "2012";
                        } else if (65 < progressValue && progressValue <= 70) {
                            year_change.setText("2013" + " and Newer");
                            yearValues = "2013";
                        } else if (70 < progressValue && progressValue <= 75) {
                            year_change.setText("2014" + " and Newer");
                            yearValues = "2014";
                        } else if (75 < progressValue && progressValue <= 80) {
                            year_change.setText("2015" + " and Newer");
                            yearValues = "2015";
                        } else if (80 < progressValue && progressValue <= 85) {
                            year_change.setText("2016" + " and Newer");
                            yearValues = "2016";
                        } else if (85 < progressValue && progressValue <= 90) {
                            year_change.setText("2017" + " and Newer");
                            yearValues = "2017";
                        } else if (90 < progressValue && progressValue <= 95) {
                            year_change.setText("2018" + " and Newer");
                            yearValues = "2018";
                        } else if (95 < progressValue && progressValue <= 100) {
                            year_change.setText("2018" + " and Newer");
                            yearValues = "2018";

                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });

        twoseekar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                //code reena
                CommonMethods.setvalueAgainstKey(activity, "price_valuesval", "true");

                setStartEndPrice(leftPinIndex, rightPinIndex);
            }
        });
    }

    //code reena

    public void setStartEndPrice(int leftPinIndex, int rightPinIndex) {
        if (leftPinIndex == 0) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "0");
            startPriceValues = "0";
        } else if (leftPinIndex == 1) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "1L");
            startPriceValues = "100000";
        } else if (leftPinIndex == 2) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "2L");
            startPriceValues = "200000";
        } else if (leftPinIndex == 3) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "3L");
            startPriceValues = "300000";
        } else if (leftPinIndex == 4) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "4L");
            startPriceValues = "400000";
        } else if (leftPinIndex == 5) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "5L");
            startPriceValues = "500000";
        } else if (leftPinIndex == 6) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "6L");
            startPriceValues = "600000";
        } else if (leftPinIndex == 7) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "7L");
            startPriceValues = "700000";
        } else if (leftPinIndex == 8) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "8L");
            startPriceValues = "800000";
        } else if (leftPinIndex == 9) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "9L");
            startPriceValues = "900000";
        } else if (leftPinIndex == 10) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "10L");
            startPriceValues = "1000000";
        } else if (leftPinIndex == 12) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "20L");
            startPriceValues = "2000000";
        } else if (leftPinIndex == 13) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "30L"); //change 1300000 to 30000000 reena
            startPriceValues = "3000000";
        } else if (leftPinIndex == 14) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "40L");
            startPriceValues = "4000000";
        } else if (leftPinIndex == 15) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "50L");
            startPriceValues = "5000000";
        } else if (leftPinIndex == 16) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "60L");
            startPriceValues = "6000000";
        } else if (leftPinIndex == 17) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "70L");
            startPriceValues = "7000000";
        } else if (leftPinIndex == 18) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "80L");
            startPriceValues = "8000000";
        } else if (leftPinIndex == 19) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "90L");
            startPriceValues = "9000000";
        } else if (leftPinIndex == 20) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "1 Cr");
            startPriceValues = "10000000";
        }


        if (rightPinIndex == 0) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "0");
            endPriceValues = "0";
        } else if (rightPinIndex == 1) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "1L");
            endPriceValues = "100000";
        } else if (rightPinIndex == 2) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "2L");
            endPriceValues = "200000";
        } else if (rightPinIndex == 3) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "3L");
            endPriceValues = "300000";
        } else if (rightPinIndex == 4) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "4L");
            endPriceValues = "400000";
        } else if (rightPinIndex == 5) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "5L");
            endPriceValues = "500000";
        } else if (rightPinIndex == 6) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "6L");
            endPriceValues = "600000";
        } else if (rightPinIndex == 7) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "7L");
            endPriceValues = "700000";
        } else if (rightPinIndex == 8) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "8L");
            endPriceValues = "800000";
        } else if (rightPinIndex == 9) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "9L");
            endPriceValues = "900000";
        } else if (rightPinIndex == 10) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "10L");
            endPriceValues = "1000000";
        } else if (rightPinIndex == 12) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "20L");
            endPriceValues = "2000000";
        } else if (rightPinIndex == 13) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "30L");
            endPriceValues = "3000000";//
        } else if (rightPinIndex == 14) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "40L");
            endPriceValues = "4000000";
        } else if (rightPinIndex == 15) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "50L");
            endPriceValues = "5000000";
        } else if (rightPinIndex == 16) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "60L");
            endPriceValues = "6000000";
        } else if (rightPinIndex == 17) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "70L");
            endPriceValues = "7000000";
        } else if (rightPinIndex == 18) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "80L");
            endPriceValues = "8000000";
        } else if (rightPinIndex == 19) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "90L");
            endPriceValues = "9000000";
        } else if (rightPinIndex == 20) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "1 Cr");
            endPriceValues = "10000000";
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
            Application.getInstance().trackScreenView(activity,GlobalText.StockFilter_Activity);
        }else{
            Application.getInstance().trackScreenView(activity,GlobalText.asm_stock_filter);
        }

    }

    private void priceRefill() {

        if (CommonMethods.getstringvaluefromkey(activity, "certifiedval").equalsIgnoreCase("true")) {
            certifiedValues.setVisibility(View.VISIBLE);
            certifiedLevel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
            certified = false;

        } else {
            certifiedValues.setVisibility(View.GONE);
            certifiedLevel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
            certified = true;

        }

        if (CommonMethods.getstringvaluefromkey(activity, "price_valuesval").equalsIgnoreCase("true")) {

            price_values.setVisibility(View.VISIBLE);
            price_arrow.setBackgroundResource(R.drawable.up_arrow);
            price = false;
        } else {
            price_values.setVisibility(View.GONE);
            price_arrow.setBackgroundResource(R.drawable.down_arrow);
            price = true;
        }
        if (CommonMethods.getstringvaluefromkey(activity, "yearval").equalsIgnoreCase("true")) {

            year_values.setVisibility(View.VISIBLE);
            yearicon.setBackgroundResource(R.drawable.up_arrow);
            yearFlag = false;
        } else {
            year_values.setVisibility(View.GONE);
            yearicon.setBackgroundResource(R.drawable.down_arrow);
            yearFlag = true;
        }

        if (CommonMethods.getstringvaluefromkey(activity, "kmsval").equalsIgnoreCase("true")) {
            kmsValues.setVisibility(View.VISIBLE);
            kmsLelel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
            kms = false;

        } else {
            kmsValues.setVisibility(View.GONE);
            kmsLelel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
            kms = true;

        }

        year_seekbar.getProgress();
        Log.e("seekpro", year_seekbar.getProgress() + "");

        twoseekar.setRangePinsByValue(start, end);
        Log.e("kilometerValues=", "kilometerValues=" + kilometerValues);
        Log.e("yearValues=", "yearValues=" + yearValues);

        if (CommonMethods.getstringvaluefromkey(this, "startpricedata").equalsIgnoreCase("")) {
            CommonMethods.setvalueAgainstKey(activity, "startpricedata", startPriceValues);
        }

        if (CommonMethods.getstringvaluefromkey(this, "endpricedata").equalsIgnoreCase("")) {
            CommonMethods.setvalueAgainstKey(activity, "endpricedata", endPriceValues);
        }

        if (CommonMethods.getstringvaluefromkey(this, "kilometerdata").equalsIgnoreCase("")) {
            CommonMethods.setvalueAgainstKey(activity, "kilometerdata", kilometerValues);
        }

        if (CommonMethods.getstringvaluefromkey(this, "yeardata").equalsIgnoreCase("")) {
            CommonMethods.setvalueAgainstKey(activity, "yeardata", yearValues);
        }

        startPriceValues = CommonMethods.getstringvaluefromkey(activity, "startpricedata").toString();
        endPriceValues = CommonMethods.getstringvaluefromkey(activity, "endpricedata").toString();
        kilometerValues = CommonMethods.getstringvaluefromkey(activity, "kilometerdata").toString();

        yearValues = CommonMethods.getstringvaluefromkey(activity, "yeardata").toString();

        /*if(CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("StockStore")) {

            startPriceValues = CommonMethods.getstringvaluefromkey(activity, "startpricedata").toString();
            endPriceValues = CommonMethods.getstringvaluefromkey(activity, "endpricedata").toString();
            kilometerValues = CommonMethods.getstringvaluefromkey(activity, "kilometerdata").toString();

            yearValues = CommonMethods.getstringvaluefromkey(activity, "yeardata").toString();

        }else{

            startPriceValues = CommonMethods.getstringvaluefromkey(activity, "bookstartpricedata").toString();
            endPriceValues = CommonMethods.getstringvaluefromkey(activity, "bookendpricedata").toString();
            kilometerValues = CommonMethods.getstringvaluefromkey(activity, "bookkilometerdata").toString();

            yearValues = CommonMethods.getstringvaluefromkey(activity, "bookyeardata").toString();
        }*/

        Log.e("startPriceValues", startPriceValues);
        Log.e("endPriceValues", endPriceValues);
        Log.e("kilometerValues", kilometerValues);
        Log.e("yearValues", yearValues);

        filter_seekbar.setProgress(Integer.parseInt(kilometerValues));
//        Code by Reena

        int kilm = Integer.parseInt(kilometerValues);

        progval = kilm;

        if (kilm == 0 || kilm <= 10000) {
            kmsvalues.setText("10,000" + " Kms");
        } else if (10000 < kilm && kilm <= 20000) {
            kmsvalues.setText("20,000" + " Kms");
        } else if (20000 < kilm && kilm <= 30000) {
            kmsvalues.setText("30,000" + " Kms");
        } else if (30000 < kilm && kilm <= 40000) {
            kmsvalues.setText("40,000" + " Kms");
        } else if (40000 < kilm && kilm <= 50000) {
            kmsvalues.setText("50,000" + " Kms");
        } else if (50000 < kilm && kilm <= 60000) {
            kmsvalues.setText("60,000" + " Kms");
        } else if (60000 < kilm && kilm <= 70000) {
            kmsvalues.setText("70,000" + " Kms");
        } else if (70000 < kilm && kilm <= 80000) {
            kmsvalues.setText("80,000" + " Kms");
        } else if (80000 < kilm && kilm <= 90000) {
            kmsvalues.setText("90,000" + " Kms");
        } else if (90000 < kilm && kilm <= 100000) {
            kmsvalues.setText("1,00,000" + " Kms");
        } else if (100000 < kilm && kilm <= 200000) {
            kmsvalues.setText("2,00,000" + " Kms");
        } else if (200000 < kilm && kilm <= 300000) {
            kmsvalues.setText("3,00,000" + " Kms");
        } else if (300000 < kilm && kilm <= 400000) {
            kmsvalues.setText("4,00,000" + " Kms");
        } else if (400000 < kilm && kilm <= 500000) {
            kmsvalues.setText("5,00,000" + " Kms");
        } else if (500000 < kilm && kilm <= 600000) {
            kmsvalues.setText("6,00,000" + " Kms");
        } else if (600000 < kilm && kilm <= 700000) {
            kmsvalues.setText("7,00,000" + " Kms");
        } else if (700000 < kilm && kilm <= 800000) {
            kmsvalues.setText("8,00,000" + " Kms");
        } else if (800000 < kilm && kilm <= 900000) {
            kmsvalues.setText("9,00,000" + " Kms");
        } else if (900000 < kilm && kilm <= 1000000) {
            kmsvalues.setText("10,00,000" + " Kms");
        }

        // kmsvalues.setText(" "+kilometerValues + " Kms");
//        End by Reena
        ///////////////////////////

        if (yearValues.equalsIgnoreCase("2002")) {
            year_seekbar.setProgress(0);
        } else if (yearValues.equalsIgnoreCase("2003")) {
            year_seekbar.setProgress(15);
        } else if (yearValues.equalsIgnoreCase("2004")) {
            year_seekbar.setProgress(20);
        } else if (yearValues.equalsIgnoreCase("2005")) {
            year_seekbar.setProgress(25);
        } else if (yearValues.equalsIgnoreCase("2006")) {
            year_seekbar.setProgress(30);
        } else if (yearValues.equalsIgnoreCase("2007")) {
            year_seekbar.setProgress(35);
        } else if (yearValues.equalsIgnoreCase("2008")) {
            year_seekbar.setProgress(40);
        } else if (yearValues.equalsIgnoreCase("2009")) {
            year_seekbar.setProgress(45);
        } else if (yearValues.equalsIgnoreCase("2010")) {
            year_seekbar.setProgress(55);
        } else if (yearValues.equalsIgnoreCase("2011")) {
            year_seekbar.setProgress(60);
        } else if (yearValues.equalsIgnoreCase("2012")) {
            year_seekbar.setProgress(65);
        } else if (yearValues.equalsIgnoreCase("2013")) {
            year_seekbar.setProgress(70);
        } else if (yearValues.equalsIgnoreCase("2014")) {
            year_seekbar.setProgress(75);
        } else if (yearValues.equalsIgnoreCase("2015")) {
            year_seekbar.setProgress(80);
        } else if (yearValues.equalsIgnoreCase("2016")) {
            year_seekbar.setProgress(85);
        } else if (yearValues.equalsIgnoreCase("2017")) {
            year_seekbar.setProgress(90);
        } else if (yearValues.equalsIgnoreCase("2018")) {
            year_seekbar.setProgress(100);
        }
//        Code by Reena, Change condition object name for year_seekbar
        year_change.setText(yearValues + " and Newer");
        ////////////////////////////////////


        Log.e("stratvaule", startPriceValues);
        Log.e("endalue", endPriceValues);
        if (startPriceValues.equalsIgnoreCase("100000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "1L");
            start = 5;
        } else if (startPriceValues.equalsIgnoreCase("200000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "2L");
            start = 10;
        } else if (startPriceValues.equalsIgnoreCase("300000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "3L");
            start = 15;
        } else if (startPriceValues.equalsIgnoreCase("400000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "4L");
            start = 20;
        } else if (startPriceValues.equalsIgnoreCase("500000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "5L");
            start = 25;
        } else if (startPriceValues.equalsIgnoreCase("600000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "6L");
            start = 30;
        } else if (startPriceValues.equalsIgnoreCase("700000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "7L");
            start = 35;
        } else if (startPriceValues.equalsIgnoreCase("800000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "8L");
            start = 40;
        } else if (startPriceValues.equalsIgnoreCase("900000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "9L");
            start = 45;
        } else if (startPriceValues.equalsIgnoreCase("1000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "10L");
            start = 50;
        } else if (startPriceValues.equalsIgnoreCase("2000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "20L");
            start = 55;
        } else if (startPriceValues.equalsIgnoreCase("3000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "30L");
            start = 60;
        } else if (startPriceValues.equalsIgnoreCase("4000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "40L");
            start = 65;
        } else if (startPriceValues.equalsIgnoreCase("5000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "50L");
            start = 70;
        } else if (startPriceValues.equalsIgnoreCase("6000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "60L");
            start = 75;
        } else if (startPriceValues.equalsIgnoreCase("7000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "70L");
            start = 80;
        } else if (startPriceValues.equalsIgnoreCase("8000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "80L");
            start = 85;
        } else if (startPriceValues.equalsIgnoreCase("9000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "90L");
            start = 90;
        } else if (startPriceValues.equalsIgnoreCase("10000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "1 Cr");
            start = 100;
        } else {
            startprice.setText(getResources().getString(R.string.rs) + " " + "0");
            start = 0;
        }

        if (endPriceValues.equalsIgnoreCase("100000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "1L");
            end = 5;
        } else if (endPriceValues.equalsIgnoreCase("200000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "2L");
            end = 10;
        } else if (endPriceValues.equalsIgnoreCase("300000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "3L");
            end = 15;
        } else if (endPriceValues.equalsIgnoreCase("400000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "4L");
            end = 20;
        } else if (endPriceValues.equalsIgnoreCase("500000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "5L");
            end = 25;
        } else if (endPriceValues.equalsIgnoreCase("600000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "6L");
            end = 30;
        } else if (endPriceValues.equalsIgnoreCase("700000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "7L");
            end = 35;
        } else if (endPriceValues.equalsIgnoreCase("800000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "8L");
            end = 40;
        } else if (endPriceValues.equalsIgnoreCase("900000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "9L");
            end = 45;
        } else if (endPriceValues.equalsIgnoreCase("1000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "10L");
            end = 50;
        } else if (endPriceValues.equalsIgnoreCase("2000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "20L");
            end = 55;
        } else if (endPriceValues.equalsIgnoreCase("3000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "30L");
            end = 60;
        } else if (endPriceValues.equalsIgnoreCase("4000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "40L");
            end = 65;
        } else if (endPriceValues.equalsIgnoreCase("5000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "50L");
            end = 70;
        } else if (endPriceValues.equalsIgnoreCase("6000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "60L");
            end = 75;
        } else if (endPriceValues.equalsIgnoreCase("7000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "70L");
            end = 80;
        } else if (endPriceValues.equalsIgnoreCase("8000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "80L");
            end = 85;
        } else if (endPriceValues.equalsIgnoreCase("9000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "90L");
            end = 90;
        } else if (endPriceValues.equalsIgnoreCase("10000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "1 Cr");
            end = 100;
        } else {
            endprice.setText(getResources().getString(R.string.rs) + " " + "0");
            end = 0;
        }

        twoseekar.setRangePinsByValue(start, end);


    }

    @OnClick(R.id.certified_level)
    public void test(View view) {

        if (certified) {
            certifiedValues.setVisibility(View.VISIBLE);
            certifiedLevel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
            certified = false;

        } else {
            certifiedValues.setVisibility(View.GONE);
            certifiedLevel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
            certified = true;
        }

    }

    @OnClick(R.id.filter_cetified_car)
    public void filter_cetified_car(View view) {
        CommonMethods.setvalueAgainstKey(activity, "certifiedval", "true");
        filterCetifiedCar.setBackgroundResource(R.drawable.my_button);
        filterCetifiedCar.setTextColor(getResources().getColor(R.color.Black));
        certifiedFlag = true;
        filterAllCar.setBackgroundResource(R.drawable.my_buttom_gray_light);
        filterAllCar.setTextColor(getResources().getColor(R.color.lgray));
    }

    @OnClick(R.id.filter_all_cars)
    public void filter_all_cars(View view) {

        CommonMethods.setvalueAgainstKey(activity, "certifiedval", "false");

        filterAllCar.setBackgroundResource(R.drawable.my_button);
        filterAllCar.setTextColor(getResources().getColor(R.color.Black));
        certifiedFlag = false;
        filterCetifiedCar.setBackgroundResource(R.drawable.my_buttom_gray_light);
        filterCetifiedCar.setTextColor(getResources().getColor(R.color.lgray));


    }

    @OnClick(R.id.year_card)
    public void year_card(View view) {

        if (yearFlag) {
            year_values.setVisibility(View.VISIBLE);
            yearicon.setBackgroundResource(R.drawable.up_arrow);

            yearFlag = false;
        } else {
            year_values.setVisibility(View.GONE);
            yearicon.setBackgroundResource(R.drawable.down_arrow);

            yearFlag = true;
        }
    }

    @OnClick(R.id.kms_level)
    public void kms(View view) {

        if (kms) {
            kmsValues.setVisibility(View.VISIBLE);
            kmsLelel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
            kms = false;
        } else {
            kmsValues.setVisibility(View.GONE);
            kmsLelel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
            kms = true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item, menu);
        MenuItem item = menu.findItem(R.id.share);
        item.setVisible(false);
        item = menu.findItem(R.id.add_image);
        item.setVisible(false);
        item = menu.findItem(R.id.delete_fea);
        item.setVisible(false);
        item = menu.findItem(R.id.stock_fil_clear);
        item.setVisible(true);
        item = menu.findItem(R.id.asmHome);
        item.setVisible(false);
        item = menu.findItem(R.id.asm_mail);
        item.setVisible(false);
        item = menu.findItem(R.id.notification_bell);
        item.setVisible(false);
        item = menu.findItem(R.id.notification_cancel);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;

            case R.id.stock_fil_clear:
                Log.e("tets", "test");

                if(!CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ASM)){
                    stock_photocountsFlag = false;
                    CommonMethods.setvalueAgainstKey(activity, "30Name","");
                    FilterInstance.getInstance().setNotification("");
                    CommonMethods.setvalueAgainstKey(activity, "thirtydaycheckbox", "false");
                    // CommonMethods.setvalueAgainstKey(activity, "thirtydaycheckbox", "false");
                    CommonMethods.setvalueAgainstKey(activity, "Notification_Switch", "false");

                    CommonMethods.setvalueAgainstKey(activity, "clear", "true");

                    CommonMethods.setvalueAgainstKey(activity, "photocounts", "false");
                    CommonMethods.setvalueAgainstKey(activity, "photocounts1", "false");
                    CommonMethods.setvalueAgainstKey(activity, "photocounts2", "false");
                    CommonMethods.setvalueAgainstKey(activity, "bookstartpricedata", startPriceValues);
                    CommonMethods.setvalueAgainstKey(activity, "bookendpricedata", endPriceValues);

                    //new ASM
                    CommonMethods.setvalueAgainstKey(activity, "statuslist", "");



                    Log.e("test", filter_seekbar.getProgress() + "");
                    kilometerValues = "1000000";
                    filter_seekbar.setProgress(Integer.parseInt(kilometerValues));
                    //        Code by Reena
                    kmsvalues.setText(" " + "10,00,000" + " Kms");

                    //year reena
                    yearValues = "2002";
                    if (yearValues.equalsIgnoreCase("2002")) {
                        year_seekbar.setProgress(0);
                    }
                    year_change.setText(yearValues + " and Newer");

                    photocounts.setChecked(false);
                    thirtydaycheckbox.setChecked(false);

                    //twoseekbar
                    start = 0;
                    end = 100;
                    twoseekar.setRangePinsByValue(start, end);

                    //allcar
                    certifiedFlag = false;
                    filterAllCar.setBackgroundResource(R.drawable.my_button);
                    filterAllCar.setTextColor(getResources().getColor(R.color.Black));
                    CommonMethods.setvalueAgainstKey(activity, "certifiedstatus", "allcar");

                    filterCetifiedCar.setBackgroundResource(R.drawable.my_buttom_gray_light);
                    filterCetifiedCar.setTextColor(getResources().getColor(R.color.lgray));

                    if (CommonMethods.getstringvaluefromkey(activity, "clear").equalsIgnoreCase("true")) {
                        CommonMethods.setvalueAgainstKey(activity, "certifiedval", "false");
                        CommonMethods.setvalueAgainstKey(activity, "price_valuesval", "false");
                        CommonMethods.setvalueAgainstKey(activity, "yearval", "false");
                        CommonMethods.setvalueAgainstKey(activity, "kmsval", "false");
                        CommonMethods.setvalueAgainstKey(activity, "clear", "false");
                    }
                /*CommonMethods.setvalueAgainstKey(activity,"certifiedval","false");
                CommonMethods.setvalueAgainstKey(activity,"price_valuesval","false");
                CommonMethods.setvalueAgainstKey(activity,"yearval","false");
                CommonMethods.setvalueAgainstKey(activity,"kmsval","false");*/

                    //  finish();

                    filterby_apply.performClick();

                }else {
                    filterby_apply.performClick();
                }

                return true;

           /* case android.R.id.stock_fil_clear:
                finish();
                return true;*/
        }
        return super.onOptionsItemSelected(item);
    }

    public static void stockResetData(Activity activity) {

        activity = activity;
        stock_photocountsFlag = false;
        FilterInstance.getInstance().setNotification("");

        CommonMethods.setvalueAgainstKey(activity, "thirtydaycheckbox", "false");
        CommonMethods.setvalueAgainstKey(activity, "Notification_Switch", "false");
        CommonMethods.setvalueAgainstKey(activity, "clear", "true");
        CommonMethods.setvalueAgainstKey(activity, "filterapply", "false");

        CommonMethods.setvalueAgainstKey(activity, "photocounts", "false");
        CommonMethods.setvalueAgainstKey(activity, "photocounts1", "false");
        CommonMethods.setvalueAgainstKey(activity, "photocounts2", "false");

        kilometerValues = "1000000";
        yearValues = "2002";
        startPriceValues = "0";
        endPriceValues = "10000000";

        CommonMethods.setvalueAgainstKey(activity, "certifiedstatus", "allcar");

        CommonMethods.setvalueAgainstKey(activity, "certifiedval", "false");
        CommonMethods.setvalueAgainstKey(activity, "price_valuesval", "false");
        CommonMethods.setvalueAgainstKey(activity, "yearval", "false");
        CommonMethods.setvalueAgainstKey(activity, "kmsval", "false");
        CommonMethods.setvalueAgainstKey(activity, "clear", "false");

    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String type = intent.getStringExtra("type");

            Log.e("test receiver", type);
        }
    };
}
