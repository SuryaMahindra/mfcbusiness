package com.mfcwl.mfc_dealer.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.ASMModel.signCityRes;
import com.mfcwl.mfc_dealer.ASMModel.signupModel;
import com.mfcwl.mfc_dealer.ASMModel.signupRes;
import com.mfcwl.mfc_dealer.ASMModel.stateModel;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.retrofitconfig.SignupServices;
import com.mfcwl.mfc_dealer.retrofitconfig.StateServices;
import com.mfcwl.mfc_dealer.retrofitconfig.signcityService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class SignupActivity extends AppCompatActivity {

    public Button sign_up_button;
    public ImageView BackNavigation;
    public EditText sign_dealername, sign_name, sign_emailid, sign_phone, sign_add1, sign_add2, sign_pincode;
    public TextView sign_state, sign_city, sign_already;

    ArrayList<String> stateList;
    ArrayList<String> stateCode;

    String stateval = "1", cityVal = "1";
    String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        Log.i(TAG, "onCreate: ");
        sign_up_button = (Button) findViewById(R.id.sign_up_button);
        BackNavigation = (ImageView) findViewById(R.id.BackNavigation);
        sign_dealername = (EditText) findViewById(R.id.sign_dealername);
        sign_name = (EditText) findViewById(R.id.sign_name);
        sign_emailid = (EditText) findViewById(R.id.sign_emailid);
        sign_phone = (EditText) findViewById(R.id.sign_phone);
        sign_add1 = (EditText) findViewById(R.id.sign_add1);
        sign_add2 = (EditText) findViewById(R.id.sign_add2);
        sign_state = (TextView) findViewById(R.id.sign_state);
        sign_city = (TextView) findViewById(R.id.sign_city);
        sign_pincode = (EditText) findViewById(R.id.sign_pincode);
        sign_already = (TextView) findViewById(R.id.sign_already);


        sign_up_button.setOnClickListener(v -> {

            signRequest();
        });
        BackNavigation.setOnClickListener(v -> {
            finish();
        });
        sign_already.setOnClickListener(v -> {
            finish();
        });
        sign_state.setOnClickListener(v -> {
            stateListReq();
        });

        sign_city.setOnClickListener(v -> {

            if (!sign_state.getText().toString().trim().equalsIgnoreCase("")) {
                cityReq(sign_state.getText().toString().trim());
            } else {
                emptyValuation(sign_state);
            }

        });

    }

    private void signRequest() {

        char firstLetter = 0;
        if (!sign_phone.getText().toString().trim().isEmpty()) {
            String no = sign_phone.getText().toString().trim();
            firstLetter = no.charAt(0);
        }

        if (sign_dealername.getText().toString().trim().equalsIgnoreCase("")) {
            emptyValuation(sign_dealername);
        } else if (sign_name.getText().toString().trim().equalsIgnoreCase("")) {
            emptyValuation(sign_name);
        } else if (sign_emailid.getText().toString().trim().equalsIgnoreCase("")) {
            emptyValuation(sign_emailid);
        } else if (sign_phone.getText().toString().trim().equalsIgnoreCase("")) {
            emptyValuation(sign_phone);
        } else if (sign_add1.getText().toString().trim().equalsIgnoreCase("")) {
            emptyValuation(sign_add1);
        } else if (sign_add2.getText().toString().trim().equalsIgnoreCase("")) {
            emptyValuation(sign_add2);
        } else if (sign_state.getText().toString().trim().equalsIgnoreCase("")) {
            emptyValuation(sign_state);
        } else if (sign_city.getText().toString().trim().equalsIgnoreCase("")) {
            emptyValuation(sign_city);
        } else if (sign_pincode.getText().toString().trim().equalsIgnoreCase("")) {
            emptyValuation(sign_pincode);
        }else if (!isValidEmail(sign_emailid.getText().toString().trim())) {
            emptyValuation("Enter vaild email id");
        }else if (sign_phone.length() != 10) {
            emptyValuation("Enter Valid Phone Number");
        }else if (sign_pincode.length() != 6) {
            emptyValuation("Enter Valid Pin Code");
        }else if (Integer.parseInt(firstLetter + "") <= 5) {
            emptyValuation("Enter Valid Mobile Number");
        }else {

            signupModel req = new signupModel();
            req.setContactname(sign_dealername.getText().toString().trim());
            req.setName(sign_name.getText().toString().trim());
            req.setEmail(sign_emailid.getText().toString().trim());
            req.setMobile(sign_phone.getText().toString().trim());
            req.setAddress(sign_add1.getText().toString().trim());
            req.setAddress2(sign_add2.getText().toString().trim());
            req.setState(sign_state.getText().toString().trim());
            req.setStateId(Integer.valueOf(stateval));
            req.setCity(sign_city.getText().toString().trim());
            req.setCityId(Integer.valueOf(cityVal));
            req.setPincode(sign_pincode.getText().toString().trim());

            signupReqservice(SignupActivity.this, req);
        }
    }

    private void emptyValuation(EditText hint) {
        Toast.makeText(SignupActivity.this, hint.getHint().toString(), Toast.LENGTH_LONG).show();
    }
    private void emptyValuation(TextView hint) {
        Toast.makeText(SignupActivity.this, hint.getHint().toString(), Toast.LENGTH_LONG).show();
    }
    private void emptyValuation(String hint) {
        Toast.makeText(SignupActivity.this, hint, Toast.LENGTH_LONG).show();
    }

    private void signupReqservice(final Context mContext, final signupModel req) {
        SpinnerManager.showSpinner(mContext);
        SignupServices.signuService(mContext, req, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                Response<signupRes> mRes = (Response<signupRes>) obj;
                signupRes mData = mRes.body();
                String msg = mData.getMessage();

                if (mData.getStatus().equalsIgnoreCase("success")) {
                    CommonMethods.alertMessage2(SignupActivity.this,msg);
                } else {
                    CommonMethods.alertMessage((Activity) mContext, msg);
                }
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                try {
                    SpinnerManager.hideSpinner(mContext);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void stateListReq() {

        SpinnerManager.showSpinner(SignupActivity.this);
        StateServices.getStateList(new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(SignupActivity.this);
                Response<List<stateModel>> mRes = (Response<List<stateModel>>) obj;
                List<stateModel> mData = mRes.body();
                /* stateModel mActualdata = mData.get(0); */
                Log.i(TAG, "OnSuccess=1: " + mRes.body());

                stateList = new ArrayList<String>();
                stateCode = new ArrayList<String>();

                for (int i = 0; i < mData.size(); i++) {
                    try {
                        stateList.add(mData.get(i).getStatename());
                        stateCode.add(String.valueOf(mData.get(i).getStatecode()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                MethodCityListPopup(stateList, stateCode, "State");
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(SignupActivity.this);
            }
        });

    }

    public void MethodCityListPopup(final ArrayList citylist, final ArrayList citycode, String title) {


        final Dialog dialog_data = new Dialog(SignupActivity.this, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        TextView alertdialog_edittext = (EditText) dialog_data.findViewById(R.id.alertdialog_edittext);
        searchtittle.setText(title + " List");
        alertdialog_edittext.setHint("Search " + title);

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

        ImageView dialog_back_btn = dialog_data.findViewById(R.id.backicon);
        dialog_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_data.dismiss();

            }
        });

        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    //   dialog_data.cancel();
                }
            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(SignupActivity.this, android.R.layout.simple_list_item_1, citylist);
        alertdialog_Listview.setAdapter(adapter);

        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                String name = a.getAdapter().getItem(position).toString();
                TextView textview = v.findViewById(android.R.id.text1);
                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                if (title.equalsIgnoreCase("state")) {
                    sign_state.setText(name);
                    stateval = (String) citycode.get(position);
                } else if (title.equalsIgnoreCase("city")) {
                    cityVal = (String) citycode.get(position);
                    sign_city.setText(name);
                }

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    //  dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);

            }
        });

        dialog_data.show();
    }


    private void cityReq(String state) {

        SpinnerManager.showSpinner(SignupActivity.this);

        signcityService.getCity(state,new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                SpinnerManager.hideSpinner(SignupActivity.this);
                Response<List<signCityRes>> mRes = (Response<List<signCityRes>>) obj;
                List<signCityRes> mData = mRes.body();

                stateList = new ArrayList<String>();
                stateCode = new ArrayList<String>();

                for (int i = 0; i < mData.size(); i++) {
                    try {
                        stateList.add(mData.get(i).getCityname());
                        stateCode.add(String.valueOf(mData.get(i).getCitycode()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                MethodCityListPopup(stateList, stateCode, "City");

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(SignupActivity.this);
            }

        });
    }

    public  boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
