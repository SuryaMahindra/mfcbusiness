package com.mfcwl.mfc_dealer.Activity.RDR;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.RequestModel.LogOutRequest;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.AppConstants;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GAConstants;
import com.mfcwl.mfc_dealer.Utility.Helper;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.retrofitconfig.RDRService;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.DealerReportRequest;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.DiscussionPoint;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.ScreenInformation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import imageeditor.base.BaseActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.activity;

public class DiscussionPoint1 extends BaseActivity implements Callback<Object> {

    TextView tvDateTime, tvMeetingLink, tvDealerName, tvFocusArea, tvSubFA, Stockscertified, Stockswithoutimage,tvStockTitle;
    ImageView createRepo_backbtn;
    TextView tvSalesTarget, tvStocksTarget;
    TextView tvStoreSpaceOccupiedCount, tvPaidUpStocksCount, tvParkSellStocksCount, tvFreeSpaceCount;
    TextView tvStoreSpaceOccupiedPercent, tvPaidUpStocksPercent, tvParkSellStocksPercent, tvFreeSpacePercent;
    TextView tvActionItems, tvResponsibilities, tvTargetDateCalendar, tvPrevious, tvNext, tvFooterHeading;

    //R4
    TextView tv_dealer_operational_val;
    EditText et_current_stock_dealership_val;
    public Dialog dialog;
    LinearLayout ll_dp1_content;
    RelativeLayout layoutFooter;

    String strDealerOperational="",strCurrentStockDealership="";

    String dealerCatVal = "select";

    private Activitykiller mKiller;

    private LinearLayout llresandtarget;
  public static   String dealerCode = "", mDealerName = "",meetinglink = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.discussion_point1);
        Log.i(TAG, "onCreate: ");
        initView();

        setListeners();

        mKiller = new Activitykiller();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(mKiller, intentFilter);
        getIntentData();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mKiller);
    }

    public void initView() {

        discussionPoints = new ArrayList<>();

        tvDateTime = findViewById(R.id.tvDateTime);
        tvMeetingLink = findViewById(R.id.mettinglink);
        tvDealerName = findViewById(R.id.tvDealer);
        tvFocusArea = findViewById(R.id.tvFocusArea);
        tvSubFA = findViewById(R.id.tvSubFA);
        tvSalesTarget = findViewById(R.id.tvSalesTarget);
        tvStocksTarget = findViewById(R.id.tvStocksTarget);
        Stockscertified = findViewById(R.id.Stockscertified);
        Stockswithoutimage = findViewById(R.id.Stockswithoutimage);
        createRepo_backbtn = findViewById(R.id.createRepo_backbtn);

        tvStockTitle=findViewById(R.id.tvStockTitle);

        tvStoreSpaceOccupiedCount = findViewById(R.id.tvStoreSpaceOccupiedCount);
        tvPaidUpStocksCount = findViewById(R.id.tvPaidUpStocksCount);
        tvParkSellStocksCount = findViewById(R.id.tvParkSellStockCount);
        tvFreeSpaceCount = findViewById(R.id.tvFreeSpaceStockCount);
        tvStoreSpaceOccupiedPercent = findViewById(R.id.tvStoreSpaceOccupiedPercent);
        tvPaidUpStocksPercent = findViewById(R.id.tvPaidUpStocksPercent);
        tvParkSellStocksPercent = findViewById(R.id.tvParkSellStockPercent);
        tvFreeSpacePercent = findViewById(R.id.tvFreeSpaceStockPercent);

        tvActionItems = findViewById(R.id.tvActionItemList);
        tvResponsibilities = findViewById(R.id.tvResponsibilityList);
        tvTargetDateCalendar = findViewById(R.id.tvTargetDateCalendar);

        tvPrevious = findViewById(R.id.tvPrevious);

        tvStockTitle.setTypeface(null, Typeface.BOLD);

        //R4
        tv_dealer_operational_val=findViewById(R.id.tv_dealer_operational_val);
        et_current_stock_dealership_val=findViewById(R.id.et_current_stock_dealership_val);
        ll_dp1_content=findViewById(R.id.ll_dp1_content);
        layoutFooter=findViewById(R.id.layoutFooter);

        tvPrevious.setVisibility(View.INVISIBLE);
        tvNext = findViewById(R.id.tvNext);
        tvFooterHeading = findViewById(R.id.tvFooterHeading);
        tvResponsibilities.setMovementMethod(new ScrollingMovementMethod());
        llresandtarget = findViewById(R.id.llresandtarget);
    }
    private void getIntentData() {
        if(getIntent()!=null)
        {
            dealerCode = getIntent().getExtras().getString(AppConstants.DEALER_CODE);
            mDealerName = getIntent().getExtras().getString(AppConstants.DEALER_NAME);
            meetinglink = getIntent().getExtras().getString(AppConstants.MEETING_LINK);
            if(meetinglink.isEmpty() && !URLUtil.isValidUrl(meetinglink) && !URLUtil.isNetworkUrl(meetinglink))
            {
                tvMeetingLink.setVisibility(View.GONE);
            }
        }

        dealerId = dealerCode;
        dealerName = mDealerName;
        tvDealerName.setText(mDealerName + "(" + dealerCode + ")");
        getScreenData(dealerCode);

        SpannableString setRDR = new SpannableString("Join Meeting on MTeams");
        setRDR.setSpan(new UnderlineSpan(), 0, setRDR.length(), 0);
        if(tvMeetingLink.getVisibility()==View.VISIBLE)
        {
            tvMeetingLink.setText(setRDR);
        }

    }

    public void setListeners() {
        //tvDealerName.setOnClickListener(this);
        if(tvMeetingLink.getVisibility()==View.VISIBLE)
        {
            tvMeetingLink.setOnClickListener(this);

        }

        tvPrevious.setOnClickListener(this);
        tvNext.setOnClickListener(this);
        tvActionItems.setOnClickListener(this);
        tvResponsibilities.setOnClickListener(this);
        tvTargetDateCalendar.setOnClickListener(this);
        createRepo_backbtn.setOnClickListener(this);
        tvDateTime.setText(getDateTime());

        //R4
        tv_dealer_operational_val.setOnClickListener(this);

        tvFocusArea.setText("Stock Management");
        tvSubFA.setText("Stock Adequacy");
        tvFooterHeading.setText("Discussion point 1");
        tvSalesTarget.setText("Sales Target:");


        tvStoreSpaceOccupiedCount.setText("");
        tvPaidUpStocksCount.setText("");
        tvParkSellStocksCount.setText("");
        tvFreeSpaceCount.setText("");
        tvStoreSpaceOccupiedPercent.setText("");
        tvPaidUpStocksPercent.setText("");
        tvParkSellStocksPercent.setText("");
        tvFreeSpacePercent.setText("");

        tvActionItems.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String data = s.toString();
                if (data.equalsIgnoreCase("No action required")) {
                    llresandtarget.setVisibility(View.GONE);
                    strResponsibility = "NA";
                    strTargetDate = "07/17/2020";
                    Application.getInstance().logASMEvent(GAConstants.AM_RDR_ID_MEETING_LINK,CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));

                } else {
                    llresandtarget.setVisibility(View.VISIBLE);
                    strResponsibility = "";
                    strTargetDate = "";
                    Application.getInstance().logASMEvent(GAConstants.AM_RDR_ID_MEETING_LINK,CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));
                }
            }
        });

    }

    private void getScreenData(final String code) {

        RDRService.fetchScreenData(code, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(DiscussionPoint1.this);

                try {
                    Response<ScreenDataResponse> mResponse = (Response<ScreenDataResponse>) obj;
                    dealerReportResponse = mResponse.body();
                    if (dealerReportResponse != null) {
                        setResponseOnUI(dealerReportResponse);
                    }

                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e.getMessage());
                    setResponseOnUI(dealerReportResponse);
                }
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(DiscussionPoint1.this);

            }
        });

    }


    private void setResponseOnUI(ScreenDataResponse response) {

        if (response.salesTarget != null) {
            tvSalesTarget.setText("Sales Target: " + Math.abs(Float.parseFloat(response.salesTarget)));
        } else {
            tvSalesTarget.setText("Sales Target: " + "");
        }

        if (response.stocksTarget != null) {
            tvStocksTarget.setText("Stocks Target: " + Math.abs(Float.parseFloat(response.stocksTarget)));
        } else {
            tvStocksTarget.setText("Stocks Target: ");
        }


        if (response.storeSpaceOccupied != null) {
            tvStoreSpaceOccupiedCount.setText("" + response.storeSpaceOccupied);
        } else {
            tvStoreSpaceOccupiedCount.setText("0");
        }

        if (response.storeSpaceOccupiedPercent != null) {
            tvStoreSpaceOccupiedPercent.setText("" + response.storeSpaceOccupiedPercent);

        } else {
            tvStoreSpaceOccupiedPercent.setText("0%");

        }
        if (response.paidUpStock != null) {
            tvPaidUpStocksCount.setText("" + response.paidUpStock);
        } else {
            tvPaidUpStocksCount.setText("0");
        }

        if (response.paidUpStockPercentage != null) {
            tvPaidUpStocksPercent.setText("" + response.paidUpStockPercentage);
        } else {
            tvPaidUpStocksPercent.setText("0%");
        }

        if (response.parkAndSellStock != null) {
            tvParkSellStocksCount.setText("" + response.parkAndSellStock);
        } else {
            tvParkSellStocksCount.setText("0");
        }

        if (response.parkAndSellStockPercentage != null) {
            String parkandsell = response.parkAndSellStockPercentage.replace("%", "");
            tvParkSellStocksPercent.setText("" + response.parkAndSellStockPercentage);
            if (Integer.parseInt(parkandsell) < 30) {
                tvParkSellStocksPercent.setTextColor(getResources().getColor(R.color.red));
            } else {
                tvParkSellStocksPercent.setTextColor(getResources().getColor(R.color.black));
            }

        } else {
            tvParkSellStocksPercent.setText("0%");
        }

        if (response.freeSpace != null) {
            String strFreeSpace=response.freeSpace;
            strFreeSpace=strFreeSpace.replaceAll("^\\-","");
            tvFreeSpaceCount.setText(strFreeSpace);
        } else {
            tvFreeSpaceCount.setText("0");
        }


        if (response.freeSpacePercentage != null) {
            String strFreeSpacePer=response.freeSpacePercentage;
            strFreeSpacePer=strFreeSpacePer.replaceAll("^\\-","");
            tvFreeSpacePercent.setText(strFreeSpacePer);
        } else {
            tvFreeSpacePercent.setText("0%");
        }


        if (response.stockscertified != null) {
            Stockscertified.setText(getResources().getString(R.string.certified_stocks) + response.stockscertified);
        } else {
            Stockscertified.setText(getResources().getString(R.string.certified_stocks) + "0");
        }

        if (response.stockswithoutimage != null) {
            Stockswithoutimage.setText(getResources().getString(R.string.stocks_without_image) + response.stockswithoutimage);
        } else {
            Stockswithoutimage.setText(getResources().getString(R.string.stocks_without_image) + "0");
        }

        // R4

       /* if(response.currentStock!=null)
        {
            Log.i(TAG, "setResponseOnUI: "+response.currentStock.toString());
            et_current_stock_dealership_val.setText(response.currentStock.toString());
        }
        else
        {*/
            et_current_stock_dealership_val.setText("");
        //}

    }


    private void setAlertDialog(View v, Activity a, final String strTitle, final String[] arrVal, final String[] codeVal) {

        AlertDialog.Builder alert = new AlertDialog.Builder(a);
        alert.setTitle(strTitle);
        alert.setItems(arrVal, (dialog, which) -> {

            if (arrVal[which].equalsIgnoreCase("Others") ||
                    arrVal[which].equalsIgnoreCase("Other")) {

                tvDealerName.setText("");
                dealerCatVal = "";
                Toast.makeText(getApplicationContext(), "please select valid dealer.", Toast.LENGTH_LONG).show();

            } else {

                dealerCatVal = codeVal[which];
                dealerId = codeVal[which];
                dealerName = arrVal[which];
                tvDealerName.setText(arrVal[which] + "(" + dealerCatVal + ")");
                tvActionItems.setText("");
                tvResponsibilities.setText("");
            }

            getScreenData(dealerCatVal);

        });
        alert.create();
        alert.show();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.createRepo_backbtn:
                Intent broadcastIntent = new Intent();
                broadcastIntent.setAction("com.package.ACTION_LOGOUT");
                sendBroadcast(broadcastIntent);
                Intent intent = new Intent(DiscussionPoint1.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
            case R.id.tvNext:
                if (tvDealerName.getText().toString().equalsIgnoreCase(""))
                    Toast.makeText(this, "Please select dealer", Toast.LENGTH_SHORT).show();
                else {
                    if (validate()) {
                        removeAnyDuplicates(tvSubFA.getText().toString());
                        discussionPoints.add(getDiscussionPoint());
                        startActivity(new Intent(this, DiscussionPoint2.class));
                    }
                }
                break;
            case R.id.tvActionItemList:
                if (dealerReportResponse != null && dealerReportResponse.stockAdequacy != null) {
                    setDialogMultiSelectionCheck(R.id.tvActionItemList, "Action Items", getStringArray(dealerReportResponse.stockAdequacy));
                } else {
                    Toast.makeText(DiscussionPoint1.this, "No data found", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.tvDealer:
                showDealerList(v);
                break;
            case R.id.tv_dealer_operational_val:
                setAlertDialog(this, "Dealer is Operational", tv_dealer_operational_val, getResources().getStringArray(R.array.yes_no));
                break;
            case R.id.mettinglink:
                try
                {
                    Application.getInstance().logASMEvent(GAConstants.AM_RDR_ID_START,CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(meetinglink));
                    startActivity(i);
                }catch(Exception ex)
                {
                    ex.printStackTrace();
                    Toast.makeText(activity,"URL not found",Toast.LENGTH_LONG).show();
                    finish();
                }
                break;


        }
    }

    public DiscussionPoint getDiscussionPoint() {
        DiscussionPoint discussionPoint = new DiscussionPoint();
        discussionPoint.setActionItem(Arrays.asList(strActionItem.split(("\\|"))));
        discussionPoint.setResponsibility(strResponsibility);
        discussionPoint.setTargetDate(strTargetDate);
        discussionPoint.setFocusArea(tvFocusArea.getText().toString());
        discussionPoint.setSubFocusArea(tvSubFA.getText().toString());

        ScreenInformation info = new ScreenInformation();
        info.setStoreSpaceOccupied(tvStoreSpaceOccupiedCount.getText().toString());
        info.setPaidUpStock(tvPaidUpStocksCount.getText().toString());
        info.setParkAndSellStock(tvParkSellStocksCount.getText().toString());
        info.setFreeSpace(tvFreeSpaceCount.getText().toString());
        info.setStocksTarget(dealerReportResponse.stocksTarget);
        info.setSalesTarget(dealerReportResponse.salesTarget);
        info.setStockscertified(dealerReportResponse.stockscertified);
        info.setStockswithoutimage(dealerReportResponse.stockswithoutimage);
        info.setCurrentStockDealerShip(strCurrentStockDealership);
        discussionPoint.setScreenInformation(info);

       return discussionPoint;
    }

    private boolean validate() {

        strActionItem = tvActionItems.getText().toString();
        strCurrentStockDealership=et_current_stock_dealership_val.getText().toString();
        if (!strActionItem.equalsIgnoreCase("No action required")) {
            strResponsibility = tvResponsibilities.getText().toString();
            strTargetDate = tvTargetDateCalendar.getText().toString();
            strDealerOperational=tv_dealer_operational_val.getText().toString();

        }

        boolean allSelected = true;

        if (strActionItem.equals("") ||
                strResponsibility.equals("") ||
                strTargetDate.equals("")||strCurrentStockDealership.equals("")) {
            allSelected = false;
            Toast.makeText(this, "Please select all fields", Toast.LENGTH_SHORT).show();
        }

        return allSelected;
    }


    private void showDealerList(View v) {

        try {

            List<String> dealerName = new ArrayList<>();
            dealerName.clear();

            dealerName = Helper.dealerName;


            List<String> dealerCode = new ArrayList<String>();
            dealerCode.clear();
            dealerCode = Helper.dealerCode;
            String[] namesArr = dealerName.toArray(new String[dealerName.size()]);

            String[] codeArr = dealerCode.toArray(new String[dealerCode.size()]);

            setAlertDialog(v, DiscussionPoint1.this, "Dealer Category", namesArr, codeArr);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }





    private void makeFieldsInvisible()
    {
        if(strDealerOperational.equalsIgnoreCase("No"))
        {
            ll_dp1_content.setVisibility(View.INVISIBLE);
            layoutFooter.setVisibility(View.INVISIBLE);
            submitAlert();
        }
        else
        {
            ll_dp1_content.setVisibility(View.VISIBLE);
            layoutFooter.setVisibility(View.VISIBLE);
        }

    }

    private void makeFieldsVisible()
    {
        ll_dp1_content.setVisibility(View.VISIBLE);
        layoutFooter.setVisibility(View.VISIBLE);

    }

    private void setAlertDialog(Activity a, final String strTitle,TextView textView, final String[] arrVal) {

        AlertDialog.Builder alert = new AlertDialog.Builder(a);
        alert.setTitle(strTitle);
        alert.setItems(arrVal, (dialog, which) -> {

            if (arrVal[which].equalsIgnoreCase("Others") ||
                    arrVal[which].equalsIgnoreCase("Other"))
            {

                textView.setText("");
                Toast.makeText(getApplicationContext(), "please select valid dealer.", Toast.LENGTH_LONG).show();


            } else {
                textView.setText(arrVal[which]);
                strDealerOperational=textView.getText().toString();
            }
            makeFieldsInvisible();

        });
        alert.create();
        alert.show();
    }
    public void submitAlert() {

            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            dialog.setContentView(R.layout.common_cus_dialog);

            TextView Message;
            Button cancel, Confirm;
            Message = dialog.findViewById(R.id.Message);
            cancel = dialog.findViewById(R.id.cancel);
            cancel.setText("CANCEL");
            Confirm = dialog.findViewById(R.id.Confirm);
            Confirm.setText("OK");

            Message.setText("Submit RDR");
            Confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    submitToWeb(getRequest());
                    dialog.dismiss();
                }
            });

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
            dialog.setCanceledOnTouchOutside(false);

    }
    private DealerReportRequest getRequest() {
        DealerReportRequest request = new DealerReportRequest();
        request.dealerCode = dealerReportResponse.dealerCode;
        request.followUpDate = "";
        request.videoUrl = "";
        request.points = new ArrayList<>();
        request.isOperational="No";
        return request;
    }

    @Override
    public void onResponse(Call<Object> call, Response<Object> response) {

    }

    @Override
    public void onFailure(Call<Object> call, Throwable t) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        Application.getInstance().logASMEvent(GAConstants.AM_RDR_ID_START,CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));

    }
}
