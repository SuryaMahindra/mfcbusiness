package com.mfcwl.mfc_dealer.Activity.RDR;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.mfcwl.mfc_dealer.Activity.RDR.Services.DiscussionPoint7;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GAConstants;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.DiscussionPoint;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.ScreenInformation;

import java.util.Arrays;

import imageeditor.base.BaseActivity;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.activity;

public class DiscussionPoint6 extends BaseActivity {

    TextView tvDateTime, tvDealerName, tvFocusArea, tvSubFA;
    ImageView createRepo_backbtn;
    TextView tvLeadsTarget, tvLeadsGenerated, tvLeadsWithStatus, tvLeadsMarked;
    TextView tvAuctionItems, tvResponsibilities, tvTargetDateCalendar, tvPrevious, tvNext, tvFooterHeading;
    EditText etReasonsForLoss;
    String strReasonForLoss = "";
    //R4
    EditText etNoOfQualifiedLeadsVal, etNoVehClosed,etRepeatLeadsVal,etWalkInVal;
    String strNoOfQualifiedLeads="", strNoVehClosed ="",strRepeatLeads="",strWalkIns="",strRetailWalkIn="",strBookingWalkIns="";

    private Activitykiller mKiller;
    private LinearLayout llresandtarget;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.discussion_point6);
        Log.i(TAG, "onCreate: ");

        initView();

        setListeners();

        mKiller = new Activitykiller();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(mKiller,intentFilter);
    }



    public void initView() {

        tvDateTime = findViewById(R.id.tvDateTime);
        tvDealerName = findViewById(R.id.tvDealer);
        tvFocusArea = findViewById(R.id.tvFocusArea);
        tvSubFA = findViewById(R.id.tvSubFA);
        createRepo_backbtn = findViewById(R.id.createRepo_backbtn);

        tvLeadsTarget = findViewById(R.id.tvLeadsTarget);
        tvLeadsGenerated = findViewById(R.id.tvLeadsGenerated);
        tvLeadsWithStatus = findViewById(R.id.tvLeadsWithStatus);
        tvLeadsMarked = findViewById(R.id.tvLeadsMarked);
        etReasonsForLoss = findViewById(R.id.etReasonsForLoss);

        tvAuctionItems = findViewById(R.id.tvActionItemList);
        tvResponsibilities = findViewById(R.id.tvResponsibilityList);
        tvTargetDateCalendar = findViewById(R.id.tvTargetDateCalendar);
        tvPrevious = findViewById(R.id.tvPrevious);
        tvNext = findViewById(R.id.tvNext);
        tvFooterHeading = findViewById(R.id.tvFooterHeading);
        llresandtarget = findViewById(R.id.llresandtarget);

        tvAuctionItems.setMovementMethod(new ScrollingMovementMethod());
        tvResponsibilities.setMovementMethod(new ScrollingMovementMethod());

        etNoOfQualifiedLeadsVal=findViewById(R.id.etNoOfQualifiedLeadsVal);
        etNoVehClosed=findViewById(R.id.etNoOfVehQualifiedLeadsVal);
        etRepeatLeadsVal=findViewById(R.id.etRepeatLeadsVal);
        etWalkInVal=findViewById(R.id.etWalkInVal);


    }

    public DiscussionPoint getDiscussionPoint() {
        DiscussionPoint discussionPoint = new DiscussionPoint();
        discussionPoint.setActionItem(Arrays.asList(strActionItem.split(("\\|"))));
        discussionPoint.setResponsibility(strResponsibility);
        discussionPoint.setTargetDate(strTargetDate);
        discussionPoint.setFocusArea(tvFocusArea.getText().toString());
        discussionPoint.setSubFocusArea(tvSubFA.getText().toString());

        ScreenInformation info = new ScreenInformation();

        info.setLeadsTarget(dealerReportResponse.leadsTarget);
        info.setLeadsGenerated(dealerReportResponse.leadsGenerated);
        info.setLeadsWalkin(dealerReportResponse.leadsWalkin);
        info.setWarrantyNaCasesFollowedUp(dealerReportResponse.warrantyNaCasesFollowedUp);
        info.setLeadsWalkinCold(dealerReportResponse.leadsWalkinCold);
        info.setQualilifiedLeads(strNoOfQualifiedLeads);
        info.setVehiclesClosed(strNoVehClosed);
        info.setRepeatLeads(strRepeatLeads);
        info.setWalkIns(strWalkIns);
        info.setReasonforloss(strReasonForLoss);
        discussionPoint.setScreenInformation(info);
        return discussionPoint;
    }

    private boolean validate() {

        strActionItem = tvAuctionItems.getText().toString();
        strNoOfQualifiedLeads=etNoOfQualifiedLeadsVal.getText().toString();
        strNoVehClosed= etNoVehClosed.getText().toString();
        strWalkIns=etWalkInVal.getText().toString();
        strRepeatLeads=etRepeatLeadsVal.getText().toString();

        if(!strActionItem.equalsIgnoreCase("No action required")){
            strResponsibility = tvResponsibilities.getText().toString();
            strTargetDate = tvTargetDateCalendar.getText().toString();
        }

        strReasonForLoss = etReasonsForLoss.getText().toString();

        boolean allSelected = true;

        if (strActionItem.equals("") ||
                strResponsibility.equals("") ||
                strTargetDate.equals("") ||
                strReasonForLoss.equals("")||strNoOfQualifiedLeads.equals("")||
                strNoVehClosed.equals("")||
                strRepeatLeads.equals("")||
        strWalkIns.equals(""))
        {
            allSelected = false;
            Toast.makeText(this, "Please select all fields", Toast.LENGTH_SHORT).show();
        }

        return allSelected;
    }

    public void setListeners() {

        tvPrevious.setOnClickListener(this);
        tvNext.setOnClickListener(this);
        tvAuctionItems.setOnClickListener(this);
        tvResponsibilities.setOnClickListener(this);
        tvTargetDateCalendar.setOnClickListener(this);
        createRepo_backbtn.setOnClickListener(this);



        tvDateTime.setText(getDateTime());
        tvDealerName.setText(dealerName + "(" + dealerId + ")");
        tvSubFA.setText("Leads Adequacy");
        tvFocusArea.setText("Web Sales Leads");
        tvFooterHeading.setText("Discussion point 6");

        if(dealerReportResponse.leadsTarget!=null)
        {
            tvLeadsTarget.setText(getResources().getString(R.string.leads_target) + dealerReportResponse.leadsTarget);
        }
        else
        {
            tvLeadsTarget.setText(getResources().getString(R.string.leads_target)+"0 " );
        }

        if(dealerReportResponse.leadsGenerated!=null)
        {
            tvLeadsGenerated.setText(getResources().getString(R.string.leads_generated) + dealerReportResponse.leadsGenerated);        }
        else
        {
            tvLeadsGenerated.setText(getResources().getString(R.string.leads_generated)+"0 ");
        }

        if(dealerReportResponse.leadsWalkin!=null) {
            tvLeadsWithStatus.setText(getResources().getString(R.string.leads_status) + dealerReportResponse.leadsWalkin);
        }
        else
        {
            tvLeadsWithStatus.setText(getResources().getString(R.string.leads_status) +"0");
        }

        if(dealerReportResponse.leadsWalkinCold!=null) {
            tvLeadsMarked.setText("Walkin/Testdrive leads marked Lost/Cold: " + dealerReportResponse.leadsWalkinCold);
        }
        else
        {
            tvLeadsMarked.setText("Walkin/Testdrive leads marked Lost/Cold: 0" );
        }



        /*if(dealerReportResponse.qualilifiedLeads!=null)
        {
            etNoOfQualifiedLeadsVal.setText(dealerReportResponse.qualilifiedLeads.toString());
        }else { etNoOfQualifiedLeadsVal.setText(""); }

        if(dealerReportResponse.vehiclesClosed!=null)
        {
            etNoVehClosed.setText(dealerReportResponse.vehiclesClosed.toString());
        }else
        {
            etNoVehClosed.setText("0");
        }

        if(dealerReportResponse.repeatLeads!=null)
        {
            etRepeatLeadsVal.setText(dealerReportResponse.repeatLeads.toString());
        }else
        {
            etRepeatLeadsVal.setText("");
        }*/
        if(dealerReportResponse.walkIns!=null)
        {
            etWalkInVal.setText(dealerReportResponse.walkIns.toString());
        }else
        {
            etWalkInVal.setText("");
        }

        tvAuctionItems.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String data = s.toString();
                if(data.equalsIgnoreCase("No action required")){
                    llresandtarget.setVisibility(View.GONE);
                    strResponsibility = "NA";
                    strTargetDate = "07/17/2020";
                    Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_ACTION_ITEM, CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));

                }else {
                    llresandtarget.setVisibility(View.VISIBLE);
                    strResponsibility = "";
                    strTargetDate = "";
                    Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_ACTION_ITEM, CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));

                }
            }
        });

    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       unregisterReceiver(mKiller);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.createRepo_backbtn:
                startActivity(new Intent(this, DiscussionPoint5.class));
                break;
            case R.id.tvPrevious:
                startActivity(new Intent(this, DiscussionPoint5.class));
                break;
            case R.id.tvNext:
                if (validate()) {
                    removeAnyDuplicates(tvSubFA.getText().toString());
                    discussionPoints.add(getDiscussionPoint());
                    startActivity(new Intent(this, DiscussionPoint7.class));
                }
                break;
            case R.id.tvActionItemList:
                if (dealerReportResponse != null && dealerReportResponse.leadsAdequacy != null) {
                    setDialogMultiSelectionCheck(R.id.tvActionItemList, "Action Items", getStringArray(dealerReportResponse.leadsAdequacy));
                }else {
                    Toast.makeText(DiscussionPoint6.this,"No data found",Toast.LENGTH_LONG).show();
                }
                break;
        }
    }




}