package com.mfcwl.mfc_dealer.Activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.InstanceCreate.ImageLibraryInstance;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddStockSucessActivity extends AppCompatActivity {


    public TextView uploadimage, todaydate;
    public TextView skipnow;
    @BindView(R.id.stId)
    public TextView stId;
    @BindView(R.id.stocklisting)
    public TextView stocklisting;
    public LinearLayout uploadString;

    String stockId = "", source = "",regno="";
    String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addstock_sucess);
        ButterKnife.bind(this);
        Log.i(TAG, "onCreate: ");
        Calendar calendar = Calendar.getInstance();

        int thisYear = calendar.get(Calendar.YEAR);
        //   Log.e("thisYear", "# thisYear : " + thisYear);

        int thisMonth = calendar.get(Calendar.MONTH);
        //  Log.e("", "@ thisMonth : " + thisMonth);

        int thisDay = calendar.get(Calendar.DAY_OF_MONTH);
        //  Log.e("", "$ thisDay : " + thisDay);


        uploadString = (LinearLayout) findViewById(R.id.uploadString);
        uploadimage = (TextView) findViewById(R.id.uploadimage);
        skipnow = (TextView) findViewById(R.id.skipnow);
        stocklisting = (TextView) findViewById(R.id.stocklisting);
        todaydate = (TextView) findViewById(R.id.todaydate);
        todaydate.setText(thisDay + " " + getMonthName(thisMonth) + " " + thisYear);

        stockId = getIntent().getExtras().getString("STCKID");
        source = getIntent().getStringExtra("source");
        regno = getIntent().getStringExtra("regno");

        try{
            regno = getIntent().getStringExtra("regno");
        }catch (Exception e){
            e.printStackTrace();
        }



        String[] splitstId = stockId.split(":", 10);
        if (splitstId.length == 1)
            stId.setText(splitstId[0]);
        else
            stId.setText(splitstId[1]);

        if (source.equalsIgnoreCase("c2c")) {
            uploadString.setVisibility(View.GONE);
            uploadimage.setVisibility(View.GONE);
            skipnow.setVisibility(View.INVISIBLE);
            stocklisting.setVisibility(View.VISIBLE);


        } else {

            uploadString.setVisibility(View.VISIBLE);
            uploadimage.setVisibility(View.VISIBLE);
            stocklisting.setVisibility(View.INVISIBLE);
            skipnow.setText("Skip Now");
        }


    }

    @OnClick(R.id.uploadimage)
    public void uploadimage(View view) {

        CommonMethods.setvalueAgainstKey(this, "stocklist", "false");

        Intent in_main = new Intent(AddStockSucessActivity.this, ImageUploadActivity1.class);


        Log.i(TAG, "stockiddd===: "+stId.getText().toString());

        in_main.putExtra("stock_id", stId.getText().toString());
        in_main.putExtra("video_url", "");
        in_main.putExtra("regno", regno);


        ImageLibraryInstance.getInstance().setUploadstatus("uploadstatus");
        CommonMethods.setvalueAgainstKey(this, "stock_id", stId.getText().toString());

        CommonMethods.setvalueAgainstKey(this, "WhichTab", "");

        startActivity(in_main);
        finish();
    }

    @OnClick(R.id.stocklisting)
    public void stocklisting(View view) {

        if (source.equalsIgnoreCase("c2c")) {

            CommonMethods.setvalueAgainstKey(this, "stockcreated", "true");
            Intent in_main = new Intent(AddStockSucessActivity.this, MainActivity.class);
            startActivity(in_main);
        }

    }

    @OnClick(R.id.skipnow)
    public void skipnow(View view) {

        CommonMethods.setvalueAgainstKey(this, "stockcreated", "true");
        //   FilterInstance.getInstance().setNotification("missing images");
        CommonMethods.setvalueAgainstKey(this, "stockfilterOnclick", "true");
        Intent in_main = new Intent(AddStockSucessActivity.this, MainActivity.class);
        startActivity(in_main);
        finish();

    }


    public static String getMonthName(int month) {
        switch (month + 1) {
            case 1:
                return "Jan";

            case 2:
                return "Feb";

            case 3:
                return "Mar";

            case 4:
                return "Apr";

            case 5:
                return "May";

            case 6:
                return "Jun";

            case 7:
                return "Jul";

            case 8:
                return "Aug";

            case 9:
                return "Sep";

            case 10:
                return "Oct";

            case 11:
                return "Nov";

            case 12:
                return "Dec";
        }

        return "";
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onResume() {
        super.onResume();

        Application.getInstance().trackScreenView(this,GlobalText.AddstocksuccessActivity);
    }

}
