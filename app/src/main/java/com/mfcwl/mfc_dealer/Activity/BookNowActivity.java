package com.mfcwl.mfc_dealer.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BookNowActivity extends AppCompatActivity {


    @BindView(R.id.booknowbtn)
    public Button booknowbtn;
    @BindView(R.id.storestocklbl)
    public TextView storestocklbl;
    @BindView(R.id.storestocktv)
    public TextView storestocktv;
    @BindView(R.id.saleexelbl)
    public TextView saleexelbl;
    @BindView(R.id.saleexetv)
    public TextView saleexetv;
    @BindView(R.id.sellingpricelbl)
    public TextView sellingpricelbl;
    @BindView(R.id.sellingpricetv)
    public TextView sellingpricetv;
    @BindView(R.id.financereqlbl)
    public TextView financereqlbl;
    @BindView(R.id.financereqtv)
    public TextView financereqtv;


    public Activity activity;
    public Context context;
    public String star = "<font color='#B40404'>*</font>";
    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.booknowpop);
        Log.i(TAG, "onCreate: ");
        ButterKnife.bind(this);
        activity = this;
        context = this;
        updateFontUI(context);

        storestocklbl.setText(Html.fromHtml("STORE STOCK " + star));
        saleexelbl.setText(Html.fromHtml("SALES EXECUTIVE " + star));
        sellingpricelbl.setText(Html.fromHtml("ACTUAL SELLING PRICE " + star));
        financereqlbl.setText(Html.fromHtml("FINANCE REQURIED " + star));

        sellingpricetv.setText((getResources().getString(R.string.rs) + " 2345452"));

        //CommonMethods.setvalueAgainstKey(this,"booknowbtn","false");
    }


    @OnClick(R.id.booknowbtn)
    public void booknowbtn(View view) {

        //CommonMethods.setvalueAgainstKey(this,"booknowbtn","true");

        Intent in_main = new Intent(BookNowActivity.this, StockStoreInfoActivity.class);
        startActivity(in_main);
    }


    private void updateFontUI(Context ctx) {

        ArrayList<Integer> arl = new ArrayList<Integer>();

        arl.add(R.id.storestocklbl);
        arl.add(R.id.storestocktv);
        arl.add(R.id.saleexelbl);
        arl.add(R.id.saleexetv);
        arl.add(R.id.sellingpricelbl);
        arl.add(R.id.sellingpricetv);
        arl.add(R.id.financereqlbl);
        arl.add(R.id.financereqtv);


        setFontStyle(arl, context);
    }

    public void setFontStyle(ArrayList<Integer> tv_list, Context ctx) {

        for (int i = 0; i < tv_list.size(); i++) {
            Typeface myTypeface = Typeface.createFromAsset(this.getAssets(), "lato_semibold.ttf");
            TextView myTextView = (TextView) findViewById(tv_list.get(i));
            myTextView.setTypeface(myTypeface);
        }


    }

    @Override
    protected void onResume() {
        super.onResume();

        Application.getInstance().trackScreenView(this,GlobalText.booknowActivity);
    }


}
