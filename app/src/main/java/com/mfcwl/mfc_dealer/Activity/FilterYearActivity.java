package com.mfcwl.mfc_dealer.Activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.mfcwl.mfc_dealer.Adapter.DividerItemDecoration;
import com.mfcwl.mfc_dealer.Adapter.FilterYearAdapter;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilterYearActivity extends AppCompatActivity {

    @BindView(R.id.filter_year_listview) public RecyclerView mRecyclerView;
    @BindView(R.id.toolbar) public Toolbar toolbar;
    @BindView(R.id.buttonbtn) public LinearLayout apply;
    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_year);
        Log.i(TAG, "onCreate: ");
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        List<String> listView = new ArrayList<>();
        listView.add("2012");
        listView.add("2011");
        listView.add("2013");
        listView.add("2014");
        listView.add("2012");
        listView.add("2011");
        listView.add("2013");
        listView.add("2014");
        listView.add("2012");
        listView.add("2011");
        listView.add("2013");
        listView.add("2014");
        listView.add("2012");
        listView.add("2011");
        listView.add("2013");
        listView.add("2014");

        FilterYearAdapter filterYearAdapter = new FilterYearAdapter(listView);

        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(filterYearAdapter);
        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();
        // put your code here...

        Application.getInstance().trackScreenView(this,GlobalText.FilterYearActivity);

    }

}
