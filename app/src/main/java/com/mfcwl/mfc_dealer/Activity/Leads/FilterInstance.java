package com.mfcwl.mfc_dealer.Activity.Leads;

import org.json.JSONArray;

public class FilterInstance {

    private static FilterInstance mInstance = null;
    private String ptoday;
    private String pyesterday;
    private String plast7days;
    private String plast15days;

    private String pchoosecustomdatefrom;
    private String pchoosecustomdateto;


    private String fchoosecustomdatefrom;
    private String fchoosecustomdateto;

    private String ftomorrow;
    private String ftoday;
    private String fyesterday;
    private String flast7days;

    private String lsopen;
    private String lshot;
    private String lswarm;
    private String lscold;
    private String lslost;
    private String lssold;
    private JSONArray leadstatusJsonArray ;

    public JSONArray getLeadstatusJsonArray() {
        return leadstatusJsonArray;
    }

    public void setLeadstatusJsonArray(JSONArray leadstatusJsonArray) {
        this.leadstatusJsonArray = leadstatusJsonArray;
    }

    private String followupallleads;


    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    private String notification;


    private FilterInstance() {
        ptoday = "";
        pyesterday = "";
        plast7days = "";
        plast15days = "";

        pchoosecustomdatefrom = "";
        pchoosecustomdateto = "";

        fchoosecustomdatefrom = "";
        fchoosecustomdateto = "";

        ftomorrow = "";
        ftoday = "";
        fyesterday = "";
        flast7days = "";

        lsopen = "";
        lshot = "";
        lswarm = "";
        lscold = "";
        lslost = "";
        lssold = "";
        notification = "";
        followupallleads = "";

    }

    public static FilterInstance getInstance() {
        if (mInstance == null) {
            mInstance = new FilterInstance();
        }
        return mInstance;
    }


    public String getPtoday() {
        return ptoday;
    }

    public void setPtoday(String ptoday) {
        this.ptoday = ptoday;
    }

    public String getPyesterday() {
        return pyesterday;
    }

    public void setPyesterday(String pyesterday) {
        this.pyesterday = pyesterday;
    }

    public String getPlast7days() {
        return plast7days;
    }

    public void setPlast7days(String plast7days) {
        this.plast7days = plast7days;
    }

    public String getPlast15days() {
        return plast15days;
    }

    public void setPlast15days(String plast15days) {
        this.plast15days = plast15days;
    }


    public String getFtomorrow() {
        return ftomorrow;
    }

    public void setFtomorrow(String ftomorrow) {
        this.ftomorrow = ftomorrow;
    }

    public String getFtoday() {
        return ftoday;
    }

    public void setFtoday(String ftoday) {
        this.ftoday = ftoday;
    }

    public String getFyesterday() {
        return fyesterday;
    }

    public void setFyesterday(String fyesterday) {
        this.fyesterday = fyesterday;
    }

    public String getFlast7days() {
        return flast7days;
    }

    public void setFlast7days(String flast7days) {
        this.flast7days = flast7days;
    }


    public String getLsopen() {
        return lsopen;
    }

    public void setLsopen(String lsopen) {
        this.lsopen = lsopen;
    }

    public String getLshot() {
        return lshot;
    }

    public void setLshot(String lshot) {
        this.lshot = lshot;
    }

    public String getLswarm() {
        return lswarm;
    }

    public void setLswarm(String lswarm) {
        this.lswarm = lswarm;
    }

    public String getLscold() {
        return lscold;
    }

    public void setLscold(String lscold) {
        this.lscold = lscold;
    }

    public String getLslost() {
        return lslost;
    }

    public void setLslost(String lslost) {
        this.lslost = lslost;
    }

    public String getLssold() {
        return lssold;
    }

    public void setLssold(String lssold) {
        this.lssold = lssold;
    }


    public String getPchoosecustomdatefrom() {
        return pchoosecustomdatefrom;
    }

    public void setPchoosecustomdatefrom(String pchoosecustomdatefrom) {
        this.pchoosecustomdatefrom = pchoosecustomdatefrom;
    }

    public String getPchoosecustomdateto() {
        return pchoosecustomdateto;
    }

    public void setPchoosecustomdateto(String pchoosecustomdateto) {
        this.pchoosecustomdateto = pchoosecustomdateto;
    }


    public String getFchoosecustomdatefrom() {
        return fchoosecustomdatefrom;
    }

    public void setFchoosecustomdatefrom(String fchoosecustomdatefrom) {
        this.fchoosecustomdatefrom = fchoosecustomdatefrom;
    }

    public String getFchoosecustomdateto() {
        return fchoosecustomdateto;
    }

    public void setFchoosecustomdateto(String fchoosecustomdateto) {
        this.fchoosecustomdateto = fchoosecustomdateto;
    }


}
