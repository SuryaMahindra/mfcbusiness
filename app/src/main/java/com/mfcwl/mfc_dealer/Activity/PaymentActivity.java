package com.mfcwl.mfc_dealer.Activity;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.Adapter.PaymentAdapter;
import com.mfcwl.mfc_dealer.Model.PaymentData;
import com.mfcwl.mfc_dealer.Model.PaymentReq;
import com.mfcwl.mfc_dealer.Model.PaymentRes;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.PaginationScrollListener;
import com.mfcwl.mfc_dealer.retrofitconfig.PaymentServices;

import java.util.List;

import retrofit2.Response;


public class PaymentActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = PaymentActivity.class.getSimpleName();
    // Object
    RecyclerView recycler_view;
    ProgressBar progressBar;

    List<PaymentData> paymentData;
    SwipeRefreshLayout procured_swipeRefresh;
    public static PaymentAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    TextView live_noResults;
    //Pagination
    private int counts = 100;
    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 1;
    private int currentPage = PAGE_START;
    Activity activity;
    TextView paycashorcheck;
    ImageView pay_back;
    Button payonline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payments);
        Log.i(TAG, "onCreate: ");
        InitUI();
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    private void InitUI() {


        paycashorcheck = findViewById(R.id.paycashorcheck);
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        progressBar = (ProgressBar) findViewById(R.id.main_progress);
        live_noResults = (TextView) findViewById(R.id.procured_noResults);
        pay_back = (ImageView) findViewById(R.id.pay_back);
        payonline = (Button) findViewById(R.id.payonline);

        procured_swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.procured_swipeRefresh);
        procured_swipeRefresh.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.CYAN);
        procured_swipeRefresh.setOnRefreshListener(this);
        SetAdapter();


        if (CommonMethods.isInternetWorking(PaymentActivity.this)) {
            SendRequest("loadfirst");
        } else {
            CommonMethods.alertMessage(PaymentActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
        }


        recycler_view.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {

                if (TOTAL_PAGES != 1) {
                    isLoading = true;
                    currentPage += 1;
                    SendRequest("loadNext");
                }
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        pay_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PaymentActivity.this, MainActivity.class));
                finish();
            }
        });

        paycashorcheck.setOnClickListener(v -> {
            startActivity(new Intent(PaymentActivity.this, OfflinePaymentDetails.class));
            finish();

        });

        payonline.setOnClickListener(v -> {

            /*startActivity(new Intent(PaymentActivity.this, OnlinePaymentActivity.class).putExtra("warantybalance",
                    CommonMethods.getstringvaluefromkey(PaymentActivity.this,"warrantybalance")));*/

            CommonMethods.alertMessage(PaymentActivity.this,"Coming soon.");
        });
    }

    private void SetAdapter() {
        linearLayoutManager = new LinearLayoutManager(PaymentActivity.this, LinearLayoutManager.VERTICAL, false);
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        if (adapter == null) {
            adapter = new PaymentAdapter(PaymentActivity.this);
        }
        recycler_view.setAdapter(adapter);
    }

    public static void onSerarchUpdate(String query) {
        if (adapter == null) {
        } else {
            adapter.filter(query);
        }
    }

    private void SendRequest(String load) {

        PaymentReq request = new PaymentReq();
        request.setPage(String.valueOf(currentPage));
        request.setPageitems(String.valueOf(counts));


       /* List<payWhere> where = new ArrayList<>();

        ArrayList<String> dealerCode = new ArrayList<String>();
        dealerCode.add(CommonMethods.getstringvaluefromkey(activity,"dealer_code"));

        payWhere pay =new payWhere();
        pay.setColumn("transaction_type");
        pay.setValue("online");
        pay.setOperator("=");

        where.add(pay);

        request.setWhere(where);

        request.setDateFrom("");
        request.setDateTo("");
        request.setDateField("created_on");*/


        procured_swipeRefresh.setRefreshing(false);


        if (CommonMethods.isInternetWorking(PaymentActivity.this)) {
            CallingProcured(request, load);
        } else {
            CommonMethods.alertMessage(PaymentActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
        }

    }

    private void CallingProcured(PaymentReq request, String load) {
        if (progressBar.getVisibility() == View.GONE) {
            progressBar.setVisibility(View.VISIBLE);
        }
        PaymentServices.paymentReq(PaymentActivity.this, request, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                if (progressBar.getVisibility() == View.VISIBLE) {
                    progressBar.setVisibility(View.GONE);
                }

                if (live_noResults.getVisibility() == View.VISIBLE) {
                    live_noResults.setVisibility(View.GONE);
                }
                live_noResults.setVisibility(View.GONE);

                Response<PaymentRes> mRes = (Response<PaymentRes>) obj;
                PaymentRes mData = mRes.body();

                if (!mData.getData().isEmpty()) {

                    if (load.equalsIgnoreCase("loadfirst")) {

                        adapter.clear();

                        TOTAL_PAGES = mData.getTotal();
                        paymentData = mData.getData();

                        if (!paymentData.isEmpty()) {

                            if (live_noResults.getVisibility() == View.VISIBLE) {
                                live_noResults.setVisibility(View.GONE);
                            }

                            adapter.addAll(paymentData);

                            live_noResults.setVisibility(View.GONE);

                            if (TOTAL_PAGES == 1) {

                                if (currentPage < TOTAL_PAGES) adapter.addLoadingFooter();
                                else isLastPage = true;

                            } else {

                                if (currentPage <= TOTAL_PAGES) adapter.addLoadingFooter();
                                else isLastPage = true;
                            }

                        } else {
                            if (live_noResults.getVisibility() == View.GONE) {
                                live_noResults.setVisibility(View.VISIBLE);
                            }
                            procured_swipeRefresh.setRefreshing(false);
                        }

                    } else {
                        adapter.removeLoadingFooter();
                        isLoading = false;
                        paymentData = mData.getData();
                        adapter.addAll(paymentData);

                        if (currentPage != TOTAL_PAGES) adapter.addLoadingFooter();
                        else isLastPage = true;

                        if (live_noResults.getVisibility() == View.VISIBLE) {
                            live_noResults.setVisibility(View.GONE);
                        }

                    }

                    live_noResults.setVisibility(View.GONE);

                } else {
                    if (live_noResults.getVisibility() == View.GONE) {
                        live_noResults.setVisibility(View.VISIBLE);
                    }

                }

                if (!adapter.isEmpty()) {
                    live_noResults.setVisibility(View.GONE);
                }
            }

            @Override
            public void OnFailure(Throwable t) {
                if (progressBar.getVisibility() == View.VISIBLE) {
                    progressBar.setVisibility(View.GONE);
                }

                if (live_noResults.getVisibility() == View.GONE) {
                    live_noResults.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onRefresh() {
        counts = 100;
        currentPage = PAGE_START;
        isLastPage = false;
        adapter.clear();
        TOTAL_PAGES = 1;

        if (CommonMethods.isInternetWorking(PaymentActivity.this)) {
            SendRequest("loadfirst");
        } else {
            CommonMethods.alertMessage(PaymentActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(PaymentActivity.this, MainActivity.class));
        finish();
    }

}
