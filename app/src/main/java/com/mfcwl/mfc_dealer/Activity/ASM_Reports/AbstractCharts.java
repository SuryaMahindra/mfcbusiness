package com.mfcwl.mfc_dealer.Activity.ASM_Reports;

import android.graphics.Color;
import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;

import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.graphdata.charts.PieChart;
import com.mfcwl.mfc_dealer.graphdata.components.Legend;

abstract class AbstractCharts extends AppCompatActivity  {

        public void setDataChart(PieChart chart) {
        try {
            setChartsInit(chart);
        } catch (Exception e) {
        }
    }

    abstract protected void setPieChart (PieChart chart);

    public void setChartsInit(PieChart chart){

        chart = findViewById(R.id.chart);

        chart.setUsePercentValues(true);
        chart.getDescription().setEnabled(false);
        chart.setExtraOffsets(35, 40, 35, 35);

        chart.setDragDecelerationFrictionCoef(0.95f);

        chart.setExtraOffsets(40.f, 20.f, 40.f, 20.f);

        chart.setDrawHoleEnabled(true);

       // chart.setHoleColor(Color.WHITE);

        //chart.setHoleColor(getResources().getColor(R.color.llorangel));
        chart.setHoleColor(getResources().getColor(R.color.white));

        chart.setBackgroundColor(getResources().getColor(R.color.white));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            chart.setOutlineAmbientShadowColor(getResources().getColor(R.color.graph_yellow));
        }

        chart.setTransparentCircleColor(Color.WHITE);
        chart.setTransparentCircleAlpha(110);

        chart.setHoleRadius(85f);
        chart.setTransparentCircleRadius(61f);

        chart.setDrawCenterText(true);

        chart.setRotationAngle(0);
        // enable rotation of the chart by touch
        chart.setRotationEnabled(true);
        chart.setHighlightPerTapEnabled(true);

        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setEnabled(false);

        setPieChart(chart);

    }

}
