package com.mfcwl.mfc_dealer.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.mfcwl.mfc_dealer.Activity.Leads.LeadsInstance;
import com.mfcwl.mfc_dealer.AddStockModel.CityMaster;
import com.mfcwl.mfc_dealer.AddStockModel.ColourMaster;
import com.mfcwl.mfc_dealer.AddStockModel.CommercialMakelist;
import com.mfcwl.mfc_dealer.AddStockModel.ModelDetails;
import com.mfcwl.mfc_dealer.AddStockModel.ModelValue;
import com.mfcwl.mfc_dealer.AddStockModel.Procurement;
import com.mfcwl.mfc_dealer.AddStockServices.YearService;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.RequestModel.ReqAddLead;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.ResponseModel.ResAddLead;
import com.mfcwl.mfc_dealer.RetrofitServices.AddLeadService;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.Global;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class AddLeadActivity extends AppCompatActivity {

    @BindView(R.id.add_savebtn)
    public Button add_savebtn;
    @BindView(R.id.addleadcancel)
    public Button addleadcancel;

    @BindView(R.id.leadtittles)
    public TextView leadtittles;
    @BindView(R.id.leadtypelbl)
    public TextView leadtypelbl;
    @BindView(R.id.vehiclelbl)
    public TextView vehiclelbl;
    public static TextView tvvehicletv;
    @BindView(R.id.leaddatelbl)
    public TextView leaddatelbl;
    @BindView(R.id.leaddatetv)
    public TextView leaddatetv;
    @BindView(R.id.leadstatustv)
    public TextView leadstatustv;
    @BindView(R.id.cusnametv)
    public EditText cusnametv;
    @BindView(R.id.cusmobilelbl)
    public TextView cusmobilelbl;
    @BindView(R.id.cusemailtv)
    public EditText cusemailtv;
    @BindView(R.id.cusaddresslbl)
    public TextView cusaddresslbl;
    @BindView(R.id.cusaddresstv)
    public EditText cusaddresstv;
    @BindView(R.id.pincodelbl)
    public TextView pincodelbl;
    @BindView(R.id.pincodetv)
    public TextView pincodetv;
    @BindView(R.id.colortv)
    public TextView colortv;
    /*@BindView(R.id.emailtv) public TextView emailtv;
    @BindView(R.id.emailvaildtv) public TextView emailvaildtv;*/
    @BindView(R.id.leadsourcelbl)
    public TextView leadsourcelbl;
    public static TextView leadsourcetv;
    @BindView(R.id.citylbl)
    public TextView citylbl;
    @BindView(R.id.citytv)
    public TextView citytv;
    @BindView(R.id.makelbl)
    public TextView makelbl;
    @BindView(R.id.maketv)
    public TextView maketv;
    @BindView(R.id.modellbl)
    public TextView modellbl;
    @BindView(R.id.cusbudgettv)
    public EditText cusbudgettv;
    @BindView(R.id.colorlbl)
    public TextView colorlbl;
    @BindView(R.id.remarklbl)
    public TextView remarklbl;
    @BindView(R.id.remarktv)
    public EditText remarktv;
    @BindView(R.id.follwdatelbl)
    public TextView follwdatelbl;
    @BindView(R.id.follwdatetv)
    public TextView follwdatetv;
    @BindView(R.id.exeinfolbl)
    public TextView exeinfolbl;
    @BindView(R.id.exeinfotv)
    public TextView exeinfotv;
    @BindView(R.id.cusnamelbl)
    public TextView cusnamelbl;
    @BindView(R.id.leadstatuslbl)
    public TextView leadstatuslbl;
    @BindView(R.id.cusemaillbl)
    public TextView cusemaillbl;
    @BindView(R.id.cusbudgetlbl)
    public TextView cusbudgetlbl;
    @BindView(R.id.leadtypetv)
    public TextView leadtypetv;
    @BindView(R.id.cusmobiletv)
    public EditText cusmobiletv;
    @BindView(R.id.leadstatusvaildtv)
    public TextView leadstatusvaildtv;
    @BindView(R.id.linearBack)
    public LinearLayout linearBack;


    static ArrayList<String> modelArr;
    static ArrayList<String> variantArr;

    public static String vehicleModel = "", vehicleVaraiant = "";

    public static TextView modeltv;
    public Activity activity;
    public Context context;
    public String star = "<font color='#B40404'>*</font>";

    String makeName = "", cityName = "", colourName = "", strDate = "", WhichDate = "", leadStatus = "", exeName = "", exeId = "";
    ArrayList<String> makeList;
    static ArrayList<String> modalVariant;

    ArrayList<String> cityList;
    ArrayList<String> cityCode;

    ArrayList<String> leadIdList;
    ArrayList<String> leadStatusList;


    ArrayList<String> colorList;
    ArrayList<String> colorIdList;

    ArrayList<String> exeIdList;
    ArrayList<String> exeNameList;
    ArrayList<String> exeNameIdList;


    public static boolean isoktorepo = false;
    ArrayAdapter<String> leadSourceAdapter;
    ArrayAdapter<String> vehiclesAdapter;

    @BindView(R.id.leadtypevaildtv)
    public TextView leadtypevaildtv;
    @BindView(R.id.vehicletypevaildtv)
    public TextView vehicletypevaildtv;
    @BindView(R.id.leaddatevaildtv)
    public TextView leaddatevaildtv;
    @BindView(R.id.namevaildtv)
    public TextView namevaildtv;
    @BindView(R.id.numbervaildtv)
    public TextView numbervaildtv;
    @BindView(R.id.emailvaildtv2)
    public TextView emailvaildtv2;

    @BindView(R.id.cityvaildtv)
    public TextView cityvaildtv;
    @BindView(R.id.makevaildtv)
    public TextView makevaildtv;
    @BindView(R.id.modelvariantvaildtv)
    public TextView modelvariantvaildtv;
    @BindView(R.id.cusbudgetvaildtv)
    public TextView cusbudgetvaildtv;
    @BindView(R.id.colorvaildtv)
    public TextView colorvaildtv;

    @BindView(R.id.followupdatevaildtv)
    public TextView followupdatevaildtv;
    @BindView(R.id.exenamevaildtv)
    public TextView exenamevaildtv;
    ProgressDialog pd;

    ArrayList<String> colorListRet = new ArrayList<>();
    ArrayList<String> colorIdRet = new ArrayList<>();

    ArrayList<String> cityListName = new ArrayList<>();
    ArrayList<String> cityListCode = new ArrayList<>();
String TAG = getClass().getSimpleName();

//update
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addlead_info1);
        Log.i(TAG, "onCreate: ");
        ButterKnife.bind(this);
        activity = this;
        context = this;
        Log.i(TAG, "onCreate: ");
        tvvehicletv = (TextView) findViewById(R.id.tvvehicletv);
        leadsourcetv = (TextView) findViewById(R.id.leadsourcetv);
        modeltv = (TextView) findViewById(R.id.modeltv);
        makeList = new ArrayList<String>();

        modalVariant = new ArrayList<String>();
        modelArr = new ArrayList<String>();
        variantArr = new ArrayList<String>();

        cityList = new ArrayList<String>();
        cityCode = new ArrayList<String>();

        leadIdList = new ArrayList<String>();
        leadStatusList = new ArrayList<String>();

        colorIdList = new ArrayList<String>();
        colorList = new ArrayList<String>();

        exeNameList = new ArrayList<String>();
        exeIdList = new ArrayList<String>();
        exeNameIdList = new ArrayList<String>();

        leadtypelbl.setText(Html.fromHtml("LEAD TYPE " + star));
        leaddatelbl.setText(Html.fromHtml("LEAD DATE " + star));
        vehiclelbl.setText(Html.fromHtml("VEHICLE " + star));
        cusnamelbl.setText(Html.fromHtml("CUSTOMER NAME " + star));
        cusmobilelbl.setText(Html.fromHtml("CUSTOMER MOBILE " + star));
        leadstatuslbl.setText(Html.fromHtml("LEAD STATUS " + star));
        cusemaillbl.setText(Html.fromHtml("CUSTOMER EMAIL ID " + star));
        //leadsourcelbl.setText(Html.fromHtml("LEAD SOURCE " + star));
        citylbl.setText(Html.fromHtml("CITY " + star));
        makelbl.setText(Html.fromHtml("MAKE " + star));
        modellbl.setText(Html.fromHtml("MODEL AND VARIANT" + star));
        //variantlbl.setText(Html.fromHtml("VARIANT "+star));
        cusbudgetlbl.setText(Html.fromHtml("CUSTOMER BUDGET " + star));
        colorlbl.setText(Html.fromHtml("COLOR " + star));
        follwdatelbl.setText(Html.fromHtml("FOLLOW DATE " + star));
        exeinfolbl.setText(Html.fromHtml("EXECUTIVE INFORMATION " + star));


        updateFontUI(context);
        modeltv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!maketv.getText().toString().equals("")) {
                    //Volley
                    // modelandvariant(view);

                    if (CommonMethods.isInternetWorking(activity)) {
                        if (modalVariant != null) {
                            modalVariant.clear();
                        }
                        //retrofit
                        fetchModelDetails(makeName, AddLeadActivity.this, activity);
                    } else {
                        CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
                    }

                } else {
                    Toast.makeText(activity, "Please select make ", Toast.LENGTH_LONG).show();
                }
            }
        });

        tvvehicletv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String[] leadvehlist = {"Private", "Commercial"};
                setAlertDialog(view, activity, "Vehicle", leadvehlist);

            }
        });

        leadsourcetv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] leadSource = {"Walk-In", "Hoarding", "Event", "Newspaper", "Internet", "Reference", "Customer to Customer"};
                setAlertDialog(view, activity, "Lead Source", leadSource);
            }
        });

        /*leadSourceAdapter = new ArrayAdapter<String>(this,R.layout.simple_spinner2,leadSource);
        spinleadsourcetv.setAdapter(leadSourceAdapter);*/

        /*vehiclesAdapter = new ArrayAdapter<String>(this,R.layout.simple_spinner2,vehicles);
        spinvehicletv.setAdapter(vehiclesAdapter);*/


        leadtypevaildtv.setVisibility(View.GONE);

        leaddatevaildtv.setVisibility(View.INVISIBLE);

        leadstatusvaildtv.setVisibility(View.GONE);

        namevaildtv.setVisibility(View.GONE);

        numbervaildtv.setVisibility(View.GONE);

        emailvaildtv2.setVisibility(View.GONE);

        cityvaildtv.setVisibility(View.GONE);

        makevaildtv.setVisibility(View.GONE);

        modelvariantvaildtv.setVisibility(View.GONE);

        cusbudgetvaildtv.setVisibility(View.GONE);

        colorvaildtv.setVisibility(View.GONE);

        followupdatevaildtv.setVisibility(View.GONE);

        exenamevaildtv.setVisibility(View.GONE);

        vehicletypevaildtv.setVisibility(View.GONE);

    }

    @OnClick(R.id.linearBack)
    public void back_btn(View view) {
        finish();
    }

    @OnClick(R.id.add_savebtn)
    public void add_savebtn(View view) {

        leadtypevaildtv.setVisibility(View.GONE);

        leaddatevaildtv.setVisibility(View.INVISIBLE);

        namevaildtv.setVisibility(View.GONE);

        numbervaildtv.setVisibility(View.GONE);

        emailvaildtv2.setVisibility(View.GONE);

        cityvaildtv.setVisibility(View.GONE);

        makevaildtv.setVisibility(View.GONE);

        modelvariantvaildtv.setVisibility(View.GONE);

        cusbudgetvaildtv.setVisibility(View.GONE);

        colorvaildtv.setVisibility(View.GONE);

        followupdatevaildtv.setVisibility(View.GONE);

        exenamevaildtv.setVisibility(View.GONE);

        vehicletypevaildtv.setVisibility(View.GONE);

        leadstatusvaildtv.setVisibility(View.GONE);

        if (leadtypetv.getText().toString().equals("")) {
            leadtypevaildtv.setVisibility(View.VISIBLE);
        } else if (tvvehicletv.getText().toString().equals("")) {
            vehicletypevaildtv.setVisibility(View.VISIBLE);
        } else if (leaddatetv.getText().toString().equals("")) {
            leaddatevaildtv.setVisibility(View.VISIBLE);
            Toast.makeText(this, "Enter a valid lead date", Toast.LENGTH_LONG).show();
        } else if (leadstatustv.getText().toString().equals("")) {
            leadstatusvaildtv.setVisibility(View.VISIBLE);
            Toast.makeText(this, "Enter a valid status", Toast.LENGTH_LONG).show();
        } else if (cusnametv.getText().toString().equals("")) {
            namevaildtv.setVisibility(View.VISIBLE);
            Toast.makeText(this, "Enter a valid name", Toast.LENGTH_LONG).show();
        } else if (cusmobiletv.getText().toString().trim().length() != 10) {
            numbervaildtv.setVisibility(View.VISIBLE);
            Toast.makeText(this, "Enter a valid mobile no.", Toast.LENGTH_LONG).show();
        } else if (cusemailtv.getText().toString().equals("")) {
            emailvaildtv2.setVisibility(View.VISIBLE);
            Toast.makeText(this, "Enter a valid e-mail ID", Toast.LENGTH_LONG).show();
        } else if (!Patterns.EMAIL_ADDRESS.matcher(cusemailtv.getText().toString()).matches()) {
            emailvaildtv2.setVisibility(View.VISIBLE);
            Toast.makeText(this, "Enter a valid e-mail ID", Toast.LENGTH_LONG).show();
        } else if (citytv.getText().toString().equals("")) {
            cityvaildtv.setVisibility(View.VISIBLE);
            Toast.makeText(this, "Select a city", Toast.LENGTH_LONG).show();
        } else if (maketv.getText().toString().equals("")) {
            makevaildtv.setVisibility(View.VISIBLE);
            Toast.makeText(this, "Select make", Toast.LENGTH_LONG).show();
        } else if (modeltv.getText().toString().equals("")) {
            modelvariantvaildtv.setVisibility(View.VISIBLE);
            Toast.makeText(this, "Select model and variant", Toast.LENGTH_LONG).show();
        } else if (cusbudgettv.getText().toString().equals("")) {
            cusbudgetvaildtv.setVisibility(View.VISIBLE);
            Toast.makeText(this, "Enter customer's budget", Toast.LENGTH_LONG).show();
        } else if (colortv.getText().toString().equals("")) {
            colorvaildtv.setVisibility(View.VISIBLE);
            Toast.makeText(this, "Select color", Toast.LENGTH_LONG).show();
        } else if (follwdatetv.getText().toString().equals("")) {
            followupdatevaildtv.setVisibility(View.VISIBLE);
            Toast.makeText(this, "Enter follow-up date", Toast.LENGTH_LONG).show();
        } else if (exeinfotv.getText().toString().equals("")) {
            exenamevaildtv.setVisibility(View.VISIBLE);
            Toast.makeText(this, "Select executive", Toast.LENGTH_LONG).show();
        } else if (!pincodetv.getText().toString().equalsIgnoreCase("") && pincodetv.getText().toString().length() < 6) {

            Toast.makeText(this, "Please enter 6 digit correct pincode", Toast.LENGTH_LONG).show();
        } else {

            // WebServicesCall.webCall(this, this, jsonMakeCreateLead(), "AddLead", GlobalText.POST);
            //retrofit call
            Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.add_lead, GlobalText.android);
//
            ReqAddLead mReqAddLead = new ReqAddLead();
            mReqAddLead.setLeadType(leadtypetv.getText().toString());
            mReqAddLead.setVehicleType(tvvehicletv.getText().toString());
            mReqAddLead.setLeadDate(leaddatetv.getText().toString());
            mReqAddLead.setCustomerName(cusnametv.getText().toString());
            mReqAddLead.setCustomerMobile(cusmobiletv.getText().toString());
            mReqAddLead.setCustomerEmail(cusemailtv.getText().toString());
            mReqAddLead.setPincode(pincodetv.getText().toString());
            mReqAddLead.setCustomerAddress(cusaddresstv.getText().toString());
            mReqAddLead.setCity(citytv.getText().toString());
            mReqAddLead.setMake(maketv.getText().toString());
            mReqAddLead.setModel(modeltv.getText().toString());
            mReqAddLead.setColour(colortv.getText().toString());
            mReqAddLead.setCustomerBudget(cusbudgettv.getText().toString());
            mReqAddLead.setLeadStatus(leadstatustv.getText().toString());
            if (!leadsourcetv.getText().toString().equals("Select Lead Source")) {
                mReqAddLead.setLeadSource(leadsourcetv.getText().toString());
            } else {
                mReqAddLead.setLeadSource("");
            }
            mReqAddLead.setRemark(remarktv.getText().toString());
            mReqAddLead.setFollowDate(follwdatetv.getText().toString());
            mReqAddLead.setSalesExecutiveId(exeId);
            mReqAddLead.setSalesExecutivename(exeinfotv.getText().toString());
            mReqAddLead.setCreatedByDevice("OMS App-android");

            // Log.e("Lead Request", mReqAddLead.toString());
            if (CommonMethods.isInternetWorking(activity)) {
                addLead(mReqAddLead, AddLeadActivity.this);

            } else {
                CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
            }

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        // put your code here...
        Application.getInstance().trackScreenView(activity,GlobalText.AddleadActivity);
    }

    public JSONObject jsonMakeCreateLead() {
        JSONObject jObj = new JSONObject();

        try {
            jObj.put("lead_type", leadtypetv.getText().toString());
            jObj.put("vehicle_type", tvvehicletv.getText().toString());
            jObj.put("lead_date", leaddatetv.getText().toString());
            jObj.put("customer_name", cusnametv.getText().toString());
            jObj.put("customer_mobile", cusmobiletv.getText().toString());
            jObj.put("customer_email", cusemailtv.getText().toString());
            jObj.put("pincode", pincodetv.getText().toString());
            jObj.put("customer_address", cusaddresstv.getText().toString());
            jObj.put("city", citytv.getText().toString());
            jObj.put("make", maketv.getText().toString());
            jObj.put("model", vehicleModel.toString());

            jObj.put("variant", vehicleVaraiant.toString());

            jObj.put("colour", colortv.getText().toString());
            jObj.put("customer_budget", cusbudgettv.getText().toString());

            jObj.put("lead_status", leadstatustv.getText().toString());
            if (!leadsourcetv.getText().toString().equals("Select Lead Source")) {
                jObj.put("lead_source", leadsourcetv.getText().toString());
            } else {
                jObj.put("lead_source", "");
            }
            jObj.put("remark", remarktv.getText().toString());
            jObj.put("follow_date", follwdatetv.getText().toString());
            jObj.put("sales_executive_id", exeId);
            jObj.put("executive_name", exeinfotv.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Log.e("CreatedLead", " PostJson" + jObj.toString());
        return jObj;

    }


    @OnClick(R.id.leadstatustv)
    public void lead_status(View view) {
        MethodofLeadStatus();
    }

    @OnClick(R.id.exeinfotv)
    public void exe_info(View view) {
        MethodofExecutiveInfo();
    }


    @OnClick(R.id.addleadcancel)
    public void addleadcancel(View view) {

        finish();
        /*Intent in_main = new Intent(AddLeadActivity.this, MainActivity.class);
        startActivity(in_main);*/
    }


    @OnClick(R.id.citytv)
    public void registrationCity(View view) {

        //Volley
        // MethodCityLoadData();

        if (CommonMethods.isInternetWorking(activity)) {
            //retrofit
            fetchCity("citylist", AddLeadActivity.this);
        } else {
            CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
        }


    }

    @OnClick(R.id.colortv)
    public void colorstv(View view) {
        //Volley
        // MethodColorLoadData();

        if (CommonMethods.isInternetWorking(activity)) {
            //Retrofit
            fetchColor("colour", AddLeadActivity.this);
        } else {
            CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
        }


    }

    private void MethodColorLoadData() {

        showing();
        if (colorIdList != null) {
            colorIdList.clear();
        }

        if (colorList != null) {
            colorList.clear();
        }

        JsonArrayRequest strReq = new JsonArrayRequest(Request.Method.GET, Global.stock_dealerURL + "master/colour", null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {


                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject data = response.getJSONObject(i);
                        colorIdList.add(data.getString("id"));
                        colorList.add(data.getString("colour"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                MethodColourListPopup(colorIdList, colorList);

                hiding();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                hiding();

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json; charset=utf-8");
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));
                Log.e("token ", "requestMethods " + CommonMethods.getstringvaluefromkey(activity, "token").toString());

                return headers;
            }
        };
       Application.getInstance().addToRequestQueue(strReq);


    }


    private void MethodofExecutiveInfo() {

        showing();

        if (exeNameList != null) {
            exeNameList.clear();
        }

        if (exeIdList != null) {
            exeIdList.clear();
        }

        JsonArrayRequest strReq = new JsonArrayRequest(Request.Method.GET, Global.stock_dealerURL + "dealer/active-executive-list/sales", null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                Log.e("MethodofExecutiveInfo ", "data " + response.toString());

                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject data = response.getJSONObject(i);
                        exeNameList.add(data.getString("Text"));
                        exeIdList.add(data.getString("Value"));
                        exeNameIdList.add(data.getString("Text") + "@" + data.getString("Value"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                MethodExeInfoListPopup(exeIdList, exeNameList, exeNameIdList);

                hiding();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("MethodofLeadStatus ", "error " + error.toString());

                hiding();

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json; charset=utf-8");
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));
                Log.e("token ", "requestMethods " + CommonMethods.getstringvaluefromkey(activity, "token").toString());

                return headers;
            }
        };
        Application.getInstance().addToRequestQueue(strReq);


    }


    private void MethodofLeadStatus() {


        if (leadIdList != null) {
            leadIdList.clear();
        }

        if (leadStatusList != null) {
            leadStatusList.clear();
        }

       /* leadStatusList.add("Hot");
        leadStatusList.add("Warm");
        leadStatusList.add("Cold");
        leadStatusList.add("Lost");*/

       // List<String> newList = new ArrayList<String>(MainActivity.statusList.size());
        leadStatusList.addAll(MainActivity.statusList);
        
        MethodLeadStatusListPopup(leadIdList, leadStatusList);

        /*JsonArrayRequest strReq = new JsonArrayRequest(Request.Method.GET, Global.stock_dealerURL+"master/leadstatus", null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                Log.e("MethodofLeadStatus ","data "+response.toString());

                for(int i=0;i<response.length();i++)
                {
                    try {
                        JSONObject data = response.getJSONObject(i);
                        leadIdList.add(data.getString("id"));
                        leadStatusList.add(data.getString("leadstatus"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                MethodLeadStatusListPopup(leadIdList,leadStatusList);



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("MethodofLeadStatus ","error "+error.toString());


            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json; charset=utf-8");
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("token", CommonMethods.getstringvaluefromkey(activity,"token"));
                Log.e("token ","requestMethods "+CommonMethods.getstringvaluefromkey(activity,"token").toString());

                return headers;
            }
        };
        MyApplication.getInstance().addToRequestQueue(strReq);*/


    }


    private void MethodCityLoadData() {

        showing();

        if (cityList != null) {
            cityList.clear();
        }

        if (cityCode != null) {
            cityCode.clear();
        }

        JsonArrayRequest strReq = new JsonArrayRequest(Request.Method.GET, Global.stock_dealerURL + "master/citylist", null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                Log.e("MethodCityLoadData ", "data " + response.toString());

                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject data = response.getJSONObject(i);
                        Log.e("city ", "name " + data.getString("cityname"));
                        cityList.add(data.getString("cityname"));
                        cityCode.add(data.getString("citycode"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                MethodCityListPopup(cityList, cityCode);

                hiding();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //   Log.e("MethodCityLoadData ", "error " + error.toString());
                hiding();

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json; charset=utf-8");
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));
                Log.e("token ", "requestMethods " + CommonMethods.getstringvaluefromkey(activity, "token").toString());

                return headers;
            }
        };
        Application.getInstance().addToRequestQueue(strReq);


    }


    private void updateFontUI(Context ctx) {

        ArrayList<Integer> arl = new ArrayList<Integer>();

        arl.add(R.id.leadtittles);
        arl.add(R.id.leadtypelbl);
        arl.add(R.id.vehiclelbl);
        arl.add(R.id.tvvehicletv);
        arl.add(R.id.leaddatelbl);
        arl.add(R.id.leaddatetv);

        //ddd
        arl.add(R.id.leadstatustv);
        arl.add(R.id.cusnametv);
        arl.add(R.id.cusmobiletv);
        arl.add(R.id.cusnamelbl);
        arl.add(R.id.cusmobilelbl);
        arl.add(R.id.cusemailtv);
        arl.add(R.id.cusaddresslbl);
        arl.add(R.id.cusaddresstv);
        arl.add(R.id.pincodelbl);
        arl.add(R.id.pincodetv);
        /*arl.add(R.id.emailtv);
        arl.add(R.id.emailvaildtv);*/
        arl.add(R.id.leadsourcelbl);
        arl.add(R.id.leadsourcetv);
        arl.add(R.id.citylbl);
        arl.add(R.id.citytv);
        arl.add(R.id.makelbl);
        arl.add(R.id.maketv);
        arl.add(R.id.modellbl);
        arl.add(R.id.modeltv);
        /*arl.add(R.id.variantlbl);
        arl.add(R.id.varianttv);*/
        arl.add(R.id.cusbudgettv);
        //arl.add(R.id.kmstv);
        arl.add(R.id.colorlbl);
        arl.add(R.id.colortv);
        arl.add(R.id.remarklbl);
        arl.add(R.id.remarktv);
        arl.add(R.id.follwdatelbl);
        arl.add(R.id.follwdatetv);
        arl.add(R.id.exeinfolbl);
        arl.add(R.id.exeinfotv);


        setFontStyle(arl, context);
    }

    public void setFontStyle(ArrayList<Integer> tv_list, Context ctx) {

        for (int i = 0; i < tv_list.size(); i++) {
            Typeface myTypeface = Typeface.createFromAsset(this.getAssets(), "lato_semibold.ttf");
            TextView myTextView = (TextView) findViewById(tv_list.get(i));
            myTextView.setTypeface(myTypeface);
        }

    }


    @OnClick(R.id.leaddatetv)
    public void setLeadDate(View view) {
        WhichDate = "LeadDate";
        follwdatetv.setText("");
        setDate(view);
    }

    @OnClick(R.id.follwdatetv)
    public void setFollowDate(View view) {
        WhichDate = "FollowDate";
        if (!leaddatetv.getText().toString().equals("")) {
            setDate1(view);
        } else {
            Toast.makeText(activity, "Please Select the Lead Date ", Toast.LENGTH_LONG).show();
        }
    }


    @OnClick(R.id.maketv)
    public void setMaketv(View view) {

        modeltv.setText("");
        //Volley
        // MethodOfMake();

        if (CommonMethods.isInternetWorking(activity)) {

            if (makeList != null) {
                makeList.clear();
            }
            //Retrofit
            fetchCommercialMakeList(AddLeadActivity.this);
        } else {
            CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
        }


    }


    public void modelandvariant(View view) {

        WebServicesCall.webCall1(this, this, jsonMake(), "LeadsGetModelVariant", makeName, GlobalText.GET);

    }

    public static JSONObject jsonMake() {
        JSONObject jObj = new JSONObject();
        try {
            jObj.put("", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jObj;
    }


    private void MethodOfMake() {

        showing();
        if (makeList != null) {
            makeList.clear();
        }

        JsonArrayRequest strReq = new JsonArrayRequest(Request.Method.GET, Global.stock_dealerURL + "master/makelist", null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject data = response.getJSONObject(i);
                        makeList.add(data.getString("make"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                hiding();
                MethodMakeListPopup(makeList);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                hiding();

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json; charset=utf-8");
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));
                Log.e("token ", "requestMethods " + CommonMethods.getstringvaluefromkey(activity, "token").toString());

                return headers;
            }
        };
        Application.getInstance().addToRequestQueue(strReq);


    }

    public void MethodMakeListPopup(final ArrayList makeList) {
        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        ImageView backicon = dialog_data.findViewById(R.id.backicon);
        backicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_data.dismiss();
            }
        });
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                }

            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        filterText.setHint("Search Make");

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        searchtittle.setText("Select Make");
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, makeList);
        alertdialog_Listview.setAdapter(adapter);

        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                makeName = a.getAdapter().getItem(position).toString();

                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                maketv.setText(makeName);

                if (dialog_data != null) {
                    dialog_data.dismiss();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });

        dialog_data.show();
    }


    public static void Parsegetmodalvariant(JSONObject jObj, Activity activity) {

        if (modalVariant != null) {
            modalVariant.clear();
        }

        if (modelArr != null) {
            modelArr.clear();
        }
        if (variantArr != null) {
            variantArr.clear();
        }

        try {
            JSONArray array = jObj.getJSONArray("model_values");
            for (int i = 0; i < array.length(); i++) {
                try {
                    JSONObject data = array.getJSONObject(i);
                    modalVariant.add(data.getString("display"));

                    modelArr.add(data.getString("model"));
                    variantArr.add(data.getString("variant"));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            // MethodModelListPopup(modalVariant, modelArr, variantArr, activity);
            MethodModelListPopup(modalVariant, activity);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void MethodColourListPopup(final ArrayList colorIdList, final ArrayList colorList) {

        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);
        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_data.getWindow().setGravity(Gravity.CENTER);
        dialog_data.setContentView(R.layout.citylist);

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());
        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        ImageView backicon = dialog_data.findViewById(R.id.backicon);
        backicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_data.dismiss();
                //   dialog_data.cancel();
            }
        });
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    //   dialog_data.cancel();
                }

            }
        });

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        searchtittle.setText("Select Colour");

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        filterText.setHint("Search Colour");
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, colorList);
        alertdialog_Listview.setAdapter(adapter);
        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                colourName = a.getAdapter().getItem(position).toString();

                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                colortv.setText(colourName);

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    //    dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });


        dialog_data.show();
    }


    public void MethodLeadStatusListPopup(final ArrayList leadIdList, final ArrayList leadStatusList) {


        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);
        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_data.getWindow().setGravity(Gravity.CENTER);
        dialog_data.setContentView(R.layout.citylist);
        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
        lp_number_picker.copyFrom(window.getAttributes());
        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

   /*     TextView alertdialog_textview = (TextView) dialog_data.findViewById(R.id.alertdialog_textview);
        alertdialog_textview.setText("City List");
        alertdialog_textview.setHint("search city");*/

        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        ImageView backicon = dialog_data.findViewById(R.id.backicon);
        backicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_data.dismiss();
                //    dialog_data.cancel();
            }
        });

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        searchtittle.setText("Select Lead Status");

        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    // dialog_data.cancel();
                }

            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        filterText.setHint("Search Lead Status");
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, leadStatusList);
        alertdialog_Listview.setAdapter(adapter);
        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                leadStatus = a.getAdapter().getItem(position).toString();

                Log.e("leadStatus ", "leadStatus " + leadStatus);


                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                leadstatustv.setText(leadStatus);

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    //  dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });


        dialog_data.show();
    }


    public void MethodExeInfoListPopup(final ArrayList exeIdList, final ArrayList exeNameList, final ArrayList exeNameIdList) {


        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);
        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        searchtittle.setText("Select Executive Information");
        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        ImageView backicon = dialog_data.findViewById(R.id.backicon);
        backicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_data.dismiss();
                //    dialog_data.cancel();
            }
        });
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    //     dialog_data.cancel();
                }

            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        filterText.setHint("Search Executive Info");
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, exeNameList);
        alertdialog_Listview.setAdapter(adapter);
        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                exeName = a.getAdapter().getItem(position).toString();


                for (int i = 0; i < exeNameIdList.size(); i++) {
                    String[] splitValues = exeNameIdList.get(i).toString().split("@", 10);
                    Log.e("splitValues ", "splitValues " + splitValues[0] + "   " + splitValues[1]);

                    if (exeName.equals(splitValues[0])) {
                        exeId = splitValues[1];
                    }

                }
                Log.e("exeId ", "procureVal " + exeId);


                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                exeinfotv.setText(exeName);

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    //    dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });


        dialog_data.show();
    }


    public void MethodCityListPopup(final ArrayList citylist, final ArrayList citycode) {

        Log.e("citycode ", "citycode " + citycode.toString());

        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

   /*     TextView alertdialog_textview = (TextView) dialog_data.findViewById(R.id.alertdialog_textview);
        alertdialog_textview.setText("City List");
        alertdialog_textview.setHint("search city");*/

        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        ImageView backicon = dialog_data.findViewById(R.id.backicon);
        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        searchtittle.setText("Select List");

        backicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_data.dismiss();
                //   dialog_data.cancel();
            }
        });
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    //       dialog_data.cancel();
                }

            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        filterText.setHint("Search City");
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, citylist);
        alertdialog_Listview.setAdapter(adapter);
        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                cityName = a.getAdapter().getItem(position).toString();

                Log.e("cityName ", "cityName " + cityName);


                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                citytv.setText(cityName);

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    //       dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });


        dialog_data.show();
    }


    public void setDate(View v) {

        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
            // when dialog box is closed, below method will be called.
            public void onDateSet(DatePicker view, int selectedYear,
                                  int selectedMonth, int selectedDay) {
                if (isoktorepo) {
                    myCalendar.set(Calendar.YEAR, selectedYear);
                    myCalendar.set(Calendar.MONTH, selectedMonth);
                    myCalendar.set(Calendar.DAY_OF_MONTH, selectedDay);

                    String strselectedDay = String.valueOf(selectedDay);
                    if (strselectedDay.length() == 1) {
                        strselectedDay = "0" + strselectedDay;
                    }
                    String strselectedMonth = String.valueOf(selectedMonth + 1);
                    if (strselectedMonth.length() == 1) {
                        strselectedMonth = "0" + strselectedMonth;
                    }


                    strDate = selectedYear + "-" + strselectedMonth + "-"
                            + strselectedDay;

                    if (WhichDate.equals("LeadDate")) {
                        leaddatetv.setText(strDate);
                    } else {
                        follwdatetv.setText(strDate);
                    }
                }


                isoktorepo = false;
            }
        };


        final DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                datePickerListener, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            dialog.cancel();
                            isoktorepo = false;
                        }
                    }
                });

        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    @SuppressLint("NewApi")
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            isoktorepo = true;

                            DatePicker datePicker = datePickerDialog
                                    .getDatePicker();
                            datePickerListener.onDateSet(datePicker,
                                    datePicker.getYear(),
                                    datePicker.getMonth(),
                                    datePicker.getDayOfMonth());


                        }
                    }
                });

        long currentTime = System.currentTimeMillis();
        datePickerDialog.getDatePicker().setMaxDate(currentTime);

        datePickerDialog.setCancelable(false);
        datePickerDialog.show();


    }


    public void setDate1(View v) {


        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
            // when dialog box is closed, below method will be called.
            public void onDateSet(DatePicker view, int selectedYear,
                                  int selectedMonth, int selectedDay) {
                if (isoktorepo) {
                    myCalendar.set(Calendar.YEAR, selectedYear);
                    myCalendar.set(Calendar.MONTH, selectedMonth);
                    myCalendar.set(Calendar.DAY_OF_MONTH, selectedDay);

                    String strselectedDay = String.valueOf(selectedDay);
                    if (strselectedDay.length() == 1) {
                        strselectedDay = "0" + strselectedDay;
                    }
                    String strselectedMonth = String.valueOf(selectedMonth + 1);
                    if (strselectedMonth.length() == 1) {
                        strselectedMonth = "0" + strselectedMonth;
                    }

                    strDate = selectedYear + "-" + strselectedMonth + "-"
                            + strselectedDay;

                    if (WhichDate.equals("LeadDate")) {
                        leaddatetv.setText(strDate);
                    } else {
                        follwdatetv.setText(strDate);
                    }
                }


                isoktorepo = false;
            }
        };


        final DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                datePickerListener, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            dialog.cancel();
                            isoktorepo = false;
                        }
                    }
                });

        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    @SuppressLint("NewApi")
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            isoktorepo = true;

                            DatePicker datePicker = datePickerDialog
                                    .getDatePicker();
                            datePickerListener.onDateSet(datePicker,
                                    datePicker.getYear(),
                                    datePicker.getMonth(),
                                    datePicker.getDayOfMonth());


                        }
                    }
                });
        long timeInMilliseconds = 0;
        String givenDateString = leaddatetv.getText().toString();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date mDate = sdf.parse(givenDateString);
            timeInMilliseconds = mDate.getTime();
            //Log.e("timeInMilliseconds ", "timeInMilliseconds " + timeInMilliseconds);
            //System.out.println("Date in milli :: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long currentTime = System.currentTimeMillis();
        datePickerDialog.getDatePicker().setMinDate(timeInMilliseconds);
        datePickerDialog.setCancelable(false);
        datePickerDialog.show();

    }


    public static void Parsecreateprivatelead(JSONObject jObj, Activity activity) {

        Log.e("CreatedLead ", "jobj " + jObj.toString());
        LeadsInstance.getInstance().setAddlead("RedirectLeadPage");
        try {
            if (jObj.getString("status").equalsIgnoreCase("SUCCESS")) {
                Toast.makeText(activity, " " + "Lead added successfully", Toast.LENGTH_LONG).show();
                activity.finish();
            } else {
                Toast.makeText(activity, " " + jObj.getString("message"), Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void showing() {

        /*pd = new ProgressDialog(activity);
        pd.setTitle("Loading...");
        pd.setMessage("Please Wait...");
        pd.setCancelable(false);
        pd.show();*/

        pd = new ProgressDialog(this);
        try {
            if (SplashActivity.progress) {
                pd.show();
            }
        } catch (WindowManager.BadTokenException e) {

        }
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        pd.setContentView(R.layout.progressdialog);

    }

    public void hiding() {
        if (pd.isShowing()) {
            pd.dismiss();
        }
    }

    private void setAlertDialog(View v, Activity a, final String strTitle, final String[] arrVal) {
        AlertDialog.Builder alert = new AlertDialog.Builder(a);
        alert.setTitle(strTitle);
        alert.setItems(arrVal, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (strTitle.equals("Vehicle")) {
                    tvvehicletv.setText(arrVal[which]);
                    maketv.setText("");
                    modeltv.setText("");
                } else if (strTitle.equals("Lead Source")) {
                    leadsourcetv.setText(arrVal[which]);
                }
            }


        });
        alert.create();
        alert.show();

    }

    // Retrofit
    public void fetchCity(String city, final Context mContext) {
        SpinnerManager.showSpinner(mContext);
        if (cityListName != null) {
            cityListName.clear();
        }

        if (cityListCode != null) {
            cityListCode.clear();
        }
        YearService.getCityFromServer(city, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                try {
                    retrofit2.Response<List<CityMaster>> mResponse = (retrofit2.Response<List<CityMaster>>) obj;


                    List<CityMaster> cityList = mResponse.body();
                    for (int i = 0; i < cityList.size(); i++) {

                        cityListName.add(cityList.get(i).getCityname());
                        cityListCode.add(String.valueOf(cityList.get(i).getCitycode()));
                        //  Log.e(TAG, "Master city: " + cityList.get(i).getCityname() + " " + cityList.get(i).getCitycode());
                    }

                    MethodCityListPopup(cityListName, cityListCode);
                } catch (Exception e) {

                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
            }
        });
    }

    //makelist and commercial-makelist
    public void fetchCommercialMakeList(final Context mContext) {
        SpinnerManager.showSpinner(mContext);
        YearService.getMakelistFromServer(new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                try {
                    retrofit2.Response<List<CommercialMakelist>> mResponse = (retrofit2.Response<List<CommercialMakelist>>) obj;

                    List<CommercialMakelist> commercialList = mResponse.body();
                    for (int i = 0; i < commercialList.size(); i++) {
                        // Log.e(TAG, "MakeList : " + commercialList.get(i).getMake());
                        makeList.add(commercialList.get(i).getMake());
                    }

                    MethodMakeListPopup(makeList);
                } catch (Exception e) {
                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
            }
        });

    }

    //model details and commercial
    public void fetchModelDetails(String make, Context mContext, Activity a) {
        SpinnerManager.showSpinner(mContext);
        YearService.getModelDetailsFromServer(make, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                try {
                    retrofit2.Response<ModelDetails> mResponse = (retrofit2.Response<ModelDetails>) obj;
                    List<ModelValue> modelValues = new ArrayList<>();
                    ModelDetails list = mResponse.body();
                    modelValues = list.getModelValues();

                    for (int i = 0; i < modelValues.size(); i++) {
                        // Log.e(TAG, "Model Details: " + modelValues.get(i).getModel() + " " + modelValues.get(i).getVariant());
                        modalVariant.add(modelValues.get(i).getDisplay());

                    }
                    MethodModelListPopup(modalVariant, activity);
                } catch (Exception e) {
                    // Log.e(TAG, "model: " + e.getMessage());

                }


            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                // Log.e(TAG, "model: " + mThrowable.getMessage());
                SpinnerManager.hideSpinner(mContext);
            }
        });
    }

    //get sales data
    private void getSalesData(Context mContext) {
        SpinnerManager.showSpinner(mContext);
        AddLeadService.getSalesFromServer(mContext, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                try {
                    retrofit2.Response<List<Procurement>> mResponse = (retrofit2.Response<List<Procurement>>) obj;

                    List<Procurement> list = mResponse.body();
                    for (int i = 0; i < list.size(); i++) {
                        exeNameList.add(list.get(i).getText());
                        exeIdList.add(list.get(i).getValue());
                        exeNameIdList.add(list.get(i).getText() + "@" + list.get(i).getValue());
                    }

                } catch (Exception e) {

                }
                MethodExeInfoListPopup(exeIdList, exeNameList, exeNameIdList);

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
            }
        });
    }

    //add lead
    private void addLead(ReqAddLead mReqAddLead, Context mContext) {
        SpinnerManager.showSpinner(mContext);

        AddLeadService.addLeadFromServer(mReqAddLead, mContext, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                try {
                    retrofit2.Response<ResAddLead> mResponse = (retrofit2.Response<ResAddLead>) obj;
                    if (mResponse.body().getStatus().equalsIgnoreCase("SUCCESS")) {
                        Toast.makeText(activity, " " + "Lead added successfully", Toast.LENGTH_LONG).show();
                        setResult(10);
                        activity.finish();
                    } else {
                        Toast.makeText(activity, " " + mResponse.body().getMessage(), Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {

                }


            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
            }
        });
    }

    //color
    public void fetchColor(String color, final Context mContext) {
        SpinnerManager.showSpinner(mContext);
        if (colorListRet != null) {
            colorListRet.clear();
        }
        if (colorIdRet != null) {
            colorIdRet.clear();
        }

        YearService.getColorFromServer(color, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                try {
                    retrofit2.Response<List<ColourMaster>> mResponse = (retrofit2.Response<List<ColourMaster>>) obj;

                    List<ColourMaster> list = mResponse.body();

                    for (int i = 0; i < list.size(); i++) {

                        colorListRet.add(list.get(i).getColour());
                        colorIdRet.add(String.valueOf(list.get(i).getId()));
                        Log.e("mater id and color", list.get(i).getColour() + " " + list.get(i).getId());
                    }


                    MethodColourListPopup(colorIdRet, colorListRet);

                } catch (Exception e) {

                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
                // Log.e("master", mThrowable.getMessage());
            }
        });
    }


    private static void MethodModelListPopup(ArrayList<String> modalVariant, Activity activity) {

        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        ImageView backicon = dialog_data.findViewById(R.id.backicon);
        backicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_data.dismiss();
                // dialog_data.cancel();
            }
        });

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        searchtittle.setText("Select Model and Variant");

        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    //  dialog_data.cancel();
                }

            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        filterText.setHint("Search Model and Variant");
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, modalVariant);
        alertdialog_Listview.setAdapter(adapter);
        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                String modVar = a.getAdapter().getItem(position).toString();


                TextView textview = v.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);


                modeltv.setText(modVar);

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });


        dialog_data.show();

    }

}
