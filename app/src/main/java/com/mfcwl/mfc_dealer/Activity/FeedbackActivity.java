package com.mfcwl.mfc_dealer.Activity;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import androidx.fragment.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.Model.feedbackReq;
import com.mfcwl.mfc_dealer.Model.feedbackRes;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.retrofitconfig.FeedbackServices;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Utility.CommonMethods.GALLERY_RESCALE_VALUE;

public class FeedbackActivity extends FragmentActivity {

    public EditText feedback_tv;
    public TextView attach1, attach2, attach3, attach4, feedback_submit;
    public ImageView feedback_back, close1, close2, close3, close4;
    public static Uri image;
    private static int RESULT_LOAD = 1;
    public String imageSet = "";
    public int width_x = 700, height_y = 700;
    public String path = "";

    String image1="",image2="",image3="",image4="";

    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        Log.i(TAG, "onCreate: ");
        image1="";image2="";image3="";image4="";

        feedback_tv = (EditText) findViewById(R.id.feedback_tv);
        attach1 = (TextView) findViewById(R.id.attach1);
        attach2 = (TextView) findViewById(R.id.attach2);
        attach3 = (TextView) findViewById(R.id.attach3);
        attach4 = (TextView) findViewById(R.id.attach4);
        feedback_submit = (TextView) findViewById(R.id.feedback_submit);
        close1 = (ImageView) findViewById(R.id.close1);
        close2 = (ImageView) findViewById(R.id.close2);
        close3 = (ImageView) findViewById(R.id.close3);
        close4 = (ImageView) findViewById(R.id.close4);
        feedback_back = (ImageView) findViewById(R.id.feedback_back);

        attach1.setOnClickListener(v -> {
            imageSet = "attach1";
            openGallary();
        });
        attach2.setOnClickListener(v -> {
            imageSet = "attach2";
            openGallary();
        });
        attach3.setOnClickListener(v -> {
            imageSet = "attach3";
            openGallary();
        });
        attach4.setOnClickListener(v -> {
            imageSet = "attach4";
            openGallary();
        });

        close1.setOnClickListener(v -> {
            closeData(attach1,image1,close1);
        });
        close2.setOnClickListener(v -> {
            closeData(attach2,image2,close2);
        });
        close3.setOnClickListener(v -> {
            closeData(attach3,image3,close3);
        });
        close4.setOnClickListener(v -> {
            closeData(attach4,image4,close4);
        });

        feedback_submit.setOnClickListener(v -> {

            if (!feedback_tv.getText().toString().equalsIgnoreCase("")) {

                feedbackReq req = new feedbackReq();
                req.setFeedback(feedback_tv.getText().toString());
                req.setDealerCode(CommonMethods.getstringvaluefromkey(FeedbackActivity.this,"dealer_code"));

                if(!image1.equalsIgnoreCase("")){
                    req.setImage(image1);
                }
                if(!image2.equalsIgnoreCase("")){
                    req.setImage2(image2);
                }
                if(!image3.equalsIgnoreCase("")){
                    req.setImage3(image3);
                }
                if(!image4.equalsIgnoreCase("")){
                    req.setImage4(image4);
                }

                if (CommonMethods.isInternetWorking(FeedbackActivity.this)) {
                    feedbackReqservice(FeedbackActivity.this,req);
                } else {
                    CommonMethods.alertMessage(FeedbackActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
                }

            } else {
                Toast.makeText(FeedbackActivity.this, "Please enter feedback.", Toast.LENGTH_LONG).show();
            }

        });

        feedback_back.setOnClickListener(v -> {
            finish();
        });

    }

    private void openGallary() {

        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(gallery, RESULT_LOAD);
    }

    private void closeData(TextView attach, String image, ImageView close) {

        attach.setText("");
        image="";
        close.setVisibility(View.INVISIBLE);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
/*
        Log.e("requestCode", "requestCode" + requestCode);
        Log.e("resultCode", "resultCode" + resultCode);
        Log.e("data", "data" + data); */

        try {

            String imageName="";

            if (requestCode == RESULT_LOAD && resultCode == RESULT_OK && data != null) {
                image = data.getData();
                path = image.getPath();


                String link = getUriRealPathAboveKitkat(FeedbackActivity.this,image);

                int cut = link.lastIndexOf("/");
                if (cut != -1) {
                    imageName = link.substring(cut+1);
                }else{
                    imageName=link;
                }


            if (imageSet.equalsIgnoreCase("attach1")) {

                attach1.setText(imageName);
                image1=imageToString("attach1",image);
                close1.setVisibility(View.VISIBLE);

            } else if (imageSet.equalsIgnoreCase("attach2")) {
                attach2.setText(imageName);
                image2=imageToString("attach2",image);
                close2.setVisibility(View.VISIBLE);

            } else if (imageSet.equalsIgnoreCase("attach3")) {
                attach3.setText(imageName);
                image3=imageToString("attach3",image);
                close3.setVisibility(View.VISIBLE);

            } else if (imageSet.equalsIgnoreCase("attach4")) {

                attach4.setText(imageName);
                image4=imageToString("attach4",image);
                close4.setVisibility(View.VISIBLE);
            }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public String imageToString(String pictureOf, Uri imageUri) {

        Bitmap bm = null;

        bm = MethodOfGettingCarImage(imageUri);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 80, baos);
        byte[] b = null;
        b = baos.toByteArray();
        try {
            baos.flush();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return globalImageCompression(b.length, bm);
    }

    private String globalImageCompression(int imageSize, Bitmap bm) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try {
        if (imageSize <= (200 * 1024)) {                            // <= 200 KB
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] b = null;
            b = baos.toByteArray();
            String encodedImage = "";
            encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            baos.flush();
                baos.close();

            return encodedImage;
        } else if (imageSize <= (500 * 1024)) {                      // <= 500 KB
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] b = null;
            b = baos.toByteArray();
            String encodedImage = "";
            encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            baos.flush();
            baos.close();
            return encodedImage;
        } else if (imageSize <= (1 * 1024 * 1024)) {                 // <= 1 MB

            bm.compress(Bitmap.CompressFormat.JPEG, 90, baos);
            byte[] b = null;
            b = baos.toByteArray();
            String encodedImage = "";
            encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            baos.flush();
            baos.close();
            return encodedImage;
        } else if (imageSize <= (2 * 1024 * 1024)) {                 // <= 2 MB

            bm.compress(Bitmap.CompressFormat.JPEG, 80, baos);
            byte[] b = null;
            b = baos.toByteArray();
            String encodedImage = "";
            encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            baos.flush();
            baos.close();
            return encodedImage;
        } else if (imageSize > (2 * 1024 * 1024)) {                  //  >  2 MB

            Double w = bm.getWidth() * (GALLERY_RESCALE_VALUE);
            width_x = w.intValue();
            Double h = bm.getHeight() * (GALLERY_RESCALE_VALUE);
            height_y = h.intValue();

            Bitmap bitmapAfterRescaleImage = null;
            bitmapAfterRescaleImage = Bitmap.createScaledBitmap(bm, width_x, height_y, false);
            bitmapAfterRescaleImage.compress(Bitmap.CompressFormat.JPEG, 80, baos);

            byte[] b = null;
            b = baos.toByteArray();
            String encodedImage = "";
            encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

            baos.flush();
            baos.close();
            return encodedImage;
        }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }


    private Bitmap MethodOfGettingCarImage(Uri imageUri) {

        Bitmap myBitmap = null;

        try {
                try {

                    myBitmap =MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);

                } catch (OutOfMemoryError error) {
                    //  Log.e("OutOfMemoryError-1 ", "rotate ");

                    try {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 8;
                        myBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                    } catch (OutOfMemoryError errors) {
                        //   Log.e("OutOfMemoryError-2 ", "rotate ");
                        try {
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inJustDecodeBounds = false;
                            options.inPreferredConfig = Bitmap.Config.RGB_565;
                            options.inDither = false;                     //Disable Dithering mode
                            options.inPurgeable = true;                   //Tell to gc that whether it needs free memory, the Bitmap can be cleared
                            options.inInputShareable = true;
                            options.inSampleSize = 15;
                            myBitmap = BitmapFactory.decodeFile(imageUri.getPath(), options);


                        } catch (OutOfMemoryError errorss) {

                        }
                    }
                }


            int x=0;

            return myBitmap;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return myBitmap;
    }

    private void feedbackReqservice(final Context mContext, final feedbackReq req) {
        SpinnerManager.showSpinner(mContext);
        FeedbackServices.feedbackService(mContext, req, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                Response<feedbackRes> mRes = (Response<feedbackRes>) obj;
                feedbackRes mData = mRes.body();
                String msg = mData.getMessage();

                if (mData.getStatus().equalsIgnoreCase("success")) {
                    CommonMethods.alertMessage2(FeedbackActivity.this,msg);
                } else {
                    CommonMethods.alertMessage((Activity) mContext, msg);
                }
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                try {
                    SpinnerManager.hideSpinner(mContext);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    /* Get uri related content real local file path. */
    private String getUriRealPath(Context ctx, Uri uri)
    {
        String ret = "";

        if( isAboveKitKat() )
        {
            // Android OS above sdk version 19.
            ret = getUriRealPathAboveKitkat(ctx, uri);
        }else
        {
            // Android OS below sdk version 19
            ret = getImageRealPath(getContentResolver(), uri, null);
        }

        return ret;
    }

    private String getUriRealPathAboveKitkat(Context ctx, Uri uri)
    {
        String ret = "";

        if(ctx != null && uri != null) {

            if(isContentUri(uri))
            {
                if(isGooglePhotoDoc(uri.getAuthority()))
                {
                    ret = uri.getLastPathSegment();
                }else {
                    ret = getImageRealPath(getContentResolver(), uri, null);
                }
            }else if(isFileUri(uri)) {
                ret = uri.getPath();
            }else if(isDocumentUri(ctx, uri)){

                // Get uri related document id.
                String documentId = DocumentsContract.getDocumentId(uri);

                // Get uri authority.
                String uriAuthority = uri.getAuthority();

                if(isMediaDoc(uriAuthority))
                {
                    String idArr[] = documentId.split(":");
                    if(idArr.length == 2)
                    {
                        // First item is document type.
                        String docType = idArr[0];

                        // Second item is document real id.
                        String realDocId = idArr[1];

                        // Get content uri by document type.
                        Uri mediaContentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                        if("image".equals(docType))
                        {
                            mediaContentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                        }else if("video".equals(docType))
                        {
                            mediaContentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                        }else if("audio".equals(docType))
                        {
                            mediaContentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                        }

                        // Get where clause with real document id.
                        String whereClause = MediaStore.Images.Media._ID + " = " + realDocId;

                        ret = getImageRealPath(getContentResolver(), mediaContentUri, whereClause);
                    }

                }else if(isDownloadDoc(uriAuthority))
                {
                    // Build download uri.
                    Uri downloadUri = Uri.parse("content://downloads/public_downloads");

                    // Append download document id at uri end.
                    Uri downloadUriAppendId = ContentUris.withAppendedId(downloadUri, Long.valueOf(documentId));

                    ret = getImageRealPath(getContentResolver(), downloadUriAppendId, null);

                }else if(isExternalStoreDoc(uriAuthority))
                {
                    String idArr[] = documentId.split(":");
                    if(idArr.length == 2)
                    {
                        String type = idArr[0];
                        String realDocId = idArr[1];

                        if("primary".equalsIgnoreCase(type))
                        {
                            ret = Environment.getExternalStorageDirectory() + "/" + realDocId;
                        }
                    }
                }
            }
        }

        return ret;
    }

    /* Check whether current android os version is bigger than kitkat or not. */
    private boolean isAboveKitKat()
    {
        boolean ret = false;
        ret = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        return ret;
    }

    /* Check whether this uri represent a document or not. */
    private boolean isDocumentUri(Context ctx, Uri uri)
    {
        boolean ret = false;
        if(ctx != null && uri != null) {
            ret = DocumentsContract.isDocumentUri(ctx, uri);
        }
        return ret;
    }

    /* Check whether this uri is a content uri or not.
     *  content uri like content://media/external/images/media/1302716
     *  */
    private boolean isContentUri(Uri uri)
    {
        boolean ret = false;
        if(uri != null) {
            String uriSchema = uri.getScheme();
            if("content".equalsIgnoreCase(uriSchema))
            {
                ret = true;
            }
        }
        return ret;
    }

    /* Check whether this uri is a file uri or not.
     *  file uri like file:///storage/41B7-12F1/DCIM/Camera/IMG_20180211_095139.jpg
     * */
    private boolean isFileUri(Uri uri)
    {
        boolean ret = false;
        if(uri != null) {
            String uriSchema = uri.getScheme();
            if("file".equalsIgnoreCase(uriSchema))
            {
                ret = true;
            }
        }
        return ret;
    }


    /* Check whether this document is provided by ExternalStorageProvider. */
    private boolean isExternalStoreDoc(String uriAuthority)
    {
        boolean ret = false;

        if("com.android.externalstorage.documents".equals(uriAuthority))
        {
            ret = true;
        }

        return ret;
    }

    /* Check whether this document is provided by DownloadsProvider. */
    private boolean isDownloadDoc(String uriAuthority)
    {
        boolean ret = false;

        if("com.android.providers.downloads.documents".equals(uriAuthority))
        {
            ret = true;
        }

        return ret;
    }

    /* Check whether this document is provided by MediaProvider. */
    private boolean isMediaDoc(String uriAuthority)
    {
        boolean ret = false;

        if("com.android.providers.media.documents".equals(uriAuthority))
        {
            ret = true;
        }

        return ret;
    }

    /* Check whether this document is provided by google photos. */
    private boolean isGooglePhotoDoc(String uriAuthority)
    {
        boolean ret = false;

        if("com.google.android.apps.photos.content".equals(uriAuthority))
        {
            ret = true;
        }

        return ret;
    }

    /* Return uri represented document file real local path.*/
    private String getImageRealPath(ContentResolver contentResolver, Uri uri, String whereClause)
    {
        String ret = "";

        // Query the uri with condition.
        Cursor cursor = contentResolver.query(uri, null, whereClause, null, null);

        if(cursor!=null)
        {
            boolean moveToFirst = cursor.moveToFirst();
            if(moveToFirst)
            {

                // Get columns name by uri type.
                String columnName = MediaStore.Images.Media.DATA;

                if( uri==MediaStore.Images.Media.EXTERNAL_CONTENT_URI )
                {
                    columnName = MediaStore.Images.Media.DATA;
                }else if( uri==MediaStore.Audio.Media.EXTERNAL_CONTENT_URI )
                {
                    columnName = MediaStore.Audio.Media.DATA;
                }else if( uri==MediaStore.Video.Media.EXTERNAL_CONTENT_URI )
                {
                    columnName = MediaStore.Video.Media.DATA;
                }

                // Get column index.
                int imageColumnIndex = cursor.getColumnIndex(columnName);

                // Get column value which is the uri related file local path.
                ret = cursor.getString(imageColumnIndex);
            }
        }

        return ret;
    }


}
