package com.mfcwl.mfc_dealer.Activity.RDR;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.Activity.RDR.Model.CountResponse;
import com.mfcwl.mfc_dealer.Activity.RDR.Model.CountRquest;
import com.mfcwl.mfc_dealer.Activity.RDR.Services.RDRDashboardService;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.AppConstants;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.MonthUtility;

import retrofit2.Response;
import sidekicklpr.PreferenceManager;

public class RDRWarningDialog extends Dialog implements
        android.view.View.OnClickListener {

    public Activity c;
    public Dialog d;
    public Button cancel, ok;
    private PreferenceManager preferenceManager;
    private TextView message;


    public RDRWarningDialog(Activity a) {
        super(a);
        this.c = a;
        preferenceManager = new PreferenceManager(c);
        CountRquest mReq= new CountRquest();
        mReq.setUserType(CommonMethods.getstringvaluefromkey(c ,"user_type"));
        if(CommonMethods.getstringvaluefromkey(c, "user_type").equalsIgnoreCase("dealer")){
            mReq.setUserId(CommonMethods.getstringvaluefromkey(c,"dealer_code"));
        }else {
            mReq.setUserId(CommonMethods.getstringvaluefromkey(c,"user_id"));
        }

       getPendingCount(mReq);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.rdrwarning_dialog);
        cancel = findViewById(R.id.btn_cancel);
        ok =  findViewById(R.id.btn_ok);
        message = findViewById(R.id.messagetxt);
        cancel.setOnClickListener(this);
        ok.setOnClickListener(this);
      //  message.setText("There are some tasks which are pending. Close them now.");

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                dismiss();
                preferenceManager.writeString(AppConstants.DATE_KEY, MonthUtility.getCurrentDate());
                break;
            case R.id.btn_ok:
                preferenceManager.writeString(AppConstants.DATE_KEY, MonthUtility.getCurrentDate());
                dismiss();
                c.startActivity(new Intent(c,ViewDetailedListing.class));
                break;
            default:
                break;
        }

    }



    private void getPendingCount(CountRquest mReq){
        RDRDashboardService.fetchCount(mReq, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                Response<CountResponse> mRes = (Response<CountResponse>) obj;
                CountResponse Data = mRes.body();
                if(Data != null){
                    message.setText("There are "+ Data.getCount()+" tasks which are pending. Close them now.");
                }else {
                    message.setText("There are some tasks which are pending. Close them now.");
                }
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                message.setText("There are some tasks which are pending. Close them now.");
            }
        });
    }

}