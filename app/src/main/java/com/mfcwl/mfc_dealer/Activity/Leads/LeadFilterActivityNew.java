package com.mfcwl.mfc_dealer.Activity.Leads;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragmentNew;
import com.mfcwl.mfc_dealer.Fragment.Leads.LeadsFragment;
import com.mfcwl.mfc_dealer.Fragment.Leads.PrivateLeadsFragment;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import org.json.JSONArray;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragmentNew.chcold;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragmentNew.chfollowlast7days;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragmentNew.chfollowtmrw;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragmentNew.chfollowtoday;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragmentNew.chfollowyester;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragmentNew.chhot;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragmentNew.chlost;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragmentNew.chopen;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragmentNew.chpost15days;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragmentNew.chpost7days;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragmentNew.chposttoday;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragmentNew.chpostyester;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragmentNew.chsold;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragmentNew.chwarm;
import static com.mfcwl.mfc_dealer.Fragment.Leads.NormalLeadsFragment.flagsetonetimecall;
import static com.mfcwl.mfc_dealer.Fragment.Leads.PrivateLeadsFragment.flagRes;


public class LeadFilterActivityNew extends AppCompatActivity {

    public Fragment fragment;
    // Button btnback1;
    FrameLayout list2, list1;
    CardView card1, card3, card5;
    Activity activity;
    TextView clearFilter;
    private SharedPreferences mSharedPreferences = null;
    private Gson gson;

    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate: ");
        setContentView(R.layout.layout_main_filter);

        InitUI();


    }

    private void InitUI() {

        mSharedPreferences = getSharedPreferences("MFC", Context.MODE_PRIVATE);
        flagRes = true;
        flagsetonetimecall = true;
        list2 = (FrameLayout) findViewById(R.id.list2);
        list1 = (FrameLayout) findViewById(R.id.list1);
        clearFilter = (TextView) findViewById(R.id.toolbar_menu);
        activity = this;
        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        LeadFilterFragmentNew filterFragment = new LeadFilterFragmentNew();
        gson = new Gson();
        this.fragmentLoad(filterFragment);

        clearFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (PrivateLeadsFragment.myPriList != null) {
                    PrivateLeadsFragment.myPriList.clear();
                }

                //ASM
                CommonMethods.setvalueAgainstKey(activity, "leadStatus", "");
                finish();
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.lead_filter_clear, GlobalText.android);

                SharedPreferences.Editor mEditor = mSharedPreferences.edit();
                mEditor.putString(LeadConstants.LEAD_STATUS_INFO, "");
                LeadFilterFragmentNew.leadstatusJsonArray = new JSONArray();
                mEditor.clear();
                mEditor.commit();

                clearAllCheckBoxData();
                ClearAllFilters();

            }
        });

        list2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.lead_filter_apply, GlobalText.android);

                //lead status section done
                if (PrivateLeadsFragment.myPriList != null) {
                    PrivateLeadsFragment.myPriList.clear();
                }
                Gson mgson = new Gson();
                String mdata = LeadFilterFragmentNew.leadstatusJsonArray.toString();
                if (mdata != null && mdata != "") {
                    SharedPreferences.Editor mEdit = mSharedPreferences.edit();
                    mEdit.putString(LeadConstants.LEAD_STATUS_INFO, mdata);
                    mEdit.commit();
                }

                SharedPreferences.Editor mEditor0 = mSharedPreferences.edit();
                mEditor0.putString(LeadConstants.LEAD_POST_TODAY, "");
                //clear others
                mEditor0.putString(LeadConstants.LEAD_POST_CUSTDATE, "");
                mEditor0.putString(LeadConstants.LEAD_POST_YESTER, "");
                mEditor0.putString(LeadConstants.LEAD_POST_7DAYS, "");
                mEditor0.putString(LeadConstants.LEAD_POST_15DAYS, "");

                mEditor0.putString(LeadConstants.LEAD_FOLUP_TMRW, "");
                mEditor0.putString(LeadConstants.LEAD_FOLUP_TODAY, "");
                mEditor0.putString(LeadConstants.LEAD_FOLUP_YESTER, "");
                mEditor0.putString(LeadConstants.LEAD_FOLUP_7DAYS, "");
                mEditor0.putString(LeadConstants.LEAD_FOLUP_CUSTDATE, "");
                mEditor0.commit();
                
                if (LeadFilterFragmentNew.chposttoday.isChecked()) {
                    SharedPreferences.Editor mEditor = mSharedPreferences.edit();
                    mEditor.putString(LeadConstants.LEAD_POST_TODAY, getTodayDate());
                    //clear others
                    mEditor.putString(LeadConstants.LEAD_POST_CUSTDATE, "");
                    mEditor.putString(LeadConstants.LEAD_POST_YESTER, "");
                    mEditor.putString(LeadConstants.LEAD_POST_7DAYS, "");
                    mEditor.putString(LeadConstants.LEAD_POST_15DAYS, "");
                    mEditor.commit();
                }

                if (LeadFilterFragmentNew.chpostyester.isChecked()) {
                    SharedPreferences.Editor mEditor = mSharedPreferences.edit();
                    mEditor.putString(LeadConstants.LEAD_POST_YESTER, getYesterdayDate());

                    mEditor.putString(LeadConstants.LEAD_POST_CUSTDATE, "");
                    mEditor.putString(LeadConstants.LEAD_POST_TODAY, "");
                    mEditor.putString(LeadConstants.LEAD_POST_7DAYS, "");
                    mEditor.putString(LeadConstants.LEAD_POST_15DAYS, "");
                    mEditor.commit();
                }

                if (LeadFilterFragmentNew.chpost7days.isChecked()) {
                    SharedPreferences.Editor mEditor = mSharedPreferences.edit();
                    mEditor.putString(LeadConstants.LEAD_POST_7DAYS, get7days());

                    mEditor.putString(LeadConstants.LEAD_POST_CUSTDATE, "");
                    mEditor.putString(LeadConstants.LEAD_POST_TODAY, "");
                    mEditor.putString(LeadConstants.LEAD_POST_YESTER, "");
                    mEditor.putString(LeadConstants.LEAD_POST_15DAYS, "");
                    mEditor.commit();
                }

                if (LeadFilterFragmentNew.chpost15days.isChecked()) {
                    SharedPreferences.Editor mEditor = mSharedPreferences.edit();
                    mEditor.putString(LeadConstants.LEAD_POST_15DAYS, get15days());

                    mEditor.putString(LeadConstants.LEAD_POST_CUSTDATE, "");
                    mEditor.putString(LeadConstants.LEAD_POST_TODAY, "");
                    mEditor.putString(LeadConstants.LEAD_POST_7DAYS, "");
                    mEditor.putString(LeadConstants.LEAD_POST_YESTER, "");
                    mEditor.commit();
                }

                if (!LeadFilterFragmentNew.PDfromdate.getText().equals("") && !LeadFilterFragmentNew.PDtodate.getText().equals("")) {
                    SharedPreferences.Editor mEditor = mSharedPreferences.edit();
                    mEditor.putString(LeadConstants.LEAD_POST_CUSTDATE, LeadFilterFragmentNew.postCustomFromDate + "@" + LeadFilterFragmentNew.postCustomToDate);

                    mEditor.putString(LeadConstants.LEAD_POST_TODAY, "");
                    mEditor.putString(LeadConstants.LEAD_POST_15DAYS, "");
                    mEditor.putString(LeadConstants.LEAD_POST_7DAYS, "");
                    mEditor.putString(LeadConstants.LEAD_POST_YESTER, "");
                    mEditor.commit();
                }

                //Folowup Section

                if (LeadFilterFragmentNew.chfollowtmrw.isChecked()) {
                    SharedPreferences.Editor mEditor = mSharedPreferences.edit();
                    mEditor.putString(LeadConstants.LEAD_FOLUP_TMRW, getFollowTomorrowDate());
                    mEditor.putString(LeadConstants.LEAD_FOLUP_TODAY, "");
                    mEditor.putString(LeadConstants.LEAD_FOLUP_YESTER, "");
                    mEditor.putString(LeadConstants.LEAD_FOLUP_7DAYS, "");
                    mEditor.putString(LeadConstants.LEAD_FOLUP_CUSTDATE, "");
                    mEditor.commit();
                }

                if (LeadFilterFragmentNew.chfollowtoday.isChecked()) {
                    SharedPreferences.Editor mEditor = mSharedPreferences.edit();
                    mEditor.putString(LeadConstants.LEAD_FOLUP_TODAY, getFollowtodayDate());
                    mEditor.putString(LeadConstants.LEAD_FOLUP_TMRW, "");
                    mEditor.putString(LeadConstants.LEAD_FOLUP_YESTER, "");
                    mEditor.putString(LeadConstants.LEAD_FOLUP_7DAYS, "");
                    mEditor.putString(LeadConstants.LEAD_FOLUP_CUSTDATE, "");
                    mEditor.commit();
                }

                if (LeadFilterFragmentNew.chfollowyester.isChecked()) {
                    SharedPreferences.Editor mEditor = mSharedPreferences.edit();
                    mEditor.putString(LeadConstants.LEAD_FOLUP_YESTER, getFollowyesterDate());
                    mEditor.putString(LeadConstants.LEAD_FOLUP_TMRW, "");
                    mEditor.putString(LeadConstants.LEAD_FOLUP_TODAY, "");
                    mEditor.putString(LeadConstants.LEAD_FOLUP_7DAYS, "");
                    mEditor.putString(LeadConstants.LEAD_FOLUP_CUSTDATE, "");
                    mEditor.commit();
                }

                if (LeadFilterFragmentNew.chfollowlast7days.isChecked()) {
                    SharedPreferences.Editor mEditor = mSharedPreferences.edit();
                    mEditor.putString(LeadConstants.LEAD_FOLUP_7DAYS, getFollowlast7Date());
                    mEditor.putString(LeadConstants.LEAD_FOLUP_TMRW, "");
                    mEditor.putString(LeadConstants.LEAD_FOLUP_TODAY, "");
                    mEditor.putString(LeadConstants.LEAD_FOLUP_YESTER, "");
                    mEditor.putString(LeadConstants.LEAD_FOLUP_CUSTDATE, "");
                    mEditor.commit();
                }

                if (!LeadFilterFragmentNew.FWfromdate.getText().equals("") && !LeadFilterFragmentNew.FWtodate.getText().equals("")) {
                    SharedPreferences.Editor mEditor = mSharedPreferences.edit();
                    mEditor.putString(LeadConstants.LEAD_FOLUP_CUSTDATE, LeadFilterFragmentNew.followCustomFromDate + "@" + LeadFilterFragmentNew.followCustomToDate);
                    mEditor.putString(LeadConstants.LEAD_FOLUP_TMRW, "");
                    mEditor.putString(LeadConstants.LEAD_FOLUP_TODAY, "");
                    mEditor.putString(LeadConstants.LEAD_FOLUP_YESTER, "");
                    mEditor.putString(LeadConstants.LEAD_FOLUP_7DAYS, "");
                    mEditor.commit();
                }
                //End Followup


                finish();
            }
        });

        list1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LeadsFragment.normalLead = true;
                PrivateLeadsFragment.mPrivate = true;


                finish();

            }
        });
    }


    public void fragmentLoad(Fragment fragment) {

        FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fillLayout_main_filter, fragment);
        fragmentTransaction.commit();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                LeadsFragment.normalLead = true;
                PrivateLeadsFragment.mPrivate = true;

                String mJSon = mSharedPreferences.getString("leadfilter", "");
                if (mJSon != null && mJSon != "") {
                    //LeadFilterFragmentNew.mInstance = gson.fromJson(mJSon, FilterInstance.class);
                }
                /*if (LeadFilterFragmentNew.mInstance.getLeadstatusJsonArray() != null) {
                    LeadFilterFragmentNew.leadstatusJsonArray = LeadFilterFragmentNew.mInstance.getLeadstatusJsonArray();
                }*/
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        // put your code here...
        Application.getInstance().trackScreenView(activity,GlobalText.LeadfilterActivity);
    }

    @Override
    public void onBackPressed() {
        LeadsFragment.normalLead = true;
    }


    public static String[] toStringArray(JSONArray array) {
        if (array == null)
            return null;
        String[] leadstsarray = new String[array.length()];
        for (int i = 0; i < leadstsarray.length; i++) {
            leadstsarray[i] = array.optString(i);
        }
        return leadstsarray;
    }

    public String getTodayDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, 1);
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        String postTodYesLastFromDate = s.format(new Date(cal.getTimeInMillis()));
        String postTodYesLastToDate = s.format(new Date(cal1.getTimeInMillis()));
        String mDate = postTodYesLastFromDate + "@" + postTodYesLastToDate;
        return mDate;
    }

    public String getYesterdayDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, 0);

        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        String postTodYesLastFromDate = s.format(new Date(cal.getTimeInMillis()));
        String postTodYesLastToDate = s.format(new Date(cal1.getTimeInMillis()));
        String mfinaldate = postTodYesLastFromDate + "@" + postTodYesLastToDate;
        return mfinaldate;
    }


    public String get7days() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -7);

        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, 0);

        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

        String postTodYesLastFromDate = s.format(new Date(cal.getTimeInMillis()));
        String postTodYesLastToDate = s.format(new Date(cal1.getTimeInMillis()));
        String mfinaldate = postTodYesLastFromDate + "@" + postTodYesLastToDate;

        return mfinaldate;
    }

    public String get15days() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -15);

        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, 0);

        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

        String postTodYesLastFromDate = s.format(new Date(cal.getTimeInMillis()));
        String postTodYesLastToDate = s.format(new Date(cal1.getTimeInMillis()));
        String mfinaldate = postTodYesLastFromDate + "@" + postTodYesLastToDate;

        return mfinaldate;
    }


    private String getFollowTomorrowDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);

        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, 0);
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        String startDate = s.format(new Date(cal.getTimeInMillis()));
        String endDate = s.format(new Date(cal1.getTimeInMillis()));

        String mfinaldate = startDate + "@" + endDate;
        return mfinaldate;

    }

    private String getFollowtodayDate() {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);

        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, 1);

        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

        String startDate = s.format(new Date(cal.getTimeInMillis()));
        String endDate = s.format(new Date(cal1.getTimeInMillis()));

        String mfinaldate = startDate + "@" + endDate;
        return mfinaldate;

    }

    private String getFollowlast7Date() {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -7);

        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, 0);

        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

        String startDate = s.format(new Date(cal.getTimeInMillis()));
        String endDate = s.format(new Date(cal1.getTimeInMillis()));

        String mfinaldate = startDate + "@" + endDate;
        return mfinaldate;

    }

    private String getFollowyesterDate() {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);

        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, 0);

        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

        String startDate = s.format(new Date(cal.getTimeInMillis()));
        String endDate = s.format(new Date(cal1.getTimeInMillis()));

        String mfinaldate = startDate + "@" + endDate;
        return mfinaldate;

    }


    //Clear All CheckBox

    private void clearAllCheckBoxData() {
        chposttoday.setChecked(false);
        chpostyester.setChecked(false);
        chpost7days.setChecked(false);
        chpost15days.setChecked(false);

        chfollowtmrw.setChecked(false);
        chfollowtoday.setChecked(false);
        chfollowyester.setChecked(false);
        chfollowlast7days.setChecked(false);

        chopen.setChecked(false);
        chhot.setChecked(false);
        chwarm.setChecked(false);
        chcold.setChecked(false);
        chlost.setChecked(false);
        chsold.setChecked(false);

        LeadFilterFragmentNew.PDfromdate.setText("");
        LeadFilterFragmentNew.PDtodate.setText("");

        LeadFilterFragmentNew.FWfromdate.setText("");
        LeadFilterFragmentNew.FWtodate.setText("");
    }


    private void ClearAllFilters() {
        SharedPreferences.Editor mEditor0 = mSharedPreferences.edit();
        mEditor0.putString(LeadConstants.LEAD_POST_TODAY, "");
        //clear others
        mEditor0.putString(LeadConstants.LEAD_POST_CUSTDATE, "");
        mEditor0.putString(LeadConstants.LEAD_POST_YESTER, "");
        mEditor0.putString(LeadConstants.LEAD_POST_7DAYS, "");
        mEditor0.putString(LeadConstants.LEAD_POST_15DAYS, "");

        mEditor0.putString(LeadConstants.LEAD_FOLUP_TMRW, "");
        //mEditor0.putString(LeadConstants.LEAD_FOLUP_TODAY, "");//because set Follow-Up date other Filter
        mEditor0.putString(LeadConstants.LEAD_FOLUP_YESTER, "");
        mEditor0.putString(LeadConstants.LEAD_FOLUP_7DAYS, "");
        mEditor0.putString(LeadConstants.LEAD_FOLUP_CUSTDATE, "");
        mEditor0.putString(LeadConstants.LEAD_STATUS_INFO, "");
        mEditor0.commit();


    }

}