package com.mfcwl.mfc_dealer.Activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.appyvet.materialrangebar.RangeBar;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.InstanceCreate.ToolsFilterInstance;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.mfcwl.mfc_dealer.Fragment.ToolFragment.tools_filter_icon;

public class ToolsFilterActivity extends AppCompatActivity {

    @BindView(R.id.filter_cetified_car)
    public Button filterCetifiedCar;
    @BindView(R.id.filter_all_cars)
    public Button filterAllCar;
    @BindView(R.id.filter_price_card)
    public CardView filterPrice;
    @BindView(R.id.filter_delear_price)
    public CardView dealerfilterPrice;
    @BindView(R.id.year_card)
    public CardView filterYear;
    @BindView(R.id.certified_level)
    public TextView certifiedLevel;
    @BindView(R.id.linear_certified_car)
    public CardView certifiedValues;
    @BindView(R.id.kms_level)
    public TextView kmsLelel;
    @BindView(R.id.startprice)
    public TextView startprice;
    @BindView(R.id.start_dealerprice)
    public TextView start_dealerprice;
    @BindView(R.id.endprice)
    public TextView endprice;
    @BindView(R.id.end_dealer_price)
    public TextView end_dealer_price;
    @BindView(R.id.kmsvalues)
    public TextView kmsvalues;
    @BindView(R.id.year_change)
    public TextView year_change;
    @BindView(R.id.kms_values)
    public CardView kmsValues;
    @BindView(R.id.year_values)
    public CardView year_values;
    @BindView(R.id.price_values)
    public CardView price_values;
    @BindView(R.id.dealer_price_values)
    public CardView dealer_price_values;
    @BindView(R.id.filter_seekbar)
    public SeekBar filter_seekbar;
    @BindView(R.id.year_seekbar)
    public SeekBar year_seekbar;

    @BindView(R.id.filterby_apply)
    public LinearLayout filterby_apply;
    @BindView(R.id.filterbycancelbtn)
    public LinearLayout filterbycancelbtn;


    @BindView(R.id.price_arrow)
    public ImageView price_arrow;
    @BindView(R.id.dealer_price_arrow)
    public ImageView dealer_price_arrow;
    @BindView(R.id.yearicon)
    public ImageView yearicon;


    public static boolean stock_photocountsFlag;
    public static boolean book_photocountsFlag;

    boolean certified, kms, price = true, dealer_price = true, yearFlag = true;
    public static Activity activity;

    public static String yearValues = "2002";
    public static String kilometerValues = "1000000";
    public static String startPriceValues = "0";
    public static String endPriceValues = "10000000";
    public static String dealer_startPriceValues = "0";
    public static String dealer_endPriceValues = "10000000";

    RangeBar twoseekar, threeseekar;
    boolean certifiedFlag = false;
    //add seekbar value
    int start = 0, end = 100, dealer_start = 0, dealer_end = 100;
    int progval = 0;
    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tools_filter);
        Log.i(TAG, "onCreate: ");
        ButterKnife.bind(this);
        activity = this;
        CommonMethods.MemoryClears();
        Log.i(TAG, "onCreate: ");
        //reena
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter("com.mfcwl.mfc_dealer.FMC"));

        if (CommonMethods.getstringvaluefromkey(activity, "bookkilometerdata").equalsIgnoreCase("") || CommonMethods.getstringvaluefromkey(activity, "bookkilometerdata") == null) {

            CommonMethods.setvalueAgainstKey(activity, "bookkilometerdata", kilometerValues);

        }

        if (CommonMethods.getstringvaluefromkey(activity, "bookstartpricedata").equalsIgnoreCase("") || CommonMethods.getstringvaluefromkey(activity, "bookstartpricedata") == null) {

            CommonMethods.setvalueAgainstKey(activity, "bookstartpricedata", startPriceValues);


        }
        if (CommonMethods.getstringvaluefromkey(activity, "bookendpricedata").equalsIgnoreCase("") || CommonMethods.getstringvaluefromkey(activity, "bookendpricedata") == null) {

            CommonMethods.setvalueAgainstKey(activity, "bookendpricedata", endPriceValues);

        }

        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        twoseekar = (RangeBar) findViewById(R.id.twoseekar);
        threeseekar = (RangeBar) findViewById(R.id.threeseekar);

        //added value progres

        certifiedValues.setVisibility(View.GONE);
        kmsValues.setVisibility(View.GONE);
        price_values.setVisibility(View.GONE);
        dealer_price_values.setVisibility(View.GONE);
        year_values.setVisibility(View.GONE);

        price_arrow.setBackgroundResource(R.drawable.down_arrow);
        dealer_price_arrow.setBackgroundResource(R.drawable.down_arrow);
        yearicon.setBackgroundResource(R.drawable.down_arrow);
        certifiedLevel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
        kmsLelel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);

        Log.e("testtwo", twoseekar.getLeftPinValue() + " " + twoseekar.getRightPinValue());
        Log.e("testthree", threeseekar.getLeftPinValue() + " " + threeseekar.getRightPinValue());
        Log.e("testyear", year_seekbar.getProgress() + "");
        Log.e("testfilter", filter_seekbar.getProgress() + "");

        /*startprice.setText(getResources().getString(R.string.rs)+" "+"0");
        endprice.setText(getResources().getString(R.string.rs)+" "+"1 Cr");*/

        priceRefill();
        // priceRefill();

        if (CommonMethods.getstringvaluefromkey(this, "tcertifiedstatus").equalsIgnoreCase("certifiedcar")) {
            filterCetifiedCar.setBackgroundResource(R.drawable.my_button);
            filterCetifiedCar.setTextColor(getResources().getColor(R.color.Black));
            certifiedFlag = true;
            filterAllCar.setBackgroundResource(R.drawable.my_buttom_gray_light);
            filterAllCar.setTextColor(getResources().getColor(R.color.lgray));
            certifiedValues.setVisibility(View.VISIBLE);
        } else {
            certifiedValues.setVisibility(View.GONE);
            certifiedFlag = false;
            filterAllCar.setBackgroundResource(R.drawable.my_button);
            filterAllCar.setTextColor(getResources().getColor(R.color.Black));
            CommonMethods.setvalueAgainstKey(activity, "tcertifiedstatus", "allcar");
            filterCetifiedCar.setBackgroundResource(R.drawable.my_buttom_gray_light);
            filterCetifiedCar.setTextColor(getResources().getColor(R.color.lgray));
        }

       /* if (CommonMethods.getstringvaluefromkey(activity, "tkilometerdata").equals(kilometerValues)) {


        } else {

        }*/


        filterby_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.tool_filter, GlobalText.android);

                Log.e("tcertifiedval", "tcertifiedval=" + CommonMethods.getstringvaluefromkey(activity, "tcertifiedval").toString());

                if (CommonMethods.getstringvaluefromkey(activity, "tcertifiedval").equalsIgnoreCase("true")
                        || CommonMethods.getstringvaluefromkey(activity, "tselling").equalsIgnoreCase("true")
                        || CommonMethods.getstringvaluefromkey(activity, "tdealer").equalsIgnoreCase("true")
                        || CommonMethods.getstringvaluefromkey(activity, "kmsval").equalsIgnoreCase("true")
                        || CommonMethods.getstringvaluefromkey(activity, "tyear").equalsIgnoreCase("true")
                        || CommonMethods.getstringvaluefromkey(activity, "tkms").equalsIgnoreCase("true")
                        || CommonMethods.getstringvaluefromkey(activity, "tcertifiedstatus").equalsIgnoreCase("certifiedcar")
                        ) {

                    if (CommonMethods.getstringvaluefromkey(activity, "tcertifiedval").equalsIgnoreCase("true")
                            ) {
                        tools_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);
                    }

                    if (yearValues.equalsIgnoreCase("2002")
                            && kilometerValues.equalsIgnoreCase("1000000")
                            && startPriceValues.equalsIgnoreCase("0")
                            && endPriceValues.equalsIgnoreCase("10000000")
                            && dealer_startPriceValues.equalsIgnoreCase("0")
                            && dealer_endPriceValues.equalsIgnoreCase("10000000")

                            ) {

                        if (CommonMethods.getstringvaluefromkey(activity, "tcertifiedstatus").equalsIgnoreCase("certifiedcar")
                                ) {
                            tools_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);
                        }

                    } else {
                        tools_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);
                        Log.e("filter_icon_yellow", "filter_icon_yellow=1");
                    }

                } else {
                    tools_filter_icon.setBackgroundResource(R.drawable.filter_48x18);
                    Log.e("filter_icon_yellow", "filter_icon_yellow=2");

                }
                Log.e("tcertifiedval", "tcertifiedval=" + CommonMethods.getstringvaluefromkey(activity, "tcertifiedval").toString());

                if (CommonMethods.getstringvaluefromkey(activity, "tcertifiedval").equalsIgnoreCase("true")
                        ) {
                    tools_filter_icon.setBackgroundResource(R.drawable.filter_icon_yellow);
                }


                if (CommonMethods.getstringvaluefromkey(activity, "tclear").equalsIgnoreCase("true")) {
                    CommonMethods.setvalueAgainstKey(activity, "tcertifiedval", "false");
                    CommonMethods.setvalueAgainstKey(activity, "tselling", "false");
                    CommonMethods.setvalueAgainstKey(activity, "tdealer", "false");
                    CommonMethods.setvalueAgainstKey(activity, "tyear", "false");
                    CommonMethods.setvalueAgainstKey(activity, "tkms", "false");
                }

                if (certifiedFlag) {
                    CommonMethods.setvalueAgainstKey(activity, "tcertifiedstatus", "certifiedcar");
                } else {
                    CommonMethods.setvalueAgainstKey(activity, "tcertifiedstatus", "allcar");
                }
                CommonMethods.setvalueAgainstKey(activity, "tstartpricedata", startPriceValues);
                CommonMethods.setvalueAgainstKey(activity, "tendpricedata", endPriceValues);
                CommonMethods.setvalueAgainstKey(activity, "tkilometerdata", kilometerValues);
                if (yearValues.equalsIgnoreCase("")) {
                    yearValues = "2002";
                }
                CommonMethods.setvalueAgainstKey(activity, "tyeardata", yearValues);
                CommonMethods.setvalueAgainstKey(activity, "startdealerpricedata", dealer_startPriceValues);
                CommonMethods.setvalueAgainstKey(activity, "enddealerpricedata", dealer_endPriceValues);

                ToolsFilterInstance.getInstance().setToolsfilterby("tfilterapply");

                finish();

            }
        });

        filterbycancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.setvalueAgainstKey(activity, "tclear", "false");
                finish();
            }
        });

        filterPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*
                Intent intent = new Intent(StockFilterActivity.this,FilterPriceActvity.class);
                startActivity(intent);*/

                if (price) {
                    price_values.setVisibility(View.VISIBLE);
                    price_arrow.setBackgroundResource(R.drawable.up_arrow);
                    price = false;
                } else {
                    price_values.setVisibility(View.GONE);
                    price_arrow.setBackgroundResource(R.drawable.down_arrow);
                    price = true;
                }
            }
        });

        dealerfilterPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*
                Intent intent = new Intent(StockFilterActivity.this,FilterPriceActvity.class);
                startActivity(intent);*/

                if (dealer_price) {
                    dealer_price_values.setVisibility(View.VISIBLE);
                    dealer_price_arrow.setBackgroundResource(R.drawable.up_arrow);
                    dealer_price = false;
                } else {
                    dealer_price_values.setVisibility(View.GONE);
                    dealer_price_arrow.setBackgroundResource(R.drawable.down_arrow);
                    dealer_price = true;
                }
            }
        });


        filter_seekbar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    int progress = 0;

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                        progress = progressValue;
                        CommonMethods.setvalueAgainstKey(activity, "tkms", "true");

                        if (progress == 0 || progress <= 10000) {
                            kmsvalues.setText("10,000" + " Kms");
                            progval = 10000;
                        } else if (10000 < progress && progress <= 20000) {
                            kmsvalues.setText("20,000" + " Kms");
                            progval = 20000;
                        } else if (20000 < progress && progress <= 30000) {
                            kmsvalues.setText("30,000" + " Kms");
                            progval = 30000;
                        } else if (30000 < progress && progress <= 40000) {
                            kmsvalues.setText("40,000" + " Kms");
                            progval = 40000;
                        } else if (40000 < progress && progress <= 50000) {
                            kmsvalues.setText("50,000" + " Kms");
                            progval = 50000;
                        } else if (50000 < progress && progress <= 60000) {
                            kmsvalues.setText("60,000" + " Kms");
                            progval = 60000;
                        } else if (60000 < progress && progress <= 70000) {
                            kmsvalues.setText("70,000" + " Kms");
                            progval = 70000;
                        } else if (70000 < progress && progress <= 80000) {
                            kmsvalues.setText("80,000" + " Kms");
                            progval = 80000;
                        } else if (80000 < progress && progress <= 90000) {
                            kmsvalues.setText("90,000" + " Kms");
                            progval = 90000;
                        } else if (90000 < progress && progress <= 100000) {
                            kmsvalues.setText("1,00,000" + " Kms");
                            progval = 100000;
                        } else if (100000 < progress && progress <= 200000) {
                            kmsvalues.setText("2,00,000" + " Kms");
                            progval = 200000;
                        } else if (200000 < progress && progress <= 300000) {
                            kmsvalues.setText("3,00,000" + " Kms");
                            progval = 300000;

                        } else if (300000 < progress && progress <= 400000) {
                            kmsvalues.setText("4,00,000" + " Kms");
                            progval = 400000;

                        } else if (400000 < progress && progress <= 500000) {
                            kmsvalues.setText("5,00,000" + " Kms");
                            progval = 500000;

                        } else if (500000 < progress && progress <= 600000) {
                            kmsvalues.setText("6,00,000" + " Kms");
                            progval = 600000;

                        } else if (600000 < progress && progress <= 700000) {
                            kmsvalues.setText("7,00,000" + " Kms");
                            progval = 700000;

                        } else if (700000 < progress && progress <= 800000) {
                            kmsvalues.setText("8,00,000" + " Kms");
                            progval = 800000;

                        } else if (800000 < progress && progress <= 900000) {
                            kmsvalues.setText("9,00,000" + " Kms");
                            progval = 900000;

                        } else if (900000 < progress && progress <= 1000000) {
                            kmsvalues.setText("10,00,000" + " Kms");
                            progval = 1000000;

                        }

                        kilometerValues = Integer.toString(progval);

                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });

        year_seekbar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    int progress = 0;

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                        progress = progressValue;
                        CommonMethods.setvalueAgainstKey(activity, "tyear", "true");

                        if (progressValue == 0 || progressValue <= 5) {
                            year_change.setText("2002" + " and Newer");
                            yearValues = "2002";
                        } else if (5 < progressValue && progressValue <= 10) {
                            year_change.setText("2002" + " and Newer");
                            yearValues = "2002";
                        } else if (10 < progressValue && progressValue <= 15) {
                            year_change.setText("2003" + " and Newer");
                            yearValues = "2003";
                        } else if (15 < progressValue && progressValue <= 20) {
                            year_change.setText("2004" + " and Newer");
                            yearValues = "2004";
                        } else if (20 < progressValue && progressValue <= 25) {
                            year_change.setText("2005" + " and Newer");
                            yearValues = "2005";
                        } else if (25 < progressValue && progressValue <= 30) {
                            year_change.setText("2006" + " and Newer");
                            yearValues = "2006";
                        } else if (30 < progressValue && progressValue <= 35) {
                            year_change.setText("2007" + " and Newer");
                            yearValues = "2007";
                        } else if (35 < progressValue && progressValue <= 40) {
                            year_change.setText("2008" + " and Newer");
                            yearValues = "2008";
                        } else if (40 < progressValue && progressValue <= 45) {
                            year_change.setText("2009" + " and Newer");
                            yearValues = "2009";
                        } else if (45 < progressValue && progressValue <= 50) {
                            year_change.setText("2010" + " and Newer");
                            yearValues = "2010";
                        } else if (50 < progressValue && progressValue <= 55) {
                            year_change.setText("2010" + " and Newer");
                            yearValues = "2010";
                        } else if (55 < progressValue && progressValue <= 60) {
                            year_change.setText("2011" + " and Newer");
                            yearValues = "2011";
                        } else if (60 < progressValue && progressValue <= 65) {
                            year_change.setText("2012" + " and Newer");
                            yearValues = "2012";
                        } else if (65 < progressValue && progressValue <= 70) {
                            year_change.setText("2013" + " and Newer");
                            yearValues = "2013";
                        } else if (70 < progressValue && progressValue <= 75) {
                            year_change.setText("2014" + " and Newer");
                            yearValues = "2014";
                        } else if (75 < progressValue && progressValue <= 80) {
                            year_change.setText("2015" + " and Newer");
                            yearValues = "2015";
                        } else if (80 < progressValue && progressValue <= 85) {
                            year_change.setText("2016" + " and Newer");
                            yearValues = "2016";
                        } else if (85 < progressValue && progressValue <= 90) {
                            year_change.setText("2017" + " and Newer");
                            yearValues = "2017";
                        } else if (90 < progressValue && progressValue <= 95) {
                            year_change.setText("2018" + " and Newer");
                            yearValues = "2018";
                        } else if (95 < progressValue && progressValue <= 100) {
                            year_change.setText("2018" + " and Newer");
                            yearValues = "2018";

                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });

        twoseekar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {

                CommonMethods.setvalueAgainstKey(activity, "tselling", "true");
                //code reena
                setStartEndPrice(leftPinIndex, rightPinIndex, "");
            }
        });

        threeseekar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                //code reena
                CommonMethods.setvalueAgainstKey(activity, "tdealer", "true");

                setStartEndPrice(leftPinIndex, rightPinIndex, "dealer");
            }
        });
    }

    //code reena

    public void setStartEndPrice(int leftPinIndex, int rightPinIndex, String select) {
        if (leftPinIndex == 0) {
            if (select.equals("")) {
                startprice.setText(getResources().getString(R.string.rs) + " " + "0");
                startPriceValues = "0";
            } else {
                start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "0");
                dealer_startPriceValues = "0";
            }
        } else if (leftPinIndex == 1) {

            if (select.equals("")) {
                startprice.setText(getResources().getString(R.string.rs) + " " + "1L");
                startPriceValues = "100000";
            } else {
                start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "1L");
                dealer_startPriceValues = "100000";
            }

        } else if (leftPinIndex == 2) {
            if (select.equals("")) {
                startprice.setText(getResources().getString(R.string.rs) + " " + "2L");
                startPriceValues = "200000";
            } else {
                start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "2L");
                dealer_startPriceValues = "200000";
            }
        } else if (leftPinIndex == 3) {
            if (select.equals("")) {
                startprice.setText(getResources().getString(R.string.rs) + " " + "3L");
                startPriceValues = "300000";
            } else {
                start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "3L");
                dealer_startPriceValues = "300000";
            }
        } else if (leftPinIndex == 4) {
            if (select.equals("")) {
                startprice.setText(getResources().getString(R.string.rs) + " " + "4L");
                startPriceValues = "400000";
            } else {
                start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "4L");
                dealer_startPriceValues = "400000";
            }
        } else if (leftPinIndex == 5) {
            if (select.equals("")) {
                startprice.setText(getResources().getString(R.string.rs) + " " + "5L");
                startPriceValues = "500000";
            } else {
                start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "5L");
                dealer_startPriceValues = "500000";
            }
        } else if (leftPinIndex == 6) {
            if (select.equals("")) {
                startprice.setText(getResources().getString(R.string.rs) + " " + "6L");
                startPriceValues = "600000";
            } else {
                start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "6L");
                dealer_startPriceValues = "600000";
            }
        } else if (leftPinIndex == 7) {
            if (select.equals("")) {
                startprice.setText(getResources().getString(R.string.rs) + " " + "7L");
                startPriceValues = "700000";
            } else {
                start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "7L");
                dealer_startPriceValues = "700000";
            }
        } else if (leftPinIndex == 8) {
            if (select.equals("")) {
                startprice.setText(getResources().getString(R.string.rs) + " " + "8L");
                startPriceValues = "800000";
            } else {
                start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "8L");
                dealer_startPriceValues = "800000";
            }
        } else if (leftPinIndex == 9) {
            if (select.equals("")) {
                startprice.setText(getResources().getString(R.string.rs) + " " + "9L");
                startPriceValues = "900000";
            } else {
                start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "9L");
                dealer_startPriceValues = "900000";
            }
        } else if (leftPinIndex == 10) {
            if (select.equals("")) {
                startprice.setText(getResources().getString(R.string.rs) + " " + "10L");
                startPriceValues = "1000000";
            } else {
                start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "10L");
                dealer_startPriceValues = "1000000";
            }
        } else if (leftPinIndex == 12) {
            if (select.equals("")) {
                startprice.setText(getResources().getString(R.string.rs) + " " + "20L");
                startPriceValues = "2000000";
            } else {
                start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "20L");
                dealer_startPriceValues = "2000000";
            }

        } else if (leftPinIndex == 13) {
            if (select.equals("")) {
                startprice.setText(getResources().getString(R.string.rs) + " " + "30L"); //change 1300000 to 30000000 reena
                startPriceValues = "3000000";
            } else {
                start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "30L");
                dealer_startPriceValues = "3000000";
            }
        } else if (leftPinIndex == 14) {
            if (select.equals("")) {
                startprice.setText(getResources().getString(R.string.rs) + " " + "40L");
                startPriceValues = "4000000";
            } else {
                start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "40L");
                dealer_startPriceValues = "4000000";
            }

        } else if (leftPinIndex == 15) {
            if (select.equals("")) {
                startprice.setText(getResources().getString(R.string.rs) + " " + "50L");
                startPriceValues = "5000000";
            } else {
                start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "50L");
                dealer_startPriceValues = "5000000";
            }

        } else if (leftPinIndex == 16) {
            if (select.equals("")) {
                startprice.setText(getResources().getString(R.string.rs) + " " + "60L");
                startPriceValues = "6000000";
            } else {
                start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "60L");
                dealer_startPriceValues = "6000000";
            }

        } else if (leftPinIndex == 17) {
            if (select.equals("")) {
                startprice.setText(getResources().getString(R.string.rs) + " " + "70L");
                startPriceValues = "7000000";
            } else {
                start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "70L");
                dealer_startPriceValues = "7000000";
            }

        } else if (leftPinIndex == 18) {
            if (select.equals("")) {
                startprice.setText(getResources().getString(R.string.rs) + " " + "80L");
                startPriceValues = "8000000";
            } else {
                start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "80L");
                dealer_startPriceValues = "8000000";
            }

        } else if (leftPinIndex == 19) {
            if (select.equals("")) {
                startprice.setText(getResources().getString(R.string.rs) + " " + "90L");
                startPriceValues = "9000000";
            } else {
                start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "90L");
                dealer_startPriceValues = "9000000";
            }

        } else if (leftPinIndex == 20) {
            if (select.equals("")) {
                startprice.setText(getResources().getString(R.string.rs) + " " + "1 Cr");
                startPriceValues = "10000000";
            } else {
                start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "1 Cr");
                dealer_startPriceValues = "10000000";
            }

        }

        if (rightPinIndex == 0) {
            if (select.equals("")) {
                endprice.setText(getResources().getString(R.string.rs) + " " + "0");
                endPriceValues = "0";
            } else {
                end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "0");
                dealer_endPriceValues = "0";
            }
        } else if (rightPinIndex == 1) {
            if (select.equals("")) {
                endprice.setText(getResources().getString(R.string.rs) + " " + "1L");
                endPriceValues = "100000";
            } else {
                end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "1L");
                dealer_endPriceValues = "100000";
            }
        } else if (rightPinIndex == 2) {
            if (select.equals("")) {
                endprice.setText(getResources().getString(R.string.rs) + " " + "2L");
                endPriceValues = "200000";
            } else {
                end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "2L");
                dealer_endPriceValues = "200000";
            }
        } else if (rightPinIndex == 3) {
            if (select.equals("")) {
                endprice.setText(getResources().getString(R.string.rs) + " " + "3L");
                endPriceValues = "300000";
            } else {
                end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "3L");
                dealer_endPriceValues = "300000";
            }

        } else if (rightPinIndex == 4) {
            if (select.equals("")) {
                endprice.setText(getResources().getString(R.string.rs) + " " + "4L");
                endPriceValues = "400000";
            } else {
                end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "4L");
                dealer_endPriceValues = "400000";
            }

        } else if (rightPinIndex == 5) {
            if (select.equals("")) {
                endprice.setText(getResources().getString(R.string.rs) + " " + "5L");
                endPriceValues = "500000";
            } else {
                end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "5L");
                dealer_endPriceValues = "500000";
            }
        } else if (rightPinIndex == 6) {
            if (select.equals("")) {
                endprice.setText(getResources().getString(R.string.rs) + " " + "6L");
                endPriceValues = "600000";
            } else {
                end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "6L");
                dealer_endPriceValues = "600000";
            }
        } else if (rightPinIndex == 7) {
            if (select.equals("")) {
                endprice.setText(getResources().getString(R.string.rs) + " " + "7L");
                endPriceValues = "700000";
            } else {
                end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "7L");
                dealer_endPriceValues = "700000";
            }
        } else if (rightPinIndex == 8) {
            if (select.equals("")) {
                endprice.setText(getResources().getString(R.string.rs) + " " + "8L");
                endPriceValues = "800000";
            } else {
                end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "8L");
                dealer_endPriceValues = "800000";
            }

        } else if (rightPinIndex == 9) {
            if (select.equals("")) {
                endprice.setText(getResources().getString(R.string.rs) + " " + "9L");
                endPriceValues = "900000";
            } else {
                end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "9L");
                dealer_endPriceValues = "900000";
            }

        } else if (rightPinIndex == 10) {
            if (select.equals("")) {
                endprice.setText(getResources().getString(R.string.rs) + " " + "10L");
                endPriceValues = "1000000";
            } else {
                end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "10L");
                dealer_endPriceValues = "1000000";
            }

        } else if (rightPinIndex == 12) {
            if (select.equals("")) {
                endprice.setText(getResources().getString(R.string.rs) + " " + "20L");
                endPriceValues = "2000000";
            } else {
                end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "20L");
                dealer_endPriceValues = "2000000";
            }

        } else if (rightPinIndex == 13) {
            if (select.equals("")) {
                endprice.setText(getResources().getString(R.string.rs) + " " + "30L");
                endPriceValues = "3000000";//
            } else {
                end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "30L");
                dealer_endPriceValues = "3000000";
            }

        } else if (rightPinIndex == 14) {
            if (select.equals("")) {
                endprice.setText(getResources().getString(R.string.rs) + " " + "40L");
                endPriceValues = "4000000";
            } else {
                end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "40L");
                dealer_endPriceValues = "4000000";
            }

        } else if (rightPinIndex == 15) {
            if (select.equals("")) {
                endprice.setText(getResources().getString(R.string.rs) + " " + "50L");
                endPriceValues = "5000000";
            } else {
                end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "50L");
                dealer_endPriceValues = "5000000";
            }

        } else if (rightPinIndex == 16) {
            if (select.equals("")) {
                endprice.setText(getResources().getString(R.string.rs) + " " + "60L");
                endPriceValues = "6000000";
            } else {
                end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "60L");
                dealer_endPriceValues = "6000000";
            }

        } else if (rightPinIndex == 17) {
            if (select.equals("")) {
                endprice.setText(getResources().getString(R.string.rs) + " " + "70L");
                endPriceValues = "7000000";
            } else {
                end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "70L");
                dealer_endPriceValues = "7000000";
            }

        } else if (rightPinIndex == 18) {
            if (select.equals("")) {
                endprice.setText(getResources().getString(R.string.rs) + " " + "80L");
                endPriceValues = "8000000";
            } else {
                end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "80L");
                dealer_endPriceValues = "8000000";
            }

        } else if (rightPinIndex == 19) {
            if (select.equals("")) {
                endprice.setText(getResources().getString(R.string.rs) + " " + "90L");
                endPriceValues = "9000000";
            } else {
                end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "90L");
                dealer_endPriceValues = "9000000";
            }

        } else if (rightPinIndex == 20) {
            if (select.equals("")) {
                endprice.setText(getResources().getString(R.string.rs) + " " + "1 Cr");
                endPriceValues = "10000000";
            } else {
                end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "1 Cr");
                dealer_endPriceValues = "10000000";
            }

        }
    }

    private void priceRefill() {
        if (CommonMethods.getstringvaluefromkey(activity, "tcertifiedval").equalsIgnoreCase("true")) {

            certifiedValues.setVisibility(View.VISIBLE);
            certifiedLevel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
            certified = false;
        } else {

            certifiedValues.setVisibility(View.GONE);
            certifiedLevel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
            certified = true;
        }

        if (CommonMethods.getstringvaluefromkey(activity, "tselling").equalsIgnoreCase("true")) {

            price_values.setVisibility(View.VISIBLE);
            price_arrow.setBackgroundResource(R.drawable.up_arrow);
            price = false;
        } else {
            price_values.setVisibility(View.GONE);
            price_arrow.setBackgroundResource(R.drawable.down_arrow);
            price = true;
        }
        if (CommonMethods.getstringvaluefromkey(activity, "tdealer").equalsIgnoreCase("true")) {

            dealer_price_values.setVisibility(View.VISIBLE);
            dealer_price_arrow.setBackgroundResource(R.drawable.up_arrow);
            dealer_price = false;
        } else {
            dealer_price_values.setVisibility(View.GONE);
            dealer_price_arrow.setBackgroundResource(R.drawable.down_arrow);
            dealer_price = true;
        }
        if (CommonMethods.getstringvaluefromkey(activity, "tyear").equalsIgnoreCase("true")) {

            year_values.setVisibility(View.VISIBLE);
            yearicon.setBackgroundResource(R.drawable.up_arrow);
            yearFlag = false;
        } else {
            year_values.setVisibility(View.GONE);
            yearicon.setBackgroundResource(R.drawable.down_arrow);
            yearFlag = true;
        }

        if (CommonMethods.getstringvaluefromkey(activity, "tkms").equalsIgnoreCase("true")) {
            kmsValues.setVisibility(View.VISIBLE);
            kmsLelel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
            kms = false;
        } else {
            kmsValues.setVisibility(View.GONE);
            kmsLelel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
            kms = true;
        }
        year_seekbar.getProgress();
        Log.e("seekpro", year_seekbar.getProgress() + "");

        twoseekar.setRangePinsByValue(start, end);
        threeseekar.setRangePinsByValue(dealer_start, dealer_end);
        Log.e("tkilometerValues=", "tkilometerValues=" + kilometerValues);
        Log.e("tyearValues=", "tyearValues=" + yearValues);

        if (CommonMethods.getstringvaluefromkey(this, "tstartpricedata").equalsIgnoreCase("")) {
            CommonMethods.setvalueAgainstKey(activity, "tstartpricedata", startPriceValues);
        }

        if (CommonMethods.getstringvaluefromkey(this, "tendpricedata").equalsIgnoreCase("")) {
            CommonMethods.setvalueAgainstKey(activity, "tendpricedata", endPriceValues);
        }

        if (CommonMethods.getstringvaluefromkey(this, "startdealerpricedata").equalsIgnoreCase("")) {
            CommonMethods.setvalueAgainstKey(activity, "startdealerpricedata", dealer_startPriceValues);
        }

        if (CommonMethods.getstringvaluefromkey(this, "enddealerpricedata").equalsIgnoreCase("")) {
            CommonMethods.setvalueAgainstKey(activity, "enddealerpricedata", dealer_endPriceValues);
        }

        if (CommonMethods.getstringvaluefromkey(this, "tkilometerdata").equalsIgnoreCase("")) {
            CommonMethods.setvalueAgainstKey(activity, "tkilometerdata", kilometerValues);
        }

        if (CommonMethods.getstringvaluefromkey(this, "tyeardata").equalsIgnoreCase("")) {
            CommonMethods.setvalueAgainstKey(activity, "tyeardata", yearValues);
        }

        //  if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("StockStore")) {

        startPriceValues = CommonMethods.getstringvaluefromkey(activity, "tstartpricedata").toString();
        endPriceValues = CommonMethods.getstringvaluefromkey(activity, "tendpricedata").toString();
        dealer_startPriceValues = CommonMethods.getstringvaluefromkey(activity, "startdealerpricedata").toString();
        dealer_endPriceValues = CommonMethods.getstringvaluefromkey(activity, "enddealerpricedata").toString();
        kilometerValues = CommonMethods.getstringvaluefromkey(activity, "tkilometerdata").toString();
        yearValues = CommonMethods.getstringvaluefromkey(activity, "tyeardata").toString();

       /* } else {
            startPriceValues = CommonMethods.getstringvaluefromkey(activity, "bookstartpricedata").toString();
            endPriceValues = CommonMethods.getstringvaluefromkey(activity, "bookendpricedata").toString();
            kilometerValues = CommonMethods.getstringvaluefromkey(activity, "bookkilometerdata").toString();

            yearValues = CommonMethods.getstringvaluefromkey(activity, "bookyeardata").toString();
        }*/

        Log.e("startPriceValues", startPriceValues);
        Log.e("endPriceValues", endPriceValues);
        Log.e("dealer_startPriceValues", dealer_startPriceValues);
        Log.e("dealer_endPriceValues", dealer_endPriceValues);
        Log.e("kilometerValues", kilometerValues);
        Log.e("yearValues", yearValues);

        filter_seekbar.setProgress(Integer.parseInt(kilometerValues));
//        Code by Reena
        //  kmsvalues.setText(" " + kilometerValues + " Kms");

        int kilm = Integer.parseInt(kilometerValues);

        progval = kilm;

        if (kilm == 0 || kilm <= 10000) {
            kmsvalues.setText("10,000" + " Kms");
        } else if (10000 < kilm && kilm <= 20000) {
            kmsvalues.setText("20,000" + " Kms");
        } else if (20000 < kilm && kilm <= 30000) {
            kmsvalues.setText("30,000" + " Kms");
        } else if (30000 < kilm && kilm <= 40000) {
            kmsvalues.setText("40,000" + " Kms");
        } else if (40000 < kilm && kilm <= 50000) {
            kmsvalues.setText("50,000" + " Kms");
        } else if (50000 < kilm && kilm <= 60000) {
            kmsvalues.setText("60,000" + " Kms");
        } else if (60000 < kilm && kilm <= 70000) {
            kmsvalues.setText("70,000" + " Kms");
        } else if (70000 < kilm && kilm <= 80000) {
            kmsvalues.setText("80,000" + " Kms");
        } else if (80000 < kilm && kilm <= 90000) {
            kmsvalues.setText("90,000" + " Kms");
        } else if (90000 < kilm && kilm <= 100000) {
            kmsvalues.setText("1,00,000" + " Kms");
        } else if (100000 < kilm && kilm <= 200000) {
            kmsvalues.setText("2,00,000" + " Kms");
        } else if (200000 < kilm && kilm <= 300000) {
            kmsvalues.setText("3,00,000" + " Kms");
        } else if (300000 < kilm && kilm <= 400000) {
            kmsvalues.setText("4,00,000" + " Kms");
        } else if (400000 < kilm && kilm <= 500000) {
            kmsvalues.setText("5,00,000" + " Kms");
        } else if (500000 < kilm && kilm <= 600000) {
            kmsvalues.setText("6,00,000" + " Kms");
        } else if (600000 < kilm && kilm <= 700000) {
            kmsvalues.setText("7,00,000" + " Kms");
        } else if (700000 < kilm && kilm <= 800000) {
            kmsvalues.setText("8,00,000" + " Kms");
        } else if (800000 < kilm && kilm <= 900000) {
            kmsvalues.setText("9,00,000" + " Kms");
        } else if (900000 < kilm && kilm <= 1000000) {
            kmsvalues.setText("10,00,000" + " Kms");
        }
        /* else if (100000 < kilm && kilm <= 120000) {
            kmsvalues.setText("1,20,000" + " Kms");
        } else if (120000 < kilm && kilm <= 140000) {
            kmsvalues.setText("1,40,000" + " Kms");
        } else if (140000 < kilm && kilm <= 160000) {
            kmsvalues.setText("1,60,000" + " Kms");
        } else if (160000 < kilm && kilm <= 180000) {
            kmsvalues.setText("1,80,000" + " Kms");
        } else if (180000 < kilm && kilm <= 200000) {
            kmsvalues.setText("2,00,000" + " Kms");
        }*/

        if (yearValues.equalsIgnoreCase("2002")) {
            year_seekbar.setProgress(0);
        } else if (yearValues.equalsIgnoreCase("2003")) {
            year_seekbar.setProgress(15);
        } else if (yearValues.equalsIgnoreCase("2004")) {
            year_seekbar.setProgress(20);
        } else if (yearValues.equalsIgnoreCase("2005")) {
            year_seekbar.setProgress(25);
        } else if (yearValues.equalsIgnoreCase("2006")) {
            year_seekbar.setProgress(30);
        } else if (yearValues.equalsIgnoreCase("2007")) {
            year_seekbar.setProgress(35);
        } else if (yearValues.equalsIgnoreCase("2008")) {
            year_seekbar.setProgress(40);
        } else if (yearValues.equalsIgnoreCase("2009")) {
            year_seekbar.setProgress(45);
        } else if (yearValues.equalsIgnoreCase("2010")) {
            year_seekbar.setProgress(55);
        } else if (yearValues.equalsIgnoreCase("2011")) {
            year_seekbar.setProgress(60);
        } else if (yearValues.equalsIgnoreCase("2012")) {
            year_seekbar.setProgress(65);
        } else if (yearValues.equalsIgnoreCase("2013")) {
            year_seekbar.setProgress(70);
        } else if (yearValues.equalsIgnoreCase("2014")) {
            year_seekbar.setProgress(75);
        } else if (yearValues.equalsIgnoreCase("2015")) {
            year_seekbar.setProgress(80);
        } else if (yearValues.equalsIgnoreCase("2016")) {
            year_seekbar.setProgress(85);
        } else if (yearValues.equalsIgnoreCase("2017")) {
            year_seekbar.setProgress(90);
        } else if (yearValues.equalsIgnoreCase("2018")) {
            year_seekbar.setProgress(100);
        }
//        Code by Reena, Change condition object name for year_seekbar
        year_change.setText(yearValues + " and Newer");
        ////////////////////////////////////

        Log.e("stratvaule", startPriceValues);
        Log.e("endalue", endPriceValues);
        if (startPriceValues.equalsIgnoreCase("100000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "1L");
            start = 5;
        } else if (startPriceValues.equalsIgnoreCase("200000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "2L");
            start = 10;
        } else if (startPriceValues.equalsIgnoreCase("300000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "3L");
            start = 15;
        } else if (startPriceValues.equalsIgnoreCase("400000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "4L");
            start = 20;
        } else if (startPriceValues.equalsIgnoreCase("500000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "5L");
            start = 25;
        } else if (startPriceValues.equalsIgnoreCase("600000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "6L");
            start = 30;
        } else if (startPriceValues.equalsIgnoreCase("700000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "7L");
            start = 35;
        } else if (startPriceValues.equalsIgnoreCase("800000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "8L");
            start = 40;
        } else if (startPriceValues.equalsIgnoreCase("900000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "9L");
            start = 45;
        } else if (startPriceValues.equalsIgnoreCase("1000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "10L");
            start = 50;
        } else if (startPriceValues.equalsIgnoreCase("2000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "20L");
            start = 55;
        } else if (startPriceValues.equalsIgnoreCase("3000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "30L");
            start = 60;
        } else if (startPriceValues.equalsIgnoreCase("4000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "40L");
            start = 65;
        } else if (startPriceValues.equalsIgnoreCase("5000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "50L");
            start = 70;
        } else if (startPriceValues.equalsIgnoreCase("6000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "60L");
            start = 75;
        } else if (startPriceValues.equalsIgnoreCase("7000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "70L");
            start = 80;
        } else if (startPriceValues.equalsIgnoreCase("8000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "80L");
            start = 85;
        } else if (startPriceValues.equalsIgnoreCase("9000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "90L");
            start = 90;
        } else if (startPriceValues.equalsIgnoreCase("10000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "1 Cr");
            start = 100;
        } else {
            startprice.setText(getResources().getString(R.string.rs) + " " + "0");
            start = 0;
        }

        if (endPriceValues.equalsIgnoreCase("100000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "1L");
            end = 5;
        } else if (endPriceValues.equalsIgnoreCase("200000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "2L");
            end = 10;
        } else if (endPriceValues.equalsIgnoreCase("300000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "3L");
            end = 15;
        } else if (endPriceValues.equalsIgnoreCase("400000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "4L");
            end = 20;
        } else if (endPriceValues.equalsIgnoreCase("500000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "5L");
            end = 25;
        } else if (endPriceValues.equalsIgnoreCase("600000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "6L");
            end = 30;
        } else if (endPriceValues.equalsIgnoreCase("700000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "7L");
            end = 35;
        } else if (endPriceValues.equalsIgnoreCase("800000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "8L");
            end = 40;
        } else if (endPriceValues.equalsIgnoreCase("900000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "9L");
            end = 45;
        } else if (endPriceValues.equalsIgnoreCase("1000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "10L");
            end = 50;
        } else if (endPriceValues.equalsIgnoreCase("2000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "20L");
            end = 55;
        } else if (endPriceValues.equalsIgnoreCase("3000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "30L");
            end = 60;
        } else if (endPriceValues.equalsIgnoreCase("4000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "40L");
            end = 65;
        } else if (endPriceValues.equalsIgnoreCase("5000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "50L");
            end = 70;
        } else if (endPriceValues.equalsIgnoreCase("6000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "60L");
            end = 75;
        } else if (endPriceValues.equalsIgnoreCase("7000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "70L");
            end = 80;
        } else if (endPriceValues.equalsIgnoreCase("8000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "80L");
            end = 85;
        } else if (endPriceValues.equalsIgnoreCase("9000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "90L");
            end = 90;
        } else if (endPriceValues.equalsIgnoreCase("10000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "1 Cr");
            end = 100;
        } else {
            endprice.setText(getResources().getString(R.string.rs) + " " + "0");
            end = 0;
        }

        twoseekar.setRangePinsByValue(start, end);

        //--Sonali--

        Log.e("dealer_stratvaule", dealer_startPriceValues);
        Log.e("dealer_endalue", dealer_endPriceValues);
        if (dealer_startPriceValues.equalsIgnoreCase("100000")) {
            start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "1L");
            dealer_start = 5;
        } else if (dealer_startPriceValues.equalsIgnoreCase("200000")) {
            start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "2L");
            dealer_start = 10;
        } else if (dealer_startPriceValues.equalsIgnoreCase("300000")) {
            start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "3L");
            dealer_start = 15;
        } else if (dealer_startPriceValues.equalsIgnoreCase("400000")) {
            start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "4L");
            dealer_start = 20;
        } else if (dealer_startPriceValues.equalsIgnoreCase("500000")) {
            start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "5L");
            dealer_start = 25;
        } else if (dealer_startPriceValues.equalsIgnoreCase("600000")) {
            start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "6L");
            dealer_start = 30;
        } else if (dealer_startPriceValues.equalsIgnoreCase("700000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "7L");
            dealer_start = 35;
        } else if (dealer_startPriceValues.equalsIgnoreCase("800000")) {
            start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "8L");
            dealer_start = 40;
        } else if (dealer_startPriceValues.equalsIgnoreCase("900000")) {
            start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "9L");
            dealer_start = 45;
        } else if (dealer_startPriceValues.equalsIgnoreCase("1000000")) {
            start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "10L");
            dealer_start = 50;
        } else if (dealer_startPriceValues.equalsIgnoreCase("2000000")) {
            start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "20L");
            dealer_start = 55;
        } else if (dealer_startPriceValues.equalsIgnoreCase("3000000")) {
            start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "30L");
            dealer_start = 60;
        } else if (dealer_startPriceValues.equalsIgnoreCase("4000000")) {
            start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "40L");
            dealer_start = 65;
        } else if (dealer_startPriceValues.equalsIgnoreCase("5000000")) {
            start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "50L");
            dealer_start = 70;
        } else if (dealer_startPriceValues.equalsIgnoreCase("6000000")) {
            start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "60L");
            dealer_start = 75;
        } else if (dealer_startPriceValues.equalsIgnoreCase("7000000")) {
            start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "70L");
            dealer_start = 80;
        } else if (dealer_startPriceValues.equalsIgnoreCase("8000000")) {
            start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "80L");
            dealer_start = 85;
        } else if (dealer_startPriceValues.equalsIgnoreCase("9000000")) {
            start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "90L");
            dealer_start = 90;
        } else if (dealer_startPriceValues.equalsIgnoreCase("10000000")) {
            start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "1 Cr");
            dealer_start = 100;
        } else {
            start_dealerprice.setText(getResources().getString(R.string.rs) + " " + "0");
            dealer_start = 0;
        }

        if (dealer_endPriceValues.equalsIgnoreCase("100000")) {
            end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "1L");
            dealer_end = 5;
        } else if (dealer_endPriceValues.equalsIgnoreCase("200000")) {
            end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "2L");
            dealer_end = 10;
        } else if (dealer_endPriceValues.equalsIgnoreCase("300000")) {
            end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "3L");
            dealer_end = 15;
        } else if (dealer_endPriceValues.equalsIgnoreCase("400000")) {
            end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "4L");
            dealer_end = 20;
        } else if (dealer_endPriceValues.equalsIgnoreCase("500000")) {
            end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "5L");
            dealer_end = 25;
        } else if (dealer_endPriceValues.equalsIgnoreCase("600000")) {
            end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "6L");
            dealer_end = 30;
        } else if (dealer_endPriceValues.equalsIgnoreCase("700000")) {
            end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "7L");
            dealer_end = 35;
        } else if (dealer_endPriceValues.equalsIgnoreCase("800000")) {
            end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "8L");
            dealer_end = 40;
        } else if (dealer_endPriceValues.equalsIgnoreCase("900000")) {
            end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "9L");
            dealer_end = 45;
        } else if (dealer_endPriceValues.equalsIgnoreCase("1000000")) {
            end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "10L");
            dealer_end = 50;
        } else if (dealer_endPriceValues.equalsIgnoreCase("2000000")) {
            end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "20L");
            dealer_end = 55;
        } else if (dealer_endPriceValues.equalsIgnoreCase("3000000")) {
            end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "30L");
            dealer_end = 60;
        } else if (dealer_endPriceValues.equalsIgnoreCase("4000000")) {
            end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "40L");
            dealer_end = 65;
        } else if (dealer_endPriceValues.equalsIgnoreCase("5000000")) {
            end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "50L");
            dealer_end = 70;
        } else if (dealer_endPriceValues.equalsIgnoreCase("6000000")) {
            end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "60L");
            dealer_end = 75;
        } else if (dealer_endPriceValues.equalsIgnoreCase("7000000")) {
            end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "70L");
            dealer_end = 80;
        } else if (dealer_endPriceValues.equalsIgnoreCase("8000000")) {
            end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "80L");
            dealer_end = 85;
        } else if (dealer_endPriceValues.equalsIgnoreCase("9000000")) {
            end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "90L");
            dealer_end = 90;
        } else if (dealer_endPriceValues.equalsIgnoreCase("10000000")) {
            end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "1 Cr");
            dealer_end = 100;
        } else {
            end_dealer_price.setText(getResources().getString(R.string.rs) + " " + "0");
            dealer_end = 0;
        }

        threeseekar.setRangePinsByValue(dealer_start, dealer_end);

    }

    @OnClick(R.id.certified_level)
    public void test(View view) {
        if (certified) {
            certifiedValues.setVisibility(View.VISIBLE);
            certifiedLevel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
            certified = false;

        } else {
            certifiedValues.setVisibility(View.GONE);
            certifiedLevel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
            certified = true;

        }
    }

    @OnClick(R.id.filter_cetified_car)
    public void filter_cetified_car(View view) {
        CommonMethods.setvalueAgainstKey(activity, "tcertifiedval", "true");
        filterCetifiedCar.setBackgroundResource(R.drawable.my_button);
        filterCetifiedCar.setTextColor(getResources().getColor(R.color.Black));
        certifiedFlag = true;
        filterAllCar.setBackgroundResource(R.drawable.my_buttom_gray_light);
        filterAllCar.setTextColor(getResources().getColor(R.color.lgray));
    }

    @OnClick(R.id.filter_all_cars)
    public void filter_all_cars(View view) {
        CommonMethods.setvalueAgainstKey(activity, "tcertifiedval", "false");
        filterAllCar.setBackgroundResource(R.drawable.my_button);
        filterAllCar.setTextColor(getResources().getColor(R.color.Black));
        certifiedFlag = false;
        filterCetifiedCar.setBackgroundResource(R.drawable.my_buttom_gray_light);
        filterCetifiedCar.setTextColor(getResources().getColor(R.color.lgray));

    }

    @OnClick(R.id.year_card)
    public void year_card(View view) {
        if (yearFlag) {
            year_values.setVisibility(View.VISIBLE);
            yearicon.setBackgroundResource(R.drawable.up_arrow);

            yearFlag = false;
        } else {
            year_values.setVisibility(View.GONE);
            yearicon.setBackgroundResource(R.drawable.down_arrow);

            yearFlag = true;
        }
    }

    @OnClick(R.id.kms_level)
    public void kms(View view) {

        if (kms) {
            kmsValues.setVisibility(View.VISIBLE);
            kmsLelel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
            kms = false;
        } else {
            kmsValues.setVisibility(View.GONE);
            kmsLelel.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
            kms = true;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // put your code here...

        Application.getInstance().trackScreenView(activity,GlobalText.ToolsFilter_Activity);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item, menu);
        MenuItem item = menu.findItem(R.id.share);
        item.setVisible(false);
        item = menu.findItem(R.id.add_image);
        item.setVisible(false);
        item = menu.findItem(R.id.delete_fea);
        item.setVisible(false);
        item = menu.findItem(R.id.stock_fil_clear);
        item.setVisible(true);
        item = menu.findItem(R.id.notification_bell);
        item.setVisible(false);
        item = menu.findItem(R.id.notification_cancel);
        item.setVisible(false);
        item = menu.findItem(R.id.asmHome);
        item.setVisible(false);
        item = menu.findItem(R.id.asm_mail);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;

            case R.id.stock_fil_clear:
               // Log.e("tets", "test");
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.tool_filter_clear, GlobalText.android);

                stock_photocountsFlag = false;
                CommonMethods.setvalueAgainstKey(activity, "thirtydaycheckbox", "false");

                CommonMethods.setvalueAgainstKey(activity, "tclear", "true");

                CommonMethods.setvalueAgainstKey(activity, "photocounts", "false");
                CommonMethods.setvalueAgainstKey(activity, "photocounts1", "false");
                CommonMethods.setvalueAgainstKey(activity, "photocounts2", "false");

              /*  CommonMethods.setvalueAgainstKey(activity, "tcertifiedval", "true");
                CommonMethods.setvalueAgainstKey(activity, "tselling", "true");
                CommonMethods.setvalueAgainstKey(activity, "tdealer", "true");
                CommonMethods.setvalueAgainstKey(activity, "tyear", "true");
                CommonMethods.setvalueAgainstKey(activity, "tkms", "true");*/
                Log.e("test", filter_seekbar.getProgress() + "");
                kilometerValues = "1000000";
                filter_seekbar.setProgress(Integer.parseInt(kilometerValues));
//        Code by Reena
                kmsvalues.setText(" " + "10,00,000" + " Kms");

                //year reena
                yearValues = "2002";
                if (yearValues.equalsIgnoreCase("2002")) {
                    year_seekbar.setProgress(0);
                }
                year_change.setText(yearValues + " and Newer");


                //twoseekbar
                start = 0;
                end = 100;
                twoseekar.setRangePinsByValue(start, end);

                //threeseekbar
                dealer_start = 0;
                dealer_end = 100;
                threeseekar.setRangePinsByValue(dealer_start, dealer_end);

                //allcar
                certifiedFlag = false;
                filterAllCar.setBackgroundResource(R.drawable.my_button);
                filterAllCar.setTextColor(getResources().getColor(R.color.Black));
                CommonMethods.setvalueAgainstKey(activity, "tcertifiedstatus", "allcar");

                filterCetifiedCar.setBackgroundResource(R.drawable.my_buttom_gray_light);
                filterCetifiedCar.setTextColor(getResources().getColor(R.color.lgray));

                yearValues = "2002";
                kilometerValues = "1000000";
                startPriceValues = "0";
                endPriceValues = "10000000";
                dealer_startPriceValues = "0";
                dealer_endPriceValues = "10000000";
                CommonMethods.setvalueAgainstKey(activity, "tstartpricedata", startPriceValues);
                CommonMethods.setvalueAgainstKey(activity, "tendpricedata", endPriceValues);
                CommonMethods.setvalueAgainstKey(activity, "tkilometerdata", kilometerValues);
                CommonMethods.setvalueAgainstKey(activity, "tyeardata", yearValues);
                CommonMethods.setvalueAgainstKey(activity, "startdealerpricedata", dealer_startPriceValues);
                CommonMethods.setvalueAgainstKey(activity, "enddealerpricedata", dealer_endPriceValues);

                if (CommonMethods.getstringvaluefromkey(activity, "tclear").equalsIgnoreCase("true")) {
                    CommonMethods.setvalueAgainstKey(activity, "tcertifiedval", "false");
                    CommonMethods.setvalueAgainstKey(activity, "tselling", "false");
                    CommonMethods.setvalueAgainstKey(activity, "tdealer", "false");
                    CommonMethods.setvalueAgainstKey(activity, "tyear", "false");
                    CommonMethods.setvalueAgainstKey(activity, "tkms", "false");
                    CommonMethods.setvalueAgainstKey(activity, "tclear", "false");
                }

                filterby_apply.performClick();

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String type = intent.getStringExtra("type");

            Log.e("test receiver", type);
        }
    };
}
