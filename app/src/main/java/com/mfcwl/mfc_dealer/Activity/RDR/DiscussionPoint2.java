package com.mfcwl.mfc_dealer.Activity.RDR;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GAConstants;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.DiscussionPoint;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.ScreenInformation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import imageeditor.base.BaseActivity;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.activity;

public class DiscussionPoint2 extends BaseActivity {

    TextView tvDateTime, tvDealerName, tvFocusArea, tvSubFA;
    ImageView createRepo_backbtn;
    TextView tvSalesTarget, tvProcurementTarget, tvStocksAdded;
    TextView tvActionItems, tvResponsibilities, tvTargetDateCalendar;
    TextView tvPrevious, tvNext, tvFooterHeading;
    EditText etBidsOwn,etvehiclereg;
    String strBidsOwn, strStocksInRefurbishment, strVehicleReg;

    // R4

    EditText et_procurement_from_xmart,et_self_procurement;
    String strProcurementFromXMart="",strSelfProcurement="";
    TextView tvStocksInProgressTitle;
    //
    private String[] mRegNo;

    private ImageView mAddMore;

    private LinearLayout mContainer;
    private Activitykiller mKiller;
    private LinearLayout llresandtarget;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.discussion_point2);
        Log.i(TAG, "onCreate: ");
        initView();

        setListeners();

        mKiller = new Activitykiller();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(mKiller,intentFilter);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mKiller);
    }

    public void initView() {

        tvDateTime = findViewById(R.id.tvDateTime);
        tvDealerName = findViewById(R.id.tvDealer);
        tvFocusArea = findViewById(R.id.tvFocusArea);
        tvSubFA = findViewById(R.id.tvSubFA);
        createRepo_backbtn = findViewById(R.id.createRepo_backbtn);
        mAddMore = findViewById(R.id.addmore);
        mContainer = findViewById(R.id.parent);

        tvSalesTarget = findViewById(R.id.tvSalesTarget);
        tvProcurementTarget = findViewById(R.id.tvProcurementTarget);
        tvStocksAdded = findViewById(R.id.tvStocksAdded);

        etBidsOwn = findViewById(R.id.etBidsOwn);
        etvehiclereg = findViewById(R.id.etregnumber);

        tvActionItems = findViewById(R.id.tvActionItemList);
        tvResponsibilities = findViewById(R.id.tvResponsibilityList);
        tvTargetDateCalendar = findViewById(R.id.tvTargetDateCalendar);

        tvPrevious = findViewById(R.id.tvPrevious);
        tvNext = findViewById(R.id.tvNext);
        tvFooterHeading = findViewById(R.id.tvFooterHeading);

        tvActionItems.setMovementMethod(new ScrollingMovementMethod());
        tvResponsibilities.setMovementMethod(new ScrollingMovementMethod());
        llresandtarget = findViewById(R.id.llresandtarget);

        //R4
        et_procurement_from_xmart=findViewById(R.id.et_procurement_from_xmart);
        et_self_procurement=findViewById(R.id.et_self_procurement);
        tvStocksInProgressTitle=findViewById(R.id.tvStocksInProgressTitle);


    }

    public void setListeners() {

        tvPrevious.setOnClickListener(this);
        tvNext.setOnClickListener(this);
        tvActionItems.setOnClickListener(this);
        tvResponsibilities.setOnClickListener(this);
        tvTargetDateCalendar.setOnClickListener(this);
        createRepo_backbtn.setOnClickListener(this);
        mAddMore.setOnClickListener(this);
        try {
            tvDateTime.setText(getDateTime());
            tvDealerName.setText(dealerName + "(" + dealerId + ")");
            tvFocusArea.setText("Procurement");
            tvSubFA.setText("Stocks in progress");
            tvFooterHeading.setText("Discussion point 2");

            //etBidsOwn.setText(dealerReportResponse.bidsWon);
            //etStocksInRefurbishment.setText(": " + dealerReportResponse.stocksInRefurbishment);*/

            tvSalesTarget.setText("Sales Target: " + dealerReportResponse.salesTarget);
            tvProcurementTarget.setText("Procurement Target: " + dealerReportResponse.procurementTargetStocks);
            tvStocksAdded.setText("Stocks added(Today): " + dealerReportResponse.stocksAdded);

            //R4
            //et_procurement_from_xmart.setText((Integer) dealerReportResponse.procurementXmart);

        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }


        tvActionItems.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String data = s.toString();
                if(data.equalsIgnoreCase("No action required")){
                    llresandtarget.setVisibility(View.GONE);
                    strResponsibility = "NA";
                    strTargetDate = "07/17/2020";
                    Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_ACTION_ITEM, CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));

                }else {
                    llresandtarget.setVisibility(View.VISIBLE);
                    strResponsibility = "";
                    strTargetDate = "";
                    Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_ACTION_ITEM,CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));

                }
            }
        });
    }

    public DiscussionPoint getDiscussionPoint() {
        DiscussionPoint discussionPoint = new DiscussionPoint();
        discussionPoint.setActionItem(Arrays.asList(strActionItem.split(("\\|"))));
        discussionPoint.setResponsibility(strResponsibility);
        discussionPoint.setTargetDate(strTargetDate);
        discussionPoint.setFocusArea(tvFocusArea.getText().toString());
        discussionPoint.setSubFocusArea(tvSubFA.getText().toString());

        ScreenInformation screenInformation = new ScreenInformation();
        screenInformation.setSalesTarget(dealerReportResponse.salesTarget);
        screenInformation.setStocksAdded(dealerReportResponse.stocksAdded);
        screenInformation.setProcurementTargetStocks(dealerReportResponse.procurementTargetStocks);

        screenInformation.setProcurementXmart(strProcurementFromXMart);
        screenInformation.setSelfProcurement(strSelfProcurement);
        screenInformation.setBidsWon(strBidsOwn);
        readDataFromDynamicViews();
        screenInformation.setStocksinrefurbishment(mRegNo);
        discussionPoint.setScreenInformation(screenInformation);



        return discussionPoint;
    }

    private boolean validate() {

        strActionItem = tvActionItems.getText().toString();
        if(!strActionItem.equalsIgnoreCase("No action required")){
            strResponsibility = tvResponsibilities.getText().toString();
            strTargetDate = tvTargetDateCalendar.getText().toString();
        }


        strProcurementFromXMart=et_procurement_from_xmart.getText().toString();
        strSelfProcurement=et_self_procurement.getText().toString();
        strBidsOwn = etBidsOwn.getText().toString();
        strVehicleReg = etvehiclereg.getText().toString();
       // strStocksInRefurbishment = etStocksInRefurbishment.getText().toString();

        boolean allSelected = true;

        if (strActionItem.equals("") ||
                strResponsibility.equals("") ||
                strTargetDate.equals("") ||
                strBidsOwn.equals("") || strVehicleReg.equals("")||strProcurementFromXMart.equals("")||strSelfProcurement.equals(""))
        {
            allSelected = false;
            Toast.makeText(this, "Please select all fields", Toast.LENGTH_SHORT).show();
        }

        return allSelected;
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.createRepo_backbtn:
                startActivity(new Intent(this, DiscussionPoint1.class));
                break;
            case R.id.tvPrevious:
                startActivity(new Intent(this, DiscussionPoint1.class));
                break;
            case R.id.tvNext:
                if (validate()) {
                    removeAnyDuplicates(tvSubFA.getText().toString());
                    discussionPoints.add(getDiscussionPoint());
                    startActivity(new Intent(this, DiscussionPoint3.class));
                }
                break;

            case R.id.addmore:
                onAddField(v);
                break;
            case R.id.tvActionItemList:
                if (dealerReportResponse != null && dealerReportResponse.stocksInProgress != null) {
                    setDialogMultiSelectionCheck(R.id.tvActionItemList, "Action Items", getStringArray(dealerReportResponse.stocksInProgress));
                }else {
                    Toast.makeText(DiscussionPoint2.this,"No data found",Toast.LENGTH_LONG).show();
                }
                break;

        }

    }


    public void onAddField(View v) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.edittext_delete, null);
        ImageView buttonRemove = rowView.findViewById(R.id.delete);

        final View.OnClickListener thisListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((LinearLayout) rowView.getParent()).removeView(rowView);

            }
        };

        buttonRemove.setOnClickListener(thisListener);
        mContainer.addView(rowView);
    }

    private void readDataFromDynamicViews() {

        int childCount = mContainer.getChildCount();
        mRegNo = new String[childCount+1];
        mRegNo[0] = strVehicleReg;
        for (int i = 0; i < childCount; i++) {
            View thisChild = mContainer.getChildAt(i);
            EditText childTextView = (EditText) thisChild.findViewById(R.id.etregnumberdy);
            String childTextViewValue = childTextView.getText().toString();
           mRegNo[i+1] = childTextViewValue;
        }

    }



}