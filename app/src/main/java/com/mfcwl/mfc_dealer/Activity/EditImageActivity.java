package com.mfcwl.mfc_dealer.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.app.ActivityCompat;
import androidx.transition.ChangeBounds;
import androidx.transition.TransitionManager;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.AnticipateOvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.ImageFilter.FilterActivity;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.Helper;
import com.soundcloud.android.crop.Crop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import imageeditor.EmojiBSFragment;
import imageeditor.PropertiesBSFragment;
import imageeditor.PropertiesContrastFragment;
import imageeditor.StickerBSFragment;
import imageeditor.TextEditorDialogFragment;
import imageeditor.base.BaseActivity;
import imageeditor.filters.FilterListener;
import imageeditor.filters.FilterViewAdapter;
import imageeditor.tools.EditingToolsAdapter;
import imageeditor.tools.ToolType;
import ja.burhanrashid52.photoeditor.OnPhotoEditorListener;
import ja.burhanrashid52.photoeditor.PhotoEditor;
import ja.burhanrashid52.photoeditor.PhotoEditorView;
import ja.burhanrashid52.photoeditor.PhotoFilter;
import ja.burhanrashid52.photoeditor.ViewType;

public class EditImageActivity extends BaseActivity implements OnPhotoEditorListener,
        View.OnClickListener,
        PropertiesContrastFragment.Properties,
        PropertiesBSFragment.Properties,
        EmojiBSFragment.EmojiListener,
        EditingToolsAdapter.OnItemSelected, FilterListener {

    private static final String TAG = EditImageActivity.class.getSimpleName();
    public static final String EXTRA_IMAGE_PATHS = "extra_image_paths";
    private static final int CAMERA_REQUEST = 52;
    private static final int PICK_REQUEST = 53;
    private static final int FILTER_REQUEST = 8;
    private PhotoEditor mPhotoEditor;
    private PhotoEditorView mPhotoEditorView;
    private PropertiesBSFragment mPropertiesBSFragment;
    private PropertiesContrastFragment mPropertiescontrastFragment;
    private EmojiBSFragment mEmojiBSFragment;
    private StickerBSFragment mStickerBSFragment;
    private TextView mTxtCurrentTool;
    private Typeface mWonderFont;
    private RecyclerView mRvTools, mRvFilters;
    private EditingToolsAdapter mEditingToolsAdapter = new EditingToolsAdapter(this);
    private FilterViewAdapter mFilterViewAdapter = new FilterViewAdapter(this);
    private ConstraintLayout mRootView;
    private ConstraintSet mConstraintSet = new ConstraintSet();
    private boolean mIsFilterVisible;
    private Uri imageUri;
    public static Bitmap bmpimg = null;
    private static final int PIC_CROP = 101;
    private int screenWidth, screenHeight;
    private DisplayMetrics displaymetrics;
    private File mFile;
    private int BITMAP_WIDTH_HIGHT = 950;
    private ProgressDialog mProgressDialog;
    public Context context;

    public float fbright = 50, fcontrast = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //makeFullScreen();
        setContentView(R.layout.activity_edit_image);
        context = this.getApplicationContext();
        Log.i(TAG, "onCreate: ");
        imageUri = ImageUploadActivity1.image1;/*Uri.parse(getIntent().getExtras().getString("imageUri"));*/
        displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        screenWidth = displaymetrics.widthPixels;
        screenHeight = displaymetrics.heightPixels;

        initViews();

        mWonderFont = Typeface.createFromAsset(getAssets(), "lato_semibold.ttf");

        mPropertiesBSFragment = new PropertiesBSFragment();
        mPropertiescontrastFragment = new PropertiesContrastFragment();
        mEmojiBSFragment = new EmojiBSFragment();
        mStickerBSFragment = new StickerBSFragment();
        // mStickerBSFragment.setStickerListener(this);
        mEmojiBSFragment.setEmojiListener(this);
        mPropertiesBSFragment.setPropertiesChangeListener(this);
        mPropertiescontrastFragment.setPropertiesChangeListener(this);

        LinearLayoutManager llmTools = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRvTools.setLayoutManager(llmTools);
        mRvTools.setAdapter(mEditingToolsAdapter);

        LinearLayoutManager llmFilters = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRvFilters.setLayoutManager(llmFilters);
        mRvFilters.setAdapter(mFilterViewAdapter);

        //Typeface mTextRobotoTf = ResourcesCompat.getFont(this, R.font.roboto_medium);
        //Typeface mEmojiTypeFace = Typeface.createFromAsset(getAssets(), "emojione-android.ttf");

        try {
            // bmpimg = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
            bmpimg = getBitmapFromReturnedImage(imageUri, BITMAP_WIDTH_HIGHT, BITMAP_WIDTH_HIGHT);
            mPhotoEditorView.getSource().setImageBitmap(bmpimg);
            //imgUri = getImageUri(context,bmpimg);
            //bmpimg = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imgUri);
        } catch (IOException e) {

            //  Log.e("IOException", "IOException=" + e);
        } catch (OutOfMemoryError memoryError) {
            System.gc();

        }

        mPhotoEditor = new PhotoEditor.Builder(this, mPhotoEditorView)
                .setPinchTextScalable(true) // set flag to make text scalable when pinch
                //.setDefaultTextTypeface(mTextRobotoTf)
                //.setDefaultEmojiTypeface(mEmojiTypeFace)
                .build(); // build photo editor sdk

        mPhotoEditor.setOnPhotoEditorListener(this);

        //Set Image Dynamically
        // mPhotoEditorView.getSource().setImageResource(R.drawable.color_palette);
    }

    private void initViews() {
        ImageView imgUndo;
        ImageView imgRedo;
        ImageView imgCamera;
        ImageView imgGallery;
        TextView imgSave;
        TextView imgClose;

        mPhotoEditorView = findViewById(R.id.photoEditorView);
        mTxtCurrentTool = findViewById(R.id.txtCurrentTool);
        mRvTools = findViewById(R.id.rvConstraintTools);
        mRvFilters = findViewById(R.id.rvFilterView);
        mRootView = findViewById(R.id.rootView);

        imgUndo = findViewById(R.id.imgUndo);
        imgUndo.setOnClickListener(this);

        imgRedo = findViewById(R.id.imgRedo);
        imgRedo.setOnClickListener(this);

        imgCamera = findViewById(R.id.imgCamera);
        imgCamera.setOnClickListener(this);

        imgGallery = findViewById(R.id.imgGallery);
        imgGallery.setOnClickListener(this);

        imgSave = findViewById(R.id.imgSave);
        imgSave.setOnClickListener(this);

        imgClose = findViewById(R.id.imgClose);
        imgClose.setOnClickListener(this);

    }

    @Override
    public void onEditTextChangeListener(final View rootView, String text, int colorCode) {
        TextEditorDialogFragment textEditorDialogFragment =
                TextEditorDialogFragment.show(this, text, colorCode);
        textEditorDialogFragment.setOnTextEditorListener(new TextEditorDialogFragment.TextEditor() {
            @Override
            public void onDone(String inputText, int colorCode) {
                mPhotoEditor.editText(rootView, inputText, colorCode);
                mTxtCurrentTool.setText(R.string.label_text);
            }
        });
    }

    @Override
    public void onAddViewListener(ViewType viewType, int numberOfAddedViews) {
        Log.d(TAG, "onAddViewListener() called with: viewType = [" + viewType + "], numberOfAddedViews = [" + numberOfAddedViews + "]");
    }

    @Override
    public void onRemoveViewListener(int numberOfAddedViews) {
        Log.d(TAG, "onRemoveViewListener() called with: numberOfAddedViews = [" + numberOfAddedViews + "]");
    }

    @Override
    public void onStartViewChangeListener(ViewType viewType) {
        Log.d(TAG, "onStartViewChangeListener() called with: viewType = [" + viewType + "]");
    }

    @Override
    public void onStopViewChangeListener(ViewType viewType) {
        Log.d(TAG, "onStopViewChangeListener() called with: viewType = [" + viewType + "]");
    }

    @Override
    protected void onResume() {
        super.onResume();

        Application.getInstance().trackScreenView(this, GlobalText.EditImageActivity);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.imgUndo:
                mPhotoEditor.undo();
                break;

            case R.id.imgRedo:
                mPhotoEditor.redo();
                break;

            case R.id.imgSave:
                saveImage();
                break;

            case R.id.imgClose:
                /*try {
                    if (bmpimg != null) {
                        bmpimg.recycle();
                        bmpimg = null;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
                onBackPressed();
                break;

            case R.id.imgCamera:
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
                break;

            case R.id.imgGallery:
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_REQUEST);
                break;
        }
    }

    @SuppressLint("MissingPermission")
    private void local_SaveImage() {
        if (requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // showLoading("Saving...");

            if (mProgressDialog == null) {
                mProgressDialog = createProgressDialog(EditImageActivity.this);

            }

            mProgressDialog.show();
            File file = new File(Environment.getExternalStorageDirectory()
                    + File.separator + ""
                    + System.currentTimeMillis() + ".png");
            try {
                file.createNewFile();
                mPhotoEditor.saveAsFile(file.getAbsolutePath(), new PhotoEditor.OnSaveListener() {
                    @Override
                    public void onSuccess(@NonNull String imagePath) {
                        //showSnackbar("Image Saved Successfully");
                        mPhotoEditorView.getSource().setImageURI(Uri.fromFile(new File(imagePath)));
                        ImageUploadActivity1.path = imagePath;
                        mProgressDialog.dismiss();
                        /*CommonMethods.setvalueAgainstKey(EditImageActivity.this, "imageIsAvaliable", "true");
                        finish();*/
                    }

                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        mProgressDialog.dismiss();
                        showSnackbar("Failed to save Image");
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
                mProgressDialog.dismiss();
                showSnackbar(e.getMessage());
            }
        }
    }

    private void saveImage() {
        if (requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // showLoading("Saving...");

            if (mProgressDialog == null) {
                mProgressDialog = createProgressDialog(EditImageActivity.this);
            }

            mProgressDialog.show();
            File file = new File(Environment.getExternalStorageDirectory().toString(),
                    System.currentTimeMillis() + ".png");
            try {
                file.createNewFile();
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                mPhotoEditor.saveAsFile(file.getAbsolutePath(), new PhotoEditor.OnSaveListener() {
                    @Override
                    public void onSuccess(@NonNull String imagePath) {
                        //  showSnackbar("Image Saved Successfully");
                        mPhotoEditorView.getSource().setImageURI(Uri.fromFile(new File(imagePath)));
                        ImageUploadActivity1.path = imagePath;
                        try {
                            if (mProgressDialog.isShowing()) {
                                mProgressDialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        CommonMethods.setvalueAgainstKey(EditImageActivity.this, "imageIsAvaliable", "true");
                        finish();
                    }

                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        if (mProgressDialog.isShowing()) {
                            mProgressDialog.dismiss();
                        }
                        showSnackbar("Failed to save Image");
                    }
                });
            } catch (IOException e) {
                //  e.printStackTrace();
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
                showSnackbar(e.getMessage());
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CAMERA_REQUEST:
                    mPhotoEditor.clearAllViews();
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    mPhotoEditorView.getSource().setImageBitmap(photo);
                    photo.recycle();
                    // photo = null;

                    break;
                case PICK_REQUEST:
                    mPhotoEditor.clearAllViews();
                    // Uri uri = data.getData();
                    mPhotoEditorView.getSource().setImageBitmap(bmpimg);

                    break;
                case Crop.REQUEST_CROP:
                    try {
                        /*bmpimg.recycle();
                        bmpimg = null;*/
                        Uri uri = Crop.getOutput(data);
                        mPhotoEditorView.getSource().setImageURI(uri);
                        imageUri = uri;
                        bmpimg = getBitmapFromReturnedImage(uri, BITMAP_WIDTH_HIGHT, BITMAP_WIDTH_HIGHT);
                        // mPhotoEditorView.getSource().setImageBitmap(bmpimg);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case FILTER_REQUEST:
                    Uri image = null;
                    try {
                       /* bmpimg.recycle();
                        bmpimg = null;*/
                        mPhotoEditor.clearAllViews();
                        String strEditText = data.getStringExtra("path");
                        image = Uri.parse(strEditText);
                        // Log.e("data", strEditText.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        bmpimg = getBitmapFromReturnedImage(image, BITMAP_WIDTH_HIGHT, BITMAP_WIDTH_HIGHT);
                        mPhotoEditorView.getSource().setImageBitmap(bmpimg);
                        imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context.getContentResolver(), bmpimg, "Title", null).toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    break;
            }
        } else {
            switch (requestCode) {
                case Crop.REQUEST_CROP:

                    try {
                        bmpimg = getBitmapFromReturnedImage(imageUri, BITMAP_WIDTH_HIGHT, BITMAP_WIDTH_HIGHT);
                        mPhotoEditorView.getSource().setImageBitmap(bmpimg);
                        // String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bmpimg, "Title", null);
                        imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context.getContentResolver(), bmpimg, "Title", null).toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;

                case FILTER_REQUEST:
                    try {
                        bmpimg = getBitmapFromReturnedImage(imageUri, BITMAP_WIDTH_HIGHT, BITMAP_WIDTH_HIGHT);
                        mPhotoEditorView.getSource().setImageBitmap(bmpimg);
                        imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context.getContentResolver(), bmpimg, "Title", null).toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    @Override
    public void onColorChanged(int colorCode) {
        mPhotoEditor.setBrushColor(colorCode);
        mTxtCurrentTool.setText(R.string.label_brush);
    }

    @Override
    public void onOpacityChanged(int opacity) {
        mPhotoEditor.setOpacity(opacity);
        mTxtCurrentTool.setText(R.string.label_brush);
    }

    @Override
    public void onBrushSizeChanged(int brushSize) {
        mPhotoEditor.setBrushSize(brushSize);
        mTxtCurrentTool.setText(R.string.label_brush);
    }

    @Override
    public void onEmojiClick(String emojiUnicode) {
        mPhotoEditor.addEmoji(emojiUnicode);
        mTxtCurrentTool.setText(R.string.label_emoji);

    }

    @Override
    public void onBrightnessChanged(int bright) {

        //changeBitmapContrastBrightness(bmpimg, (float) bright / 100f, 60);

        fbright = (float) bright;

        changeBitmapContrastBrightness(bmpimg, fcontrast, fbright);

    }

    @Override
    public void onContrastChanged(int contrast) {
        //changeBitmapContrastBrightness(bmpimg, 1, contrast);

        fcontrast = (float) contrast / 75f;

        if (contrast < 15) {
            fcontrast = (float) 15 / 75f;
        }

        changeBitmapContrastBrightness(bmpimg, fcontrast, fbright);
        //changeBitmapContrastBrightness(bmpimg, (float) contrast / 100f, 60);

        // createContrast(bmpimg, contrast);

    }

    /*@Override
    public void onStickerClick(Bitmap bitmap) {
        mPhotoEditor.addImage(bitmap);

        mTxtCurrentTool.setText(R.string.label_rotate);
    }*/

    @Override
    public void isPermissionGranted(boolean isGranted, String permission) {
        if (isGranted) {
            saveImage();
        }
    }

    public void changeBitmapContrastBrightness(Bitmap bmp, float contrast, float brightness) {

        if (mProgressDialog == null) {
            mProgressDialog = createProgressDialog(EditImageActivity.this);

        }

        mProgressDialog.show();

        new AsyncTask<Void, Void, Bitmap>() {

            @Override
            protected Bitmap doInBackground(Void... voids) {
                Bitmap ret = null;
                try {
                    ColorMatrix cm = new ColorMatrix(new float[]
                            {
                                    contrast, 0, 0, 0, brightness,
                                    0, contrast, 0, 0, brightness,
                                    0, 0, contrast, 0, brightness,
                                    0, 0, 0, 1, 0});

                    ret = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());
                    Canvas canvas = new Canvas(ret);
                    Paint paint = new Paint();
                    paint.setColorFilter(new ColorMatrixColorFilter(cm));
                    canvas.drawBitmap(bmp, 0, 0, paint);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.gc();
                } catch (OutOfMemoryError memoryError) {
                    System.gc();
                    try {
                        if (mProgressDialog.isShowing()) {
                            mProgressDialog.dismiss();
                        }
                        mPhotoEditorView.getSource().setImageBitmap(bmp);
                    } catch (Exception e) {
                        //  Log.e("ret ", "Bitmap " + e.toString());
                        if (mProgressDialog.isShowing()) {
                            mProgressDialog.dismiss();
                        }
                    }
                    return ret;
                }
                return ret;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                // ImageUploadActivity1.path = String.valueOf(getImageUri(context, bitmap));
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
                mPhotoEditorView.getSource().setImageBitmap(bitmap);
                // Log.e("created", "Creater=");
                try {
                    imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "Title", null).toString());
                } catch (Exception e) {
                    if (mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                    }
                    mPhotoEditorView.getSource().setImageBitmap(bmpimg);
                }
                super.onPostExecute(bitmap);

            }
        }.execute();

    }

    private void showSaveDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you want to exit without saving image ?");
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveImage();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setNeutralButton("Discard", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.create().show();

    }

    @Override
    public void onFilterSelected(PhotoFilter photoFilter) {
        mPhotoEditor.setFilterEffect(photoFilter);
    }

    @Override
    public void onToolSelected(ToolType toolType) {

        switch (toolType) {
            case PAINT:
                mPhotoEditor.setBrushDrawingMode(true);
                mTxtCurrentTool.setText(R.string.label_brush);
                mPropertiesBSFragment.show(getSupportFragmentManager(), mPropertiesBSFragment.getTag());
                break;
            case TEXT:
                TextEditorDialogFragment textEditorDialogFragment = TextEditorDialogFragment.show(this);
                textEditorDialogFragment.setOnTextEditorListener(new TextEditorDialogFragment.TextEditor() {
                    @Override
                    public void onDone(String inputText, int colorCode) {
                        mPhotoEditor.addText(inputText, colorCode);
                        mTxtCurrentTool.setText(R.string.label_text);
                    }

                });
                break;
            case ERASER:
                mPhotoEditor.brushEraser();
                mTxtCurrentTool.setText(R.string.label_eraser);
                break;
            case ADJUSTMENT:
                mTxtCurrentTool.setText(R.string.label_adjust);
                mPropertiescontrastFragment.show(getSupportFragmentManager(), mPropertiescontrastFragment.getTag());
                // showFilter(true);

                break;
            case CROP:
                // mEmojiBSFragment.show(getSupportFragmentManager(), mEmojiBSFragment.getTag());
                mTxtCurrentTool.setText(R.string.label_filter);
                saveEditedFile(Helper.createFilePath());

                break;
            case ROTATION:

                mTxtCurrentTool.setText(R.string.label_rotate);
                String path = "";
                path = getRealPathFromURI(imageUri);
                if (path == null) {
                    path = imageUri.getPath(); //from File Manager
                }
                mPhotoEditorView.getSource().setImageURI(imageUri);
                rotateImage(path);
                break;

            case FILTER:
                mTxtCurrentTool.setText("Filter");
                saveFilterFile(Helper.createFilePath());

                break;
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);

    }

    void showFilter(boolean isVisible) {
        mIsFilterVisible = isVisible;
        mConstraintSet.clone(mRootView);

        if (isVisible) {
            mConstraintSet.clear(mRvFilters.getId(), ConstraintSet.START);
            mConstraintSet.connect(mRvFilters.getId(), ConstraintSet.START,
                    ConstraintSet.PARENT_ID, ConstraintSet.START);
            mConstraintSet.connect(mRvFilters.getId(), ConstraintSet.END,
                    ConstraintSet.PARENT_ID, ConstraintSet.END);
        } else {
            mConstraintSet.connect(mRvFilters.getId(), ConstraintSet.START,
                    ConstraintSet.PARENT_ID, ConstraintSet.END);
            mConstraintSet.clear(mRvFilters.getId(), ConstraintSet.END);
        }

        ChangeBounds changeBounds = new ChangeBounds();
        changeBounds.setDuration(350);
        changeBounds.setInterpolator(new AnticipateOvershootInterpolator(1.0f));
        TransitionManager.beginDelayedTransition(mRootView, changeBounds);

        mConstraintSet.applyTo(mRootView);
    }

    @Override
    public void onBackPressed() {

        ImageUploadActivity1.imageIsAvaliable = false;
        if (mIsFilterVisible) {
            showFilter(false);
            mTxtCurrentTool.setText(R.string.app_name);
        } else if (!mPhotoEditor.isCacheEmpty()) {
            showSaveDialog();
        } else {
            super.onBackPressed();
        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    public void rotateImage(String path) {
        File file = new File(path);
        ExifInterface exifInterface = null;
        try {
            ExifInterface exifReader = new ExifInterface(path);
            exifInterface = new ExifInterface(file.getPath());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        if ((orientation == ExifInterface.ORIENTATION_NORMAL) | (orientation == 0)) {
            exifInterface.setAttribute(ExifInterface.TAG_ORIENTATION, "" + ExifInterface.ORIENTATION_ROTATE_90);
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
            exifInterface.setAttribute(ExifInterface.TAG_ORIENTATION, "" + ExifInterface.ORIENTATION_ROTATE_180);
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
            exifInterface.setAttribute(ExifInterface.TAG_ORIENTATION, "" + ExifInterface.ORIENTATION_ROTATE_270);
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
            exifInterface.setAttribute(ExifInterface.TAG_ORIENTATION, "" + ExifInterface.ORIENTATION_NORMAL);
        }
        try {
            exifInterface.saveAttributes();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //ImageUploadActivity1.image1=getBitmap(path);

        mPhotoEditorView.getSource().setImageBitmap(getBitmap(path));

    }

    public int orientations = 0;

    public static Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical) {
        Matrix matrix = new Matrix();
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public void flipImage(String path) {
        File file = new File(path);
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(file.getPath());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        // Log.e("orientation", "orientation=" + orientations);
        // Log.e("orientation", "wewe=" + ExifInterface.ORIENTATION_NORMAL);

        if (orientations == 0) {
            exifInterface.setAttribute(ExifInterface.TAG_ORIENTATION, "" + ExifInterface.ORIENTATION_FLIP_HORIZONTAL);
            orientations = 1;
        } else {
            exifInterface.setAttribute(ExifInterface.TAG_ORIENTATION, "" + ExifInterface.ORIENTATION_FLIP_VERTICAL);
            orientations = 0;
        }

        try {
            exifInterface.saveAttributes();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //ImageUploadActivity1.image1=getBitmap(path);

        mPhotoEditorView.getSource().setImageBitmap(getBitmap(path));

    }

    @SuppressLint("NewApi")
    private Bitmap getBitmap(String path) {
        //  Log.e("inside of", "getBitmap = " + path);
        try {
            // bmpimg = null;
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            Matrix matrix = new Matrix();
            ExifInterface exifReader = new ExifInterface(path);
            int orientation = exifReader.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);
            int rotate = 0;
            if (orientation == ExifInterface.ORIENTATION_NORMAL) {
                // Do nothing. The original image is fine.
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                rotate = 90;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                rotate = 180;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                rotate = 270;
            }
            matrix.postRotate(rotate);

            try {
                bmpimg = loadBitmap(path, rotate, BITMAP_WIDTH_HIGHT, BITMAP_WIDTH_HIGHT);

            } catch (OutOfMemoryError e) {
                System.gc();
            }
            System.gc();
            return bmpimg;
        } catch (Exception e) {
            //  Log.e("my tag", e.getMessage(), e);
            return null;
        }
    }

    public static Bitmap loadBitmap(String path, int orientation, final int targetWidth, final int targetHeight) {
        Bitmap bitmap = null;
        try {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, options);
            int sourceWidth, sourceHeight;
            if (orientation == 90 || orientation == 270) {
                sourceWidth = options.outHeight;
                sourceHeight = options.outWidth;
            } else {
                sourceWidth = options.outWidth;
                sourceHeight = options.outHeight;
            }
            if (sourceWidth > targetWidth || sourceHeight > targetHeight) {
                float widthRatio = (float) sourceWidth / (float) targetWidth;
                float heightRatio = (float) sourceHeight / (float) targetHeight;
                float maxRatio = Math.max(widthRatio, heightRatio);
                options.inJustDecodeBounds = false;
                options.inSampleSize = (int) maxRatio;
                bitmap = BitmapFactory.decodeFile(path, options);
            } else {
                bitmap = BitmapFactory.decodeFile(path);
            }
            if (orientation > 0) {
                Matrix matrix = new Matrix();
                matrix.postRotate(orientation);
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            }
            sourceWidth = bitmap.getWidth();
            sourceHeight = bitmap.getHeight();
            if (sourceWidth != targetWidth || sourceHeight != targetHeight) {
                float widthRatio = (float) sourceWidth / (float) targetWidth;
                float heightRatio = (float) sourceHeight / (float) targetHeight;
                float maxRatio = Math.max(widthRatio, heightRatio);
                sourceWidth = (int) ((float) sourceWidth / maxRatio);
                sourceHeight = (int) ((float) sourceHeight / maxRatio);
                bitmap = Bitmap.createScaledBitmap(bitmap, sourceWidth, sourceHeight, true);
            }
        } catch (Exception e) {
        }
        return bitmap;
    }

    @Override
    public void onLowMemory() {
        System.gc();
        super.onLowMemory();

    }

    public int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    public Bitmap getBitmapFromReturnedImage(Uri selectedImage, int reqWidth, int reqHeight) throws IOException {

        InputStream inputStream = null;
        BitmapFactory.Options options = null;
        try {
            inputStream = getContentResolver().openInputStream(selectedImage);

            // First decode with inJustDecodeBounds=true to check dimensions
            options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(inputStream, null, options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            /*// Calculate inSampleSize
            options.inSampleSize = 1;*/

            // close the input stream
            inputStream.close();

            // reopen the input stream
            inputStream = getContentResolver().openInputStream(selectedImage);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
        } catch (IOException e) {
            // e.printStackTrace();
            System.gc();
        } catch (OutOfMemoryError memoryError) {
            System.gc();

        }
        return BitmapFactory.decodeStream(inputStream, null, options);
    }

    public ProgressDialog createProgressDialog(Context mContext) {
        ProgressDialog dialog = new ProgressDialog(mContext, R.style.AppTheme_loading);
        /*try {
            if (SplashActivity.progress) {
                dialog.show();
            }
        } catch (WindowManager.BadTokenException e) {

        }*/
        dialog.show();
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.progressdialog);
        // dialog.setMessage(Message);
        return dialog;
    }


    public void createContrast(Bitmap src, double value) {

        new AsyncTask<Void, Void, Bitmap>() {

            @Override
            protected Bitmap doInBackground(Void... voids) {

                int width = src.getWidth();
                int height = src.getHeight();
                // create output bitmap
                Bitmap bmOut = Bitmap.createBitmap(width, height, src.getConfig());
                // color information
                int A, R, G, B;
                int pixel;
                // get contrast value
                double contrast = Math.pow((100 + value) / 100, 2);
                for (int x = 0; x < width; ++x) {
                    for (int y = 0; y < height; ++y) {
                        // get pixel color
                        pixel = src.getPixel(x, y);
                        A = Color.alpha(pixel);
                        // apply filter contrast for every channel R, G, B
                        R = Color.red(pixel);
                        R = (int) (((((R / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
                        if (R < 0) {
                            R = 0;
                        } else if (R > 255) {
                            R = 255;
                        }

                        G = Color.red(pixel);
                        G = (int) (((((G / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
                        if (G < 0) {
                            G = 0;
                        } else if (G > 255) {
                            G = 255;
                        }

                        B = Color.red(pixel);
                        B = (int) (((((B / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
                        if (B < 0) {
                            B = 0;
                        } else if (B > 255) {
                            B = 255;
                        }

                        // set new pixel color to output bitmap
                        bmOut.setPixel(x, y, Color.argb(A, R, G, B));
                    }
                }
                return bmOut;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                mPhotoEditorView.getSource().setImageBitmap(bitmap);


                super.onPostExecute(bitmap);


            }
        }.execute();


    }


    private Uri getImageUri(Context context, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @SuppressLint("MissingPermission")
    private void saveEditedFile(File file) {

        if (requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {


            if (mProgressDialog == null) {
                mProgressDialog = createProgressDialog(EditImageActivity.this);

            }

            mProgressDialog.show();
            try {
                file.createNewFile();
                mPhotoEditor.saveAsFile(file.getAbsolutePath(), new PhotoEditor.OnSaveListener() {
                    @Override
                    public void onSuccess(@NonNull String imagePath) {
                        // Log.e("saveEditedFile ", "Success");
                        imageUri = Uri.fromFile(new File(imagePath));
                        mPhotoEditorView.getSource().setImageURI(imageUri);
                        if (mProgressDialog.isShowing()) {
                            mProgressDialog.dismiss();
                        }
                        beginCrop(imageUri);

                    }

                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        if (mProgressDialog.isShowing()) {
                            mProgressDialog.dismiss();
                        }

                    }
                });
            } catch (IOException e) {
                // e.printStackTrace();
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
                showSnackbar(e.getMessage());
            }
        }
    }

    @SuppressLint("MissingPermission")
    private void saveFilterFile(File file) {
        if (requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            if (mProgressDialog == null) {
                mProgressDialog = createProgressDialog(EditImageActivity.this);

            }
            mProgressDialog.show();
            try {

                file.createNewFile();
                mPhotoEditor.saveAsFile(file.getAbsolutePath(), new PhotoEditor.OnSaveListener() {
                    @Override
                    public void onSuccess(@NonNull String imagePath) {
                        imageUri = Uri.fromFile(new File(imagePath));
                        mPhotoEditorView.getSource().setImageURI(imageUri);
                        try {
                            bmpimg = getBitmapFromReturnedImage(imageUri, BITMAP_WIDTH_HIGHT, BITMAP_WIDTH_HIGHT);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if (mProgressDialog.isShowing()) {
                            mProgressDialog.dismiss();
                        }
                        if (bmpimg != null) {
                            Intent i = new Intent(EditImageActivity.this, FilterActivity.class);
                            startActivityForResult(i, FILTER_REQUEST);
                            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        if (mProgressDialog.isShowing()) {
                            mProgressDialog.dismiss();
                        }
                    }
                });
            } catch (IOException e) {
                // e.printStackTrace();
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
                showSnackbar(e.getMessage());
            }
        }
    }
}


