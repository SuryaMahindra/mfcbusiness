package com.mfcwl.mfc_dealer.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mfcwl.mfc_dealer.Adapter.StockFeatureMarkAdapter;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Model.StockModel;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.mfcwl.mfc_dealer.Activity.StockStoreInfoActivity.yellow_star;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_DEALER_CRE;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_PROCUREMENT;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_SALES;

public class StockFeaturedCarActivity extends AppCompatActivity {


    public static RecyclerView mRecyclerStockFea;
    @BindView(R.id.toolbar)
    public Toolbar toolbar;
    public static Activity activity;

    @BindView(R.id.featurecarid)
    public ImageView featurecarid;
    @BindView(R.id.stock_fea_make)
    public TextView stock_fea_make;
    @BindView(R.id.stock_fea_model)
    public TextView stock_fea_model;
    @BindView(R.id.stock_fea_regno)
    public TextView stock_fea_regno;
    // @BindView(R.id.feature_mark) public  Button feature_mark;
    public static Button feature_mark;

    public static StockFeatureMarkAdapter mStockFeatureMarkAdapter;

    private static List<StockModel> stock = new ArrayList<>();

    public static String[] imageurl;
    public static String[] imagename;
    public static TextView featurelist;


    static String make, variantModel, variant, regno, selling_price, owner, Regyear, kms, registraionCity, certifiedNo, insurance, insuranceExp_date,
            sources, warranty, surveyor_remark, photo_count, reg_month, fuel_type, bought, refurbishment_cost, dealer_price, procurement_executive_id, cng_kit, chassis_number, engine_number, is_display, is_certified, is_featured_car, is_featured_car_admin, procurement_executive_name, private_vehicle, commentss, finance_required, manufacture_month, sales_executive_id, sales_executive_name, manufacture_year, actual_selling_price, CertifiedText, IsDisplay, TransmissionType, colour, dealer_code, is_offload, posted_date, stock_id, is_booked, coverimage;

    public static MenuItem item;
    public static Menu menus;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)


    String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_featured_car);
        Log.i(TAG, "onCreate: ");
        ButterKnife.bind(this);
        activity = this;
        feature_mark = (Button) findViewById(R.id.feature_mark);
        featurelist = (TextView) findViewById(R.id.featurelist);
        mRecyclerStockFea = (RecyclerView) findViewById(R.id.recycler_stock_fea);
        Log.i(TAG, "onCreate: ");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Drawable drawable = ResourcesCompat.getDrawable(this.getResources(), R.drawable.back, null);

        //custom color
        //drawable.setColorFilter(ResourcesCompat.getColor(this.getResources(), R.color.colorPrimaryLightText, null), PorterDuff.Mode.SRC_IN);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(drawable);

        imageurl = getIntent().getStringArrayExtra("imageurl");
        imagename = getIntent().getStringArrayExtra("imagename");

        make = getIntent().getStringExtra("make");
        variantModel = getIntent().getStringExtra("model");
        variant = getIntent().getStringExtra("variant");
        regno = getIntent().getStringExtra("registraionNo");
        selling_price = getIntent().getStringExtra("selling_price");
        owner = getIntent().getStringExtra("owner");
        Regyear = getIntent().getStringExtra("Regyear");
        kms = getIntent().getStringExtra("kms");
        registraionCity = getIntent().getStringExtra("registraionCity");
        certifiedNo = getIntent().getStringExtra("certifiedNo");
        insurance = getIntent().getStringExtra("insurance");
        insuranceExp_date = getIntent().getStringExtra("insuranceExp_date");
        sources = getIntent().getStringExtra("source");
        warranty = getIntent().getStringExtra("warranty");
        surveyor_remark = getIntent().getStringExtra("surveyor_remark");
        photo_count = getIntent().getStringExtra("photo_count");
        reg_month = getIntent().getStringExtra("reg_month");


        fuel_type = getIntent().getStringExtra("fuel_type");
        bought = getIntent().getStringExtra("bought");
        refurbishment_cost = getIntent().getStringExtra("refurbishment_cost");

        if (refurbishment_cost.equalsIgnoreCase("-1") || TextUtils.isEmpty(refurbishment_cost)) {
            refurbishment_cost = "0";
        }

        dealer_price = getIntent().getStringExtra("dealer_price");
        procurement_executive_id = getIntent().getStringExtra("ProcurementExecId");

        if (procurement_executive_id.equalsIgnoreCase("-1") || TextUtils.isEmpty(procurement_executive_id)) {
            procurement_executive_id = "0";
        }
        Log.e("refurbishment_cost-1", "refurbishment_cost=" + refurbishment_cost);
        Log.e("procurement_executive_id-1", "procurement_executive_id=" + procurement_executive_id);


        cng_kit = getIntent().getStringExtra("cng_kit");
        chassis_number = getIntent().getStringExtra("chassis_number");
        engine_number = getIntent().getStringExtra("engine_number");
        coverimage = getIntent().getStringExtra("coverimage");

        // is_display= getIntent().getStringExtra("is_display");
        is_certified = getIntent().getStringExtra("is_certified");
        is_featured_car = getIntent().getStringExtra("is_featured_car");

        Log.e("coverimage", "coverimage=" + coverimage);

        if (TextUtils.isEmpty(coverimage)) {
            coverimage = "";
        }

        if (!coverimage.equalsIgnoreCase("")) {

            Picasso.get()
                    .load(coverimage)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .placeholder(R.drawable.no_car_small)
                    .noFade()
                    .error(R.drawable.no_car_small)
                    .tag(this)
                    .into(featurecarid);
        }


        if (is_featured_car.equalsIgnoreCase("true")) {
            feature_mark.setText("Unmark as Featured");
            featurelist.setVisibility(View.INVISIBLE);

        } else {
            feature_mark.setText("Mark as Featured");
            featurelist.setVisibility(View.VISIBLE);

            WebServicesCall.webCall(this, this, jsonMake1(), "FeaturedCarlist", GlobalText.POST);

        }

        Log.i("testfeatured", is_featured_car);
        is_featured_car_admin = getIntent().getStringExtra("is_featured_car_admin");
        procurement_executive_name = getIntent().getStringExtra("procurement_executive_name");
        private_vehicle = getIntent().getStringExtra("private_vehicle");
        commentss = getIntent().getStringExtra("comments");//
        finance_required = getIntent().getStringExtra("finance_required");
        manufacture_month = getIntent().getStringExtra("manufacture_month");
        sales_executive_id = getIntent().getStringExtra("sales_executive_id");
        sales_executive_name = getIntent().getStringExtra("sales_executive_name");
        manufacture_year = getIntent().getStringExtra("manufacture_year");
        actual_selling_price = getIntent().getStringExtra("actual_selling_price");
        if (actual_selling_price == null || actual_selling_price.equalsIgnoreCase("")) {
            actual_selling_price = "0";
        }
        CertifiedText = getIntent().getStringExtra("CertifiedText");
        IsDisplay = getIntent().getStringExtra("IsDisplay");
        TransmissionType = getIntent().getStringExtra("TransmissionType");
        colour = getIntent().getStringExtra("colour");
        dealer_code = getIntent().getStringExtra("dealer_code");
        is_offload = getIntent().getStringExtra("is_offload");
        posted_date = getIntent().getStringExtra("posted_date");
        stock_id = getIntent().getStringExtra("stock_id");
        is_booked = getIntent().getStringExtra("is_booked");

        stock_fea_make.setText(make);
        stock_fea_model.setText(variantModel + " " + variant);
        stock_fea_regno.setText(regno);

        Log.e("test123", variantModel);


        Log.e("variantModel", "variantModel" + variantModel);
        Log.e("va", "variantModel" + variant);


        //status bar color change
        Window window = this.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.Black));


        //end of status bar color

        //call the post


        feature_mark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String usertype = CommonMethods.getstringvaluefromkey(activity, "user_type");
                if (usertype.equalsIgnoreCase(USER_TYPE_DEALER_CRE) ||
                        usertype.equalsIgnoreCase(USER_TYPE_SALES) ||
                        usertype.equalsIgnoreCase(USER_TYPE_PROCUREMENT)) {
                    Toast.makeText(activity, "You are not authorized ", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (is_featured_car.equalsIgnoreCase("true")) {
                    deleteFeaturedItem();

                } else {
                    if (stock.size() == 15) {
                        Toast.makeText(getApplicationContext(), "Only up to 15 cars can be marked as featured", Toast.LENGTH_LONG).show();
                    } else {
                        CustomDialogClass mCustomDialogClass = new CustomDialogClass(StockFeaturedCarActivity.this);
                        //mCustomDialogClass.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        mCustomDialogClass.show();
                    }

                }


            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        // put your code here...

        Application.getInstance().trackScreenView(activity,GlobalText.StockFeaturedCar_Activity);

    }

    public static JSONObject jsonMake1() {
        JSONObject jObj = new JSONObject();

        JSONObject jObj5 = new JSONObject();
        JSONArray jsonArray = new JSONArray();


        try {
            jObj.put("Page", "1");
            jObj.put("PageItems", "15");


            jObj5.put("column", "is_featured_car");
            jObj5.put("operator", "=");
            jObj5.put("value", "true");
            jsonArray.put(jObj5);


            jObj.put("where", jsonArray);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("jsonMake1 ", "jsonMake1" + jObj.toString());

        return jObj;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item, menu);
        MenuItem item = menu.findItem(R.id.share);
        item.setVisible(false);
        item = menu.findItem(R.id.add_image);
        item.setVisible(false);
        item = menu.findItem(R.id.delete_fea);
        item.setVisible(false);
        item = menu.findItem(R.id.stock_fil_clear);
        item.setVisible(false);
        item = menu.findItem(R.id.notification_cancel);
        item.setVisible(false);
        item = menu.findItem(R.id.notification_bell);
        item.setVisible(false);
        item = menu.findItem(R.id.asmHome);
        item.setVisible(false);
        item = menu.findItem(R.id.asm_mail);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                // return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public class CustomDialogClass extends Dialog implements View.OnClickListener {

        public Activity activity;
        public Dialog d;
        public Button yes, no;

        public CustomDialogClass(@NonNull Activity context) {
            super(context);
            this.activity = context;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_yes:

                    dismiss();
                    break;
                case R.id.btn_no:
                    // dismiss();
                    //activity.finish();
                    addFeaturedItem();

                    Log.i("ree", "xccxdsdfsfdfds");
                    break;
                default:
                    break;
            }
            dismiss();
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.stock_fea_cus_dialog);
            Log.i(TAG, "onCreate: ");
            yes = findViewById(R.id.btn_yes);
            no = findViewById(R.id.btn_no);
            yes.setOnClickListener(this);
            no.setOnClickListener(this);
        }
    }
    /*@OnClick(R.id.feature_mark)
    public void feature_popup(){
        CustomDialogClass mCustomDialogClass = new CustomDialogClass(StockFeaturedCarActivity.this);
        //mCustomDialogClass.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mCustomDialogClass.show();
    }*/

    public static void deleteFeaturedItem() {

        WebServicesCall.webCall(activity, activity, deleteFeaturedMakeBookNow(), "deleteFeatureCar", GlobalText.PUT);

    }

    public void addFeaturedItem() {
        is_featured_car = "true";
        // Toast.makeText(getApplicationContext(),is_featured_car,Toast.LENGTH_LONG).show();
    /*    StockModel stockModel = new StockModel(make,variantModel,regno);
        List<StockModel> addList = new ArrayList<>();
        addList.add(stockModel);*/

        WebServicesCall.webCall(this, this, jsonFeaturedMakeBookNow(), "FeatureCar", GlobalText.PUT);

      /*  mStockFeatureMarkAdapter = new StockFeatureMarkAdapter(addList,this);
        mRecyclerStockFea.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerStockFea.setLayoutManager(mLayoutManager);
        mRecyclerStockFea.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mRecyclerStockFea.setItemAnimator(new DefaultItemAnimator());
        mRecyclerStockFea.setAdapter(mStockFeatureMarkAdapter);*/

    }

    public static String MethodofMonth_Num(String month) {

        if (month.equalsIgnoreCase("January") || month.equalsIgnoreCase("Jan")) {
            month = "1";
            return month;
        }


        if (month.equalsIgnoreCase("February") || month.equalsIgnoreCase("Feb")) {
            month = "2";
            return month;

        }

        if (month.equalsIgnoreCase("March") || month.equalsIgnoreCase("Mar")) {
            month = "3";
            return month;

        }

        if (month.equalsIgnoreCase("April") || month.equalsIgnoreCase("Apr")) {
            month = "4";
            return month;

        }


        if (month.equalsIgnoreCase("May") || month.equalsIgnoreCase("May")) {
            month = "5";
            return month;

        }

        if (month.equalsIgnoreCase("June") || month.equalsIgnoreCase("Jun")) {
            month = "6";
            return month;

        }

        if (month.equalsIgnoreCase("July") || month.equalsIgnoreCase("Jul")) {
            month = "7";
            return month;

        }

        if (month.equalsIgnoreCase("August") || month.equalsIgnoreCase("Aug")) {
            month = "8";
            return month;

        }


        if (month.equalsIgnoreCase("September") || month.equalsIgnoreCase("Sep")) {
            month = "9";
            return month;

        }

        if (month.equalsIgnoreCase("October") || month.equalsIgnoreCase("Oct")) {
            month = "10";
            return month;

        }

        if (month.equalsIgnoreCase("November") || month.equalsIgnoreCase("Nov")) {
            month = "11";
            return month;

        }

        if (month.equalsIgnoreCase("December") || month.equalsIgnoreCase("Dec")) {
            month = "12";
            return month;

        }

        return month;

    }

    public JSONObject jsonFeaturedMakeBookNow() {
        JSONObject jObj = new JSONObject();

        try {
            jObj.put("stock_id", stock_id);
            jObj.put("stock_source", sources);
            jObj.put("posted_date", posted_date);
            jObj.put("vehicle_make", make);
            jObj.put("vehicle_model", variantModel);
            jObj.put("vehicle_variant", variant);
            jObj.put("reg_month", MethodofMonth_Num(reg_month));
            jObj.put("reg_year", Regyear);
            jObj.put("registraion_city", registraionCity);
            jObj.put("registration_number", regno);

            jObj.put("colour", colour);
            jObj.put("kilometer", kms);
            jObj.put("owner", owner);
            jObj.put("insurance", insurance);


            if (insurance.equalsIgnoreCase("")) {
                insurance = "NA";
            }

            Log.e("insurance=", "insurance=" + insurance);
            Log.e("insuranceExp_date=", "insuranceExp_date=" + insuranceExp_date);

            if (insurance.equalsIgnoreCase("NA")) {
                jObj.put("insurance_exp_date", "");
            } else {
                jObj.put("insurance_exp_date", insuranceExp_date);
            }

            jObj.put("selling_price", selling_price);
            jObj.put("is_display", is_display);
            jObj.put("bought_price", bought);

            Log.e("refurbishment_cost", "refurbishment_cost=" + refurbishment_cost);
            Log.e("procurement_executive_id", "procurement_executive_id=" + procurement_executive_id);

            try {
                jObj.put("refurbishment_cost", Integer.parseInt(refurbishment_cost));
            } catch (NumberFormatException e) {
                jObj.put("refurbishment_cost", "0");
            }

            try {
                jObj.put("procurement_executive_id", Integer.parseInt(procurement_executive_id));
            } catch (NumberFormatException e) {
                jObj.put("procurement_executive_id", "0");
            }

            jObj.put("cng_kit", cng_kit);

            if (private_vehicle.equalsIgnoreCase("1") || private_vehicle.equalsIgnoreCase("true")) {
                jObj.put("private_vehicle", "true");

            } else {
                jObj.put("private_vehicle", "false");

            }

            jObj.put("comments", commentss);
            jObj.put("manufacture_month", MethodofMonth_Num(manufacture_month));
            jObj.put("is_offload", is_offload);
            jObj.put("manufacture_year", manufacture_year);
            jObj.put("updated_by_device", "MOBILE");
            jObj.put("is_featured_car", is_featured_car);
            jObj.put("is_booked", is_booked);
            jObj.put("actual_selling_price", actual_selling_price);
            jObj.put("sales_executive_id", sales_executive_id);
            jObj.put("finance_required", finance_required);

            jObj.put("chassis_number", chassis_number);
            jObj.put("engine_number", engine_number);
            jObj.put("dealer_price", dealer_price);
            jObj.put("dealer_code", dealer_code);
            // jObj.put("is_display","1");


        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("respose fe", e.getMessage());


        }
        return jObj;

    }


    public static JSONObject deleteFeaturedMakeBookNow() {
        JSONObject jObj = new JSONObject();

        try {

            jObj.put("stock_id", stock_id);
            jObj.put("stock_source", sources);
            jObj.put("posted_date", posted_date);
            jObj.put("vehicle_make", make);
            jObj.put("vehicle_model", variantModel);
            jObj.put("vehicle_variant", variant);
            jObj.put("reg_month", MethodofMonth_Num(reg_month));
            jObj.put("reg_year", Regyear);
            jObj.put("registraion_city", registraionCity);
            jObj.put("registration_number", regno);

            jObj.put("colour", colour);
            jObj.put("kilometer", kms);
            jObj.put("owner", owner);
            jObj.put("insurance", insurance);

            if (insurance.equalsIgnoreCase("NA")) {
                jObj.put("insurance_exp_date", "");
            } else {
                jObj.put("insurance_exp_date", insuranceExp_date);
            }
            jObj.put("selling_price", selling_price);
            jObj.put("is_display", is_display);
            jObj.put("bought_price", bought);
            /*jObj.put("refurbishment_cost", Integer.parseInt(refurbishment_cost));
            jObj.put("procurement_executive_id", Integer.parseInt(procurement_executive_id));
*/
            try {
                jObj.put("refurbishment_cost", Integer.parseInt(refurbishment_cost));
            } catch (NumberFormatException e) {
                jObj.put("refurbishment_cost", "0");
            }

            try {
                jObj.put("procurement_executive_id", Integer.parseInt(procurement_executive_id));
            } catch (NumberFormatException e) {
                jObj.put("procurement_executive_id", "0");
            }

            jObj.put("cng_kit", cng_kit);
            jObj.put("private_vehicle", private_vehicle);
            jObj.put("comments", commentss);
            jObj.put("manufacture_month", MethodofMonth_Num(manufacture_month));
            jObj.put("is_offload", is_offload);
            jObj.put("manufacture_year", manufacture_year);
            jObj.put("updated_by_device", "MOBILE");
            jObj.put("is_featured_car", "false");
            jObj.put("is_booked", is_booked);
            jObj.put("actual_selling_price", actual_selling_price);
            jObj.put("sales_executive_id", sales_executive_id);
            jObj.put("finance_required", finance_required);

            jObj.put("chassis_number", chassis_number);
            jObj.put("engine_number", engine_number);
            jObj.put("dealer_price", dealer_price);
            jObj.put("dealer_code", dealer_code);


        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("respose fe", e.getMessage());
        }
        return jObj;

    }


    public static void Parsebooknow(JSONObject jObj, String strMethod) {
        try {

            // CommonMethods.alertMessage(a,"");

            Log.e("fearted data res", jObj.toString());
            if (jObj.getString("status").equals("SUCCESS")) {

                Log.e("SUCCESS -->> ", "SUCCESS");
                Toast.makeText(activity, "Successfully marked", Toast.LENGTH_SHORT).show();
                yellow_star();
                activity.finish();

            } else {
                Log.e("SUCCESS -->> ", "SUCCESS-2");
                Toast.makeText(activity, jObj.getString("Message"), Toast.LENGTH_SHORT).show();
                CommonMethods.getstringvaluefromkey(activity, jObj.getString("Message"));
                activity.finish();
            }


          /*  Intent in_main = new Intent(activity, StockStoreInfoActivity.class);
            activity.startActivity(in_main);
*/


        } catch (Exception ex) {
            Log.e("Parsestockstore-Ex", ex.toString());
        }

    }

    public static void reloadData() {

        Log.i("test test", stock.toString());
        mStockFeatureMarkAdapter = new StockFeatureMarkAdapter(stock, activity);
        mRecyclerStockFea.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(activity);
        mRecyclerStockFea.setLayoutManager(mLayoutManager);
        // mRecyclerStockFea.addItemDecoration(new DividerItemDecoration(activity, LinearLayoutManager.VERTICAL));
        mRecyclerStockFea.setItemAnimator(new DefaultItemAnimator());
        mRecyclerStockFea.setAdapter(mStockFeatureMarkAdapter);


    }

    public static void Parsestockstore(JSONObject jObj, String strMethod) {
        Log.i("test pars", jObj.toString());
        try {


            stock = new ArrayList<>();


            JSONArray arrayList = jObj.getJSONArray("data");
            for (int i = 0; i < arrayList.length(); i++) {
                JSONObject jsonobject = new JSONObject(
                        arrayList.getString(i));

                StockModel stockModel = new StockModel();

                stockModel.setSource(jsonobject.getString("stock_source"));
                stockModel.setId(jsonobject.getInt("stock_id"));
                stockModel.setFeaturedCarText(/*jsonobject.getString("featured_car_text")*/"");
                stockModel.setPostedDate(jsonobject.getString("posted_date"));
                // stockModel.setStockAge(jsonobject.getInt("stock_age"));//
                stockModel.setMake(jsonobject.getString("vehicle_make"));
                stockModel.setModel(jsonobject.getString("vehicle_model"));
                stockModel.setVariant(jsonobject.getString("vehicle_variant"));
                stockModel.setRegMonth(jsonobject.getString("reg_month"));
                stockModel.setRegYear(jsonobject.getInt("reg_year"));


                stockModel.setRegistraionCity(jsonobject.getString("registraion_city"));
                stockModel.setRegistrationNumber(jsonobject.getString("registration_number"));
                stockModel.setColour(jsonobject.getString("colour"));
                stockModel.setKilometer(jsonobject.getInt("kilometer"));
                stockModel.setOwner(jsonobject.getInt("owner"));
                stockModel.setInsurance(jsonobject.getString("insurance"));
                stockModel.setInsuranceExpDate(jsonobject.getString("insurance_exp_date"));
                stockModel.setSellingPrice(jsonobject.getInt("selling_price"));
                stockModel.setCertifiedText(jsonobject.getString("CertifiedText"));
                stockModel.setCertificationNumber(jsonobject.getString("certification_number"));
                stockModel.setWarranty(jsonobject.getString("warranty_recommended"));
                stockModel.setPhotoCount(1);
                stockModel.setSurveyorCode(jsonobject.getString("surveyor_code"));
                stockModel.setSurveyorModifiedDate(jsonobject.getString("surveyor_modified_date"));
                stockModel.setSurveyorKilometer(jsonobject.getString("surveyor_kilometer"));
                stockModel.setSurveyorRemark(jsonobject.getString("surveyor_remark"));
                stockModel.setDealerCode(jsonobject.getString("dealer_code"));
                stockModel.setIsbooked(jsonobject.getString("is_booked"));
                stockModel.setIssold(/*jsonobject.getString("is_sold")*/"");
                stockModel.setIsDisplay(/*jsonobject.getString("is_display")*/"");
                stockModel.setIsOffload(jsonobject.getString("is_offload"));
                stockModel.setFuel_type(jsonobject.getString("fuel_type"));
                stockModel.setBought(jsonobject.getString("bought_price"));
                stockModel.setRefurbishment_cost(jsonobject.getString("refurbishment_cost"));
                stockModel.setDealer_price(jsonobject.getString("dealer_price"));
                stockModel.setProcurementExecId(jsonobject.getString("procurement_executive_id"));
                stockModel.setCng_kit(jsonobject.getString("cng_kit"));
                stockModel.setChassis_number(jsonobject.getString("chassis_number"));
                stockModel.setEngine_number(jsonobject.getString("engine_number"));

                stockModel.setIs_certified(jsonobject.getString("is_certified"));
                stockModel.setIs_featured_car(jsonobject.getString("is_featured_car"));
                stockModel.setIs_featured_car_admin(""/*jsonobject.getString("is_featured_car_admin")*/);
                stockModel.setProcurement_executive_name(jsonobject.getString("procurement_executive_name"));
                stockModel.setComments(jsonobject.getString("comments"));
                stockModel.setFinance_required(jsonobject.getString("finance_required"));
                stockModel.setManufacture_month(jsonobject.getString("manufacture_month"));
                stockModel.setSales_executive_id(jsonobject.getString("sales_executive_id"));
                stockModel.setSales_executive_name(jsonobject.getString("sales_executive_name"));
                stockModel.setManufacture_year(jsonobject.getString("manufacture_year"));
                stockModel.setActual_selling_price(jsonobject.getString("actual_selling_price"));
                stockModel.setCertifiedText(jsonobject.getString("CertifiedText"));
                stockModel.setIsDisplay(/*jsonobject.getString("IsDisplay")*/"");
                stockModel.setTransmissionType(/*jsonobject.getString("TransmissionType")*/"");
                stockModel.setPrivate_vehicle(jsonobject.getString("private_vehicle"));

                stock.add(stockModel);

            }


        } catch (Exception ex) {
            Log.e("Parsestockstore-Ex", ex.toString());
        }

        reloadData();
    }
}
