package com.mfcwl.mfc_dealer.Activity.RDR.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RDRActionItemResponse {

    @SerializedName("current_page")
    @Expose
    public Integer currentPage;
    @SerializedName("per_page")
    @Expose
    public Integer perPage;
    @SerializedName("total")
    @Expose
    public Integer total;
    @SerializedName("data")
    @Expose
    public List<RDRActionItemData> data = null;

}
