package com.mfcwl.mfc_dealer.Activity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class FullscreenVideoActivity extends AppCompatActivity {

    public static VideoView video_view;
    public TextView play_button;
    public String video_url = "",stock_id="",regno="";
    public String TAG=getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate: ");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_fullscreen_video);

        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        | View.SYSTEM_UI_FLAG_LOW_PROFILE
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
        decorView.setOnSystemUiVisibilityChangeListener(null);
        decorView.setOnDragListener(null);


        try {
            stock_id = getIntent().getStringExtra("stock_id");
            video_url = getIntent().getStringExtra("video_url");
            regno = getIntent().getStringExtra("regno");


        } catch (Exception e) {
            e.printStackTrace();
        }

        video_view = (VideoView) findViewById(R.id.video_view);
        play_button = (TextView) findViewById(R.id.play_button);

       /* Animation myFadeInAnimation = AnimationUtils.loadAnimation(FullscreenVideoActivity.this, R.anim.fade_in);
        Animation myFadeOutAnimation = AnimationUtils.loadAnimation(FullscreenVideoActivity.this, R.anim.fade_out);
        play_button.startAnimation(myFadeInAnimation);
        play_button.startAnimation(myFadeOutAnimation);*/

        play_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (video_url != null && !video_url.isEmpty() && !video_url.equalsIgnoreCase("") && !video_url.equalsIgnoreCase("null")) {

                    if (video_view.isPlaying()) {

                    } else {
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                play_button.setText("Loading...");
                                Log.i(TAG, "onClick: ="+video_url);

                                resetVideoPlayer();

                                MediaController mediaController = new MediaController(FullscreenVideoActivity.this);
                                mediaController.setAnchorView(video_view);
                                mediaController.setMediaPlayer(video_view);

                                video_view.setMediaController(mediaController);
                                Uri video = Uri.parse(video_url);
                                video_view.setVideoURI(video);
                                video_view.requestFocus();
                                play_button.setBackground(null);

                            }
                        }, 0);



                        video_view.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {

                                video_view.start();

                                play_button.setText("");
                            }
                        });



                    }

                }
            }
        });

        video_view.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                play_button.setText("");
                play_button.setBackgroundResource(R.drawable.ic_play);
                play_button.setVisibility(View.VISIBLE);
                play_button.clearAnimation();
            }
        });

        play_button.performClick();

    }

    public void resetVideoPlayer(){

        CommonMethods.deleteCache(this);
        CommonMethods.MemoryClears();

        if (video_view != null) {
            video_view.seekTo(0);
            video_view.stopPlayback();
        }
        video_view.setMediaController(null);

    }

    @Override
    public void onBackPressed() {


        Intent intent = new Intent(FullscreenVideoActivity.this, VideoActivity.class);
        intent.putExtra("video_url", video_url);
        intent.putExtra("stock_id", stock_id);
        intent.putExtra("regno", regno);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();


    }

}
