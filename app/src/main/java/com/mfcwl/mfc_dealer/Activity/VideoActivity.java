package com.mfcwl.mfc_dealer.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.mfcwl.mfc_dealer.ASMModel.videoUrlRes;
import com.mfcwl.mfc_dealer.Fragment.StockFragment;
import com.mfcwl.mfc_dealer.Model.videoUrlReq;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.encrypt.AWSSecretsManager;
import com.mfcwl.mfc_dealer.encrypt.s3Res;
import com.mfcwl.mfc_dealer.retrofitconfig.S3DetailsServices;
import com.mfcwl.mfc_dealer.retrofitconfig.VideoUrlServices;
import com.mfcwl.mfc_dealer.videoAppSpecific.AISQLLiteAdapter;
import com.mfcwl.mfc_dealer.videoAppSpecific.DatabaseExportHelper;
import com.mfcwl.mfc_dealer.videoAppSpecific.SqlAdapterForDML;
import com.mfcwl.mfc_dealer.videoAppSpecific.VideoCaptureActivityForAutoInspect;
import com.mfcwl.mfc_dealer.videoAppSpecific.VideoUploadManager;

import java.io.File;

import retrofit2.Response;
import videoCapture.VideoCaptureConstants;

public class VideoActivity extends AppCompatActivity{

    public String TAG = getClass().getSimpleName();
    public LinearLayout select_camera, video_edit;
    public VideoView video_view;
    public TextView play_button, edit, delete, database,full;
    public String dealer_code = "", localpath = "";
    public ImageView back;
    public static Activity act;
    public String path = "";
    public static String videoData = "";
    String stock_id, video_url = "",regno="";
    public boolean uploading = false;
    public  String btnClick="";
    public Context context;
    public int Count=6;
    Handler handler;
    private Runnable myRunnable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        Log.i(TAG, "onCreate: ");
        act = this;
        new VideoUploadManager().uploadAllPendingMedia();
        uploading = false;

        handler= new Handler();

        if (CommonMethods.isInternetWorking(VideoActivity.this)) {
            sendS3Details(this);
        } else {
            CommonMethods.alertMessage(VideoActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
        }

        select_camera = (LinearLayout) findViewById(R.id.select_camera);
        video_edit = (LinearLayout) findViewById(R.id.video_edit);
        video_view = (VideoView) findViewById(R.id.video_view);
        edit = (TextView) findViewById(R.id.edit);
        delete = (TextView) findViewById(R.id.delete);
        database = (TextView) findViewById(R.id.database);
        full = (TextView) findViewById(R.id.full);
        play_button = (TextView) findViewById(R.id.play_button);
        back = (ImageView) findViewById(R.id.back);


        try {

            stock_id = getIntent().getStringExtra("stock_id");
            video_url = getIntent().getStringExtra("video_url");
            regno = getIntent().getStringExtra("regno");

            Log.i(TAG, "onCreate: -stock_id-="+stock_id);
            Log.i(TAG, "onCreate: -already-url="+video_url);
            Log.i(TAG, "onCreate: -regno-="+regno);

            if(video_url.contains("http")){
                path = video_url;
                uploading = false;
                autoFillVideos();
            }else{
                videoUploadStatusCheck();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        CommonMethods.setvalueAgainstKey(this, "videopath", "");
        localpath = CommonMethods.getstringvaluefromkey(VideoActivity.this, "videopath");
        dealer_code = CommonMethods.getstringvaluefromkey(VideoActivity.this, "dealer_code");


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    handler.removeCallbacks(myRunnable);
                }catch (Exception e){
                    e.printStackTrace();
                }

                if(CommonMethods.getstringvaluefromkey(VideoActivity.this, "fromImage").equalsIgnoreCase("yes")) {
                    finish();
                }else{
                    StockFragment.onBack = true;
                    Intent intent = new Intent(VideoActivity.this, StockStoreInfoActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
            }
        });

        database.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new DatabaseExportHelper().exportDatabase(VideoActivity.this, AISQLLiteAdapter.DATABASE_NAME);

            }
        });
        full.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(VideoActivity.this, FullscreenVideoActivity.class);
                intent.putExtra("video_url", path);
                intent.putExtra("stock_id", stock_id);
                intent.putExtra("regno", regno);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            }
        });
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnClick="edit";

                if (!uploading) {
                    if (video_view.isPlaying()) {
                        video_view.stopPlayback();
                    }


                    Intent intent = new Intent(VideoActivity.this, VideoCaptureActivityForAutoInspect.class);
                    intent.putExtra("lead", stock_id);
                    intent.putExtra("dealer_code", dealer_code);
                    intent.putExtra("regno", regno);
                    intent.putExtra("dealer_name", CommonMethods.getstringvaluefromkey(VideoActivity.this, "dealer_display_name"));
                    startActivityForResult(intent, VideoCaptureConstants.VIDEO_ACTIVITY_REQUEST_CODE);
                }
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnClick="";

                DeletetAert();
            }
        });

        play_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnClick="";
                if (path != null && !path.isEmpty() && !path.equalsIgnoreCase("") && !path.equalsIgnoreCase("null")) {

                    if (video_view.isPlaying()) {

                    } else {
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                play_button.setText("Loading...");
                                Log.i(TAG, "onClick: ="+path);

                                resetVideoPlayer();

                                MediaController mediaController = new MediaController(VideoActivity.this);
                                mediaController.setAnchorView(video_view);
                                mediaController.setMediaPlayer(video_view);

                                video_view.setMediaController(mediaController);
                                Uri video = Uri.parse(path);
                                video_view.setVideoURI(video);

                                video_view.requestFocus();
                                play_button.setBackground(null);
                            }
                        }, 0);



                        video_view.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {

                                video_view.start();
                                play_button.setText("");
                            }
                        });



                    }

                } else {
                    if (!uploading) {
                        if (video_view.isPlaying()) {
                            video_view.stopPlayback();
                        }
                        Intent intent = new Intent(VideoActivity.this, VideoCaptureActivityForAutoInspect.class);
                        intent.putExtra("lead", stock_id);
                        intent.putExtra("dealer_code", dealer_code);
                        intent.putExtra("regno", regno);
                        intent.putExtra("dealer_name", CommonMethods.getstringvaluefromkey(VideoActivity.this, "dealer_display_name"));

                        startActivityForResult(intent, VideoCaptureConstants.VIDEO_ACTIVITY_REQUEST_CODE);
                    }
                }
            }
        });

        video_view.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {

                if (video_view != null) {
                    video_view.seekTo(0);
                    video_view.stopPlayback();
                }

                play_button.setText("");
                play_button.setBackgroundResource(R.drawable.ic_play);
                play_button.setVisibility(View.VISIBLE);
            }
        });

        video_view.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.d("video", "setOnErrorListener ");
                return true;
            }
        });

    }

    public void resetVideoPlayer(){

        CommonMethods.deleteCache(this);
        CommonMethods.MemoryClears();

        if (video_view != null) {
           video_view.seekTo(0);
            video_view.stopPlayback();
        }
        video_view.setMediaController(null);

    }
    private void videoUploadStatusCheck() {


        final AISQLLiteAdapter dbAdapter = AISQLLiteAdapter.getInstance();
         String isVideoUploaded = dbAdapter.isVideoUploaded(stock_id);
        Log.i(TAG, "isVideoUploaded: " + isVideoUploaded);

        if(isVideoUploaded.equalsIgnoreCase("")){
            String localPath = dbAdapter.isVideoLocalPath(stock_id);
            if(!localPath.isEmpty())isVideoUploaded="0";
        }

        switch (isVideoUploaded){

            case "0":

                path = "";
                play_button.setBackground(null);
                play_button.setText("Uploading...");
                video_edit.setVisibility(View.GONE);
                new VideoUploadManager().uploadAllPendingMedia();

                break;

            case "1":

                path = "";
                play_button.setBackground(null);
                play_button.setText("Uploading...");
                video_edit.setVisibility(View.GONE);

                String links = dbAdapter.isVideoGetlink(stock_id);

                if (links.contains("http")) {
                    path = links;
                    uploading = false;
                    autoFillVideos();
                }

                break;

            case "":
                play_button.setText("");
                play_button.setBackgroundResource(R.drawable.ic_recording);
                play_button.setVisibility(View.VISIBLE);
                video_edit.setVisibility(View.GONE);

                break;

        }

    }


    @Override
    protected void onResume() {

        videoUpload();

        if(!path.contains("http") ||btnClick.equalsIgnoreCase("edit")) {
            uploadCheck();
        }

        if(btnClick.equalsIgnoreCase("edit")){

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    resetVideoPlayer();
                    autoFillVideos();
                }
            }, 0);

        }



        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if ( resultCode  == RESULT_OK) {

                String requiredValue = data.getStringExtra("lead");
                Log.i(TAG, "onActivityResult: "+requiredValue);
            }else{
                Log.i(TAG, "onActivityResult: "+"cancel video");
                reload();
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.toString(),Toast.LENGTH_SHORT).show();
        }

    }

    public void reload() {
        Intent intent = getIntent();
        finish();
        intent.putExtra("video_url", video_url);
        intent.putExtra("stock_id", stock_id);
        intent.putExtra("regno", regno);
        startActivity(intent);
    }

    private void uploadCheck() {

        Count=Count+1;
        Log.i(TAG, "path: " + path);

         myRunnable = new Runnable() {
            public void run() {
                // Things to be done
                final AISQLLiteAdapter dbAdapter = AISQLLiteAdapter.getInstance();
                final String isVideoUploaded = dbAdapter.isVideoUploaded(stock_id);
                Log.i(TAG, "isVideoUploaded: " + isVideoUploaded);
                Log.i(TAG, "stock_id: " + stock_id);
                if (isVideoUploaded.equals("1")) {

                    String links = dbAdapter.isVideoGetlink(stock_id);

                    if (links.contains("http")) {
                        path = links;
                        uploading = false;
                        autoFillVideos();
                    }
                    Log.i(TAG, "run: " + links);
                } else {
                    Log.i(TAG, "path:-2 " + path);

                    if(Count==7){
                        new VideoUploadManager().uploadAllPendingMedia();
                        Count=0;
                        Log.i(TAG, "Count=0=: "+Count);
                    }

                    Log.i(TAG, "Count==: "+Count);

                    uploadCheck();
                }
            }
        };

        handler.postDelayed(myRunnable, 5000);

/*        handler.postDelayed(new Runnable() {
            public void run() {

                final AISQLLiteAdapter dbAdapter = AISQLLiteAdapter.getInstance();
                final String isVideoUploaded = dbAdapter.isVideoUploaded(stock_id);
                Log.i(TAG, "isVideoUploaded: " + isVideoUploaded);
                Log.i(TAG, "stock_id: " + stock_id);
                if (isVideoUploaded.equals("1")) {

                    String links = dbAdapter.isVideoGetlink(stock_id);

                    if (links.contains("http")) {
                        path = links;
                        uploading = false;
                        autoFillVideos();
                    }
                    Log.i(TAG, "run: " + links);
                } else {
                    Log.i(TAG, "path:-2 " + path);

                    if(Count==5){
                        new VideoUploadManager().uploadAllPendingMedia();
                        Count=0;
                        Log.i(TAG, "Count=0=: "+Count);
                    }

                    Log.i(TAG, "Count==: "+Count);

                    uploadCheck();
                }
            }
        }, 5000);*/

    }


    private void videoUpload() {

        localpath = CommonMethods.getstringvaluefromkey(VideoActivity.this, "videopath");

        if (!localpath.isEmpty() && !videoData.isEmpty()) {

            if (null != video_view) {
                video_view.stopPlayback();
            }

            path = "";
            CommonMethods.setvalueAgainstKey(this, "videopath", "");
            play_button.setBackground(null);
            play_button.setText("Uploading...");

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    uploading = true;
                    play_button.setBackground(null);
                    play_button.setText("Uploading...");
                }
            }, 3000);

        }
    }

    private void autoFillVideos() {

        if (!path.isEmpty() && !path.equalsIgnoreCase("") && !path.equalsIgnoreCase("null")) {
            play_button.setBackgroundResource(R.drawable.ic_play);
            play_button.setVisibility(View.VISIBLE);
            video_edit.setVisibility(View.VISIBLE);

            play_button.setText("");

        } else {
            play_button.setBackgroundResource(R.drawable.ic_recording);
            play_button.setText("");
            video_edit.setVisibility(View.GONE);
        }

    }

    public void sendVideoUrl(final Context mContext, final videoUrlReq req, String status) {

        videoData = "";
        SpinnerManager.showSpinner(mContext);

        VideoUrlServices.sendVideoUrl(req, mContext, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                try {
                    Response<videoUrlRes> mRes = (Response<videoUrlRes>) obj;

                    videoUrlRes mData = mRes.body();

                    if (status.equalsIgnoreCase("update")) {

                        SpinnerManager.hideSpinner(mContext);

                    } else {

                        uploading = false;
                        deleteStockid();

                        SpinnerManager.hideSpinner(mContext);

                        path="";
                        Toast.makeText(VideoActivity.this, mData.getMessage().toString().trim(), Toast.LENGTH_SHORT).show();
                        Intent intents=new Intent(VideoActivity.this, VideoActivity.class);
                        intents.putExtra("stock_id", stock_id);
                        intents.putExtra("video_url", path);
                        intents.putExtra("regno", regno);
                        startActivity(intents);
                        finish();
                    }

                } catch (Exception e) {
                    SpinnerManager.hideSpinner(mContext);
                    e.printStackTrace();
                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
            }
        });
    }

    private void deleteStockid() {

        try {

            final AISQLLiteAdapter dbAdapter = AISQLLiteAdapter.getInstance();
            String localPath = dbAdapter.isVideoLocalPath(stock_id);

            try {
                final File file = new File(localPath);
                if (file.exists()) {

                    file.delete();

                    Log.d(TAG, "The video file is exists=" + localPath);
                }else{
                    Log.d(TAG, "The video file is deleted=" + localPath);
                }

            } catch (Exception e) {
                Log.e(TAG, "When deleting file= " + localPath + " error=" + e.getMessage());
            }

            SqlAdapterForDML.getInstance().deleteSteps(stock_id);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void DeletetAert() {

        // custom dialog
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.common_cus_dialog);
        //dialog.setTitle("Custom Dialog");

        TextView Message, Message2;
        Button cancel, Confirm;
        Message = dialog.findViewById(R.id.Message);
        Message2 = dialog.findViewById(R.id.Message2);
        cancel = dialog.findViewById(R.id.cancel);
        Confirm = dialog.findViewById(R.id.Confirm);
        Message.setText("Are you sure you want to ");
        Message2.setText("Delete ?");

        Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnClick="";
                videoUrlReq req = new videoUrlReq();
                req.setDealerCode(CommonMethods.getstringvaluefromkey(VideoActivity.this, "dealer_code"));
                req.setVideoUrl("");
                req.setStockId(CommonMethods.getstringvaluefromkey(VideoActivity.this, "stock_id"));
                req.setIsNonMFC("false");
                req.setUserId(CommonMethods.getstringvaluefromkey(VideoActivity.this, "user_id"));
                req.setUploadedFrom("mobile-android");
                req.setUploadedLoc("unknow");

                dialog.dismiss();

                if (!path.isEmpty()) {

                    if (CommonMethods.isInternetWorking(VideoActivity.this)) {
                        sendVideoUrl(VideoActivity.this, req, "delete");
                    } else {
                        CommonMethods.alertMessage(VideoActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
                    }

                }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void sendS3Details(final Context mContext) {


        SpinnerManager.showSpinner(mContext);
        S3DetailsServices.getDetails(VideoActivity.this, "", new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(VideoActivity.this);
                Response<s3Res> mRes = (Response<s3Res>) obj;
                s3Res mData = mRes.body();

                try {
                    AWSSecretsManager.getInstance().parseAWSSecret(mData);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void OnFailure(Throwable t) {
                SpinnerManager.hideSpinner(VideoActivity.this);
                t.printStackTrace();
            }
        });

    }

    @Override
    public void onBackPressed()
    {
        try {
            handler.removeCallbacks(myRunnable);
        }catch (Exception e){
            e.printStackTrace();
        }

        if(CommonMethods.getstringvaluefromkey(VideoActivity.this, "fromImage").equalsIgnoreCase("yes")) {
            finish();
        }else {
            StockFragment.onBack = true;
            Intent intent = new Intent(VideoActivity.this, StockStoreInfoActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

}
