package com.mfcwl.mfc_dealer.Activity.Leads;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Activity.SplashActivity;
import com.mfcwl.mfc_dealer.AddStockModel.ReqUpdateStock;
import com.mfcwl.mfc_dealer.AddStockModel.ResponseCreateStock;
import com.mfcwl.mfc_dealer.AddStockServices.YearService;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.DasBoardModels.CustomWhere;
import com.mfcwl.mfc_dealer.DashBoardServices.WebLeadsService;
import com.mfcwl.mfc_dealer.Fragment.Leads.FollowupHistoryFragment;
import com.mfcwl.mfc_dealer.Fragment.Leads.LeadDetailsFragment;
import com.mfcwl.mfc_dealer.Fragment.Leads.NormalLeadsDetailsFragment;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PWLRequest;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.RequestModel.PrivateLeadRequest;
import com.mfcwl.mfc_dealer.RequestModel.ReqLeadFollowUp;
import com.mfcwl.mfc_dealer.RequestModel.ReqPrivateLeadFollowUp;
import com.mfcwl.mfc_dealer.RequestModel.WebLeadReq1;
import com.mfcwl.mfc_dealer.RequestModel.WhereinPrivateLeadReq;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.ResponseModel.PrivateLeadResponse;
import com.mfcwl.mfc_dealer.ResponseModel.ResLeadFollowUp;
import com.mfcwl.mfc_dealer.ResponseModel.ResPrivateLeadFollowUp;
import com.mfcwl.mfc_dealer.RetrofitServices.AddLeadService;
import com.mfcwl.mfc_dealer.RetrofitServices.LeadFollowUpService;
import com.mfcwl.mfc_dealer.RetrofitServices.PrivateLeadService;
import com.mfcwl.mfc_dealer.StockModels.StockData;
import com.mfcwl.mfc_dealer.StockModels.StockRequest;
import com.mfcwl.mfc_dealer.StockModels.StockResponse;
import com.mfcwl.mfc_dealer.StockModels.Where;
import com.mfcwl.mfc_dealer.StockServices.StockService;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.Global;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.OnClick;
import imageeditor.base.BaseActivity;

import static com.mfcwl.mfc_dealer.Fragment.Leads.NormalLeadsFragment.flagsetonetimecall;
import static com.mfcwl.mfc_dealer.Fragment.Leads.PrivateLeadsFragment.flagRes;

/**
 * Created by HP 240 G5 on 12-03-2018.
 */

public class LeadDetailsActivity extends BaseActivity implements GlobalText {

    public static TextView datefollup, toolbar_lead_name, toolbar_lead_num;
    public static EditText edit_Remark;
    public static TextView Calendarline;
    public static ImageView toolbar_menu;
    public static LinearLayout badgeid;
    public static TextView leads_book_now, leads_update_followup, saleexetv, storestocktv;
    public static String position = "";
    public static String Lregcity = "", Lowner = "", Lregyear = "", Lcolor = "", Lkms = "", Ldid = "", Ltype = "", Leadid = "", Remarks = "", Lmfyear = "", Webleadid = "";
    public static ArrayList<String> registnumberArrayList;
    public static Activity activity;
    public static String stock_reg_number = "";
    public static CheckBox financereqtv;
    public static String actual_selling_price = "";
    public static EditText sellingpricetv;
    public static JSONObject bookNowJsonObject;
    public static FrameLayout list1, list2;
    //retrofit response
    public static StockResponse bookNowJsonObject1;
    public static String upleadstatus = "", upnextfollowdate = "", upremark = "";
    public static Spinner spinSelectStatus;
    public static boolean isoktorepo = false;
    public static String strDate = "";
    static String Lstatus = "";
    static String Lfollupdate = "";
    static String selectStatus = "";
    static String sales_executive_id = "";
    //  public LeadDetailsFragment normalFragment = new LeadDetailsFragment();
    public Fragment fragment;
    String Lname = "";
    String Lmobile = "";
    String Lemail = "";
    String Lposteddate = "";
    String Lmake = "";
    String Lmodel = "";
    String Lprice = "";
    String Lregno = "";
    String Lexename = "", Ldays = "", Lvariant;
    String[] status = {"Select Status", "Hot", "Warm", "Cold", "Lost"};
    String sales_executive_name = "";
    String finacReq = "No";
    ArrayList<String> salesexecIdList;
    ArrayList<String> salesexecList;
    ProgressDialog dialog;
    String lead_type;
    String followupstatus;
    private String TAG = getClass().getSimpleName();

    private static JSONObject jsonMakeforbookstockupdatehistory() {

        JSONObject jObj = new JSONObject();

        try {

            jObj.put("Type", "mfc");
            jObj.put("LeadId", Ldid);
            jObj.put("LeadType", Ltype);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("ParseUpdate ", "jObj " + jObj.toString());
        return jObj;

    }

    private static JSONObject jsonMakePrivate1() {

        JSONObject jObj = new JSONObject();

        try {

            jObj.put("Type", "mfc");
            jObj.put("LeadId", Leadid);
            jObj.put("LeadType", Ltype);
            jObj.put("LeadStatus", Lstatus);
            jObj.put("LeadRemark", "");
            jObj.put("Followup", Lfollupdate);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("ParseUpdatePrivate ", "jObj " + jObj.toString());
        return jObj;

    }

    private static JSONObject jsonMake1() {

        JSONObject jObj = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        try {

            jObj.put("access_token", CommonMethods.getstringvaluefromkey(activity, "access_token"));
            jObj.put("dispatch_id", Ldid);
            jObj.put("employee_id", " ");
            jObj.put("employee_name", " ");
            jObj.put("employee_type", " ");
            jObj.put("followup_date", "0000-00-00");
            jObj.put("followup_status", Lstatus);
            jObj.put("followup_comments", "Booked The Stock");


        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("ParseUpdate ", "jObj " + jObj.toString());
        return jObj;

    }

    public static void Invisible() {
        badgeid.setVisibility(View.GONE);
        /*toolbar_menu.setVisibility(View.INVISIBLE);
        badgeid.setVisibility(View.GONE);*/
    }

    public static void Visible() {


        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
            badgeid.setVisibility(View.VISIBLE);
        } else {

        }



        /*toolbar_menu.setVisibility(View.VISIBLE);
        badgeid.setVisibility(View.VISIBLE);*/
    }

    public static void Parseupdatefollowlead(JSONObject jObj, Activity activity) {

        upleadstatus = spinSelectStatus.getSelectedItem().toString();
        upnextfollowdate = datefollup.getText().toString();
        upremark = edit_Remark.getText().toString();

        //NormalLeadsDetailsFragment.updateLeadState(upleadstatus, upnextfollowdate, upremark);

        Log.e("ParseUpdate ", "followlead " + jObj.toString());

        try {
            if (jObj.getString("status").equals("failure")) {
                WebServicesCall.error_popup2(activity, Integer.parseInt(jObj.getString("status_code")), "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            if (jObj.getString("status").equalsIgnoreCase("success")) {
                Toast.makeText(activity, "Updated successfully", Toast.LENGTH_LONG).show();
                //LeadDetailsFragment.viewPager.setCurrentItem(1);
                CommonMethods.setvalueAgainstKey(activity, "isLeadDetails", "true");
                WebServicesCall.webCall(activity, activity, jsonMakegetLeadsWeb(), "WebLeadStore", GlobalText.POST);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static JSONObject jsonMakeParsePerticularStock() {

        JSONObject jObj = new JSONObject();
        JSONObject jObj1 = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        try {

            jObj.put("Page", "1");
            jObj.put("PageItems", "10");
            jObj.put("OrderBy", "stock_id");
            jObj.put("OrderByReverse", "true");
            //jObj.put("where", jsonArray);

            jObj1.put("column", "registration_number");
            jObj1.put("operator", "=");
            jObj1.put("value", stock_reg_number);
            jsonArray.put(jObj1);
            jObj.put("where", jsonArray);
            //

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //   Log.e("ParseGetStock ", "getStock " + jObj.toString());

        return jObj;

    }

    //retrofit prepare
    private static void getPreparePrivateUpdateLeadStore() {
        List<CustomWhere> customWhereList = new ArrayList<>();
        PrivateLeadRequest mPrivateLeadRequest = new PrivateLeadRequest();
        mPrivateLeadRequest.setPage("1");
        mPrivateLeadRequest.setPageItems("1");
        mPrivateLeadRequest.setOrderBy("created_at");
        mPrivateLeadRequest.setOrderByReverse("true");
        mPrivateLeadRequest.setWhereOr(customWhereList);

        List<CustomWhere> idList = new ArrayList<>();
        CustomWhere id = new CustomWhere();
        /*if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_DEALER_CRE) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_SALES) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_PROCUREMENT)) {
            id.setColumn("executive_id");
            id.setOperator("=");
            id.setValue(CommonMethods.getstringvaluefromkey(activity, "user_id"));
        } else {
            id.setColumn("id");
            id.setOperator("=");
            id.setValue(Leadid);
        }*/
        id.setColumn("id");
        id.setOperator("=");
        id.setValue(Leadid);
        idList.add(id);


        mPrivateLeadRequest.setWhere(idList);

        List<WhereinPrivateLeadReq> wherein = new ArrayList<>();
        mPrivateLeadRequest.setWherein(wherein);


        if (CommonMethods.isInternetWorking(activity)) {
            getfetchPrivateUpdateLeadStore(mPrivateLeadRequest, activity);

        } else {
            CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
        }
        //retrofit call
    }

    //retrofit call
    private static void getfetchPrivateUpdateLeadStore(PrivateLeadRequest mPrivateLeadRequest, Context mContext) {
        SpinnerManager.showSpinner(mContext);
        PrivateLeadService.fetchPrivateLeads(mContext, mPrivateLeadRequest, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);

                try {

                    retrofit2.Response<PrivateLeadResponse> mRes = (retrofit2.Response<PrivateLeadResponse>) obj;

                    PrivateLeadResponse mPrivateLeadResponse = mRes.body();

                    String str = new Gson().toJson(mPrivateLeadResponse, PrivateLeadResponse.class);
                    Log.i("TAG", "Response "+str);

                    NormalLeadsDetailsFragment.RetroParsePrivateLeadStore(mPrivateLeadResponse);
                } catch (Exception e) {

                }
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
            }
        });
    }

    //retrofit ParsePerticularStock
    private static void prepareParsePerticularStock() {

        StockRequest mStockRequest = new StockRequest();
        mStockRequest.setPage("1");
        mStockRequest.setPageItems("10");
        mStockRequest.setOrderBy("stock_id");
        mStockRequest.setOrderByReverse("true");

        Where registration_number = new Where();
        List<Where> mWhereList = new ArrayList<>();

        registration_number.setColumn("registration_number");
        registration_number.setOperator("=");
        registration_number.setValue(stock_reg_number);
        mWhereList.add(registration_number);

        mStockRequest.setWhere(mWhereList);


        //retrofit call
        getParsePerticularStock(mStockRequest, activity);

    }

    //retrofit ParsePerticularStock creating
    private static void getParsePerticularStock(StockRequest mStockRequest, Context mContext) {

        SpinnerManager.showSpinner(mContext);
        StockService.fetchStock(mStockRequest, mContext, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                try {
                    retrofit2.Response<StockResponse> mRes = (retrofit2.Response<StockResponse>) obj;
                    bookNowJsonObject1 = mRes.body();
                } catch (Exception e) {

                }

                SpinnerManager.hideSpinner(mContext);
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
            }
        });

    }

    //retrofit UpdateFollowLeadPrivateForBookNow
    private static void getUpdateFollowLeadPrivateForBookNow(Context mContext, ReqPrivateLeadFollowUp mReqPrivateLeadFollowUp) {
        SpinnerManager.showSpinner(mContext);
        AddLeadService.privateFollowUpLeadFromServer(mReqPrivateLeadFollowUp, mContext, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                try {
                    retrofit2.Response<ResPrivateLeadFollowUp> mRes = (retrofit2.Response<ResPrivateLeadFollowUp>) obj;
                    Toast.makeText(activity, "Booked successfully", Toast.LENGTH_LONG).show();
                } catch (Exception e) {

                }
                SpinnerManager.hideSpinner(mContext);
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
            }
        });
    }

    //retrofit UpdateFollowLeadForBooknow
    private static void getUpdateFollowLeadForBooknow(ReqLeadFollowUp mReqLeadFollowUp, Context mContext) {
        SpinnerManager.showSpinner(mContext);
        LeadFollowUpService.leadFollowUpFromServer(mReqLeadFollowUp, mContext, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                try {
                    retrofit2.Response<ResLeadFollowUp> mRes = (retrofit2.Response<ResLeadFollowUp>) obj;
                    Toast.makeText(activity, "Booked successfully", Toast.LENGTH_LONG).show();
                } catch (Exception e) {

                }
                SpinnerManager.hideSpinner(mContext);
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
            }
        });
    }

    public static void Parsebooknow(JSONObject jObj, String strMethod) {
        try {

           /* CommonMethods.setvalueAgainstKey(activity,"booknowbtn","true");
            dialog2.dismiss();
            Intent in_main = new Intent(activity, MainActivity.class);
            activity.startActivity(in_main);*/


        } catch (Exception ex) {
            Log.e("Parsestockstore-Ex", ex.toString());
        }


    }

    public static void Parsebooknowgetstock(JSONObject jObj, Activity activity) {

        Log.e("jsonMakeGetStock ", "parse " + jObj.toString());

        registnumberArrayList = new ArrayList<String>();

        try {
            JSONArray arrayList = jObj.getJSONArray("data");
            for (int i = 0; i < jObj.getString("data").length(); i++) {
                JSONObject jsonobject = new JSONObject(
                        arrayList.getString(i));
                registnumberArrayList.add(jsonobject.getString("registration_number"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        MethodStockRegisterNumberListPopup(registnumberArrayList);

    }

    public static void MethodStockRegisterNumberListPopup(final ArrayList registnumberArrayList) {


        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        TextView alertdialog_edittext = (EditText) dialog_data.findViewById(R.id.alertdialog_edittext);

        searchtittle.setText("Select Registration no.");
        alertdialog_edittext.setHint("Search Registration no.");


        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);


        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    // dialog_data.cancel();
                }

            }
        });

        ImageView backicon = dialog_data.findViewById(R.id.backicon);
        backicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_data.dismiss();
                //  dialog_data.cancel();
            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, R.layout.simple_list_item_1, registnumberArrayList);
        alertdialog_Listview.setAdapter(adapter);
        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                stock_reg_number = a.getAdapter().getItem(position).toString();


                TextView textview = v.findViewById(R.id.text1);

                //Set your Font Size Here.
                //textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                storestocktv.setText(stock_reg_number);

                WebServicesCall.webCall(activity, activity, jsonMakeParsePerticularStock(), "ParsePerticularStock", GlobalText.POST);

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    //   dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });


        dialog_data.show();
    }

    public static void ParsePerticularStock(JSONObject jObj, Activity activity) {

        Log.e("ParseGetStock ", "parse " + jObj.toString());
        bookNowJsonObject = jObj;
        // jsonMakeBookNow(bookNowJsonObject);
    }

    public static JSONObject jsonMakeBookNow(JSONObject jObj1) {
        JSONObject jObj = new JSONObject();

        try {
            JSONArray arrayList = jObj1.getJSONArray("data");
            JSONObject jsonObject = new JSONObject(
                    arrayList.getString(0));

            jObj.put("stock_id", jsonObject.getString("stock_id"));
            jObj.put("stock_source", jsonObject.getString("stock_source"));
            jObj.put("posted_date", jsonObject.getString("posted_date"));
            jObj.put("vehicle_make", jsonObject.getString("vehicle_make"));
            jObj.put("vehicle_model", jsonObject.getString("vehicle_model"));
            jObj.put("vehicle_variant", jsonObject.getString("vehicle_variant"));
            jObj.put("reg_month", "01");
            jObj.put("reg_year", jsonObject.getString("reg_year"));
            jObj.put("registraion_city", jsonObject.getString("registraion_city"));
            jObj.put("registration_number", jsonObject.getString("registration_number"));

            jObj.put("colour", jsonObject.getString("colour"));
            jObj.put("kilometer", jsonObject.getString("kilometer"));
            jObj.put("owner", jsonObject.getString("owner"));
            if (jsonObject.getString("insurance").trim().equalsIgnoreCase("")) {
                jObj.put("insurance", "NA");
            } else {
                jObj.put("insurance", jsonObject.getString("insurance"));
            }
            if (jsonObject.getString("insurance_exp_date").trim().equalsIgnoreCase("")) {
                jObj.put("insurance_exp_date", "NA");
            } else {
                jObj.put("insurance_exp_date", jsonObject.getString("insurance_exp_date"));
            }
            jObj.put("selling_price", jsonObject.getString("selling_price"));
            /*if(jsonObject.getString("is_display").equals("null"))
            {
                Log.e("booknow ","Inside");
                jObj.put("is_display", null);
            }
            else
            {
                Log.e("booknow ","Outside");
                jObj.put("is_display", jsonObject.getBoolean("is_display"));
            }*/

            jObj.put("bought_price", jsonObject.getString("bought_price"));
            if (jsonObject.getString("refurbishment_cost").trim().equalsIgnoreCase("null")) {
                jObj.put("refurbishment_cost", "0");
            } else {
                jObj.put("refurbishment_cost", jsonObject.getString("refurbishment_cost"));
            }
            jObj.put("procurement_executive_id", jsonObject.getString("procurement_executive_id"));
            jObj.put("cng_kit", jsonObject.getString("cng_kit"));
            if (jsonObject.getString("private_vehicle").equals("null")) {
                jObj.put("private_vehicle", null);
            } else {
                jObj.put("private_vehicle", jsonObject.getBoolean("private_vehicle"));
            }
            jObj.put("comments", jsonObject.getString("comments"));


            jObj.put("manufacture_month", "01");

            jObj.put("is_offload", jsonObject.getString("is_offload"));

            jObj.put("manufacture_year", jsonObject.getString("manufacture_year"));
            jObj.put("updated_by_device", "MOBILE");
            /* if(jsonObject.getString("is_featured_car").equals("null")) {
                 jObj.put("is_featured_car", null);
             }
             else {
                 jObj.put("is_featured_car", jsonObject.getBoolean("is_featured_car"));
             }*/
            jObj.put("is_featured_car", "false");
            jObj.put("is_booked", "true");
            jObj.put("actual_selling_price", sellingpricetv.getText().toString());
            jObj.put("sales_executive_id", sales_executive_id);
            jObj.put("finance_required", financereqtv.isChecked());

            jObj.put("chassis_number", jsonObject.getString("chassis_number"));
            jObj.put("engine_number", jsonObject.getString("engine_number"));
            jObj.put("dealer_price", jsonObject.getString("dealer_price"));
            jObj.put("dealer_code", jsonObject.getString("dealer_code"));

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("booknow ", "Exception " + e.toString());
        }

        Log.e("booknow ", "booknow " + jObj.toString());
        return jObj;

    }

    public static void ParseBookNowStock(JSONObject jObj, Activity activity) {

        //   Log.e("Submit ", "ParseBookNowStock " + jObj.toString());
        CommonMethods.setvalueAgainstKey(activity, "isLeadDetails", "true");
        UpdateFollowUpForStatusHistory();
    }

    private static void UpdateFollowUpForStatusHistory() {

        if (CommonMethods.getstringvaluefromkey(activity, "WLEADS").equals("PrivateLeads")) {

            WebServicesCall.webCall(activity, activity, jsonMakePrivate1(), "UpdateFollowLeadPrivateForBookNow", GlobalText.PUT);
        }
        if (CommonMethods.getstringvaluefromkey(activity, "WLEADS").equals("WebLeads")) {
            WebServicesCall.webCall(activity, activity, jsonMake1(), "UpdateFollowLeadForBooknow", GlobalText.POST);
        }
    }

    public static void ParseUpdateFollowupLeadBookNow(JSONObject jObj, Activity activity) {

        Log.e("Submit ", "ParseUpdateFollowupLeadBookNow " + jObj.toString());

        try {
            if (jObj.getString("status").equalsIgnoreCase("success")) {
                Toast.makeText(activity, jObj.getString("message"), Toast.LENGTH_LONG).show();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public static void ParseUpdateFollowLeadPrivate(JSONObject jObj, Activity activity) {

        upleadstatus = spinSelectStatus.getSelectedItem().toString();
        upnextfollowdate = datefollup.getText().toString();
        upremark = edit_Remark.getText().toString();

        //NormalLeadsDetailsFragment.updateLeadState(upleadstatus, upnextfollowdate, upremark);

        Log.e("UpdateFolrivate ", "leads " + jObj.toString());
        try {
            if (jObj.getString("status").equalsIgnoreCase("success")) {
                Toast.makeText(activity, "Updated successfully", Toast.LENGTH_LONG).show();
                // jsonMakePrivateLeadStore();
                CommonMethods.setvalueAgainstKey(activity, "isLeadDetails", "true");
                WebServicesCall.webCall(activity, activity, jsonMakePrivateLeadStore(), "PrivateUpdateLeadStore", GlobalText.POST);
                //LeadDetailsFragment.viewPager.setCurrentItem(1);
                //FollowupHistoryFragment.reloadAPI();
            } else {
                Toast.makeText(activity, jObj.getString("message"), Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        /*try {
            if(jObj.getString("status").equalsIgnoreCase("success"))
            {
                Toast.makeText(activity,jObj.getString("message"),Toast.LENGTH_LONG).show();
                LeadDetailsFragment.viewPager.setCurrentItem(1);
                FollowupHistoryFragment.reloadAPI();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

    }

    public static void ParseUpdateFollowLeadPrivateForBookNow(JSONObject jObj, Activity activity) {

        Log.e("PriorWebjObj ", "booknowPri " + jObj.toString());

        try {
            String res = jObj.getString("message");
            Toast.makeText(activity, "Booked successfully", Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public static void ParseUpdateFollowLeadForBooknow(JSONObject jObj, Activity activity) {

        Log.e("PriorWebjObj ", "booknowWeb " + jObj.toString());

        try {
            if (jObj.getString("status").equals("failure")) {
                WebServicesCall.error_popup2(activity, Integer.parseInt(jObj.getString("status_code")), "");
            } else {
                Toast.makeText(activity, "Booked successfully", Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //   Log.e("PriorWebjObj ", "booknowWeb " + jObj.toString());

    }

    private static JSONObject jsonMakegetLeadsWeb() {

        JSONObject jObj = new JSONObject();

        JSONObject jObjleadsid = new JSONObject();
        JSONObject jObjtargetid = new JSONObject();
        JSONObject jObjtargetleadsource = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONArray jsonArrayWherenot = new JSONArray();
        try {
            jObj.put("filter_by_fields", "");
            jObj.put("per_page", 1);
            jObj.put("page", 1);
            jObj.put("tag", "android");
            jObj.put("alias_fields", "leads.created_at as PostedDate,leads.created_at as LeadDate,leads.id as id,dispatched.id as dispatchid,leads.customer_name as Name,leads.customer_mobile as Mobile,leads.customer_email as Email,leads.primary_make,leads.primary_model,leads.primary_variant,leads.secondary_make,leads.secondary_model,leads.secondary_variant,leads.register_no as regno,leads.register_month as regmonth,leads.register_year as regyear,leads.color,leads.kms,mfg_year,leads.owner as owners,leads.register_city,leads.stock_listed_price as price,dispatched.status,dispatched.followup_comments as remark,dispatched.followup_date as FollowUpDate,dispatched.employee_name as ExecutiveName");

            jObjleadsid.put("column", "leads.id");
            jObjleadsid.put("operator", "=");
            jObjleadsid.put("value", Webleadid);

            //dealer id
            jObjtargetid.put("column", "dispatched.target_id");
            jObjtargetid.put("operator", "=");
            jObjtargetid.put("value", CommonMethods.getstringvaluefromkey(activity, "dealer_code"));
            jsonArray.put(jObjtargetid);


            jObjtargetleadsource.put("column", "dispatched.target_source");
            jObjtargetleadsource.put("operator", "=");
            jObjtargetleadsource.put("value", "MFC");
            jsonArray.put(jObjtargetleadsource);


            jsonArray.put(jObjleadsid);
            jsonArray.put(jObjtargetid);

            jObj.put("wherenotin", jsonArrayWherenot);
            jObj.put("custom_where", jsonArray);
            jObj.put("access_token", CommonMethods.getstringvaluefromkey(activity, "access_token"));
        } catch (Exception e) {
            Log.e("NewCheck ", "Exception " + e.toString());

        }
        Log.e("NewCheck ", "Request " + jObj.toString());
        return jObj;
    }

    public static void Parsewebleadstore(JSONObject jObj, Activity activity) {

        FollowupHistoryFragment.reloadAPI();
    }

    private static JSONObject jsonMakePrivateLeadStore() {
        JSONObject jObj = new JSONObject();
        JSONObject jObjleadid = new JSONObject();
        JSONArray jsonSearchVal = new JSONArray();
        JSONArray jsonleadid = new JSONArray();
        JSONArray whereJsonArray = new JSONArray();
        JSONArray whereinJsonArray = new JSONArray();


        try {
            jObj.put("PageItems", "1");
            jObj.put("Page", "1");

            jObj.put("OrderBy", "created_at");
            jObj.put("OrderByReverse", "true");

            jObj.put("where_or", jsonSearchVal);


            jObjleadid.put("column", "id");
            jObjleadid.put("operator", "=");
            jObjleadid.put("value", Leadid);

            whereJsonArray.put(jObjleadid);

            jObj.put("where", whereJsonArray);

            jObj.put("wherein", whereinJsonArray);

        } catch (JSONException e) {

        }
        Log.e("PrivateLeadStore ", "JsonExcep " + jObj.toString());
        return jObj;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate: ");
        setContentView(R.layout.layout_main_leadsdetails_activity);
        //ButterKnife.bind(this);
        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        activity = this;
        flagRes = true;
        NormalLeadsDetailsFragment.lost = false;
        flagsetonetimecall = true;
        salesexecIdList = new ArrayList<String>();
        salesexecList = new ArrayList<String>();
        toolbar_menu = (ImageView) findViewById(R.id.toolbar_menu);
        badgeid = (LinearLayout) findViewById(R.id.badgeid);

        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer") ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_SALES) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_DEALER_CRE)) {

        } else {
            badgeid.setVisibility(View.GONE);
        }

        leads_book_now = (TextView) findViewById(R.id.leads_book_now);
        leads_update_followup = (TextView) findViewById(R.id.leads_update_followup);
        toolbar_lead_name = (TextView) findViewById(R.id.toolbar_lead_name);
        toolbar_lead_num = (TextView) findViewById(R.id.toolbar_lead_num);
        list1 = (FrameLayout) findViewById(R.id.list1);
        list2 = (FrameLayout) findViewById(R.id.list2);
        //btnback = (Button) findViewById(R.id.btnback);
        //toolbar_menu.setVisibility(View.VISIBLE);
       /* this.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);*/

        Bundle bundle1 = getIntent().getExtras();
        Lname = bundle1.getString("NM");
        Lmobile = bundle1.getString("MN");
        Lemail = bundle1.getString("EM");
        Lstatus = bundle1.getString("LS");
        Lposteddate = bundle1.getString("PD");
        Lmake = bundle1.getString("MK");
        Lmodel = bundle1.getString("MD");
        Lprice = bundle1.getString("PR");

        Lregno = bundle1.getString("RN");
        Lfollupdate = bundle1.getString("NFD");
        Lexename = bundle1.getString("ENM");
        // Log.e("private:lead date", Lfollupdate);

        Lregcity = bundle1.getString("RGC");
        Lowner = bundle1.getString("OWN");
        Lregyear = bundle1.getString("MYR");
        Lcolor = bundle1.getString("CLR");
        Lkms = bundle1.getString("KMS");
        Ldid = bundle1.getString("DID");
        Ldays = bundle1.getString("LD");
        Lvariant = bundle1.getString("MV");

        if (CommonMethods.getstringvaluefromkey(activity, "WLEADS").equals("PrivateLeads")) {
            Ltype = bundle1.getString("LT");
            Leadid = bundle1.getString("LID");
            Remarks = bundle1.getString("RMK");
        }
        if (CommonMethods.getstringvaluefromkey(activity, "WLEADS").equals("WebLeads")) {
            Remarks = bundle1.getString("RMK");
            Lmfyear = bundle1.getString("MFY");
            Webleadid = bundle1.getString("IDL");
        }

        position = bundle1.getString("position");
        lead_type = bundle1.getString("lead_type");

        Bundle bundle = new Bundle();
        bundle.putString("paramsname", Lname);
        bundle.putString("paramsmn", Lmobile);
        bundle.putString("paramsem", Lemail);
        bundle.putString("paramsstat", Lstatus);
        bundle.putString("paramspd", Lposteddate);
        bundle.putString("paramsmk", Lmake);

        bundle.putString("paramsmd", Lmodel);
        bundle.putString("paramsprice", Lprice);
        bundle.putString("paramsrn", Lregno);
        bundle.putString("paramsfollup", Lfollupdate);
        bundle.putString("paramsexenm", Lexename);

        bundle.putString("paramsrgcity", Lregcity);
        bundle.putString("paramsowner", Lowner);
        bundle.putString("paramsrgyr", Lregyear);
        bundle.putString("paramscolor", Lcolor);
        bundle.putString("paramskms", Lkms);
        bundle.putString("paramsdid", Ldid);
        bundle.putString("paramsldays", Ldays);
        bundle.putString("paramslvar", Lvariant);

        if (CommonMethods.getstringvaluefromkey(activity, "WLEADS").equals("PrivateLeads")) {
            bundle.putString("paramsleadtype", Ltype);
            bundle.putString("paramsleadid", Leadid);
            bundle.putString("paramsremark", Remarks);
        }
        if (CommonMethods.getstringvaluefromkey(activity, "WLEADS").equals("WebLeads")) {
            bundle.putString("paramsremark", Remarks);
            bundle.putString("paramsmfy", Lmfyear);
        }

        LeadDetailsFragment homeFragment = new LeadDetailsFragment();
        homeFragment.setArguments(bundle);
        this.fragmentLoad(homeFragment);

        leads_book_now.setOnClickListener(view -> {

            if (bookNotAllowed())
                return;

            Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.leads_details_booknow, GlobalText.android);
            ShowBookNow();
        });

        leads_update_followup.setOnClickListener(view -> {

            if (followUpNotAllowed())
                return;

            Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.leads_details_update, GlobalText.android);

            UpdateFollowUp();

        });

        toolbar_lead_name.setText(Lname);
        toolbar_lead_num.setText(Lmobile);

        //      Log.e("Lstatus", "Lstatus " + Lstatus);


    }

    //get weblead

    public void fragmentLoad(Fragment fragment) {

        FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fillLayout_main, fragment);
        fragmentTransaction.commit();

    }

    public void UpdateFollowUp() {

        final Dialog dialog2 = new Dialog(this, R.style.full_screen_dialog);

        dialog2.setContentView(R.layout.booknowpop2);
        String star = "<font color='#B40404'>*</font>";
        // dialog2.show();
        Button updatenowbtn = dialog2.findViewById(R.id.updatenowbtn);
        Button cancelbtn = dialog2.findViewById(R.id.addleadcancel);
        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  dialog2.cancel();
                dialog2.dismiss();
            }
        });

        TextView next_followupdate = dialog2.findViewById(R.id.next_followupdate);
        TextView lead_status = dialog2.findViewById(R.id.lead_status);
        edit_Remark = dialog2.findViewById(R.id.edit_Remark);
        datefollup = dialog2.findViewById(R.id.datefollup);
        Calendarline = dialog2.findViewById(R.id.Calendarline);

        spinSelectStatus = dialog2.findViewById(R.id.spinSelectStatus);

        List<String> mList = new ArrayList<>(MainActivity.statusList);
        mList.add(0, "Select Status");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.simple_spinner2, mList);

        // spinSelectStatus.setDropDownViewResource(R.layout.simple_spinner2);
        spinSelectStatus.setAdapter(adapter);
        spinSelectStatus.setPrompt("Select Status");
        spinSelectStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                View view1 = adapterView.getSelectedView();
                TextView tv = (TextView) view1;
                if (i == 0) {
                    // Set the disable item text color
                    tv.setTextColor(Color.GRAY);

                } else {
                    tv.setTextColor(Color.BLACK);
                }

                selectStatus = adapterView.getSelectedItem().toString();

                // Log.e("LeadStatus", MainActivity.leadstatus.get(i).getStatus());

                if (i != 0) {
                    followupstatus = MainActivity.leadstatus.get(i - 1).getIsFollowup();

                    if (MainActivity.leadstatus.get(i - 1).getIsFollowup().equalsIgnoreCase("false")) {
                        next_followupdate.setVisibility(View.INVISIBLE);
                        datefollup.setVisibility(View.INVISIBLE);
                        Calendarline.setVisibility(View.INVISIBLE);
                    } else {
                        next_followupdate.setVisibility(View.VISIBLE);
                        datefollup.setVisibility(View.VISIBLE);
                        Calendarline.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        lead_status.setText(Html.fromHtml("LEAD STATUS " + star));
        next_followupdate.setText(Html.fromHtml("NEXT FOLLOW UP DATE " + star));

        CommonMethods.setvalueAgainstKey(this, "updatenowbtn", "false");

        Window window = dialog2.getWindow();
        this.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);

        datefollup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDateFuture(view);
            }
        });

        next_followupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDate(view);
            }
        });

       /* updatenowbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long milliseconds = System.currentTimeMillis();
                Calendar cl = Calendar.getInstance();
                cl.setTimeInMillis(milliseconds);  //here your time in miliseconds
                String date = "" + cl.get(Calendar.DAY_OF_MONTH);
                Log.e("date ", "show " + datefollup.getText().toString());
                String[] splitIfun = datefollup.getText().toString().split("-", 10);

                CommonMethods.setvalueAgainstKey(LeadDetailsActivity.this, "updatenowbtn", "true");

                if (selectStatus.equals("Select Status")) {
                    Toast.makeText(LeadDetailsActivity.this, "Please select the status ", Toast.LENGTH_LONG).show();

                } else {
                    if (selectStatus.equals("Lost")) {
                        if (CommonMethods.getstringvaluefromkey(activity, "WLEADS").equals("PrivateLeads")) {
                            // WebServicesCall.webCall(LeadDetailsActivity.this, LeadDetailsActivity.this, jsonMakePrivate(), "UpdateFollowLeadPrivate", GlobalText.PUT);
                            ReqPrivateLeadFollowUp mReqPrivateLeadFollowUp = new ReqPrivateLeadFollowUp();
                            mReqPrivateLeadFollowUp.setType("mfc");
                            mReqPrivateLeadFollowUp.setLeadId(Leadid);
                            mReqPrivateLeadFollowUp.setLeadType(Ltype);
                            mReqPrivateLeadFollowUp.setLeadStatus(selectStatus);
                            if (edit_Remark.getText().toString().trim().equals("")) {
                                mReqPrivateLeadFollowUp.setLeadRemark("NA");
                            } else {
                                mReqPrivateLeadFollowUp.setLeadRemark(edit_Remark.getText().toString());
                            }
                            if (datefollup.getText().toString().equals("")) {
                                mReqPrivateLeadFollowUp.setFollowup("");
                            } else {
                                mReqPrivateLeadFollowUp.setFollowup(datefollup.getText().toString());
                            }

                            if (CommonMethods.isInternetWorking(activity)) {
                                privateFollowUpLead(mReqPrivateLeadFollowUp, LeadDetailsActivity.this);

                            } else {
                                CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
                            }

                        }
                        if (CommonMethods.getstringvaluefromkey(activity, "WLEADS").equals("WebLeads")) {
                            //WebServicesCall.webCall(LeadDetailsActivity.this, LeadDetailsActivity.this, jsonMake(), "UpdateFollowLead", GlobalText.POST);
                            ReqLeadFollowUp mReqLeadFollowUp = new ReqLeadFollowUp();
                            mReqLeadFollowUp.setAccessToken(CommonMethods.getstringvaluefromkey(activity, "access_token"));
                            mReqLeadFollowUp.setDispatchId(Ldid);
                            mReqLeadFollowUp.setEmployeeId("");
                            mReqLeadFollowUp.setEmployeeName("");
                            mReqLeadFollowUp.setEmployeeType("");
                            mReqLeadFollowUp.setTag("android");
                            if (datefollup.getText().toString().equals("")) {
                                mReqLeadFollowUp.setFollowupDate("0000-00-00");
                            } else {
                                mReqLeadFollowUp.setFollowupDate(datefollup.getText().toString());
                            }
                            mReqLeadFollowUp.setFollowupStatus(selectStatus);
                            if (edit_Remark.getText().toString().equals("")) {
                                mReqLeadFollowUp.setFollowupComments("NA");
                            } else {
                                mReqLeadFollowUp.setFollowupComments(edit_Remark.getText().toString());
                            }


                            if (CommonMethods.isInternetWorking(activity)) {
                                leadFollowUpHistory(mReqLeadFollowUp, LeadDetailsActivity.this);

                            } else {
                                CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
                            }

                        }
                        dialog2.dismiss();
                    } else {

                        if (datefollup.getText().toString().equals("")) {
                            Toast.makeText(LeadDetailsActivity.this, "Please select the follow up date", Toast.LENGTH_LONG).show();
                        } else {
                            if (CommonMethods.getstringvaluefromkey(activity, "WLEADS").equals("PrivateLeads")) {

                                // if (!date.trim().equals(splitIfun[2].trim())) {
                                // WebServicesCall.webCall(LeadDetailsActivity.this, LeadDetailsActivity.this, jsonMakePrivate(), "UpdateFollowLeadPrivate", GlobalText.PUT);
                                ReqPrivateLeadFollowUp mReqPrivateLeadFollowUp = new ReqPrivateLeadFollowUp();
                                mReqPrivateLeadFollowUp.setType("mfc");
                                mReqPrivateLeadFollowUp.setLeadId(Leadid);
                                mReqPrivateLeadFollowUp.setLeadType(Ltype);
                                mReqPrivateLeadFollowUp.setLeadStatus(selectStatus);
                                if (edit_Remark.getText().toString().trim().equals("")) {
                                    mReqPrivateLeadFollowUp.setLeadRemark("NA");
                                } else {
                                    mReqPrivateLeadFollowUp.setLeadRemark(edit_Remark.getText().toString());
                                }
                                if (datefollup.getText().toString().equals("")) {
                                    mReqPrivateLeadFollowUp.setFollowup("");
                                } else {
                                    mReqPrivateLeadFollowUp.setFollowup(datefollup.getText().toString());
                                }
                                privateFollowUpLead(mReqPrivateLeadFollowUp, LeadDetailsActivity.this);

                                dialog2.dismiss();
                                *//*} else {
                                    Toast.makeText(LeadDetailsActivity.this, "Please Select future date", Toast.LENGTH_LONG).show();
                                }*//*
                            }
                            if (CommonMethods.getstringvaluefromkey(activity, "WLEADS").equals("WebLeads")) {
                                Log.e("SetFuturevali ", "SetFuturevali " + date + "   " + splitIfun[2]);
                                // if (!date.trim().equals(splitIfun[2].trim())) {
                                // WebServicesCall.webCall(LeadDetailsActivity.this, LeadDetailsActivity.this, jsonMake(), "UpdateFollowLead", GlobalText.POST);
                                ReqLeadFollowUp mReqLeadFollowUp = new ReqLeadFollowUp();
                                mReqLeadFollowUp.setAccessToken(CommonMethods.getstringvaluefromkey(activity, "access_token"));
                                mReqLeadFollowUp.setDispatchId(Ldid);
                                mReqLeadFollowUp.setEmployeeId("");
                                mReqLeadFollowUp.setEmployeeName("");
                                mReqLeadFollowUp.setEmployeeType("");
                                mReqLeadFollowUp.setTag("android");
                                if (datefollup.getText().toString().equals("")) {
                                    mReqLeadFollowUp.setFollowupDate("0000-00-00");
                                } else {
                                    mReqLeadFollowUp.setFollowupDate(datefollup.getText().toString());
                                }
                                mReqLeadFollowUp.setFollowupStatus(selectStatus);
                                if (edit_Remark.getText().toString().equals("")) {
                                    mReqLeadFollowUp.setFollowupComments("NA");
                                } else {
                                    mReqLeadFollowUp.setFollowupComments(edit_Remark.getText().toString());
                                }

                                if (CommonMethods.isInternetWorking(activity)) {
                                    leadFollowUpHistory(mReqLeadFollowUp, LeadDetailsActivity.this);

                                } else {
                                    CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
                                }

                                dialog2.dismiss();
                                *//*} else {
                                    Toast.makeText(LeadDetailsActivity.this, "Please Select future date", Toast.LENGTH_LONG).show();
                                }*//*
                            }
                        }
                    }

                }
            }
        });

    }
*/

        updatenowbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long milliseconds = System.currentTimeMillis();
                Calendar cl = Calendar.getInstance();
                cl.setTimeInMillis(milliseconds);  //here your time in miliseconds
                String date = "" + cl.get(Calendar.DAY_OF_MONTH);
                //   Log.e("date ", "show " + datefollup.getText().toString());
                String[] splitIfun = datefollup.getText().toString().split("-", 10);

                CommonMethods.setvalueAgainstKey(LeadDetailsActivity.this, "updatenowbtn", "true");

                if (selectStatus.equals("Select Status")) {
                    Toast.makeText(LeadDetailsActivity.this, "Please select the status ", Toast.LENGTH_LONG).show();

                } else {
                    if (followupstatus.equalsIgnoreCase("false")) {
                        if (CommonMethods.getstringvaluefromkey(activity, "WLEADS").equals("PrivateLeads")) {
                            WebServicesCall.webCall(LeadDetailsActivity.this, LeadDetailsActivity.this, jsonMakePrivate(), "UpdateFollowLeadPrivate", GlobalText.PUT);
                        }
                        if (CommonMethods.getstringvaluefromkey(activity, "WLEADS").equals("WebLeads")) {
                            WebServicesCall.webCall(LeadDetailsActivity.this, LeadDetailsActivity.this, jsonMake(), "UpdateFollowLead", GlobalText.POST);
                        }
                        dialog2.dismiss();
                    } else {

                        if (datefollup.getText().toString().equals("")) {
                            Toast.makeText(LeadDetailsActivity.this, "Please select the follow up date", Toast.LENGTH_LONG).show();
                        } else {
                            if (CommonMethods.getstringvaluefromkey(activity, "WLEADS").equals("PrivateLeads")) {

                                // if (!date.trim().equals(splitIfun[2].trim())) {
                                WebServicesCall.webCall(LeadDetailsActivity.this, LeadDetailsActivity.this, jsonMakePrivate(), "UpdateFollowLeadPrivate", GlobalText.PUT);
                                dialog2.dismiss();
                                /*} else {
                                    Toast.makeText(LeadDetailsActivity.this, "Please Select future date", Toast.LENGTH_LONG).show();
                                }*/
                            }
                            if (CommonMethods.getstringvaluefromkey(activity, "WLEADS").equals("WebLeads")) {
                                Log.e("SetFuturevali ", "SetFuturevali " + date + "   " + splitIfun[2]);
                                // if (!date.trim().equals(splitIfun[2].trim())) {
                                WebServicesCall.webCall(LeadDetailsActivity.this, LeadDetailsActivity.this, jsonMake(), "UpdateFollowLead", GlobalText.POST);
                                dialog2.dismiss();
                                /*} else {
                                    Toast.makeText(LeadDetailsActivity.this, "Please Select future date", Toast.LENGTH_LONG).show();
                                }*/
                            }
                        }
                    }

                }
            }
        });

        dialog2.show();
    }

    private JSONObject jsonMakePrivate() {

        JSONObject jObj = new JSONObject();

        try {

            jObj.put("Type", "mfc");
            jObj.put("LeadId", Leadid);

            jObj.put("LeadType", lead_type);
            jObj.put("LeadStatus", selectStatus);
            jObj.put("updated_by_device", "OMS App-android");
            if (edit_Remark.getText().toString().trim().equals("")) {
                jObj.put("LeadRemark", "NA");
            } else {
                jObj.put("LeadRemark", edit_Remark.getText().toString());
            }

            if (datefollup.getText().toString().equals("")) {
                jObj.put("Followup", "");
            } else {
                jObj.put("Followup", datefollup.getText().toString());
            }

            //jObj.put("Followup", datefollup.getText().toString());


        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("LeadDetails : request", jObj.toString());
        return jObj;

    }

    private JSONObject jsonMake() {

        //new updates

        JSONObject jObj = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            jObj.put("access_token", CommonMethods.getstringvaluefromkey(this, "access_token"));
            jObj.put("dispatch_id", Ldid);
            jObj.put("employee_id", "");
            jObj.put("employee_name", "");
            jObj.put("employee_type", lead_type);
            jObj.put("tag", "android");
            if (datefollup.getText().toString().equals("")) {
                jObj.put("followup_date", "0000-00-00");
            } else {
                jObj.put("followup_date", datefollup.getText().toString());
            }
            jObj.put("followup_status", selectStatus);
            if (edit_Remark.getText().toString().equals("")) {
                jObj.put("followup_comments", "NA");
            } else {
                jObj.put("followup_comments", edit_Remark.getText().toString());
            }

            if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
                jObj.put("statusupdatedby", CommonMethods.getstringvaluefromkey(activity, "user_name") + "-" +
                        CommonMethods.getstringvaluefromkey(activity, "dealer_code"));
            } else {

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("Request Sales Lead", "jObj " + jObj.toString());

        return jObj;

    }

    public void setDateFuture(View v) {

        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
            // when dialog box is closed, below method will be called.
            public void onDateSet(DatePicker view, int selectedYear,
                                  int selectedMonth, int selectedDay) {
                if (isoktorepo) {
                    myCalendar.set(Calendar.YEAR, selectedYear);
                    myCalendar.set(Calendar.MONTH, selectedMonth);
                    myCalendar.set(Calendar.DAY_OF_MONTH, selectedDay);

                    // System.out.println();
                    String strselectedDay = String.valueOf(selectedDay);
                    if (strselectedDay.length() == 1) {
                        strselectedDay = "0" + strselectedDay;
                    }
                    String strselectedMonth = String.valueOf(selectedMonth + 1);
                    if (strselectedMonth.length() == 1) {
                        strselectedMonth = "0" + strselectedMonth;
                    }


                    /*strDate = strselectedDay + "-" + strselectedMonth + "-"
                            + selectedYear;*/

                    strDate = selectedYear + "-" + strselectedMonth + "-"
                            + strselectedDay;

                    datefollup.setText(strDate);

                }
                isoktorepo = false;
            }
        };

        final DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                datePickerListener, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            dialog.cancel();
                            isoktorepo = false;
                        }
                    }
                });
        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    @SuppressLint("NewApi")
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            isoktorepo = true;

                            DatePicker datePicker = datePickerDialog
                                    .getDatePicker();
                            datePickerListener.onDateSet(datePicker,
                                    datePicker.getYear(),
                                    datePicker.getMonth(),
                                    datePicker.getDayOfMonth());

                        }
                    }
                });

        long currentTime = System.currentTimeMillis();
        datePickerDialog.getDatePicker().setMinDate(currentTime);
        datePickerDialog.setCancelable(false);
        datePickerDialog.show();

    }

    public void setDate(View v) {

        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
            // when dialog box is closed, below method will be called.
            public void onDateSet(DatePicker view, int selectedYear,
                                  int selectedMonth, int selectedDay) {
                if (isoktorepo) {
                    myCalendar.set(Calendar.YEAR, selectedYear);
                    myCalendar.set(Calendar.MONTH, selectedMonth);
                    myCalendar.set(Calendar.DAY_OF_MONTH, selectedDay);

                    // System.out.println();
                    String strselectedDay = String.valueOf(selectedDay);
                    if (strselectedDay.length() == 1) {
                        strselectedDay = "0" + strselectedDay;
                    }
                    String strselectedMonth = String.valueOf(selectedMonth + 1);
                    if (strselectedMonth.length() == 1) {
                        strselectedMonth = "0" + strselectedMonth;
                    }

                    strDate = selectedYear + "-" + strselectedMonth + "-"
                            + strselectedDay;

                    datefollup.setText(strDate + " ");

                }
                isoktorepo = false;
            }
        };

        final DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                datePickerListener, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            dialog.cancel();
                            isoktorepo = false;
                        }
                    }
                });
        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    @SuppressLint("NewApi")
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            isoktorepo = true;

                            DatePicker datePicker = datePickerDialog
                                    .getDatePicker();
                            datePickerListener.onDateSet(datePicker,
                                    datePicker.getYear(),
                                    datePicker.getMonth(),
                                    datePicker.getDayOfMonth());

                        }
                    }
                });

        datePickerDialog.setCancelable(false);
        datePickerDialog.show();


    }

    public void ShowBookNow() {
        final Dialog dialog2 = new Dialog(this, R.style.full_screen_dialog);
        dialog2.setContentView(R.layout.booknowpop1);
        String star = "<font color='#B40404'>*</font>";
        Button booknowbtn = dialog2.findViewById(R.id.booknowbtn);
        Button cancelbtn = dialog2.findViewById(R.id.addleadcancel);
        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // dialog2.cancel();
                dialog2.dismiss();
            }
        });

        TextView storestocklbl = dialog2.findViewById(R.id.storestocklbl);
        TextView saleexelbl = dialog2.findViewById(R.id.saleexelbl);
        TextView financereqlbl = dialog2.findViewById(R.id.financereqlbl);
        TextView sellingpricelbl = dialog2.findViewById(R.id.sellingpricelbl);
        sellingpricetv = dialog2.findViewById(R.id.sellingpricetv);

        storestocktv = dialog2.findViewById(R.id.storestocktv);
        saleexetv = dialog2.findViewById(R.id.saleexetv);
        financereqtv = dialog2.findViewById(R.id.financereqtv);

        // updateFontUI(activity);

        storestocklbl.setText(Html.fromHtml("STORE STOCK " + star));
        saleexelbl.setText(Html.fromHtml("SALES EXECUTIVE " + star));
        sellingpricelbl.setText(Html.fromHtml("ACTUAL SELLING PRICE " + star));
        financereqlbl.setText(Html.fromHtml("FINANCE REQURIED "));

        //sellingpricetv.setText((this.getResources().getString(R.string.rs)+""));

        CommonMethods.setvalueAgainstKey(this, "booknowbtn", "false");

        Window window = dialog2.getWindow();
        this.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);

        booknowbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (storestocktv.getText().toString().trim().equals("")) {
                    Toast.makeText(LeadDetailsActivity.this, "Select stock registration no.", Toast.LENGTH_LONG).show();
                } else if (saleexetv.getText().toString().trim().equals("")) {
                    Toast.makeText(LeadDetailsActivity.this, "Select sales executive name", Toast.LENGTH_LONG).show();
                } else if (sellingpricetv.getText().toString().trim().equals("")) {
                    Toast.makeText(LeadDetailsActivity.this, "Enter actual selling price", Toast.LENGTH_LONG).show();
                } else {
                    // CommonMethods.setvalueAgainstKey(LeadDetailsActivity.this, "booknowbtn", "true");
                    CommonMethods.setvalueAgainstKey(LeadDetailsActivity.this, "booknowbtn", "false");
                    // Toast.makeText(LeadDetailsActivity.this, "Sucessfully submitted", Toast.LENGTH_LONG).show();

                    WebServicesCall.webCall(activity, activity, jsonMakeBookNow(bookNowJsonObject), "BooknowStock", GlobalText.PUT);

                    // prepareBooknowStock(bookNowJsonObject1);
                    dialog2.dismiss();
                }
            }
        });

        saleexetv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    dialog = new ProgressDialog(LeadDetailsActivity.this);
                    try {
                        if (SplashActivity.progress) {
                            dialog.show();
                        }
                    } catch (WindowManager.BadTokenException e) {

                    }
                    dialog.setCancelable(false);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.setContentView(R.layout.progressdialog);

                } catch (Exception e) {
                    e.printStackTrace();

                }

                GetMethodSalesExecutiveLoadData();
            }
        });

        storestocktv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WebServicesCall.webCall(activity, activity, jsonMakeGetStock(), "BooknowGetStock", GlobalText.POST);
            }
        });

        dialog2.show();

    }

    @OnClick(R.id.bottom_sheet_book_now)
    public void toggleBottomSheet1() {

    }

    @Override
    public void onResume() {
        super.onResume();
        // put your code here...
        Application.getInstance().trackScreenView(activity, GlobalText.LeadDetailsActivity);
    }

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {

        try {
            if (CommonMethods.getstringvaluefromkey(activity, "isLeadDetails").equalsIgnoreCase("true")) {
                MainActivity.getFragmentRefreshListener().onRefresh();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //  CommonMethods.setvalueAgainstKey(activity, "isBackLeadDetails", "true");
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            if (CommonMethods.getstringvaluefromkey(activity, "isLeadDetails").equalsIgnoreCase("true")) {
                MainActivity.getFragmentRefreshListener().onRefresh();
            }
            // CommonMethods.setvalueAgainstKey(activity, "isBackLeadDetails", "true");
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void GetMethodSalesExecutiveLoadData() {

        if (salesexecIdList != null) {
            salesexecIdList.clear();
        }

        if (salesexecList != null) {
            salesexecList.clear();
        }

        JsonArrayRequest strReq = new JsonArrayRequest(Request.Method.GET, Global.stock_dealerURL + "dealer/active-executive-list/sales", null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject data = response.getJSONObject(i);
                        salesexecIdList.add(data.getString("Value"));
                        salesexecList.add(data.getString("Text"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                MethodsalesexecIdListPopup(salesexecIdList, salesexecList);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json; charset=utf-8");
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));
                Log.e("token ", "requestMethods " + CommonMethods.getstringvaluefromkey(activity, "token").toString());

                return headers;
            }
        };
        Application.getInstance().addToRequestQueue(strReq);

    }

    public void MethodsalesexecIdListPopup(final ArrayList salesexecIdList, final ArrayList salesexecList) {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
        final Dialog dialog_data = new Dialog(activity, R.style.full_screen_dialog);

        dialog_data.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_data.getWindow().setGravity(Gravity.CENTER);

        dialog_data.setContentView(R.layout.citylist);

        TextView searchtittle = dialog_data.findViewById(R.id.searchtittle);
        TextView alertdialog_edittext = (EditText) dialog_data.findViewById(R.id.alertdialog_edittext);

        searchtittle.setText("Sales Executive List");
        alertdialog_edittext.setHint("Search Sales Executive");

        WindowManager.LayoutParams lp_number_picker = new WindowManager.LayoutParams();
        Window window = dialog_data.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);

        lp_number_picker.copyFrom(window.getAttributes());

        lp_number_picker.width = WindowManager.LayoutParams.FILL_PARENT;
        lp_number_picker.height = WindowManager.LayoutParams.FILL_PARENT;

        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp_number_picker);

        ImageView dialog_cancel_btn = dialog_data.findViewById(R.id.dialog_cancel_btn);
        dialog_cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog_data != null) {
                    dialog_data.dismiss();
                    // dialog_data.cancel();
                }

            }
        });

        ImageView backicon = dialog_data.findViewById(R.id.backicon);
        backicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_data.dismiss();
                //  dialog_data.cancel();
            }
        });

        EditText filterText = dialog_data.findViewById(R.id.alertdialog_edittext);
        ListView alertdialog_Listview = dialog_data.findViewById(R.id.alertdialog_Listview);
        alertdialog_Listview.setChoiceMode(ListView.GONE);
        alertdialog_Listview.setSelector(new ColorDrawable(0));
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, R.layout.simple_list_item_1, salesexecList);
        alertdialog_Listview.setAdapter(adapter);
        alertdialog_Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                sales_executive_name = a.getAdapter().getItem(position).toString();

                sales_executive_id = salesexecIdList.get(position).toString();

                TextView textview = v.findViewById(R.id.text1);

                //Set your Font Size Here.
                //textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                saleexetv.setText(sales_executive_name);

                if (dialog_data != null) {
                    dialog_data.dismiss();
                    // dialog_data.cancel();
                }
            }
        });

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s);
            }
        });


        dialog_data.show();
    }

    public JSONObject jsonMakeGetStock() {
        JSONObject jObj = new JSONObject();
        JsonArray jsonArray = new JsonArray();

        try {

            jObj.put("Page", "1");
            jObj.put("PageItems", "20");
            jObj.put("OrderBy", "stock_id");
            jObj.put("OrderByReverse", "true");
            jObj.put("where", jsonArray);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        //   Log.e("jsonMakeGetStock ", "post " + jObj.toString());
        return jObj;


    }

    private void leadFollowUpHistory(ReqLeadFollowUp mReqLeadFollowUp, Context mContext) {
        SpinnerManager.showSpinner(mContext);

        LeadFollowUpService.leadFollowUpFromServer(mReqLeadFollowUp, mContext, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                upleadstatus = spinSelectStatus.getSelectedItem().toString();
                upnextfollowdate = datefollup.getText().toString();
                upremark = edit_Remark.getText().toString();

                try {
                    retrofit2.Response<ResLeadFollowUp> mResponse = (retrofit2.Response<ResLeadFollowUp>) obj;

                    if (mResponse.body().getStatus().equalsIgnoreCase("success")) {

                        ResLeadFollowUp mResLeadFollowUp = mResponse.body();
                        Toast.makeText(activity, "Updated successfully", Toast.LENGTH_LONG).show();


                        //LeadDetailsFragment.viewPager.setCurrentItem(1);

                        //WebServicesCall.webCall(activity, activity, jsonMakegetLeadsWeb(), "WebLeadStore", GlobalText.POST);

                        //retrofit call webleadstore

                        if (CommonMethods.isInternetWorking(activity)) {
                            prepareWebLeadStore();
                        } else {
                            CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
                        }

                    } else if (mResponse.body().getStatus().equalsIgnoreCase("success")) {
                        WebServicesCall.error_popup2(activity, mResponse.body().getStatusCode(), "");
                    }
                } catch (Exception e) {

                }


            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
                Log.e(TAG, mThrowable.getMessage());
            }
        });
    }

    //private followup lead
    private void privateFollowUpLead(ReqPrivateLeadFollowUp mReqPrivateLeadFollowUp, Context mContext) {
        SpinnerManager.showSpinner(mContext);

        AddLeadService.privateFollowUpLeadFromServer(mReqPrivateLeadFollowUp, mContext, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                upleadstatus = spinSelectStatus.getSelectedItem().toString();
                upnextfollowdate = datefollup.getText().toString();
                upremark = edit_Remark.getText().toString();
                SpinnerManager.hideSpinner(mContext);
                try {
                    retrofit2.Response<ResPrivateLeadFollowUp> mResponse = (retrofit2.Response<ResPrivateLeadFollowUp>) obj;

                    if (mResponse.body().getStatus().equalsIgnoreCase("success")) {
                        Toast.makeText(activity, "Updated successfully", Toast.LENGTH_LONG).show();
                        // jsonMakePrivateLeadStore();

                        //WebServicesCall.webCall(activity, activity, jsonMakePrivateLeadStore(), "PrivateUpdateLeadStore", GlobalText.POST);

                        /*TODO Retrofit call*/
                        getPreparePrivateUpdateLeadStore();
                        //end
                        //LeadDetailsFragment.viewPager.setCurrentItem(1);
                        //FollowupHistoryFragment.reloadAPI();
                    }


                    Log.e(TAG, mResponse.body().getStatus());
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
                Log.e(TAG, mThrowable.getMessage());
            }
        });
    }

    private void prepareBooknowStock(StockResponse mStockResponse) {

        List<StockData> mList = mStockResponse.getData();
        ReqUpdateStock mReqUpdateStock = new ReqUpdateStock();
        mReqUpdateStock.setStockId(String.valueOf(mList.get(0).getStockId()));
        mReqUpdateStock.setStockSource(mList.get(0).getStockSource());
        mReqUpdateStock.setPostedDate(mList.get(0).getPostedDate());
        mReqUpdateStock.setVehicleMake(mList.get(0).getVehicleMake());
        mReqUpdateStock.setVehicleModel(mList.get(0).getVehicleModel());
        mReqUpdateStock.setVehicleVariant(mList.get(0).getVehicleVariant());
        mReqUpdateStock.setRegMonth("01");
        mReqUpdateStock.setRegYear(mList.get(0).getRegYear());
        mReqUpdateStock.setRegistraionCity(mList.get(0).getRegistraionCity());
        mReqUpdateStock.setRegistrationNumber(mList.get(0).getRegistrationNumber());
        mReqUpdateStock.setColour(mList.get(0).getColour());
        mReqUpdateStock.setKilometer(String.valueOf(mList.get(0).getKilometer()));
        mReqUpdateStock.setOwner(String.valueOf(mList.get(0).getOwner()));
        if (mList.get(0).getInsurance().trim().equalsIgnoreCase("")) {
            mReqUpdateStock.setInsurance("NA");
        } else {
            mReqUpdateStock.setInsurance(mList.get(0).getInsurance());

        }
        if (mList.get(0).getInsuranceExpDate().trim().equalsIgnoreCase("")) {
            mReqUpdateStock.setInsuranceExpDate("NA");
        } else {
            mReqUpdateStock.setInsuranceExpDate(mList.get(0).getInsuranceExpDate());
        }
        mReqUpdateStock.setSellingPrice(String.valueOf(mList.get(0).getSellingPrice()));
        mReqUpdateStock.setBoughtPrice(String.valueOf(mList.get(0).getBoughtPrice()));
        if (mList.get(0).getRefurbishmentCost() == null) {
            mReqUpdateStock.setRefurbishmentCost("0");
        } else {
            mReqUpdateStock.setRefurbishmentCost(String.valueOf(mList.get(0).getRefurbishmentCost()));

        }
        mReqUpdateStock.setProcurementExecutiveId(String.valueOf(mList.get(0).getProcurementExecutiveId()));
        mReqUpdateStock.setCngKit(mList.get(0).getCngKit());

        if (mList.get(0).getPrivateVehicle().equals("null")) {
            mReqUpdateStock.setPrivateVehicle(null);
        } else {
            mReqUpdateStock.setPrivateVehicle(mList.get(0).getPrivateVehicle());
        }
        mReqUpdateStock.setComments(mList.get(0).getComments());

        mReqUpdateStock.setManufactureMonth("01");

        mReqUpdateStock.setIsOffload(mList.get(0).getIsOffload());

        mReqUpdateStock.setManufactureYear(mList.get(0).getManufactureYear());
        mReqUpdateStock.setUpdatedByDevice("MOBILE");

        mReqUpdateStock.setIsFeaturedCar("false");
        mReqUpdateStock.setIsBooked("true");
        mReqUpdateStock.setActualSellingPrice(sellingpricetv.getText().toString());
        mReqUpdateStock.setSalesExecutiveId(sales_executive_id);
        mReqUpdateStock.setFinanceRequired(financereqtv.isChecked());

        mReqUpdateStock.setChassisNumber(mList.get(0).getChassisNumber());
        mReqUpdateStock.setEngineNumber(mList.get(0).getEngineNumber());
        mReqUpdateStock.setDealerPrice(String.valueOf(mList.get(0).getDealerPrice()));
        mReqUpdateStock.setDealerCode(mList.get(0).getDealerCode());

        //retofit call
        getBooknowStock(mReqUpdateStock, activity);

    }

    //call retrofit BooknowStock
    private void getBooknowStock(ReqUpdateStock mReqUpdateStock, Context mContext) {
        SpinnerManager.showSpinner(mContext);

        YearService.getUpdateStockFromServer(mReqUpdateStock, mContext, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                try {
                    retrofit2.Response<ResponseCreateStock> mRes = (retrofit2.Response<ResponseCreateStock>) obj;

                    UpdateFollowUpForStatusHistory();
                } catch (Exception e) {

                }
                SpinnerManager.hideSpinner(mContext);
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
            }
        });
    }

    //BooknowGetStock
    private void prepareBooknowGetStock() {

        List<Where> mList = new ArrayList<>();

        StockRequest mStockRequest = new StockRequest();
        mStockRequest.setPage("1");
        mStockRequest.setPageItems("20");
        mStockRequest.setOrderBy("stock_id");
        mStockRequest.setOrderByReverse("true");
        mStockRequest.setWhere(mList);
        getBooknowGetStock(LeadDetailsActivity.this, mStockRequest);
    }

    //retrofit
    private void getBooknowGetStock(Context mContext, StockRequest mStockRequest) {
        SpinnerManager.showSpinner(mContext);
        registnumberArrayList = new ArrayList<String>();
        StockService.fetchStock(mStockRequest, mContext, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                try {
                    retrofit2.Response<StockResponse> mRes = (retrofit2.Response<StockResponse>) obj;
                    List<StockData> mlist = mRes.body().getData();
                    for (int i = 0; i < mlist.size(); i++) {
                        registnumberArrayList.add(mlist.get(i).getRegistrationNumber());
                        // Log.e("registnumberArrayList1",registnumberArrayList.get(i));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                MethodStockRegisterNumberListPopup(registnumberArrayList);

                SpinnerManager.hideSpinner(mContext);
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
            }
        });

    }

    private void prepareWebLeadStore() {

        WebLeadReq1 mWebLeadReq1 = new WebLeadReq1();
        List<CustomWhere> customWhereList = new ArrayList<>();
        mWebLeadReq1.setFilterByFields("");
        mWebLeadReq1.setPerPage("1");
        mWebLeadReq1.setPage("1");
        mWebLeadReq1.setTag("android");
        mWebLeadReq1.setAliasFields("leads.created_at as PostedDate,leads.created_at as LeadDate,leads.id as id,dispatched.id as dispatchid,leads.customer_name as Name,leads.customer_mobile as Mobile,leads.customer_email as Email,leads.primary_make,leads.primary_model,leads.primary_variant,leads.secondary_make,leads.secondary_model,leads.secondary_variant,leads.register_no as regno,leads.register_month as regmonth,leads.register_year as regyear,leads.color,leads.kms,mfg_year,leads.owner as owners,leads.register_city,leads.stock_listed_price as price,dispatched.status,dispatched.followup_comments as remark,dispatched.followup_date as FollowUpDate,dispatched.employee_name as ExecutiveName");

        CustomWhere leads_id = new CustomWhere();
        leads_id.setColumn("leads.id");
        leads_id.setOperator("=");
        leads_id.setValue(Webleadid);
        customWhereList.add(leads_id);

        CustomWhere dispatched_target_id = new CustomWhere();
        dispatched_target_id.setColumn("dispatched.target_id");
        dispatched_target_id.setOperator("=");
        dispatched_target_id.setValue(CommonMethods.getstringvaluefromkey(activity, "dealer_code"));
        customWhereList.add(dispatched_target_id);

        CustomWhere dispatched_target_source = new CustomWhere();
        dispatched_target_source.setColumn("dispatched.target_source");
        dispatched_target_source.setOperator("=");
        dispatched_target_source.setValue("MFC");
        customWhereList.add(dispatched_target_source);

        List<CustomWhere> list = new ArrayList<>();
        mWebLeadReq1.setWherenotin(list);
        mWebLeadReq1.setCustomWhere(customWhereList);
        mWebLeadReq1.setAccessToken(CommonMethods.getstringvaluefromkey(activity, "access_token"));

        getWebLeadStore(LeadDetailsActivity.this, mWebLeadReq1);

    }

    //retrofit call
    private void getWebLeadStore(Context mContext, WebLeadReq1 mWebLeadReq1) {
        SpinnerManager.showSpinner(mContext);
        WebLeadsService.fetchWebLeads(mContext, mWebLeadReq1, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);


            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
            }
        });
    }

}
