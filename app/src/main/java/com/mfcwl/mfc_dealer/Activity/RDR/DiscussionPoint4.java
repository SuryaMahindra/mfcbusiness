package com.mfcwl.mfc_dealer.Activity.RDR;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.text.style.StrikethroughSpan;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GAConstants;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.DiscussionPoint;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.ScreenInformation;

import java.util.Arrays;

import imageeditor.base.BaseActivity;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.activity;

public class DiscussionPoint4 extends BaseActivity {

    TextView tvDateTime, tvDealerName, tvFocusArea, tvSubFA;
    ImageView createRepo_backbtn;
    TextView tvProcurementTarget;
    TextView tvLiveAuctions, tvActionsBidsOwn, tvAuctionsWon;
    //tvLeadsAsperOMS,tvLeadsActual,tvProcuredAsperOMS,tvProcuredActual,,etActualProcuredDP4,etActualLeadsDP4, strActualLeadsCTP
    TextView tvActionItems, tvResponsibilities, tvTargetDateCalendar,etAuctionsBidsOnDP4, tvAuctionsWonDP4;
    TextView tvPrevious, tvNext, tvFooterHeading;
    EditText etlowparticipationDP4;
    String strAuctionsBidsOnCTP, strAuctionsWonCTP,strLowparticipationCTP, strActualProcuredCTP;

    // R4

    EditText et_veh_not_asp_req_dp4;
    TextView tvNegotiationsValDP4,tvProcuredValDP4,tvParticipation_mtd_lbl,tvLive_auctions_lbl;
    String strVehNotAsPerReq="",strNegotiationsCPVal="",strProcuredCPVal="";

    private Activitykiller mKiller;
    private LinearLayout llresandtarget;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.discussion_point4);
        Log.i(TAG, "onCreate: ");

        initView();


        setListeners();

        mKiller = new Activitykiller();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(mKiller,intentFilter);
    }



    public void initView() {

        createRepo_backbtn = findViewById(R.id.createRepo_backbtn);

        tvDateTime = findViewById(R.id.tvDateTime);
        tvDealerName = findViewById(R.id.tvDealer);
        tvFocusArea = findViewById(R.id.tvFocusArea);
        tvSubFA = findViewById(R.id.tvSubFA);
        tvParticipation_mtd_lbl=findViewById(R.id.tvParticipation_mtd_lbl);
        tvProcurementTarget = findViewById(R.id.tvProcurementTarget);
        tvLiveAuctions = findViewById(R.id.tvLiveAuctions);

        tvActionsBidsOwn = findViewById(R.id.tvAuctionsBidsOn);
        etAuctionsBidsOnDP4= findViewById(R.id.etAuctionsBidsOnDP4);
        tvAuctionsWon = findViewById(R.id.tvAuctionsWon);
        tvAuctionsWonDP4 = findViewById(R.id.etAuctionsWonDP4);
        etlowparticipationDP4= findViewById(R.id.etlowparticipationDP4);
/*        tvLeadsAsperOMS = findViewById(R.id.tvLeadsAsPerOMS);
        tvLeadsActual = findViewById(R.id.tvLeadsAsPerOMSActual);
        etActualLeadsDP4= findViewById(R.id.etActualLeadsDP4);
        tvProcuredAsperOMS = findViewById(R.id.tvProcuredAsPerOMS);
        tvProcuredActual = findViewById(R.id.tvProcuredAsPerOMSActual);
        etActualProcuredDP4= findViewById(R.id.etActualProcuredDP4);*/
        tvActionItems = findViewById(R.id.tvActionItemList);
        tvResponsibilities = findViewById(R.id.tvResponsibilityList);
        tvTargetDateCalendar = findViewById(R.id.tvTargetDateCalendar);
        tvPrevious = findViewById(R.id.tvPrevious);
        tvNext = findViewById(R.id.tvNext);
        tvFooterHeading = findViewById(R.id.tvFooterHeading);
        llresandtarget = findViewById(R.id.llresandtarget);
        tvLive_auctions_lbl=findViewById(R.id.tvLive_auctions_lbl);
        tvActionItems.setMovementMethod(new ScrollingMovementMethod());
        tvResponsibilities.setMovementMethod(new ScrollingMovementMethod());

        // R4
        et_veh_not_asp_req_dp4=findViewById(R.id.et_veh_not_asp_req_dp4);
        tvNegotiationsValDP4=findViewById(R.id.tvNegotiationsValDP4);
        tvProcuredValDP4=findViewById(R.id.tvProcuredValDP4);

    }

    public void setListeners() {

        tvPrevious.setOnClickListener(this);
        tvNext.setOnClickListener(this);
        tvActionItems.setOnClickListener(this);
        tvResponsibilities.setOnClickListener(this);
        tvTargetDateCalendar.setOnClickListener(this);
        createRepo_backbtn.setOnClickListener(this);

        etAuctionsBidsOnDP4.setOnClickListener(this);
        tvAuctionsWonDP4.setOnClickListener(this);
        /*etActualLeadsDP4.setOnClickListener(this);*/
        /*etActualProcuredDP4.setOnClickListener(this);*/


        tvDateTime.setText(getDateTime());
        tvDealerName.setText(dealerName + "(" + dealerId + ")");
        tvFocusArea.setText("Procurement");
        tvSubFA.setText("Procurement through CPT");
        tvFooterHeading.setText("Discussion point 4");

        tvProcurementTarget.setText("Procurement Target: " + dealerReportResponse.procurementTargetStocks);

        if(dealerReportResponse.liveAuctionsCpt != null){
            tvLiveAuctions.setText(" " + dealerReportResponse.liveAuctionsCpt);

        }else {
            tvLiveAuctions.setText(" " + "0");

        }

            etAuctionsBidsOnDP4.setText(dealerReportResponse.auctionsBidsOnCpt);
            tvAuctionsWonDP4.setText(dealerReportResponse.auctionsBidsWonCpt);

            //R4
       /* if(dealerReportResponse.vehicleNotPerRequirementIep != null){
            et_veh_not_asp_req_dp4.setText(dealerReportResponse.vehicleNotPerRequirementIep.toString());

        }else {
            et_veh_not_asp_req_dp4.setText("");

        }*/
        if(dealerReportResponse.negotiationsCpt != null){
            tvNegotiationsValDP4.setText(dealerReportResponse.negotiationsCpt.toString());

        }else {
            tvNegotiationsValDP4.setText("0");
        }
        strNegotiationsCPVal=tvNegotiationsValDP4.getText().toString();

        if(dealerReportResponse.procuredActualCpt!=null)
        {
            tvProcuredValDP4.setText(dealerReportResponse.procuredActualCpt);

        }else
        {
            tvProcuredValDP4.setText("0");

        }
        strProcuredCPVal=tvProcuredValDP4.getText().toString();


        // tvAuctionsWon.setText("Auctions won: " + dealerReportResponse.auctionsBidsOnCpt);
/*
        tvLeadsAsperOMS.setText("Leads as per OMS: " + dealerReportResponse.leadsOmsCpt);
*/
       /* if(dealerReportResponse.leadsActualCpt != null){
            tvLeadsActual.setText("Leads (Actual): " + dealerReportResponse.leadsActualCpt);

        }else {
            tvLeadsActual.setText("Leads (Actual): " + "0");
        }*/

        /*tvProcuredAsperOMS.setText("Procured as per OMS: " + dealerReportResponse.procuredOmsCpt);*/
      /*  if(dealerReportResponse.procuredActualCpt != null){
            tvProcuredActual.setText("Procured (Actual): " + dealerReportResponse.procuredActualCpt);
        }else {
            tvProcuredActual.setText("Procured (Actual): " + "0");
        }*/


        tvActionItems.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String data = s.toString();
                if(data.equalsIgnoreCase("No action required")){
                    llresandtarget.setVisibility(View.GONE);
                    strResponsibility = "NA";
                    strTargetDate = "07/17/2020";
                    Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_ACTION_ITEM, CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));

                }else {
                    llresandtarget.setVisibility(View.VISIBLE);
                    strResponsibility = "";
                    strTargetDate = "";
                    Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_ACTION_ITEM,CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));
                }
            }
        });
    }

    public DiscussionPoint getDiscussionPoint() {
        DiscussionPoint discussionPoint = new DiscussionPoint();
        discussionPoint.setActionItem(Arrays.asList(strActionItem.split(("\\|"))));
        discussionPoint.setResponsibility(strResponsibility);
        discussionPoint.setTargetDate(strTargetDate);
        discussionPoint.setFocusArea(tvFocusArea.getText().toString());
        discussionPoint.setSubFocusArea(tvSubFA.getText().toString());

        ScreenInformation screenInformation = new ScreenInformation();


        screenInformation.setProcurementTargetCpt(dealerReportResponse.procurementTargetCpt);
        screenInformation.setLiveAuctionsCpt(dealerReportResponse.liveAuctionsCpt);
        screenInformation.setLeadsOmsCpt(dealerReportResponse.leadsOmsCpt);
        screenInformation.setProcuredOmsCpt(dealerReportResponse.procuredOmsCpt);
       // screenInformation.setProcuredActualCpt(dealerReportResponse.procuredActualCpt);


        screenInformation.setVehicleNotPerRequirementCpt(strVehNotAsPerReq);

        screenInformation.setAuctionsBidsOnCpt(strAuctionsBidsOnCTP);
        screenInformation.setAuctionsBidsWonCpt(strAuctionsWonCTP);
        screenInformation.setNegotiationsCpt(strNegotiationsCPVal);
        screenInformation.setReasonslowparticipationcpt(strLowparticipationCTP);
        screenInformation.setProcuredActualCpt(strProcuredCPVal);
        /*screenInformation.setLeadsActualCpt(strActualLeadsCTP);*/

        discussionPoint.setScreenInformation(screenInformation);

        return discussionPoint;
    }

    private boolean validate() {

        strActionItem = tvActionItems.getText().toString();
        if(!strActionItem.equalsIgnoreCase("No action required")){
            strResponsibility = tvResponsibilities.getText().toString();
            strTargetDate = tvTargetDateCalendar.getText().toString();
        }

        strAuctionsBidsOnCTP = etAuctionsBidsOnDP4.getText().toString();
        strAuctionsWonCTP = tvAuctionsWonDP4.getText().toString();
        strLowparticipationCTP = etlowparticipationDP4.getText().toString();
        /*strActualLeadsCTP = etActualLeadsDP4.getText().toString();*/

        strVehNotAsPerReq=et_veh_not_asp_req_dp4.getText().toString();

        /*strActualProcuredCTP = etActualProcuredDP4.getText().toString(); strActualLeadsCTP.equals("") ||
         */

        boolean allSelected = true;

        if (strActionItem.equals("") ||
                strResponsibility.equals("") ||
                strTargetDate.equals("") ||
                strAuctionsBidsOnCTP.equals("") ||
                strAuctionsWonCTP.equals("") ||
                strLowparticipationCTP.equals("")||
        strVehNotAsPerReq.equals("")|| strNegotiationsCPVal.equals("")||strProcuredCPVal.equals(""))
        {
            allSelected = false;
            Toast.makeText(this, "Please select all fields", Toast.LENGTH_SHORT).show();
        }

        return allSelected;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mKiller);
    }

    @Override
    public void onBackPressed() {

    }
    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.createRepo_backbtn:
                startActivity(new Intent(this, DiscussionPoint3.class));
                break;
            case R.id.tvPrevious:
                startActivity(new Intent(this,DiscussionPoint3.class));
                break;
            case R.id.tvNext:
                if (validate()) {
                    removeAnyDuplicates(tvSubFA.getText().toString());
                    discussionPoints.add(getDiscussionPoint());
                    startActivity(new Intent(this, DiscussionPoint5.class));
                }
                break;
            case R.id.tvActionItemList:
                if (dealerReportResponse != null && dealerReportResponse.procurementCPT != null) {
                    setDialogMultiSelectionCheck(R.id.tvActionItemList, "Action Items", getStringArray(dealerReportResponse.procurementCPT));
                }else {
                    Toast.makeText(DiscussionPoint4.this,"No data found",Toast.LENGTH_LONG).show();
                }
                break;
        }
    }



}