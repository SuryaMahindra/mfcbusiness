package com.mfcwl.mfc_dealer.Activity.Leads;

public class NotificationInstance {

    private static NotificationInstance mInstance = null;
    private String todayfollowdate;
    private String todayfollowdateprivate;

    private NotificationInstance(){
        todayfollowdate = "";

    }


    public static NotificationInstance getInstance(){
        if(mInstance == null)
        {
            mInstance = new NotificationInstance();
        }
        return mInstance;
    }


    public String getTodayfollowdate() {
        return todayfollowdate;
    }

    public void setTodayfollowdate(String todayfollowdate) {
        this.todayfollowdate = todayfollowdate;
    }

    public String getTodayfollowdateprivate() {
        return todayfollowdateprivate;
    }

    public void setTodayfollowdateprivate(String todayfollowdateprivate) {
        this.todayfollowdateprivate = todayfollowdateprivate;
    }
}
