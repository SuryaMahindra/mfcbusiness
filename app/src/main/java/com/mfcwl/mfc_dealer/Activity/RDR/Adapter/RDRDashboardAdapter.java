package com.mfcwl.mfc_dealer.Activity.RDR.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.mfcwl.mfc_dealer.Activity.RDR.Model.DashboardData;
import com.mfcwl.mfc_dealer.Activity.RDR.Model.RDRActionItemData;
import com.mfcwl.mfc_dealer.Activity.RDR.ViewDetailedListing;
import com.mfcwl.mfc_dealer.R;

import java.util.ArrayList;
import java.util.List;

public class RDRDashboardAdapter extends RecyclerView.Adapter<RDRDashboardAdapter.MyViewHolder> implements Filterable {

    private List<DashboardData> dataSet;
    private List<DashboardData> contactListFiltered;
    private Context mContext;

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    contactListFiltered = dataSet;
                } else {
                    List<DashboardData> filteredList = new ArrayList<>();
                    for (DashboardData row : dataSet) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.dealerName.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    contactListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = contactListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                contactListFiltered = (ArrayList<DashboardData>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView DealerName;
        TextView pendingcount;
        TextView completedcount;
        TextView overduecount;
        LinearLayout llViewDetailed;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.DealerName = (TextView) itemView.findViewById(R.id.tvdealerName);
            this.pendingcount = (TextView) itemView.findViewById(R.id.pendingcount);
            this.completedcount = (TextView) itemView.findViewById(R.id.completedcount);
            this.overduecount = (TextView) itemView.findViewById(R.id.overduecount);
            this.llViewDetailed = (LinearLayout) itemView.findViewById(R.id.llviewdetailed);
        }
    }

    public RDRDashboardAdapter(List<DashboardData> data, Context mContext) {
        this.dataSet = data;
        contactListFiltered = data;
        this.mContext = mContext;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rdrdashboard_item, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        DashboardData mData = contactListFiltered.get(listPosition);
        holder.DealerName.setText(mData.dealerName);
        holder.pendingcount.setText("" + mData.rdrPending);
        holder.completedcount.setText("" + mData.rdrComplete);
        holder.overduecount.setText("" + mData.rdrOverdue);

        holder.llViewDetailed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detailedIntent = new Intent(mContext, ViewDetailedListing.class);
                detailedIntent.putExtra("dcode", mData.dealerCode);
                mContext.startActivity(detailedIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return contactListFiltered.size();
    }
}
