package com.mfcwl.mfc_dealer.Activity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.ASMModel.signupRes;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.retrofitconfig.forgotService;

import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity {

    public Button sign_up_button;
    public ImageView BackNavigation;
    public EditText forgotpassword;
    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        Log.i(TAG, "onCreate: ");
        sign_up_button = (Button) findViewById(R.id.sign_up_button);
        BackNavigation = (ImageView) findViewById(R.id.BackNavigation);
        forgotpassword = (EditText) findViewById(R.id.forgotpassword);

        sign_up_button.setOnClickListener(v -> {

            signRequest();
        });
        BackNavigation.setOnClickListener(v -> {
            finish();
        });

    }

    private void signRequest() {

        if (forgotpassword.getText().toString().trim().equalsIgnoreCase("")) {
            emptyValuation(forgotpassword);
        } else {
            forgotReq(forgotpassword.getText().toString().trim());

        }
    }

    private void emptyValuation(EditText hint) {
        Toast.makeText(ForgotPasswordActivity.this, hint.getHint().toString(), Toast.LENGTH_LONG).show();
    }

    private void forgotReq(String name) {

        SpinnerManager.showSpinner(ForgotPasswordActivity.this);

        forgotService.getpassword(name, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                SpinnerManager.hideSpinner(ForgotPasswordActivity.this);
                Response<signupRes> mRes = (Response<signupRes>) obj;
                signupRes mData = mRes.body();

                String msg = mData.getMessage();

                if (mData.getStatus().equalsIgnoreCase("success")) {
                    CommonMethods.alertMessage2(ForgotPasswordActivity.this,msg);
                } else {
                    CommonMethods.alertMessage(ForgotPasswordActivity.this, msg);
                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(ForgotPasswordActivity.this);

                WebServicesCall.error_popup2(ForgotPasswordActivity.this, 500, "500 Internal Server Error.");

            }

        });
    }


    @Override
    public void onBackPressed() {
        finish();
    }
}
