package com.mfcwl.mfc_dealer.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.PayUPayment.PayUPaymentResponse;
import com.mfcwl.mfc_dealer.PayUPayment.PaymentRequest;
import com.mfcwl.mfc_dealer.Procurement.RetrofitConfig.ProcPrivateLeadService;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.Global;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.sdc.mfcpaymentgateway.MFCPayments;
import com.sdc.mfcpaymentgateway.PaymentConfig;
import com.sdc.mfcpaymentgateway.PaymentInput;
import com.sdc.mfcpaymentgateway.PaymentResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import retrofit2.Response;

public class OnlinePaymentActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private EditText et_price;
    private Button btn_makepayment;
    private Activity mActivity;
    private String warantybalance;
    private TextView tv_balance;

    String TAG = getClass().getSimpleName();

    @SuppressLint("ServiceCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_payment);
        Log.i(TAG, "onCreate: ");
        Intent intent = getIntent();
        if (intent != null) {
            this.warantybalance = intent.getStringExtra("warantybalance");
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        getSupportActionBar().setTitle("Online Payment");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        et_price = (EditText) findViewById(R.id.et_price);
        btn_makepayment = (Button) findViewById(R.id.btn_makepayment);
        tv_balance = (TextView) findViewById(R.id.tv_balance);
        tv_balance.setText(warantybalance);

        btn_makepayment.setOnClickListener(this);

        mActivity = this;

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(PaymentConfig.ACTION_PAYMENT);
        mActivity.registerReceiver(broadcastReceiver, intentFilter);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_makepayment:
                if (!et_price.getText().toString().trim().isEmpty() && Integer.parseInt(et_price.getText().toString().trim()) > 0) {
                    PaymentInput paymentinput = new PaymentInput();
                    paymentinput.setFirstname(CommonMethods.getstringvaluefromkey(this, "dealer_display_name"));
                    paymentinput.setLastaname("");
                    //paymentinput.setDealerid(CommonMethods.getstringvaluefromkey(this, "dealer_code"));
                    paymentinput.setEmailid(CommonMethods.getstringvaluefromkey(this, "dealer_email"));
                    paymentinput.setPhonenumber(CommonMethods.getstringvaluefromkey(this, "dealer_mobile"));
                    paymentinput.setState(CommonMethods.getstringvaluefromkey(this, "dealer_state"));
                    paymentinput.setCity(CommonMethods.getstringvaluefromkey(this, "dealer_city"));
                    paymentinput.setAddress(CommonMethods.getstringvaluefromkey(this, "dealer_address"));
                    paymentinput.setPincode(CommonMethods.getstringvaluefromkey(this, "dealer_pincode"));
                    paymentinput.setAmount(et_price.getText().toString().trim());
                    paymentinput.setTitle("Make Payment");
                    paymentinput.setProductinfo("MFC Business");
                    paymentinput.setMerchantkey(Global.merchantkey);
                    paymentinput.setSalt(Global.salt);
                    paymentinput.setPaymentbaseurl(Global.paymentbaseurl);
                    MFCPayments mfc = new MFCPayments();
                    mfc.makePayment(OnlinePaymentActivity.this, paymentinput);
                } else {
                    Toast.makeText(OnlinePaymentActivity.this, "Please enter warranty balance", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mActivity.unregisterReceiver(broadcastReceiver);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent != null) {
                String action = intent.getAction();
                if (action != null && action.equalsIgnoreCase(PaymentConfig.ACTION_PAYMENT)) {
                    PaymentResponse model = (PaymentResponse) intent.getSerializableExtra(PaymentConfig.PAYMENT_RESULT);
                    if (model.getStatus().equalsIgnoreCase("success")) {
                        if (CommonMethods.isInternetWorking(OnlinePaymentActivity.this)) {
                            PaymentRequest paymentRequest = new PaymentRequest();
                            paymentRequest.setCode(CommonMethods.getstringvaluefromkey(OnlinePaymentActivity.this, "dealer_code"));
                            paymentRequest.setAmount(Integer.parseInt(model.getAmount()));

                            paymentRequest.setPg_transaction_id(model.getTxnid());

                            //current date
                            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Kolkata"));
                            Date currentLocalTime = cal.getTime();
                            DateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            date.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                            String localTime = date.format(currentLocalTime);

                            paymentRequest.setPg_transaction_datetime_yyyymmddhhmmss(localTime);


                            CallingPayment(OnlinePaymentActivity.this, paymentRequest, model.getAmount(), model.getTxnid());
                        } else {
                            CommonMethods.alertMessage(OnlinePaymentActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
                        }
                    } else {
                        Toast.makeText(OnlinePaymentActivity.this, "Payment Failed", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    };

    private void CallingPayment(Activity activity, PaymentRequest request, String amount, String txnid) {
        SpinnerManager.showSpinner(activity);

        //request.setCode(null);

        ProcPrivateLeadService.CallingPayment(activity, request, amount, txnid, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(activity);
                Response<PayUPaymentResponse> mRes = (Response<PayUPaymentResponse>) obj;
                PayUPaymentResponse mData = mRes.body();

                //if (mData.getErrorStatusCode().equalsIgnoreCase("200")) {
                if (mRes.code() == 200) {
                    new AlertDialog.Builder(activity)
                            //.setTitle("Payment")
                            .setMessage("Your warranty balance has been updated successfully.")
                            .setCancelable(false)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    startActivity(new Intent(OnlinePaymentActivity.this, MainActivity.class));
                                    finish();
                                }
                            }).show();
                    CommonMethods.setvalueAgainstKey(activity, "amount", "");
                    CommonMethods.setvalueAgainstKey(activity, "txnid", "");
                } else {
                    String str = "";
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(mRes.errorBody().string());

                        String data = jsonObject.getString("Message");
                        Log.i(TAG, "OnSuccess: " + jsonObject);
                        Log.i(TAG, "data: " + data);

                        str = jsonObject.toString();
                        Log.i(TAG, "OnSuccess: " + str);

                    } catch (JSONException e) {
                        //Log.i(TAG, "JSONObject=: "+jsonObj.toString());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    CommonMethods.setvalueAgainstKey(activity, "amount", amount);
                    CommonMethods.setvalueAgainstKey(activity, "txnid", txnid);
                    payuAlert(OnlinePaymentActivity.this, mRes.code(), request, amount, txnid, str);
                    // callingfailre(activity, amount, txnid);
                }
            }

            @Override
            public void OnFailure(Throwable t) {
                CommonMethods.setvalueAgainstKey(activity, "amount", amount);
                CommonMethods.setvalueAgainstKey(activity, "txnid", txnid);
                SpinnerManager.hideSpinner(activity);
                t.printStackTrace();
                payuAlert(OnlinePaymentActivity.this, 500, request, amount, txnid, t.toString());
            }
        });
    }

    public void payuAlert(OnlinePaymentActivity context, int errorcode, PaymentRequest request, String amount, String txnid, String serverData) {

        final Dialog dialog = new Dialog(OnlinePaymentActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.common_alert);
        //dialog.setTitle("Custom Dialog");
        dialog.setCancelable(false);

        TextView Message, Message2, Confirm;
        Message = dialog.findViewById(R.id.Message);
        Message.setTypeface(Message.getTypeface(), Typeface.BOLD_ITALIC);

        Message2 = dialog.findViewById(R.id.Message2);
        Confirm = dialog.findViewById(R.id.Confirm);
        Confirm.setText("Send");
        Message.setText("Something went wrong !");
        Message2.setText("Click on the button below to share error details with administrator");

        Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendEmail(OnlinePaymentActivity.this, errorcode, request, amount, txnid, serverData);
                dialog.dismiss();

            }
        });


        dialog.show();
    }

    public static void sendEmail(OnlinePaymentActivity context, int errorcode, PaymentRequest request, String amount, String txnid, String serverData) {

        String[] TO = {"vijay.sukriti@mahindra.com","pandya.mukul@mahindra.com"};
        String[] CC = {"vijay.sukriti@mahindra.com"};

 /*       String[] TO = {"p.vellaichamy@mahindra.com"};
        */

        Intent i = new Intent(Intent.ACTION_SENDTO);
        i.setData(Uri.parse("mailto:"));
        i.putExtra(Intent.EXTRA_EMAIL, TO);
        i.putExtra(Intent.EXTRA_CC, CC);
        i.putExtra(Intent.EXTRA_SUBJECT, "Payment Details update Failure");
        i.putExtra(Intent.EXTRA_TEXT,

                "Dealer Code :" + request.getCode() + "\n" +
                        "Amount :" + amount + "\n" +
                        "Transaction id :" + txnid + "\n" +
                        "Transaction Time :" + request.getPg_transaction_datetime_yyyymmddhhmmss() + "\n" +
                        "Error Code :" + errorcode + "\n" +
                        "Error Message :" + serverData);
        if (isPackageInstalled(context, "com.google.android.gm")) {
            i.setPackage("com.google.android.gm");
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        } else {
            try {
                context.startActivity(Intent.createChooser(i, "Send mail..."));
            } catch (ActivityNotFoundException e) {
                Toast.makeText(context, "No email app is installed on your device...", Toast.LENGTH_SHORT).show();
            }
        }

        // close
       // context.finish();
    }

    public static boolean isPackageInstalled(@NonNull Context context, @NonNull String packageName) {
        PackageManager pm = context.getPackageManager();
        if (pm != null) {
            try {
                pm.getPackageInfo(packageName, 0);
                return true;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    private void callingfailre(Activity activity, String amount, String txnid) {
        CommonMethods.setvalueAgainstKey(activity, "amount", amount);
        CommonMethods.setvalueAgainstKey(activity, "txnid", txnid);
        startActivity(new Intent(OnlinePaymentActivity.this, MainActivity.class));
        finish();
    }

}
