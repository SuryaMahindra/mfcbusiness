package com.mfcwl.mfc_dealer.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.payAttachModel;
import com.mfcwl.mfc_dealer.ASMModel.paymentDetails;
import com.mfcwl.mfc_dealer.AddStockModel.paymentdetailsRes;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.RetrofitServices.Paymentservice;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.Global;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.encrypt.AWSSecretsManager;
import com.mfcwl.mfc_dealer.encrypt.s3Res;
import com.mfcwl.mfc_dealer.retrofitconfig.S3DetailsServices;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import Listners.ImageUploadListner;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Utility.GlobalText.BUCKET_KEY;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.BUCKET_SECRET;

public class OfflinePaymentDetails extends AppCompatActivity implements ImageUploadListner {

    Unbinder unbinder;

    @BindView(R.id.royalty)
    TextView royalty;
    @BindView(R.id.royaltyDetails)
    LinearLayout royaltyDetails;
    @BindView(R.id.variable)
    TextView variable;
    @BindView(R.id.variableDetails)
    LinearLayout variableDetails;
    @BindView(R.id.brandingDetails)
    LinearLayout brandingDetails;
    @BindView(R.id.oms)
    TextView oms;

    @BindView(R.id.attach1)
    TextView attach1;
    @BindView(R.id.attach2)
    TextView attach2;
    @BindView(R.id.attach3)
    TextView attach3;
    @BindView(R.id.attach4)
    TextView attach4;

    @BindView(R.id.close1)
    ImageView close1;
    @BindView(R.id.close2)
    ImageView close2;
    @BindView(R.id.close3)
    ImageView close3;
    @BindView(R.id.close4)
    ImageView close4;

    @BindView(R.id.omsDetails)
    LinearLayout omsDetails;
    @BindView(R.id.footer)
    TextView footer;

    @BindView(R.id.royaltyDate)
    TextView royaltyDate;
    @BindView(R.id.royaltyModeOfPayment)
    TextView royaltyModeOfPayment;
    @BindView(R.id.royaltyAmountPaid)
    EditText royaltyAmountPaid;
    @BindView(R.id.royaltyTDSDeducted)
    EditText royaltyTDSDeducted;
    @BindView(R.id.royaltyRefereneNumber)
    EditText royaltyRefereneNumber;
    @BindView(R.id.royaltyBankName)
    EditText royaltyBankName;

    @BindView(R.id.variableDate)
    TextView variableDate;
    @BindView(R.id.variableModeOfPayment)
    TextView variableModeOfPayment;
    @BindView(R.id.variableAmountPaid)
    EditText variableAmountPaid;
    @BindView(R.id.variableTaxDeducted)
    EditText variableTaxDeducted;
    @BindView(R.id.variableReferenceNumber)
    EditText variableReferenceNumber;
    @BindView(R.id.variableBankName)
    EditText variableBankName;

    @BindView(R.id.brandingDate)
    TextView brandingDate;
    @BindView(R.id.brandinModeOfPayment)
    TextView brandinModeOfPayment;
    @BindView(R.id.brandingAmountPaid)
    EditText brandingAmountPaid;
    @BindView(R.id.brandingTaxDeducted)
    EditText brandingTaxDeducted;
    @BindView(R.id.brandingReferenceNumber)
    EditText brandingReferenceNumber;
    @BindView(R.id.brandingBankName)
    EditText brandingBankName;

    @BindView(R.id.omsDate)
    TextView omsDate;
    @BindView(R.id.omsModeOfPayment)
    TextView omsModeOfPayment;
    @BindView(R.id.omsAmountPaid)
    EditText omsAmountPaid;
    @BindView(R.id.omsTaxDeducted)
    EditText omsTaxDeducted;
    @BindView(R.id.omsReferenceNumber)
    EditText omsReferenceNumber;
    @BindView(R.id.omsBankName)
    EditText omsBankName;


    @BindView(R.id.branding)
    TextView branding;

    @BindView(R.id.royaltyClear)
    TextView royaltyClear;

    @BindView(R.id.varClear)
    TextView varClear;

    @BindView(R.id.brandClear)
    TextView brandClear;

    @BindView(R.id.omsClear)
    TextView omsClear;

    @BindView(R.id.addplus)
    ImageView addplus;

    @BindView(R.id.attach1_ll)
    LinearLayout attach1_ll;
    @BindView(R.id.attach2_ll)
    LinearLayout attach2_ll;
    @BindView(R.id.attach3_ll)
    LinearLayout attach3_ll;
    @BindView(R.id.attach4_ll)
    LinearLayout attach4_ll;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.pay_back)
    ImageView pay_back;

    @BindView(R.id.var_autoSwitch)
    Switch var_autoSwitch;
    @BindView(R.id.brand_autoSwitch)
    Switch brand_autoSwitch;
    @BindView(R.id.oms_autoSwitch)
    Switch oms_autoSwitch;
    @BindView(R.id.royal_autoSwitch)
    Switch royal_autoSwitch;

    public String TAG = getClass().getSimpleName();

    File file;
    Uri fileUri;
    private static int RESULT_LOAD = 1;
    public String path = "";
    public String image_url1 = "";
    private int mYear, mMonth, mDay;
    public PAdapter pAdapter;
    private List<payAttachModel> pList;
public boolean allValues=false;
    String[] autolist;
    JSONObject apiParamsJSON;
    ArrayList list;

    String[] paylist = {"Cheque", "RTGS", "IMPS", "NEFT", "CASH", "PDC", "ACH", "PTM"};
    public String imageName = "test";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offline_payment_details);
        Log.i(TAG, "onCreate: ");
        unbinder = ButterKnife.bind(this);
        pList = new ArrayList<>();
        list = new ArrayList<>();
        apiParamsJSON = new JSONObject();

        sendS3Details(this);


        royalty.setOnClickListener(v -> {

            if (royaltyDetails.getVisibility() == View.VISIBLE)
                royaltyDetails.setVisibility(View.GONE);
            else
                // royaltyDetails.setVisibility(View.VISIBLE);
                closeAll(royaltyDetails, variableDetails, brandingDetails, omsDetails);
            setDataVariables();
            cursorClear(royaltyAmountPaid, royaltyTDSDeducted, royaltyRefereneNumber, royaltyBankName);

        });

        variable.setOnClickListener(v -> {

            if (variableDetails.getVisibility() == View.VISIBLE)
                variableDetails.setVisibility(View.GONE);
            else
                // variableDetails.setVisibility(View.VISIBLE);
                closeAll(variableDetails, royaltyDetails, brandingDetails, omsDetails);
            setDataVariables();
            cursorClear(variableAmountPaid, variableTaxDeducted, variableReferenceNumber, variableBankName);
        });
        branding.setOnClickListener(v -> {

            if (brandingDetails.getVisibility() == View.VISIBLE)
                brandingDetails.setVisibility(View.GONE);
            else
                //  brandingDetails.setVisibility(View.VISIBLE);
                closeAll(brandingDetails, royaltyDetails, variableDetails, omsDetails);
            setDataVariables();
            cursorClear(brandingAmountPaid, brandingTaxDeducted, brandingReferenceNumber, brandingBankName);
        });
        oms.setOnClickListener(v -> {

            if (omsDetails.getVisibility() == View.VISIBLE)
                omsDetails.setVisibility(View.GONE);
            else
                // omsDetails.setVisibility(View.VISIBLE);
                closeAll(omsDetails, royaltyDetails, variableDetails, brandingDetails);
            setDataVariables();
            cursorClear(omsAmountPaid, omsTaxDeducted, omsReferenceNumber, omsBankName);

        });

        footer.setOnClickListener(v -> {


            try {


                if (CommonMethods.isInternetWorking(OfflinePaymentDetails.this)) {
                    if (emptyCheck()) {

                        //check all values
                        emptyCheckALL();
                        if(!allValues){
                            return;
                        }

                        if (pList.size() != 0) {
                            payRequestDetails();
                        } else {
                            Toast.makeText(OfflinePaymentDetails.this, "Please attach minimum 1 reference", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(OfflinePaymentDetails.this, "please fill  data", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    CommonMethods.alertMessage(OfflinePaymentDetails.this, GlobalText.CHECK_NETWORK_CONNECTION);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        });

        addplus.setOnClickListener(v -> {
            addbutton();
        });

        royaltyDate.setOnClickListener(v -> {
            dateCalenderShow(royaltyDate);
        });

        variableDate.setOnClickListener(v -> {
            dateCalenderShow(variableDate);
        });

        brandingDate.setOnClickListener(v -> {
            dateCalenderShow(brandingDate);
        });

        omsDate.setOnClickListener(v -> {
            dateCalenderShow(omsDate);
        });

        royaltyClear.setOnClickListener(v -> {
            setValueOff(royal_autoSwitch, royaltyDate, royaltyModeOfPayment, royaltyAmountPaid, royaltyTDSDeducted, royaltyRefereneNumber, royaltyBankName);
        });

        varClear.setOnClickListener(v -> {
            setValueOff(var_autoSwitch, variableDate, variableModeOfPayment, variableAmountPaid, variableTaxDeducted, variableReferenceNumber, variableBankName);

        });

        brandClear.setOnClickListener(v -> {
            setValueOff(brand_autoSwitch, brandingDate, brandinModeOfPayment, brandingAmountPaid, brandingTaxDeducted, brandingReferenceNumber, brandingBankName);

        });

        omsClear.setOnClickListener(v -> {
            setValueOff(oms_autoSwitch, omsDate, omsModeOfPayment, omsAmountPaid, omsTaxDeducted, omsReferenceNumber, omsBankName);

        });

        pay_back.setOnClickListener(v -> {
            startActivity(new Intent(OfflinePaymentDetails.this, PaymentActivity.class));
            finish();
        });

        royaltyModeOfPayment.setOnClickListener(v -> {
            setAlertDialog(v, OfflinePaymentDetails.this, GlobalText.paymentModeTitle, paylist, royaltyModeOfPayment);
        });
        variableModeOfPayment.setOnClickListener(v -> {
            setAlertDialog(v, OfflinePaymentDetails.this, GlobalText.paymentModeTitle, paylist, variableModeOfPayment);
        });
        brandinModeOfPayment.setOnClickListener(v -> {
            setAlertDialog(v, OfflinePaymentDetails.this, GlobalText.paymentModeTitle, paylist, brandinModeOfPayment);
        });
        omsModeOfPayment.setOnClickListener(v -> {
            setAlertDialog(v, OfflinePaymentDetails.this, GlobalText.paymentModeTitle, paylist, omsModeOfPayment);
        });

        royal_autoSwitch.setOnClickListener(v -> {

            if (royal_autoSwitch.isChecked()) {

                setDataVariables();

                setAlertDialogPreFill(royal_autoSwitch, v, OfflinePaymentDetails.this, GlobalText.PrefillTitle, autolist, "royal");
            }
        });

        var_autoSwitch.setOnClickListener(v -> {

            if (var_autoSwitch.isChecked()) {

                setDataVariables();

                setAlertDialogPreFill(var_autoSwitch, v, OfflinePaymentDetails.this, GlobalText.PrefillTitle, autolist, "variable");

            }
        });

        brand_autoSwitch.setOnClickListener(v -> {

            if (brand_autoSwitch.isChecked()) {
                setDataVariables();
                setAlertDialogPreFill(brand_autoSwitch, v, OfflinePaymentDetails.this, GlobalText.PrefillTitle, autolist, "brand");

            }

        });

        oms_autoSwitch.setOnClickListener(v -> {

            if (oms_autoSwitch.isChecked()) {
                setDataVariables();
                setAlertDialogPreFill(oms_autoSwitch, v, OfflinePaymentDetails.this, GlobalText.PrefillTitle, autolist, "oms");
            }
        });

    }

    private void closeAll(LinearLayout royaltyDetails, LinearLayout variableDetails,
                          LinearLayout brandingDetails, LinearLayout omsDetails) {


        royaltyDetails.setVisibility(View.GONE);
        variableDetails.setVisibility(View.GONE);
        brandingDetails.setVisibility(View.GONE);
        omsDetails.setVisibility(View.GONE);
        royaltyDetails.setVisibility(View.VISIBLE);


        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public void cursorClear(EditText AmountPaid, EditText TDSDeducted, EditText RefereneNumber, EditText BankName) {

        AmountPaid.clearFocus();
        TDSDeducted.clearFocus();
        RefereneNumber.clearFocus();
        BankName.clearFocus();

    }

    private void payRequestDetails() {

        String image_url = "";
        ArrayList<paymentDetails> reqData = new ArrayList<>();

        try {
            try {

                if (pList.size() > 0) {

                    String[] stringArray = new String[pList.size()];
                    for (int i = 0; i < pList.size(); i++) {
                        stringArray[i] = pList.get(i).getUrl();
                    }

                    String str = TextUtils.join(",", stringArray);
                    image_url = str;
                    Log.i(TAG, "payRequestDetails: " + str);
                }

                Log.i(TAG, "payRequestDetails: " + image_url);

            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!royaltyDate.getText().toString().trim().isEmpty()) {

                paymentDetails req = new paymentDetails();
                req.setDealerCode(CommonMethods.getstringvaluefromkey(OfflinePaymentDetails.this, "dealer_code"));
                req.setTypeOfFee("Royalty");
                req.setTransactionDate(royaltyDate.getText().toString());
                req.setModeOfPayment(royaltyModeOfPayment.getText().toString());
                req.setAmountPaid(royaltyAmountPaid.getText().toString());
                req.setTDS(royaltyTDSDeducted.getText().toString());
                req.setRefNo(royaltyRefereneNumber.getText().toString());
                req.setBank(royaltyBankName.getText().toString());
                req.setAttachments(image_url);
                reqData.add(req);
            }
            if (!variableDate.getText().toString().trim().isEmpty()) {

                paymentDetails req = new paymentDetails();
                req.setDealerCode(CommonMethods.getstringvaluefromkey(OfflinePaymentDetails.this, "dealer_code"));
                req.setTypeOfFee("Variable");
                req.setTransactionDate(variableDate.getText().toString());
                req.setModeOfPayment(variableModeOfPayment.getText().toString());
                req.setAmountPaid(variableAmountPaid.getText().toString());
                req.setTDS(variableTaxDeducted.getText().toString());
                req.setRefNo(variableReferenceNumber.getText().toString());
                req.setBank(variableBankName.getText().toString());
                req.setAttachments(image_url);
                reqData.add(req);
            }

            if (!brandingDate.getText().toString().trim().isEmpty()) {

                paymentDetails req = new paymentDetails();
                req.setDealerCode(CommonMethods.getstringvaluefromkey(OfflinePaymentDetails.this, "dealer_code"));
                req.setTypeOfFee("Branding");
                req.setTransactionDate(brandingDate.getText().toString());
                req.setModeOfPayment(brandinModeOfPayment.getText().toString());
                req.setAmountPaid(brandingAmountPaid.getText().toString());
                req.setTDS(brandingTaxDeducted.getText().toString());
                req.setRefNo(brandingReferenceNumber.getText().toString());
                req.setBank(brandingBankName.getText().toString());
                req.setAttachments(image_url);
                reqData.add(req);
            }

            if (!omsDate.getText().toString().trim().isEmpty()) {

                paymentDetails req = new paymentDetails();
                req.setDealerCode(CommonMethods.getstringvaluefromkey(OfflinePaymentDetails.this, "dealer_code"));
                req.setTypeOfFee("Oms");
                req.setTransactionDate(omsDate.getText().toString());
                req.setModeOfPayment(omsModeOfPayment.getText().toString());
                req.setAmountPaid(omsAmountPaid.getText().toString());
                req.setTDS(omsTaxDeducted.getText().toString());
                req.setRefNo(omsReferenceNumber.getText().toString());
                req.setBank(omsBankName.getText().toString());
                req.setAttachments(image_url);
                reqData.add(req);
            }

            sendPayDetails(OfflinePaymentDetails.this, reqData);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void setValueOff(Switch autoSwitch, TextView Date, TextView ModeOfPayment, EditText AmountPaid,
                             EditText TaxDeducted, EditText ReferenceNumber, EditText BankName) {
        Date.setText("");
        ModeOfPayment.setText("");
        AmountPaid.setText("");
        TaxDeducted.setText("");
        ReferenceNumber.setText("");
        BankName.setText("");
        autoSwitch.setChecked(false);

        AmountPaid.clearFocus();
        TaxDeducted.clearFocus();
        ReferenceNumber.clearFocus();
        BankName.clearFocus();

        setDataVariables();

    }

    private void setDataVariables() {

        try {

            int i = 0;
            list.clear();

            if (!royaltyDate.getText().toString().isEmpty()
                    && !royaltyModeOfPayment.getText().toString().isEmpty()
                    && !royaltyAmountPaid.getText().toString().isEmpty()
                    && !royaltyTDSDeducted.getText().toString().isEmpty()
                    && !royaltyRefereneNumber.getText().toString().isEmpty()
                    && !royaltyBankName.getText().toString().isEmpty()
            ) {

                list.add(i, "Royalty");

                i = i + 1;

                royal_autoSwitch.setVisibility(View.GONE);
            }
            if (!variableDate.getText().toString().isEmpty()
                    && !variableModeOfPayment.getText().toString().isEmpty()
                    && !variableAmountPaid.getText().toString().isEmpty()
                    && !variableTaxDeducted.getText().toString().isEmpty()
                    && !variableReferenceNumber.getText().toString().isEmpty()
                    && !variableBankName.getText().toString().isEmpty()
            ) {
                list.add(i, "Variable");
                i = i + 1;

                var_autoSwitch.setVisibility(View.GONE);
            }

            if (!brandingDate.getText().toString().isEmpty()
                    && !brandinModeOfPayment.getText().toString().isEmpty()
                    && !brandingAmountPaid.getText().toString().isEmpty()
                    && !brandingTaxDeducted.getText().toString().isEmpty()
                    && !brandingReferenceNumber.getText().toString().isEmpty()
                    && !brandingBankName.getText().toString().isEmpty()
            ) {

                list.add(i, "Branding");
                i = i + 1;

                brand_autoSwitch.setVisibility(View.GONE);
            }

            if (!omsDate.getText().toString().isEmpty()
                    && !omsModeOfPayment.getText().toString().isEmpty()
                    && !omsAmountPaid.getText().toString().isEmpty()
                    && !omsTaxDeducted.getText().toString().isEmpty()
                    && !omsReferenceNumber.getText().toString().isEmpty()
                    && !omsBankName.getText().toString().isEmpty()
            ) {

                list.add(i, "Oms");
                i = i + 1;

                oms_autoSwitch.setVisibility(View.GONE);
            }

            if (i == 0) {
                var_autoSwitch.setVisibility(View.GONE);
                brand_autoSwitch.setVisibility(View.GONE);
                oms_autoSwitch.setVisibility(View.GONE);
                royal_autoSwitch.setVisibility(View.GONE);

            } else {
                autolist = (String[]) list.toArray(new String[list.size()]);

                if (!royaltyDate.getText().toString().isEmpty()
                        && !royaltyModeOfPayment.getText().toString().isEmpty()
                        && !royaltyAmountPaid.getText().toString().isEmpty()
                        && !royaltyTDSDeducted.getText().toString().isEmpty()
                        && !royaltyRefereneNumber.getText().toString().isEmpty()
                        && !royaltyBankName.getText().toString().isEmpty()
                ) {

                } else {
                    royal_autoSwitch.setVisibility(View.VISIBLE);
                }
                if (!variableDate.getText().toString().isEmpty()
                        && !variableModeOfPayment.getText().toString().isEmpty()
                        && !variableAmountPaid.getText().toString().isEmpty()
                        && !variableTaxDeducted.getText().toString().isEmpty()
                        && !variableReferenceNumber.getText().toString().isEmpty()
                        && !variableBankName.getText().toString().isEmpty()
                ) {

                } else {
                    var_autoSwitch.setVisibility(View.VISIBLE);
                }

                if (!brandingDate.getText().toString().isEmpty()
                        && !brandinModeOfPayment.getText().toString().isEmpty()
                        && !brandingAmountPaid.getText().toString().isEmpty()
                        && !brandingTaxDeducted.getText().toString().isEmpty()
                        && !brandingReferenceNumber.getText().toString().isEmpty()
                        && !brandingBankName.getText().toString().isEmpty()
                ) {
                } else {
                    brand_autoSwitch.setVisibility(View.VISIBLE);
                }

                if (!omsDate.getText().toString().isEmpty()
                        && !omsModeOfPayment.getText().toString().isEmpty()
                        && !omsAmountPaid.getText().toString().isEmpty()
                        && !omsTaxDeducted.getText().toString().isEmpty()
                        && !omsReferenceNumber.getText().toString().isEmpty()
                        && !omsBankName.getText().toString().isEmpty()
                ) {

                } else {
                    oms_autoSwitch.setVisibility(View.VISIBLE);
                }

            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void setAlertDialog(View v, Activity a, final String strTitle, final String[] arrVal, TextView text) {
        AlertDialog.Builder alert = new AlertDialog.Builder(a);
        alert.setTitle(strTitle);

        alert.setItems(arrVal, (dialog, which) -> text.setText(arrVal[which]));
        alert.create();
        alert.show();
    }

    public void setAlertDialogPreFill(Switch Switch, View v, Activity a, final String strTitle, final String[] arrVal, String status) {
        AlertDialog.Builder alert = new AlertDialog.Builder(a);
        alert.setTitle(strTitle);

        alert.setItems(arrVal, (dialog, which) -> {

            if (status.equalsIgnoreCase("royal")) {

                getTextValues("royal", arrVal[which]);

            } else if (status.equalsIgnoreCase("variable")) {

                getTextValues("variable", arrVal[which]);
            } else if (status.equalsIgnoreCase("brand")) {

                getTextValues("brand", arrVal[which]);
            } else if (status.equalsIgnoreCase("oms")) {

                getTextValues("oms", arrVal[which]);
            }


        });

        alert.setPositiveButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Switch.setChecked(false);
                    }
                });

        alert.create();
        alert.show();
    }


    private void getTextValues(String status, String status2) {

        switch (status) {

            case "royal":

                setTextValues(status2, royaltyDate, royaltyModeOfPayment, royaltyAmountPaid, royaltyTDSDeducted,
                        royaltyRefereneNumber, royaltyBankName);
                royal_autoSwitch.setVisibility(View.GONE);

                break;
            case "variable":

                setTextValues(status2, variableDate, variableModeOfPayment, variableAmountPaid, variableTaxDeducted,
                        variableReferenceNumber, variableBankName);
                var_autoSwitch.setVisibility(View.GONE);
                break;
            case "brand":

                setTextValues(status2, brandingDate, brandinModeOfPayment, brandingAmountPaid, brandingTaxDeducted,
                        brandingReferenceNumber, brandingBankName);
                brand_autoSwitch.setVisibility(View.GONE);

                break;
            case "oms":

                setTextValues(status2, omsDate, omsModeOfPayment, omsAmountPaid, omsTaxDeducted,
                        omsReferenceNumber, omsBankName);
                oms_autoSwitch.setVisibility(View.GONE);

                break;

        }

    }


    private void setTextValues(String status, TextView Date, TextView ModeOfPayment, TextView AmountPaid,
                               EditText TaxDeducted, EditText ReferenceNumber, EditText BankName) {

        switch (status) {

            case "Royalty":

                Date.setText(royaltyDate.getText().toString());
                ModeOfPayment.setText(royaltyModeOfPayment.getText().toString());
                AmountPaid.setText(royaltyAmountPaid.getText().toString());
                TaxDeducted.setText(royaltyTDSDeducted.getText().toString());
                ReferenceNumber.setText(royaltyTDSDeducted.getText().toString());
                BankName.setText(royaltyBankName.getText().toString());

                break;

            case "Variable":

                Date.setText(variableDate.getText().toString());
                ModeOfPayment.setText(variableModeOfPayment.getText().toString());
                AmountPaid.setText(variableAmountPaid.getText().toString());
                TaxDeducted.setText(variableTaxDeducted.getText().toString());
                ReferenceNumber.setText(variableReferenceNumber.getText().toString());
                BankName.setText(variableBankName.getText().toString());

                break;

            case "Branding":

                Date.setText(brandingDate.getText().toString());
                ModeOfPayment.setText(brandinModeOfPayment.getText().toString());
                AmountPaid.setText(brandingAmountPaid.getText().toString());
                TaxDeducted.setText(brandingTaxDeducted.getText().toString());
                ReferenceNumber.setText(brandingReferenceNumber.getText().toString());
                BankName.setText(brandingBankName.getText().toString());

                break;

            case "Oms":

                Date.setText(omsDate.getText().toString());
                ModeOfPayment.setText(omsModeOfPayment.getText().toString());
                AmountPaid.setText(omsAmountPaid.getText().toString());
                TaxDeducted.setText(omsTaxDeducted.getText().toString());
                ReferenceNumber.setText(omsReferenceNumber.getText().toString());
                BankName.setText(omsBankName.getText().toString());

                break;
        }
    }

    private void dateCalenderShow(TextView text) {

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear += 1;
                String mt, dy;   //local variable
                if (monthOfYear < 10)
                    mt = "0" + monthOfYear; //if month less than 10 then ad 0 before month
                else mt = String.valueOf(monthOfYear);

                if (dayOfMonth < 10)
                    dy = "0" + dayOfMonth;
                else dy = String.valueOf(dayOfMonth);

                text.setText(year + "-" + (mt) + "-" + dy);

            }
        }, mYear, mMonth, mDay);

        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());

        datePickerDialog.show();
    }

    private void addbutton() {
        openGallary();
    }

    private void attachtoAdapter(List<payAttachModel> mlist) {

        pAdapter = new PAdapter(this, this, pList);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLinearLayoutManager);
        recyclerView.setAdapter(pAdapter);

    }


    public class PAdapter extends RecyclerView.Adapter<PAdapter.MyViewHolder> implements View.OnClickListener {

        private List<payAttachModel> messageList;
        LayoutInflater layoutInflater;
        public Context mContext;
        Activity activity;
        int i;

        public int counts = 0;
        public String TAG = getClass().getSimpleName();

        public PAdapter(Context mContext, Activity act, List<payAttachModel> procList) {
            this.mContext = mContext;
            this.messageList = procList;
            this.activity = act;
            layoutInflater = LayoutInflater.from(mContext);

        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView attach1;
            public ImageView close1, close_min;

            public MyViewHolder(View view) {
                super(view);
                attach1 = (TextView) view.findViewById(R.id.attach1);
                close1 = (ImageView) view.findViewById(R.id.close1);
            }
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = layoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_pay_attach, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

            }
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            i = position;

            final payAttachModel Data = messageList.get(position);

            try {

                holder.attach1.setText(Data.getName());

                holder.close1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        /*messageList.remove(holder.getAdapterPosition());
                        notifyDataSetChanged();*/

                        imageDelete(messageList, holder.getAdapterPosition(), holder);
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return messageList.size();
        }

        public void updateData(String delete, int pos, String name, String url) {

            messageList.get(pos).setStatus(delete);
            messageList.get(pos).setName(name);
            messageList.get(pos).setUrl(url);

            notifyItemChanged(pos);

        }
    }

    private void openGallary() {

        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(gallery, RESULT_LOAD);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == RESULT_LOAD && resultCode == RESULT_OK && data != null) {

                fileUri = data.getData();
                String imgName = getUriRealPathAboveKitkat(OfflinePaymentDetails.this, fileUri);
                image_url1 = compressImage(imgName);

                alert();


            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void addData(String imageName, String image_url1) {

        payAttachModel req = new payAttachModel();
        req.setName(imageName);
        req.setUrl(image_url1);
        //Log.i(TAG, "addbutton: "+pList.size());
        pList.add(pList.size(), req);
        attachtoAdapter(pList);

        royaltyDetails.setVisibility(View.GONE);
        variableDetails.setVisibility(View.GONE);
        brandingDetails.setVisibility(View.GONE);
        omsDetails.setVisibility(View.GONE);


    }

    private String getImageName(String image_url1) {


        int cut = image_url1.lastIndexOf("/");
        if (cut != -1) {
            return image_url1.substring(cut + 1);
        } else {
            return image_url1;
        }
    }

    private boolean emptyCheck() {


        if (!royaltyDate.getText().toString().isEmpty()
                && !royaltyModeOfPayment.getText().toString().isEmpty()
                && !royaltyAmountPaid.getText().toString().isEmpty()
                && !royaltyTDSDeducted.getText().toString().isEmpty()
                && !royaltyRefereneNumber.getText().toString().isEmpty()
                && !royaltyBankName.getText().toString().isEmpty()
        ) {
            return true;

        } else if (!variableDate.getText().toString().isEmpty()
                && !variableModeOfPayment.getText().toString().isEmpty()
                && !variableAmountPaid.getText().toString().isEmpty()
                && !variableTaxDeducted.getText().toString().isEmpty()
                && !variableReferenceNumber.getText().toString().isEmpty()
                && !variableBankName.getText().toString().isEmpty()) {
            return true;

        } else if (!brandingDate.getText().toString().isEmpty()
                && !brandinModeOfPayment.getText().toString().isEmpty()
                && !brandingAmountPaid.getText().toString().isEmpty()
                && !brandingTaxDeducted.getText().toString().isEmpty()
                && !brandingReferenceNumber.getText().toString().isEmpty()
                && !brandingBankName.getText().toString().isEmpty()) {
            return true;

        } else if (!omsDate.getText().toString().isEmpty()
                && !omsModeOfPayment.getText().toString().isEmpty()
                && !omsAmountPaid.getText().toString().isEmpty()
                && !omsTaxDeducted.getText().toString().isEmpty()
                && !omsReferenceNumber.getText().toString().isEmpty()
                && !omsBankName.getText().toString().isEmpty()

        ) {
            return true;

        } else {
            return false;
        }
    }

    private void emptyCheckALL() {

        emptyCheck2("royalty", royaltyDate, royaltyModeOfPayment, royaltyAmountPaid, royaltyTDSDeducted, royaltyRefereneNumber, royaltyBankName);

        if(allValues)emptyCheck2("variable", variableDate, variableModeOfPayment, variableAmountPaid, variableTaxDeducted, variableReferenceNumber, variableBankName);

        if(allValues)emptyCheck2("branding", brandingDate, brandinModeOfPayment, brandingAmountPaid, brandingTaxDeducted, brandingReferenceNumber, brandingBankName);

        if(allValues) emptyCheck2("oms", omsDate, omsModeOfPayment, omsAmountPaid, omsTaxDeducted, omsReferenceNumber, omsBankName);

    }

    private void emptyCheck2(String royalty, TextView Date, TextView ModeOfPayment, EditText AmountPaid,
                             EditText TDSDeducted, EditText RefereneNumber, EditText BankName) {

        if (!Date.getText().toString().isEmpty()
                || !ModeOfPayment.getText().toString().isEmpty()
                || !AmountPaid.getText().toString().isEmpty()
                || !TDSDeducted.getText().toString().isEmpty()
                || !RefereneNumber.getText().toString().isEmpty()
                || !BankName.getText().toString().isEmpty()
        ) {
            if (!Date.getText().toString().isEmpty()
                    && !ModeOfPayment.getText().toString().isEmpty()
                    && !AmountPaid.getText().toString().isEmpty()
                    && !TDSDeducted.getText().toString().isEmpty()
                    && !RefereneNumber.getText().toString().isEmpty()
                    && !BankName.getText().toString().isEmpty()
            ) {
                allValues=true;
            } else {
                allValues=false;
                Toast.makeText(OfflinePaymentDetails.this, "please fill " + royalty + " data", Toast.LENGTH_SHORT).show();
                return;
            }
        }else{
            allValues=true;
        }
    }

    public void imageDelete(List<payAttachModel> messageList, int adapterPosition, PAdapter.MyViewHolder holder) {

        // custom dialog
        Dialog dialog = new Dialog(OfflinePaymentDetails.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setGravity(Gravity.CENTER | Gravity.CENTER);
        dialog.setContentView(R.layout.common_cus_dialog);
        TextView Message, Message2;
        Button cancel, Confirm;
        Message = dialog.findViewById(R.id.Message);
        Message2 = dialog.findViewById(R.id.Message2);
        cancel = dialog.findViewById(R.id.cancel);
        Confirm = dialog.findViewById(R.id.Confirm);
        Message.setText(GlobalText.are_you_sure_to_delete);
        //Message2.setText("Go ahead and Submit ?");
        Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                messageList.remove(adapterPosition);

                pAdapter.notifyDataSetChanged();

                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void alert() {


        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.imagename);
        dialog.setCancelable(false);

        EditText imagename = dialog.findViewById(R.id.imagename);
        Button submit = dialog.findViewById(R.id.submit);


        ProgressDialog pd = new ProgressDialog(OfflinePaymentDetails.this, R.style.AppCompatAlertDialogStyle);

        pd.setMessage("Please Wait...");
        pd.setIndeterminate(true);

        pd.setCancelable(false);
        pd.show();

        submit.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.P)
            @Override
            public void onClick(View v) {

                if (imagename.getText().toString().isEmpty()) {
                    CommonMethods.alertMessage(OfflinePaymentDetails.this, "please enter attachment name");
                } else {
                    imageName = imagename.getText().toString();
                    dialog.dismiss();

                    image_url1 = getImageUrl(image_url1, imageName);
                    addData(imageName, image_url1);

                    pd.dismiss();


                }
            }
        });

        dialog.show();
    }


    public String getImageUrl(final String filePath, final String ImgName) {

        String mImageUrl = "";


        try {
            String resourceType = filePath.substring(filePath.lastIndexOf(".") + 1);
            String keydata = ImgName + "." + resourceType; //confirmed by surya and deleted since it is for testing purpose
            ClientConfiguration clientConfiguration = getClientConfiguration();

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            String path = "";

            String Dealer_code = CommonMethods.getstringvaluefromkey(OfflinePaymentDetails.this, "dealer_code");
            String dealer_display_name = CommonMethods.getstringvaluefromkey(OfflinePaymentDetails.this, "dealer_display_name");


            path = dealer_display_name + "/" + Dealer_code + "/" + keydata; // the folder structure for the image in Prod


            final AWSSecretsManager SECRETS_MANAGER = AWSSecretsManager.getInstance();
            final String key = SECRETS_MANAGER.get(BUCKET_KEY);
            final String secret = SECRETS_MANAGER.get(BUCKET_SECRET);

            final String bucketName = Global.imageBucket;
            //Log.i(TAG, "BUCKET_KEY: "+key);
           //Log.i(TAG, "BUCKET_SECRET: "+secret);

            AWSCredentials awsCredentials = getAWSCredentials(key, secret);

            AmazonS3Client s3Client = awsCredentials != null ? new AmazonS3Client(awsCredentials, clientConfiguration) : new AmazonS3Client();
            s3Client.setRegion(Region.getRegion(Regions.AP_SOUTH_1));

          //  Log.i(TAG, " " + Regions.US_WEST_2);//BucketOwnerFullControl

            s3Client.putObject(new PutObjectRequest(bucketName, path, new File(filePath)).withCannedAcl(CannedAccessControlList.BucketOwnerFullControl));

            mImageUrl = s3Client.getResourceUrl(bucketName, path);

            Log.i(TAG, "getImageUrl: " + mImageUrl);


        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }

        return mImageUrl;
    }

    @Override
    public void onSuccess(String s, String s1, String s2) {


    }

    @Override
    public void onProgress(int i, long l, long l1) {

        Log.i(TAG, "onProgress: " + i);

    }

    private static AWSCredentials getAWSCredentials(final String awsKey, final String awsSecret) {
        if (awsKey != null && !awsKey.isEmpty() && awsSecret != null && !awsSecret.isEmpty()) {
            return new BasicAWSCredentials(awsKey, awsSecret);
        }
        return null;
    }

    private static ClientConfiguration getClientConfiguration() {
        final ClientConfiguration clientConfiguration = new ClientConfiguration();
        clientConfiguration.setConnectionTimeout(300000);
        clientConfiguration.setMaxErrorRetry(10);
        clientConfiguration.setSocketTimeout(300000);
        clientConfiguration.setMaxConnections(500);
        clientConfiguration.setProtocol(Protocol.HTTP);
        return clientConfiguration;
    }

    private String compressImage(final String imageUri) {

        Log.e(TAG, "" + imageUri);
        if (imageUri == null || imageUri.equalsIgnoreCase("")) {
            return "";
        }
        String filePath = imageUri;
        try {

            FileInputStream fis = null;
            File imagefile = new File(filePath);
            try {
                fis = new FileInputStream(imagefile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            Bitmap bmp = BitmapFactory.decodeStream(fis);
            filePath = compressImageBasedOnSize(filePath, bmp);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return filePath;

    }

    private String compressImageBasedOnSize(String imagePath, Bitmap mBitmap) {
        FileOutputStream out = null;
        try {

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] imagesize = baos.toByteArray();

            if (imagesize.length > 3 * 1024 * 1024) { //3MB
                out = new FileOutputStream(imagePath);
                mBitmap.compress(Bitmap.CompressFormat.JPEG, 50, out);
            } else if (imagesize.length > 2 * 1024 * 1024) { //2MB
                out = new FileOutputStream(imagePath);
                mBitmap.compress(Bitmap.CompressFormat.JPEG, 60, out);
            } else if (imagesize.length > 1024 * 1024) { //1MB
                out = new FileOutputStream(imagePath);
                mBitmap.compress(Bitmap.CompressFormat.JPEG, 70, out);
            } else if (imagesize.length > 500 * 1024) { //500 KB
                out = new FileOutputStream(imagePath);
                mBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
            } else {
                out = new FileOutputStream(imagePath);
                mBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            }

            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return imagePath;
    }

    private void sendPayDetails(final OfflinePaymentDetails activity, ArrayList<paymentDetails> req) {

        SpinnerManager.showSpinner(activity);

        Paymentservice paymentservice = new Paymentservice();

        paymentservice.pushMultipleData(req, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                SpinnerManager.hideSpinner(activity);
                Response<paymentdetailsRes> mRes = (Response<paymentdetailsRes>) obj;
                paymentdetailsRes mData = mRes.body();

                if (mData.getStatus().equalsIgnoreCase("Success")) {

                    Toast.makeText(activity, "Information saved successfully", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(OfflinePaymentDetails.this, OfflinePaymentDetails.class));
                    finish();
                } else {

                }
            }

            @Override
            public void OnFailure(Throwable t) {
                SpinnerManager.hideSpinner(activity);

            }

        });
    }


    private void sendS3Details(final Context mContext) {


        SpinnerManager.showSpinner(mContext);
        S3DetailsServices.getDetails(OfflinePaymentDetails.this, "", new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(OfflinePaymentDetails.this);
                Response<s3Res> mRes = (Response<s3Res>) obj;
                s3Res mData = mRes.body();

                try {
                    AWSSecretsManager.getInstance().parseAWSSecret(mData);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void OnFailure(Throwable t) {
                SpinnerManager.hideSpinner(OfflinePaymentDetails.this);
                t.printStackTrace();
            }
        });

    }


    private String getUriRealPathAboveKitkat(Context ctx, Uri uri) {
        String ret = "";

        if (ctx != null && uri != null) {

            if (isContentUri(uri)) {
                if (isGooglePhotoDoc(uri.getAuthority())) {
                    ret = uri.getLastPathSegment();
                } else {
                    ret = getImageRealPath(getContentResolver(), uri, null);
                }
            } else if (isFileUri(uri)) {
                ret = uri.getPath();
            } else if (isDocumentUri(ctx, uri)) {

                // Get uri related document id.
                String documentId = DocumentsContract.getDocumentId(uri);

                // Get uri authority.
                String uriAuthority = uri.getAuthority();

                if (isMediaDoc(uriAuthority)) {
                    String idArr[] = documentId.split(":");
                    if (idArr.length == 2) {
                        // First item is document type.
                        String docType = idArr[0];

                        // Second item is document real id.
                        String realDocId = idArr[1];

                        // Get content uri by document type.
                        Uri mediaContentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                        if ("image".equals(docType)) {
                            mediaContentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                        } else if ("video".equals(docType)) {
                            mediaContentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                        } else if ("audio".equals(docType)) {
                            mediaContentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                        }

                        // Get where clause with real document id.
                        String whereClause = MediaStore.Images.Media._ID + " = " + realDocId;

                        ret = getImageRealPath(getContentResolver(), mediaContentUri, whereClause);
                    }

                } else if (isDownloadDoc(uriAuthority)) {
                    // Build download uri.
                    Uri downloadUri = Uri.parse("content://downloads/public_downloads");

                    // Append download document id at uri end.
                    Uri downloadUriAppendId = ContentUris.withAppendedId(downloadUri, Long.valueOf(documentId));

                    ret = getImageRealPath(getContentResolver(), downloadUriAppendId, null);

                } else if (isExternalStoreDoc(uriAuthority)) {
                    String idArr[] = documentId.split(":");
                    if (idArr.length == 2) {
                        String type = idArr[0];
                        String realDocId = idArr[1];

                        if ("primary".equalsIgnoreCase(type)) {
                            ret = Environment.getExternalStorageDirectory() + "/" + realDocId;
                        }
                    }
                }
            }
        }

        return ret;
    }


    /* Check whether this uri represent a document or not. */
    private boolean isDocumentUri(Context ctx, Uri uri) {
        boolean ret = false;
        if (ctx != null && uri != null) {
            ret = DocumentsContract.isDocumentUri(ctx, uri);
        }
        return ret;
    }

    private boolean isContentUri(Uri uri) {
        boolean ret = false;
        if (uri != null) {
            String uriSchema = uri.getScheme();
            if ("content".equalsIgnoreCase(uriSchema)) {
                ret = true;
            }
        }
        return ret;
    }

    /* Check whether this uri is a file uri or not.
     *  file uri like file:///storage/41B7-12F1/DCIM/Camera/IMG_20180211_095139.jpg
     * */
    private boolean isFileUri(Uri uri) {
        boolean ret = false;
        if (uri != null) {
            String uriSchema = uri.getScheme();
            if ("file".equalsIgnoreCase(uriSchema)) {
                ret = true;
            }
        }
        return ret;
    }


    /* Check whether this document is provided by ExternalStorageProvider. */
    private boolean isExternalStoreDoc(String uriAuthority) {
        boolean ret = false;

        if ("com.android.externalstorage.documents".equals(uriAuthority)) {
            ret = true;
        }

        return ret;
    }

    /* Check whether this document is provided by DownloadsProvider. */
    private boolean isDownloadDoc(String uriAuthority) {
        boolean ret = false;

        if ("com.android.providers.downloads.documents".equals(uriAuthority)) {
            ret = true;
        }

        return ret;
    }

    /* Check whether this document is provided by MediaProvider. */
    private boolean isMediaDoc(String uriAuthority) {
        boolean ret = false;

        if ("com.android.providers.media.documents".equals(uriAuthority)) {
            ret = true;
        }

        return ret;
    }

    /* Check whether this document is provided by google photos. */
    private boolean isGooglePhotoDoc(String uriAuthority) {
        boolean ret = false;

        if ("com.google.android.apps.photos.content".equals(uriAuthority)) {
            ret = true;
        }

        return ret;
    }

    /* Return uri represented document file real local path.*/
    private String getImageRealPath(ContentResolver contentResolver, Uri uri, String whereClause) {
        String ret = "";

        // Query the uri with condition.
        Cursor cursor = contentResolver.query(uri, null, whereClause, null, null);

        if (cursor != null) {
            boolean moveToFirst = cursor.moveToFirst();
            if (moveToFirst) {

                // Get columns name by uri type.
                String columnName = MediaStore.Images.Media.DATA;

                if (uri == MediaStore.Images.Media.EXTERNAL_CONTENT_URI) {
                    columnName = MediaStore.Images.Media.DATA;
                } else if (uri == MediaStore.Audio.Media.EXTERNAL_CONTENT_URI) {
                    columnName = MediaStore.Audio.Media.DATA;
                } else if (uri == MediaStore.Video.Media.EXTERNAL_CONTENT_URI) {
                    columnName = MediaStore.Video.Media.DATA;
                }

                // Get column index.
                int imageColumnIndex = cursor.getColumnIndex(columnName);

                // Get column value which is the uri related file local path.
                ret = cursor.getString(imageColumnIndex);
            }
        }
        return ret;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(OfflinePaymentDetails.this, PaymentActivity.class));
        finish();
    }
}
