package com.mfcwl.mfc_dealer.Activity.RDR.Services;

import com.mfcwl.mfc_dealer.Activity.RDR.Model.ActionCompleteRequest;
import com.mfcwl.mfc_dealer.Activity.RDR.Model.CountResponse;
import com.mfcwl.mfc_dealer.Activity.RDR.Model.CountRquest;
import com.mfcwl.mfc_dealer.Activity.RDR.Model.DashboardRequest;
import com.mfcwl.mfc_dealer.Activity.RDR.Model.DashboardResponse;
import com.mfcwl.mfc_dealer.Activity.RDR.Model.EscalateRequest;
import com.mfcwl.mfc_dealer.Activity.RDR.Model.RDRActionItemRequest;
import com.mfcwl.mfc_dealer.Activity.RDR.Model.RDRActionItemResponse;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.retrofitconfig.BaseService;
import com.mfcwl.mfc_dealer.retrofitconfig.DasboardBaseService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class RDRDashboardService extends DasboardBaseService {

    public static void fetchRDRData(DashboardRequest mRequest, HttpCallResponse mHttpCallResponse){
        RDRInterFace mInterFace =  retrofit.create(RDRInterFace.class);
        Call<DashboardResponse> mCall = mInterFace.getDashBoardData(mRequest);

        mCall.enqueue(new Callback<DashboardResponse>() {
            @Override
            public void onResponse(Call<DashboardResponse> call, Response<DashboardResponse> response) {
                if(response.isSuccessful()){
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<DashboardResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });
    }


    public static void fetchDealerActionsItems(RDRActionItemRequest mRequest, HttpCallResponse mHttpCallResponse){
        RDRInterFace mInterFace =  retrofit.create(RDRInterFace.class);
        Call<RDRActionItemResponse> mCall = mInterFace.getDealerActionItems(mRequest);

        mCall.enqueue(new Callback<RDRActionItemResponse>() {
            @Override
            public void onResponse(Call<RDRActionItemResponse> call, Response<RDRActionItemResponse> response) {
                if(response.isSuccessful()){
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<RDRActionItemResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });
    }

    public static void postAction(ActionCompleteRequest mRequest, HttpCallResponse mHttpCallResponse){
        RDRInterFace mInterFace =  retrofit.create(RDRInterFace.class);
        Call<Object> mCall = mInterFace.sendAction(mRequest);

        mCall.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if(response.isSuccessful()){
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });
    }

    public static void postEscalation(EscalateRequest mRequest, HttpCallResponse mHttpCallResponse){
        RDRInterFace mInterFace =  retrofit.create(RDRInterFace.class);
        Call<Object> mCall = mInterFace.sendEscalation(mRequest);

        mCall.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if(response.isSuccessful()){
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });
    }

    public static void fetchCount(CountRquest mRequest , HttpCallResponse mHttpCallResponse){
        RDRInterFace mInterFace =  retrofit.create(RDRInterFace.class);
        Call<CountResponse> mCall = mInterFace.getPendingCount(mRequest);
        mCall.enqueue(new Callback<CountResponse>() {
            @Override
            public void onResponse(Call<CountResponse> call, Response<CountResponse> response) {
                if(response.isSuccessful()){
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<CountResponse> call, Throwable t) {
                   mHttpCallResponse.OnFailure(t);
            }
        });
    }


    public interface RDRInterFace {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("franschisereport/GetRdrInfoForAsmDealers")
        Call<DashboardResponse> getDashBoardData(@Body DashboardRequest mRequest);


        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("franschisereport/DealerActionItems")
        Call<RDRActionItemResponse> getDealerActionItems(@Body RDRActionItemRequest mRequest);


        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("franschisereport/UpdateActionItem")
        Call<Object> sendAction(@Body ActionCompleteRequest mRequest);


        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("franschisereport/GetPendingActionItemCount")
        Call<CountResponse> getPendingCount(@Body CountRquest mRequest);


        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("franschisereport/EscalateActionItem")
        Call<Object> sendEscalation(@Body EscalateRequest mRequest);



    }


}
