package com.mfcwl.mfc_dealer.Activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;

import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.util.Log;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.Activity.LeadSection.LeadConstantsSection;
import com.mfcwl.mfc_dealer.Activity.RDR.Services.OnKillApp;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Fragment.StockStoreFrag;
import com.mfcwl.mfc_dealer.InstanceCreate.LeadSection.LeadFilterSaveInstance;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.NotificatioService.Config;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.LoggerGeneral;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


import remotedealer.activity.RDMLandingPage;
import remotedealer.activity.TutorialOne;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.resetMethodForLeads;
import static com.mfcwl.mfc_dealer.NotificatioService.MyFirebaseMessagingService.AgeingInventory;
import static com.mfcwl.mfc_dealer.NotificatioService.MyFirebaseMessagingService.lead;
import static com.mfcwl.mfc_dealer.NotificatioService.MyFirebaseMessagingService.leadFollowUp;
import static com.mfcwl.mfc_dealer.NotificatioService.MyFirebaseMessagingService.noStockImage;
import static com.mfcwl.mfc_dealer.NotificatioService.MyFirebaseMessagingService.notificationsStatus;

public class SplashActivity extends Activity {

    public static boolean progress = true;

    private final String TAG = MainActivity.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    String actionevent, actioneventvalue;

    private SharedPreferences mSharedPreferences = null;

    Runnable r = null;
    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Log.i(TAG, "onCreate: ");
        restoreLeadInstance();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(new Intent(getBaseContext(), OnKillApp.class));
        } else {
           startService(new Intent(getBaseContext(), OnKillApp.class));
        }

        try {
            StockStoreFrag.stock.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }



        handler = new Handler();

        if (lead.equalsIgnoreCase("lead")) {
            CommonMethods.setvalueAgainstKey(SplashActivity.this, "Notification_Switch", "lead");
        } else if (noStockImage.equalsIgnoreCase("noStockImage")) {
            Log.e("noStockImage", "noStockImage-coming-0");
            CommonMethods.setvalueAgainstKey(SplashActivity.this, "Notification_Switch", "noStockImage");
        } else if (leadFollowUp.equalsIgnoreCase("leadFollowUp")) {
            CommonMethods.setvalueAgainstKey(SplashActivity.this, "Notification_Switch", "leadFollowUp");
        } else if (AgeingInventory.equalsIgnoreCase("AgeingInventory")) {
            CommonMethods.setvalueAgainstKey(SplashActivity.this, "Notification_Switch", "AgeingInventory");
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter("com.mfcwl.mfc_dealer"));
        //  Log.e("title", "title=2");

        r = new Runnable() {

            public void run() {

                if (getIntent().getExtras() != null) {
                    try {
                        for (String key : getIntent().getExtras().keySet()) {


                            String value = getIntent().getExtras().getString(key) == null ? "" : getIntent().getExtras().getString(key);
                            //    Log.d("Hii", "Key: " + key + " Value: " + value);

                            if (key.equalsIgnoreCase("actionevent")) {
                                actionevent = key;
                                actioneventvalue = value;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    //Log.e("Token","Token-3");
                }
                // Log.e("Token","Token-4"+actioneventvalue);

                if (actionevent != null) {
                    if (actionevent != "") {

                        LoggerGeneral.info("actionevent--" + actioneventvalue);
                        if (actioneventvalue.equalsIgnoreCase("lead")
                                || notificationsStatus.equalsIgnoreCase("lead")) {

                            CommonMethods.setvalueAgainstKey(SplashActivity.this, "Notification_Switch", "lead");
                            CommonMethods.setvalueAgainstKey(SplashActivity.this, "nofication_status", "true");

                        } else if (actioneventvalue.equalsIgnoreCase("noStockImage")
                                || notificationsStatus.equalsIgnoreCase("noStockImage")) {
                            Log.e("noStockImage", "noStockImage-coming-1");
                            CommonMethods.setvalueAgainstKey(SplashActivity.this, "Notification_Switch", "noStockImage");
                            CommonMethods.setvalueAgainstKey(SplashActivity.this, "nofication_status", "true");

                        } else if (actioneventvalue.equalsIgnoreCase("leadFollowUp")
                                || notificationsStatus.equalsIgnoreCase("leadFollowUp")) {
                            CommonMethods.setvalueAgainstKey(SplashActivity.this, "Notification_Switch", "leadFollowUp");
                            CommonMethods.setvalueAgainstKey(SplashActivity.this, "nofication_status", "true");

                        } else if (actioneventvalue.equalsIgnoreCase("AgeingInventory")
                                || notificationsStatus.equalsIgnoreCase("AgeingInventory")) {
                            CommonMethods.setvalueAgainstKey(SplashActivity.this, "Notification_Switch", "AgeingInventory");
                            CommonMethods.setvalueAgainstKey(SplashActivity.this, "nofication_status", "true");

                        } else {
                            //main
                            // CommonMethods.setvalueAgainstKey(MainActivity.this, "Notification_Switch", "type");

                        }
                    } else {
                        // CommonMethods.setvalueAgainstKey(MainActivity.this, "Notification_Switch", "type");

                        //main
                    }
                } else {
                    // CommonMethods.setvalueAgainstKey(MainActivity.this, "Notification_Switch", "type");

                        /*Intent intent = new Intent(MainActivity.this, DisplayAct.class);
                        startActivity(intent);
                        finish();*/
                }
                // }

                handler.postDelayed(r, 1000);
            }
        };
        handler.post(r);

        showIntentDetails();

        new CountDownTimer(2000, 1000) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                if (!CommonMethods.getstringvaluefromkey(SplashActivity.this, "token").equalsIgnoreCase("")) {

                    CommonMethods.setvalueAgainstKey(SplashActivity.this, "status", "Home");

                    if (CommonMethods.isInternetWorking(SplashActivity.this)) {

                        gotoHomePage();

                    } else {
                        CommonMethods.alertMessage(SplashActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
                    }

                } else {
                    ActivityCompat.finishAffinity(SplashActivity.this);
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }
            }
        }.start();
    }


    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        showIntentDetails();
    }

    private void showIntentDetails() {
        Bundle extras = getIntent().getExtras();
        String intentDetailsLog = "";

        String action = getIntent().getAction();
        if (action != null) {
            intentDetailsLog +=
                    "INTENT ACTION\n" + " - " + action + "\n";
        }

        if (extras != null) {
            intentDetailsLog += "\nINTENT EXTRAS\n";
            for (String key : extras.keySet()) {
                intentDetailsLog += " - " + key + " : " + extras.get(key) + "\n";
            }
        }
        LoggerGeneral.info("inLog--" + intentDetailsLog);
    }

    @Override
    protected void onStop() {
        super.onStop();

        handler.removeCallbacks(r);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(r);
    }


    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        resetMethodForLeads();

        Application.getInstance().trackScreenView(SplashActivity.this, GlobalText.Splash);

    }

    private void gotoHomePage() {


        if (CommonMethods.getstringvaluefromkey(this, "user_type").equalsIgnoreCase("AM")
        ) {

            Intent in_main = new Intent(SplashActivity.this, RDMLandingPage.class);
            startActivity(in_main);
            finish();

        } else {
            WebServicesCall.webCall(this, this, jsonMakeLeadAccessToken(), "LeadToken", GlobalText.POST);
            Intent in_main = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(in_main);
            finish();
        }
    }

    private static JSONObject jsonMakeLeadAccessToken() {

        JSONObject jObj = new JSONObject();
        try {
            jObj.put("username", "mfcwapp@lms.com");
            jObj.put("password", "p@ssword");
            jObj.put("tag", "android");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jObj;
    }

    public static void ParseLeadToken(JSONObject jObj, Activity activity) {
        try {
            if (jObj.getString("status").equalsIgnoreCase("success")) {
                CommonMethods.setvalueAgainstKey(activity, "access_token", jObj.getString("access_token"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void restoreLeadInstance() {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        JSONArray leadstatusJsonArray = new JSONArray();
        mSharedPreferences = this.getSharedPreferences("MFCB", Context.MODE_PRIVATE);
        mSharedPreferences = this.getSharedPreferences("MFCP", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String postfollDate = mSharedPreferences.getString(LeadConstantsSection.LEAD_SAVE_DATE, "");
        String status = mSharedPreferences.getString(LeadConstantsSection.LEAD_SAVE_STATUS, "");
        hashMap = gson.fromJson(postfollDate, hashMap.getClass());
        leadstatusJsonArray = gson.fromJson(status, leadstatusJsonArray.getClass());
        if (hashMap != null) {
            LeadFilterSaveInstance.getInstance().setSavedatahashmap(hashMap);
        }
        if (leadstatusJsonArray != null) {
            LeadFilterSaveInstance.getInstance().setStatusarray(leadstatusJsonArray);
        }
    }



}

