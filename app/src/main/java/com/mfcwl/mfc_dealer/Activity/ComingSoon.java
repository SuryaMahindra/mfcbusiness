package com.mfcwl.mfc_dealer.Activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.mfcwl.mfc_dealer.R;





public class ComingSoon extends AppCompatActivity {
    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate: ");
        setContentView(R.layout.activity_comingsoon);
        ImageView backBtn = findViewById(R.id.createRepo_backbtn);

        Button callnow = findViewById(R.id.callnow);

        callnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             /*   String encryption = "1";
                vSettings().mEncryptionKey = encryption;
                Intent i = new Intent(ComingSoon.this, CallActivity.class);
                i.putExtra(ConstantApp.ACTION_KEY_CHANNEL_NAME, "Surya");
                i.putExtra(ConstantApp.ACTION_KEY_ENCRYPTION_KEY, encryption);
                i.putExtra(ConstantApp.ACTION_KEY_ENCRYPTION_MODE, getResources().getStringArray(R.array.encryption_mode_values)[vSettings().mEncryptionModeIndex]);

                startActivity(i);*/
            }
        });
      /*  try {
            Calendar calendar = Calendar.getInstance();
            CalendarView calendarView = (CalendarView) findViewById(R.id.calendarView);
            calendarView.setDate(calendar);

        }catch (Exception e){
            e.printStackTrace();
        }*/
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ComingSoon.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ComingSoon.this, MainActivity.class);
        startActivity(intent);
    }
}
