package com.mfcwl.mfc_dealer.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.videoAppSpecific.SqlAdapterForDML;
import com.vigilant.mcsidekicksdk.Screens.ScreenOnboardCamera;
import com.vigilant.mcsidekicksdk.app.VKey;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import sidekicklpr.LPRConstants;
import sidekicklpr.LPRResponse;
import sidekicklpr.PreferenceManager;
import sidekicklpr.StockActivity;

public class ScannerActivity extends AppCompatActivity {

    private Button mStartScan;
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    final private int REQUEST_CODE_SCREEN_ON_BOARD_CAMERA = 0;
    private PreferenceManager preferenceManager;

    private SqlAdapterForDML mSqlAdapterForDML = SqlAdapterForDML.getInstance();

    String TAG = getClass().getSimpleName();
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demoscanner_view);
        Log.i(TAG, "onCreate: ");
        mStartScan = findViewById(R.id.beginscan);
        preferenceManager = new PreferenceManager(this);
        checkPermissionApp();
        mStartScan.setOnClickListener(v -> {
            startLPRScan(ScannerActivity.this, LPRConstants.REQUEST_CODE_LPR_MULTIPLE_SCAN);

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LPRConstants.REQUEST_CODE_LPR_MULTIPLE_SCAN) {
            Log.e("Onactivity called", "onActivity called");
            String value = readLPRFile(this, LPRConstants.REQUEST_CODE_LPR_MULTIPLE_SCAN);
            Log.e("LPR Data", "" + value);
            parseLPRData(value);
        }

    }

    private void checkPermissionApp() {
        super.onUserLeaveHint();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            int hasWriteContactsPermission;
            hasWriteContactsPermission = checkSelfPermission(Manifest.permission.CAMERA);
            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_PHONE_STATE}, REQUEST_CODE_ASK_PERMISSIONS);
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    finish();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    public void startLPRScan(Activity context, int type) { // type 1001(sinlge scan), 1002(multiple)
        String lprDir = preferenceManager.readString(LPRConstants.LPR_DIR, ""); //this will be YMS / LPR

        if (lprDir.isEmpty()) {
            File sdCard = Environment.getExternalStorageDirectory();
            lprDir = sdCard.getAbsolutePath() + "/" + context.getResources().getString(R.string.app_name) + "/" + "LPR";
            File dir = new File(lprDir);
            if (!dir.exists())
                dir.mkdirs();

            lprDir = dir.getPath();
            preferenceManager.writeString(LPRConstants.LPR_DIR, lprDir);
        }

        /*LPR FILENAME will be created for every call of this method..*/
        String lprFileName = UUID.randomUUID().toString() + "_" + System.currentTimeMillis(); // unique id creating..
        /*LPR_FILENAME1 for SINGLE SCAN and LPR_FILENAME2 for MULTIPLE SCAN*/
        preferenceManager.writeString(LPRConstants.LPR_FILENAME + type, lprFileName); //this is unique id only..now storing bsed on type of scan..

        Intent intent = new Intent(context, ScreenOnboardCamera.class);

        Log.e("LPR Path", lprDir + "/" + lprFileName);
        intent.putExtra(VKey.PathFolder, lprDir + "/" + lprFileName); // path folder setting..
        if (type == LPRConstants.REQUEST_CODE_LPR_SINGLE_SCAN) {
            intent.putExtra(VKey.LimitScans, 1);
            intent.putExtra(VKey.ShowListScans, false);
        }

        context.startActivityForResult(intent, type);
    }

    public String readLPRFile(Context context, int type) { // reading file..for single scan..jSON filw is deleted after reading image..
        String value = "";
        final String lprDir, lprFileName;
        lprDir = preferenceManager.readString(LPRConstants.LPR_DIR, "");
        lprFileName = preferenceManager.readString(LPRConstants.LPR_FILENAME + type, "");
        try {

            int fileCount = 0;
            File directory;
            File[] allFiles;

            FilenameFilter fileFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return (name.startsWith(lprFileName) && name.endsWith(".json"));
                }
            };

            directory = new File(lprDir + "/" + lprFileName);
            allFiles = directory.listFiles(fileFilter);
            Log.e("LPR", "file lenght: " + allFiles.length);
            if (allFiles.length == 1) {

                File file = allFiles[0];/*LPR required file..*/
                if (file.exists()) {
                    FileInputStream fis = new FileInputStream(file);
                    DataInputStream in = new DataInputStream(fis);
                    BufferedReader br = new BufferedReader(new InputStreamReader(in));
                    String strLine;
                    while ((strLine = br.readLine()) != null) {
                        value = value + strLine;
                    }
                    in.close();
                    if (type == LPRConstants.REQUEST_CODE_LPR_SINGLE_SCAN && file.delete()) {
                        fileCount++;
                        Log.e("LPR", "file Deleted");
                    } else {
                        Log.e("LPR", "file not Deleted");
                    }
                }
            } else {
                for (File f : allFiles) {
                    if (f.delete()) {
                        fileCount++;
                        Log.e("LPR", "file Deleted");
                    } else {
                        Log.e("LPR", "file not Deleted");
                    }
                }
            }
            Log.e("LPR", "deleted file count: " + fileCount);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return value;
    }


    private void parseLPRData(String value) {
        try {
            Type empTypeList = new TypeToken<ArrayList<LPRResponse>>() {
            }.getType();
            List<LPRResponse> mData = new Gson().fromJson(value, empTypeList);
            if ( mData !=null && mData.size() > 0) {
                Log.i("LPR", "Result: ");
                for (int i = 0; i < mData.size(); i++) {
                    LPRResponse response = mData.get(i);
                    String outputValue = response.getOutputValue();
                    if (outputValue.toLowerCase().equals("unreg/na"))
                        outputValue = "";
                    mSqlAdapterForDML.insertLPRItem(response);
                }

            }
            if(mSqlAdapterForDML.getLPRListCount() > 0){
                showStockListingScreen();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showStockListingScreen() {
        Intent intent = new Intent(ScannerActivity.this, StockActivity.class);
        startActivityForResult(intent, LPRConstants.REQUEST_CODE_LPR_STOCK_LIST);
    }


}


