package com.mfcwl.mfc_dealer.Activity.ASM_Reports;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.WeekReportSubmitModel;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.weekReportModel;
import com.mfcwl.mfc_dealer.ASMModel.AutoFillResModel;
import com.mfcwl.mfc_dealer.ASMModel.CWRAutoFillModel;
import com.mfcwl.mfc_dealer.ASMModel.amsCreateRes;
import com.mfcwl.mfc_dealer.ASMModel.walkinCountRes;
import com.mfcwl.mfc_dealer.ASMModel.weekReportWalkinCountModel;
import com.mfcwl.mfc_dealer.ASMReportsServices.WRSubmitService;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.retrofitconfig.CWRAutoFillService;
import com.mfcwl.mfc_dealer.retrofitconfig.WeekReportServices;
import com.mfcwl.mfc_dealer.retrofitconfig.weekReportWalkinCountServices;
import com.mfcwl.mfc_dealer.videoAppSpecific.SqlAdapterForDML;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import lprServices.AsyncResponse;
import lprServices.LPRServerResponse;
import retrofit2.Response;
import sidekicklpr.LPRConstants;
import sidekicklpr.LPRDataUploadManager;
import sidekicklpr.LPRResponse;
import sidekicklpr.LPRWarningDialog;
import sidekicklpr.PreferenceManager;
import sidekicklpr.StockActivity;

public class CreateReportActivity extends AppCompatActivity implements AsyncResponse {

    public TextView dealername_tv, cr_day, cr_date, cr_time, zone, zoneSelect, omsCode, dealer_category_selectview, stksevdaysSelect,
            week_title_tv, week_start_date, week_end_date, currentStock, conversion_IEP, totalAggregateCases, conversion_NCD, totalWalkins, total_sales, warrantyPen, actualActivationTime, total_procurement, royalty_select_tv, cr_dealer_category_lbl, dealer_category_selectview_emtpy;

    public EditText state, city, acSapCode, amToken, amName, shTokenno, shName, osOfMonth, stkLtSixty,
            stkGtSixty, pAStock, cpt_procure, self_procure, iep_procure, ncd_procure, leadstIEP, leadstNCD,
            xmart_nonIEP, retail_Sales, offload_sales, bookingInHand,
            omsLeads, walkinstOMS, DoOWalkins, numOfFinCases, warrantyNos, warrantyValue,
            openAggregateCases, closedAggregateCases, actualCollection,
            NumOfDealerVisits, conversion_OMS, Num_Of_highestBids, paidstock_ev, parksell_ev;

    public ImageView crClose,LprCamera;

    public LinearLayout zone_ll, statecity_lbl_ll, statecity_ev_ll;

    LinearLayout dealername_ll;
    String createdDateOn, activationTime_Value, actualActivation_Value;
    String[] zoneArray = new String[]{"North", "South", "East", "West"};
    String[] dealerCategoryArray = new String[]{"D Cat", "L2", "RD", "RD 2W", "RD CV"};
    String[] stksevdaysArray = new String[]{"Yes", "No"};

    String zoneVal, dealerCatVal, stksevVal;

    public String dealername = "", dealer_code = "", tempdealer_code = "", editJson = "", cbFranchiseId = "", id = "";
    public Button week_save_btn, submitBtn;
    public String TAG = getClass().getSimpleName();
    public JSONObject newJsonObj = new JSONObject();
    final Calendar myCalendar = Calendar.getInstance();

    public ArrayList<String> weeksdate;
    public ArrayList<String> weekedate;

    public String[] weeksdatearray;
    public String[] weekedatearray;


    WeekReportSubmitModel weeklyReportSubmitModel = new WeekReportSubmitModel();

    //Related to LPR
    private Button mStartScan;
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    final private int REQUEST_CODE_SCREEN_ON_BOARD_CAMERA = 0;
    private PreferenceManager preferenceManager;

    private SqlAdapterForDML mSqlAdapterForDML = SqlAdapterForDML.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_report);
        Log.i(TAG, "onCreate: ");
        LPRDataUploadManager.delegate = this;
        try {

            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
            SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
            Date d = new Date();
            String dayOfTheWeek = sdf.format(d);

            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");

            String formattedDate = df.format(c);
            String date[] = formattedDate.split("-");
            SimpleDateFormat sdff = new SimpleDateFormat("HH:mm");
            String str = sdff.format(new Date());

            String day = date[0];
            String month = date[1];
            String year = date[2];
            createdDateOn = year + "-" + month + "-" + day;

            cr_day = findViewById(R.id.cr_day);
            cr_date = findViewById(R.id.cr_date);
            cr_time = findViewById(R.id.cr_time);
            cr_day.setHint("");
            cr_date.setHint("");
            cr_time.setHint("");
            cr_day.setText(dayOfTheWeek);
            cr_date.setText(formattedDate);
            cr_time.setText(str);

        } catch (Exception e) {
            e.printStackTrace();
        }

        weeksdate = new ArrayList<String>();
        weekedate = new ArrayList<String>();

        week_title_tv = (TextView) findViewById(R.id.week_title_tv);
        dealername_tv = (TextView) findViewById(R.id.dealername_tv);
        dealername_ll = (LinearLayout) findViewById(R.id.dealername_ll);
        week_start_date = (TextView) findViewById(R.id.week_start_date);
        week_end_date = (TextView) findViewById(R.id.week_end_date);
        cr_dealer_category_lbl = (TextView) findViewById(R.id.cr_dealer_category_lbl);
        dealer_category_selectview_emtpy = (TextView) findViewById(R.id.dealer_category_selectview_emtpy);

        week_save_btn = (Button) findViewById(R.id.week_save_btn);
        crClose = (ImageView) findViewById(R.id.crClose);

        LprCamera = findViewById(R.id.lprCamera);
        LprCamera.setVisibility(View.VISIBLE);

        preferenceManager = new PreferenceManager(this);
        checkPermissionApp();

        week_start_date.setText(getFirstDay(new Date()));

        //BackButton
        ImageView backIV = findViewById(R.id.createRepo_backbtn);
        //Textviews
        //activationDate=findViewById(R.id.cr_activationet);
        actualActivationTime = findViewById(R.id.cr_dealercategorytv);
        submitBtn = findViewById(R.id.submitBtn);


        //linnerlayout
        zone_ll = findViewById(R.id.zone_ll);
        statecity_lbl_ll = findViewById(R.id.statecity_lbl_ll);
        statecity_ev_ll = findViewById(R.id.statecity_ev_ll);

        //EditTexts
        state = findViewById(R.id.cr_stateet);
        city = findViewById(R.id.cr_cityet);
        omsCode = findViewById(R.id.cr_omscodeet);
        acSapCode = findViewById(R.id.ac_sapcodeet);
        amToken = findViewById(R.id.cr_amtokenet);
        amName = findViewById(R.id.cr_amnameet);
        shTokenno = findViewById(R.id.cr_smtokenet);
        shName = findViewById(R.id.cr_shnameet);
        osOfMonth = findViewById(R.id.cr_openingstocket);
        stkLtSixty = findViewById(R.id.cr_stocktillsixtyet);
        stkGtSixty = findViewById(R.id.cr_stockgtsixtyet);
        currentStock = findViewById(R.id.cr_current_stocket);
        pAStock = findViewById(R.id.cr_actualstocket);
        cpt_procure = findViewById(R.id.cr_cptet);
        self_procure = findViewById(R.id.cr_selfet);
        iep_procure = findViewById(R.id.cr_iepet);
        ncd_procure = findViewById(R.id.cr_ncdet);
        leadstIEP = findViewById(R.id.cr_leadsthiepet);
        leadstNCD = findViewById(R.id.cr_leadsthrncdet);
        conversion_IEP = findViewById(R.id.cr_coniepet);
        conversion_NCD = findViewById(R.id.cr_conncdet);
        Num_Of_highestBids = findViewById(R.id.cr_nohighestbidset);
        xmart_nonIEP = findViewById(R.id.cr_xmartet);
        total_procurement = findViewById(R.id.cr_procurementet);
        retail_Sales = findViewById(R.id.cr_retailsaleset);
        offload_sales = findViewById(R.id.cr_offloadsaleset);
        total_sales = findViewById(R.id.cr_totalet);
        bookingInHand = findViewById(R.id.cr_bookinginhandet);
        conversion_OMS = findViewById(R.id.cr_conomsset);
        omsLeads = findViewById(R.id.cr_omsleadset);
        walkinstOMS = findViewById(R.id.cr_walkinins);
        DoOWalkins = findViewById(R.id.cr_directorganicet);
        totalWalkins = findViewById(R.id.cr_totalnowalkinset);
        numOfFinCases = findViewById(R.id.cr_nofinancecaseset);
        warrantyNos = findViewById(R.id.cr_warrantyet);
        warrantyValue = findViewById(R.id.cr_warrantyvalet);
        warrantyPen = findViewById(R.id.cr_warrantypenet);
        openAggregateCases = findViewById(R.id.cr_openaggregateet);
        closedAggregateCases = findViewById(R.id.cr_closedaggregateet);
        totalAggregateCases = findViewById(R.id.cr_totalaggregateet);
        actualCollection = findViewById(R.id.cr_actualcollet);
        NumOfDealerVisits = findViewById(R.id.cr_nodealeret);
        paidstock_ev = findViewById(R.id.paidstock_ev);
        parksell_ev = findViewById(R.id.parksell_ev);
        royalty_select_tv = findViewById(R.id.royalty_select_tv);

        //Select items
        zone = (TextView) findViewById(R.id.zonelbl);
        zoneSelect = (TextView) findViewById(R.id.zone_selectview);
        dealer_category_selectview = findViewById(R.id.dealer_category_selectview);
        stksevdaysSelect = findViewById(R.id.cr_stockrefreshedsv);

        try {
            dealername = getIntent().getStringExtra("dealername");
            dealer_code = getIntent().getStringExtra("dealer_code");
            tempdealer_code = getIntent().getStringExtra("tempdealer_code");
            editJson = getIntent().getStringExtra("editJson");
        } catch (Exception e) {
            e.printStackTrace();
        }

        week_title_tv.setText(dealername);

        if (dealername.equalsIgnoreCase("Others")) {
            dealername_ll.setVisibility(View.VISIBLE);
            zone_ll.setVisibility(View.VISIBLE);
            statecity_lbl_ll.setVisibility(View.VISIBLE);
            statecity_ev_ll.setVisibility(View.VISIBLE);
            cr_dealer_category_lbl.setVisibility(View.VISIBLE);
            dealer_category_selectview.setVisibility(View.VISIBLE);
            dealer_category_selectview_emtpy.setVisibility(View.VISIBLE);

        } else {
            dealername_ll.setVisibility(View.GONE);
            zone_ll.setVisibility(View.GONE);
            statecity_lbl_ll.setVisibility(View.GONE);
            statecity_ev_ll.setVisibility(View.GONE);
            cr_dealer_category_lbl.setVisibility(View.GONE);
            dealer_category_selectview.setVisibility(View.GONE);
            dealer_category_selectview_emtpy.setVisibility(View.GONE);
        }

        omsCode.setText(dealer_code);

        if (!dealername.equalsIgnoreCase("Others")) {
            dealer_category_selectview.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        } else {
            dealer_category_selectview.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_down_black_24dp, 0);
        }

        if (!dealername.equalsIgnoreCase("Others")) {
            zoneSelect.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        } else {
            dealer_category_selectview.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_down_black_24dp, 0);
        }


        DatePickerDialog.OnDateSetListener ddate = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                generateDatePickerAAT();
            }

        };


        actualActivationTime.setOnClickListener(v -> new DatePickerDialog(CreateReportActivity.this, ddate, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show());


        backIV.setOnClickListener(v -> {
            Intent intent = new Intent(CreateReportActivity.this, WeeklyReport.class);
            startActivity(intent);
        });

        crClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreateReportActivity.this, WeeklyReport.class);
                startActivity(intent);
            }
        });


        LprCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LPRWarningDialog mDialog = new LPRWarningDialog(CreateReportActivity.this,false);
                mDialog.show();
            }
        });


        week_end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (weekedatearray.length != 0) {
                    setAlertDialog(CreateReportActivity.this, "Week", weekedatearray);
                } else {
                    Toast.makeText(getApplicationContext(), "No Week data.", Toast.LENGTH_LONG).show();
                }
            }
        });

        zoneSelect.setOnClickListener(v -> {
            if (dealername.equalsIgnoreCase("Others")) {
                setAlertDialog(CreateReportActivity.this, "Zone", zoneArray);
            }

        });

        dealer_category_selectview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (dealername.equalsIgnoreCase("Others")) {
                    setAlertDialog(CreateReportActivity.this, "Dealer Type", dealerCategoryArray);
                    //dealerCategory.setText(weekSelectItem(dealerCategoryArray));
                }
            }
        });
        stksevdaysSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setAlertDialog(CreateReportActivity.this, "Stock Refreshed within 7 Days", stksevdaysArray);
                //dealerCategory.setText(weekSelectItem(dealerCategoryArray));
            }
        });

        royalty_select_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setAlertDialog(CreateReportActivity.this, "Royalty collection", stksevdaysArray);
            }
        });


        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Date c = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String cformattedDate = df.format(c);
                createdDateOn = cformattedDate;
                Log.d("CreatedDateOn Date", createdDateOn);


                // createdDateOn=cr_day.getText().toString()+cr_date.getText().toString();


                if (CommonMethods.isInternetWorking(CreateReportActivity.this)) {

                    String userType = CommonMethods.getstringvaluefromkey(CreateReportActivity.this, "user_type");

                    String text = week_start_date.getText().toString().trim() + week_end_date.getText().toString().trim();

                /*       if(CommonMethods.getstringvaluefromkey(CreateReportActivity.this,userType+dealer_code+text)
                                .equalsIgnoreCase(userType+dealer_code+text)){

                            Toast.makeText(getApplicationContext(), "Please select another week this one already created.", Toast.LENGTH_LONG).show();
                        }else{*/

                    boolean result = isValuesEmpty();

                    if (result == true) {
                        // custom dialog
                        Dialog dialog = new Dialog(CreateReportActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                        dialog.setContentView(R.layout.common_cus_dialog);
                        //dialog.setTitle("Custom Dialog");

                        TextView Message, Message2;
                        Button cancel, Confirm;
                        Message = dialog.findViewById(R.id.Message);
                        Message2 = dialog.findViewById(R.id.Message2);
                        cancel = dialog.findViewById(R.id.cancel);
                        Confirm = dialog.findViewById(R.id.Confirm);
                        Message.setText("Report once submitted cannot be changed ");
                        Message2.setText("Go ahead and Submit ?");


                        Confirm.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                submitReportDetails(userType, text);
                                dialog.dismiss();
                            }
                        });

                        cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                dialog.dismiss();
                            }
                        });

                        dialog.show();


                    } else {
                        Toast.makeText(getApplicationContext(), "Please Fill all mandatory Field.", Toast.LENGTH_LONG).show();

                    }

                    //   }


                } else {
                    CommonMethods.alertMessage(CreateReportActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
                }
            }
        });

        week_save_btn.setOnClickListener(v -> {


            String userType = CommonMethods.getstringvaluefromkey(CreateReportActivity.this, "user_type");

            String text = week_start_date.getText().toString().trim() + week_end_date.getText().toString().trim();


            if (CommonMethods.getstringvaluefromkey(CreateReportActivity.this, userType + dealer_code + text)
                    .equalsIgnoreCase(userType + dealer_code + text)) {

                if (!editJson.isEmpty()) {

                    week_report_save_data(userType + dealer_code + text);
                    CommonMethods.setvalueAgainstKey(CreateReportActivity.this, userType + dealer_code + text, userType + dealer_code + text);
                    Intent intent = new Intent(CreateReportActivity.this, WeeklyReport.class);
                    startActivity(intent);
                    finish();

                } else {
                    Toast.makeText(getApplicationContext(), "Please select another week this one already created.", Toast.LENGTH_LONG).show();
                }

            } else {

                week_report_save_data(userType + dealer_code + text);
                CommonMethods.setvalueAgainstKey(CreateReportActivity.this, userType + dealer_code + text, userType + dealer_code + text);

                Intent intent = new Intent(CreateReportActivity.this, WeeklyReport.class);
                startActivity(intent);
                finish();
            }

        });


        if (!editJson.equalsIgnoreCase("")) {

            setData(editJson);

        }

        if (CommonMethods.isInternetWorking(CreateReportActivity.this)) {
            weekReportReq();
        } else {
            CommonMethods.alertMessage(CreateReportActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
        }

        dataCalcluationSetData();

        if (!dealername.equalsIgnoreCase("Others")) {
            state.setEnabled(false);
            city.setEnabled(false);
            omsCode.setEnabled(false);
            amToken.setEnabled(false);
            amName.setEnabled(false);
            omsLeads.setEnabled(false);
            zone.setEnabled(false);
        }


    }

    int stockTotal = 0, conversionIEP = 0, conversionNCD = 0, totalProc = 0, totalSales = 0, totalWalkin = 0, totalArgg = 0, warrantyP = 0;

    private void dataCalcluationSetData() {


        try {

            osOfMonth.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {

                    int stockData = 0;
                    if (s.toString().equals("")) {
                    } else {
                        stockData = Integer.parseInt(s.toString());
                    }


                    int totalproc = 0, totalSales = 0;
                    if (!total_procurement.getText().toString().trim().equalsIgnoreCase("")) {
                        totalproc = Integer.parseInt(total_procurement.getText().toString());
                    }
                    if (!total_sales.getText().toString().trim().equalsIgnoreCase("")) {
                        totalSales = Integer.parseInt(total_sales.getText().toString());
                    }


                    stockTotal = (stockData + totalproc) - totalSales;

                    currentStock.setText(String.valueOf(stockTotal));


                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    if (s.length() > 0) {
                        int number = Integer.parseInt(s.toString());
                        stockTotal -= number;
                    }

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });

            iep_procure.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {

                    int iepData = 0;
                    if (s.toString().equals("")) {
                    } else {
                        iepData = Integer.parseInt(s.toString());
                    }

                    int leadiep = 0;

                    if (!leadstIEP.getText().toString().trim().equalsIgnoreCase("")) {
                        leadiep = Integer.parseInt(leadstIEP.getText().toString());
                    }


                    try {

                        Double iepDatas, leadieps,conversionD;

                        iepDatas = Double.valueOf(iepData);
                        leadieps = Double.valueOf(leadiep);

                        conversionD = iepDatas / leadieps;

                        conversionD = conversionD * 100;
                        conversionIEP = (int) Math.round(conversionD);
                        if(conversionIEP<0)conversionIEP=0;


                        /*conversionIEP = iepData / leadiep;
                        conversionIEP = conversionIEP * 100;*/

                    } catch (ArithmeticException e) {
                        conversionIEP = 0;
                    }

                    conversion_IEP.setText(String.valueOf(conversionIEP));


                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    if (s.length() > 0) {
                        int number = Integer.parseInt(s.toString());
                        conversionIEP -= number;
                    }

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });

            leadstIEP.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {

                    int leadiep = 0;
                    if (s.toString().equals("")) {
                    } else {
                        leadiep = Integer.parseInt(s.toString());
                    }

                    int iepData = 0;

                    if (!iep_procure.getText().toString().trim().equalsIgnoreCase("")) {
                        iepData = Integer.parseInt(iep_procure.getText().toString());
                    }

                    try {

                        Double iepDatas, leadieps,conversionD;

                        iepDatas = Double.valueOf(iepData);
                        leadieps = Double.valueOf(leadiep);

                        conversionD = iepDatas / leadieps;

                        conversionD = conversionD * 100;
                        conversionIEP = (int) Math.round(conversionD);

                        if(conversionIEP<0)conversionIEP=0;

                       /* conversionIEP = iepData / leadiep;
                        conversionIEP = conversionIEP * 100;*/

                    } catch (ArithmeticException e) {
                        conversionIEP = 0;
                    }

                    conversion_IEP.setText(String.valueOf(conversionIEP));


                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    if (s.length() > 0) {
                        int number = Integer.parseInt(s.toString());
                        conversionIEP -= number;
                    }

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });

            ncd_procure.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {

                    int ncd = 0;
                    if (s.toString().equals("")) {
                    } else {
                        ncd = Integer.parseInt(s.toString());
                    }


                    int leadncd = 0;

                    if (!leadstNCD.getText().toString().trim().equalsIgnoreCase("")) {
                        leadncd = Integer.parseInt(leadstNCD.getText().toString());
                    }
                    try {

                        Double ncds, leadncds,conversionNCDD;

                        ncds = Double.valueOf(ncd);
                        leadncds = Double.valueOf(leadncd);

                        conversionNCDD = ncds / leadncds;

                        conversionNCDD = conversionNCDD * 100;
                        conversionNCD = (int) Math.round(conversionNCDD);
                        if(conversionNCD<0)conversionNCD=0;
                        //conversionNCD = ncd / leadncd;
                    } catch (ArithmeticException e) {
                        conversionNCD = 0;
                    }

                    conversion_NCD.setText(String.valueOf(conversionNCD));

                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    if (s.length() > 0) {
                        int number = Integer.parseInt(s.toString());
                        conversionNCD -= number;
                    }

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });

            leadstNCD.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {

                    int leadncd = 0;
                    if (s.toString().equals("")) {
                    } else {
                        leadncd = Integer.parseInt(s.toString());
                    }
                    int ncd = 0;

                    if (!ncd_procure.getText().toString().trim().equalsIgnoreCase("")) {
                        ncd = Integer.parseInt(ncd_procure.getText().toString());
                    }
                    try {

                        Double ncds, leadncds,conversionNCDD;

                        ncds = Double.valueOf(ncd);
                        leadncds = Double.valueOf(leadncd);

                        conversionNCDD = ncds / leadncds;

                        conversionNCDD = conversionNCDD * 100;
                        conversionNCD = (int) Math.round(conversionNCDD);
                        if(conversionNCD<0)conversionNCD=0;
                        //conversionNCD = ncd / leadncd;
                    } catch (ArithmeticException e) {
                        conversionNCD = 0;
                    }

                    conversion_NCD.setText(String.valueOf(conversionNCD));

                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    if (s.length() > 0) {
                        int number = Integer.parseInt(s.toString());
                        conversionNCD -= number;
                    }

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });

            cpt_procure.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {

                    int cpt = 0;
                    if (s.toString().equals("")) {
                    } else {
                        cpt = Integer.parseInt(s.toString());
                    }

                    int self = 0, iep = 0, ncd = 0, xmart = 0;

                    if (!self_procure.getText().toString().trim().equalsIgnoreCase("")) {
                        self = Integer.parseInt(self_procure.getText().toString());
                    }
                    if (!iep_procure.getText().toString().trim().equalsIgnoreCase("")) {
                        iep = Integer.parseInt(iep_procure.getText().toString());
                    }
                    if (!ncd_procure.getText().toString().trim().equalsIgnoreCase("")) {
                        ncd = Integer.parseInt(ncd_procure.getText().toString());
                    }
                    if (!xmart_nonIEP.getText().toString().trim().equalsIgnoreCase("")) {
                        xmart = Integer.parseInt(xmart_nonIEP.getText().toString());
                    }

                    try {
                        totalProc = cpt + self + iep + ncd + xmart;
                    } catch (ArithmeticException e) {
                        totalProc = 0;
                    }

                    total_procurement.setText(String.valueOf(totalProc));





                    // newly add
                    int stockData = 0, totalSales = 0;
                    if (!osOfMonth.getText().toString().trim().equalsIgnoreCase("")) {
                        stockData = Integer.parseInt(osOfMonth.getText().toString());
                    }
                    if (!total_sales.getText().toString().trim().equalsIgnoreCase("")) {
                        totalSales = Integer.parseInt(total_sales.getText().toString());
                    }
                    stockTotal = (stockData + totalProc) - totalSales;
                    currentStock.setText(String.valueOf(stockTotal));


                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    if (s.length() > 0) {
                        int number = Integer.parseInt(s.toString());
                        totalProc -= number;
                    }

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });
            self_procure.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {

                    int self = 0;
                    if (s.toString().equals("")) {
                    } else {
                        self = Integer.parseInt(s.toString());
                    }

                    int cpt = 0, iep = 0, ncd = 0, xmart = 0;

                    if (!cpt_procure.getText().toString().trim().equalsIgnoreCase("")) {
                        cpt = Integer.parseInt(cpt_procure.getText().toString());
                    }
                    if (!iep_procure.getText().toString().trim().equalsIgnoreCase("")) {
                        iep = Integer.parseInt(iep_procure.getText().toString());
                    }
                    if (!ncd_procure.getText().toString().trim().equalsIgnoreCase("")) {
                        ncd = Integer.parseInt(ncd_procure.getText().toString());
                    }
                    if (!xmart_nonIEP.getText().toString().trim().equalsIgnoreCase("")) {
                        xmart = Integer.parseInt(xmart_nonIEP.getText().toString());
                    }

                    try {
                        totalProc = cpt + self + iep + ncd + xmart;
                    } catch (ArithmeticException e) {
                        totalProc = 0;
                    }

                    total_procurement.setText(String.valueOf(totalProc));

                    // newly add
                    int stockData = 0, totalSales = 0;
                    if (!osOfMonth.getText().toString().trim().equalsIgnoreCase("")) {
                        stockData = Integer.parseInt(osOfMonth.getText().toString());
                    }
                    if (!total_sales.getText().toString().trim().equalsIgnoreCase("")) {
                        totalSales = Integer.parseInt(total_sales.getText().toString());
                    }
                    stockTotal = (stockData + totalProc) - totalSales;
                    currentStock.setText(String.valueOf(stockTotal));


                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    if (s.length() > 0) {
                        int number = Integer.parseInt(s.toString());
                        totalProc -= number;
                    }

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });
            iep_procure.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {

                    int iep = 0;
                    if (s.toString().equals("")) {
                    } else {
                        iep = Integer.parseInt(s.toString());
                    }

                    int cpt = 0, self = 0, ncd = 0, xmart = 0;

                    if (!cpt_procure.getText().toString().trim().equalsIgnoreCase("")) {
                        cpt = Integer.parseInt(cpt_procure.getText().toString());
                    }
                    if (!self_procure.getText().toString().trim().equalsIgnoreCase("")) {
                        self = Integer.parseInt(self_procure.getText().toString());
                    }
                    if (!ncd_procure.getText().toString().trim().equalsIgnoreCase("")) {
                        ncd = Integer.parseInt(ncd_procure.getText().toString());
                    }
                    if (!xmart_nonIEP.getText().toString().trim().equalsIgnoreCase("")) {
                        xmart = Integer.parseInt(xmart_nonIEP.getText().toString());
                    }

                    try {
                        totalProc = cpt + self + iep + ncd + xmart;
                    } catch (ArithmeticException e) {
                        totalProc = 0;
                    }

                    total_procurement.setText(String.valueOf(totalProc));


                    // newly add
                    int stockData = 0, totalSales = 0;
                    if (!osOfMonth.getText().toString().trim().equalsIgnoreCase("")) {
                        stockData = Integer.parseInt(osOfMonth.getText().toString());
                    }
                    if (!total_sales.getText().toString().trim().equalsIgnoreCase("")) {
                        totalSales = Integer.parseInt(total_sales.getText().toString());
                    }
                    stockTotal = (stockData + totalProc) - totalSales;
                    currentStock.setText(String.valueOf(stockTotal));


                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    if (s.length() > 0) {
                        int number = Integer.parseInt(s.toString());
                        totalProc -= number;
                    }

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });
            ncd_procure.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {

                    int ncd = 0;
                    if (s.toString().equals("")) {
                    } else {
                        ncd = Integer.parseInt(s.toString());
                    }
                    int cpt = 0, self = 0, iep = 0, xmart = 0;

                    if (!cpt_procure.getText().toString().trim().equalsIgnoreCase("")) {
                        cpt = Integer.parseInt(cpt_procure.getText().toString());
                    }
                    if (!self_procure.getText().toString().trim().equalsIgnoreCase("")) {
                        self = Integer.parseInt(self_procure.getText().toString());
                    }
                    if (!iep_procure.getText().toString().trim().equalsIgnoreCase("")) {
                        iep = Integer.parseInt(iep_procure.getText().toString());
                    }
                    if (!xmart_nonIEP.getText().toString().trim().equalsIgnoreCase("")) {
                        xmart = Integer.parseInt(xmart_nonIEP.getText().toString());
                    }

                    try {
                        totalProc = cpt + self + iep + ncd + xmart;
                    } catch (ArithmeticException e) {
                        totalProc = 0;
                    }

                    total_procurement.setText(String.valueOf(totalProc));

                    // newly add
                    int stockData = 0, totalSales = 0;
                    if (!osOfMonth.getText().toString().trim().equalsIgnoreCase("")) {
                        stockData = Integer.parseInt(osOfMonth.getText().toString());
                    }
                    if (!total_sales.getText().toString().trim().equalsIgnoreCase("")) {
                        totalSales = Integer.parseInt(total_sales.getText().toString());
                    }
                    stockTotal = (stockData + totalProc) - totalSales;
                    currentStock.setText(String.valueOf(stockTotal));


                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    if (s.length() > 0) {
                        int number = Integer.parseInt(s.toString());
                        totalProc -= number;
                    }

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });

            xmart_nonIEP.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {

                    int xmart = 0;
                    if (s.toString().equals("")) {
                    } else {
                        xmart = Integer.parseInt(s.toString());
                    }
                    int cpt = 0, self = 0, iep = 0, ncd = 0;

                    if (!cpt_procure.getText().toString().trim().equalsIgnoreCase("")) {
                        cpt = Integer.parseInt(cpt_procure.getText().toString());
                    }
                    if (!self_procure.getText().toString().trim().equalsIgnoreCase("")) {
                        self = Integer.parseInt(self_procure.getText().toString());
                    }
                    if (!iep_procure.getText().toString().trim().equalsIgnoreCase("")) {
                        iep = Integer.parseInt(iep_procure.getText().toString());
                    }
                    if (!ncd_procure.getText().toString().trim().equalsIgnoreCase("")) {
                        ncd = Integer.parseInt(ncd_procure.getText().toString());
                    }

                    try {
                        totalProc = cpt + self + iep + ncd + xmart;
                    } catch (ArithmeticException e) {
                        totalProc = 0;
                    }

                    total_procurement.setText(String.valueOf(totalProc));
                    // newly add
                    int stockData = 0, totalSales = 0;
                    if (!osOfMonth.getText().toString().trim().equalsIgnoreCase("")) {
                        stockData = Integer.parseInt(osOfMonth.getText().toString());
                    }
                    if (!total_sales.getText().toString().trim().equalsIgnoreCase("")) {
                        totalSales = Integer.parseInt(total_sales.getText().toString());
                    }
                    stockTotal = (stockData + totalProc) - totalSales;
                    currentStock.setText(String.valueOf(stockTotal));

                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    if (s.length() > 0) {
                        int number = Integer.parseInt(s.toString());
                        totalProc -= number;
                    }

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });

            retail_Sales.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {

                    int retails = 0;
                    if (s.toString().equals("")) {
                    } else {
                        retails = Integer.parseInt(s.toString());
                    }
                    int offload = 0;

                    if (!offload_sales.getText().toString().trim().equalsIgnoreCase("")) {
                        offload = Integer.parseInt(offload_sales.getText().toString());
                    }

                    try {
                        totalSales = retails + offload;
                    } catch (ArithmeticException e) {
                        totalSales = 0;
                    }

                    total_sales.setText(String.valueOf(totalSales));

                    // newly add
                    int stockData = 0, totalProc = 0;
                    if (!osOfMonth.getText().toString().trim().equalsIgnoreCase("")) {
                        stockData = Integer.parseInt(osOfMonth.getText().toString());
                    }
                    if (!total_procurement.getText().toString().trim().equalsIgnoreCase("")) {
                        totalProc = Integer.parseInt(total_procurement.getText().toString());
                    }
                    stockTotal = (stockData + totalProc) - totalSales;
                    currentStock.setText(String.valueOf(stockTotal));


                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    if (s.length() > 0) {
                        int number = Integer.parseInt(s.toString());
                        totalSales -= number;
                    }

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });

            offload_sales.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {

                    int offload = 0;
                    if (s.toString().equals("")) {
                    } else {
                        offload = Integer.parseInt(s.toString());
                    }
                    int retails = 0;

                    if (!retail_Sales.getText().toString().trim().equalsIgnoreCase("")) {
                        retails = Integer.parseInt(retail_Sales.getText().toString());
                    }

                    try {
                        totalSales = retails + offload;
                    } catch (ArithmeticException e) {
                        totalSales = 0;
                    }

                    total_sales.setText(String.valueOf(totalSales));

                    // newly add
                    int stockData = 0, totalProc = 0;
                    if (!osOfMonth.getText().toString().trim().equalsIgnoreCase("")) {
                        stockData = Integer.parseInt(osOfMonth.getText().toString());
                    }
                    if (!total_procurement.getText().toString().trim().equalsIgnoreCase("")) {
                        totalProc = Integer.parseInt(total_procurement.getText().toString());
                    }
                    stockTotal = (stockData + totalProc) - totalSales;
                    currentStock.setText(String.valueOf(stockTotal));


                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    if (s.length() > 0) {
                        int number = Integer.parseInt(s.toString());
                        totalSales -= number;
                    }

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });

            walkinstOMS.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {

                    int walkin = 0;
                    if (s.toString().equals("")) {
                    } else {
                        walkin = Integer.parseInt(s.toString());
                    }
                    int dwalkin = 0;

                    if (!DoOWalkins.getText().toString().trim().equalsIgnoreCase("")) {
                        dwalkin = Integer.parseInt(DoOWalkins.getText().toString());
                    }

                    try {
                        totalWalkin = dwalkin + walkin;
                    } catch (ArithmeticException e) {
                        totalWalkin = 0;
                    }

                    totalWalkins.setText(String.valueOf(totalWalkin));


                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    if (s.length() > 0) {
                        int number = Integer.parseInt(s.toString());
                        totalWalkin -= number;
                    }

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });

            DoOWalkins.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {

                    int dwalkin = 0;
                    if (s.toString().equals("")) {
                    } else {
                        dwalkin = Integer.parseInt(s.toString());
                    }
                    int walkin = 0;

                    if (!walkinstOMS.getText().toString().trim().equalsIgnoreCase("")) {
                        walkin = Integer.parseInt(walkinstOMS.getText().toString());
                    }

                    try {
                        totalWalkin = dwalkin + walkin;
                    } catch (ArithmeticException e) {
                        totalWalkin = 0;
                    }

                    totalWalkins.setText(String.valueOf(totalWalkin));

                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    if (s.length() > 0) {
                        int number = Integer.parseInt(s.toString());
                        totalWalkin -= number;
                    }

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });

            openAggregateCases.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {

                    int open = 0;
                    if (s.toString().equals("")) {
                    } else {
                        open = Integer.parseInt(s.toString());
                    }
                    int close = 0;

                    if (!closedAggregateCases.getText().toString().trim().equalsIgnoreCase("")) {
                        close = Integer.parseInt(closedAggregateCases.getText().toString());
                    }

                    try {
                        totalArgg = open + close;
                    } catch (ArithmeticException e) {
                        totalArgg = 0;
                    }

                    totalAggregateCases.setText(String.valueOf(totalArgg));


                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    if (s.length() > 0) {
                        int number = Integer.parseInt(s.toString());
                        totalArgg -= number;
                    }

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });

            closedAggregateCases.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {

                    int close = 0;
                    if (s.toString().equals("")) {
                    } else {
                        close = Integer.parseInt(s.toString());
                    }
                    int opnen = 0;

                    if (!openAggregateCases.getText().toString().trim().equalsIgnoreCase("")) {
                        opnen = Integer.parseInt(openAggregateCases.getText().toString());
                    }

                    try {
                        totalArgg = opnen + close;
                    } catch (ArithmeticException e) {
                        totalArgg = 0;
                    }

                    totalAggregateCases.setText(String.valueOf(totalArgg));


                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    if (s.length() > 0) {
                        int number = Integer.parseInt(s.toString());
                        totalArgg -= number;
                    }

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });


            warrantyNos.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {

                    int warrantyno = 0;
                    if (s.toString().equals("")) {
                    } else {
                        warrantyno = Integer.parseInt(s.toString());
                    }
                    int totalSales = 0;

                    if (!total_sales.getText().toString().trim().equalsIgnoreCase("")) {
                        totalSales = Integer.parseInt(total_sales.getText().toString());
                    }
                    try {

                        Double warrantynos, totalSalse,warrantyPD;

                        warrantynos = Double.valueOf(warrantyno);
                        totalSalse = Double.valueOf(totalSales);

                        warrantyPD = warrantynos / totalSalse;

                        warrantyPD = warrantyPD * 100;
                        warrantyP = (int) Math.round(warrantyPD);
                        if(warrantyP<0)warrantyP=0;

                    } catch (ArithmeticException e) {
                        warrantyP = 0;
                    }

                    warrantyPen.setText(String.valueOf(warrantyP));


                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    if (s.length() > 0) {
                        int number = Integer.parseInt(s.toString());
                        warrantyP -= number;
                    }

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


    }




    private void setData(String editJson) {

        try {


            WeekReportSubmitModel setData ;
            Gson gson = new Gson();
            setData = gson.fromJson(editJson.toString(), WeekReportSubmitModel.class);

            try {

                dealername = setData.getDealerName();
                dealer_code = setData.getDealerCode();
                tempdealer_code = setData.getTempdealer_code();

                week_title_tv.setText(dealername);

                if (dealername.equalsIgnoreCase("Others")) {
                    dealername_ll.setVisibility(View.VISIBLE);

                    dealername_tv.setText(dealername);
                } else {
                    dealername_ll.setVisibility(View.GONE);
                }

                omsCode.setText(setData.getDealerCode());

            } catch (Exception e) {
                e.printStackTrace();
            }


            zoneSelect.setText(setData.getZone());
            state.setText(setData.getState());
            city.setText(setData.getCity());
            dealer_code = setData.getDealerCode();
            acSapCode.setText(setData.getAccountSapCode());


            dealer_category_selectview.setText(setData.getDealerCategory());
            amToken.setText(setData.getAmTokenNo());
            amName.setText(setData.getAmName());
            shTokenno.setText(setData.getSmTokenNo());
            shName.setText(setData.getSmName());
            osOfMonth.setText(setData.getStcokOpeningMonth());
            stksevdaysSelect.setText(setData.getStockRefreshed());
            stkLtSixty.setText(setData.getStockLess60Days());
            stkGtSixty.setText(setData.getStockGrater60Days());

            if (setData.getCurrentStock().isEmpty()) {
                currentStock.setText("0");
            } else {
                currentStock.setText(setData.getCurrentStock());
            }


            pAStock.setText(setData.getActualStock());
            cpt_procure.setText(setData.getCpt());
            self_procure.setText(setData.getSelfProcurement());
            iep_procure.setText(setData.getIep());
            ncd_procure.setText(setData.getNcd());
            leadstIEP.setText(setData.getIepLead());
            leadstNCD.setText(setData.getNcd());

            if (setData.getCurrentStock().isEmpty()) {
                conversion_IEP.setText("0");
            } else {
                conversion_IEP.setText(setData.getIepConversion());
            }

            if (setData.getCurrentStock().isEmpty()) {
                conversion_NCD.setText("0");
            } else {
                conversion_NCD.setText(setData.getNcdConversionPer());

            }


            Num_Of_highestBids.setText(setData.getNoOfBids());
            xmart_nonIEP.setText(setData.getXMart());
            total_procurement.setText(setData.getTotalProcurement());
            retail_Sales.setText(setData.getRetailSales());
            offload_sales.setText(setData.getOffloadSales());
            total_sales.setText(setData.getTotalSales());
            bookingInHand.setText(setData.getBookingInHand());
            conversion_OMS.setText(setData.getOmsLeadConversion());
            omsLeads.setText(setData.getOmsLeads());
            walkinstOMS.setText(setData.getOmsWalkinThrough());
            numOfFinCases.setText(setData.getNoOfFinanceCases());
            warrantyNos.setText(setData.getWarrantyNo());
            warrantyValue.setText(setData.getWarrantyValues());
            warrantyPen.setText(setData.getWarrantyPercentage());
            openAggregateCases.setText(setData.getOpenAggregateCases());
            closedAggregateCases.setText(setData.getClosedAggregateCases());
            totalAggregateCases.setText(setData.getTotalAggregateCases());
            actualCollection.setText(setData.getActualCollection());
            NumOfDealerVisits.setText(setData.getNoOfDealerVisits());
            createdDateOn = setData.getCreatedOn().toString();
            week_start_date.setText(setData.getFormDate());
            week_end_date.setText(setData.getToDate());

            DoOWalkins.setText(String.valueOf(setData.getDirectWalkin()));
            conversion_IEP.setText(setData.getIepConversion());

            parksell_ev.setText(setData.getParkAndSellStock());
            paidstock_ev.setText(setData.getPaidUpStock());
            royalty_select_tv.setText(setData.getRoyaltyCollection());

            if (setData.getDealerName().isEmpty()) {
                week_title_tv.setText(dealername);
            } else {
                week_title_tv.setText(setData.getDealerName());
            }

            if (setData.getActualActivationTime() != null) {
                String actActivDateValue = setData.getActualActivationTime().toString();
                String date[] = actActivDateValue.split("-");
                try {
                    String month = date[1];
                    String year = date[0];
                    actualActivationTime.setText(month + " | " + year);
                    actualActivation_Value = actActivDateValue;

                } catch (Exception e) {
                    actualActivationTime.setText(actActivDateValue);

                }

            } else {
                actualActivationTime.setText("");
            }


        } catch (Exception e) {

        }
    }

    private void submitReportDetails(String userType, String text) {

        setData();

        ArrayList<WeekReportSubmitModel> rq = new ArrayList<>();

        rq.add(weeklyReportSubmitModel);

        WRSubmitService wrSubmitService = new WRSubmitService();

        wrSubmitService.pushMultipleData(rq, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                Response<amsCreateRes> mRes = (Response<amsCreateRes>) obj;
                amsCreateRes mData = mRes.body();

                if (mData.getStatus().equalsIgnoreCase("SUCCESS")) {
                    Toast.makeText(getApplicationContext(), "Successfully Submitted", Toast.LENGTH_LONG).show();
                    CommonMethods.setvalueAgainstKey(CreateReportActivity.this, userType + dealer_code + text, userType + dealer_code + text);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), mData.getMessage().toString(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                Toast.makeText(getApplicationContext(), "Something went wrong Please check try again.", Toast.LENGTH_LONG).show();
            }
        });
    }







    private void setData() {

        weeklyReportSubmitModel.setZone(zoneSelect.getText().toString());
        weeklyReportSubmitModel.setState(state.getText().toString());
        weeklyReportSubmitModel.setCity(city.getText().toString());
        weeklyReportSubmitModel.setDealerCode(dealer_code);
        weeklyReportSubmitModel.setDealerCategory(dealer_category_selectview.getText().toString());

        weeklyReportSubmitModel.setAmTokenNo(amToken.getText().toString());
        weeklyReportSubmitModel.setAmName(amName.getText().toString());
        weeklyReportSubmitModel.setSmTokenNo(shTokenno.getText().toString());
        weeklyReportSubmitModel.setSmName(shName.getText().toString());
        weeklyReportSubmitModel.setStcokOpeningMonth(osOfMonth.getText().toString());
        weeklyReportSubmitModel.setStockRefreshed(stksevdaysSelect.getText().toString());
        weeklyReportSubmitModel.setStockLess60Days(stkLtSixty.getText().toString());
        weeklyReportSubmitModel.setStockGrater60Days(stkGtSixty.getText().toString());
        weeklyReportSubmitModel.setCurrentStock(currentStock.getText().toString());
        weeklyReportSubmitModel.setActualStock(pAStock.getText().toString());
        weeklyReportSubmitModel.setCpt(cpt_procure.getText().toString());
        weeklyReportSubmitModel.setSelfProcurement(self_procure.getText().toString());
        weeklyReportSubmitModel.setIep(iep_procure.getText().toString());
        weeklyReportSubmitModel.setNcd(ncd_procure.getText().toString());
        weeklyReportSubmitModel.setIepLead(leadstIEP.getText().toString());
        weeklyReportSubmitModel.setNcpLead(leadstNCD.getText().toString());

        weeklyReportSubmitModel.setNcdConversionPer(conversion_IEP.getText().toString());
        weeklyReportSubmitModel.setNcdConversionPer(conversion_NCD.getText().toString());
        weeklyReportSubmitModel.setNoOfBids(Num_Of_highestBids.getText().toString());
        weeklyReportSubmitModel.setXMart(xmart_nonIEP.getText().toString());
        weeklyReportSubmitModel.setTotalProcurement(total_procurement.getText().toString());
        weeklyReportSubmitModel.setRetailSales(retail_Sales.getText().toString());
        weeklyReportSubmitModel.setOffloadSales(offload_sales.getText().toString());
        weeklyReportSubmitModel.setTotalSales(total_sales.getText().toString());
        weeklyReportSubmitModel.setBookingInHand(bookingInHand.getText().toString());
        weeklyReportSubmitModel.setOmsLeadConversion(conversion_OMS.getText().toString());
        weeklyReportSubmitModel.setOmsLeads(omsLeads.getText().toString());
        weeklyReportSubmitModel.setOmsWalkinThrough(walkinstOMS.getText().toString());
        weeklyReportSubmitModel.setNoOfFinanceCases(numOfFinCases.getText().toString());
        weeklyReportSubmitModel.setWarrantyNo(warrantyNos.getText().toString());
        weeklyReportSubmitModel.setWarrantyValues(warrantyValue.getText().toString());
        weeklyReportSubmitModel.setWarrantyPercentage(warrantyPen.getText().toString());
        weeklyReportSubmitModel.setOpenAggregateCases(openAggregateCases.getText().toString());
        weeklyReportSubmitModel.setClosedAggregateCases(closedAggregateCases.getText().toString());
        weeklyReportSubmitModel.setTotalAggregateCases(totalAggregateCases.getText().toString());
        weeklyReportSubmitModel.setActualCollection(actualCollection.getText().toString());

        weeklyReportSubmitModel.setNoOfDealerVisits(NumOfDealerVisits.getText().toString());

        createdDateOn = cr_day.getText().toString() + cr_date.getText().toString();

        weeklyReportSubmitModel.setCreatedOn(createdDateOn);
        weeklyReportSubmitModel.setFormDate(week_start_date.getText().toString());
        weeklyReportSubmitModel.setToDate(week_end_date.getText().toString());
        weeklyReportSubmitModel.setAccountSapCode(acSapCode.getText().toString());
        // weeklyReportSubmitModel.setActivationTime(activationTime_Value);
        weeklyReportSubmitModel.setActualActivationTime(actualActivation_Value);

        weeklyReportSubmitModel.setPaidUpStock(paidstock_ev.getText().toString());
        weeklyReportSubmitModel.setParkAndSellStock(parksell_ev.getText().toString());
        weeklyReportSubmitModel.setRoyaltyCollection(royalty_select_tv.getText().toString());

        if (DoOWalkins.getText().toString().trim().isEmpty()) {
            DoOWalkins.setText(String.valueOf(0));
        } else {
            try {
                int i = Integer.parseInt(DoOWalkins.getText().toString());
                weeklyReportSubmitModel.setDirectWalkin(i);
            } catch (NumberFormatException ex) { // handle your exception
                ex.printStackTrace();
               // Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
            }

        }
        weeklyReportSubmitModel.setIepConversion(conversion_IEP.getText().toString());

        if(!totalWalkins.getText().toString().equalsIgnoreCase("")){
            weeklyReportSubmitModel.setTotalWalkin(Integer.valueOf(totalWalkins.getText().toString()));
        }else{
            weeklyReportSubmitModel.setTotalWalkin(0);
        }

        if (dealername.equalsIgnoreCase("Others")) {
            weeklyReportSubmitModel.setDealerName(dealername_tv.getText().toString());
        } else {
            weeklyReportSubmitModel.setDealerName(dealername.toString());
        }


    }


    private void setAlertDialog(Activity a, final String strTitle, final String[] arrVal) {

        AlertDialog.Builder alert = new AlertDialog.Builder(a);
        alert.setTitle(strTitle);
        alert.setItems(arrVal, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {


                if (strTitle.equalsIgnoreCase("Week")) {
                    week_end_date.setText(weekedatearray[which]);
                    showDialog();
                    autoFillServiceCall(dealer_code, "AM", weeksdatearray[which], weekedatearray[which]);

                } else if (strTitle.equalsIgnoreCase("Zone")) {
                    zoneSelect.setText(arrVal[which]);
                    zoneVal = arrVal[which];
                } else if (strTitle.equalsIgnoreCase("Dealer Type")) {
                    dealer_category_selectview.setText(arrVal[which]);
                    dealerCatVal = arrVal[which];
                } else if (strTitle.equalsIgnoreCase("Stock Refreshed within 7 Days")) {
                    stksevdaysSelect.setText(arrVal[which]);
                    stksevVal = arrVal[which];
                } else if (strTitle.equalsIgnoreCase("Royalty collection")) {
                    royalty_select_tv.setText(arrVal[which]);
                }

            }
        });
        alert.create();
        alert.show();

    }

    private void showDialog(){
        AlertDialog.Builder alert = new AlertDialog.Builder(CreateReportActivity.this);
        alert.setMessage("Submission for weekly report will be closed by 12.00 pm on Saturday");
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.create();
        alert.show();
    }

    private void week_report_save_data(String keyfor) {


        String commonJson = CommonMethods.getstringvaluefromkey(this, "jsonweek");

        JSONArray new_array = new JSONArray();
        JSONArray old_array = new JSONArray();
        /*JSONObject newJsonObj = new JSONObject();*/

        JSONObject jsonObjMain = new JSONObject();

        Log.i(TAG, "commonJson-in: " + commonJson);


        try {

            if (!commonJson.equalsIgnoreCase("")) {
                jsonObjMain = new JSONObject(commonJson);
                old_array = jsonObjMain.getJSONArray("data");
                for (int i = 0; i < old_array.length(); i++) {
                    new_array.put(old_array.get(i));
                }
                for (int i = 0; i < new_array.length(); i++) {

                    JSONObject updateJson = new JSONObject();
                    updateJson = new_array.getJSONObject(i);

                    if (updateJson.get("tempdealer_code").equals(tempdealer_code)) {

                        Log.i(TAG, "dealer_code-11: " + updateJson.get("dealer_code"));
                        Log.i(TAG, "dealer_code-12: " + dealer_code);

                        new_array.remove(i);

                    } else {
                        // Log.i(TAG, "week_report_save_data-out: "+updateJson.get("dealername"));
                    }

                }

            }


            setData();


            if (dealername.equalsIgnoreCase("Others")) {
                weeklyReportSubmitModel.setDealerName(dealername_tv.getText().toString());
            } else {
                weeklyReportSubmitModel.setDealerName(dealername);
            }



            weeklyReportSubmitModel.setDealerCode(dealer_code);
            weeklyReportSubmitModel.setTempdealer_code(dealer_code + getCreateDate());
            weeklyReportSubmitModel.setCreatedforKey(keyfor);
            weeklyReportSubmitModel.setZone(zoneSelect.getText().toString());


            Gson gson = new Gson();
            String Json = gson.toJson(weeklyReportSubmitModel);  //see firstly above
            newJsonObj = new JSONObject(Json);


            getDate(newJsonObj);

            //  new_array.put(0,newJsonObj);

            new_array.put(newJsonObj);

            jsonObjMain.put("data", new_array);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String saveJsonNew = jsonObjMain.toString(); //http request
        Log.d("saveJsonNew", "saveJsonNew= " + saveJsonNew.toString());

        CommonMethods.setvalueAgainstKey(this, "jsonweek", saveJsonNew);

        Log.d("saveJsonNew", "saveJsonNew= " + saveJsonNew.toString());

    }


    public void getDate(JSONObject newJsonObj) {

        Date now = new Date();
        SimpleDateFormat simpleDateformat = new SimpleDateFormat("E");

        String month = simpleDateformat.format(now);

        Calendar cal = java.util.Calendar.getInstance();
        cal.add(Calendar.MONTH, 1);

        String day = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
        String year = String.valueOf(cal.get(Calendar.YEAR));

        String date = String.valueOf(cal.get(Calendar.MONTH));

        if (date.length() == 1) {
            date = date = "0" + date;
        }

        SimpleDateFormat simpleDateFormat;
        simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        String hour = simpleDateFormat.format(cal.getTime());

        try {
            newJsonObj.put("day", month);
            newJsonObj.put("date", day + "/" + date + "/" + year);
            newJsonObj.put("hour", hour);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public String getCreateDate() {

        Calendar cal = java.util.Calendar.getInstance();
        cal.add(Calendar.MONTH, 1);

        String day = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
        String year = String.valueOf(cal.get(Calendar.YEAR));

        String date = String.valueOf(cal.get(Calendar.MONTH));

        if (date.length() == 1) {
            date = date = "0" + date;
        }

        SimpleDateFormat simpleDateFormat;
        simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        String hour = simpleDateFormat.format(cal.getTime());

        try {
            return day + "-" + date + "-" + year + "-" + hour;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";

    }


    private void weekReportReq() {

        SpinnerManager.showSpinner(CreateReportActivity.this);

        WeekReportServices.getWeekStatus(dealer_code, CreateReportActivity.this, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                SpinnerManager.hideSpinner(CreateReportActivity.this);

                Response<List<weekReportModel>> mRes = (Response<List<weekReportModel>>) obj;
                List<weekReportModel> mData = mRes.body();

                try {

                    if (weeksdate != null) weeksdate.clear();
                    if (weekedate != null) weekedate.clear();

                    for (int i = 0; i < mData.size(); i++) {

                        weeksdate.add(i, mData.get(i).getFromdate());
                        weekedate.add(i, mData.get(i).getTodate());
                    }

                    weeksdatearray = weeksdate.toArray(new String[0]);
                    weekedatearray = weekedate.toArray(new String[0]);

                    /// change later

                    if (weekedatearray.length == 0) {
                        CommonMethods.alertMessage2(CreateReportActivity.this, "No Report to submit \n"
                                + "All report has been submitted for " + dealername);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(CreateReportActivity.this);
            }

        });
    }

    private boolean isValuesEmpty() {
        boolean result;
        if (week_start_date.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Week", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (week_end_date.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Week", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (zoneSelect.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Zone", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (state.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter State ", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (city.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter City ", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (omsCode.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter OMS Code ", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (dealer_category_selectview.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Dealer Type ", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (acSapCode.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter A/C SAP Code ", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (actualActivationTime.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Actual Activation Time", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (dealer_code.trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter DealerCode", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (amToken.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter AMToken Number", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (amName.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter AM Name", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (shTokenno.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter SH Token number", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (shName.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter SH Name", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (osOfMonth.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Opening Stock of the Month", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (stksevdaysSelect.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter whether Stock refreshed or not", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (stkLtSixty.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Stock less than Sixty days", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (stkGtSixty.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Stock greater than Sixty days", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (currentStock.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Current Stock", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (pAStock.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Physical Actual Stock", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (paidstock_ev.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Paid-up stock", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (parksell_ev.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Park and Sell Stock", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (cpt_procure.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter CPT value", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (self_procure.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Self value", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (iep_procure.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter IEP value", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (ncd_procure.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter NCD value", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (leadstIEP.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Leads through IEP", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (leadstNCD.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Leads through NCD", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (conversion_IEP.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Conversion % IEP", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (conversion_NCD.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Conversion % NCD", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (Num_Of_highestBids.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Number of highest bids", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (xmart_nonIEP.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Xmart(Non - IEP)", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (total_procurement.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Total Procurement", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (retail_Sales.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Retail Sales", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (offload_sales.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Offload Sales", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (total_sales.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Total Sales", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (bookingInHand.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Booking in hand", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (conversion_OMS.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Conversion % OMS", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (omsLeads.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter OMS Leads", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (walkinstOMS.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Walkins through OMS", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (numOfFinCases.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Number of Finance Cases", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (warrantyNos.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Warranty Numbers", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (warrantyValue.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Warranty Value", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (warrantyPen.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Warranty Percentage", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (openAggregateCases.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Open aggregate Cases", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (closedAggregateCases.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Closed aggregate cases", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (totalAggregateCases.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Total aggregate cases", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (actualCollection.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Actual collection", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (NumOfDealerVisits.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Number of dealer visits(MTD)", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (royalty_select_tv.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter Royalty collection", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (createdDateOn.trim().isEmpty()) {
            result = false;
        } else {
            result = true;
        }
        return result;
    }


    private void weekReportWalkinCountservice(Activity mContext, final weekReportWalkinCountModel req) {
        SpinnerManager.showSpinner(mContext);
        weekReportWalkinCountServices.reqservice(mContext, req, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                Response<walkinCountRes> mRes = (Response<walkinCountRes>) obj;
                walkinCountRes mData = mRes.body();

                String msg = mData.getMessage().toString();


                if (mData.getStatus().equalsIgnoreCase("success")) {


                    if (mData.getSalesCount() != null) {
                        omsLeads.setText(String.valueOf(mData.getSalesCount()));
                        omsLeads.setEnabled(false);
                    } else {
                        omsLeads.setText("0");

                    }
                    if (mData.getSalesCount() != null) {
                        walkinstOMS.setText(String.valueOf(mData.getWalkinCount()));
                        walkinstOMS.setEnabled(false);
                    } else {
                        walkinstOMS.setText("0");
                    }

                    //Toast.makeText(getApplicationContext(), "Walkin Count Success.", Toast.LENGTH_SHORT).show();

                } else {

                    if (mData.getStatusCode().equals(401)) {

                        WebServicesCall.error_popup_retrofit(401, msg, mContext);

                    } else {
                        CommonMethods.alertMessage((Activity) mContext, msg);
                    }

                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                try {
                    SpinnerManager.hideSpinner(mContext);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //  Toast.makeText(mContext, mThrowable.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LPRConstants.REQUEST_CODE_LPR_MULTIPLE_SCAN) {
            Log.e("Onactivity called", "onActivity called");
            String value = readLPRFile(this, LPRConstants.REQUEST_CODE_LPR_MULTIPLE_SCAN);
            Log.e("LPR Data", "" + value);
            parseLPRData(value);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    finish();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void autoFillServiceCall(String dealerid, String dealerType, String startDate, String endDate) {

        CWRAutoFillModel cwrAutoFillModel = new CWRAutoFillModel(dealerid, dealerType, startDate, endDate);

        CWRAutoFillService cwrAutoFillService = new CWRAutoFillService();
        cwrAutoFillService.getAutoFillData(cwrAutoFillModel, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                Response<AutoFillResModel> sData = (Response<AutoFillResModel>) obj;
                AutoFillResModel resModel = sData.body();

                autoFillSetData(resModel);

                weekReportWalkinCountModel req = new weekReportWalkinCountModel();

                req.setDealerCode(dealer_code);
                req.setAccessToken(CommonMethods.getstringvaluefromkey(CreateReportActivity.this, "access_token"));
                req.setFromDate(week_start_date.getText().toString().trim());
                req.setToDate(week_end_date.getText().toString());
                req.setTag("Android");

                weekReportWalkinCountservice(CreateReportActivity.this, req);
            }


            @Override
            public void OnFailure(Throwable mThrowable) {

                Toast.makeText(getApplicationContext(), mThrowable.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

    private void autoFillSetData(AutoFillResModel resModel) {

        try {

            if (resModel.getId() != null) {
                id = "NA";
            } else if (!resModel.getId().toString().trim().isEmpty()) {
                id = resModel.getId().toString();
            }
            if (resModel.getZone() != null) {
                zoneSelect.setText(resModel.getZone().toString().trim());
            } else {
                zoneSelect.setText("");
            }
            if (resModel.getState() != null && !resModel.getState().toString().isEmpty()) {
                state.setText(resModel.getState());
                state.setEnabled(false);
            } else {
                state.setText("");
            }

            if (resModel.getCurrentStock() != null && !resModel.getCurrentStock().toString().isEmpty()) {
                currentStock.setText(String.valueOf(resModel.getCurrentStock()));
                currentStock.setEnabled(false);
            } else {
                currentStock.setText("0");
            }

            if (resModel.getCity() != null && !resModel.getCity().toString().isEmpty()) {
                city.setText(resModel.getCity());
                city.setEnabled(false);
            } else {
                city.setText("");
            }

            if (resModel.getDealerCode() != null && !resModel.getDealerCode().toString().isEmpty()) {
                omsCode.setText(resModel.getDealerCode());
                omsCode.setEnabled(false);

            } else {
                omsCode.setText("");

            }
            if (resModel.getDealerCategory() != null && !resModel.getDealerCategory().toString().isEmpty()) {
                dealer_category_selectview.setText(resModel.getDealerCategory());
                dealer_category_selectview.setEnabled(false);
            } else {
                dealer_category_selectview.setText("");

            }
            if (resModel.getAmTokenNo() != null && !resModel.getAmTokenNo().toString().isEmpty()) {
                amToken.setText(resModel.getAmTokenNo());
                amToken.setEnabled(false);
            } else {
                amToken.setText("");

            }
            if (resModel.getAmName() != null && !resModel.getAmName().toString().isEmpty()) {
                amName.setText(resModel.getAmName());
                amName.setEnabled(false);
            } else {
                amName.setText("");

            }
            if (resModel.getSmTokenNo() != null && !resModel.getSmTokenNo().toString().isEmpty()) {
                shTokenno.setText(resModel.getSmTokenNo());
                shTokenno.setEnabled(false);
            } else {
                shTokenno.setText("");

            }
            if (resModel.getSmName() != null && !resModel.getSmName().toString().isEmpty()) {
                shName.setText(resModel.getSmName());
                shName.setEnabled(false);
            } else {
                shName.setText("");

            }
            if (resModel.getStockRefreshed() != null && !resModel.getStockRefreshed().toString().isEmpty()) {
                stksevdaysSelect.setText(resModel.getStockRefreshed().toString());
                stksevdaysSelect.setEnabled(false);
            } else {
                stksevdaysSelect.setText("");

            }
            if (resModel.getStockLess60Days() != null && !resModel.getStockLess60Days().toString().isEmpty()) {
                stkLtSixty.setText(resModel.getStockLess60Days().toString());
                stkLtSixty.setEnabled(false);
            } else {
                stkLtSixty.setText("");

            }
            if (String.valueOf(resModel.getStockLess60Days()) != null && !(String.valueOf(resModel.getStockLess60Days())).toString().isEmpty()) {
                stkLtSixty.setText(String.valueOf(resModel.getStockLess60Days()));
                stkLtSixty.setEnabled(false);
            } else {
                stkLtSixty.setText("");

            }
            if (String.valueOf(resModel.getStockGrater60Days()) != null && !(String.valueOf(resModel.getStockGrater60Days())).toString().isEmpty()) {
                stkGtSixty.setText(String.valueOf(resModel.getStockGrater60Days()));
                stkGtSixty.setEnabled(false);
            } else {
                stkGtSixty.setText("");

            }

            if (resModel.getActualStock() != null && !resModel.getActualStock().toString().isEmpty()) {
                pAStock.setText(resModel.getActualStock().toString());
                pAStock.setEnabled(false);
            } else {
                pAStock.setText("");

            }
            if (resModel.getCpt() != null && !resModel.getCpt().toString().isEmpty()) {
                cpt_procure.setText(resModel.getCpt().toString());
                cpt_procure.setEnabled(false);
            } else {
                cpt_procure.setText("");

            }
            if (resModel.getSelfProcurement() != null && !resModel.getSelfProcurement().toString().isEmpty()) {
                self_procure.setText(resModel.getSelfProcurement().toString());
                self_procure.setEnabled(false);
            } else {
                self_procure.setText("");

            }
            if (resModel.getIep() != null && !resModel.getIep().toString().isEmpty()) {
                iep_procure.setText(resModel.getIep().toString());
                iep_procure.setEnabled(false);
            } else {
                iep_procure.setText("");

            }
            if (resModel.getNcd() != null && !resModel.getNcd().toString().isEmpty()) {
                ncd_procure.setText(resModel.getNcd().toString());
                ncd_procure.setEnabled(false);
            } else {
                ncd_procure.setText("");

            }
            if (resModel.getIepLead() != null && !resModel.getIepLead().toString().isEmpty()) {
                leadstIEP.setText(resModel.getIepLead().toString());
                leadstIEP.setEnabled(false);
            } else {
                leadstIEP.setText("");

            }
            if (resModel.getNcpLead() != null && !resModel.getNcpLead().toString().isEmpty()) {
                leadstNCD.setText(resModel.getNcpLead().toString());
                leadstNCD.setEnabled(false);
            } else {
                leadstNCD.setText("");

            }
            if (resModel.getIepConversion() != null && !resModel.getIepConversion().toString().isEmpty()) {
                conversion_IEP.setText(resModel.getIepConversion().toString());
                conversion_IEP.setEnabled(false);
            } else {
                conversion_IEP.setText("0");
            }
            if (resModel.getNcdConversionPer() != null && !resModel.getNcdConversionPer().toString().isEmpty()) {
                conversion_NCD.setText(resModel.getNcdConversionPer().toString());
                conversion_NCD.setEnabled(false);
            } else {
                conversion_NCD.setText("0");
            }

            if (resModel.getNoOfBids() != null && !resModel.getNoOfBids().toString().isEmpty()) {
                Num_Of_highestBids.setText(resModel.getNoOfBids().toString());
                Num_Of_highestBids.setEnabled(false);
            } else {
                Num_Of_highestBids.setText("");
            }
            if (resModel.getXMart() != null && !resModel.getXMart().toString().isEmpty()) {
                xmart_nonIEP.setText(resModel.getXMart().toString());
                xmart_nonIEP.setEnabled(false);
            } else {
                xmart_nonIEP.setText("");
            }
            if (resModel.getTotalProcurement() != null && !resModel.getTotalProcurement().toString().isEmpty()) {
                total_procurement.setText(resModel.getTotalProcurement().toString());
                total_procurement.setEnabled(false);
            } else {
                total_procurement.setText("0");
            }
            if (resModel.getRetailSales() != null && !resModel.getRetailSales().toString().isEmpty()) {
                retail_Sales.setText(resModel.getRetailSales().toString());
                retail_Sales.setEnabled(false);
            } else {
                retail_Sales.setText("0");
            }
            if (String.valueOf(resModel.getOffloadSales()) != null && !(String.valueOf(resModel.getOffloadSales()).toString().isEmpty())) {
                offload_sales.setText(String.valueOf(resModel.getOffloadSales()));
                offload_sales.setEnabled(false);
            } else {
                offload_sales.setText("");
            }
            if (resModel.getTotalSales() != null && !resModel.getTotalSales().toString().isEmpty()) {
                total_sales.setText(resModel.getTotalSales().toString());
                total_sales.setEnabled(false);
            } else {
                total_sales.setText("0");
            }
            if (String.valueOf(resModel.getBookingInHand()) != null && !resModel.getBookingInHand().toString().isEmpty()) {
                bookingInHand.setText(String.valueOf(resModel.getBookingInHand()));
                bookingInHand.setEnabled(false);
            } else {
                bookingInHand.setText("");
            }
            if (resModel.getOmsLeadConversion() != null && !resModel.getOmsLeadConversion().toString().isEmpty()) {
                conversion_OMS.setText(resModel.getOmsLeadConversion().toString());
                conversion_OMS.setEnabled(false);
            } else {
                conversion_OMS.setText("");
            }
            if (resModel.getOmsLeads() != null && !resModel.getOmsLeads().toString().isEmpty()) {
                omsLeads.setText(resModel.getOmsLeads().toString());
                omsLeads.setEnabled(false);
            } else {
                omsLeads.setText("");
            }
            if (resModel.getOmsWalkinThrough() != null && !resModel.getOmsWalkinThrough().toString().isEmpty()) {
                walkinstOMS.setText(String.valueOf(resModel.getOmsLeads()));
                walkinstOMS.setEnabled(false);
            } else {
                walkinstOMS.setText("");
            }
            if (resModel.getNoOfFinanceCases() != null && !resModel.getNoOfFinanceCases().toString().isEmpty()) {
                numOfFinCases.setText(resModel.getNoOfFinanceCases().toString());
                numOfFinCases.setEnabled(false);
            } else {
                numOfFinCases.setText("");
            }
            if (resModel.getWarrantyNo() != null && !resModel.getWarrantyNo().toString().isEmpty()) {
                warrantyNos.setText(resModel.getWarrantyNo());
                warrantyNos.setEnabled(false);
            } else {
                warrantyNos.setText("");
            }
            if (resModel.getWarrantyValues() != null && !resModel.getWarrantyValues().toString().isEmpty()) {
                warrantyValue.setText(resModel.getWarrantyValues());
                warrantyValue.setEnabled(false);
            } else {
                warrantyValue.setText("");
            }
            if (resModel.getWarrantyPercentage() != null && !resModel.getWarrantyPercentage().toString().isEmpty()) {
                warrantyPen.setText(resModel.getWarrantyPercentage().toString());
                warrantyPen.setEnabled(false);
            } else {
                warrantyPen.setText("0");
            }
            if (resModel.getOpenAggregateCases() != null && !resModel.getOpenAggregateCases().toString().isEmpty()) {
                openAggregateCases.setText(resModel.getOpenAggregateCases().toString());
                openAggregateCases.setEnabled(false);
            } else {
                openAggregateCases.setText("");
            }
            if (resModel.getClosedAggregateCases() != null && !resModel.getClosedAggregateCases().toString().isEmpty()) {
                closedAggregateCases.setText(resModel.getClosedAggregateCases().toString());
                closedAggregateCases.setEnabled(false);
            } else {
                closedAggregateCases.setText("");
            }
            if (resModel.getTotalAggregateCases() != null && !resModel.getTotalAggregateCases().toString().isEmpty()) {
                totalAggregateCases.setText(resModel.getTotalAggregateCases().toString());
                totalAggregateCases.setEnabled(false);
            } else {
                totalAggregateCases.setText("0");
            }

            if (resModel.getActualCollection() != null && !resModel.getActualCollection().toString().isEmpty()) {
                actualCollection.setText(resModel.getActualCollection().toString());
                actualCollection.setEnabled(false);
            } else {
                actualCollection.setText("");
            }

            //new add
            if (resModel.getPaidUpStock() != null && !resModel.getPaidUpStock().toString().isEmpty()) {
                paidstock_ev.setText(resModel.getPaidUpStock().toString());
                paidstock_ev.setEnabled(false);
            } else {
                paidstock_ev.setText("");
            }

            if (resModel.getParkAndSellStock() != null && !resModel.getParkAndSellStock().toString().isEmpty()) {
                parksell_ev.setText(resModel.getParkAndSellStock().toString());
                parksell_ev.setEnabled(false);
            } else {
                parksell_ev.setText("");
            }

            if (resModel.getRoyaltyCollection() != null && !resModel.getRoyaltyCollection().toString().isEmpty()) {
                royalty_select_tv.setText(resModel.getRoyaltyCollection().toString());
                royalty_select_tv.setEnabled(false);
            } else {
                royalty_select_tv.setText("");
            }


            if (resModel.getNoOfDealerVisits() != null && !resModel.getNoOfDealerVisits().toString().isEmpty()) {
                NumOfDealerVisits.setText(resModel.getNoOfDealerVisits().toString());
                NumOfDealerVisits.setEnabled(false);
            } else {
                NumOfDealerVisits.setText("");
            }
            if (resModel.getCreatedOn() != null) {
                createdDateOn = resModel.getCreatedOn().toString();
            } else {
                createdDateOn = "";
            }

            if (resModel.getCreatedOn() != null) {
                createdDateOn = resModel.getCreatedOn().toString();
            } else {
                createdDateOn = "";
            }
            if (resModel.getFormDate() != null) {

                try {
                    String dateVal = resModel.getFormDate();
                    String subString = dateVal.substring(0, 10);
                    week_start_date.setText(subString);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                week_start_date.setText("");
            }
            if (resModel.getToDate() != null) {
                try {
                    String dateVal = resModel.getToDate();
                    String subString = dateVal.substring(0, 10);
                    week_end_date.setText(subString);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                week_end_date.setText("");
            }
            if (resModel.getAccountSapCode() != null && !resModel.getAccountSapCode().toString().isEmpty()) {
                acSapCode.setText(resModel.getAccountSapCode());
                acSapCode.setEnabled(false);
            } else {
                acSapCode.setText("");
            }

            if (resModel.getActualActivationTime() != null && !resModel.getActualActivationTime().toString().isEmpty()) {
                String actActivDateValue = resModel.getActualActivationTime().toString();
                String date[] = actActivDateValue.split("-");
                try {
                    String month = date[1];
                    String year = date[0];
                    actualActivationTime.setText(month + " | " + year);
                    actualActivationTime.setEnabled(false);

                    actualActivation_Value = actActivDateValue.substring(0, 10);

                } catch (Exception e) {
                    //   actualActivationTime.setText(actActivDateValue);

                }

            } else {
                actualActivationTime.setText("");
            }

            if (String.valueOf(resModel.getDirectWalkin()) != null && !resModel.getDirectWalkin().toString().isEmpty()) {
                DoOWalkins.setText(String.valueOf(resModel.getDirectWalkin()));
                DoOWalkins.setEnabled(false);
            } else {
                DoOWalkins.setText("");
            }

            if (resModel.getCreatedByFranchiseid() != null) {
                cbFranchiseId = resModel.getDealerName().toString();
            } else {
                cbFranchiseId = "";
            }


            serverDataCaluclation(resModel);


        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    public int getvalue(String values) {
        if (values != null && !values.isEmpty()) {
            return Integer.parseInt(values.toString());
        }
        return 0;
    }

    private void serverDataCaluclation(AutoFillResModel resModel) {

        int stockTotal = 0, conversionIEP = 0, conversionNCD = 0, totalProc = 0, totalSales = 0, totalWalkin = 0, totalArgg = 0, warrantyP = 0;

        try {
            int stockData = 0, totalproc = 0, totalSale = 0;

            stockData = getvalue(resModel.getStcokOpeningMonth().toString());
            totalproc = getvalue(resModel.getTotalProcurement().toString());
            totalSale = getvalue(resModel.getTotalSales().toString());
            stockTotal = (stockData + totalproc) - totalSale;

        } catch (ArithmeticException e) {
            stockTotal = 0;
        } catch (Exception e) {
            stockTotal = 0;
        }
        currentStock.setText(String.valueOf(stockTotal));

        //

        try {


            Double iepDatas, leadieps,conversionD;

            iepDatas = Double.valueOf(resModel.getIep().toString());
            leadieps = Double.valueOf(resModel.getIepLead().toString());

            conversionD = iepDatas / leadieps;

            conversionD = conversionD * 100;
            conversionIEP = (int) Math.round(conversionD);
            if(conversionIEP<0)conversionIEP=0;

        } catch (ArithmeticException e) {
            conversionIEP = 0;
        } catch (Exception e) {
            conversionIEP = 0;
        }
        conversion_IEP.setText(String.valueOf(conversionIEP));

        //

        try {

            Double ncds, leadncds,conversionNCDD;

            ncds = Double.valueOf(resModel.getNcd().toString());
            leadncds = Double.valueOf(resModel.getNcpLead().toString());

            conversionNCDD = ncds / leadncds;

            conversionNCDD = conversionNCDD * 100;
            conversionNCD = (int) Math.round(conversionNCDD);
            if(conversionNCD<0)conversionNCD=0;

        } catch (ArithmeticException e) {
            conversionNCD = 0;
        } catch (Exception e) {
            conversionNCD = 0;
        }
        conversion_NCD.setText(String.valueOf(conversionNCD));

        //

        try {

            int cpt = 0, self = 0, iep = 0, ncd = 0, xmart = 0;
            cpt = getvalue(resModel.getCpt().toString());
            self = getvalue(resModel.getSelfProcurement().toString());
            iep = getvalue(resModel.getIep().toString());
            ncd = getvalue(resModel.getNcpLead().toString());
            xmart = getvalue(resModel.getXMart().toString());
            totalProc = cpt + self + iep + ncd + xmart;

        } catch (ArithmeticException e) {
            totalProc = 0;
        } catch (Exception e) {
            totalProc = 0;
        }
        total_procurement.setText(String.valueOf(totalProc));

        //

        try {

            int retails = 0, offload = 0;
            retails = getvalue(resModel.getRetailSales().toString());
            offload = getvalue(resModel.getOffloadSales().toString());
            totalSales = retails + offload;

        } catch (ArithmeticException e) {
            totalSales = 0;
        } catch (Exception e) {
            totalSales = 0;
        }
        total_sales.setText(String.valueOf(totalSales));

        //

        try {

            int walkin = 0, dwalkin = 0;
            walkin = getvalue(resModel.getOmsWalkinThrough().toString());
            dwalkin = getvalue(resModel.getDirectWalkin().toString());
            totalWalkin = dwalkin + walkin;

        } catch (ArithmeticException e) {
            totalWalkin = 0;
        } catch (Exception e) {
            totalWalkin = 0;
        }
        totalWalkins.setText(String.valueOf(totalWalkin));

        //

        try {

            int open = 0, close = 0;
            open = getvalue(resModel.getOpenAggregateCases().toString());
            close = getvalue(resModel.getClosedAggregateCases().toString());
            totalArgg = open + close;

        } catch (ArithmeticException e) {
            totalArgg = 0;
        } catch (Exception e) {
            totalArgg = 0;
        }
        totalAggregateCases.setText(String.valueOf(totalArgg));

        //
        //

        Double warrantyPD;

        try {
            Double warrantyno, totalSale;
            warrantyno = Double.valueOf(getvalue(resModel.getWarrantyNo().toString()));
            totalSale = Double.valueOf(getvalue(resModel.getTotalSales().toString()));

            warrantyPD = warrantyno / totalSale;

            warrantyPD = warrantyPD * 100;
            warrantyP = (int) Math.round(warrantyPD);
            if(warrantyP<0)warrantyP=0;

        } catch (ArithmeticException e) {
            warrantyP = 0;
        } catch (Exception e) {
            warrantyP = 0;
        }

        warrantyPen.setText(String.valueOf(warrantyP));


    }

    private void generateDatePickerAAT() {
        String format = "yyyy-MM-dd ";
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
        String dateValue = sdf.format(myCalendar.getTime()).toString();
        /*actualActivationTime.setText(dateValue.substring(0,9));*/
        //actualActivationTime.setText(dateValue);
        String date[] = dateValue.split("-");
        try {
            String month = date[1];
            String year = date[0];
            actualActivationTime.setText(month + " | " + year);

        } catch (Exception e) {
            actualActivationTime.setText(dateValue);

        }
        actualActivation_Value = dateValue;
        // Toast.makeText(getApplicationContext(),actualActivation_Value,Toast.LENGTH_LONG).show();

    }

    private String getFirstDay(Date d) {
        String formatedDate = "";
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(d);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            Date mdate = calendar.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            formatedDate = sdf.format(mdate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formatedDate;
    }



    private void checkPermissionApp() {
        super.onUserLeaveHint();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            int hasWriteContactsPermission;
            hasWriteContactsPermission = checkSelfPermission(Manifest.permission.CAMERA);
            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_PHONE_STATE}, REQUEST_CODE_ASK_PERMISSIONS);
            }
        }
    }



    //LPR Realted Methods



    public String readLPRFile(Context context, int type) { // reading file..for single scan..jSON filw is deleted after reading image..
        String value = "";
        final String lprDir, lprFileName;
        lprDir = preferenceManager.readString(LPRConstants.LPR_DIR, "");
        lprFileName = preferenceManager.readString(LPRConstants.LPR_FILENAME + type, "");
        try {

            int fileCount = 0;
            File directory;
            File[] allFiles;

            FilenameFilter fileFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return (name.startsWith(lprFileName) && name.endsWith(".json"));
                }
            };

            directory = new File(lprDir + "/" + lprFileName);
            allFiles = directory.listFiles(fileFilter);
            Log.e("LPR", "file lenght: " + allFiles.length);
            if (allFiles.length == 1) {

                File file = allFiles[0];/*LPR required file..*/
                if (file.exists()) {
                    FileInputStream fis = new FileInputStream(file);
                    DataInputStream in = new DataInputStream(fis);
                    BufferedReader br = new BufferedReader(new InputStreamReader(in));
                    String strLine;
                    while ((strLine = br.readLine()) != null) {
                        value = value + strLine;
                    }
                    in.close();
                    if (type == LPRConstants.REQUEST_CODE_LPR_SINGLE_SCAN && file.delete()) {
                        fileCount++;
                        Log.e("LPR", "file Deleted");
                    } else {
                        Log.e("LPR", "file not Deleted");
                    }
                }
            } else {
                for (File f : allFiles) {
                    if (f.delete()) {
                        fileCount++;
                        Log.e("LPR", "file Deleted");
                    } else {
                        Log.e("LPR", "file not Deleted");
                    }
                }
            }
            Log.e("LPR", "deleted file count: " + fileCount);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return value;
    }


    private void parseLPRData(String value) {
        try {
            Type empTypeList = new TypeToken<ArrayList<LPRResponse>>() {
            }.getType();
            List<LPRResponse> mData = new Gson().fromJson(value, empTypeList);
            if ( mData !=null && mData.size() > 0) {
                Log.i("LPR", "Result: ");
                for (int i = 0; i < mData.size(); i++) {
                    LPRResponse response = mData.get(i);
                    String outputValue = response.getOutputValue();
                    if (outputValue.toLowerCase().equals("unreg/na"))
                        outputValue = "";
                    mSqlAdapterForDML.insertLPRItem(response);
                }

            }
            if(mSqlAdapterForDML.getLPRListCount() > 0){
                showStockListingScreen();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showStockListingScreen() {
        Intent intent = new Intent(CreateReportActivity.this, StockActivity.class);
        startActivityForResult(intent, LPRConstants.REQUEST_CODE_LPR_STOCK_LIST);
    }


    @Override
    public void processFinish(LPRServerResponse output) {
        if(output != null){
            pAStock.setText(output.getSuccessEventCount().toString());

        }else {
            pAStock.setText("0");
        }
    }
}