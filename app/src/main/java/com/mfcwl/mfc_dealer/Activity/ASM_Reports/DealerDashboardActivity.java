package com.mfcwl.mfc_dealer.Activity.ASM_Reports;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mfcwl.mfc_dealer.ASMModel.DealerName;
import com.mfcwl.mfc_dealer.ASMModel.chartDataRes;
import com.mfcwl.mfc_dealer.ASMModel.cityRes;
import com.mfcwl.mfc_dealer.ASMModel.dealerReq;
import com.mfcwl.mfc_dealer.ASMModel.graphDataRes;
import com.mfcwl.mfc_dealer.ASMModel.highParaData;
import com.mfcwl.mfc_dealer.ASMModel.highparamRes;
import com.mfcwl.mfc_dealer.ASMModel.stateRes;
import com.mfcwl.mfc_dealer.DealerPerformanceModels.Datum;
import com.mfcwl.mfc_dealer.DealerPerformanceModels.DealerPerformanceResponse;
import com.mfcwl.mfc_dealer.Model.stateReq;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.graphdata.animation.Easing;
import com.mfcwl.mfc_dealer.graphdata.buffer.DayAxisValueFormatter;
import com.mfcwl.mfc_dealer.graphdata.charts.BarChart;
import com.mfcwl.mfc_dealer.graphdata.charts.PieChart;
import com.mfcwl.mfc_dealer.graphdata.components.Legend;
import com.mfcwl.mfc_dealer.graphdata.components.XAxis;
import com.mfcwl.mfc_dealer.graphdata.data.BarData;
import com.mfcwl.mfc_dealer.graphdata.data.BarDataSet;
import com.mfcwl.mfc_dealer.graphdata.data.BarEntry;
import com.mfcwl.mfc_dealer.graphdata.data.PieData;
import com.mfcwl.mfc_dealer.graphdata.data.PieDataSet;
import com.mfcwl.mfc_dealer.graphdata.data.PieEntry;
import com.mfcwl.mfc_dealer.graphdata.formatter.PercentFormatter;
import com.mfcwl.mfc_dealer.graphdata.formatter.ValueFormatter;
import com.mfcwl.mfc_dealer.graphdata.interfaces.datasets.IBarDataSet;
import com.mfcwl.mfc_dealer.graphdata.utils.ColorTemplate;
import com.mfcwl.mfc_dealer.graphdata.utils.Fill;
import com.mfcwl.mfc_dealer.retrofitconfig.DasboardStatusServices;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

import static com.mfcwl.mfc_dealer.graphdata.utils.ColorTemplate.rgb;

public class DealerDashboardActivity extends AbstractCharts {

    @BindView(R.id.dealer_tv)
    public TextView dealer_tv;
    @BindView(R.id.zone_tv)
    public TextView zone_tv;

    @BindView(R.id.location_tv)
    public TextView location_tv;

    @BindView(R.id.state_tv)
    public TextView state_tv;

    @BindView(R.id.duration_tv)
    public TextView duration_tv;

    @BindView(R.id.listofdealer_tv)
    public TextView listofdealer_tv;
    @BindView(R.id.highperformance_tv)
    public TextView highperformance_tv;
    @BindView(R.id.past_performance)
    public TextView past_performance;

    @BindView(R.id.redline)
    public TextView redline;
    @BindView(R.id.yellowline)
    public TextView yellowline;
    @BindView(R.id.greenline)
    public TextView greenline;

    @BindView(R.id.redlabel)
    public TextView redlabel;
    @BindView(R.id.greenlabel)
    public TextView greenlabel;
    @BindView(R.id.yellowlabel)
    public TextView yellowlabel;

    @BindView(R.id.monthly_back)
    public ImageView monthly_back;

    @BindView(R.id.dealerlist_ll)
    public LinearLayout dealerlist_ll;

    @BindView(R.id.llPrams)
    public LinearLayout params_ll;

    @BindView(R.id.highpara_ll)
    public LinearLayout highpara_ll;

    @BindView(R.id.pastp_ll)
    public LinearLayout pastp_ll;

    @BindView(R.id.scrollView)
    public ScrollView scrollView;

    @BindView(R.id.fab)
    public FloatingActionButton fab;
    @BindView(R.id.dealerlist_tv)
    TextView dealerlistTv;
    @BindView(R.id.nameopeningstock)
    TextView nameopeningstock;
    @BindView(R.id.countopeningstock)
    TextView countopeningstock;
    @BindView(R.id.colorcodeopeningstock)
    TextView colorcodeopeningstock;
    @BindView(R.id.namesalesplan)
    TextView namesalesplan;
    @BindView(R.id.countsalesplan)
    TextView countsalesplan;
    @BindView(R.id.colorcodesalesplan)
    TextView colorcodesalesplan;
    @BindView(R.id.nameprocplan)
    TextView nameprocplan;
    @BindView(R.id.countprocplan)
    TextView countprocplan;
    @BindView(R.id.colorcodeprocplan)
    TextView colorcodeprocplan;
    @BindView(R.id.nameclosingstock)
    TextView nameclosingstock;
    @BindView(R.id.countclosingstock)
    TextView countclosingstock;
    @BindView(R.id.colorcodeclosingstock)
    TextView colorcodeclosingstock;
    @BindView(R.id.namewarrantyunits)
    TextView namewarrantyunits;
    @BindView(R.id.countwarranty)
    TextView countwarranty;
    @BindView(R.id.colorcodewarranty)
    TextView colorcodewarranty;
    @BindView(R.id.namewarrantyvalues)
    TextView namewarrantyvalues;
    @BindView(R.id.countwarrantyvalues)
    TextView countwarrantyvalues;
    @BindView(R.id.colorcodewarrantyvalues)
    TextView colorcodewarrantyvalues;
    @BindView(R.id.nameprociep)
    TextView nameprociep;
    @BindView(R.id.countprociep)
    TextView countprociep;
    @BindView(R.id.colorcodeprociep)
    TextView colorcodeprociep;
    @BindView(R.id.nameproccpt)
    TextView nameproccpt;
    @BindView(R.id.countproccpt)
    TextView countproccpt;
    @BindView(R.id.colorcodeproccpt)
    TextView colorcodeproccpt;
    @BindView(R.id.namewalkins)
    TextView namewalkins;
    @BindView(R.id.countwalkins)
    TextView countwalkins;
    @BindView(R.id.colorcodewalkins)
    TextView colorcodewalkins;
    @BindView(R.id.namesalesconv)
    TextView namesalesconv;
    @BindView(R.id.countsalesconv)
    TextView countsalesconv;
    @BindView(R.id.colorcodesales)
    TextView colorcodesales;
    @BindView(R.id.nameoms)
    TextView nameoms;
    @BindView(R.id.countoms)
    TextView countoms;
    @BindView(R.id.colorcodeoms)
    TextView colorcodeoms;
    @BindView(R.id.nameconoms)
    TextView nameconoms;
    @BindView(R.id.countconoms)
    TextView countconoms;
    @BindView(R.id.colorcodeconoms)
    TextView colorcodeconoms;


    private DealerPerformanceResponse mData = null;

    private PieChart chart;
    String dealerCount = "0", date = "0000-00-00";

    int redval = 0, amberval = 0, greenval = 0;

    public String TAG = getClass().getSimpleName();
    public String dealerCatVal = "", tDate = "", stateCode = "", cityCode = "";
    CommonMethods db = new CommonMethods();

    private ArrayList<String> mDealerList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_dealerdashboard);
        Log.i(TAG, "onCreate: ");
        ButterKnife.bind(this);

        CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) fab.getLayoutParams();
        lp.anchorGravity = Gravity.BOTTOM | GravityCompat.START;
        fab.setLayoutParams(lp);

        mDealerList = new ArrayList<>();

        redline.setVisibility(View.INVISIBLE);
        redlabel.setVisibility(View.INVISIBLE);

        yellowline.setVisibility(View.INVISIBLE);
        yellowlabel.setVisibility(View.INVISIBLE);

        greenline.setVisibility(View.INVISIBLE);
        greenlabel.setVisibility(View.INVISIBLE);

        setValues();
        setListeners();

        setDataChart(chart);


        if (db.isInternetWorking(DealerDashboardActivity.this)) {
            getChartData();
            getDealerPerfomaceParams();
        } else {
            db.alertMessage(DealerDashboardActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
        }

    }


    private void setListeners() {

        dealer_tv.setOnClickListener(v -> {

            if (state_tv.getText().toString().equals("")) {
                Toast.makeText(getApplicationContext(), "Please select state", Toast.LENGTH_SHORT).show();
                return;

            }

            if (location_tv.getText().toString().equals("")) {
                Toast.makeText(getApplicationContext(), "Please select city", Toast.LENGTH_SHORT).show();
                return;

            }

            dealerReq req = new dealerReq();

            if (!location_tv.getText().toString().equalsIgnoreCase("")) {

                req.setCitycode(cityCode);

                if (db.isInternetWorking(DealerDashboardActivity.this)) {
                    getDealerName(v, req);
                } else {
                    db.alertMessage(DealerDashboardActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
                }
            } else if (!state_tv.getText().toString().equalsIgnoreCase("")) {

                req.setStatecode(stateCode);

                if (db.isInternetWorking(DealerDashboardActivity.this)) {
                    getDealerName(v, req);
                } else {
                    db.alertMessage(DealerDashboardActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
                }

            } else if (!zone_tv.getText().toString().equalsIgnoreCase("")) {

                req.setZone(zone_tv.getText().toString());

                if (db.isInternetWorking(DealerDashboardActivity.this)) {
                    getDealerName(v, req);
                } else {
                    db.alertMessage(DealerDashboardActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
                }

            } else {
                Toast.makeText(getApplicationContext(), "Please select Zone", Toast.LENGTH_SHORT).show();

            }

        });

        zone_tv.setOnClickListener(v -> {

            if (db.isInternetWorking(DealerDashboardActivity.this)) {
                getZoneList(v);
            } else {
                db.alertMessage(DealerDashboardActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
            }

        });


        state_tv.setOnClickListener(v -> {
            if (!zone_tv.getText().toString().equalsIgnoreCase("")) {
                if (db.isInternetWorking(DealerDashboardActivity.this)) {
                    getStateList(v);
                } else {
                    db.alertMessage(DealerDashboardActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
                }
            } else {
                Toast.makeText(getApplicationContext(), "Please select Zone", Toast.LENGTH_SHORT).show();

            }
        });

        location_tv.setOnClickListener(v -> {

            if (!state_tv.getText().toString().equalsIgnoreCase("")) {
                if (db.isInternetWorking(DealerDashboardActivity.this)) {
                    getLocationList(v);
                } else {
                    db.alertMessage(DealerDashboardActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
                }

            } else {
                Toast.makeText(getApplicationContext(), "Please select State", Toast.LENGTH_SHORT).show();

            }
        });

        duration_tv.setOnClickListener(v -> {
            if (db.isInternetWorking(DealerDashboardActivity.this)) {
                getDurationList(v);
            } else {
                db.alertMessage(DealerDashboardActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
            }
        });

        dealerlistTv.setOnClickListener(v -> {

            try {
                mDealerList.clear();

                if (mData != null && mData.data.size() > 0) {
                    for (int i = 0; i < mData.data.size(); i++) {
                        Datum dealerData = mData.data.get(i);
                        mDealerList.add(dealerData.dealerName);
                    }

                    String[] data = mDealerList.toArray(new String[mDealerList.size()]);

                    setAlertDialogs(v, "Dealers", data);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        });

        listofdealer_tv.setOnClickListener(v -> {

            if (dealerlist_ll.getVisibility() == View.VISIBLE) {
                dealerlist_ll.setVisibility(View.GONE);
                listofdealer_tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_down_black_24dp, 0);

            } else {

                highpara_ll.setVisibility(View.GONE);
                pastp_ll.setVisibility(View.GONE);
                listofdealer_tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_up_arrow_black_24dp, 0);

                dealerlist_ll.setVisibility(View.VISIBLE);
            }


        });
        highperformance_tv.setOnClickListener(v -> {
            if (highpara_ll.getVisibility() == View.VISIBLE) {
                highpara_ll.setVisibility(View.GONE);
                highperformance_tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_down_black_24dp, 0);

            } else {
                pastp_ll.setVisibility(View.GONE);
                dealerlist_ll.setVisibility(View.GONE);
                highpara_ll.setVisibility(View.VISIBLE);
                highperformance_tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_up_arrow_black_24dp, 0);

                getHighParam();
            }

        });
        past_performance.setOnClickListener(v -> {

            if (pastp_ll.getVisibility() == View.VISIBLE) {

                pastp_ll.setVisibility(View.GONE);
                past_performance.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_down_black_24dp, 0);


            } else {

                dealerlist_ll.setVisibility(View.GONE);
                highpara_ll.setVisibility(View.GONE);
                highpara_ll.setVisibility(View.GONE);

                pastp_ll.setVisibility(View.VISIBLE);
                past_performance.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_up_arrow_black_24dp, 0);


                // graphData();

                if (db.isInternetWorking(DealerDashboardActivity.this)) {
                    getGraphData();
                } else {
                    db.alertMessage(DealerDashboardActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
                }
            }

        });

        fab.setOnClickListener(v -> {

            dashboradpopup();
        });

        monthly_back.setOnClickListener(v -> {

            finish();

        });
    }


    @Override
    protected void setPieChart(PieChart charts) {
        chart = charts;
    }

    private void setValues() {

        Date cDate = new Date();
        tDate = new SimpleDateFormat("yyyy-MM-dd").format(cDate);

        dealerlist_ll.setVisibility(View.GONE);
        highpara_ll.setVisibility(View.GONE);
        pastp_ll.setVisibility(View.GONE);
    }

    public void listpopup() {


        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(DealerDashboardActivity.this);
        builder.setTitle("");
        String[] animals;
        animals = new String[]{"List Of Dealer", "Hightest Parameter", "Past Performance"};


        builder.setItems(animals, (dialog, which) -> {
            switch (which) {
                case 0:
                    highpara_ll.setVisibility(View.GONE);
                    pastp_ll.setVisibility(View.GONE);
                    dealerlist_ll.setVisibility(View.VISIBLE);
                    break;
                case 1:
                    pastp_ll.setVisibility(View.GONE);
                    dealerlist_ll.setVisibility(View.GONE);
                    highpara_ll.setVisibility(View.VISIBLE);
                    getHighParam();
                    break;
                case 2:

                    dealerlist_ll.setVisibility(View.GONE);
                    highpara_ll.setVisibility(View.GONE);
                    highpara_ll.setVisibility(View.GONE);
                    pastp_ll.setVisibility(View.VISIBLE);

                    if (db.isInternetWorking(DealerDashboardActivity.this)) {
                        getGraphData();
                    } else {
                        db.alertMessage(DealerDashboardActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
                    }

                    break;
            }
        });

        // create and show the alert dialog
        android.app.AlertDialog dialog = builder.create();

        dialog.getWindow().setGravity(Gravity.CENTER | Gravity.RIGHT);

        dialog.show();
    }

    private void getZoneList(View v) {

        DasboardStatusServices dash = new DasboardStatusServices();

        SpinnerManager.showSpinner(this);
        dash.getZoneList(this, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(DealerDashboardActivity.this);
                Response<List<String>> mRes = (Response<List<String>>) obj;
                List<String> mData = mRes.body();

                String[] zone = mData.toArray(new String[mData.size()]);

                setAlertDialogs(v, "Zone", zone);

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(DealerDashboardActivity.this);
            }


        });
    }

    private void getDurationList(View v) {

        DasboardStatusServices dash = new DasboardStatusServices();

        SpinnerManager.showSpinner(this);
        dash.getDurationList(this, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(DealerDashboardActivity.this);
                Response<List<String>> mRes = (Response<List<String>>) obj;
                List<String> mData = mRes.body();

                String[] duration = mData.toArray(new String[mData.size()]);

                setAlertDialogs(v, "Duration", duration);

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(DealerDashboardActivity.this);
            }

        });
    }

    private void getDealerName(View v, dealerReq req) {


        DasboardStatusServices dash = new DasboardStatusServices();

        SpinnerManager.showSpinner(this);
        dash.getDealerListPost(this, req, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                SpinnerManager.hideSpinner(DealerDashboardActivity.this);
                Response<List<DealerName>> mRes = (Response<List<DealerName>>) obj;
                List<DealerName> mData = mRes.body();
                displayData(mData, v);

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(DealerDashboardActivity.this);
            }


        });
    }

    private void getLocationList(View v) {

        stateReq state = new stateReq();
        state.setParam(stateCode);

        DasboardStatusServices dash = new DasboardStatusServices();

        SpinnerManager.showSpinner(this);
        dash.getCityList(this, state, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(DealerDashboardActivity.this);
                Response<List<cityRes>> mRes = (Response<List<cityRes>>) obj;
                List<cityRes> mData = mRes.body();
                displaycity(mData, v);

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(DealerDashboardActivity.this);
            }


        });
    }

    private void getStateList(View v) {

        stateReq state = new stateReq();
        state.setParam(zone_tv.getText().toString());

        DasboardStatusServices dash = new DasboardStatusServices();

        SpinnerManager.showSpinner(this);
        dash.getStateList(this, state, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(DealerDashboardActivity.this);
                Response<List<stateRes>> mRes = (Response<List<stateRes>>) obj;
                List<stateRes> mData = mRes.body();
                displayDatastate(mData, v);

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(DealerDashboardActivity.this);
            }

        });
    }

    private void displayData(List<DealerName> mData, View v) {

        try {
            List<String> dealerName = new ArrayList<String>();
            List<String> dealerCode = new ArrayList<String>();


            if (mData.size() == 0 || mData.isEmpty()) {
                Toast.makeText(getApplicationContext(), "No Dealer in this Location.", Toast.LENGTH_SHORT).show();
                return;
            }

            if (mData.size() != 0 && mData != null) dealerCount = String.valueOf(mData.size());


            for (int i = 0; i < mData.size(); i++) {
                dealerName.add(i, mData.get(i).getDealername());
                dealerCode.add(i, mData.get(i).getDealercode());
            }

            String[] namesArr = dealerName.toArray(new String[dealerName.size()]);
            String[] codeArr = dealerCode.toArray(new String[dealerCode.size()]);
            setAlertDialog(v, DealerDashboardActivity.this, "Dealer Category", namesArr, codeArr);

        } catch (Exception e) {

        }

    }

    private void displaycity(List<cityRes> mData, View v) {

        try {
            List<String> name = new ArrayList<String>();
            List<String> code = new ArrayList<String>();

            for (int i = 0; i < mData.size(); i++) {
                name.add(i, mData.get(i).getCityname());
                code.add(i, mData.get(i).getCitycode());
            }

            String[] namesArr = name.toArray(new String[name.size()]);
            String[] codeArr = code.toArray(new String[code.size()]);
            setAlertDialog(v, DealerDashboardActivity.this, "City List", namesArr, codeArr);

        } catch (Exception e) {

        }

    }

    private void displayDatastate(List<stateRes> mData, View v) {

        try {
            List<String> name = new ArrayList<String>();
            List<String> code = new ArrayList<String>();

            for (int i = 0; i < mData.size(); i++) {
                name.add(i, mData.get(i).getStatename());
                code.add(i, mData.get(i).getStatecode());
            }

            String[] namesArr = name.toArray(new String[name.size()]);
            String[] codeArr = code.toArray(new String[code.size()]);
            setAlertDialog(v, DealerDashboardActivity.this, "State List", namesArr, codeArr);

        } catch (Exception e) {

        }

    }

    private void setAlertDialog(View v, Activity a, final String strTitle,
                                final String[] arrVal, final String[] codeVal) {

        AlertDialog.Builder alert = new AlertDialog.Builder(a);
        alert.setTitle(strTitle);
        alert.setItems(arrVal, (dialog, which) -> {

            if (strTitle.equalsIgnoreCase("Dealer Category")) {
                dealer_tv.setText(arrVal[which]);
                dealerCatVal = codeVal[which];
                duration_tv.setText("");
            } else if (strTitle.equalsIgnoreCase("State List")) {
                state_tv.setText(arrVal[which]);
                stateCode = codeVal[which];
                duration_tv.setText("");
                dealer_tv.setText("");
                location_tv.setText("");
                dealerCount = "0";

            } else if (strTitle.equalsIgnoreCase("City List")) {
                location_tv.setText(arrVal[which]);
                cityCode = codeVal[which];

                dealer_tv.setText("");
                duration_tv.setText("");
                dealerCount = "0";
            }

            if (db.isInternetWorking(DealerDashboardActivity.this)) {
                runOnUiThread(() -> {
                    getChartData();
                    getDealerPerfomaceParams();
                });

            } else {
                db.alertMessage(DealerDashboardActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
            }

        });
        alert.create();
        alert.show();

    }


    public void setAlertDialogs(View v, final String strTitle, final String[] arrVal) {

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(strTitle);
        alert.setItems(arrVal, (dialog, which) -> {

            if (strTitle.equals("Zone")) {
                zone_tv.setText(arrVal[which]);

                dealer_tv.setText("");
                dealerCount = "0";
                state_tv.setText("");
                location_tv.setText("");
                duration_tv.setText("");

            } else if (strTitle.equals("Duration")) {
                duration_tv.setText(arrVal[which]);
                getDealerPerfomaceParams();


            } else if (strTitle.equals("Dealers")) {
                dealerlistTv.setText(arrVal[which]);

                setDealerPerformanceData(mData.data.get(which));
            }

            if (db.isInternetWorking(DealerDashboardActivity.this)) {
                runOnUiThread(() -> {

                    getChartData();
                   // getDealerPerfomaceParams();

                });
            } else {
                db.alertMessage(DealerDashboardActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
            }

        });
        alert.create();
        alert.show();

    }


    private void chartReset() {

        chart.setCenterText(generateCenterSpannableText(dealerCount, date));

        setData(3, 1);
        chart.animateY(1000, Easing.EaseInOutQuad);
    }


    private void setData(int count, float range) {

        ArrayList<PieEntry> entries = new ArrayList<>();

        // add a lot of colors
        ArrayList<Integer> colors = new ArrayList<>();

        final String[] parties = new String[]{
                ""
        };


        redline.setVisibility(View.INVISIBLE);
        redlabel.setVisibility(View.INVISIBLE);

        yellowline.setVisibility(View.INVISIBLE);
        yellowlabel.setVisibility(View.INVISIBLE);

        greenline.setVisibility(View.INVISIBLE);
        greenlabel.setVisibility(View.INVISIBLE);

        if (greenval != 0) {
            entries.add(new PieEntry(greenval, parties[1 % parties.length]));
            colors.add(rgb("#53b151"));
            greenline.setVisibility(View.VISIBLE);
            greenlabel.setVisibility(View.VISIBLE);
        }
        if (amberval != 0) {
            entries.add(new PieEntry(amberval, parties[2 % parties.length]));
            colors.add(rgb("#fec134"));
            yellowline.setVisibility(View.VISIBLE);
            yellowlabel.setVisibility(View.VISIBLE);
        }
        if (redval != 0) {
            entries.add(new PieEntry(redval, parties[3 % parties.length]));
            colors.add(rgb("#f34423"));
            redline.setVisibility(View.VISIBLE);
            redlabel.setVisibility(View.VISIBLE);
        }

      /* // test only
        entries.add(new PieEntry(8, parties[1 % parties.length]));
        entries.add(new PieEntry(16, parties[2 % parties.length]));
        entries.add(new PieEntry(40, parties[3 % parties.length]));*/

        PieDataSet dataSet = new PieDataSet(entries, "Dealer Dashboard");
        // bar space set
        dataSet.setSliceSpace(0f);
        dataSet.setSelectionShift(0f);

        /*int[] MATERIAL_COLORS = {
                rgb("#2ecc71"), rgb("#f1c40f"), rgb("#e74c3c"), rgb("#3498db")
        };*/

        //new color code changes
       /* for (int c : ColorTemplate.MATERIAL_COLORS)
            colors.add(c);*/

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);

        dataSet.setValueLinePart1OffsetPercentage(80.f);
        dataSet.setValueLinePart1Length(0.2f);
        dataSet.setValueLinePart2Length(0.2f);

        dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(14f);
        data.setValueTextColor(Color.BLACK);
        chart.setData(data);

        // undo all highlights
        chart.highlightValues(null);

        chart.invalidate();
    }

    private SpannableString generateCenterSpannableText(String dealerCount, String date) {

        try {

            SpannableString s = new SpannableString("Total Dealer\n" + dealerCount + "\nWeekly reports dated\n" + date);
            s.setSpan(new RelativeSizeSpan(1.5f), 0, 0, 0);
            s.setSpan(new StyleSpan(Typeface.NORMAL), 0, 0, 0);
            s.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 0, 0);

            if (dealerCount.length() == 1) {
                s.setSpan(new RelativeSizeSpan(1.8f), 12, 15, 0);
                s.setSpan(new RelativeSizeSpan(1.5f), 36, 46, 0);
            } else if (dealerCount.length() == 2) {
                s.setSpan(new RelativeSizeSpan(1.8f), 12, 15, 0);
                s.setSpan(new RelativeSizeSpan(1.5f), 37, 47, 0);
            } else if (dealerCount.length() == 3) {
                s.setSpan(new RelativeSizeSpan(1.8f), 12, 16, 0);
                s.setSpan(new RelativeSizeSpan(1.5f), 38, 48, 0);
            } else {
                s.setSpan(new RelativeSizeSpan(1.8f), 13, 17, 0);
                s.setSpan(new RelativeSizeSpan(1.5f), 36, 48, 0);
            }

            s.setSpan(new StyleSpan(Typeface.NORMAL), 0, 0, 0);
            s.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 0, 0);

            return s;
        } catch (Exception e) {
            return null;

        }
    }


/*    private void graphData() {

        graph1 = findViewById(R.id.graph1);
        graph2 = findViewById(R.id.graph2);
        graph3 = findViewById(R.id.graph3);

        setGraphData(graph1);
        setGraphData(graph2);
        setGraphData(graph3);


    }*/

    private void setGraphData(BarChart graph) {

        graph.setDrawBarShadow(false);
        graph.setDrawValueAboveBar(true);
        graph.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the graph, no values will be
        // drawn
        graph.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        graph.setPinchZoom(false);

        graph.setDrawGridBackground(false);
        // graph.setDrawYLabels(false);

        ValueFormatter xAxisFormatter = new DayAxisValueFormatter(graph);

        XAxis xAxis = graph.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(10f); // only intervals of 1 day
        xAxis.setLabelCount(7);
        xAxis.setValueFormatter(xAxisFormatter);

        Legend l = graph.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.NONE);
        l.setFormSize(12f);
        l.setTextSize(11f);
        l.setXEntrySpace(14f);

    }

    private void resetGraphData(BarChart graph, int red, int amber, int green, String month) {

        graphsetData(red, amber, green, month, graph, 3, 3);
        graph.animateY(1000, Easing.EaseInOutQuad);
        graph.invalidate();
    }


    private void graphsetData(int red, int amber, int green, String month, BarChart graph, int count, float range) {


        ArrayList<BarEntry> values = new ArrayList<>();

        values.add(new BarEntry(0, red, getResources().getDrawable(R.drawable.ic_launcher_background)));
        values.add(new BarEntry(1, amber, getResources().getDrawable(R.drawable.ic_launcher_background)));
        values.add(new BarEntry(2, green, getResources().getDrawable(R.drawable.ic_launcher_background)));


       /* //test only
        values.add(new BarEntry(0, 23, getResources().getDrawable(R.drawable.ic_launcher_background)));
        values.add(new BarEntry(1, 75, getResources().getDrawable(R.drawable.ic_launcher_background)));
        values.add(new BarEntry(2, 197, getResources().getDrawable(R.drawable.ic_launcher_background)));*/

        BarDataSet set1;

        if (graph.getData() != null &&
                graph.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) graph.getData().getDataSetByIndex(0);
            set1.setValues(values);
            graph.getData().notifyDataChanged();
            graph.notifyDataSetChanged();

        } else {
            set1 = new BarDataSet(values, month);

            set1.setDrawIcons(false);

            int endColor1 = ContextCompat.getColor(this, R.color.graph_red);
            int endColor2 = ContextCompat.getColor(this, R.color.graph_yellow);
            int endColor3 = ContextCompat.getColor(this, R.color.graph_green);

            List<Fill> gradientFills = new ArrayList<>();
            gradientFills.add(new Fill(endColor1, endColor1));

            gradientFills.add(new Fill(endColor2, endColor2));
            gradientFills.add(new Fill(endColor3, endColor3));

            set1.setFills(gradientFills);

            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);

            data.setValueTextSize(10f);

            //bar size
            data.setBarWidth(0.18f);

            graph.setData(data);
        }
    }


    private void getDealerPerfomaceParams() {
        DasboardStatusServices dash = new DasboardStatusServices();

        String zone = zone_tv.getText().toString();
        String statecode = stateCode;
        String citycode = cityCode;
        String Friday = duration_tv.getText().toString();
        String dealercode = dealerCatVal;

        dash.getDealerPerformace(this, zone, statecode, citycode, Friday, dealercode, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                Response<DealerPerformanceResponse> mRes = (Response<DealerPerformanceResponse>) obj;
                mData = mRes.body();
                if (mData.data != null) {
                    setDealerPerformanceData(mData.data.get(0));
                } else {
                    refershDealerPerformance();
                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {

            }
        });

    }

    private void getChartData() {

        DasboardStatusServices dash = new DasboardStatusServices();

        String zone = zone_tv.getText().toString();
        String statecode = stateCode;
        String citycode = cityCode;
        String Friday = duration_tv.getText().toString();
        String dealercode = dealerCatVal;

        SpinnerManager.showSpinner(this);
        dash.getChartData(this, zone, statecode, citycode, Friday, dealercode, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                SpinnerManager.hideSpinner(DealerDashboardActivity.this);

                try {
                    Response<chartDataRes> mRes = (Response<chartDataRes>) obj;
                    chartDataRes mData = mRes.body();

                    displayChart(mData);
                } catch (Exception e) {

                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(DealerDashboardActivity.this);
            }


        });
    }


    private void getHighParam() {

        DasboardStatusServices dash = new DasboardStatusServices();

        String zone = zone_tv.getText().toString();
        String statecode = stateCode;
        String citycode = cityCode;
        String Friday = duration_tv.getText().toString();
        String dealercode = dealerCatVal;

        SpinnerManager.showSpinner(this);
        dash.getHighParam(this, zone, statecode, citycode, Friday, dealercode, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                SpinnerManager.hideSpinner(DealerDashboardActivity.this);

                try {
                    Response<highparamRes> mRes = (Response<highparamRes>) obj;
                    highparamRes mData = mRes.body();

                    scrollView.post(() -> scrollView.smoothScrollTo(0, 1000));

                    if (mData != null) displayHighParam(mData);

                } catch (Exception e) {

                }
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(DealerDashboardActivity.this);
            }


        });
    }

    private void getGraphData() {

        DasboardStatusServices dash = new DasboardStatusServices();

        String zone = zone_tv.getText().toString();
        String statecode = stateCode;
        String citycode = cityCode;
        String Friday = duration_tv.getText().toString();
        String dealercode = dealerCatVal;

        SpinnerManager.showSpinner(this);
        dash.getGraphData(this, zone, statecode, citycode, Friday, dealercode, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                SpinnerManager.hideSpinner(DealerDashboardActivity.this);

                try {
                    Response<graphDataRes> mRes = (Response<graphDataRes>) obj;
                    graphDataRes mData = mRes.body();

                    if (mData.getData() != null) {

                        displayGraph(mData);
                        scrollView.post(() -> scrollView.smoothScrollTo(0, 1300));
                    }

                } catch (Exception e) {

                }
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(DealerDashboardActivity.this);
            }
        });
    }

    private void displayChart(chartDataRes mData) {

        if (mData.getLastSubmitedDate() != null) date = mData.getLastSubmitedDate();

        redval = mData.getData().getRed();
        amberval = mData.getData().getAmber();
        greenval = mData.getData().getGreen();

        dealerCount = String.valueOf(redval + amberval + greenval);

        chartReset();
    }

    private void displayHighParam(highparamRes mData) {

        try {

            List<highParaData> data = mData.getData();

            LinearLayout svi = (LinearLayout) findViewById(R.id.highper_data);
            svi.removeAllViews();
            LayoutInflater layoutInflater = getLayoutInflater();

            if (data.isEmpty() || data.size() == 0) return;

            for (int i = 0; i < data.size(); i++) {
                View rowView = layoutInflater.inflate(R.layout.row_high_layout, null, false);

                // Left
                LinearLayout linearLayoutLeft = (LinearLayout) rowView.findViewById(R.id.row_left_element);
                TextView title = (TextView) linearLayoutLeft.findViewById(R.id.default_id_title);
                TextView red = (TextView) linearLayoutLeft.findViewById(R.id.default_id_option1);
                TextView amber = (TextView) linearLayoutLeft.findViewById(R.id.default_id_option2);
                TextView green = (TextView) linearLayoutLeft.findViewById(R.id.default_id_option3);

                String head = data.get(i).getParameterName();

                if (head.equalsIgnoreCase("Sales_Plan")) {
                    title.setText("Sales Plan for the Month");
                } else {
                    head = head.replace("_", " ");
                    title.setText(head);
                }

                red.setText(String.valueOf(data.get(i).getRed()));
                amber.setText(String.valueOf(data.get(i).getAmber()));
                green.setText(String.valueOf(data.get(i).getGreen()));

                svi.addView(rowView);
            }

            svi.refreshDrawableState();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void displayGraph(graphDataRes mData) {

        try {

            LinearLayout svi = (LinearLayout) findViewById(R.id.graph_ll);
            svi.removeAllViews();
            LayoutInflater layoutInflater = getLayoutInflater();

            for (int i = 0; i < mData.getData().size(); i++) {
                View rowView = layoutInflater.inflate(R.layout.graph_row, null, false);

                // Left
                BarChart graph1 = (BarChart) rowView.findViewById(R.id.graph1);
                setGraphData(graph1);

                int red = mData.getData().get(i).getRed();
                int amber = mData.getData().get(i).getAmber();
                int green = mData.getData().get(i).getGreen();

                String month = mData.getData().get(i).getMonth();

                resetGraphData(graph1, red, amber, green, month);

                svi.addView(rowView);
            }

            svi.refreshDrawableState();

        } catch (Exception e) {

        }
    }

    private void dashboradpopup() {
        final Dialog dialog = new Dialog(this);
        //Theme_DeviceDefault_Light_DarkActionBar
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dashboardpopup);
        dialog.setCancelable(true);

        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.BOTTOM | Gravity.RIGHT;
        wmlp.x = 100;   //x position
        wmlp.y = 150;   //y position

        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        TextView listdealer_tv = dialog.findViewById(R.id.listdealer_tv);
        TextView highper_tv = dialog.findViewById(R.id.highper_tv);
        TextView pastper_tv = dialog.findViewById(R.id.pastper_tv);

        listdealer_tv.setOnClickListener(v -> {

            highpara_ll.setVisibility(View.GONE);
            pastp_ll.setVisibility(View.GONE);
            dealerlist_ll.setVisibility(View.VISIBLE);
            dialog.dismiss();
        });

        highper_tv.setOnClickListener(v -> {

            pastp_ll.setVisibility(View.GONE);
            dealerlist_ll.setVisibility(View.GONE);
            highpara_ll.setVisibility(View.VISIBLE);
            getHighParam();
            dialog.dismiss();
        });

        pastper_tv.setOnClickListener(v -> {

            dealerlist_ll.setVisibility(View.GONE);
            highpara_ll.setVisibility(View.GONE);
            highpara_ll.setVisibility(View.GONE);
            pastp_ll.setVisibility(View.VISIBLE);

            if (db.isInternetWorking(DealerDashboardActivity.this)) {
                getGraphData();
            } else {
                db.alertMessage(DealerDashboardActivity.this, GlobalText.CHECK_NETWORK_CONNECTION);
            }
            dialog.dismiss();
        });

        dialog.show();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    private Drawable getColrCode(int code) {

        Drawable color = null;

        if (code == 1) {

            color = this.getResources().getDrawable(R.drawable.circle_red);
        }

        if (code == 2) {
            color = this.getResources().getDrawable(R.drawable.circle_amber);
        }

        if (code == 3) {
            color = this.getResources().getDrawable(R.drawable.circle_green);
        }
        if (code == 4) {
            color = null;
        }


        return color;
    }


    private void setDealerPerformanceData(Datum data) {
        try {
            if (data != null) {
                String dealerName = data.dealerName;
                dealerlistTv.setText(dealerName);

                countopeningstock.setText(data.parameters.openingStock.get(0).toString());
                colorcodeopeningstock.setBackground(getColrCode(data.parameters.openingStock.get(1)));

                countclosingstock.setText(data.parameters.closingStock.get(0).toString());
                colorcodeclosingstock.setBackground(getColrCode(data.parameters.closingStock.get(1)));

                countsalesplan.setText(data.parameters.salesPlan.get(0).toString());
                colorcodesalesplan.setBackground(getColrCode(data.parameters.salesPlan.get(1)));

                countprocplan.setText(data.parameters.procurementPlan.get(0).toString());
                colorcodeprocplan.setBackground(getColrCode(data.parameters.procurementPlan.get(1)));

                countwarranty.setText(data.parameters.warrantyUnits.get(0).toString());
                colorcodewarranty.setBackground(getColrCode(data.parameters.warrantyUnits.get(1)));

                countwarrantyvalues.setText(data.parameters.warrantyValue.get(0).toString());
                colorcodewarrantyvalues.setBackground(getColrCode(data.parameters.warrantyValue.get(1)));

                countprociep.setText(data.parameters.procurementFromIEP.get(0).toString());
                colorcodeprociep.setBackground(getColrCode(data.parameters.procurementFromIEP.get(1)));

                countproccpt.setText(data.parameters.procurementFromCPT.get(0).toString());
                colorcodeproccpt.setBackground(getColrCode(data.parameters.procurementFromCPT.get(1)));

                countprocplan.setText(data.parameters.procurementPlan.get(0).toString());
                colorcodeprocplan.setBackground(getColrCode(data.parameters.procurementPlan.get(1)));

                countwalkins.setText(data.parameters.walkIns.get(0).toString());
                colorcodewalkins.setBackground(getColrCode(data.parameters.walkIns.get(1)));

                countsalesconv.setText(data.parameters.salesConversion.get(0).toString());
                colorcodesales.setBackground(getColrCode(data.parameters.salesConversion.get(1)));

                countoms.setText(data.parameters.oMSLeads.get(0).toString());
                colorcodeoms.setBackground(getColrCode(data.parameters.oMSLeads.get(1)));

                countconoms.setText(data.parameters.conversionThroughOMSLeads.get(0).toString());
                colorcodeconoms.setBackground(getColrCode(data.parameters.conversionThroughOMSLeads.get(1)));
            } else {
                refershDealerPerformance();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void refershDealerPerformance() {
        try {

            dealerlistTv.setText("");

            countopeningstock.setText("0");
            colorcodeopeningstock.setBackground(null);

            countclosingstock.setText("0");
            colorcodeclosingstock.setBackground(null);

            countsalesplan.setText("0");
            colorcodesalesplan.setBackground(null);

            countprocplan.setText("0");
            colorcodeprocplan.setBackground(null);

            countwarranty.setText("0");
            colorcodewarranty.setBackground(null);

            countwarrantyvalues.setText("0");
            colorcodewarrantyvalues.setBackground(null);

            countprociep.setText("0");
            colorcodeprociep.setBackground(null);

            countproccpt.setText("0");
            colorcodeproccpt.setBackground(null);

            countprocplan.setText("0");
            colorcodeprocplan.setBackground(null);

            countwalkins.setText("0");
            colorcodewalkins.setBackground(null);

            countsalesconv.setText("0");
            colorcodesales.setBackground(null);

            countoms.setText("0");
            colorcodeoms.setBackground(null);

            countconoms.setText("0");
            colorcodeconoms.setBackground(null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
