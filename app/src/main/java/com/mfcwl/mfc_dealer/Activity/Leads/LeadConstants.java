package com.mfcwl.mfc_dealer.Activity.Leads;

public class LeadConstants {

    public static final String LEAD_STATUS_INFO = "statusinfo";

    public static final String LEAD_POST_TODAY = "psotDtoday";
    public static final String LEAD_POST_YESTER = "psotDyesterday";
    public static final String LEAD_POST_7DAYS = "psotD7days";
    public static final String LEAD_POST_15DAYS = "psotD15days";
    public static final String LEAD_POST_CUSTDATE = "chpDCustomfromtodate";

    public static final String LEAD_FOLUP_TMRW = "fwotDtomorrow";
    public static final String LEAD_FOLUP_TODAY = "fwotDtoday";
    public static final String LEAD_FOLUP_YESTER = "fwotDyester";
    public static final String LEAD_FOLUP_7DAYS = "fwotD7days";
    public static final String LEAD_FOLUP_CUSTDATE = "chfDCustomfromtodate";

}
