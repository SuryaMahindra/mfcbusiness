package com.mfcwl.mfc_dealer.Activity;

import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.retrofitconfig.BaseService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class ResetPasswordService  extends ResetPassBaseService {


    public static void passwordReset(final Object request, HttpCallResponse mCallBack){
        ResetPassInterface mInterface = retrofit.create(ResetPassInterface.class);
        Call<ResetPassResponse> mCall = mInterface.resetPassword(request);
        mCall.enqueue(new Callback<ResetPassResponse>() {
            @Override
            public void onFailure(Call<ResetPassResponse> call, Throwable t) {
                      mCallBack.OnFailure(t);
            }

            @Override
            public void onResponse(Call<ResetPassResponse> call, Response<ResetPassResponse> response) {
                if(response.isSuccessful()){
                    mCallBack.OnSuccess(response);
                }
            }
        });
    }




    public interface ResetPassInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("resetpassword/resetpassword")
        Call<ResetPassResponse> resetPassword(@Body Object request);
    }
}
