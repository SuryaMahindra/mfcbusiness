package com.mfcwl.mfc_dealer.Activity.ASM_Reports;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.SubmittedReportModel;
import com.mfcwl.mfc_dealer.AddStockModel.dvrPointData;
import com.mfcwl.mfc_dealer.R;

import java.util.ArrayList;

public class SubmittedDVRExpanded extends AppCompatActivity {
    ImageView backBtn;

    TextView dealerCategorySelect, dvr_next_btn, dvr_sumbit_btn, point_tv, dvr_pre_btn,followup_date_set,dealer_name,textDate;

    public static TextView focus_area_dd,target_date_set, target_date;
public String Json="",dealer="";
    public int level = 1;
    SubmittedReportModel dvrReq;
    ArrayList<dvrPointData> points;
    EditText  pointsDiscussed, action_planned, responsibility_tv;
    LinearLayout followup_ll;
    ArrayList<dvrPointData> lists;
    public String TAG =getClass().getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate: ");
        setContentView(R.layout.activity_dealer_visit_report);

        backBtn = findViewById(R.id.createRepo_backbtn);

        try {

            Json = getIntent().getStringExtra("Json");
            dealer = getIntent().getStringExtra("dealer");

            Log.i(TAG, "onCreate: "+Json);

            Gson gson = new Gson();
            dvrReq = gson.fromJson(Json.toString(), SubmittedReportModel.class);

            lists = (ArrayList<dvrPointData>) dvrReq.getPoints();

        }catch (Exception e){
            e.printStackTrace();
        }


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 finish();
            }
    });



        focus_area_dd = findViewById(R.id.focus_area_dd);
        pointsDiscussed = findViewById(R.id.points_discussed);
        action_planned = findViewById(R.id.action_planned);
        responsibility_tv = findViewById(R.id.responsibility_tv);
        target_date = findViewById(R.id.target_date);
        followup_ll = findViewById(R.id.followup_ll);
        followup_date_set = findViewById(R.id.followup_date_set);
        dealer_name = findViewById(R.id.dealer_name);
        dealer_name.setText("Dealer Name");

        dvr_next_btn = findViewById(R.id.dvr_next_btn);
        dvr_sumbit_btn = findViewById(R.id.dvr_sumbit_btn);
        dvr_sumbit_btn.setVisibility(View.INVISIBLE);
        point_tv = findViewById(R.id.point_tv);
        dvr_pre_btn = findViewById(R.id.dvr_pre_btn);
        target_date_set = findViewById(R.id.target_date_set);
        textDate = findViewById(R.id.textDate);

        textDate.setText(dvrReq.getDaytv());

      /*  SimpleDateFormat sdf = new SimpleDateFormat("EEE d MMM HH:mm", Locale.getDefault());
        String currentDateandTime = sdf.format(dvrReq.getCreated());


        textDate.setText(currentDateandTime.replace(","," "));
*/

        dealerCategorySelect = findViewById(R.id.spinnerDealer);
        dealerCategorySelect.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        focus_area_dd.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

        followup_ll.setVisibility(View.VISIBLE);

        target_date.setVisibility(View.INVISIBLE);

        //focus_area_dd.setInputType(0);
     /*   pointsDiscussed.setInputType(1);
        action_planned.setInputType(2);
        responsibility_tv.setInputType(3);*/

        focus_area_dd.setEnabled(false);
        pointsDiscussed.setEnabled(false);
        action_planned.setEnabled(false);
        responsibility_tv.setEnabled(false);

        Button btn = findViewById(R.id.sub_dealer_visit);
        btn.setVisibility(View.GONE);

        dvr_next_btn.setOnClickListener(v -> {

                try {
                    nextpreSetData();
                } catch (Exception e) {
                    e.printStackTrace();
                }



        });


        dvr_pre_btn.setOnClickListener(v -> {
            try {

                preSetData();
            } catch (Exception e) {
                e.printStackTrace();
            }

        });


        dealerCategorySelect.setText(dealer);

        int len =0;

        if(dvrReq.getFollow_date()!=null) {

            if (!dvrReq.getFollow_date().isEmpty()) {
                len =dvrReq.getFollow_date().length();
            }
        }

        if(len>10) {

            followup_date_set.setText(dvrReq.getFollow_date().substring(0, 10));
        }else{
            followup_date_set.setText(dvrReq.getFollow_date());

        }

        if(!dvrReq.getPoints().isEmpty()) {

            focus_area_dd.setText(dvrReq.getPoints().get(level - 1).getFocusArea());
            pointsDiscussed.setText(dvrReq.getPoints().get(level - 1).getPointsDiscussed());
            action_planned.setText(dvrReq.getPoints().get(level - 1).getActionPlan());
            responsibility_tv.setText(dvrReq.getPoints().get(level - 1).getResponsibility());

            int lens=0;

            if(dvrReq.getPoints().get(level - 1).getTargetDate()!=null) {

                if (!dvrReq.getPoints().get(level - 1).getTargetDate().isEmpty()) {
                    lens = dvrReq.getPoints().get(level - 1).getTargetDate().length();
                }
            }

            if(lens>10) {
                target_date_set.setText(dvrReq.getPoints().get(level - 1).getTargetDate().substring(0,10));
            }else{
                target_date_set.setText(dvrReq.getPoints().get(level - 1).getTargetDate());
            }

        }

        if(dvrReq.getPoints().isEmpty()|| dvrReq.getPoints().size()==0) {
            dvr_next_btn.setVisibility(View.INVISIBLE);
        }else{
            dvr_next_btn.setVisibility(View.VISIBLE);
        }

        point_tv.setText("Point Discussed " + String.valueOf(level));

    }

    private void nextpreSetData() {

        try {


            if((lists.size())==level || dvrReq.getPoints().isEmpty()){
                dvr_next_btn.setVisibility(View.INVISIBLE);
                return;
            }

            dvr_next_btn.setVisibility(View.VISIBLE);

            level = level + 1;

            if (level >= 2) {
                dvr_pre_btn.setVisibility(View.VISIBLE);
            } else {
                dvr_pre_btn.setVisibility(View.INVISIBLE);
            }

            point_tv.setText("Point Discussed " + String.valueOf(level));

            if (dvrReq.getPoints().size() >= level) {

                action_planned.setText(dvrReq.getPoints().get(level - 1).getActionPlan());
                focus_area_dd.setText(dvrReq.getPoints().get(level - 1).getFocusArea());
                pointsDiscussed.setText(dvrReq.getPoints().get(level - 1).getPointsDiscussed());
                responsibility_tv.setText(dvrReq.getPoints().get(level - 1).getResponsibility());



                int lens=0;

                if(dvrReq.getPoints().get(level - 1).getTargetDate()!=null) {

                    if (!dvrReq.getPoints().get(level - 1).getTargetDate().isEmpty()) {
                        lens = dvrReq.getPoints().get(level - 1).getTargetDate().length();
                    }
                }

                if(lens>10) {
                    target_date_set.setText(dvrReq.getPoints().get(level - 1).getTargetDate().substring(0,10));
                }else{
                    target_date_set.setText(dvrReq.getPoints().get(level - 1).getTargetDate());
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        if((lists.size())==level || dvrReq.getPoints().isEmpty()){
            dvr_next_btn.setVisibility(View.INVISIBLE);
        }
    }


    private void preSetData() {

        try {

            if(dvrReq.getPoints().isEmpty()){
                return;
            }

            level = level - 1;
            if (level <= 1) {
                dvr_pre_btn.setVisibility(View.INVISIBLE);

            }

            dvr_next_btn.setVisibility(View.VISIBLE);

            action_planned.setText(dvrReq.getPoints().get(level - 1).getActionPlan());
            focus_area_dd.setText(dvrReq.getPoints().get(level - 1).getFocusArea());
            pointsDiscussed.setText(dvrReq.getPoints().get(level - 1).getPointsDiscussed());
            responsibility_tv.setText(dvrReq.getPoints().get(level - 1).getResponsibility());

            int lens=0;

            if(dvrReq.getPoints().get(level - 1).getTargetDate()!=null) {

                if (!dvrReq.getPoints().get(level - 1).getTargetDate().isEmpty()) {
                    lens = dvrReq.getPoints().get(level - 1).getTargetDate().length();
                }
            }

            if(lens>10) {
                target_date_set.setText(dvrReq.getPoints().get(level - 1).getTargetDate().substring(0,10));
            }else{
                target_date_set.setText(dvrReq.getPoints().get(level - 1).getTargetDate());
            }

            point_tv.setText("Point Discussed " + String.valueOf(level));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
