package com.mfcwl.mfc_dealer.Activity.ASM_Reports;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.ASMAdapter.ASMReportAdapter.SRLastTtyDaysAdapter;
import com.mfcwl.mfc_dealer.ASMAdapter.ASMReportAdapter.SubmittedRptAdapterWR;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.SRLastTtyDaysModel;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.SubmittedDVReport;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.SubmittedReportModel;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.WeekReportSubmitModel;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.WeeklyRModel;
import com.mfcwl.mfc_dealer.ASMReportsServices.DvReport;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.Helper;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;

import retrofit2.Response;

public class SubmittedReports extends AppCompatActivity {

    SubmittedReportModel submittedReportModel = new SubmittedReportModel();
    SRLastTtyDaysModel srLastTtyDaysModel = new SRLastTtyDaysModel();

    static RecyclerView recyclerView;
    static SubmittedRptAdapterWR adapter;
    static SRLastTtyDaysAdapter srAdapter;
    static TextView dealerCategorySelect, dateText;
    int count = 10;

    static String dealerCatVal = "select", dealer_name;

    static String dateFormat = "YYYY-MM-DD";

    static LinearLayout customLayout;
    static TextView tv_Last7Days, tv_Last30Days, tv_Custom;
    static ImageView backBtn;

    static List<SubmittedDVReport> data;
    static ArrayList<WeekReportSubmitModel> list = new ArrayList<WeekReportSubmitModel>();
    static LinearLayoutManager layoutManager;
    // TextView btn;

    static Calendar Calendar;
    static ProgressBar pgs;
    static String date1;

    final static Calendar myCalendar = Calendar.getInstance();
    static String curdate, prevdate;
    private static TextView fromdate, todate,titleWR_tv;
    static TextView notitxt;

    static Context mcontext, c;

    private static boolean TO = false, FROM = false, SELECTED;

    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submitted_wr);
        Log.i(TAG, "onCreate: ");
        mcontext = getApplicationContext();
        list.clear();
        SimpleDateFormat sdf = new SimpleDateFormat("EEE", Locale.getDefault());
        Date d = new Date();
        String dayOfTheWeek = sdf.format(d);
        Log.i("dayofweek", "onCreate: " + dayOfTheWeek);
        setContentView(R.layout.activity_submitted_reports);

        backBtn = findViewById(R.id.createRepo_backbtn);
        titleWR_tv = findViewById(R.id.titleWR_tv);
        titleWR_tv.setText("Submitted WR");

        dealerCatVal = "select";
        LinkedHashSet<String> lnkHash = new LinkedHashSet<>(Helper.dealerName);
        ArrayList<String> dealerName = new ArrayList<String>(lnkHash);

        LinkedHashSet<String> hashset = new LinkedHashSet<>(Helper.dealerCode);
        ArrayList<String> dealerCode = new ArrayList<String>(hashset);
        //DvReportModel[] dvReportModels = new DvReportModel[];
        curdate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        prevdate = getCalculatedDate(-6);
        pgs = findViewById(R.id.progressBar2);
        recyclerView = (RecyclerView) findViewById(R.id.srrecyclerview);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        adapter = new SubmittedRptAdapterWR(getApplicationContext(), list, SubmittedReports.this);
        todate = findViewById(R.id.todate);
        //todate.setEnabled(false);
        todate.setText(curdate.toString());
        c = SubmittedReports.this;

        String abb = curdate.substring(0, 8) + "01";
        Log.i("datenow", "onCreate: " + abb);

        String datenow;
        fromdate = findViewById(R.id.fdate);
        notitxt = findViewById(R.id.notificationText);
        notitxt.setVisibility(View.GONE);

        fromdate.setText(abb.toString());
        customLayout = findViewById(R.id.customDate_layOut);
        customLayout.setVisibility(View.GONE);
        SELECTED = false;

        //btn = findViewById(R.id.fetchbutton);





        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SubmittedReports.this, MainActivity.class);
                startActivity(intent);
            }
        });

        dealerCategorySelect = findViewById(R.id.select_dealer);
        dealerCategorySelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<String> dealerName = new ArrayList<String>();
                dealerName.clear();

                dealerName = Helper.dealerName;

                if(!dealerName.isEmpty()) {

                    if(!dealerName.get(dealerName.size()-1).equalsIgnoreCase("Others")){
                        dealerName.add(dealerName.size(),"Others");
                    }
                }else{
                    dealerName.add(dealerName.size(), "Others");
                }



                List<String> dealerCode = new ArrayList<String>();
                dealerCode.clear();
                dealerCode = Helper.dealerCode;


                if(!dealerCode.isEmpty()) {

                    if(!dealerCode.get(dealerCode.size()-1).equalsIgnoreCase("Others")){
                        dealerCode.add(dealerCode.size(),"Others");
                    }
                }else{
                    dealerCode.add(dealerCode.size(), "Others");
                }




                String[] namesArr = dealerName.toArray(new String[dealerName.size()]);

                String[] codeArr = dealerCode.toArray(new String[dealerCode.size()]);


              /*
                String[] namesArr = dealerName.toArray(new String[dealerName.size()+1]);

                String[] codeArr = dealerCode.toArray(new String[dealerCode.size()]);*/

                setAlertDialog(v, SubmittedReports.this, "Dealer Category", namesArr, codeArr);

                customLayout.setVisibility(View.GONE);
                prevdate = getCalculatedDate(-6);
                fetchdata(dealerCatVal, prevdate, curdate);
                setRecyclerView();
            }
        });


        tv_Last7Days = findViewById(R.id.lastsevdaystv);
        tv_Last7Days.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                if (!SELECTED) {
                                                    Toast.makeText(getApplicationContext(), "Please select the dealer", Toast.LENGTH_LONG).show();
                                                } else {
//                                                        tv_Last7Days.setTextColor(getResources().getColor(R.color.Red));

                                                    tv_Last7Days.setTextColor(getResources().getColor(R.color.orange));
                                                    tv_Last30Days.setTextColor(getResources().getColor(R.color.white));
                                                    tv_Custom.setTextColor(getResources().getColor(R.color.white));
                                                }
                                                customLayout.setVisibility(View.GONE);
                                                prevdate = getCalculatedDate(-6);
                                                try {
                                                    fetchdata(dealerCatVal, prevdate, curdate);
                                                } catch (Exception e) {

                                                }
                                                recyclerView = (RecyclerView) findViewById(R.id.srrecyclerview);
                                                LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                                                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                                                recyclerView.setLayoutManager(layoutManager);
                                                adapter = new SubmittedRptAdapterWR(getApplicationContext(), list, c);
                                                recyclerView.setAdapter(adapter);

                                            }
                                        }
        );

        tv_Last30Days = findViewById(R.id.lastthirtydystv);

        tv_Last30Days.setOnClickListener(new View.OnClickListener() {
                                             @Override
                                             public void onClick(View v) {

                                                 if (!SELECTED) {
                                                     Toast.makeText(getApplicationContext(), "Please select the dealer", Toast.LENGTH_LONG).show();
                                                 } else {

                                                     tv_Last30Days.setTextColor(getResources().getColor(R.color.orange));
                                                     tv_Last7Days.setTextColor(getResources().getColor(R.color.white));
                                                     tv_Custom.setTextColor(getResources().getColor(R.color.white));
                                                 }
                                                 try {
                                                     fetchdata(dealerCatVal, prevdate, curdate);
                                                 } catch (Exception e) {

                                                 }


                                                 customLayout.setVisibility(View.GONE);
                                                 prevdate = getCalculatedDate(-29);
                                                 fetchdata(dealerCatVal, prevdate, curdate);
                                                 ArrayList<SubmittedReportModel> lists = submittedReportModel.submittedReportModels();
                                                 recyclerView = (RecyclerView) findViewById(R.id.srrecyclerview);
                                                 LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                                                 layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                                                 recyclerView.setLayoutManager(layoutManager);
                                                 adapter = new SubmittedRptAdapterWR(getApplicationContext(), list, c);
                                                 recyclerView.setAdapter(adapter);
                                                 recyclerView.setOnClickListener(new View.OnClickListener() {
                                                     @Override
                                                     public void onClick(View v) {

                                                     }
                                                 });
                                             }
                                         }
        );
        tv_Custom = findViewById(R.id.customswrtv);

        tv_Custom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SELECTED) {
                    Toast.makeText(getApplicationContext(), "Please select the dealer", Toast.LENGTH_LONG).show();
                } else {

                    list.clear();

//                date1 = curdate;
//                date1 = todate.getText().toString();

//                fetchdata(dealerCatVal, prevdate, date1);

                    String datefrom = fromdate.getText().toString();
                    String dateto = todate.getText().toString();
                    fetchdata(dealerCatVal, datefrom, dateto);
                    tv_Last30Days.setTextColor(getResources().getColor(R.color.white));
                    tv_Last7Days.setTextColor(getResources().getColor(R.color.white));
                    tv_Custom.setTextColor(getResources().getColor(R.color.orange));
                    setRecyclerView();
                    customLayout.setVisibility(View.VISIBLE);
                }


                // ArrayList<SubmittedReportModel> lists = submittedReportModel.submittedReportModels();

                fromdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FROM = true;
                        showDatePickerDialog(v);
                        //setRecyclerView();
                    }
                });


                //prevdate = "";
                todate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // btn.setVisibility(View.VISIBLE);
                        FROM = false;
                        showDatePickerDialog1(v);


                    }


                });


                if (dealerCatVal.equalsIgnoreCase("select")) {
                    dealerCategorySelect.setText("select");
                }


            }
        });
    }


    private void setAlertDialog(View v, Activity a, final String strTitle, final String[] arrVal, final String[] codeVal) {

        AlertDialog.Builder alert = new AlertDialog.Builder(a);
        alert.setTitle(strTitle);
        dealerCatVal = "select";
        alert.setItems(arrVal, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dealerCategorySelect.setText(arrVal[which]);
                dealer_name = arrVal[which];
                dealerCatVal = codeVal[which];
                Log.i("dealercode", dealerCatVal);
                SELECTED = true;
                //todate.setText(dateFormat);
                //fromdate.setText(dateFormat);

                Log.i("CurrentDate", curdate);
                Log.i("prevdateDate", prevdate);

                //pgs.setVisibility(View.VISIBLE);

                initdata();

            }
        });
        customLayout.setVisibility(View.GONE);

        alert.create();
        alert.show();

    }

    public void initdata() {

        recyclerView.setVisibility(View.VISIBLE);
        tv_Last7Days.setTextColor(getResources().getColor(R.color.orange));
        tv_Last30Days.setTextColor(getResources().getColor(R.color.white));
        tv_Custom.setTextColor(getResources().getColor(R.color.white));
        prevdate = getCalculatedDate(-6);
        try {
            fetchdata(dealerCatVal, prevdate, curdate);
            setRecyclerView();
        } catch (Exception e) {

        }
    }

    public String getCalculatedDate(int days) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        cal.add(Calendar.DAY_OF_YEAR, days);
        return s.format(new Date(cal.getTimeInMillis()));
    }

    public static void setRecyclerView() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(mcontext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new SubmittedRptAdapterWR(mcontext, list, c);
        recyclerView.setAdapter(adapter);

    }

    public static void getData() {
        String date1 = fromdate.getText().toString();
        String date2 = todate.getText().toString();
        int dateclick = -1, datenow = -1, dateclickfrom = -1;
        try {
            String check0 = date1.replace("-", "");
            dateclickfrom = Integer.parseInt(check0);
            String check1 = date2.replace("-", "");
            dateclick = Integer.parseInt(check1);
            String check2 = curdate.replace("-", "");
            datenow = Integer.parseInt(check2);
        } catch (Exception e) {

        }
        Log.i("datedata", "onClick: " + date1);
        if (dateclick <= datenow && dateclickfrom <= datenow && dateclick >= dateclickfrom && !date1.equalsIgnoreCase("YYYY-MM-DD") && !date2.equalsIgnoreCase("YYYY-MM-DD")
                && dateclick != -1 && datenow != -1 && dateclickfrom != -1) {
            try {
                // btn.setVisibility(View.GONE);
                fetchdata(dealerCatVal, date1, date2);
                setRecyclerView();
            } catch (Exception e) {

            }
        } else {
            Toast.makeText(mcontext, "please check the date entered", Toast.LENGTH_LONG).show();
        }
    }

    public void showDatePickerDialog(View v) {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
        //fromdate.setText(newFragment.showDate());

    }

    public void showDatePickerDialog1(View v) {
        DatePickerFragment newFragment1 = new DatePickerFragment();
        newFragment1.show(getSupportFragmentManager(), "datePicker");
        //fromdate.setText(newFragment.showDate());

    }

    public static void fetchdata(String dealercode, String fromDate, String todate) {
        if (SELECTED && !dealerCatVal.equalsIgnoreCase("select"))
            SpinnerManager.showSpinner(c);
        list.clear();
        WeeklyRModel weeklyRModel = new WeeklyRModel(dealercode, "AM", fromDate, todate);
        DvReport dvReport = new DvReport();
        dvReport.getWR(weeklyRModel, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                list.clear();
                Response<List<WeekReportSubmitModel> >res = (Response<List<WeekReportSubmitModel>>) obj;
                Log.i("successmsg", "data" + obj.toString());
                List<WeekReportSubmitModel>data = res.body();
                try {
                    if (!data.toString().equalsIgnoreCase("[]")) {
                        recyclerView.setVisibility(View.VISIBLE);
                        notitxt.setVisibility(View.GONE);
//                        pgs.setVisibility(View.GONE);
                        SpinnerManager.hideSpinner(c);
                        for (WeekReportSubmitModel svr : data) {

                            list.add(svr);
                        }


                        setRecyclerView();
                    } else {
                        SpinnerManager.hideSpinner(c);
//                        list.clear();
                        list.add(new WeekReportSubmitModel());
                        setRecyclerView();
                        recyclerView.setVisibility(View.GONE);
                        notitxt.setVisibility(View.VISIBLE);
                        notitxt.setText("No data");
//                        Toast.makeText(mcontext,"No data",Toast.LENGTH_SHORT).show();
                        //fetchdata(dealerCatVal, fromDate, todate);

                    }
                } catch (Exception e) {

                    SpinnerManager.hideSpinner(c);

                    list.add(new WeekReportSubmitModel());
                    setRecyclerView();
                    Toast.makeText(mcontext, "No data", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                Log.i("failuremsg", "OnFailure: "+mThrowable);
                Toast.makeText(mcontext,"fail",Toast.LENGTH_SHORT).show();
                SpinnerManager.hideSpinner(c);
            }
        });

        setRecyclerView();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(SubmittedReports.this, MainActivity.class);
        startActivity(intent);

    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        static int year, month, day;
        static Calendar cal = java.util.Calendar.getInstance();

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = cal.getInstance();
            int year = c.get(cal.YEAR);
            int month = c.get(cal.MONTH);
            int day;
            if (FROM)
                day = 1;
            else
                day = c.get(cal.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            this.year = year;
            this.month = month + 1;

//            if(FROM)
//                this.day = 1;
//            else
            this.day = day;
            if (FROM) {


                fromdate.setText(showDate().toString());
                getData();
                setRecyclerView();
//                todate.setEnabled(true);
            } else {
                todate.setText(showDate().toString());
//                String datefrom = fromdate.getText().toString();
//                String dateto = todate.getText().toString();
//                fetchdata(dealerCatVal,datefrom,dateto);
                getData();
                setRecyclerView();
                // showDate();
                //setCustom();
            }

        }

        public static StringBuilder showDate() {
            StringBuilder str = new StringBuilder();
            String m = Integer.toString(month);
            String d = Integer.toString(day);
            if (m.length() == 1 && d.length() == 1) {
                str.append(year).append("-").append("0")
                        .append(month).append("-").append("0").append(day);
            } else if (m.length() == 1) {
                str.append(year).append("-").append("0")
                        .append(month).append("-").append(day);
            } else if (d.length() == 1) {
                str.append(year).append("-")
                        .append(month).append("-").append("0").append(day);
            } else {
                str.append(year).append("-")
                        .append(month).append("-").append(day);
            }
            return str;
        }
    }
}
