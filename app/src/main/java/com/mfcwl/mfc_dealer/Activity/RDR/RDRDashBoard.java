package com.mfcwl.mfc_dealer.Activity.RDR;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mfcwl.mfc_dealer.Activity.RDR.Adapter.RDRDashboardAdapter;
import com.mfcwl.mfc_dealer.Activity.RDR.Model.DashboardData;
import com.mfcwl.mfc_dealer.Activity.RDR.Model.DashboardRequest;
import com.mfcwl.mfc_dealer.Activity.RDR.Model.DashboardResponse;
import com.mfcwl.mfc_dealer.Activity.RDR.Services.RDRDashboardService;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GAConstants;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import java.util.ArrayList;
import java.util.List;

import imageeditor.base.BaseActivity;
import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.activity;

public class RDRDashBoard extends BaseActivity {

    private ImageView mBack;
    private TextView mTotalCount;
    private LinearLayout viewAll;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView recyclerView;
    private RDRDashboardAdapter mAdapter;
    private DashboardRequest mRequest;
    private List<DashboardData> mData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rdr_dashboard);
        Log.i(TAG, "onCreate: ");
        initViews();
        setLitners();
        mRequest = new DashboardRequest();
        Log.i(TAG, "onCreate: ");
        mRequest.userType = "AM";
        mRequest.userId = CommonMethods.getstringvaluefromkey(this, "user_id");
       // loadDummyData();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.black));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("RDR Dashboard");
        getRDRDashboardData(mRequest);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        MenuItem searchItem = menu.findItem(R.id.search_bar);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    private void initViews() {
        mData = new ArrayList<>();
      //  mBack = findViewById(R.id.createRepo_backbtn);
        mTotalCount = findViewById(R.id.totalcount);
        recyclerView = findViewById(R.id.rdrdashboardview);
        viewAll =  findViewById(R.id.viewAll);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
    }


    private void setLitners() {
      //  mBack.setOnClickListener(this);
        viewAll.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {

            case R.id.createRepo_backbtn:
                finish();
                break;

            case R.id.viewAll:
                Intent detailedIntent = new Intent(RDRDashBoard.this, ViewDetailedListing.class);
                startActivity(detailedIntent);
                break;

        }
    }

    private void getRDRDashboardData(DashboardRequest mRequest) {
        SpinnerManager.showSpinner(RDRDashBoard.this);
        RDRDashboardService.fetchRDRData(mRequest, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(RDRDashBoard.this);
                Response<DashboardResponse> mRes = (Response<DashboardResponse>) obj;
                DashboardResponse data = mRes.body();
                mData = data.data;
                mAdapter = new RDRDashboardAdapter(mData,RDRDashBoard.this);
                recyclerView.setAdapter(mAdapter);
                mTotalCount.setText("" + mData.size() + " Dealers Available");
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(RDRDashBoard.this);

            }
        });
    }

    private void loadDummyData() {
        DashboardData data = new DashboardData();

        mData = new ArrayList<>();

        data.rdrOverdue = "26";
        data.rdrComplete = "67";
        data.rdrPending = "89";
        data.dealerName = "Surya";
        mData.add(data);

        DashboardData data1 = new DashboardData();
        data1.rdrOverdue = "28";
        data1.rdrComplete = "60";
        data1.rdrPending = "890";
        data1.dealerName = "Raju";

        mData.add(data1);

        DashboardData data2 = new DashboardData();
        data2.rdrOverdue = "23";
        data2.rdrComplete = "68";
        data2.rdrPending = "80";
        data2.dealerName = "Hema";

        mData.add(data2);

        DashboardData data3 = new DashboardData();
        data3.rdrOverdue = "23";
        data3.rdrComplete = "68";
        data3.rdrPending = "80";
        data3.dealerName = "Sindhu";

        mData.add(data3);
        mAdapter = new RDRDashboardAdapter(mData,RDRDashBoard.this);
        recyclerView.setAdapter(mAdapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Application.getInstance().logASMEvent(GAConstants.AM_RDR_SUMMARY, CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));

    }
}
