package com.mfcwl.mfc_dealer.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mfcwl.mfc_dealer.ASMFragments.ASMLeadsFragment;
import com.mfcwl.mfc_dealer.ASMFragments.ASMReportsFragment;
import com.mfcwl.mfc_dealer.ASMFragments.ASMSalesFragment;
import com.mfcwl.mfc_dealer.ASMFragments.ASMStockFragment;
import com.mfcwl.mfc_dealer.ASMFragments.DasboardFragment;
import com.mfcwl.mfc_dealer.ASMFragments.MessageCenter;
import com.mfcwl.mfc_dealer.Activity.ASM_Reports.DealerDashboardActivity;
import com.mfcwl.mfc_dealer.Activity.ASM_Reports.MonthlyTargetActivity;
import com.mfcwl.mfc_dealer.Activity.ASM_Reports.SubmittedReports;
import com.mfcwl.mfc_dealer.Activity.ASM_Reports.WeeklyReport;
import com.mfcwl.mfc_dealer.Activity.LeadSection.LeadConstantsSection;
import com.mfcwl.mfc_dealer.Activity.Leads.FilterInstance;
import com.mfcwl.mfc_dealer.Activity.Leads.LeadConstants;
import com.mfcwl.mfc_dealer.Activity.Leads.LeadsInstance;
import com.mfcwl.mfc_dealer.Activity.Leads.NotificationInstance;
import com.mfcwl.mfc_dealer.Activity.RDR.RDRDashBoard;
import com.mfcwl.mfc_dealer.Activity.RDR.RDRWarningDialog;
import com.mfcwl.mfc_dealer.Activity.RDR.ViewDetailedListing;
import com.mfcwl.mfc_dealer.BuildConfig;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Fragment.BookedStockFrag;
import com.mfcwl.mfc_dealer.Fragment.HomeFragment;
import com.mfcwl.mfc_dealer.Fragment.LeadSection.LeadsFragmentSection;
import com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragmentNew;
import com.mfcwl.mfc_dealer.Fragment.StockFragment;
import com.mfcwl.mfc_dealer.Fragment.StockStoreFrag;
import com.mfcwl.mfc_dealer.Fragment.ToolFragment;
import com.mfcwl.mfc_dealer.Fragment.ToolIbbPriceFrag;
import com.mfcwl.mfc_dealer.IbbMarketTradeModel.ResponseAccessTokenIbbTrade;
import com.mfcwl.mfc_dealer.IbbMarketTradeService.MarketTradeService;
import com.mfcwl.mfc_dealer.InstanceCreate.LeadSection.LeadFilterSaveInstance;
import com.mfcwl.mfc_dealer.InstanceCreate.LeadSection.SortInstanceLeadSection;
import com.mfcwl.mfc_dealer.InstanceCreate.Stocklisting;
import com.mfcwl.mfc_dealer.Interface.LeadSection.FilterApply;
import com.mfcwl.mfc_dealer.Interface.LeadSection.LazyloaderPrivateLeads;
import com.mfcwl.mfc_dealer.Interface.LeadSection.LazyloaderWebLeads;
import com.mfcwl.mfc_dealer.Interface.LeadSection.SearchbyPrivateLeads;
import com.mfcwl.mfc_dealer.Interface.LeadSection.SearchbyWebLeads;
import com.mfcwl.mfc_dealer.Interface.LeadSection.SortedbyLead;
import com.mfcwl.mfc_dealer.LoginServices.LoginService;
import com.mfcwl.mfc_dealer.Model.AppVersionResponse;
import com.mfcwl.mfc_dealer.Model.DashboardModels;
import com.mfcwl.mfc_dealer.Model.Leadstatus;
import com.mfcwl.mfc_dealer.Model.LeadstatusResponse;
import com.mfcwl.mfc_dealer.Procurement.Activity.AddProcLead;
import com.mfcwl.mfc_dealer.Procurement.Fragment.ProcurementTAB;
import com.mfcwl.mfc_dealer.Procurement.ProcInterface.PFilterApply;
import com.mfcwl.mfc_dealer.Procurement.ProcInterface.PLazyloaderPrivateLeads;
import com.mfcwl.mfc_dealer.Procurement.ProcInterface.PLazyloaderWebLeads;
import com.mfcwl.mfc_dealer.Procurement.ProcInterface.PSearchPL;
import com.mfcwl.mfc_dealer.Procurement.ProcInterface.PSearchWL;
import com.mfcwl.mfc_dealer.Procurement.ProcInterface.PSortedL;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.RequestModel.LogOutRequest;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.ResponseModel.LogOutResponse;
import com.mfcwl.mfc_dealer.RetrofitConfigLeadSection.LeadStatusServices;
import com.mfcwl.mfc_dealer.SalesStocks.Fragment.SalesStocksAll;
import com.mfcwl.mfc_dealer.SalesStocks.Fragment.WarrantyStocks;
import com.mfcwl.mfc_dealer.SalesStocks.SalesInterface.SSFilterApply;
import com.mfcwl.mfc_dealer.SalesStocks.SalesInterface.SSLazyloaderSalesStocks;
import com.mfcwl.mfc_dealer.SalesStocks.SalesInterface.SSLazyloaderWarranty;
import com.mfcwl.mfc_dealer.SalesStocks.SalesInterface.SSSearchSales;
import com.mfcwl.mfc_dealer.SalesStocks.SalesInterface.SSSearchWarranty;
import com.mfcwl.mfc_dealer.StateHead.StateHeadDashBoardFragment;
import com.mfcwl.mfc_dealer.Utility.AppConstants;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.CustomFonts;
import com.mfcwl.mfc_dealer.Utility.GAConstants;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.Global_Urls;
import com.mfcwl.mfc_dealer.Utility.MonthUtility;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.ZonalHead.ZonalHeadDashBoardFragment;
import com.mfcwl.mfc_dealer.videoAppSpecific.SqlAdapterForDML;
import com.mfcwl.mfc_dealer.videoAppSpecific.VideoUploadManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import imageeditor.base.BaseActivity;
import rdm.ASMLandingFragment;
import rdm.NotificationActivity;
import rdm.PowerBIFragment;
import rdm.RDMDealerListFragment;
import rdm.RDMScheduleFragment;
import retrofit2.Response;
import sidekicklpr.LPRConstants;
import sidekicklpr.LPRResponse;
import sidekicklpr.LPRWarningDialog;
import sidekicklpr.SelectDealerForLPR;
import sidekicklpr.StockActivity;
import sidekicklpr.StockReconciliationReport;
import utility.AutoFinConstants;
import utility.CommonStrings;
import v2.view.HostActivity;

import static com.mfcwl.mfc_dealer.Activity.StockFilterActivity.stock_photocountsFlag;
import static com.mfcwl.mfc_dealer.Fragment.BookedStockFrag.swipeRefreshLayout_book;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chcold;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chfollowlast7days;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chfollowtmrw;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chfollowtoday;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chfollowyester;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chhot;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chlost;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chopen;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chpost15days;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chpost7days;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chposttoday;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chpostyester;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chsold;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chwarm;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.followFromDate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.followToDate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.followTodYesLastFromDate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.followTodYesLastToDate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.followTodayDate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.followTomorowdate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.fromdate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.fromdatefoll;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.leadstatusJsonArray;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.postFromDate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.postToDate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.postTodYesLastFromDate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.postTodYesLastToDate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.todate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.todatefoll;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadsFragment.headerdataLeads;
import static com.mfcwl.mfc_dealer.Fragment.Leads.NormalLeadsFragment.oneTimeSearch;
import static com.mfcwl.mfc_dealer.Fragment.Leads.NormalLeadsFragment.swipeRefreshLayoutWebLeads;
import static com.mfcwl.mfc_dealer.Fragment.Leads.PrivateLeadsFragment.swipeRefreshLayoutPrivate;
import static com.mfcwl.mfc_dealer.Fragment.StockFragment.headerdata;
import static com.mfcwl.mfc_dealer.Fragment.StockStoreFrag.swipeRefreshLayout;
import static com.mfcwl.mfc_dealer.NotificatioService.MyFirebaseMessagingService.AgeingInventory;
import static com.mfcwl.mfc_dealer.NotificatioService.MyFirebaseMessagingService.lead;
import static com.mfcwl.mfc_dealer.NotificatioService.MyFirebaseMessagingService.leadFollowUp;
import static com.mfcwl.mfc_dealer.NotificatioService.MyFirebaseMessagingService.noStockImage;
import static com.mfcwl.mfc_dealer.Utility.LeadsUtils.ClearFollowDataFilter;
import static com.mfcwl.mfc_dealer.Utility.LeadsUtils.ClearLeadStatusFilter;
import static com.mfcwl.mfc_dealer.Utility.LeadsUtils.ClearPostDataFilter;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, BottomNavigationView.OnNavigationItemSelectedListener,
        SortedbyLead, LazyloaderWebLeads,
        LazyloaderPrivateLeads, FilterApply, PFilterApply, PLazyloaderPrivateLeads,
        GlobalText,
        PLazyloaderWebLeads, PSortedL, SSFilterApply, SSLazyloaderSalesStocks, SSLazyloaderWarranty {

    public static Button homeTab;
    public static Button stockTab;
    public static Button addStockTab;
    public static Button leadsTab;
    public static Button toolTab;
    public static TextView hometv;
    public static TextView stocktv;
    public static TextView leadtv;
    //@BindView(R.id.tooltv)
    public static TextView tooltv;
    // @BindView(R.id.searchicon)
    public static RelativeLayout searchicon;
    public static ImageView searchImage;

    public static TextView tv_header_title;
    //@BindView(R.id.homeTabList)
    public RelativeLayout homeTabList;
    //@BindView(R.id.stockTabList)
    public RelativeLayout stockTabList;
    //@BindView(R.id.leadsTabList)
    public RelativeLayout leadsTabList;
    //@BindView(R.id.toolTabList)
    public RelativeLayout toolTabList;

    public static LinearLayout badgeid;

    public static LinearLayout asm_btm_nav_bar;
    public static LinearLayout asm_dashboard_tab, asm_create_wr_tab, asm_dealers_tab, asm_schedule_tab;
    public static Button btnASMDashboard, btnASMCreateWR, btnASMDealer, btnASMSchedule;
    public static Boolean searchFlag = false;

    // @BindView(R.id.search_view)
    public static SearchView searchView;
    public static ActionBarDrawerToggle toggle;
    //public HomeFragment homeFragment = new HomeFragment();
    public Fragment fragment;


    public static Toolbar toolbar;

    public static Activity activity;

    public static boolean postdatelessthirty = false;

    public static String searchVal = "";

    public static boolean entersearch = false;
    public static LinearLayout layout_back_btn, linearLayoutVisible;
    public static ImageView iv_toggle_back_btn;
    public static CardView cardView;

    public static Dialog dialog;


    private SharedPreferences mSharedPreferences = null;
    private SharedPreferences mSharedPreferencesLead = null;

    private FragmentSortListener fragmentSortListener;
    private FragmentPrivateSortListener fragmentPrivateSortListener;
    private LazyloaderWebLeads lazyloaderWebLeads;
    private LazyloaderPrivateLeads lazyloaderPrivateLeads;


    private FilterApply applyFilterWebLeads, applyFilterPriLeads;

    public static FragmentRefreshListener fragmentRefreshListener;

    public static ProcFragmentRefreshListener ProcRefreshListener;
    public static SSFragmentRefreshListener SSRefreshListener;

    private PFragmentSortListener PfragmentSortListener;
    private FragmentPPrivateSortListener PPrivateSortListener;
    private PLazyloaderWebLeads PlazyloaderWebLeads;

    //sales
    private SSLazyloaderSalesStocks SSLazyloaderSalesStocks;

    private SSLazyloaderWarranty SSLazyloaderWarranty;

    private PLazyloaderPrivateLeads PlazyloaderPrivateLeads;
    private PFilterApply applyPFilterWebLeads, applyPFilterPriLeads;

    //sales and warranty
    private SSFilterApply applySSFilterApply, applyWaranthyFilterApply;

    public static List<Leadstatus> leadstatus;

    public static List<String> warrantyStatus;

    public static List<String> statusList = new ArrayList<String>();

    public static BottomNavigationView navigation;
    private DashboardModels dashboardSwtich;

    public Observer<String> nameObserver1;

    TextView tvASMDashboard, tvASMCreateWR, tvASMDealer, tvASMSchedule;
    NavigationView navigationView;

    public String TAG = getClass().getSimpleName();

    Thread thread = new Thread();
    private FirebaseAnalytics firebaseAnalytics;

    public static boolean isLPRstartedfromMenu;
    private SqlAdapterForDML mSqlAdapterForDML = SqlAdapterForDML.getInstance();
    public static sidekicklpr.PreferenceManager preferenceManager;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i(TAG, "onCreate: ");

       /* writeAFile();
        ReadBtn();*/
        isLPRstartedfromMenu = false;
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        preferenceManager = new sidekicklpr.PreferenceManager(this);
        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {


      /*      PeriodicWorkRequest workRequest = new PeriodicWorkRequest.Builder(
                    UploadWorker.class,
                    1,
                    TimeUnit.SECONDS,
                    PeriodicWorkRequest.MIN_PERIODIC_FLEX_MILLIS,
                    TimeUnit.MILLISECONDS)
                    .setInitialDelay(1, TimeUnit.MINUTES)
                    .addTag("send_reminder_periodic")
                    .build();*/

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    new VideoUploadManager().uploadAllPendingMedia();
                }
            }, 60 * 1000);
        }

        warrantyStatus = new ArrayList<String>();

        String[] status = {"SurakshaFirst", "WarrantyFirst", "AssistFirst", "CertiFirst+", "CertiFirst", "WarrantyFirst-2W",
                "CertiFirst-2W", "AssistFirst-C2C", "Others"};


        warrantyStatus = Arrays.asList(status);


        LoginActivity.isLoading = false;
        ButterKnife.bind(this);
        activity = this;
        mSharedPreferences = activity.getSharedPreferences("MFC", Context.MODE_PRIVATE);
        mSharedPreferencesLead = activity.getSharedPreferences("MFCB", Context.MODE_PRIVATE);
        mSharedPreferencesLead = activity.getSharedPreferences("MFCP", Context.MODE_PRIVATE);
        dashboardSwtich = ViewModelProviders.of(this).get(DashboardModels.class);

        CommonMethods.setvalueAgainstKey(this, "videopath", "");

        searchView = (SearchView) findViewById(R.id.search_view);
        CommonMethods.MemoryClears();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        String uType = CommonMethods.getstringvaluefromkey(this, "user_type");
        if (uType.equalsIgnoreCase("dealer") || uType.equalsIgnoreCase("AM")) {
            //showDialogOnceDay();
        }


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        homeTabList = (RelativeLayout) findViewById(R.id.homeTabList);
        stockTabList = (RelativeLayout) findViewById(R.id.stockTabList);
        leadsTabList = (RelativeLayout) findViewById(R.id.leadsTabList);
        toolTabList = (RelativeLayout) findViewById(R.id.toolTabList);

        homeTab = (Button) findViewById(R.id.homeTab);
        stockTab = (Button) findViewById(R.id.stockTab);
        addStockTab = (Button) findViewById(R.id.addStockTab);
        addStockTab = (Button) findViewById(R.id.addStockTab);
        leadsTab = (Button) findViewById(R.id.leadsTab);
        toolTab = (Button) findViewById(R.id.toolTab);

        hometv = (TextView) findViewById(R.id.hometv);
        stocktv = (TextView) findViewById(R.id.stocktv);
        leadtv = (TextView) findViewById(R.id.leadtv);
        tooltv = (TextView) findViewById(R.id.tooltv);

        asm_btm_nav_bar = findViewById(R.id.asm_btm_nav_bar);
        asm_dashboard_tab = findViewById(R.id.asm_dashboard_tab);
        asm_create_wr_tab = findViewById(R.id.asm_create_wr_tab);
        asm_dealers_tab = findViewById(R.id.asm_dealers_tab);
        asm_schedule_tab = findViewById(R.id.asm_schedule_tab);

        btnASMDashboard = findViewById(R.id.btnASMDashboard);
        btnASMCreateWR = findViewById(R.id.btnASMCreateWR);
        btnASMDealer = findViewById(R.id.btnASMDealer);
        btnASMSchedule = findViewById(R.id.btnASMSchedule);

        tvASMDashboard = findViewById(R.id.tvASMDashboard);
        tvASMCreateWR = findViewById(R.id.tvASMCreateWR);
        tvASMDealer = findViewById(R.id.tvASMDealer);
        tvASMSchedule = findViewById(R.id.tvASMSchedule);

        searchicon = (RelativeLayout) findViewById(R.id.searchicon);
        searchImage = findViewById(R.id.searchimage);
        searchicon.setVisibility(View.VISIBLE);
        badgeid = (LinearLayout) findViewById(R.id.badgeid);

        navigation = findViewById(R.id.navigation);

        String mUserType = CommonMethods.getstringvaluefromkey(activity, "user_type");

        if (mUserType.equalsIgnoreCase(AppConstants.LOGINTYPE_DEALER) || mUserType.equalsIgnoreCase(AppConstants.LOGINTYPE_DEALERCRE) || mUserType.equalsIgnoreCase(AppConstants.LOGINTYPE_PROCUREMENT) || mUserType.equalsIgnoreCase(AppConstants.LOGINTYPE_SLAES)) {

            badgeid.setVisibility(View.VISIBLE);
            navigation.setVisibility(View.GONE);

        } else {
            badgeid.setVisibility(View.GONE);
            navigation.setOnNavigationItemSelectedListener(this);
            navigation.setVisibility(View.GONE);

        }

        fragments_Swtich_page();

        tv_header_title = toolbar.findViewById(R.id.tv_header_title);

        tv_header_title.setVisibility(View.VISIBLE);
        tv_header_title.setText("Dashboard");
        //search code
        layout_back_btn = findViewById(R.id.layout_back_btn);
        iv_toggle_back_btn = findViewById(R.id.toggle_back_btn);
        linearLayoutVisible = (LinearLayout) findViewById(R.id.layout_visible);
        cardView = (CardView) findViewById(R.id.layout_invisible);
        layout_back_btn.setVisibility(View.GONE);
        iv_toggle_back_btn.setVisibility(View.GONE);
        CommonMethods.setvalueAgainstKey(MainActivity.this, "isLeadDetails", "false");
        CommonMethods.setvalueAgainstKey(MainActivity.this, "isProcLeadDetails", "false");

        CommonMethods.setvalueAgainstKey(MainActivity.this, "issalesDetails", "false");


        //LeadStatus Calling
        GetLeadStatus();

        if (lead.equalsIgnoreCase("lead")) {
            CommonMethods.setvalueAgainstKey(MainActivity.this, "Notification_Switch", "lead");
        } else if (noStockImage.equalsIgnoreCase("noStockImage")) {
            //   Log.e("noStockImage", "noStockImage-coming-0");

            CommonMethods.setvalueAgainstKey(MainActivity.this, "Notification_Switch", "noStockImage");

        } else if (leadFollowUp.equalsIgnoreCase("leadFollowUp")) {
            CommonMethods.setvalueAgainstKey(MainActivity.this, "Notification_Switch", "leadFollowUp");
        } else if (AgeingInventory.equalsIgnoreCase("AgeingInventory")) {
            CommonMethods.setvalueAgainstKey(MainActivity.this, "Notification_Switch", "AgeingInventory");
        }


        /*Log.e("lead", "lead=1" + lead);
        Log.e("noStockImage", "noStockImage-1" + noStockImage);
        Log.e("leadFollowUp", "leadFollowUp-1" + leadFollowUp);
        Log.e("AgeingInventory", "AgeingInventory-1" + AgeingInventory);
*/

        linearLayoutVisible.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggle.setDrawerIndicatorEnabled(false);
                linearLayoutVisible.setVisibility(View.GONE);
                cardView.setVisibility(View.VISIBLE);
                searchView.setIconified(false);
                searchFlag = true;

               /* if (tv_header_title.getVisibility() == View.VISIBLE) {
                    tv_header_title.setVisibility(View.GONE);
                }*/

                swipeRefreshLayout();

                // searchView.setQueryHint("Search for make, model, variant, reg no..");
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        searchFlag = true;
                        swipeRefreshLayout();

                        if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("StockStore")) {
                            StockStoreFrag.onSerarchResultUpdate(query);
                        } else if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("BookedStock")) {
                            BookedStockFrag.onSerarchResultUpdate(query);
                        } else if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("WebLeads")) {
                            if (searchbyWebLeads != null) {
                                searchbyWebLeads.onSearchWebLead(query);
                            }
                            try {
                                if (headerdataLeads.getVisibility() == View.VISIBLE) {
                                    headerdataLeads.setVisibility(View.GONE);
                                }
                            } catch (Exception e) {

                            }

                        } else if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("PrivateLeads")) {

                            if (searchbyPrivateLeads != null) {
                                searchbyPrivateLeads.onSearchPrivateLead(query);
                            }

                            try {
                                if (headerdataLeads.getVisibility() == View.VISIBLE) {
                                    headerdataLeads.setVisibility(View.GONE);
                                }
                            } catch (Exception e) {

                            }

                        } else if (CommonMethods.getstringvaluefromkey(activity, "Pstatus").equalsIgnoreCase("WebLeads")) {
                            if (searchWL != null) {
                                searchWL.onPSearchWL(query);
                            }
                            try {
                                if (headerdataLeads.getVisibility() == View.VISIBLE) {
                                    headerdataLeads.setVisibility(View.GONE);
                                }
                            } catch (Exception e) {

                            }

                        } else if (CommonMethods.getstringvaluefromkey(activity, "Pstatus").equalsIgnoreCase("PrivateLeads")) {
                            if (searchPl != null) {
                                searchPl.onPSearchPrivateLead(query);
                            }
                            try {
                                if (headerdataLeads.getVisibility() == View.VISIBLE) {
                                    headerdataLeads.setVisibility(View.GONE);
                                }
                            } catch (Exception e) {

                            }

                        } else if (CommonMethods.getstringvaluefromkey(activity, "Pstatus").equalsIgnoreCase("salesstock")) {
                            SalesStocksAll.onSerarchResultUpdate(query);
                            try {
                                if (headerdataLeads.getVisibility() == View.VISIBLE) {
                                    headerdataLeads.setVisibility(View.GONE);
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "Exception: " + e.getMessage());
                            }

                        } else if (CommonMethods.getstringvaluefromkey(activity, "Pstatus").equalsIgnoreCase("warranty")) {
                            WarrantyStocks.onSerarchResultUpdate(query);
                            try {
                                if (headerdataLeads.getVisibility() == View.VISIBLE) {
                                    headerdataLeads.setVisibility(View.GONE);
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "Exception: " + e.getMessage());
                            }

                        } /*else if (CommonMethods.getstringvaluefromkey(activity, "Pstatus").equalsIgnoreCase("salesstock")) {
                            //sales search
                            if (searchSS != null) {
                                searchSS.onSearchSales(query);
                            }
                            try {
                                if (headerdataLeads.getVisibility() == View.VISIBLE) {
                                    headerdataLeads.setVisibility(View.GONE);
                                }
                            } catch (Exception e) {

                            }

                        } */ else if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("Tools")) {

                            ToolFragment.onSerarchResultUpdate(query);

                        } else if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("dashboard")) {
                            DasboardFragment.onSerarchResultUpdate(query);
                        } else if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("statedashboard")) {
                            StateHeadDashBoardFragment.onSerarchResultUpdate(query);
                        } else if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("zonaldashboard")) {
                            ZonalHeadDashBoardFragment.onSerarchResultUpdate(query);
                        }

                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String s) {

                        searchFlag = true;
                        swipeRefreshLayout();

                        if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("StockStore")) {
                            StockStoreFrag.onSerarchResultUpdate(s);
                        } else if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("BookedStock")) {
                            BookedStockFrag.onSerarchResultUpdate(s);
                        } else if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("WebLeads")) {
                           /* NormalLeadsFragment.onSerarchResultUpdate(s);
                            try {
                                headerdataLeads.setVisibility(View.GONE);
                            } catch (Exception e) {

                            }*/

                            if (searchbyWebLeads != null) {
                                searchbyWebLeads.onSearchWebLead(s);

                            }

                            try {
                                if (headerdataLeads.getVisibility() == View.VISIBLE) {
                                    headerdataLeads.setVisibility(View.GONE);
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "Exception: " + e.getMessage());
                            }
                        } else if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("PrivateLeads")) {
                            /*PrivateLeadsFragment.onSerarchResultUpdate(s);
                            try {
                                headerdataLeads.setVisibility(View.GONE);
                            } catch (Exception e) {

                            }*/
                            searchbyPrivateLeads.onSearchPrivateLead(s);
                            try {
                                if (headerdataLeads.getVisibility() == View.VISIBLE) {
                                    headerdataLeads.setVisibility(View.GONE);
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "Exception: " + e.getMessage());
                            }
                        } else if (CommonMethods.getstringvaluefromkey(activity, "Pstatus").equalsIgnoreCase("WebLeads")) {
                            searchWL.onPSearchWL(s);
                            try {
                                if (headerdataLeads.getVisibility() == View.VISIBLE) {
                                    headerdataLeads.setVisibility(View.GONE);
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "Exception: " + e.getMessage());
                            }

                        } else if (CommonMethods.getstringvaluefromkey(activity, "Pstatus").equalsIgnoreCase("PrivateLeads")) {
                            searchPl.onPSearchPrivateLead(s);
                            try {
                                if (headerdataLeads.getVisibility() == View.VISIBLE) {
                                    headerdataLeads.setVisibility(View.GONE);
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "Exception: " + e.getMessage());
                            }

                        } else if (CommonMethods.getstringvaluefromkey(activity, "Pstatus").equalsIgnoreCase("salesstock")) {


                            SalesStocksAll.onSerarchResultUpdate(s);
                            try {
                                if (headerdataLeads.getVisibility() == View.VISIBLE) {
                                    headerdataLeads.setVisibility(View.GONE);
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "Exception: " + e.getMessage());
                            }

                        } else if (CommonMethods.getstringvaluefromkey(activity, "Pstatus").equalsIgnoreCase("warranty")) {


                            WarrantyStocks.onSerarchResultUpdate(s);
                            try {
                                if (headerdataLeads.getVisibility() == View.VISIBLE) {
                                    headerdataLeads.setVisibility(View.GONE);
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "Exception: " + e.getMessage());
                            }

                        } else if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("Tools")) {

                            ToolFragment.onSerarchResultUpdate(s);
                        } else if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("dashboard")) {

                            DasboardFragment.onSerarchResultUpdate(s);

                        } else if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("statedashboard")) {
                            StateHeadDashBoardFragment.onSerarchResultUpdate(s);
                        } else if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("zonaldashboard")) {
                            ZonalHeadDashBoardFragment.onSerarchResultUpdate(s);
                        }

                        return false;
                    }
                });
            }
        });

        if (mUserType.equalsIgnoreCase(USERTYPE_ASM)) {
            btnASMDashboard.performClick();
        }
        if (asm_dashboard_tab.getVisibility() == View.VISIBLE) {

            btnASMDashboard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    asm_dashboard_tab.setBackgroundColor(getResources().getColor(R.color.dull_yellow));
                    asm_create_wr_tab.setBackgroundColor(getResources().getColor(R.color.rdm_bnv_bg_yellow));
                    asm_dealers_tab.setBackgroundColor(getResources().getColor(R.color.rdm_bnv_bg_yellow));
                    asm_schedule_tab.setBackgroundColor(getResources().getColor(R.color.rdm_bnv_bg_yellow));

                    loadFragment(new ASMLandingFragment(MainActivity.this));
                }

            });
            if (uType.equalsIgnoreCase(USERTYPE_ASM)) {
                btnASMDashboard.performClick();
            }


        }


        if (asm_dashboard_tab.getVisibility() == View.VISIBLE) {
            btnASMDealer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    asm_dashboard_tab.setBackgroundColor(getResources().getColor(R.color.rdm_bnv_bg_yellow));
                    asm_create_wr_tab.setBackgroundColor(getResources().getColor(R.color.rdm_bnv_bg_yellow));
                    asm_dealers_tab.setBackgroundColor(getResources().getColor(R.color.dull_yellow));
                    asm_schedule_tab.setBackgroundColor(getResources().getColor(R.color.rdm_bnv_bg_yellow));

                    loadFragment(new RDMDealerListFragment(MainActivity.this));
                }

            });
        }
        if (asm_dashboard_tab.getVisibility() == View.VISIBLE) {
            btnASMSchedule.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    asm_dashboard_tab.setBackgroundColor(getResources().getColor(R.color.rdm_bnv_bg_yellow));
                    asm_create_wr_tab.setBackgroundColor(getResources().getColor(R.color.rdm_bnv_bg_yellow));
                    asm_dealers_tab.setBackgroundColor(getResources().getColor(R.color.rdm_bnv_bg_yellow));
                    asm_schedule_tab.setBackgroundColor(getResources().getColor(R.color.dull_yellow));

                    loadFragment(new RDMScheduleFragment(MainActivity.this));
                }

            });
        }


        if (asm_create_wr_tab.getVisibility() == View.VISIBLE) {
            btnASMCreateWR.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    asm_dashboard_tab.setBackgroundColor(getResources().getColor(R.color.rdm_bnv_bg_yellow));
                    asm_create_wr_tab.setBackgroundColor(getResources().getColor(R.color.dull_yellow));
                    asm_dealers_tab.setBackgroundColor(getResources().getColor(R.color.rdm_bnv_bg_yellow));
                    asm_schedule_tab.setBackgroundColor(getResources().getColor(R.color.rdm_bnv_bg_yellow));
                    startActivity(new Intent(MainActivity.this, WeeklyReport.class));
                }

            });
        }

        searchView.setOnSearchClickListener(view -> entersearch = true);


        if (iv_toggle_back_btn.getVisibility() == View.VISIBLE) {
            iv_toggle_back_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (tv_header_title != null) {
                        tv_header_title.setVisibility(View.VISIBLE);
                        tv_header_title.setTypeface(CustomFonts.getRobotoRegularTF(activity));
                        tv_header_title.setText("DEALER LIST");
                    }

                    fragmentLoad(new PowerBIFragment(MainActivity.this));
                }
            });
        }
        searchView.setOnCloseListener(() -> {

            //  tv_header_title.setVisibility(View.VISIBLE);
            searchClose();
            return false;
        });


        //end of search code
/*
        test = (ImageView)findViewById(R.id.toolbar_menu);
        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"te",Toast.LENGTH_LONG).show();
            }
        });*/
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);

        View headerView = navigationView.getHeaderView(0);
        TextView profilename1 = headerView.findViewById(R.id.profilename1);
        TextView profilename2 = headerView.findViewById(R.id.profilename2);
        String userType = CommonMethods.getstringvaluefromkey(activity, "user_type");
        if (userType.equalsIgnoreCase("dealer")) {
            navigationView.getMenu().findItem(R.id.nav_camera).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_feedback).setVisible(true);
            if (CommonMethods.getstringvaluefromkey(this, "IsAutoFin").equalsIgnoreCase("true")) {
                navigationView.getMenu().findItem(R.id.nav_autofin).setVisible(false);
            } else {
                navigationView.getMenu().findItem(R.id.nav_autofin).setVisible(false);
            }
            navigationView.getMenu().findItem(R.id.nav_viewreport).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_payment).setVisible(true);
            navigationView.getMenu().findItem(R.id.sub_wkreport).setVisible(false);
            navigationView.getMenu().findItem(R.id.capture_stock).setVisible(false);
            navigationView.getMenu().findItem(R.id.rdr_menu_submitted_wr).setVisible(false);
            navigationView.getMenu().findItem(R.id.rdr_menu_rdr_dashboard).setVisible(false);
            navigationView.getMenu().findItem(R.id.rdr_menu_logout).setVisible(false);

            navigationView.getMenu().findItem(R.id.sub_dealer_visit).setVisible(false);
            navigationView.getMenu().findItem(R.id.dealer_actionitems).setVisible(false);
            navigationView.getMenu().findItem(R.id.monthly_target).setVisible(false);
            navigationView.getMenu().findItem(R.id.dealer_dashboard).setVisible(false);
            navigationView.getMenu().findItem(R.id.dashboard).setVisible(false);

        } else if (userType.equalsIgnoreCase(USERTYPE_STATEHEAD) ||
                userType.equalsIgnoreCase(USERTYPE_ZONALHEAD)) {
            navigationView.getMenu().findItem(R.id.nav_camera).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_feedback).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_viewreport).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_payment).setVisible(false);
            navigationView.getMenu().findItem(R.id.sub_wkreport).setVisible(false);
            navigationView.getMenu().findItem(R.id.capture_stock).setVisible(false);
            navigationView.getMenu().findItem(R.id.sub_dealer_visit).setVisible(false);
            navigationView.getMenu().findItem(R.id.dealer_actionitems).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_autofin).setVisible(false);
            navigationView.getMenu().findItem(R.id.dealer_dashboard).setVisible(true);
            navigationView.getMenu().findItem(R.id.dashboard).setVisible(false);

            navigationView.getMenu().findItem(R.id.rdr_menu_submitted_wr).setVisible(false);
            navigationView.getMenu().findItem(R.id.rdr_menu_rdr_dashboard).setVisible(false);
            navigationView.getMenu().findItem(R.id.rdr_menu_logout).setVisible(false);

        } else if (userType.equalsIgnoreCase(USERTYPE_ASM)) {

            navigationView.getMenu().findItem(R.id.rdr_menu_submitted_wr).setVisible(true);
            navigationView.getMenu().findItem(R.id.rdr_menu_rdr_dashboard).setVisible(true);
            navigationView.getMenu().findItem(R.id.rdr_menu_logout).setVisible(true);

            navigationView.getMenu().findItem(R.id.nav_autofin).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_camera).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_feedback).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_viewreport).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_payment).setVisible(false);
            navigationView.getMenu().findItem(R.id.sub_wkreport).setVisible(false);
            navigationView.getMenu().findItem(R.id.capture_stock).setVisible(false);
            navigationView.getMenu().findItem(R.id.sub_dealer_visit).setVisible(false);
            navigationView.getMenu().findItem(R.id.dealer_actionitems).setVisible(false);
            navigationView.getMenu().findItem(R.id.dealer_dashboard).setVisible(false);
            navigationView.getMenu().findItem(R.id.dashboard).setVisible(false);
            navigationView.getMenu().findItem(R.id.monthly_target).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_gallery).setVisible(false);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
            toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        drawer.openDrawer(GravityCompat.START);
                    }
                }
            });
            toggle.setDrawerIndicatorEnabled(false);
            toggle.setHomeAsUpIndicator(R.drawable.ic_new_hamburger_menu);
            hideASMMailOption(R.id.asm_mail);
            toolbar.setBackgroundColor(getResources().getColor(R.color.all_dealers_fragment_bg));
            asm_btm_nav_bar.setVisibility(View.VISIBLE);
            asm_dashboard_tab.setVisibility(View.VISIBLE);
            asm_create_wr_tab.setVisibility(View.VISIBLE);
            asm_dealers_tab.setVisibility(View.VISIBLE);
            asm_schedule_tab.setVisibility(View.VISIBLE);
            tvASMDashboard.setTypeface(CustomFonts.getRobotoRegularTF(this));
            tvASMCreateWR.setTypeface(CustomFonts.getRobotoRegularTF(this));
            tvASMDealer.setTypeface(CustomFonts.getRobotoRegularTF(this));
            tvASMSchedule.setTypeface(CustomFonts.getRobotoRegularTF(this));
            navigation.setVisibility(View.GONE);
            tv_header_title.setText("DASHBOARD");
            // navigation.setBackgroundColor(getResources().getColor(R.color.rdm_bnv_bg_yellow));
            //navigation.setItemBackground(getResources().getDrawable(R.drawable.asm_bottom_navigation_view_bg));
            /*navigation.setItemIconTintList(ColorStateList.valueOf(getResources().getColor(R.color.black)));
            navigation.setItemTextColor(ColorStateList.valueOf(getResources().getColor(R.color.black)));
            navigation.setItemTextAppearanceActive(R.style.menu_item_custom_font);
            */
            tv_header_title.setTextColor(getResources().getColor(R.color.black));
            tv_header_title.setTypeface(CustomFonts.getRobotoRegularTF(this));

            changeStatusBarColor();
            setOverflowButtonColor(toolbar, getResources().getColor(R.color.black));

        } else {
            navigationView.getMenu().findItem(R.id.nav_camera).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_feedback).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_autofin).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_viewreport).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_payment).setVisible(false);
            navigationView.getMenu().findItem(R.id.dealer_actionitems).setVisible(false);
            navigationView.getMenu().findItem(R.id.monthly_target).setVisible(false);
            navigationView.getMenu().findItem(R.id.rdr_menu_submitted_wr).setVisible(false);
            navigationView.getMenu().findItem(R.id.rdr_menu_rdr_dashboard).setVisible(false);
            navigationView.getMenu().findItem(R.id.rdr_menu_logout).setVisible(false);

            navigationView.getMenu().findItem(R.id.dealer_dashboard).setVisible(true);
            navigationView.getMenu().findItem(R.id.dashboard).setVisible(true);
        }

        TextView logout = navigationView.findViewById(R.id.logout);
        logout.setText("Version" + "  " + BuildConfig.VERSION_NAME);

        String s1 = CommonMethods.getstringvaluefromkey(this, "dealer_display_name");
        String s2 = CommonMethods.getstringvaluefromkey(this, "user_name");

        try {

            if (!s1.equalsIgnoreCase(""))
                s1 = s1.substring(0, 1).toUpperCase() + s1.substring(1).toLowerCase();

            if (!s2.equalsIgnoreCase(""))
                s2 = s2.substring(0, 1).toUpperCase() + s2.substring(1).toLowerCase();

        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }

        // profilename1.setText(s1);
        profilename2.setText(s2);

        CircleImageView car_image = headerView.findViewById(R.id.car_image);

    /*    if (!CommonMethods.getstringvaluefromkey(this, "dealer_store_image").equalsIgnoreCase("")) {
            Picasso.Builder builder = new Picasso.Builder(this);
            builder.build()
                    .load(CommonMethods.getstringvaluefromkey(this, "dealer_store_image"))
                    .placeholder(R.drawable.dealerimg)
                    .error(R.drawable.dealerimg)
                    .into(car_image);
        }*/

        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);

        homeTab.setBackgroundResource(R.drawable.hometab_click);
        stockTab.setBackgroundResource(R.drawable.stocktab_hover);
        leadsTab.setBackgroundResource(R.drawable.leadtab_hover);
        toolTab.setBackgroundResource(R.drawable.tooltab_hover);

        hometv.setTextColor(this.getResources().getColor(R.color.yellow));
        stocktv.setTextColor(this.getResources().getColor(R.color.lgray));
        leadtv.setTextColor(this.getResources().getColor(R.color.lgray));
        tooltv.setTextColor(this.getResources().getColor(R.color.lgray));

        //   Log.e("lead", "lead===" + CommonMethods.getstringvaluefromkey(this, "Notification_Switch"));

        if (CommonMethods.getstringvaluefromkey(this, "stockcreated").equalsIgnoreCase("true")) {
            CommonMethods.setvalueAgainstKey(MainActivity.this, "Notification_Switch", "Reset");
            CommonMethods.setvalueAgainstKey(MainActivity.this, "status", "StockStore");
        }

        if (CommonMethods.getstringvaluefromkey(this, "Notification_Switch").equalsIgnoreCase("AgeingInventory")
                && CommonMethods.getstringvaluefromkey(this, "user_type").equalsIgnoreCase("dealer")) {
            thirtydays_old_notification();
        } else if (CommonMethods.getstringvaluefromkey(this, "Notification_Switch").equalsIgnoreCase("noStockImage")
                && CommonMethods.getstringvaluefromkey(this, "user_type").equalsIgnoreCase("dealer")) {
            noimage_notification();
        } else if (CommonMethods.getstringvaluefromkey(this, "Notification_Switch").equalsIgnoreCase("lead")
                && CommonMethods.getstringvaluefromkey(this, "user_type").equalsIgnoreCase("dealer")) {
            newleads_notification();
        } else if (CommonMethods.getstringvaluefromkey(this, "Notification_Switch").equalsIgnoreCase("leadFollowUp")
                && CommonMethods.getstringvaluefromkey(this, "user_type").equalsIgnoreCase("dealer")) {
            followup_notification();
        } else if (CommonMethods.getstringvaluefromkey(this, "stockcreated").equalsIgnoreCase("true")
                && CommonMethods.getstringvaluefromkey(this, "user_type").equalsIgnoreCase("dealer")) {

            test(getResources().getString(R.string.stock_hint));

            searchicon.setVisibility(View.VISIBLE);
            if (tv_header_title != null)
                if (tv_header_title.getVisibility() == View.VISIBLE) {
                    tv_header_title.setVisibility(View.GONE);
                }

            homeTab.setBackgroundResource(R.drawable.hometab_hover);
            stockTab.setBackgroundResource(R.drawable.stocktab_click);
            leadsTab.setBackgroundResource(R.drawable.leadtab_hover);
            toolTab.setBackgroundResource(R.drawable.tooltab_hover);

            hometv.setTextColor(this.getResources().getColor(R.color.lgray));
            stocktv.setTextColor(this.getResources().getColor(R.color.yellow));
            leadtv.setTextColor(this.getResources().getColor(R.color.lgray));
            tooltv.setTextColor(this.getResources().getColor(R.color.lgray));
            CommonMethods.setvalueAgainstKey(this, "stockcreated", "false");
            if (!CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("AM")) {
                StockFragment stockFragment = new StockFragment();
                this.fragmentLoad(stockFragment);
            }


        } else if (CommonMethods.getstringvaluefromkey(this, "filterapply").equalsIgnoreCase("true")
                || CommonMethods.getstringvaluefromkey(this, "booknowbtn").equalsIgnoreCase("true")
                || CommonMethods.getstringvaluefromkey(this, "unbooknowbtn").equalsIgnoreCase("true")) {

            searchicon.setVisibility(View.VISIBLE);

            if (tv_header_title != null)
                if (tv_header_title.getVisibility() == View.VISIBLE) {
                    tv_header_title.setVisibility(View.GONE);
                }

            test(getResources().getString(R.string.stock_hint));
            homeTab.setBackgroundResource(R.drawable.hometab_hover);
            stockTab.setBackgroundResource(R.drawable.stocktab_click);
            leadsTab.setBackgroundResource(R.drawable.leadtab_hover);
            toolTab.setBackgroundResource(R.drawable.tooltab_hover);

            hometv.setTextColor(this.getResources().getColor(R.color.lgray));
            stocktv.setTextColor(this.getResources().getColor(R.color.yellow));
            leadtv.setTextColor(this.getResources().getColor(R.color.lgray));
            tooltv.setTextColor(this.getResources().getColor(R.color.lgray));

            //CommonMethods.setvalueAgainstKey(activity,"filterapply","false");

            if (!CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("AM")) {
                StockFragment stockFragment = new StockFragment();
                this.fragmentLoad(stockFragment);
            }

        } else if (Stocklisting.getInstance().getStockstatus().equals("StockStoreFrag")
                || Stocklisting.getInstance().getStockstatus().equals("BookedStockFrag")) {

            test(getResources().getString(R.string.stock_hint));
            homeTab.setBackgroundResource(R.drawable.hometab_hover);
            stockTab.setBackgroundResource(R.drawable.stocktab_click);
            leadsTab.setBackgroundResource(R.drawable.leadtab_hover);
            toolTab.setBackgroundResource(R.drawable.tooltab_hover);

            hometv.setTextColor(this.getResources().getColor(R.color.lgray));
            stocktv.setTextColor(this.getResources().getColor(R.color.yellow));
            leadtv.setTextColor(this.getResources().getColor(R.color.lgray));
            tooltv.setTextColor(this.getResources().getColor(R.color.lgray));

            if (tv_header_title != null)
                if (tv_header_title.getVisibility() == View.VISIBLE) {
                    tv_header_title.setVisibility(View.GONE);
                }

            if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
                MenuItem item = menu.findItem(R.id.asm_mail);
                item.setVisible(true);
                item = menu.findItem(R.id.notification_bell);
                item.setVisible(false);
                StockFragment stockFragment = new StockFragment();
                this.fragmentLoad(stockFragment);
            } else {

                searchicon.setVisibility(View.VISIBLE);
                test("Search..");

                if (tv_header_title != null) {
                    tv_header_title.setVisibility(View.VISIBLE);
                    tv_header_title.setTypeface(CustomFonts.getRobotoRegularTF(this));
                    tv_header_title.setTextColor(getResources().getColor(R.color.black));
                    tv_header_title.setText("Dashboard");
                }
                //  DasboardFragment dashboard = new DasboardFragment();
                //  ASMLandingFragment dashboard = new ASMLandingFragment(this);
                //this.fragmentLoad(dashboard);
            }

        } else {

            searchicon.setVisibility(View.INVISIBLE);

            homeTab.setBackgroundResource(R.drawable.hometab_click);
            stockTab.setBackgroundResource(R.drawable.stocktab_hover);
            leadsTab.setBackgroundResource(R.drawable.leadtab_hover);
            toolTab.setBackgroundResource(R.drawable.tooltab_hover);

            hometv.setTextColor(this.getResources().getColor(R.color.yellow));
            stocktv.setTextColor(this.getResources().getColor(R.color.lgray));
            leadtv.setTextColor(this.getResources().getColor(R.color.lgray));
            tooltv.setTextColor(this.getResources().getColor(R.color.lgray));


            if (mUserType.equalsIgnoreCase(AppConstants.LOGINTYPE_DEALER) || mUserType.equalsIgnoreCase(AppConstants.LOGINTYPE_DEALERCRE) || mUserType.equalsIgnoreCase(AppConstants.LOGINTYPE_PROCUREMENT) || mUserType.equalsIgnoreCase(AppConstants.LOGINTYPE_SLAES)) {

                HomeFragment homeFragment = new HomeFragment();
                this.fragmentLoad(homeFragment);

            } else if (mUserType.equalsIgnoreCase(USERTYPE_STATEHEAD)) {
                searchicon.setVisibility(View.VISIBLE);
                setMainHeading(stateHeadDashboard);
                fragmentLoad(new StateHeadDashBoardFragment());
            } else if (mUserType.equalsIgnoreCase(USERTYPE_ZONALHEAD)) {
                searchicon.setVisibility(View.VISIBLE);
                setMainHeading(zonalHeadDashboard);
                fragmentLoad(new ZonalHeadDashBoardFragment());
            } else if (mUserType.equalsIgnoreCase(USERTYPE_ASM)) {


                searchicon.setVisibility(View.GONE);

                test("Search..");
                if (tv_header_title != null) {
                    tv_header_title.setVisibility(View.VISIBLE);
                    tv_header_title.setTypeface(CustomFonts.getRobotoRegularTF(this));
                    tv_header_title.setText("Dashboard");
                }


                // DasboardFragment dashboard = new DasboardFragment();
                // ASMLandingFragment dashboard = new ASMLandingFragment(this);
                // this.fragmentLoad(dashboard);
                //  }

            }

        }

        //Need to Disable always
        navigationView.getMenu().findItem(R.id.nav_sales).setVisible(false);

        if (userType.equalsIgnoreCase(USER_TYPE_DEALER_CRE) ||
                userType.equalsIgnoreCase(USER_TYPE_PROCUREMENT)) {
            navigationView.getMenu().findItem(R.id.sub_wkreport).setVisible(false);
            navigationView.getMenu().findItem(R.id.capture_stock).setVisible(false);
            navigationView.getMenu().findItem(R.id.sub_dealer_visit).setVisible(false);
            navigationView.getMenu().findItem(R.id.dealer_actionitems).setVisible(false);
            navigationView.getMenu().findItem(R.id.dealer_dashboard).setVisible(false);
            navigationView.getMenu().findItem(R.id.dashboard).setVisible(false);
        } else if (userType.equalsIgnoreCase(USER_TYPE_SALES)) {
            navigationView.getMenu().findItem(R.id.sub_wkreport).setVisible(false);
            navigationView.getMenu().findItem(R.id.capture_stock).setVisible(false);
            navigationView.getMenu().findItem(R.id.sub_dealer_visit).setVisible(false);
            navigationView.getMenu().findItem(R.id.dealer_actionitems).setVisible(false);
            navigationView.getMenu().findItem(R.id.dealer_dashboard).setVisible(false);
            navigationView.getMenu().findItem(R.id.dashboard).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_sales).setVisible(true);
        }


    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.all_dealers_fragment_bg));
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        }
    }


    public static void stockreset() {

        if (searchicon != null) {
            searchicon.setVisibility(View.VISIBLE);
        }

        if (tv_header_title != null)
            if (tv_header_title.getVisibility() == View.VISIBLE) {
                tv_header_title.setVisibility(View.GONE);
            }
        homeTab.setBackgroundResource(R.drawable.hometab_hover);
        stockTab.setBackgroundResource(R.drawable.stocktab_click);
        leadsTab.setBackgroundResource(R.drawable.leadtab_hover);
        toolTab.setBackgroundResource(R.drawable.tooltab_hover);

        hometv.setTextColor(activity.getResources().getColor(R.color.lgray));
        stocktv.setTextColor(activity.getResources().getColor(R.color.yellow));
        leadtv.setTextColor(activity.getResources().getColor(R.color.lgray));
        tooltv.setTextColor(activity.getResources().getColor(R.color.lgray));
    }

    public static void leadsreset(String data) {
        searchicon.setVisibility(View.VISIBLE);

        if (tv_header_title != null) {
            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText(data);
        }

       /* if (tv_header_title.getVisibility() == View.VISIBLE) {
            tv_header_title.setVisibility(View.GONE);
        }*/
        test(activity.getResources().getString(R.string.lead_hint));
        homeTab.setBackgroundResource(R.drawable.hometab_hover);
        stockTab.setBackgroundResource(R.drawable.stocktab_hover);
        leadsTab.setBackgroundResource(R.drawable.leadtab_click);
        toolTab.setBackgroundResource(R.drawable.tooltab_hover);

        hometv.setTextColor(activity.getResources().getColor(R.color.lgray));
        stocktv.setTextColor(activity.getResources().getColor(R.color.lgray));
        leadtv.setTextColor(activity.getResources().getColor(R.color.yellow));
        tooltv.setTextColor(activity.getResources().getColor(R.color.lgray));
    }

    /*public static void toolbarAnimateShow(final int verticalOffset) {
        badgeid.animate()
                .translationY(0)
                .setInterpolator(new LinearInterpolator())
                .setDuration(180)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        //toolbarSetElevation(verticalOffset == 0 ? 0 : TOOLBAR_ELEVATION);
                    }
                });
    }



    public static void toolbarAnimateHide() {
        badgeid.animate()
                .translationY(-badgeid.getHeight())
                .setInterpolator(new LinearInterpolator())
                .setDuration(180)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        //toolbarSetElevation(0);
                    }
                });
    }*/

    public static void top_bottomAnimation() {
        Animation hide = AnimationUtils.loadAnimation(activity, R.anim.top_bottomm);

        badgeid.startAnimation(hide);
    }

    public static void bottom_topAnimation() {
        Animation hide = AnimationUtils.loadAnimation(activity, R.anim.bottom_topp);

        badgeid.startAnimation(hide);

    }

    @OnClick(R.id.homeTab)
    public void homeTab(View view) {

        FilterInstance.getInstance().setNotification("");
        searchicon.setVisibility(View.INVISIBLE);
        homeTab.setBackgroundResource(R.drawable.hometab_click);
        stockTab.setBackgroundResource(R.drawable.stocktab_hover);
        leadsTab.setBackgroundResource(R.drawable.leadtab_hover);
        toolTab.setBackgroundResource(R.drawable.tooltab_hover);

        hometv.setTextColor(this.getResources().getColor(R.color.yellow));
        stocktv.setTextColor(this.getResources().getColor(R.color.lgray));
        leadtv.setTextColor(this.getResources().getColor(R.color.lgray));
        tooltv.setTextColor(this.getResources().getColor(R.color.lgray));

        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ASM)) {

            homeTab.setBackgroundResource(R.color.black);
            stockTab.setBackgroundResource(R.color.black);
            leadsTab.setBackgroundResource(R.color.black);
            toolTab.setBackgroundResource(R.color.black);

        }

        if (CommonMethods.getstringvaluefromkey(activity, "nofication_status").equalsIgnoreCase("true")) {
            hideOption(R.id.notification_bell);
        }

        //   Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.home, GlobalText.android);

        HomeFragment homeFragment = new HomeFragment();
        this.fragmentLoad(homeFragment);

    }

    @OnClick(R.id.homeTabList)
    public void homeTabList(View view) {

        //  MainActivity.avi.smoothToShow();
        Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.home, GlobalText.android);
        FilterInstance.getInstance().setNotification("");
        searchicon.setVisibility(View.INVISIBLE);
        homeTab.setBackgroundResource(R.drawable.hometab_click);
        stockTab.setBackgroundResource(R.drawable.stocktab_hover);
        leadsTab.setBackgroundResource(R.drawable.leadtab_hover);
        toolTab.setBackgroundResource(R.drawable.tooltab_hover);

        hometv.setTextColor(this.getResources().getColor(R.color.yellow));
        stocktv.setTextColor(this.getResources().getColor(R.color.lgray));
        leadtv.setTextColor(this.getResources().getColor(R.color.lgray));
        tooltv.setTextColor(this.getResources().getColor(R.color.lgray));


      /*  if(CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ASM))
        {

            homeTab.setBackgroundResource(R.color.dull_yellow);
            stockTab.setBackgroundResource(R.color.rdm_bnv_bg_yellow);
            leadsTab.setBackgroundResource(R.color.rdm_bnv_bg_yellow);
            toolTab.setBackgroundResource(R.color.rdm_bnv_bg_yellow);
        }*/


        if (CommonMethods.getstringvaluefromkey(activity, "nofication_status").equalsIgnoreCase("true")) {
            hideOption(R.id.notification_bell);
        }

        //Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.home, "Android");

        HomeFragment homeFragment = new HomeFragment();
        this.fragmentLoad(homeFragment);

    }

    @OnClick(R.id.stockTabList)
    public void stockTabList(View view) {
        FilterInstance.getInstance().setNotification("");
        //  MainActivity.avi.smoothToShow();
        test(getResources().getString(R.string.stock_hint));
        homeTab.setBackgroundResource(R.drawable.hometab_hover);
        stockTab.setBackgroundResource(R.drawable.stocktab_click);
        leadsTab.setBackgroundResource(R.drawable.leadtab_hover);
        toolTab.setBackgroundResource(R.drawable.tooltab_hover);

        hometv.setTextColor(this.getResources().getColor(R.color.lgray));
        stocktv.setTextColor(this.getResources().getColor(R.color.yellow));
        leadtv.setTextColor(this.getResources().getColor(R.color.lgray));
        tooltv.setTextColor(this.getResources().getColor(R.color.lgray));

        Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.stocks, "Android");

        if (tv_header_title != null)
            if (tv_header_title.getVisibility() == View.VISIBLE) {
                tv_header_title.setVisibility(View.GONE);
            }
        if (!CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("AM")) {
            StockFragment stockFragment = new StockFragment();
            this.fragmentLoad(stockFragment);
        }

    }

    @OnClick(R.id.stockTab)
    public void stockTab(View view) {

        FilterInstance.getInstance().setNotification("");

        test(getResources().getString(R.string.stock_hint));

        homeTab.setBackgroundResource(R.drawable.hometab_hover);
        stockTab.setBackgroundResource(R.drawable.stocktab_click);
        leadsTab.setBackgroundResource(R.drawable.leadtab_hover);
        toolTab.setBackgroundResource(R.drawable.tooltab_hover);

        hometv.setTextColor(this.getResources().getColor(R.color.lgray));
        stocktv.setTextColor(this.getResources().getColor(R.color.yellow));
        leadtv.setTextColor(this.getResources().getColor(R.color.lgray));
        tooltv.setTextColor(this.getResources().getColor(R.color.lgray));

        Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.stocks, GlobalText.android);

        if (tv_header_title != null)
            if (tv_header_title.getVisibility() == View.VISIBLE) {
                tv_header_title.setVisibility(View.GONE);
            }
        if (!CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("AM")) {
            StockFragment stockFragment = new StockFragment();
            this.fragmentLoad(stockFragment);
        }


    }

    @OnClick(R.id.addStockTab)
    public void addStockTab(View view) {

        FilterInstance.getInstance().setNotification("");
        // searchicon.setVisibility(View.VISIBLE);
        /*if (tv_header_title.getVisibility() == View.VISIBLE) {
            tv_header_title.setVisibility(View.GONE);
        }*/


        selectImage();
    }

    @OnClick(R.id.leadsTab)
    public void leadsTab(View view) {

        //  Log.e("review lead", "lead");
        FilterInstance.getInstance().setNotification("");
        test(getResources().getString(R.string.lead_hint));

        //   searchicon.setVisibility(View.INVISIBLE);

        homeTab.setBackgroundResource(R.drawable.hometab_hover);
        stockTab.setBackgroundResource(R.drawable.stocktab_hover);
        leadsTab.setBackgroundResource(R.drawable.leadtab_click);
        toolTab.setBackgroundResource(R.drawable.tooltab_hover);

        hometv.setTextColor(this.getResources().getColor(R.color.lgray));
        stocktv.setTextColor(this.getResources().getColor(R.color.lgray));
        leadtv.setTextColor(this.getResources().getColor(R.color.yellow));
        tooltv.setTextColor(this.getResources().getColor(R.color.lgray));

        Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.leads, GlobalText.android);
        SalesANDProc();

       /* LeadsFragmentSection leadsFragment = new LeadsFragmentSection();
        this.fragmentLoad(leadsFragment);*/

    }

    @OnClick(R.id.leadsTabList)
    public void leadsTabList(View view) {

        FilterInstance.getInstance().setNotification("");
        // MainActivity.avi.smoothToShow();
        //  searchicon.setVisibility(View.INVISIBLE);
        test(activity.getResources().getString(R.string.stock_hint));

        homeTab.setBackgroundResource(R.drawable.hometab_hover);
        stockTab.setBackgroundResource(R.drawable.stocktab_hover);
        leadsTab.setBackgroundResource(R.drawable.leadtab_click);
        toolTab.setBackgroundResource(R.drawable.tooltab_hover);

        hometv.setTextColor(this.getResources().getColor(R.color.lgray));
        stocktv.setTextColor(this.getResources().getColor(R.color.lgray));
        leadtv.setTextColor(this.getResources().getColor(R.color.yellow));
        tooltv.setTextColor(this.getResources().getColor(R.color.lgray));

        Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.leads, "Android");

       /* LeadsFragmentSection leadsFragment = new LeadsFragmentSection();
        this.fragmentLoad(leadsFragment);*/

        SalesANDProc();

    }


    @OnClick(R.id.toolTab)
    public void toolTab(View view) {

        FilterInstance.getInstance().setNotification("");
        // searchicon.setVisibility(View.INVISIBLE);

        homeTab.setBackgroundResource(R.drawable.hometab_hover);
        stockTab.setBackgroundResource(R.drawable.stocktab_hover);
        leadsTab.setBackgroundResource(R.drawable.leadtab_hover);
        toolTab.setBackgroundResource(R.drawable.tooltab_click);

        hometv.setTextColor(this.getResources().getColor(R.color.lgray));
        stocktv.setTextColor(this.getResources().getColor(R.color.lgray));
        leadtv.setTextColor(this.getResources().getColor(R.color.lgray));
        tooltv.setTextColor(this.getResources().getColor(R.color.yellow));

        Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.leads, GlobalText.android);
       /*ToolFragment toolFragment = new ToolFragment();
        this.fragmentLoad(toolFragment);*/

        selectImage2();
    }

    @OnClick(R.id.toolTabList)
    public void toolTabList(View view) {

        FilterInstance.getInstance().setNotification("");
        //  searchicon.setVisibility(View.INVISIBLE);

        homeTab.setBackgroundResource(R.drawable.hometab_hover);
        stockTab.setBackgroundResource(R.drawable.stocktab_hover);
        leadsTab.setBackgroundResource(R.drawable.leadtab_hover);
        toolTab.setBackgroundResource(R.drawable.tooltab_click);

        hometv.setTextColor(this.getResources().getColor(R.color.lgray));
        stocktv.setTextColor(this.getResources().getColor(R.color.lgray));
        leadtv.setTextColor(this.getResources().getColor(R.color.lgray));
        tooltv.setTextColor(this.getResources().getColor(R.color.yellow));
/*
        ToolFragment toolFragment = new ToolFragment();
        this.fragmentLoad(toolFragment);*/
        selectImage2();

        //navigation.getMenu().findItem(R.id.navigation_createWR).setChecked(false);
    }


    public boolean fragmentLoad(Fragment fragment) {

        if (fragment != null) {
            this.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fillLayout, fragment)
                    .commit();

            return true;
        }


        CommonMethods.setvalueAgainstKey(this, "stockcreated", "false");
        return false;
    }


    public void fragmentLoad_s(Fragment fragment) {


        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {

        } else {

            if (searchicon != null) {
                linearLayoutVisible.setVisibility(View.GONE);
                searchicon.setVisibility(View.VISIBLE);
            }
            if (tv_header_title != null) {

                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText(CommonMethods.getstringvaluefromkey(activity, "dealerName"));
            }
        }

        if (fragment != null) {
            this.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fillLayout, fragment)
                    .commit();
        }


        CommonMethods.setvalueAgainstKey(this, "stockcreated", "false");
    }


    private void selectImage() {
        final Dialog dialog = new Dialog(this);
        //Theme_DeviceDefault_Light_DarkActionBar
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.addstockpop);
        dialog.setCancelable(true);

        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.BOTTOM | Gravity.CENTER;
        wmlp.x = 100;   //x position
        wmlp.y = 150;   //y position

        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        LinearLayout addstockselect = dialog.findViewById(R.id.addstockselect);
        LinearLayout addsalesselect = dialog.findViewById(R.id.addleadselect);
        LinearLayout addproclead = dialog.findViewById(R.id.addproclead);
        LinearLayout capturestock = dialog.findViewById(R.id.capturestock);

        capturestock.setOnClickListener(v -> {

            if (isSalesUser())
                return;

            if (isProcurementUser())
                return;

            if (isDealerCreUser())
                return;

            LPRWarningDialog mDialog = new LPRWarningDialog(MainActivity.this, true);
            mDialog.show();
            dialog.dismiss();
        });

        addstockselect.setOnClickListener(v -> {

            if (isNotAllowed())
                return;

            Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.add_stock, GlobalText.android);
            //AddStockActivity
            Intent in_main = new Intent(MainActivity.this, AddStockActivity.class);
            startActivity(in_main);
            dialog.dismiss();
        });

        addsalesselect.setOnClickListener(v -> {

            if (isProcurementUser())
                return;

            if (isDealerCreUser())
                return;

            Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.add_lead_tab, GlobalText.android);
            //AddLeadActivity
            Intent in_main = new Intent(MainActivity.this, AddLeadActivity.class);
            startActivityForResult(in_main, 200);
            dialog.dismiss();

        });
        addproclead.setOnClickListener(v -> {

            if (isSalesUser())
                return;

            if (isDealerCreUser())
                return;

            dialog.dismiss();
            Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.add_proc_leads, GlobalText.android);
            Intent in_main = new Intent(MainActivity.this, AddProcLead.class);
            startActivity(in_main);

        });

        dialog.show();
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("Home")) {

                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    finish();
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click back again to exit", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 4000);


            } /*else if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {

                if (CommonMethods.getstringvaluefromkey(activity, "asm_pages").equalsIgnoreCase("massagecenter")) {
                    if (searchicon != null) {
                        linearLayoutVisible.setVisibility(View.GONE);
                        searchicon.setVisibility(View.VISIBLE);
                    }
                    if (tv_header_title != null) {
                        tv_header_title.setVisibility(View.VISIBLE);
                        tv_header_title.setText(CommonMethods.getstringvaluefromkey(activity, "dealerName"));
                    }
                    messageFragmentsSwtich();
                } else if (CommonMethods.getstringvaluefromkey(activity, "asm_pages").equalsIgnoreCase("lead_sales_dashboard")) {

                    Fragment loadFragments = new DasboardFragment();
                    loadFragmentHome(loadFragments);
                } else if (CommonMethods.getstringvaluefromkey(activity, "asm_pages").equalsIgnoreCase("report_page")) {
                    Fragment loadFragments = new DasboardFragment();
                    loadFragmentHome(loadFragments);
                } else if (CommonMethods.getstringvaluefromkey(activity, "asm_pages").equalsIgnoreCase("sales_dashboard")) {
                    Fragment loadFragments = new DasboardFragment();
                    loadFragmentHome(loadFragments);
                } else if (CommonMethods.getstringvaluefromkey(activity, "asm_pages").equalsIgnoreCase("leads_list")) {

                    navigation.getMenu().findItem(R.id.navigation_leads).setChecked(true);
                    CommonMethods.setvalueAgainstKey(activity, "leadasm_status", "USEDCARSALES");
                    fragment = new ASMLeadsFragment();
                    loadFragment(fragment);

                }else if (CommonMethods.getstringvaluefromkey(activity, "asm_pages").equalsIgnoreCase("procurement_list")) {

                    searchicon.setVisibility(View.VISIBLE);
                    navigation.getMenu().findItem(R.id.navigation_leads).setChecked(true);
                     CommonMethods.setvalueAgainstKey(activity, "leadasm_status", "PROCUREMENT");
                    fragment = new ASMLeadsFragment();
                    loadFragment(fragment);

                } else if (CommonMethods.getstringvaluefromkey(activity, "asm_pages").equalsIgnoreCase("dashboard")) {
                    if (doubleBackToExitPressedOnce) {
                        super.onBackPressed();
                        finish();
                        return;
                    }
                    this.doubleBackToExitPressedOnce = true;
                    Toast.makeText(this, "Please click back again to exit", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            doubleBackToExitPressedOnce = false;
                        }
                    }, 4000);

                } else {

                }
            }*/
        }
    }

    public static Menu menu;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main2, menu);
        Log.i(TAG, "onCreateOptionsMenu: ");
        MainActivity.menu = menu;
        getMenuInflater().inflate(R.menu.menu_item, menu);
        MenuItem item = menu.findItem(R.id.share);
        item.setVisible(false);
        item = menu.findItem(R.id.add_image);
        item.setVisible(false);
        item = menu.findItem(R.id.delete_fea);
        item.setVisible(false);
        item = menu.findItem(R.id.stock_fil_clear);
        item.setVisible(false);
        item = menu.findItem(R.id.asm_mail);
        item.setVisible(false);
        item = menu.findItem(R.id.asmHome);
        item.setVisible(false);
        String utype = CommonMethods.getstringvaluefromkey(this, "user_type");


        if (utype.equalsIgnoreCase("dealer") || utype.equalsIgnoreCase("AM")) {
            item = menu.findItem(R.id.notification_bell);
            item.setVisible(true);
            int i = R.id.notification_bell;
            hideOption(i);
        } else if (utype.equalsIgnoreCase("ASM")) {

            item = menu.findItem(R.id.notification_bell);
            menu.findItem(R.id.notification_bell).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_rdr_notifications));
            item.setVisible(true);
            int i = R.id.notification_bell;
            hideOption(i);
        }

        if (CommonMethods.getstringvaluefromkey(this, "user_type").equalsIgnoreCase(USER_TYPE_DEALER_CRE) ||
                CommonMethods.getstringvaluefromkey(this, "user_type").equalsIgnoreCase(USERTYPE_ASM) ||
                CommonMethods.getstringvaluefromkey(this, "user_type").equalsIgnoreCase(USER_TYPE_SALES) ||
                CommonMethods.getstringvaluefromkey(this, "user_type").equalsIgnoreCase(USER_TYPE_PROCUREMENT)) {

            item = menu.findItem(R.id.asm_mail);
            item.setVisible(false);
        }

        item = menu.findItem(R.id.notification_cancel);
        item.setVisible(false);


        return true;

    }

    public static void hideASMMailOption(int id) {
        try {

            if (menu != null) {
                MenuItem item = menu.findItem(id);
                item.setVisible(false);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public static void hideOption(int id) {
        try {
            MenuItem item = menu.findItem(id);
            item.setVisible(true);

            if (CommonMethods.getstringvaluefromkey(activity, "nofication_status").equalsIgnoreCase("true")) {

                if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ASM)) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            item.setIcon(R.drawable.ic_rdr_notifications);
                        }
                    });

                } else {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            item.setIcon(R.drawable.bell_notofication);
                        }
                    });
                }


            } else {

                if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ASM)) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            item.setIcon(R.drawable.ic_rdr_notifications);
                        }
                    });

                } else {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            item.setIcon(R.drawable.bell);
                        }
                    });
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (item.getItemId()) {
            case R.id.notification_bell:

                String utype = CommonMethods.getstringvaluefromkey(this, "user_type");

                if (utype.equalsIgnoreCase("AM")) {
                    Intent intent = new Intent(MainActivity.this, NotificationActivity.class);
                    startActivity(intent);
                } else {
                    if (isNotAllowed()) {

                    } else {

                        CommonMethods.setvalueAgainstKey(MainActivity.this, "nofication_status", "false");

                        hideOption(R.id.notification_bell);

                        Intent intent = new Intent(MainActivity.this, NotificationListActivity.class);
                        startActivity(intent);
                    }
                }


                return true;
        }

      /*  //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        */

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

//        findViewById(R.id.navigation_asmcalendar).setEnabled(false);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);


        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {


        } else {


            if (tv_header_title.getText().toString().equalsIgnoreCase("Dashboard")) {

                searchicon.setVisibility(View.VISIBLE);
                if (tv_header_title != null) {
                    tv_header_title.setVisibility(View.VISIBLE);
                    tv_header_title.setText("Dashboard");
                }
            } else {
                searchicon.setVisibility(View.VISIBLE);
            }
        }

        if (id == R.id.nav_camera) {
            // Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.nav_my_profile, GlobalText.android);

            Intent in_main = new Intent(MainActivity.this, MapsActivity.class);
            startActivity(in_main);

        } else if (id == R.id.nav_feedback) {

            Intent in_main = new Intent(MainActivity.this, FeedbackActivity.class);
            startActivity(in_main);

        } else if (id == R.id.nav_autofin) {

            Intent in_main = new Intent(MainActivity.this, HostActivity.class);
            in_main.putExtra(AutoFinConstants.APP_NAME, CommonStrings.APP_NAME_VAL);
            // in_main.putExtra(AutoFinConstants.DEALER_ID,"242");
            in_main.putExtra(AutoFinConstants.DEALER_ID, CommonMethods.getstringvaluefromkey(this, "dealer_code"));
            in_main.putExtra(AutoFinConstants.USER_TYPE, "Dealer");
            startActivity(in_main);

        } else if (id == R.id.nav_viewreport) {
            Intent in_main = new Intent(MainActivity.this, StockReconciliationReport.class);
            startActivity(in_main);
        } else if (id == R.id.nav_payment) {

            Intent in_main = new Intent(MainActivity.this, PaymentActivity.class);
            startActivity(in_main);

        } else if (id == R.id.nav_gallery) {


            if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {

            } else {
                if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("dashboard")) {

                    searchicon.setVisibility(View.VISIBLE);
                    if (tv_header_title != null) {
                        tv_header_title.setVisibility(View.VISIBLE);
                        tv_header_title.setText("Dashboard");
                    }

                }
            }
            logoutAlert();
        } else if (id == R.id.sub_wkreport) {
            Intent intent = new Intent(MainActivity.this, SubmittedReports.class);
            startActivity(intent);

        } else if (id == R.id.capture_stock) {

            Intent intent = new Intent(MainActivity.this, SelectDealerForLPR.class);
            startActivity(intent);

        } else if (id == R.id.sub_dealer_visit) {
            Intent i = new Intent(MainActivity.this, RDRDashBoard.class);
            startActivity(i);
        } else if (id == R.id.dealer_actionitems) {
            Intent i = new Intent(MainActivity.this, ViewDetailedListing.class);
            i.putExtra("dcode", CommonMethods.getstringvaluefromkey(MainActivity.this, "dealer_code"));
            startActivity(i);
        } else if (id == R.id.monthly_target) {
            Intent i = new Intent(MainActivity.this, MonthlyTargetActivity.class);
            startActivity(i);
        } else if (id == R.id.dealer_dashboard) {
            Intent i = new Intent(MainActivity.this, DealerDashboardActivity.class);
            startActivity(i);
        } else if (id == R.id.dashboard) {
            this.loadFragment(new DasboardFragment());
        } else if (id == R.id.rdr_menu_submitted_wr) {
            Intent intent = new Intent(MainActivity.this, SubmittedReports.class);
            startActivity(intent);
        } else if (id == R.id.rdr_menu_rdr_dashboard) {
            startActivity(new Intent(this, RDRDashBoard.class));
            // fragmentLoad(new ASMLandingFragment(this));
        } else if (id == R.id.rdr_menu_logout) {
            if (tv_header_title != null) {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("DASHBOARD");
            }
            logoutAlert();
        }

        //asm n
        //searchClose();

        Fragment fragment = null;


        switch (item.getItemId()) {
            case R.id.navigation_stock:

                searchbtmHide();
                fragment = new ASMStockFragment();
                break;
            case R.id.navigation_leads:
                ASMselectImage();
                break;
            case R.id.nav_sales:
            case R.id.navigation_sales:
                searchClose();
                searchbtmHide();
                fragment = new ASMSalesFragment();
                break;

            case R.id.navigation_reports:
                searchClose();
                searchbtmHide();
                if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ZONALHEAD) ||
                        CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_STATEHEAD)) {

                    if (linearLayoutVisible != null) {
                        if (linearLayoutVisible.getVisibility() == View.VISIBLE) {
                            linearLayoutVisible.setVisibility(View.GONE);
                            setMainHeading(CommonMethods.getstringvaluefromkey(activity, "dealerName"));
                        }
                    }
                }
                fragment = new ASMReportsFragment();
                break;

            case R.id.navigation_createWR:
                leadsTabList.setBackgroundResource(R.color.dull_yellow);
/*
                Drawable unwrappedDrawable = AppCompatResources.getDrawable(this, R.drawable.ic_rdr_create_wr_side);
                Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                DrawableCompat.setTint(wrappedDrawable, Color.BLACK);*/
                //navigation.setItemBackgroundResource(R.color.dull_yellow);
                Intent intent = new Intent(MainActivity.this, WeeklyReport.class);
                startActivity(intent);
              /*  Intent intent=new Intent(MainActivity.this, ComingSoon.class);
                startActivity(intent);*/
                break;

            case R.id.navigation_dashboard:

                /*navigation.setSelected(true);*/
                //this.invalidateOptionsMenu();

                //fragment = new ASMLandingFragment(this);
                break;

            case R.id.navigation_dlrvisit:
                //navigation.setItemBackgroundResource(R.color.dull_yellow);
                /*navigation.setSelected(true);*/

                fragment = new RDMDealerListFragment(this);
                break;

        /*    case R.id.navigation_dlrvisit:

                try {

                    Intent i = new Intent(MainActivity.this, DiscussionPoint1.class);
                    startActivity(i);
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "please go to home page to view this page", Toast.LENGTH_SHORT).show();
                }
                break;*/

            case R.id.navigation_asmcalendar:
                //navigation.setItemBackgroundResource(R.color.dull_yellow);
                /*navigation.setSelected(true);*/

                fragment = new RDMScheduleFragment(MainActivity.this);
                break;
               /* Intent inte=new Intent(MainActivity.this, VideoActivity.class);
                startActivity(inte);*/

              /*  Intent inte = new Intent(MainActivity.this, ComingSoon.class);
                startActivity(inte);
*/


        }

        return loadFragment(fragment);

        // fragmentLoad(fragment);

        // return true;
    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        Log.i(TAG, "loadFragment: ");
        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")
                || CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_STATEHEAD)
                || CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ZONALHEAD)) {

        } else {

            if (searchicon != null) {
                linearLayoutVisible.setVisibility(View.GONE);
                searchicon.setVisibility(View.VISIBLE);
            }
            if (tv_header_title != null) {

                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText(CommonMethods.getstringvaluefromkey(activity, "dealerName"));
            }
        }

        if (fragment != null) {
            this.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fillLayout, fragment)
                    .commit();

            return true;
        }
        return false;
    }


    public static void logOut() {

        // CommonMethods.setvalueAgainstKey(activity, "appstatus", "0");


        try {
            resetMethodForLeads();
            resetMehods();
            SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(activity);
            SharedPreferences mSharedPreferences = activity.getSharedPreferences("MFC", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preference.edit();
            SharedPreferences.Editor e = mSharedPreferences.edit();
            e.clear();
            e.commit();
            editor.clear().commit();
            CommonMethods.deleteCache(activity);
            CommonMethods.setvalueAgainstKey(activity, "token", "");
            CommonMethods.MemoryClears();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            FirebaseInstanceId.getInstance().deleteInstanceId();
        } catch (IOException e) {
            e.printStackTrace();
        }

        CommonMethods.setvalueAgainstKey(activity, "appstatus", "0");
      /*  Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("com.package.ACTION_LOGOUT");
        activity.sendBroadcast(broadcastIntent);
      */
        Intent in_main = new Intent(activity, LoginActivity.class);
        in_main.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(in_main);
        activity.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume: ");
        try {
            PackageInfo pInfo = activity.getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            this.fetchAppVersion(activity, version, "Android");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        // this lines are causing the issue when you power off/ power on why this is written need know from sangeetha

      /*  if(CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(USERTYPE_ASM))
        {
            toolbar.setBackgroundColor(getResources().getColor(R.color.all_dealers_fragment_bg));
        }
*/
        if (!CommonMethods.getstringvaluefromkey(activity, "token").equalsIgnoreCase("")) {

            String Dealer_code = CommonMethods.getstringvaluefromkey(activity, "dealer_code");
            String dealer_display_name = CommonMethods.getstringvaluefromkey(activity, "dealer_display_name");

            Bundle params = new Bundle();
            params.putString("Dealer_code", Dealer_code);
            params.putString("dealer_display_name", dealer_display_name);
            params.putString("token", CommonMethods.getstringvaluefromkey(activity, "token"));
            params.putString(GlobalText.android, GlobalText.android);

            firebaseAnalytics.logEvent("Dealer_code", params);

        }


        LoginActivity.isLoading = false;

        if (LeadsInstance.getInstance().getAddlead().equals("RedirectLeadPage")) {
            searchicon.setVisibility(View.VISIBLE);

            if (tv_header_title != null) {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Sales Leads");
            }
            homeTab.setBackgroundResource(R.drawable.hometab_hover);
            stockTab.setBackgroundResource(R.drawable.stocktab_hover);
            leadsTab.setBackgroundResource(R.drawable.leadtab_click);
            toolTab.setBackgroundResource(R.drawable.tooltab_hover);

            hometv.setTextColor(this.getResources().getColor(R.color.lgray));
            stocktv.setTextColor(this.getResources().getColor(R.color.lgray));
            leadtv.setTextColor(this.getResources().getColor(R.color.yellow));
            tooltv.setTextColor(this.getResources().getColor(R.color.lgray));

            LeadsFragmentSection leadsFragment = new LeadsFragmentSection();
            this.fragmentLoad(leadsFragment);
        }


    }

    String todayfromDate = "", todaytoDate = "";

    public String CalendarTodayDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, 1);
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        todayfromDate = s.format(new Date(cal.getTimeInMillis()));
        todaytoDate = s.format(new Date(cal1.getTimeInMillis()));
        return todayfromDate + "@" + todaytoDate;
    }

    public static void test(String test) {
      /*  SpannableString span = new SpannableString(strHint);
        span.setSpan(new RelativeSizeSpan(0.5f), 0, strHint.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        editText.setHint(span);*/

        String queryHint = "<small>" + test + "</small>";
        searchView.setQueryHint(Html.fromHtml(queryHint));
    }


    public void swipeRefreshLayout() {

        if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("StockStore")) {
            headerdata.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            swipeRefreshLayout.setEnabled(false);

        } else if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("BookedStock")) {
            headerdata.setVisibility(View.GONE);
            swipeRefreshLayout_book.setRefreshing(false);
            swipeRefreshLayout_book.setEnabled(false);

        } else if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("Tools")) {
            ToolFragment.tool_headerdata.setVisibility(View.GONE);
            ToolFragment.swipeRefreshLayout.setRefreshing(false);
            ToolFragment.swipeRefreshLayout.setEnabled(false);

        } else if (LeadsInstance.getInstance().getWhichleads().equals("WebLeads")) {
            try {
                headerdataLeads.setVisibility(View.GONE);
            } catch (Exception e) {

            }
            try {
                swipeRefreshLayoutWebLeads.setRefreshing(false);
                swipeRefreshLayoutWebLeads.setEnabled(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (LeadsInstance.getInstance().getWhichleads().equals("PrivateLeads")) {
            try {
                headerdataLeads.setVisibility(View.GONE);
            } catch (Exception e) {

            }
            swipeRefreshLayoutPrivate.setRefreshing(false);
            swipeRefreshLayoutPrivate.setEnabled(false);
        }

    }

    private void ASMselectImage() {
        final Dialog dialog = new Dialog(this);
        //Theme_DeviceDefault_Light_DarkActionBar
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.addlead_asmpop);
        dialog.setCancelable(true);

        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.BOTTOM | Gravity.CENTER;
        wmlp.x = 100;   //x position
        wmlp.y = 150;   //y position

        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        LinearLayout sales_leads = dialog.findViewById(R.id.sales_leads);
        LinearLayout procurements_leads = dialog.findViewById(R.id.procurements_leads);

        sales_leads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                navigation.getMenu().findItem(R.id.navigation_leads).setChecked(true);
                searchClose();
                CommonMethods.setvalueAgainstKey(activity, "leadasm_status", "USEDCARSALES");

                searchbtmHide();

                fragment = new ASMLeadsFragment();
                loadFragment(fragment);

                dialog.dismiss();
            }
        });

        procurements_leads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                searchClose();
                searchicon.setVisibility(View.VISIBLE);
                navigation.getMenu().findItem(R.id.navigation_leads).setChecked(true);

                CommonMethods.setvalueAgainstKey(activity, "leadasm_status", "PROCUREMENT");

                fragment = new ASMLeadsFragment();
                loadFragment(fragment);
                searchbtmHide();

                dialog.dismiss();

            }
        });


        dialog.show();
    }

    public static void searchDisable() {

        searchFlag = false;
        entersearch = false;

        searchVal = "";
        oneTimeSearch = true;

        //
        try {
            searchView.setQuery("", false);
            searchView.clearFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }


        searchicon.setVisibility(View.VISIBLE);
        cardView.setVisibility(View.GONE);

        if (tv_header_title != null) {

            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText(CommonMethods.getstringvaluefromkey(activity, "dealerName"));
        }
    }

    public static void searchClose() {

        test("");
        searchFlag = false;
        entersearch = false;

        searchVal = "";
        oneTimeSearch = true;

        //
        try {
            searchView.setQuery("", false);
            searchView.clearFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("StockStore")) {
            if (headerdata != null) {
                headerdata.setVisibility(View.VISIBLE);
            }
            test(activity.getResources().getString(R.string.stock_hint));
            if (swipeRefreshLayout != null) {
                swipeRefreshLayout.setEnabled(true);
            }

        } else if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("BookedStock")) {
            test(activity.getResources().getString(R.string.stock_hint));
            if (swipeRefreshLayout_book != null) {
                swipeRefreshLayout_book.setEnabled(true);
            }

            if (headerdata != null) {
                headerdata.setVisibility(View.VISIBLE);
            }
        } else if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("Tools")) {
            try {
                if (ToolFragment.tool_headerdata != null)
                    ToolFragment.tool_headerdata.setVisibility(View.VISIBLE);
                test(activity.getResources().getString(R.string.stock_hint));
                ToolFragment.swipeRefreshLayout.setEnabled(true);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
        if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("WebLeads")) {
            if (swipeRefreshLayoutWebLeads != null) {
                swipeRefreshLayoutWebLeads.setEnabled(true);
            }

           /* try {
                SortByLeads();
            } catch (Exception e) {
                Log.e("Exception ", "Exception " + e.toString());
            }*/

            test(activity.getResources().getString(R.string.lead_hint));
            try {
                headerdataLeads.setVisibility(View.VISIBLE);
            } catch (Exception e) {

            }
        }
        if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("PrivateLeads")) {
            test(activity.getResources().getString(R.string.lead_hint));
            if (swipeRefreshLayoutPrivate != null) {
                swipeRefreshLayoutPrivate.setEnabled(true);
            }

            try {
                headerdataLeads.setVisibility(View.VISIBLE);
            } catch (Exception e) {

            }

        }

        linearLayoutVisible.setVisibility(View.VISIBLE);
        cardView.setVisibility(View.GONE);
        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ASM)) {
            toggle.setDrawerIndicatorEnabled(false);
            toggle.setHomeAsUpIndicator(R.drawable.ic_new_hamburger_menu);
        } else {
            toggle.setDrawerIndicatorEnabled(true);
        }

    }

    private void selectImage2() {

        final Dialog dialog = new Dialog(this);
        //Theme_DeviceDefault_Light_DarkActionBar
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_tool_popup);
        dialog.setCancelable(false);

        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.BOTTOM | Gravity.CENTER;
        wmlp.x = 100;   //x position
        wmlp.y = 150;   //y position

        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        LinearLayout addstockselect = dialog.findViewById(R.id.addstockselect);
        LinearLayout addleadselect = dialog.findViewById(R.id.addleadselect);

        addstockselect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNotAllowed())
                    return;

                //AddStockActivity
           /*     Intent in_main = new Intent(MainActivity.this, AddStockActivity.class);
                startActivity(in_main);*/


                searchicon.setVisibility(View.VISIBLE);
                if (tv_header_title != null)
                    if (tv_header_title.getVisibility() == View.VISIBLE) {
                        tv_header_title.setVisibility(View.GONE);
                    }
                searchClose();


                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.market_trade, GlobalText.android);

                testtoolfrg();

                dialog.dismiss();
            }
        });

        addleadselect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //AddLeadActivity
              /*  Intent in_main = new Intent(MainActivity.this, AddLeadActivity.class);
                startActivity(in_main);*/

                searchicon.setVisibility(View.INVISIBLE);
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.view_ibb_price, GlobalText.android);
                testtoolIbbPrice();
                dialog.dismiss();

            }
        });

        dialog.show();

    }

    public void testtoolfrg() {
        ToolFragment toolFragment = new ToolFragment();
        this.fragmentLoad(toolFragment);
    }

    public void testtoolIbbPrice() {
        ToolIbbPriceFrag toolIbbPrice = new ToolIbbPriceFrag();
        this.fragmentLoad(toolIbbPrice);

        //WebServicesCall.webCall(activity, activity, jsonMakeIBBAccessToken(), "IBBAccessToken", GlobalText.POST);

        //Staging
        //  Ibbmarket(this, "mfc@ibb.com", "dHk69ffu7ebP");

        if (CommonMethods.isInternetWorking(activity)) {
            //Production
            Ibbmarket(this, Global_Urls.ibb_uname, Global_Urls.ibb_password);

        } else {
            CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
        }

    }

    private JSONObject jsonMakeIBBAccessToken() {
        JSONObject jObj = new JSONObject();
        try {
            jObj.put("username", "mfc@ibb.com");
            jObj.put("password", "LVUM2gHfiDrJ");

        } catch (JSONException e) {
            e.printStackTrace();
            //  FirebaseCrash.report(e);
        }

        return jObj;
    }

    public static void ParseIBBAccessToken(JSONObject jObj, Activity activity) {

        // Log.e("IBBaccess_token ", "is " + jObj.toString());
        try {
            Log.e("IBBaccess_token ", "is " + jObj.getString("access_token"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            if (jObj.getString("status").equalsIgnoreCase("success")) {

                CommonMethods.setvalueAgainstKey(activity, "IBB_access_token", jObj.getString("access_token"));

            /*    //MainActivity
                Intent in_main = new Intent(activity, MainActivity.class);
                activity.startActivity(in_main);*/
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void logoutAlert() {

        // custom dialog
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.common_cus_dialog);
        //dialog.setTitle("Custom Dialog");

        TextView Message, Message2;
        Button cancel, Confirm;
        Message = dialog.findViewById(R.id.Message);
        Message2 = dialog.findViewById(R.id.Message2);
        cancel = dialog.findViewById(R.id.cancel);
        Confirm = dialog.findViewById(R.id.Confirm);
        Message.setText("Are you sure you want to ");
        Message2.setText("Logout ?");
        String title_text = tv_header_title.getText().toString();

        Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //    Log.e("access_token", CommonMethods.getstringvaluefromkey(activity, "access_token"));

                //Volley
                // WebServicesCall.webCall(activity, activity, jsonMakelogout(), "Logout", GlobalText.POST);

                // Retrofit
                LogOutRequest mLogOutRequest = new LogOutRequest();
                mLogOutRequest.setUserId(CommonMethods.getstringvaluefromkey(activity, "user_id").toString());
                mLogOutRequest.setDeviceId(CommonMethods.getstringvaluefromkey(activity, "device_id").toString());
                logoutApi(mLogOutRequest, MainActivity.this);
                if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ASM)) {
                    Application.getInstance().logASMEvent(GAConstants.LOGOUT_DETAILS, CommonMethods.getstringvaluefromkey(MainActivity.this, "user_id") + System.currentTimeMillis() + "" + CommonMethods.getstringvaluefromkey(activity, "device_id"));

                }
                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {

                } else if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ASM)) {

                    if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("dashboard")) {
                        if (tv_header_title != null) {
                            tv_header_title.setVisibility(View.VISIBLE);
                            tv_header_title.setText("DASHBOARD");
                        }
                    } else if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("dealers")) {
                        if (tv_header_title != null) {
                            tv_header_title.setVisibility(View.VISIBLE);
                            tv_header_title.setText("DEALERS");
                        }
                    } else if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("dealer_details")) {
                        if (tv_header_title != null) {
                            tv_header_title.setVisibility(View.VISIBLE);
                            tv_header_title.setText("DEALER DETAILS");
                        }
                    }
                } else {
                    if (CommonMethods.getstringvaluefromkey(activity, "status").equalsIgnoreCase("dashboard")) {

                        searchicon.setVisibility(View.VISIBLE);
                        if (tv_header_title != null) {
                            tv_header_title.setVisibility(View.VISIBLE);
                            tv_header_title.setText("Dashboard");
                        }

                    }
                }

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public JSONObject jsonMakelogout() {
        JSONObject jObj = new JSONObject();


        try {
            jObj.put("userId", CommonMethods.getstringvaluefromkey(activity, "user_id"));
            jObj.put("deviceId", CommonMethods.getstringvaluefromkey(activity, "device_id").toString());


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;

    }


    public static void ParseLogout(JSONObject jObj, String strMethod) {
        try {
            if (jObj.getString("status").equalsIgnoreCase("SUCCESS")) {
                logOut();
            } else {
                CommonMethods.alertMessage(activity, "Logout Failed.");
            }
        } catch (Exception ex) {
            Log.e("logout=Ex", ex.toString());

        }
    }

    //log out api
    private void logoutApi(LogOutRequest mLogOutRequest, Context mContext) {
        SpinnerManager.showSpinner(mContext);
        LoginService.logOutfromServer(mLogOutRequest, mContext, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                try {
                    Response<LogOutResponse> mRes = (Response<LogOutResponse>) obj;
                    if (mRes.body().getStatus().equalsIgnoreCase("SUCCESS")) {
                        logOut();
                    } else {
                        CommonMethods.alertMessage(activity, "Logout Failed.");
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);
            }
        });
    }

    public static void resetMethodForLeads() {
        //Yuvaraj
        try {
            ClearPostDataFilter();

            ClearFollowDataFilter();

            ClearLeadStatusFilter();

            NotificationInstance.getInstance().setTodayfollowdate("");

            chposttoday.setChecked(false);
            chpostyester.setChecked(false);
            chpost7days.setChecked(false);
            chpost15days.setChecked(false);

            chfollowtmrw.setChecked(false);
            chfollowtoday.setChecked(false);
            chfollowyester.setChecked(false);
            chfollowlast7days.setChecked(false);

            chopen.setChecked(false);
            chhot.setChecked(false);
            chwarm.setChecked(false);
            chcold.setChecked(false);
            chlost.setChecked(false);
            chsold.setChecked(false);

            postFromDate = "";
            postToDate = "";

            followFromDate = "";
            followToDate = "";

            postTodYesLastFromDate = "";
            postTodYesLastToDate = "";

            followTodYesLastFromDate = "";
            followTodYesLastToDate = "";

            followTomorowdate = "";
            followTodayDate = "";

            leadstatusJsonArray = new JSONArray();

            fromdate.setText("");
            todate.setText("");

            fromdatefoll.setText("");
            todatefoll.setText("");
        } catch (Exception e) {
            Log.e("LoginExcep ", "Exception");
        }

    }


    public void followup_notification() {

        searchicon.setVisibility(View.VISIBLE);
        homeTab.setBackgroundResource(R.drawable.hometab_hover);
        stockTab.setBackgroundResource(R.drawable.stocktab_hover);
        leadsTab.setBackgroundResource(R.drawable.leadtab_click);
        toolTab.setBackgroundResource(R.drawable.tooltab_hover);

        clearFilterLeads();
        saveTodayFollowupLeads();

        SharedPreferences.Editor mEdit = mSharedPreferences.edit();
        mEdit.putString(LeadConstants.LEAD_FOLUP_TODAY, CalendarTodayDate());
        mEdit.commit();
        ClearOtherFilterData();

        //NotificationInstance.getInstance().setTodayfollowdate(CalendarTodayDate());
        FilterInstance.getInstance().setNotification("Follow");

        leadFollowUp = "";
        hometv.setTextColor(this.getResources().getColor(R.color.lgray));
        stocktv.setTextColor(this.getResources().getColor(R.color.lgray));
        leadtv.setTextColor(this.getResources().getColor(R.color.yellow));
        tooltv.setTextColor(this.getResources().getColor(R.color.lgray));
        test(activity.getResources().getString(R.string.lead_hint));

        if (tv_header_title != null) {
            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText("Sales Leads");
        }
        LeadsFragmentSection leadsFragment = new LeadsFragmentSection();
        this.fragmentLoad(leadsFragment);

        searchClose();
    }

    public void newleads_notification() {

        clearFilterLeads();
        // write by surya for notification open code optimization
        //leadsTab.performClick();

        searchicon.setVisibility(View.VISIBLE);
        homeTab.setBackgroundResource(R.drawable.hometab_hover);
        stockTab.setBackgroundResource(R.drawable.stocktab_hover);
        leadsTab.setBackgroundResource(R.drawable.leadtab_click);
        toolTab.setBackgroundResource(R.drawable.tooltab_hover);

        FilterInstance.getInstance().setNotification("new lead");
        lead = "";
        hometv.setTextColor(this.getResources().getColor(R.color.lgray));
        stocktv.setTextColor(this.getResources().getColor(R.color.lgray));
        leadtv.setTextColor(this.getResources().getColor(R.color.yellow));
        tooltv.setTextColor(this.getResources().getColor(R.color.lgray));

        test(activity.getResources().getString(R.string.lead_hint));

        ClearOtherFilterData();
        if (tv_header_title != null) {
            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText("Sales Leads");
        }
        LeadsFragmentSection leadsFragment = new LeadsFragmentSection();
        this.fragmentLoad(leadsFragment);

        searchClose();
    }

    public void thirtydays_old_notification() {
        test(getResources().getString(R.string.stock_hint));

        homeTab.setBackgroundResource(R.drawable.hometab_hover);
        stockTab.setBackgroundResource(R.drawable.stocktab_click);
        leadsTab.setBackgroundResource(R.drawable.leadtab_hover);
        toolTab.setBackgroundResource(R.drawable.tooltab_hover);

        hometv.setTextColor(this.getResources().getColor(R.color.lgray));
        stocktv.setTextColor(this.getResources().getColor(R.color.yellow));
        leadtv.setTextColor(this.getResources().getColor(R.color.lgray));
        tooltv.setTextColor(this.getResources().getColor(R.color.lgray));

        postdatelessthirty = true;
        CommonMethods.setvalueAgainstKey(this, "Notification_Switch", "type");
        stock_photocountsFlag = false;
        CommonMethods.setvalueAgainstKey(this, "photocounts", "false");
        CommonMethods.setvalueAgainstKey(this, "filterapply", "false");
        AgeingInventory = "";
        FilterInstance.getInstance().setNotification("old");

        CommonMethods.setvalueAgainstKey(MainActivity.this, "stockfilterOnclick", "true");


    /*    stock_photocountsFlag = false;LeadsFragmentSection
        CommonMethods.setvalueAgainstKey(activity, "photocounts", "false");
        CommonMethods.setvalueAgainstKey(activity, "Notification_Switch", "AgeingInventory");
        CommonMethods.setvalueAgainstKey(activity,"filterapply","false");
        MainActivity.postdatelessthirty = true;
        FilterInstance.getInstance().setNotification("old");*/

        if (tv_header_title != null)
            if (tv_header_title.getVisibility() == View.VISIBLE) {
                tv_header_title.setVisibility(View.GONE);
            }


        if (!CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("AM")) {
            StockFragment stockFragment = new StockFragment();
            this.fragmentLoad(stockFragment);
        }
        searchClose();
        //StockFilterActivity.stock_photocountsFlag = false;
    }

    public void noimage_notification() {

        searchicon.setVisibility(View.VISIBLE);
        homeTab.setBackgroundResource(R.drawable.hometab_hover);
        stockTab.setBackgroundResource(R.drawable.stocktab_click);
        leadsTab.setBackgroundResource(R.drawable.leadtab_hover);
        toolTab.setBackgroundResource(R.drawable.tooltab_hover);

        hometv.setTextColor(this.getResources().getColor(R.color.lgray));
        stocktv.setTextColor(this.getResources().getColor(R.color.yellow));
        leadtv.setTextColor(this.getResources().getColor(R.color.lgray));
        tooltv.setTextColor(this.getResources().getColor(R.color.lgray));
        test(getResources().getString(R.string.stock_hint));
        postdatelessthirty = false;
        noStockImage = "";
        StockFilterActivity.stock_photocountsFlag = true;
        CommonMethods.setvalueAgainstKey(this, "photocounts", "true");
        CommonMethods.setvalueAgainstKey(this, "thirtydaycheckbox", "false");
        CommonMethods.setvalueAgainstKey(this, "filterapply", "false");
        // CommonMethods.setvalueAgainstKey(this, "photocounts", "true");
        FilterInstance.getInstance().setNotification("missing images");

        CommonMethods.setvalueAgainstKey(MainActivity.this, "stockfilterOnclick", "true");

        if (tv_header_title != null)
            if (tv_header_title.getVisibility() == View.VISIBLE) {
                tv_header_title.setVisibility(View.GONE);
            }
        if (!CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("AM")) {
            StockFragment stockFragment = new StockFragment();
            this.fragmentLoad(stockFragment);
        }

        searchClose();
    }


    public static void resetMehods() {

        CommonMethods.setvalueAgainstKey(activity, "booknowbtn", "false");
        CommonMethods.setvalueAgainstKey(activity, "bookfilter", "false");
        CommonMethods.setvalueAgainstKey(activity, "stockdetails", "empty");


        //notification data
        CommonMethods.setvalueAgainstKey(activity, "AgeingStockTrue", "");
        CommonMethods.setvalueAgainstKey(activity, "LeadFollowUpTrue", "");
        CommonMethods.setvalueAgainstKey(activity, "ImageUploadRemainderTrue", "");
        CommonMethods.setvalueAgainstKey(activity, "NewLeadTrue", "");

        CommonMethods.setvalueAgainstKey(activity, "status", "Home");

    }

    private void Ibbmarket(final Context mContext, String name, String password) {
        SpinnerManager.showSpinner(mContext);
        MarketTradeService.IddMrkTradegetAccessToken(mContext, name, password, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                Response<ResponseAccessTokenIbbTrade> mRes = (Response<ResponseAccessTokenIbbTrade>) obj;
                ResponseAccessTokenIbbTrade mResponseAccessTokenIbbTrade = mRes.body();

                //   Log.e("test retrofit: ", mResponseAccessTokenIbbTrade.getAccessToken());
                if (mRes.isSuccessful()) {
                    CommonMethods.setvalueAgainstKey(activity, "IBB_access_token", mResponseAccessTokenIbbTrade.getAccessToken());
                } else {
                    //  Log.e("ibbmarket", mRes.message());
                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);

            }
        });
    }

    private void fetchAppVersion(final Context mContext, final String mVersion, final String mDeviceType) {
        SpinnerManager.showSpinner(mContext);
        LoginService.getAppVersion(mVersion, mDeviceType, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                Response<AppVersionResponse> mRes = (Response<AppVersionResponse>) obj;
                AppVersionResponse mResponse = mRes.body();
                SpinnerManager.hideSpinner(mContext);
                // check version res if pass don't do any thing if fail show the pop up to upgrade
                if (mResponse.getStatus().equalsIgnoreCase("SUCCESS")) {

                } else {
                    CommonMethods.error_popup(mContext, mResponse.getUrl());
                }
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);

            }
        });

    }

    private void ClearOtherFilterData() {
        SharedPreferences.Editor mEditor0 = mSharedPreferences.edit();
        mEditor0.putString(LeadConstants.LEAD_POST_TODAY, "");
        //clear others
        mEditor0.putString(LeadConstants.LEAD_POST_CUSTDATE, "");
        mEditor0.putString(LeadConstants.LEAD_POST_YESTER, "");
        mEditor0.putString(LeadConstants.LEAD_POST_7DAYS, "");
        mEditor0.putString(LeadConstants.LEAD_POST_15DAYS, "");

        mEditor0.putString(LeadConstants.LEAD_FOLUP_TMRW, "");
        //mEditor0.putString(LeadConstants.LEAD_FOLUP_TODAY, "");//because set Follow-Up date other Filter
        mEditor0.putString(LeadConstants.LEAD_FOLUP_YESTER, "");
        mEditor0.putString(LeadConstants.LEAD_FOLUP_7DAYS, "");
        mEditor0.putString(LeadConstants.LEAD_FOLUP_CUSTDATE, "");
        mEditor0.putString(LeadConstants.LEAD_STATUS_INFO, "");

        mEditor0.commit();

        try {
            LeadFilterFragmentNew.leadstatusJsonArray = new JSONArray();
        } catch (Exception e) {

        }

    }

    @Override
    public void applySSFilter(String typeLead) {

        try {
            if (typeLead.equals("salesstock")) {
                applySSFilterApply.applySSFilter(typeLead);
            } else {
                applyWaranthyFilterApply.applySSFilter(typeLead);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //WebLeads

    public interface FragmentSortListener {
        void onSortWeb();
    }

    public FragmentSortListener getFragmentWebTriggerListener() {
        return fragmentSortListener;
    }

    public void setFragmentSortListener(FragmentSortListener fragmentSortListener) {
        this.fragmentSortListener = fragmentSortListener;
    }

    //PrivateLeads

    public interface FragmentPrivateSortListener {
        void onSortPrivate();
    }

    public FragmentPrivateSortListener getFragmentPriTriggerListener() {
        return fragmentPrivateSortListener;
    }

    public void setFragmentSortPrivateListener(FragmentPrivateSortListener fragmentPrivateSortListener) {
        this.fragmentPrivateSortListener = fragmentPrivateSortListener;
    }

    @Override
    public void onSortSelection() {
        try {
            getFragmentWebTriggerListener().onSortWeb();
            getFragmentPriTriggerListener().onSortPrivate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Search functionality in Lead Section

    SearchbyWebLeads searchbyWebLeads;

    public void updateSearchWebLead(SearchbyWebLeads listener) {
        searchbyWebLeads = listener;
    }

    SearchbyPrivateLeads searchbyPrivateLeads;

    public void updateSearchPrivateLead(SearchbyPrivateLeads listener) {
        searchbyPrivateLeads = listener;
    }


    //LazyLoader in WebLeads

    public void setLazyloaderWebLeadsListener(LazyloaderWebLeads lazyloaderWebLeads) {
        this.lazyloaderWebLeads = lazyloaderWebLeads;
    }

    public LazyloaderWebLeads getLazyloaderWebLeadsListener() {
        return lazyloaderWebLeads;
    }

    @Override
    public void loadmore(int ItemCount) {
        lazyloaderWebLeads.loadmore(ItemCount);

    }

    @Override
    public void updatecountweb(int count) {
        lazyloaderWebLeads.updatecountweb(count);
    }

    //LazyLoader in PrivateLeads

    public void setLazyloaderPrivateLeadsListener(LazyloaderPrivateLeads lazyloaderPrivateLeads) {
        this.lazyloaderPrivateLeads = lazyloaderPrivateLeads;
    }

    public LazyloaderPrivateLeads getLazyloaderPrivateLeadsListener() {
        return lazyloaderPrivateLeads;
    }

    @Override
    public void loadmorePrivate(int ItemCount) {
        lazyloaderPrivateLeads.loadmorePrivate(ItemCount);
    }

    @Override
    public void updatecountPrivate(int count) {
        lazyloaderPrivateLeads.updatecountPrivate(count);
    }

    //sales load

    @Override
    public void Salesloadmore(int ItemCount) {
        SSLazyloaderSalesStocks.Salesloadmore(ItemCount);
    }

    @Override
    public void SalesUpdatecount(int count) {

        SSLazyloaderSalesStocks.SalesUpdatecount(count);
    }


    // warranty load
    @Override
    public void loadmoreWarranty(int ItemCount) {
        SSLazyloaderWarranty.loadmoreWarranty(ItemCount);

    }

    @Override
    public void updatecountWarranty(int count) {
        SSLazyloaderWarranty.updatecountWarranty(count);
    }

    //ApplyFilter for Webleads


    public void setApplyWebFilterListener(FilterApply applyFilterWebLeads) {

        Log.i(TAG, "interface -2: ");

        this.applyFilterWebLeads = applyFilterWebLeads;
    }

    public FilterApply getapplywebFilterListener() {
        return applyFilterWebLeads;
    }

    //ApplyFilter for Privateleads

    public void setApplyPrivateFilterListener(FilterApply applyFilterPriLeads) {
        this.applyFilterPriLeads = applyFilterPriLeads;
    }

    public FilterApply getapplyprivateFilterListener() {
        return applyFilterPriLeads;
    }

    @Override
    public void applyFilter(String typeLead) {

        Log.i(TAG, "interface -3: ");

        if (typeLead.equals("WebLeads")) {
            applyFilterWebLeads.applyFilter(typeLead);
        } else {
            applyFilterPriLeads.applyFilter(typeLead);
        }

    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 10) {
            leadsreset();
            LeadsInstance.getInstance().setLeaddirection("PrivateLeadsPage");
            LeadsFragmentSection leadsFragment = new LeadsFragmentSection();
            this.fragmentLoad(leadsFragment);
        } else if (resultCode == 50) {
            leadsreset();
            LeadsInstance.getInstance().setLeaddirection("PrivateLeadsPage");
            ProcurementTAB leadsFragment = new ProcurementTAB();
            this.fragmentLoad(leadsFragment);
        }
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LPRConstants.REQUEST_CODE_LPR_MULTIPLE_SCAN) {
            Log.e("Onactivity called", "onActivity called");
            String value = readLPRFile(this, LPRConstants.REQUEST_CODE_LPR_MULTIPLE_SCAN);
            Log.e("LPR Data", "" + value);
            parseLPRData(value);
        }

    }

    private void saveTodayFollowupLeads() {
        LeadConstantsSection leadConstantsSection = new LeadConstantsSection();
        HashMap<String, String> followupTodayDate = new HashMap<String, String>();

        followupTodayDate.put(LeadConstantsSection.LEAD_FOLUP_TODAY, leadConstantsSection.getFollowtodayDate());
        LeadFilterSaveInstance.getInstance().setSavedatahashmap(followupTodayDate);

        SharedPreferences.Editor mEditor = mSharedPreferencesLead.edit();
        Gson gson = new Gson();
        String follupDate = gson.toJson(followupTodayDate);
        mEditor.putString(LeadConstantsSection.LEAD_SAVE_DATE, follupDate);
        mEditor.commit();
    }

    private void clearFilterLeads() {
        SortInstanceLeadSection.getInstance().setSortby("leads.created_at");
        SharedPreferences.Editor mEditor = mSharedPreferencesLead.edit();
        mEditor.putString(LeadConstantsSection.LEAD_SAVE_DATE, "");
        mEditor.putString(LeadConstantsSection.LEAD_SAVE_STATUS, "");
        LeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());
        LeadFilterSaveInstance.getInstance().setStatusarray(new JSONArray());
        mEditor.clear();
        mEditor.commit();
    }

    public static FragmentRefreshListener getFragmentRefreshListener() {
        return fragmentRefreshListener;
    }

    public void setFragmentRefreshListener(FragmentRefreshListener fragmentRefreshListener) {
        this.fragmentRefreshListener = fragmentRefreshListener;
    }

    public interface FragmentRefreshListener {
        void onRefresh();
    }

    private void SalesANDProc() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.sales_proc);
        dialog.setCancelable(false);

        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.BOTTOM | Gravity.CENTER;
        wmlp.x = 100;   //x position
        wmlp.y = 150;   //y position

        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        LinearLayout salesLeads = dialog.findViewById(R.id.salesLeads);
        LinearLayout procLeads = dialog.findViewById(R.id.procLeads);

        salesLeads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isProcurementUser()) {
                    return;
                }
               /* toolbar.setTitleTextColor(0xFFFFFFFF);
                toolbar.setTitle("Sales Lead");*/
                searchicon.setVisibility(View.VISIBLE);
                linearLayoutVisible.setVisibility(View.VISIBLE);
                if (tv_header_title != null) {
                    tv_header_title.setVisibility(View.VISIBLE);
                    tv_header_title.setText("Sales Leads");
                }
                searchClose();
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.sales_leads, GlobalText.android);

                LeadsFragmentSection leadsFragment = new LeadsFragmentSection();
                fragmentLoad(leadsFragment);
                dialog.dismiss();
            }
        });

        procLeads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isDealerCreUser())
                    return;

                if (isSalesUser())
                    return;

              /*  toolbar.setTitleTextColor(0xFFFFFFFF);
                toolbar.setTitle("Procurement Lead");*/
                searchicon.setVisibility(View.VISIBLE);
                linearLayoutVisible.setVisibility(View.VISIBLE);
                if (tv_header_title != null) {
                    tv_header_title.setVisibility(View.VISIBLE);
                    tv_header_title.setText("Procurement Leads");
                }
                searchClose();
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.proc_leads, GlobalText.android);
                ProcurementTAB leadsFragment = new ProcurementTAB();
                fragmentLoad(leadsFragment);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    // Procurement Section ==========================

    //Filter

    public void setPApplyWebFilterListener(PFilterApply applyPFilterWebLeads) {
        this.applyPFilterWebLeads = applyPFilterWebLeads;
    }

    //sales
    public void setSSFilterApplyFilterListener(SSFilterApply applySSFilterApply) {
        this.applySSFilterApply = applySSFilterApply;
    }

    public void setSSApplyWarrantyFilterListener(SSFilterApply applyWaranthyFilterApply) {
        this.applyWaranthyFilterApply = applyWaranthyFilterApply;
    }

    public PFilterApply getPapplywebFilterListener() {
        return applyPFilterWebLeads;
    }

    //ApplyFilter for Privateleads

    public void setPApplyPrivateFilterListener(PFilterApply applyPFilterPriLeads) {
        this.applyPFilterPriLeads = applyPFilterPriLeads;
    }

    public PFilterApply getPapplyprivateFilterListener() {
        return applyPFilterPriLeads;
    }

    @Override
    public void applyPFilter(String typeLead) {
        try {
            if (typeLead.equals("WebLeads")) {
                applyPFilterWebLeads.applyPFilter(typeLead);
            } else {
                applyPFilterPriLeads.applyPFilter(typeLead);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //WebLeads
    public interface PFragmentSortListener {
        void onSortPWeb();
    }

    public PFragmentSortListener getWebTriggerListener() {
        return PfragmentSortListener;
    }

    public void setPFragmentSortListener(PFragmentSortListener PfragmentSortListener) {
        this.PfragmentSortListener = PfragmentSortListener;
    }

    //PrivateLeads
    public interface FragmentPPrivateSortListener {
        void onSortPPrivate();
    }

    public FragmentPPrivateSortListener getPriTriggerListener() {
        return PPrivateSortListener;
    }

    public void setFragmentPPrivateSortListener(FragmentPPrivateSortListener PPrivateSortListener) {
        this.PPrivateSortListener = PPrivateSortListener;
    }

    @Override
    public void onPSortSelection() {
        try {
            getWebTriggerListener().onSortPWeb();
            getPriTriggerListener().onSortPPrivate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Search functionality in Lead Section

    PSearchWL searchWL;

    public void PupdateSearchWebLead(PSearchWL listener) {
        searchWL = listener;
    }

    //sales
    SSSearchSales searchSS;

    public void SSupdateSearchSales(SSSearchSales listener) {
        searchSS = listener;
    }

    //Warranty
    SSSearchWarranty searchWarranty;

    public void SSupdateSearchWarranty(SSSearchWarranty listener) {
        searchWarranty = listener;
    }

    PSearchPL searchPl;

    public void PupdateSearchPrivateLead(PSearchPL listener) {
        searchPl = listener;
    }

    //LazyLoader in WebLeads
    public void setPLazyloaderWebLeadsListener(PLazyloaderWebLeads PlazyloaderWebLeads) {
        this.PlazyloaderWebLeads = PlazyloaderWebLeads;
    }


    public PLazyloaderWebLeads getPLazyloaderWebLeadsListener() {
        return PlazyloaderWebLeads;
    }


    //LazyLoader in Sales Stock
    public void setSSLazyloaderSalesStocksListener(SSLazyloaderSalesStocks SSLazyloaderSalesStocks) {
        this.SSLazyloaderSalesStocks = SSLazyloaderSalesStocks;
    }

    public SSLazyloaderSalesStocks getPSSLazyloaderSalesStocksListener() {
        return SSLazyloaderSalesStocks;
    }

    // warranty sales

    //LazyLoader in Sales Stock
    public void setSSLazyloaderWarrantyListener(SSLazyloaderWarranty SSLazyloaderWarranty) {
        this.SSLazyloaderWarranty = SSLazyloaderWarranty;
    }

    public SSLazyloaderWarranty getSSLazyloaderWarrantyListener() {
        return SSLazyloaderWarranty;
    }

    @Override
    public void Ploadmore(int ItemCount) {
        PlazyloaderWebLeads.Ploadmore(ItemCount);
    }

    @Override
    public void Pupdatecountweb(int count) {
        PlazyloaderWebLeads.Pupdatecountweb(count);
    }

    //LazyLoader in PrivateLeads
    public void setPLazyloaderPrivateLeadsListener(PLazyloaderPrivateLeads PlazyloaderPrivateLeads) {
        this.PlazyloaderPrivateLeads = PlazyloaderPrivateLeads;
    }

    public PLazyloaderPrivateLeads getPLazyloaderPrivateLeadsListener() {
        return PlazyloaderPrivateLeads;
    }

    @Override
    public void PloadmorePrivate(int ItemCount) {
        PlazyloaderPrivateLeads.PloadmorePrivate(ItemCount);
    }

    @Override
    public void PupdatecountPrivate(int count) {
        PlazyloaderPrivateLeads.PupdatecountPrivate(count);
    }

    //Refresh Procurement Leads

    public static ProcFragmentRefreshListener getProcFragmentRefreshListener() {
        return ProcRefreshListener;
    }

    public void setProcFragmentRefreshListener(ProcFragmentRefreshListener fragmentRefreshListener) {
        this.ProcRefreshListener = fragmentRefreshListener;
    }


    public static SSFragmentRefreshListener getSSFragmentRefreshListener() {
        return SSRefreshListener;
    }

    public void setSSFragmentRefreshListener(SSFragmentRefreshListener SSFragmentRefreshListener) {
        this.SSRefreshListener = SSFragmentRefreshListener;
    }

    public interface ProcFragmentRefreshListener {
        void onRefresh();
    }

    public interface SSFragmentRefreshListener {
        void onRefresh();
    }

    // Retrofit
    public void GetLeadStatus() {

        LeadStatusServices.getLeadStatus(new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                try {
                    if (!statusList.isEmpty()) {
                        statusList.clear();
                    }

                    Response<LeadstatusResponse> mRes = (Response<LeadstatusResponse>) obj;
                    LeadstatusResponse mData = mRes.body();
                    if (mData.getStatus().equalsIgnoreCase("SUCCESS")) {
                        leadstatus = mData.getLeadstatus();
                        for (int i = 0; i < leadstatus.size(); i++) {
                            statusList.add(leadstatus.get(i).getStatus());
                        }
                        //   Log.e("statusList", "" + statusList.size());
                    }

                } catch (Exception e) {

                }
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                leadstatus = new ArrayList<>();
            }
        });
    }

    public void fragments_Swtich_page() {

        try {
            nameObserver1 = dash -> {
                switch (dash) {
                    case "MessageCenter":

                        if (searchicon != null) {
                            linearLayoutVisible.setVisibility(View.GONE);
                            searchicon.setVisibility(View.VISIBLE);
                        }

                        if (tv_header_title != null) {
                            tv_header_title.setVisibility(View.VISIBLE);
                            tv_header_title.setText(CommonMethods.getstringvaluefromkey(activity, "dealerName"));
                        }

                        messageFragmentsSwtich();

                        break;

                    case "SalesDashboard":

                        if (tv_header_title != null) {
                            tv_header_title.setVisibility(View.VISIBLE);
                            tv_header_title.setText(CommonMethods.getstringvaluefromkey(activity, "dealerName"));
                        }

                        fragment = new ASMSalesFragment();
                        navigation.getMenu().findItem(R.id.navigation_sales).setChecked(true);
                        fragmentLoad_s(fragment);

                        break;

                    case "StockList":

                        fragment = new StockFragment();
                        navigation.getMenu().findItem(R.id.navigation_stock).setChecked(true);
                        fragmentLoad_s(fragment);

                        break;

                }
            };
            dashboardSwtich.getdashboardSwtich().observe(this, nameObserver1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void searchVisble(String title) {

        if (tv_header_title != null) {

            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText(title);
        }

        if (searchicon != null) {
            linearLayoutVisible.setVisibility(View.VISIBLE);
            searchicon.setVisibility(View.VISIBLE);
        }
    }

    public void messageFragmentsSwtich() {

        //searchicon.setVisibility(View.INVISIBLE);
        navigationView.setVisibility(View.VISIBLE);
        navigation.setVisibility(View.GONE);

        MessageCenter dashboard = new MessageCenter();
        this.fragmentLoad(dashboard);


    }

    public void searchbtmHide() {

        if (searchicon != null) {
            linearLayoutVisible.setVisibility(View.GONE);
            searchicon.setVisibility(View.VISIBLE);
        }

        if (tv_header_title != null) {
            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText(CommonMethods.getstringvaluefromkey(activity, "dealerName"));
        }
    }

    @Override
    protected void onPause() {

        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        // dashboardSwtich.getdashboardSwtich().removeObserver(nameObserver1);

        super.onPause();
    }

    private boolean loadFragmentHome(Fragment fragment) {
        //switching fragment

        if (tv_header_title != null) {

            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText("Dashboard");
        }


        if (fragment != null) {
            this.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fillLayout, fragment)
                    .commit();

            return true;
        }
        return false;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        /*searchicon.setVisibility(View.VISIBLE);
        tv_header_title.setVisibility(View.VISIBLE);
        tv_header_title.setText("Dashboard");
        searchView.setVisibility(View.VISIBLE);*/
    }


    public void writeAFile() {
        String fileName = "myFile.txt";
        String textToWrite = "This is some text!";
        FileOutputStream outputStream;

        try {
            outputStream = openFileOutput(fileName, Context.MODE_PRIVATE);
            outputStream.write(textToWrite.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setOverflowButtonColor(final Toolbar toolbar, final int color) {
        Drawable drawable = toolbar.getOverflowIcon();
        if (drawable != null) {
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable.mutate(), color);
            toolbar.setOverflowIcon(drawable);
        }
    }


    public void ReadBtn() {
        //reading text from file
        try {
            FileInputStream fileIn = openFileInput("myFile.txt");
            InputStreamReader InputRead = new InputStreamReader(fileIn);

            char[] inputBuffer = new char[1024 * 1024];
            String s = "";
            int charRead;

            while ((charRead = InputRead.read(inputBuffer)) > 0) {
                String readstring = String.copyValueOf(inputBuffer, 0, charRead);
                s += readstring;
            }
            InputRead.close();

            Log.i(TAG, "ReadBtn: " + s);

            // textmsg.setText(s);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void setMainHeading(String heading) {
        if (tv_header_title != null) {
            tv_header_title.setText(heading);
            tv_header_title.setVisibility(View.VISIBLE);
        }
    }


    private void parseLPRData(String value) {
        try {
            Type empTypeList = new TypeToken<ArrayList<LPRResponse>>() {
            }.getType();
            List<LPRResponse> mData = new Gson().fromJson(value, empTypeList);
            if (mData != null && mData.size() > 0) {
                Log.i("LPR", "Result: ");
                for (int i = 0; i < mData.size(); i++) {
                    LPRResponse response = mData.get(i);
                    String outputValue = response.getOutputValue();
                    if (outputValue.toLowerCase().equals("unreg/na"))
                        outputValue = "";
                    mSqlAdapterForDML.insertLPRItem(response);
                }

            }
            if (mSqlAdapterForDML.getLPRListCount() > 0) {
                showStockListingScreen();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showStockListingScreen() {
        Intent intent = new Intent(MainActivity.this, StockActivity.class);
        startActivityForResult(intent, LPRConstants.REQUEST_CODE_LPR_STOCK_LIST);
    }

    public String readLPRFile(Context context, int type) { // reading file..for single scan..jSON filw is deleted after reading image..
        String value = "";
        final String lprDir, lprFileName;
        lprDir = preferenceManager.readString(LPRConstants.LPR_DIR, "");
        lprFileName = preferenceManager.readString(LPRConstants.LPR_FILENAME + type, "");
        try {

            int fileCount = 0;
            File directory;
            File[] allFiles;

            FilenameFilter fileFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return (name.startsWith(lprFileName) && name.endsWith(".json"));
                }
            };

            directory = new File(lprDir + "/" + lprFileName);
            allFiles = directory.listFiles(fileFilter);
            Log.e("LPR", "file lenght: " + allFiles.length);
            if (allFiles.length == 1) {

                File file = allFiles[0];/*LPR required file..*/
                if (file.exists()) {
                    FileInputStream fis = new FileInputStream(file);
                    DataInputStream in = new DataInputStream(fis);
                    BufferedReader br = new BufferedReader(new InputStreamReader(in));
                    String strLine;
                    while ((strLine = br.readLine()) != null) {
                        value = value + strLine;
                    }
                    in.close();
                    if (type == LPRConstants.REQUEST_CODE_LPR_SINGLE_SCAN && file.delete()) {
                        fileCount++;
                        Log.e("LPR", "file Deleted");
                    } else {
                        Log.e("LPR", "file not Deleted");
                    }
                }
            } else {
                for (File f : allFiles) {
                    if (f.delete()) {
                        fileCount++;
                        Log.e("LPR", "file Deleted");
                    } else {
                        Log.e("LPR", "file not Deleted");
                    }
                }
            }
            Log.e("LPR", "deleted file count: " + fileCount);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return value;
    }


    private void showCovidForm() {
        Intent covidform = new Intent(MainActivity.this, CovidDeclarationFrom.class);
        startActivity(covidform);
    }


    public void showDialogOnceDay() {
        String currentDate = preferenceManager.readString(AppConstants.DATE_KEY, "");
        if (currentDate == null || currentDate.equalsIgnoreCase("")) {
            RDRWarningDialog mDialog = new RDRWarningDialog(activity);
            mDialog.setCancelable(false);
            mDialog.show();
            preferenceManager.writeString(AppConstants.DATE_KEY, MonthUtility.getCurrentDate());
        } else {
            if (!currentDate.equals(MonthUtility.getCurrentDate())) {
                RDRWarningDialog mDialog = new RDRWarningDialog(activity);
                mDialog.setCancelable(false);
                mDialog.show();
                preferenceManager.writeString(AppConstants.DATE_KEY, MonthUtility.getCurrentDate());
            }
        }


    }


    public void switchContent(int id, Fragment fragment) {
        if (CommonMethods.getstringvaluefromkey(this, "user_type").equalsIgnoreCase(USERTYPE_ASM)) {

            toggle.setDrawerIndicatorEnabled(false);
            toggle.setHomeAsUpIndicator(R.drawable.ic_new_hamburger_menu);
            toolbar.setBackgroundColor(getResources().getColor(R.color.all_dealers_fragment_bg));
            tv_header_title.setTextColor(getResources().getColor(R.color.black));
            MenuItem item = menu.findItem(R.id.asm_mail);
            item.setVisible(false);
            MenuItem item1 = menu.findItem(R.id.notification_bell);
            searchicon.setVisibility(View.VISIBLE);
            searchImage.setImageResource(0);
            item1.setVisible(true);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(id, fragment, fragment.toString());
            ft.addToBackStack(null);
            ft.commit();
        } else {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(id, fragment, fragment.toString());
            ft.addToBackStack(null);
            ft.commit();
        }

    }


}
