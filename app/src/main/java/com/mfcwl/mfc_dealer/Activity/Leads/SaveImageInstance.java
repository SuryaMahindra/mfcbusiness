package com.mfcwl.mfc_dealer.Activity.Leads;

import android.graphics.Bitmap;

/**
 * Created by sowmya on 6/4/18.
 */

public class SaveImageInstance {

    private static SaveImageInstance mInstance = null;

    private Bitmap coverimage;
    private Bitmap frontview;
    private Bitmap rearview;
    private Bitmap leftsideview;
    private Bitmap rightsideview;
    private Bitmap rearleft;
    private Bitmap frontright;
    private Bitmap rearright;
    private Bitmap wheel;
    private Bitmap frontseats;
    private Bitmap backseats;
    private Bitmap odometer;
    private Bitmap dashboard;
    private Bitmap steering;
    private Bitmap frontrowfromside;
    private Bitmap backrowfromside;
    private Bitmap trunkdooropenview;



    //Interior

    public static SaveImageInstance getInstance(){
        if(mInstance == null)
        {
            mInstance = new SaveImageInstance();
        }
        return mInstance;
    }

    private SaveImageInstance(){
        coverimage = null;
        frontview =null;
        rearview =null;
        leftsideview =null;
        rightsideview =null;
        rearleft =null;
        frontright =null;
        rearright =null;
        wheel =null;
        frontseats =null;
        backseats =null;
        odometer =null;
        dashboard =null;
        steering =null;
        frontrowfromside =null;
        backrowfromside =null;
        trunkdooropenview =null;
    }

    public Bitmap getCoverimage() {
        return coverimage;
    }

    public void setCoverimage(Bitmap coverimage) {
        this.coverimage = coverimage;
    }

    public Bitmap getFrontview() {
        return frontview;
    }

    public void setFrontview(Bitmap frontview) {
        this.frontview = frontview;
    }

    public Bitmap getRearview() {
        return rearview;
    }

    public void setRearview(Bitmap rearview) {
        this.rearview = rearview;
    }

    public Bitmap getLeftsideview() {
        return leftsideview;
    }

    public void setLeftsideview(Bitmap leftsideview) {
        this.leftsideview = leftsideview;
    }

    public Bitmap getRightsideview() {
        return rightsideview;
    }

    public void setRightsideview(Bitmap rightsideview) {
        this.rightsideview = rightsideview;
    }

    public Bitmap getRearleft() {
        return rearleft;
    }

    public void setRearleft(Bitmap rearleft) {
        this.rearleft = rearleft;
    }

    public Bitmap getFrontright() {
        return frontright;
    }

    public void setFrontright(Bitmap frontright) {
        this.frontright = frontright;
    }

    public Bitmap getRearright() {
        return rearright;
    }

    public void setRearright(Bitmap rearright) {
        this.rearright = rearright;
    }

    public Bitmap getWheel() {
        return wheel;
    }

    public void setWheel(Bitmap wheel) {
        this.wheel = wheel;
    }

    public Bitmap getFrontseats() {
        return frontseats;
    }

    public void setFrontseats(Bitmap frontseats) {
        this.frontseats = frontseats;
    }

    public Bitmap getBackseats() {
        return backseats;
    }

    public void setBackseats(Bitmap backseats) {
        this.backseats = backseats;
    }

    public Bitmap getOdometer() {
        return odometer;
    }

    public void setOdometer(Bitmap odometer) {
        this.odometer = odometer;
    }

    public Bitmap getDashboard() {
        return dashboard;
    }

    public void setDashboard(Bitmap dashboard) {
        this.dashboard = dashboard;
    }

    public Bitmap getSteering() {
        return steering;
    }

    public void setSteering(Bitmap steering) {
        this.steering = steering;
    }

    public Bitmap getFrontrowfromside() {
        return frontrowfromside;
    }

    public void setFrontrowfromside(Bitmap frontrowfromside) {
        this.frontrowfromside = frontrowfromside;
    }

    public Bitmap getBackrowfromside() {
        return backrowfromside;
    }

    public void setBackrowfromside(Bitmap backrowfromside) {
        this.backrowfromside = backrowfromside;
    }

    public Bitmap getTrunkdooropenview() {
        return trunkdooropenview;
    }

    public void setTrunkdooropenview(Bitmap trunkdooropenview) {
        this.trunkdooropenview = trunkdooropenview;
    }
}
