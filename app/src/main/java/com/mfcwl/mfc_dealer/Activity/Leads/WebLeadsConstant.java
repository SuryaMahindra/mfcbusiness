package com.mfcwl.mfc_dealer.Activity.Leads;

public class WebLeadsConstant {

    private static WebLeadsConstant mInstance = null;

    public String getWhichleads() {
        return whichleads;
    }

    public void setWhichleads(String whichleads) {
        this.whichleads = whichleads;
    }

    private String whichleads;

    private WebLeadsConstant() {
        whichleads = "";

    }

    public static WebLeadsConstant getInstance() {
        if (mInstance == null) {
            mInstance = new WebLeadsConstant();
        }
        return mInstance;
    }
}
