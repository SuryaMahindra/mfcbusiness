package com.mfcwl.mfc_dealer.Activity.RDR;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GAConstants;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.DiscussionPoint;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.ScreenInformation;

import java.util.Arrays;

import imageeditor.base.BaseActivity;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.activity;

public class DiscussionPoint5 extends BaseActivity {

    TextView tvDateTime, tvDealerName, tvFocusArea, tvSubFA;
    ImageView createRepo_backbtn;
    TextView tvMeetingSetup, tvCommercialClosed, tvVehicleProcured;
    TextView tvActionItems, tvResponsibilities, tvTargetDateCalendar, tvPrevious, tvNext, tvFooterHeading;

    EditText etMeetingSetUp, etCommercialsClosed, etVehicleProcured;
    String strMeetingSetUp = "", strCommercialsClosed = "", strVehicleProcured = "";

    private Activitykiller mKiller;
    private LinearLayout llresandtarget;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.discussion_point5);
        Log.i(TAG, "onCreate: ");

        initView();

        setListeners();

        mKiller = new Activitykiller();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(mKiller,intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       unregisterReceiver(mKiller);
    }

    public void initView() {

        createRepo_backbtn = findViewById(R.id.createRepo_backbtn);

        tvDateTime = findViewById(R.id.tvDateTime);
        tvDealerName = findViewById(R.id.tvDealer);
        tvFocusArea = findViewById(R.id.tvFocusArea);
        tvSubFA = findViewById(R.id.tvSubFA);

        tvMeetingSetup = findViewById(R.id.tvMeetingSetUp);
        tvCommercialClosed = findViewById(R.id.tvCommercialClosed);
        tvVehicleProcured = findViewById(R.id.tvVehicleProcured);

        tvActionItems = findViewById(R.id.tvActionItemList);
        tvResponsibilities = findViewById(R.id.tvResponsibilityList);
        tvTargetDateCalendar = findViewById(R.id.tvTargetDateCalendar);

        tvPrevious = findViewById(R.id.tvPrevious);
        tvNext = findViewById(R.id.tvNext);
        tvFooterHeading = findViewById(R.id.tvFooterHeading);

        etMeetingSetUp = findViewById(R.id.etMeetingSetUp);
        etCommercialsClosed = findViewById(R.id.etCommercialsClosed);
        etVehicleProcured = findViewById(R.id.etVehicleProcured);
        llresandtarget = findViewById(R.id.llresandtarget);

        tvActionItems.setMovementMethod(new ScrollingMovementMethod());
        tvResponsibilities.setMovementMethod(new ScrollingMovementMethod());

    }

    public void setListeners() {

        tvPrevious.setOnClickListener(this);
        tvNext.setOnClickListener(this);
        tvActionItems.setOnClickListener(this);
        tvResponsibilities.setOnClickListener(this);
        tvTargetDateCalendar.setOnClickListener(this);
        createRepo_backbtn.setOnClickListener(this);

        tvDateTime.setText(getDateTime());
        tvDealerName.setText(dealerName + "(" + dealerId + ")");
        tvFocusArea.setText("Procurement");
        tvSubFA.setText("Local Partnership");
        tvFooterHeading.setText("Discussion point 5");

        /*tvMeetingSetup.setText("Meeting setup: " + dealerReportResponse.meetings);
        tvCommercialClosed.setText("Commercials closed: " + dealerReportResponse.commercialClosed);
        tvVehicleProcured.setText("Vehicle procured through partnership: " + dealerReportResponse.vehiclesProcuredPartnership);*/


        /*if(dealerReportResponse.meetings!=null)
        {
            etMeetingSetUp.setText(dealerReportResponse.meetings);
        }
        else
        {
            etMeetingSetUp.setText("");
        }

        if(dealerReportResponse.commercialClosed!=null)
        {
            etCommercialsClosed.setText(dealerReportResponse.commercialClosed);
        }
        else
        {
            etCommercialsClosed.setText("");
        }

        if(dealerReportResponse.vehiclesProcuredPartnership!=null)
        {
            etVehicleProcured.setText(dealerReportResponse.vehiclesProcuredPartnership);
        }
        else
        {
            etVehicleProcured.setText("");
        }
*/



        tvActionItems.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String data = s.toString();
                if(data.equalsIgnoreCase("No action required")){
                    llresandtarget.setVisibility(View.GONE);
                    strResponsibility = "NA";
                    strTargetDate = "07/17/2020";
                    Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_ACTION_ITEM, CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));

                }else {
                    llresandtarget.setVisibility(View.VISIBLE);
                    strResponsibility = "";
                    strTargetDate = "";
                    Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_ACTION_ITEM,CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));
                }
            }
        });
    }

    public DiscussionPoint getDiscussionPoint() {
        DiscussionPoint discussionPoint = new DiscussionPoint();
        discussionPoint.setActionItem(Arrays.asList(strActionItem.split(("\\|"))));
        discussionPoint.setResponsibility(strResponsibility);
        discussionPoint.setTargetDate(strTargetDate);
        discussionPoint.setFocusArea(tvFocusArea.getText().toString());
        discussionPoint.setSubFocusArea(tvSubFA.getText().toString());

        ScreenInformation screenFiveInformation = new ScreenInformation();
        screenFiveInformation.setMeetings(strMeetingSetUp);
        screenFiveInformation.setCommercialClosed(strCommercialsClosed);
        screenFiveInformation.setVehiclesProcuredPartnership(strVehicleProcured);

        discussionPoint.setScreenInformation(screenFiveInformation);

        return discussionPoint;
    }

    private boolean validate() {

        strActionItem = tvActionItems.getText().toString();
        if(!strActionItem.equalsIgnoreCase("No action required")){
            strResponsibility = tvResponsibilities.getText().toString();
            strTargetDate = tvTargetDateCalendar.getText().toString();
        }

        strMeetingSetUp = etMeetingSetUp.getText().toString();
        strCommercialsClosed = etCommercialsClosed.getText().toString();
        strVehicleProcured = etVehicleProcured.getText().toString();

        boolean allSelected = true;

        if (strActionItem.equals("") ||
                strResponsibility.equals("") ||
                strTargetDate.equals("") ||
                strMeetingSetUp.equals("") ||
                strCommercialsClosed.equals("") ||
                strVehicleProcured.equals("")) {
            allSelected = false;
            Toast.makeText(this, "Please select all fields", Toast.LENGTH_SHORT).show();
        }

        return allSelected;
    }

    @Override
    public void onBackPressed() {

    }
    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.createRepo_backbtn:
                startActivity(new Intent(this, DiscussionPoint4.class));
                break;
            case R.id.tvPrevious:
                startActivity(new Intent(this,DiscussionPoint4.class));
                break;
            case R.id.tvNext:
                if (validate()) {
                    removeAnyDuplicates(tvSubFA.getText().toString());
                    discussionPoints.add(getDiscussionPoint());
                    startActivity(new Intent(this, DiscussionPoint6.class));
                }
                break;

            case R.id.tvActionItemList:
                if (dealerReportResponse != null && dealerReportResponse.localPartnership != null) {
                    setDialogMultiSelectionCheck(R.id.tvActionItemList, "Action Items", getStringArray(dealerReportResponse.localPartnership));
                }else {
                    Toast.makeText(DiscussionPoint5.this,"No data found",Toast.LENGTH_LONG).show();
                }
                break;
        }
    }



}