package com.mfcwl.mfc_dealer.Activity.ASM_Reports;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.ASMAdapter.ASMReportAdapter.SRLastTtyDaysAdapter;
import com.mfcwl.mfc_dealer.ASMAdapter.ASMReportAdapter.SubmittedRptAdapter;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.DVReportFetchModel;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.SRLastTtyDaysModel;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.SubmittedDVReport;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.SubmittedReportModel;
import com.mfcwl.mfc_dealer.ASMReportsServices.DvReport;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.AddStockModel.dvrPointData;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.Helper;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;

import retrofit2.Response;

public class SubmittedDVR extends AppCompatActivity {

    static SubmittedReportModel submittedReportModel = new SubmittedReportModel();
    static SRLastTtyDaysModel srLastTtyDaysModel = new SRLastTtyDaysModel();
    static RecyclerView recyclerView;
    static SubmittedRptAdapter adapter;
    static SRLastTtyDaysAdapter srAdapter;
    static TextView dealerCategorySelect, dateText;
    int count = 10;

    static String dealerCatVal = "select", dealer_name;

    static String dateFormat = "YYYY-MM-DD";

    static LinearLayout customLayout;
    static TextView tv_Last7Days, tv_Last30Days, tv_Custom;
    static ImageView backBtn;
    static List<SubmittedDVReport> data;
    static ArrayList<SubmittedReportModel> list = new ArrayList<SubmittedReportModel>();
    static LinearLayoutManager layoutManager;
    // TextView btn;

    static Calendar Calendar;
    static ProgressBar pgs;
    static String date1;

    final static Calendar myCalendar = Calendar.getInstance();
    static String curdate, prevdate;
    private static TextView fromdate, todate;
    static TextView notitxt;

    static Context mcontext , c;

    private static boolean TO = false, FROM = false, SELECTED;

    private void setAlertDialog(View v, Activity a, final String strTitle, final String[] arrVal, final String[] codeVal) {

        AlertDialog.Builder alert = new AlertDialog.Builder(a);
        alert.setTitle(strTitle);
        dealerCatVal = "select";
        alert.setItems(arrVal, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dealerCategorySelect.setText(arrVal[which]);
                dealer_name = arrVal[which];
                dealerCatVal = codeVal[which];
                Log.i("dealercode", dealerCatVal);
                SELECTED = true;
                //todate.setText(dateFormat);
                //fromdate.setText(dateFormat);

                Log.i("CurrentDate", curdate);
                Log.i("prevdateDate", prevdate);

                //pgs.setVisibility(View.VISIBLE);
                count = 10;

                initdata();

            }
        });
        customLayout.setVisibility(View.GONE);

        alert.create();
        alert.show();

    }

    public void initdata(){

        recyclerView.setVisibility(View.VISIBLE);
        tv_Last7Days.setTextColor(getResources().getColor(R.color.orange));
        tv_Last30Days.setTextColor(getResources().getColor(R.color.white));
        tv_Custom.setTextColor(getResources().getColor(R.color.white));
        prevdate = getCalculatedDate(-6);
        notitxt.setVisibility(View.GONE);
        try {
            fetchdata(dealerCatVal, prevdate, curdate);
            setRecyclerView();
        } catch (Exception e) {

        }
    }
    public static void fetchdata(String dealercode, String fromDate, String todate) {
        notitxt.setVisibility(View.GONE);

        if (SELECTED && !dealerCatVal.equalsIgnoreCase("select"))
            SpinnerManager.showSpinner(c);

        DVReportFetchModel dvReportFetchModel = new DVReportFetchModel(dealercode, fromDate, todate);
        DvReport dvReport = new DvReport();
        dvReport.fetchDVInfo(dvReportFetchModel, new HttpCallResponse() {
            @SuppressLint("NewApi")
            @Override
            public void OnSuccess(Object obj) {

                notitxt.setVisibility(View.GONE);
                Response<List<SubmittedDVReport>> res = (Response<List<SubmittedDVReport>>) obj;
                Log.i("successmsg", "data" + obj.toString());
                data = res.body();
                Log.i("dvrdata", " " + data);
                list.clear();
                try {
                    if (!data.toString().equalsIgnoreCase("[]")) {
                        recyclerView.setVisibility(View.VISIBLE);
//                        pgs.setVisibility(View.GONE);
                        SpinnerManager.hideSpinner(c);
                        for (SubmittedDVReport svr : data) {

                            String a = svr.getCreated_on();
                            String cre = a;
                            String sort_id_str = a.replace("-", "");


                            sort_id_str = sort_id_str.substring(0, 8);
                            int sort_id = Integer.parseInt(sort_id_str);

                            a = a.replace("T", " | ");
                            a = a.substring(0, 18);
                            String b = a.substring(0, 10);
                            String[] values = b.split("-");
                            int day = Integer.parseInt(values[2]);
                            int month = Integer.parseInt(values[1]);
                            int year = Integer.parseInt(values[0]);

                            myCalendar.set(year, month - 1, day - 1);
                            int dayof = myCalendar.get(myCalendar.DAY_OF_WEEK);
                            Log.i(" dayofweek", "OnSuccess: " + myCalendar.DAY_OF_WEEK);
                            Log.i(" dayofweek", "OnSuccess: " + dayof);
                            String dayofweek = getDayOfWEek(dayof);
                            Log.i(" dayofweek", "OnSuccess: " + dayofweek);
                            a = dayofweek + " | " + a;

                            List<dvrPointData> pp =svr.getPoints();

                            list.add(new SubmittedReportModel(pp,svr.getFollow_up_date(),String.valueOf(svr.getId()), dealer_name, svr.getDealer_code(), a,svr.getCreated_on(), svr.getDealer_code(),  svr.getLatitude(), svr.getLongitude(), sort_id,cre));
                        }
                        Comparator<SubmittedReportModel> compareById = (SubmittedReportModel o1, SubmittedReportModel o2) -> Integer.compare(o1.getSort_id(), o2.getSort_id());

                        Comparator<SubmittedReportModel> compareByday = (SubmittedReportModel o1, SubmittedReportModel o2) -> o1.getDaytv().compareTo(o2.getDaytv());
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                        Collections.sort(list, compareByday.reversed());
                        Collections.sort(list, compareById.reversed());
                        }


                        setRecyclerView();
                        return;
                    } else {
                        SpinnerManager.hideSpinner(c);
//                        list.clear();
                        list.add(new SubmittedReportModel());
                        setRecyclerView();
                        recyclerView.setVisibility(View.GONE);
                        notitxt.setVisibility(View.VISIBLE);
                        notitxt.setText("No data");
//                        Toast.makeText(mcontext,"No data",Toast.LENGTH_SHORT).show();
                        //fetchdata(dealerCatVal, fromDate, todate);

                    }
                } catch (Exception e) {
                    // fetchdata(dealerCatVal, prevdate, curdate);
//                    pgs.setVisibility(View.GONE);
                    SpinnerManager.hideSpinner(c);
                    e.printStackTrace();
//                    dealerCategorySelect.setText(dealer_name);
//                    list.clear();
                    list.add(new SubmittedReportModel());
                    notitxt.setVisibility(View.GONE);
                    setRecyclerView();
                    Toast.makeText(mcontext, "No data", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                //pgs.setVisibility(View.GONE);
//                SpinnerManager.hideSpinner(c);
                dealerCategorySelect.setText(dealer_name);
                Toast.makeText(mcontext, "NO data", Toast.LENGTH_SHORT).show();
//                list.clear();
                list.add(new SubmittedReportModel());
                setRecyclerView();
                Log.i("failuremsg", "fail" + mThrowable);

            }
        });
    }

    public static String getDayOfWEek(int a) {
        String str;
        switch (a) {
            case 1:
                str = "Mon";
                break;
            case 2:
                str = "Tue";
                break;
            case 3:
                str = "Wed";
                break;
            case 4:
                str = "Thu";
                break;
            case 5:
                str = "Fri";
                break;
            case 6:
                str = "Sat";
                break;
            case 7:
                str = "Sun";
                break;
            default:
                str = "mon";
        }
        return str;
    }


    public String getCalculatedDate(int days) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        cal.add(Calendar.DAY_OF_YEAR, days);
        return s.format(new Date(cal.getTimeInMillis()));
    }
    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate: ");
        //btn.setVisibility(View.GONE);
        mcontext = getApplicationContext();
        list.clear();
        SimpleDateFormat sdf = new SimpleDateFormat("EEE", Locale.getDefault());
        Date d = new Date();
        String dayOfTheWeek = sdf.format(d);
        Log.i("dayofweek", "onCreate: " + dayOfTheWeek);
        setContentView(R.layout.activity_submitted_reports);
        backBtn = findViewById(R.id.createRepo_backbtn);
        dealerCatVal = "select";
        LinkedHashSet<String> lnkHash = new LinkedHashSet<>(Helper.dealerName);
        ArrayList<String> dealerName = new ArrayList<String>(lnkHash);

        LinkedHashSet<String> hashset = new LinkedHashSet<>(Helper.dealerCode);
        ArrayList<String> dealerCode = new ArrayList<String>(hashset);
        //DvReportModel[] dvReportModels = new DvReportModel[];
        curdate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        prevdate = getCalculatedDate(-6);
        pgs = findViewById(R.id.progressBar2);
        recyclerView = (RecyclerView) findViewById(R.id.srrecyclerview);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        adapter = new SubmittedRptAdapter(getApplicationContext(), list, SubmittedDVR.this);
        todate = findViewById(R.id.todate);
        //todate.setEnabled(false);
        todate.setText(curdate.toString());
        c = SubmittedDVR.this;

        String abb = curdate.substring(0, 8) + "01";
        Log.i("datenow", "onCreate: " + abb);

        String datenow;
        fromdate = findViewById(R.id.fdate);
        notitxt = findViewById(R.id.notificationText);
        fromdate.setText(abb.toString());
        customLayout = findViewById(R.id.customDate_layOut);
        customLayout.setVisibility(View.GONE);
        SELECTED = false;

        //btn = findViewById(R.id.fetchbutton);


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SubmittedDVR.this, MainActivity.class);
                startActivity(intent);
            }
        });

        dealerCategorySelect = findViewById(R.id.select_dealer);
        dealerCategorySelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] namesArr = dealerName.toArray(new String[dealerName.size()]);

                String[] codeArr = dealerCode.toArray(new String[dealerCode.size()]);

                setAlertDialog(v, SubmittedDVR.this, "Dealer Category", namesArr, codeArr);

                customLayout.setVisibility(View.GONE);

                /*prevdate = getCalculatedDate(-6);
                fetchdata(dealerCatVal, prevdate, curdate);
                setRecyclerView();*/
            }
        });


        tv_Last7Days = findViewById(R.id.lastsevdaystv);
        tv_Last7Days.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                if (!SELECTED) {
                                                    Toast.makeText(getApplicationContext(), "Please select the dealer", Toast.LENGTH_LONG).show();
                                                } else {
//                                                        tv_Last7Days.setTextColor(getResources().getColor(R.color.Red));

                                                    tv_Last7Days.setTextColor(getResources().getColor(R.color.orange));
                                                    tv_Last30Days.setTextColor(getResources().getColor(R.color.white));
                                                    tv_Custom.setTextColor(getResources().getColor(R.color.white));
                                                }
                                                customLayout.setVisibility(View.GONE);
                                                prevdate = getCalculatedDate(-6);
                                                try {
                                                    fetchdata(dealerCatVal, prevdate, curdate);
                                                } catch (Exception e) {

                                                }
                                                ArrayList<SubmittedReportModel> lists = submittedReportModel.submittedReportModels();
                                                recyclerView = (RecyclerView) findViewById(R.id.srrecyclerview);
                                                LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                                                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                                                recyclerView.setLayoutManager(layoutManager);
                                                adapter = new SubmittedRptAdapter(getApplicationContext(), list, c);
                                                recyclerView.setAdapter(adapter);

                                            }
                                        }
        );

        tv_Last30Days = findViewById(R.id.lastthirtydystv);

        tv_Last30Days.setOnClickListener(new View.OnClickListener() {
                                             @Override
                                             public void onClick(View v) {

                                                 if (!SELECTED) {
                                                     Toast.makeText(getApplicationContext(), "Please select the dealer", Toast.LENGTH_LONG).show();
                                                 } else {

                                                     tv_Last30Days.setTextColor(getResources().getColor(R.color.orange));
                                                     tv_Last7Days.setTextColor(getResources().getColor(R.color.white));
                                                     tv_Custom.setTextColor(getResources().getColor(R.color.white));
                                                 }

                                                 /*try {
                                                     fetchdata(dealerCatVal, prevdate, curdate);
                                                 } catch (Exception e) {

                                                 }*/


                                                 customLayout.setVisibility(View.GONE);
                                                 prevdate = getCalculatedDate(-29);
                                                 fetchdata(dealerCatVal, prevdate, curdate);
                                                 ArrayList<SubmittedReportModel> lists = submittedReportModel.submittedReportModels();
                                                 recyclerView = (RecyclerView) findViewById(R.id.srrecyclerview);
                                                 LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                                                 layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                                                 recyclerView.setLayoutManager(layoutManager);
                                                 adapter = new SubmittedRptAdapter(getApplicationContext(), list, c);
                                                 recyclerView.setAdapter(adapter);
                                                 recyclerView.setOnClickListener(new View.OnClickListener() {
                                                     @Override
                                                     public void onClick(View v) {

                                                     }
                                                 });
                                             }
                                         }
        );
        tv_Custom = findViewById(R.id.customswrtv);

        tv_Custom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SELECTED) {
                    Toast.makeText(getApplicationContext(), "Please select the dealer", Toast.LENGTH_LONG).show();
                } else {

                    list.clear();

//                date1 = curdate;
//                date1 = todate.getText().toString();

//                fetchdata(dealerCatVal, prevdate, date1);

                    String datefrom = fromdate.getText().toString();
                    String dateto = todate.getText().toString();
                    fetchdata(dealerCatVal, datefrom, dateto);
                    tv_Last30Days.setTextColor(getResources().getColor(R.color.white));
                    tv_Last7Days.setTextColor(getResources().getColor(R.color.white));
                    tv_Custom.setTextColor(getResources().getColor(R.color.orange));
                    setRecyclerView();
                    customLayout.setVisibility(View.VISIBLE);
                }


                // ArrayList<SubmittedReportModel> lists = submittedReportModel.submittedReportModels();

                fromdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FROM = true;
                        showDatePickerDialog(v);
                        //setRecyclerView();
                    }
                });


                //prevdate = "";
                todate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // btn.setVisibility(View.VISIBLE);
                        FROM = false;
                        showDatePickerDialog1(v);

//                        while(prevdate.equalsIgnoreCase("")) {
//                            prevdate = fromdate.getText().toString();
//                        }
                        //todate.setText();
//                        try {
//                            prevdate = fromdate.getText().toString();
//                            String todate1 = curdate;
//                            if(todate != null) {
//                                todate1 = todate.getText().toString();
//                            }
//                            fetchdata(dealerCatVal, prevdate, todate1);
//                            setRecyclerView();
//                            Log.i("onClick: " ,"try exeuted");
//                        } catch (Exception e) {
//                            Log.i("onClick: " ,"try not exeuted"+e);
//                        }
////                        finally {
////                            String todate1 = todate.getText().toString();
////                            fetchdata(dealerCatVal, prevdate, todate1);
////                            setRecyclerView();
////                        }
//                        fetchdata(dealerCatVal, prevdate, curdate);
                    }


                });

//                    btn.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            String date1 = fromdate.getText().toString();
//                            String date2 = todate.getText().toString();
//                            int dateclick=-1,datenow=-1,dateclickfrom=-1;
//                            try {
//                                String check0 = date1.replace("-", "");
//                                dateclickfrom = Integer.parseInt(check0);
//                                String check1 = date2.replace("-", "");
//                                dateclick = Integer.parseInt(check1);
//                                String check2 = curdate.replace("-", "");
//                                datenow = Integer.parseInt(check2);
//                            }
//                            catch (Exception e){
//
//                            }
//                            Log.i("datedata", "onClick: "+date1);
//                            if(dateclick<=datenow && dateclickfrom <=datenow && dateclick>=dateclickfrom && !date1.equalsIgnoreCase("YYYY-MM-DD") && !date2.equalsIgnoreCase("YYYY-MM-DD")
//                            && dateclick!=-1 && datenow !=-1 && dateclickfrom != -1) {
//                                try {
//                                    btn.setVisibility(View.GONE);
//                                    fetchdata(dealerCatVal, date1, date2);
//                                    setRecyclerView();
//                                } catch (Exception e) {
//
//                                }
//                            }
//                            else
//                            {
//                                Toast.makeText(getApplicationContext(),"please check the date entered",Toast.LENGTH_LONG).show();
//                            }
//                            // customLayout.setVisibility(View.GONE);
//                        }
//                    });


                //String date = fromdate.getText().toString().replace('/', '-');
                //Log.i("fromdate", "" + date);


            }


        });


        if (dealerCatVal.equalsIgnoreCase("select")) {
            dealerCategorySelect.setText("select");
        }


    }
//
//    public static void setCustom(){
//        prevdate = fromdate.getText().toString();
//        String todate1 = todate.getText().toString();
//        fetchdata(dealerCatVal, prevdate, todate1);
//        //setRecyclerView();
//    }

    public static void setRecyclerView() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(mcontext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new SubmittedRptAdapter(mcontext, list, c);
        recyclerView.setAdapter(adapter);

    }

    public static void getData() {
        String date1 = fromdate.getText().toString();
        String date2 = todate.getText().toString();
        int dateclick = -1, datenow = -1, dateclickfrom = -1;
        try {
            String check0 = date1.replace("-", "");
            dateclickfrom = Integer.parseInt(check0);
            String check1 = date2.replace("-", "");
            dateclick = Integer.parseInt(check1);
            String check2 = curdate.replace("-", "");
            datenow = Integer.parseInt(check2);
        } catch (Exception e) {

        }
        Log.i("datedata", "onClick: " + date1);
        if (dateclick <= datenow && dateclickfrom <= datenow && dateclick >= dateclickfrom && !date1.equalsIgnoreCase("YYYY-MM-DD") && !date2.equalsIgnoreCase("YYYY-MM-DD")
                && dateclick != -1 && datenow != -1 && dateclickfrom != -1) {
            try {
                // btn.setVisibility(View.GONE);
                fetchdata(dealerCatVal, date1, date2);
                setRecyclerView();
            } catch (Exception e) {

            }
        } else {
            Toast.makeText(mcontext, "please check the date entered", Toast.LENGTH_LONG).show();
        }
    }

    public void showDatePickerDialog(View v) {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
        //fromdate.setText(newFragment.showDate());

    }

    public void showDatePickerDialog1(View v) {
        DatePickerFragment newFragment1 = new DatePickerFragment();
        newFragment1.show(getSupportFragmentManager(), "datePicker");
        //fromdate.setText(newFragment.showDate());

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        // list.clear();
        //dealerCatVal = "none";
        // dealer_name = "---select ---";
        SELECTED = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //list.clear();
        SELECTED = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        //list.clear();
        SELECTED = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
       /* Log.i("dealrcatval", "onResume: "+dealerCatVal);
        Log.i("dealrcatval", "onResume: "+prevdate);
        Log.i("dealrcatval", "onResume: "+curdate);
        initdata();*/
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(SubmittedDVR.this, MainActivity.class);
        startActivity(intent);

    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        static int year, month, day;
        static Calendar cal = java.util.Calendar.getInstance();

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = cal.getInstance();
            int year = c.get(cal.YEAR);
            int month = c.get(cal.MONTH);
            int day ;
            if(FROM)
                day = 1;
            else
            day= c.get(cal.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            this.year = year;
            this.month = month + 1;

//            if(FROM)
//                this.day = 1;
//            else
                this.day = day;
            if (FROM) {


                fromdate.setText(showDate().toString());
                getData();
                setRecyclerView();
//                todate.setEnabled(true);
            } else {
                todate.setText(showDate().toString());
//                String datefrom = fromdate.getText().toString();
//                String dateto = todate.getText().toString();
//                fetchdata(dealerCatVal,datefrom,dateto);
                getData();
                setRecyclerView();
                // showDate();
                //setCustom();
            }

        }

        public static StringBuilder showDate() {
            StringBuilder str = new StringBuilder();
            String m = Integer.toString(month);
            String d = Integer.toString(day);
            if (m.length() == 1 && d.length() == 1) {
                str.append(year).append("-").append("0")
                        .append(month).append("-").append("0").append(day);
            } else if (m.length() == 1) {
                str.append(year).append("-").append("0")
                        .append(month).append("-").append(day);
            } else if (d.length() == 1) {
                str.append(year).append("-")
                        .append(month).append("-").append("0").append(day);
            } else {
                str.append(year).append("-")
                        .append(month).append("-").append(day);
            }
            return str;
        }
    }

}


