package com.mfcwl.mfc_dealer.Activity.RDR;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.DatePicker;
import android.widget.ImageView;

import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mfcwl.mfc_dealer.Activity.RDR.Adapter.DealerActionItemsAdapter;
import com.mfcwl.mfc_dealer.Activity.RDR.Model.RDRActionItemData;
import com.mfcwl.mfc_dealer.Activity.RDR.Model.RDRActionItemRequest;
import com.mfcwl.mfc_dealer.Activity.RDR.Model.RDRActionItemResponse;
import com.mfcwl.mfc_dealer.Activity.RDR.Model.Where;
import com.mfcwl.mfc_dealer.Activity.RDR.Services.RDRDashboardService;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.SalesStocks.Instances.SalesCalendarFilterDialog;
import com.mfcwl.mfc_dealer.SalesStocks.SalesInterface.SalesFilterSelected;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import imageeditor.base.BaseActivity;
import retrofit2.Response;

public class ViewDetailedListing extends BaseActivity implements SalesFilterSelected {
    private ImageView mBack;
    private static RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private static DealerActionItemsAdapter mAdapter;
    private static List<RDRActionItemData> mData;
    private static String mDealerCode = "";
    private static TextView mTotalTasks;
    public static TextView sortbytext;
    private TextView headertitle;
    private TextView fromtodates;
    private LinearLayout llSortBy, llFilterBydates;
    private String[] options = {"All", "Completed", "Overdue", "Pending"};
    private static Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_detailed_page);
        initViews();
        mDealerCode = getIntent().getStringExtra("dcode");
        setLitners();
        mContext = this;
        getReq("All","","");
        Log.i(TAG, "onCreate: ");
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.black));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        if(CommonMethods.getstringvaluefromkey(this,"user_type").equalsIgnoreCase("dealer")){
          //  headertitle.setText("Dealer Action List");
            getSupportActionBar().setTitle("Dealer Action List");

        }else {
           // toolbar.setTitle("Dealer Action Item List");
            getSupportActionBar().setTitle("Dealer Action Item List");

            // headertitle.setText("Dealer Action Item List");
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void initViews() {
       // mBack = findViewById(R.id.createRepo_backbtn);
        recyclerView = findViewById(R.id.rdrdetailedview);
        mTotalTasks = findViewById(R.id.totalcount);
        sortbytext = findViewById(R.id.sortbytext);
        fromtodates = findViewById(R.id.fromtodates);
      //  headertitle = findViewById(R.id.headertitle);
        layoutManager = new LinearLayoutManager(this);
        llSortBy = findViewById(R.id.llsortby);
        llFilterBydates = findViewById(R.id.lldates);
        recyclerView.setLayoutManager(layoutManager);

    }

    private void setLitners() {
        //mBack.setOnClickListener(this);
        llSortBy.setOnClickListener(this);
        llFilterBydates.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {
            case R.id.createRepo_backbtn:
                finish();
                break;

            case R.id.llsortby:
                setOptionsDialog(sortbytext, "Sort by", options);
                break;

            case R.id.lldates:
                SalesCalendarFilterDialog postcalendarFilterDialog = new SalesCalendarFilterDialog(this, "CustomPostDate");
                postcalendarFilterDialog.setCancelable(false);
                postcalendarFilterDialog.show();
                break;

        }
    }


    public static void getReq(String filter,String startdate,String enddate) {
        RDRActionItemRequest mdata = new RDRActionItemRequest();
        mdata.filterByFields = "";
        mdata.orderBy = "action_id";
        mdata.orderByReverse = "true";
        mdata.page = "1";
        mdata.pageItems = "6000";
        List<Where> data = new ArrayList<>();


        if (mDealerCode != null && !mDealerCode.equalsIgnoreCase("")) {
            Where m = new Where();
            m.column = "dealer_code";
            m.operator = "=";
            m.value = mDealerCode;
            data.add(m);
        }


        if (!filter.equalsIgnoreCase("All")) {
            Where m1 = new Where();
            m1.column = "action_status";
            m1.operator = "=";
            m1.value = filter;
            data.add(m1);
        }

        if(!startdate.equalsIgnoreCase("")){
            Where m2 = new Where();
            m2.column = "rdr_submitted_date";
            m2.operator = ">=";
            m2.value = startdate;
            data.add(m2);
        }
        if(!enddate.equalsIgnoreCase("")){
            Where m3 = new Where();
            m3.column = "rdr_submitted_date";
            m3.operator = "<=";
            m3.value = enddate;
            data.add(m3);
        }



        mdata.where = data;
        getRDRDashboardData(mdata);

    }

    private void setOptionsDialog(TextView tv, final String strTitle, final String[] arrVal) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(strTitle);
        alert.setItems(arrVal, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tv.setText(arrVal[which]);
                getReq(arrVal[which],"","");
            }
        });
        alert.create();
        alert.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        MenuItem searchItem = menu.findItem(R.id.search_bar);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

        if(CommonMethods.getstringvaluefromkey(this,"user_type").equalsIgnoreCase("dealer")){
            searchItem.setVisible(false);
        }else {
            searchItem.setVisible(true);
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });

        return true;
    }

    public static void getRDRDashboardData(RDRActionItemRequest mRequest) {
        SpinnerManager.showSpinner(mContext);
        RDRDashboardService.fetchDealerActionsItems(mRequest, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);
                Response<RDRActionItemResponse> mRes = (Response<RDRActionItemResponse>) obj;
                RDRActionItemResponse actual = mRes.body();
                mData = actual.data;
                mTotalTasks.setText("Total tasks: " + mData.size());
                mAdapter = new DealerActionItemsAdapter(mData, mContext);
                recyclerView.setAdapter(mAdapter);
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(mContext);

            }
        });
    }


    @Override
    public void onSalesFilterSelection(String startDateString, String endDateString, String dateType) {
           Log.e("start",""+startDateString);
           Log.e("end",""+endDateString);
           fromtodates.setText("Date from: "+ChangeDateFormat(startDateString) +" To "+ ChangeDateFormat(endDateString));
        getReq(sortbytext.getText().toString(),startDateString,endDateString);

    }


    private String ChangeDateFormat(String input){

        String formattedDate = "";
        try {
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
            Date date = inputFormat.parse(input);
            formattedDate = outputFormat.format(date);
            System.out.println(formattedDate);
        }catch (Exception e){
            e.printStackTrace();
        }

        return formattedDate;
    }
}
