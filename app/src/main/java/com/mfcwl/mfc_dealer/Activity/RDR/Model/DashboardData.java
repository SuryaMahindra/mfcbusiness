package com.mfcwl.mfc_dealer.Activity.RDR.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DashboardData {

    @SerializedName("dealer_name")
    @Expose
    public String dealerName;
    @SerializedName("dealer_code")
    @Expose
    public String dealerCode;
    @SerializedName("rdr_pending")
    @Expose
    public String rdrPending;
    @SerializedName("rdr_complete")
    @Expose
    public String rdrComplete;
    @SerializedName("rdr_overdue")
    @Expose
    public String rdrOverdue;

}
