package com.mfcwl.mfc_dealer.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    @BindView(R.id.sharelocation)
    public ImageView sharelocation;

    @BindView(R.id.myprofiles)
    public TextView myprofiles;
    @BindView(R.id.dealername)
    public TextView dealername;
    @BindView(R.id.dealeremail)
    public TextView dealeremail;
    @BindView(R.id.dealermobileno)
    public TextView dealermobileno;
    @BindView(R.id.dealeraddress)
    public TextView dealeraddress;
    @BindView(R.id.dealerlatitude)
    public TextView dealerlatitude;
    @BindView(R.id.lonitude)
    public TextView lonitude;
    @BindView(R.id.mapback)
    public ImageView mapback;
    @BindView(R.id.app_bar)
    public ImageView app_bar;

    public Double dealer_latitude=0.0, dealer_longitude=0.0;

    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        ButterKnife.bind(this);

        Log.i(TAG, "onCreate: ");
        try {
            dealer_latitude = Double.parseDouble(CommonMethods.getstringvaluefromkey(this, "dealer_latitude").toString());
            dealer_longitude = Double.parseDouble(CommonMethods.getstringvaluefromkey(this, "dealer_longitude").toString());
        } catch (Exception e) {
            dealer_latitude = 0.0;
            dealer_longitude = 0.0;

        }

        try {

            String s1 = CommonMethods.getstringvaluefromkey(this, "dealer_principal_name");
            if (!s1.equalsIgnoreCase("")) {
                s1 = s1.substring(0, 1).toUpperCase() + s1.substring(1);
            }

            String s2 = CommonMethods.getstringvaluefromkey(this, "dealer_display_name");
            if (!s2.equalsIgnoreCase("")) {
                s2 = s2.substring(0, 1).toUpperCase() + s2.substring(1);
            }


            myprofiles.setText(s1/*+"("+CommonMethods.getstringvaluefromkey(this,"dealer_display_name")+")"*/);
            dealername.setText(s2);


            dealeremail.setText("    " + CommonMethods.getstringvaluefromkey(this, "dealer_email"));
            dealermobileno.setText("    " + CommonMethods.getstringvaluefromkey(this, "dealer_phone"));
            dealermobileno.setText("  " + CommonMethods.getstringvaluefromkey(this, "dealer_phone"));
            dealeraddress.setText("" + CommonMethods.getstringvaluefromkey(this, "dealer_address"));
            dealerlatitude.setText("" + CommonMethods.getstringvaluefromkey(this, "dealer_latitude"));
            lonitude.setText("" + CommonMethods.getstringvaluefromkey(this, "dealer_longitude"));


            if (!CommonMethods.getstringvaluefromkey(this, "dealer_store_image").equalsIgnoreCase("")) {

                Picasso.Builder builder = new Picasso.Builder(this);

                builder.build()
                        .load(CommonMethods.getstringvaluefromkey(this, "dealer_store_image").toString())
                        .placeholder(R.drawable.dealerimg)
                        .error(R.drawable.dealerimg)
                        .into(app_bar);
            }


            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @OnClick(R.id.sharelocation)
    public void sharelocation(View view) {

        Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(this, "dealer_code"), GlobalText.nav_my_profile, GlobalText.android);

        try {
            String share_Content =
                    "*" + dealername.getText().toString().trim()  +"*" +"\n" +
                            "" + dealermobileno.getText().toString().trim() + "\n" +
                            "" + dealeraddress.getText().toString().trim() + "\n";

            Log.e("test share content", share_Content);
            Intent i = new Intent(Intent.ACTION_SEND);
            // i.setType("stock_fragment/plain");
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "Share Location");/*dealer_latitude,dealer_longitude;*/
            // i.putExtra(Intent.EXTRA_TEXT, "https://www.google.co.in/maps/@12.9789127,77.6937003,19z?hl=en&authuser=0");
            i.putExtra(Intent.EXTRA_TEXT, share_Content + "http://maps.google.com/maps?q=" + String.format("%f,%f", dealer_latitude, dealer_longitude));//dealer_latitude+","+dealer_longitude+"19z?hl=en&authuser=0");
            startActivity(Intent.createChooser(i, "Choose One"));
        } catch (Exception e) {
            //e.toString();

        }

        // shareImageWhatsApp();

    }

    @Override
    public void onResume() {
        super.onResume();
        // put your code here...

     //   Application.getInstance().trackScreenView(this,GlobalText.Maps_Activity);

    }

    public void shareImageWhatsApp() {

        Bitmap adv = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);

        Intent share = new Intent(Intent.ACTION_SEND);

        share.setType("image/jpeg");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        adv.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File f = new File(Environment.getExternalStorageDirectory()
                + File.separator + "temporary_file.jpg");
        try {
            f.createNewFile();
            new FileOutputStream(f).write(bytes.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
        share.putExtra(Intent.EXTRA_TEXT, "1.Ensure that the car is washed and cleaned well while photographing it. " +
                "2. Click the image without any harsh shadows or bright glare. Best time to click is early morning/late evening");
        share.putExtra(Intent.EXTRA_STREAM,
                Uri.parse(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg"));

        if (isPackageInstalled("com.whatsapp", this)) {
            share.setPackage("com.whatsapp");
            startActivity(Intent.createChooser(share, "Share Image"));

        } else {

            Toast.makeText(getApplicationContext(), "Please Install Whatsapp", Toast.LENGTH_LONG).show();
        }

    }

    private boolean isPackageInstalled(String packagename, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    @OnClick(R.id.mapback)
    public void mapback(View view) {
        finish();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or top_bottom the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        Log.e("dealer_latitude", "dealer_latitude=" + dealer_latitude);
        Log.e("dealer_longitude", "dealer_longitude=" + dealer_longitude);

        // Add a marker in Sydney and top_bottom the camera
        LatLng bangalore = new LatLng(dealer_latitude, dealer_longitude);
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.icons_user);

        mMap.addMarker(new MarkerOptions()
                .position(bangalore)
                .title("MFC-Dealer"));
        // .icon(icon));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(bangalore, 15));
        // Zoom in, animating the camera.
        googleMap.animateCamera(CameraUpdateFactory.zoomIn());
        // Zoom out to zoom level 10, animating with a duration of 2 seconds.
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
    }
}
