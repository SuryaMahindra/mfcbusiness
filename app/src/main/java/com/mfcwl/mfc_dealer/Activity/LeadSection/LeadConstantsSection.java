package com.mfcwl.mfc_dealer.Activity.LeadSection;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class LeadConstantsSection {

    public static final String LEAD_STATUS_INFO = "STATUSINFO";

    public static final String LEAD_POST_TODAY = "PTODAY";
    public static final String LEAD_POST_YESTER = "PYESTDAY";
    public static final String LEAD_POST_7DAYS = "P7DAY";
    public static final String LEAD_POST_15DAYS = "P15DAY";
    public static final String LEAD_POST_CUSTDATE = "PCUSDATE";

    public static final String LEAD_FOLUP_TMRW = "FTMRW";
    public static final String LEAD_FOLUP_TODAY = "FTODAY";
    public static final String LEAD_FOLUP_YESTER = "FYESTDAY";
    public static final String LEAD_FOLUP_7DAYS = "F7DAY";
    public static final String LEAD_FOLUP_CUSTDATE = "FCUSDATE";

    public static final String LEAD_SAVE_DATE = "SPFDATE";

    public static final String LEAD_SAVE_STATUS = "SSTATUS";


    private String regisMonth = "";

    public String getTodayDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, 1);
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        String postTodYesLastFromDate = s.format(new Date(cal.getTimeInMillis()));
        String postTodYesLastToDate = s.format(new Date(cal1.getTimeInMillis()));
        String mDate = postTodYesLastFromDate + "@" + postTodYesLastToDate;
        return mDate;
    }

    public String getYesterdayDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, 0);

        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        String postTodYesLastFromDate = s.format(new Date(cal.getTimeInMillis()));
        String postTodYesLastToDate = s.format(new Date(cal1.getTimeInMillis()));
        String mfinaldate = postTodYesLastFromDate + "@" + postTodYesLastToDate;
        return mfinaldate;
    }


    public String get7days() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -7);

        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, 1);

        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

        String postTodYesLastFromDate = s.format(new Date(cal.getTimeInMillis()));
        String postTodYesLastToDate = s.format(new Date(cal1.getTimeInMillis()));
        String mfinaldate = postTodYesLastFromDate + "@" + postTodYesLastToDate;

        return mfinaldate;
    }

    public String get15days() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -15);

        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, 1);

        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

        String postTodYesLastFromDate = s.format(new Date(cal.getTimeInMillis()));
        String postTodYesLastToDate = s.format(new Date(cal1.getTimeInMillis()));
        String mfinaldate = postTodYesLastFromDate + "@" + postTodYesLastToDate;

        return mfinaldate;
    }



    //Follow-UP Date

    public String getFollowTomorrowDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);

        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, 0);
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        String startDate = s.format(new Date(cal.getTimeInMillis()));
        String endDate = s.format(new Date(cal1.getTimeInMillis()));

        String mfinaldate = startDate + "@" + endDate;
        return mfinaldate;

    }

    public String getFollowtodayDate() {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);

        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, 1);

        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

        String startDate = s.format(new Date(cal.getTimeInMillis()));
        String endDate = s.format(new Date(cal1.getTimeInMillis()));

        String mfinaldate = startDate + "@" + endDate;
        return mfinaldate;

    }

    public String getFollowlast7Date() {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -7);

        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, 1);

        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

        String startDate = s.format(new Date(cal.getTimeInMillis()));
        String endDate = s.format(new Date(cal1.getTimeInMillis()));

        String mfinaldate = startDate + "@" + endDate;
        return mfinaldate;

    }

    public String getFollowyesterDate() {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);

        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, 0);

        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

        String startDate = s.format(new Date(cal.getTimeInMillis()));
        String endDate = s.format(new Date(cal1.getTimeInMillis()));

        String mfinaldate = startDate + "@" + endDate;
        return mfinaldate;

    }

    public String MethodofMonth(String selMonth) {
        if (selMonth.equals("01")||selMonth.equals("1")) {
            regisMonth = "January";
        }
        if (selMonth.equals("02")||selMonth.equals("2")) {
            regisMonth = "February";
        }
        if (selMonth.equals("03")||selMonth.equals("3")) {
            regisMonth = "March";
        }
        if (selMonth.equals("04")||selMonth.equals("4")) {
            regisMonth = "April";
        }
        if (selMonth.equals("05")||selMonth.equals("5")) {
            regisMonth = "May";
        }
        if (selMonth.equals("06")||selMonth.equals("6")) {
            regisMonth = "June";
        }
        if (selMonth.equals("07")||selMonth.equals("7")) {
            regisMonth = "July";
        }
        if (selMonth.equals("08")||selMonth.equals("8")) {
            regisMonth = "August";
        }
        if (selMonth.equals("09")||selMonth.equals("9")) {
            regisMonth = "September";
        }
        if (selMonth.equals("10")) {
            regisMonth = "October";
        }
        if (selMonth.equals("11")) {
            regisMonth = "November";
        }
        if (selMonth.equals("12")) {
            regisMonth = "December";
        }
        return regisMonth;
    }
}
