package com.mfcwl.mfc_dealer.Activity;


import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.InstanceCreate.LeadSection.LeadFilterSaveInstance;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.NetworkConnect.webServices_okHttp;
import com.mfcwl.mfc_dealer.NotificatioService.Config;
import com.mfcwl.mfc_dealer.Procurement.Instances.ProcLeadFilterSaveInstance;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.Global;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.Helper;
import com.mfcwl.mfc_dealer.Utility.LoggerGeneral;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.Credentials;
import remotedealer.activity.RDMLandingPage;
import remotedealer.activity.TutorialOne;
import remotedealer.retrofit.RetroBase;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mfcwl.mfc_dealer.NotificatioService.MyFirebaseMessagingService.AgeingInventory;
import static com.mfcwl.mfc_dealer.NotificatioService.MyFirebaseMessagingService.lead;
import static com.mfcwl.mfc_dealer.NotificatioService.MyFirebaseMessagingService.leadFollowUp;
import static com.mfcwl.mfc_dealer.NotificatioService.MyFirebaseMessagingService.noStockImage;
import static com.mfcwl.mfc_dealer.NotificatioService.MyFirebaseMessagingService.notificationsStatus;


public class LoginActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_READ_PHONE_STATE = 0;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 0;
    private static final String TAG = MainActivity.class.getSimpleName();
    public static boolean resumeFlag = false;
    public static Activity activity;
    // public static boolean progress = true;
    public static boolean isLoading = true;

    public EditText mEmailView;

    public EditText mPasswordView;

    public TextView signup_tv;

    public TextView forgotpassword;

    public Button mLoginFormView;
    public String regId = "";
    String actionevent, actioneventvalue;
    boolean checkedPermissionsmarshmellow = false;
    Runnable r = null;
    Handler handler;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    public static String mToken = "";

    public static void ParseAppVersion(JSONObject json, String strMethod) {
        try {

            //   Log.e("status", "" + json.getString("status"));
            if (json.getString("status").equalsIgnoreCase("SUCCESS")) {

                if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {

                    if (CommonMethods.getstringvaluefromkey(activity, "appstatus").equalsIgnoreCase("1")) {
                        Intent in_main = new Intent(activity, MainActivity.class);
                        activity.startActivity(in_main);
                        activity.finish();
                    }
                } else {

                    //new Flow

                    CommonMethods.setvalueAgainstKey(activity, "MessageCenter", "'");

                    Intent in_main = new Intent(activity, MainActivity.class);
                    activity.startActivity(in_main);
                    activity.finish();

                }

            } else {
                // error_popup();
            }
        } catch (Exception ex) {
            // Log.e("Ex", ex.toString());
            CommonMethods.setvalueAgainstKey(activity, "appstatus", "0");
            // activity.startActivity(new Intent(activity, LoginActivity.class));
        }
    }

    private static void saveLoginData(JSONObject jObj) {

        try {

            Application.getInstance().trackEvent(jObj.getString("user_name"), "action_login", jObj.getString("user_id"));
           // CommonMethods.setvalueAgainstKey(activity,"IsAutoFin",jObj.getString("IsAutoFin"));
            CommonMethods.setvalueAgainstKey(activity, "user_id", jObj.getString("user_id"));
            CommonMethods.setvalueAgainstKey(activity, "user_name", jObj.getString("user_name"));
            CommonMethods.setvalueAgainstKey(activity, "user_is_dealer", jObj.getString("user_is_dealer"));
            CommonMethods.setvalueAgainstKey(activity, "user_landing_page", jObj.getString("user_landing_page"));
            CommonMethods.setvalueAgainstKey(activity, "role_id", jObj.getString("role_id"));
            CommonMethods.setvalueAgainstKey(activity, "role_name", jObj.getString("role_name"));
            //CommonMethods.setvalueAgainstKey(activity,"dealer_id",jObj.getString("dealer_id"));
            CommonMethods.setvalueAgainstKey(activity, "dealer_code", jObj.getString("dealer_code"));
            //CommonMethods.setvalueAgainstKey(activity,"dealer_ibb_id",jObj.getString("dealer_ibb_id"));
            CommonMethods.setvalueAgainstKey(activity, "dealer_store_image", jObj.getString("dealer_store_image"));
            CommonMethods.setvalueAgainstKey(activity, "dealer_type", jObj.getString("dealer_type"));
            CommonMethods.setvalueAgainstKey(activity, "dealer_city", jObj.getString("dealer_city"));
            CommonMethods.setvalueAgainstKey(activity, "dealer_state", jObj.getString("dealer_state"));
            CommonMethods.setvalueAgainstKey(activity, "dealer_mobile", jObj.getString("dealer_mobile"));
            CommonMethods.setvalueAgainstKey(activity, "dealer_display_name", jObj.getString("dealer_display_name"));
            CommonMethods.setvalueAgainstKey(activity, "dealer_created_date", jObj.getString("dealer_created_date"));
            CommonMethods.setvalueAgainstKey(activity, "dealer_modified_date", jObj.getString("dealer_modified_date"));
            CommonMethods.setvalueAgainstKey(activity, "dealer_email", jObj.getString("dealer_email"));
            CommonMethods.setvalueAgainstKey(activity, "dealer_phone", jObj.getString("dealer_phone"));
            CommonMethods.setvalueAgainstKey(activity, "dealer_address", jObj.getString("dealer_address"));
            CommonMethods.setvalueAgainstKey(activity, "dealer_pincode", jObj.getString("dealer_pincode"));
            CommonMethods.setvalueAgainstKey(activity, "dealer_latitude", jObj.getString("dealer_latitude"));
            CommonMethods.setvalueAgainstKey(activity, "dealer_longitude", jObj.getString("dealer_longitude"));
            CommonMethods.setvalueAgainstKey(activity, "dealer_principal_name", jObj.getString("dealer_principal_name"));
            CommonMethods.setvalueAgainstKey(activity, "dealers_in_state", jObj.getString("dealers_in_state"));

        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    public static void ParseLogin(JSONObject json, String strMethod) {
        try {

            Log.e(TAG, "after login " + json);

            if (json.getString("status").equalsIgnoreCase("SUCCESS")) {

                JSONObject jObj = json.getJSONObject("data");

                String mUserType = json.getString("user_type");
                String mUserId = jObj.getString("user_id");
                String mUserName = jObj.getString("user_name");
                Boolean isPasswordPolicy = jObj.getBoolean("IsPasswordPolicy");
                if(isPasswordPolicy){
                     hideKeyBoard();
                    CommonMethods.setvalueAgainstKey(activity,"token","");
                    passwordResetDialog(mUserName);
                    return;
                }
                Application.getInstance().trackEvent("LoggedIn_Android", "" + mUserType + " - " + mUserId, "Android");

                CommonMethods.setvalueAgainstKey(activity, "user_type", json.getString("user_type"));
                CommonMethods.setvalueAgainstKey(activity, "appstatus", "1");
                CommonMethods.setvalueAgainstKey(activity, "user_id", jObj.getString("user_id"));

                saveLoginData(jObj);

                if (json.getString("user_type").equalsIgnoreCase("dealer")) {

                    if (jObj.getString("user_is_dealer").equalsIgnoreCase("true")) {
                        WebServicesCall.webCall(activity, activity, jsonMakeLeadAccessToken(), "LeadAccessToken", GlobalText.POST);
                    } else {
                        CommonMethods.alertMessage(activity, "Please use Dealer credentials to Login");
                    }

                } else {

                    if (jObj.has("user_zone")) {
                        CommonMethods.setvalueAgainstKey(activity, "user_zone", jObj.getString("user_zone"));
                    }
                    WebServicesCall.webCall(activity, activity, jsonMakeLeadAccessToken(), "LeadAccessToken", GlobalText.POST);

                }

                //already filter data clear
                CommonMethods.setvalueAgainstKey(activity, "stockFilterClear", "changes");
                CommonMethods.setvalueAgainstKey(activity, "leadFilterClear", "changes");
                CommonMethods.setvalueAgainstKey(activity, "procurementFilterClear", "changes");


            } else {

            }


        } catch (Exception ex) {
            //  Log.e("Ex", ex.toString());
            CommonMethods.setvalueAgainstKey(activity, "appstatus", "0");

        }
    }

    private static JSONObject jsonMakeLeadAccessToken() {

        JSONObject jObj = new JSONObject();
        try {
            jObj.put("username", "mfcwapp@lms.com");
            jObj.put("password", "p@ssword");
            jObj.put("tag", "android");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Log.e("jsonMakeLeadAcc ", "token " + jObj.toString());

        return jObj;
    }

    public static void ParseLeadAccessToken(JSONObject jObj, Activity activity) {
        try {
            if (jObj.getString("status").equalsIgnoreCase("success")) {
                CommonMethods.setvalueAgainstKey(activity, "access_token", jObj.getString("access_token"));
                String userType = CommonMethods.getstringvaluefromkey(activity,"user_type");
               String isUserSawTutorial = CommonMethods.getstringvaluefromkey(activity,"isTutorialSaw");
               if(userType.equalsIgnoreCase("AM")){
                   if(isUserSawTutorial != "" && isUserSawTutorial.equalsIgnoreCase("true")){
                       Intent in_main = new Intent(activity, RDMLandingPage.class);
                       activity.startActivity(in_main);
                       activity.finish();
                   }else {
                       Intent in_main = new Intent(activity, TutorialOne.class);
                       activity.startActivity(in_main);
                       activity.finish();
                   }
               }else {
                   Intent in_main = new Intent(activity, MainActivity.class);
                   activity.startActivity(in_main);
                   activity.finish();
               }




            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loginApiCall(String username, String password) {
        @NotNull String credential = Credentials.basic(username, password);
        RetroBase.Companion.getMfcRetroInterface().getFromWeb(Global.loginURL,credential).enqueue(new Callback<Object>(){

            @Override
            public void onResponse(final @NotNull Call<Object> call, final @NotNull Response<Object> response) {
                try {
                    String mRes = new Gson().toJson(response.body());
                    String headers = new Gson().toJson(response.headers());
                    JSONObject headerJson = new JSONObject(headers);
                    CommonMethods.setvalueAgainstKey(activity,"access_token_from_header",headerJson.getString("Token"));
                    JSONObject json = new JSONObject(mRes);
                    ParseLogin(json,"");
                } catch (Exception e){
                        e.printStackTrace();
                }
            }

            @Override
            public void onFailure(final Call<Object> call, final Throwable t) {

            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate: ");
        activity = this;
        handler = new Handler();

        if (!CommonMethods.getstringvaluefromkey(activity, "token").equalsIgnoreCase("")) {

            CommonMethods.setvalueAgainstKey(activity, "status", "Home");

            if (CommonMethods.isInternetWorking(activity)) {
                resumeFlag = true;
                this.gotoHomePage();
            } else {
                CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
            }
        } else {
            setContentView(R.layout.activity_login);
            mLoginFormView = (Button) findViewById(R.id.email_sign_in_button);
            signup_tv = findViewById(R.id.signup_tv);
            mEmailView = findViewById(R.id.email);
            mPasswordView = findViewById(R.id.password);
            forgotpassword = findViewById(R.id.forgotpassword);

            resetMehods();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                checkMPermissions();
            }

            mRegistrationBroadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String message = "Assigend";
                    CommonMethods.setvalueAgainstKey(LoginActivity.activity, "nofication_status", "true");
                    if (notificationsStatus.equalsIgnoreCase("lead")) {
                        CommonMethods.setvalueAgainstKey(LoginActivity.this, "Notification_Switch", "lead");
                    } else if (notificationsStatus.equalsIgnoreCase("noStockImage")) {
                        CommonMethods.setvalueAgainstKey(LoginActivity.this, "Notification_Switch", "noStockImage");
                        //    Log.e("noStockImage", "noStockImage-coming-11");
                    } else if (notificationsStatus.equalsIgnoreCase("leadFollowUp")) {
                        CommonMethods.setvalueAgainstKey(LoginActivity.this, "Notification_Switch", "leadFollowUp");
                    } else if (notificationsStatus.equalsIgnoreCase("AgeingInventory")) {
                        CommonMethods.setvalueAgainstKey(LoginActivity.this, "Notification_Switch", "AgeingInventory");
                    }
                    if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                        // gcm successfully registered
                        // now subscribe to `global` topic to receive app wide notifications
                        FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                        displayFirebaseRegId();

                    } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                        // new push notification is received
                        message = intent.getStringExtra("message");
                        //Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                        // txtMessage.setText(message);
                        // Log.e(TAG, "Firebase reg id: " + message);
                    }
                }
            };


            if (regId.equalsIgnoreCase("") || regId == null) {
                CommonMethods.setvalueAgainstKey(this, "device_id", FirebaseInstanceId.getInstance().getToken());
            }
            if (!regId.equalsIgnoreCase("")) {
                CommonMethods.setvalueAgainstKey(this, "device_id", regId.toString());
            }
            CommonMethods.setvalueAgainstKey(this, "device_type", "android");
            CommonMethods.setvalueAgainstKey(this, "device_version", Build.VERSION.RELEASE);

            mLoginFormView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (CommonMethods.isInternetWorking(activity)) {

                            if (mEmailView.getText().toString().trim().equalsIgnoreCase("")) {
                                Toast.makeText(LoginActivity.this, "Enter user name.", Toast.LENGTH_LONG).show();
                            } else if (mEmailView.getText().toString().trim().equalsIgnoreCase("")) {
                                Toast.makeText(LoginActivity.this, "Enter password.", Toast.LENGTH_LONG).show();
                            } else {
                                if (regId.equalsIgnoreCase("") || regId == null) {
                                    CommonMethods.setvalueAgainstKey(activity, "device_id", FirebaseInstanceId.getInstance().getToken());
                                }
                                webServices_okHttp.MethodGetCallDetails(activity, Global.loginURL, mEmailView.getText().toString()
                                        , mPasswordView.getText().toString(), "Login");
//                                loginApiCall( mEmailView.getText().toString()
//                                        , mPasswordView.getText().toString());
                                CommonMethods.setvalueAgainstKey(activity, "usernameforIbb", mEmailView.getText().toString());
                                CommonMethods.getstringvaluefromkey(activity, "usernameforIbb");
                            }

                        } else {
                            CommonMethods.alertMessage(activity, "Please Check Network Connection.");
                        }
                    } catch (Exception e) {
                        //   e.printStackTrace();
                        Log.e("Exception", "Exception=" + e.toString());
                        //.smoothToHide();
                    }

       /* } else {
            login_username_et.setText("");
            login_password_et.setText("");
        }*/

                }

            });

            signup_tv.setOnClickListener(v -> {
                Intent in_main = new Intent(activity, SignupActivity.class);
                activity.startActivity(in_main);
            });
            forgotpassword.setOnClickListener(v -> {
                Intent in_main = new Intent(activity, ForgotPasswordActivity.class);
                activity.startActivity(in_main);
            });

        }

        if (lead.equalsIgnoreCase("lead")) {
            CommonMethods.setvalueAgainstKey(LoginActivity.this, "Notification_Switch", "lead");
        } else if (noStockImage.equalsIgnoreCase("noStockImage")) {
            Log.e("noStockImage", "noStockImage-coming-0");
            CommonMethods.setvalueAgainstKey(LoginActivity.this, "Notification_Switch", "noStockImage");
        } else if (leadFollowUp.equalsIgnoreCase("leadFollowUp")) {
            CommonMethods.setvalueAgainstKey(LoginActivity.this, "Notification_Switch", "leadFollowUp");
        } else if (AgeingInventory.equalsIgnoreCase("AgeingInventory")) {
            CommonMethods.setvalueAgainstKey(LoginActivity.this, "Notification_Switch", "AgeingInventory");
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter("com.mfcwl.mfc_dealer"));

        r = new Runnable() {

            public void run() {

                if (getIntent().getExtras() != null) {
                    try {
                        for (String key : getIntent().getExtras().keySet()) {
                            String value = getIntent().getExtras().getString(key);
                            Log.d("Hii", "Key: " + key + " Value: " + value);

                            if (key.equalsIgnoreCase("actionevent")) {
                                actionevent = key;
                                actioneventvalue = value;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    //Log.e("Token","Token-3");
                }

                // Log.e("Token","Token-4"+actioneventvalue);

                if (actionevent != null) {
                    if (actionevent != "") {

                        LoggerGeneral.info("actionevent--" + actioneventvalue);
                        if (actioneventvalue.equalsIgnoreCase("lead")
                                || notificationsStatus.equalsIgnoreCase("lead")) {

                            CommonMethods.setvalueAgainstKey(LoginActivity.this, "Notification_Switch", "lead");
                            CommonMethods.setvalueAgainstKey(LoginActivity.activity, "nofication_status", "true");

                        } else if (actioneventvalue.equalsIgnoreCase("noStockImage")
                                || notificationsStatus.equalsIgnoreCase("noStockImage")) {
                            //   Log.e("noStockImage", "noStockImage-coming-1");
                            CommonMethods.setvalueAgainstKey(LoginActivity.this, "Notification_Switch", "noStockImage");
                            CommonMethods.setvalueAgainstKey(LoginActivity.activity, "nofication_status", "true");

                        } else if (actioneventvalue.equalsIgnoreCase("leadFollowUp")
                                || notificationsStatus.equalsIgnoreCase("leadFollowUp")) {
                            CommonMethods.setvalueAgainstKey(LoginActivity.this, "Notification_Switch", "leadFollowUp");
                            CommonMethods.setvalueAgainstKey(LoginActivity.activity, "nofication_status", "true");

                        } else if (actioneventvalue.equalsIgnoreCase("AgeingInventory")
                                || notificationsStatus.equalsIgnoreCase("AgeingInventory")) {
                            CommonMethods.setvalueAgainstKey(LoginActivity.this, "Notification_Switch", "AgeingInventory");
                            CommonMethods.setvalueAgainstKey(LoginActivity.activity, "nofication_status", "true");

                        } else {
                            //main
                            // CommonMethods.setvalueAgainstKey(MainActivity.this, "Notification_Switch", "type");

                        }
                    } else {
                        // CommonMethods.setvalueAgainstKey(MainActivity.this, "Notification_Switch", "type");

                        //main
                    }
                } else {
                    // CommonMethods.setvalueAgainstKey(MainActivity.this, "Notification_Switch", "type");

                        /*Intent intent = new Intent(MainActivity.this, DisplayAct.class);
                        startActivity(intent);
                        finish();*/
                }
                // }

                handler.postDelayed(r, 1000);
            }
        };
        handler.post(r);

        showIntentDetails();


    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        showIntentDetails();
    }

    private void showIntentDetails() {
        try {
            Bundle extras = getIntent().getExtras();
            String intentDetailsLog = "";

            String action = getIntent().getAction();
            if (action != null) {
                intentDetailsLog +=
                        "INTENT ACTION\n" + " - " + action + "\n";
            }

            if (extras != null) {
                intentDetailsLog += "\nINTENT EXTRAS\n";
                for (String key : extras.keySet()) {
                    intentDetailsLog += " - " + key + " : " + extras.get(key) + "\n";
                }
            }
            LoggerGeneral.info("inLog--" + intentDetailsLog);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(r);
    }

    private void checkMPermissions() {

        try {
            ActivityCompat.requestPermissions(LoginActivity.this,
                    new String[]{Manifest.permission.INTERNET,
                            Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.READ_CONTACTS,
                            Manifest.permission.ACCESS_NETWORK_STATE,
                            //  Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.ACCESS_WIFI_STATE,
                            Manifest.permission.CALL_PHONE,
                            Manifest.permission.CAMERA,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.RECEIVE_BOOT_COMPLETED,

                    }, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkedPermissionsmarshmellow = true;
                } else {
                    checkMPermissions();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                //  return;
                break;
            }
        }
    }

    private void displayFirebaseRegId() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        regId = pref.getString("regId", null);

        if (!TextUtils.isEmpty(regId))
            // txtRegId.setText("Firebase Reg Id: " + regId);
            Log.e(TAG, "Firebase reg id: " + regId);
        else
            Log.e(TAG, "Firebase Reg Id is not received yet!");
        // txtRegId.setText("Firebase Reg Id is not received yet!");

    }

    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));
        Application.getInstance().trackScreenView(activity, GlobalText.login);


    }

    public void resetMehods() {

        CommonMethods.setvalueAgainstKey(activity, "booknowbtn", "");
        CommonMethods.setvalueAgainstKey(activity, "bookfilter", "");
        CommonMethods.setvalueAgainstKey(activity, "stockdetails", "");
        CommonMethods.setvalueAgainstKey(activity, "filterapply", "");
        CommonMethods.setvalueAgainstKey(activity, "unbooknowbtn", "");
        CommonMethods.setvalueAgainstKey(activity, "AgeingStockTrue", "");
        CommonMethods.setvalueAgainstKey(activity, "LeadFollowUpTrue", "");
        CommonMethods.setvalueAgainstKey(activity, "ImageUploadRemainderTrue", "");
        CommonMethods.setvalueAgainstKey(activity, "NewLeadTrue", "");
        CommonMethods.setvalueAgainstKey(activity, "status", "Home");

        clearDB();
    }

    private void clearDB() {

        CommonMethods.setvalueAgainstKey(activity, "column", "");
        CommonMethods.setvalueAgainstKey(activity, "operator", "");
        CommonMethods.setvalueAgainstKey(activity, "value", "");
        CommonMethods.setvalueAgainstKey(activity, "statuslist", "");

        CommonMethods.setvalueAgainstKey(activity, "filterapply", "");
        CommonMethods.setvalueAgainstKey(activity, "unbooknowbtn", "");

        CommonMethods.setvalueAgainstKey(activity, "column2", "");
        CommonMethods.setvalueAgainstKey(activity, "operator2", "");
        CommonMethods.setvalueAgainstKey(activity, "value2", "");

        CommonMethods.setvalueAgainstKey(activity, "30Name", "");

        CommonMethods.setvalueAgainstKey(activity, "leads_status", "");
        CommonMethods.setvalueAgainstKey(activity, "withoutfollowup", "");

        LeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());
        LeadFilterSaveInstance.getInstance().setStatusarray(new JSONArray());

        ProcLeadFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());
        ProcLeadFilterSaveInstance.getInstance().setStatusarray(new JSONArray());


    }

    private void gotoHomePage() {
        Intent in_main = new Intent(activity, MainActivity.class);
        activity.startActivity(in_main);
        activity.finish();
    }


    private static void passwordResetDialog(String userName){

         AlertDialog.Builder alert = new AlertDialog.Builder(activity,R.style.AlertDialogTheme);
        alert.setTitle("MFC Business");
        alert.setMessage("Please reset your password as per our new password policy rules");
        alert.setPositiveButton("Reset",
                (dialog, which) -> {
                    dialog.cancel();
                    Intent restpass = new Intent(activity,ResetPasswordActivity.class);
                    restpass.putExtra("uname",userName);
                    activity.startActivity(restpass);
                });
        alert.setNegativeButton("Close",
                (dialog, which) -> {
                    dialog.cancel();

                });
        alert.show();

    }

    private static void hideKeyBoard(){
        InputMethodManager imm=(InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }
    
}

