package com.mfcwl.mfc_dealer.Activity.Leads;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment;
import com.mfcwl.mfc_dealer.Fragment.Leads.LeadsFragment;
import com.mfcwl.mfc_dealer.Fragment.Leads.PrivateLeadsFragment;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import org.json.JSONArray;

import butterknife.ButterKnife;

import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chcold;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chfollowlast7days;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chfollowtmrw;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chfollowtoday;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chfollowyester;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chhot;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chlost;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chopen;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chpost15days;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chpost7days;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chposttoday;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chpostyester;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chsold;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chwarm;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.followFromDate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.followToDate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.followTodYesLastFromDate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.followTodYesLastToDate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.followTodayDate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.followTomorowdate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.fromdate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.fromdatefoll;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.leadstatusJsonArray;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.postFromDate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.postToDate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.postTodYesLastFromDate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.postTodYesLastToDate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.todate;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.todatefoll;
import static com.mfcwl.mfc_dealer.Fragment.Leads.NormalLeadsFragment.flagsetonetimecall;
import static com.mfcwl.mfc_dealer.Fragment.Leads.PrivateLeadsFragment.flagRes;
import static com.mfcwl.mfc_dealer.Utility.LeadsUtils.ClearFollowDataFilter;
import static com.mfcwl.mfc_dealer.Utility.LeadsUtils.ClearLeadStatusFilter;
import static com.mfcwl.mfc_dealer.Utility.LeadsUtils.ClearPostDataFilter;

/**
 * Created by HP 240 G5 on 12-03-2018.
 */

public class LeadFilterActivity extends AppCompatActivity {

    public Fragment fragment;
    // Button btnback1;
    FrameLayout list2, list1;
    Activity activity;
    TextView toolbar_menu;
    private SharedPreferences mSharedPreferences = null;
    private Gson gson;
    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate: ");
        setContentView(R.layout.layout_main_filter);
        ButterKnife.bind(this);
        mSharedPreferences = getSharedPreferences("MFC", Context.MODE_PRIVATE);
        // myPriList.clear();
        // myJobList.clear();
        flagRes = true;
        flagsetonetimecall = true;
        list2 = (FrameLayout) findViewById(R.id.list2);
        list1 = (FrameLayout) findViewById(R.id.list1);
        toolbar_menu = (TextView) findViewById(R.id.toolbar_menu);
        activity = this;
        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        LeadFilterFragment filterFragment = new LeadFilterFragment();
        gson = new Gson();
        this.fragmentLoad(filterFragment);

        list2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.lead_filter_apply, GlobalText.android);
                FilterInstance.getInstance().setNotification("");
                if (LeadsInstance.getInstance().getWhichleads().equals("WebLeads")) {
                    Log.e("LeadsInstance ", "WebLeads " + LeadsInstance.getInstance().getWhichleads());
                    // NormalLeadsFragment.SortByLeads();
                } else {
                    Log.e("LeadsInstance ", "PrivateLeads " + LeadsInstance.getInstance().getWhichleads());
                    // PrivateLeadsFragment.SortByLeads();
                }

                //ASM
                CommonMethods.setvalueAgainstKey(activity,"leadStatus","false");
                CommonMethods.setvalueAgainstKey(activity,"Lvalue","");

                LeadFilterFragment.mInstance.setLeadstatusJsonArray(LeadFilterFragment.leadstatusJsonArray);
                SharedPreferences.Editor mEdit = mSharedPreferences.edit();
                String json = gson.toJson(LeadFilterFragment.mInstance);
                mEdit.putString("leadfilter", json);
                mEdit.commit();

                //  Log.e("LeadFilter", json.toString());
                finish();
            }
        });

        list1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LeadsFragment.normalLead = true;
                PrivateLeadsFragment.mPrivate = true;

                String mJSon = mSharedPreferences.getString("leadfilter", "");
                if (mJSon != null && mJSon != "") {
                    LeadFilterFragment.mInstance = gson.fromJson(mJSon, FilterInstance.class);
                }
                if (LeadFilterFragment.mInstance.getLeadstatusJsonArray() != null) {
                    LeadFilterFragment.leadstatusJsonArray = LeadFilterFragment.mInstance.getLeadstatusJsonArray();
                }

                finish();

            }
        });

        toolbar_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Application.getInstance().trackEvent(CommonMethods.getstringvaluefromkey(activity, "dealer_code"), GlobalText.lead_filter_clear, GlobalText.android);

                ClearPostDataFilter();

                ClearFollowDataFilter();

                ClearLeadStatusFilter();

                //ASM
                CommonMethods.setvalueAgainstKey(activity, "leadStatus", "");

                //yuvaraj added this line
                // ClearALLLeads();
                LeadFilterFragment.mInstance = FilterInstance.getInstance();
                SharedPreferences.Editor mEditor = mSharedPreferences.edit();
                mEditor.clear();
                mEditor.commit();

                NotificationInstance.getInstance().setTodayfollowdate("");

                chposttoday.setChecked(false);
                chpostyester.setChecked(false);
                chpost7days.setChecked(false);
                chpost15days.setChecked(false);

                chfollowtmrw.setChecked(false);
                chfollowtoday.setChecked(false);
                chfollowyester.setChecked(false);
                chfollowlast7days.setChecked(false);

                chopen.setChecked(false);
                chhot.setChecked(false);
                chwarm.setChecked(false);
                chcold.setChecked(false);
                chlost.setChecked(false);
                chsold.setChecked(false);

                postFromDate = "";
                postToDate = "";

                followFromDate = "";
                followToDate = "";

                postTodYesLastFromDate = "";
                postTodYesLastToDate = "";

                followTodYesLastFromDate = "";
                followTodYesLastToDate = "";

                followTomorowdate = "";
                followTodayDate = "";

                leadstatusJsonArray = new JSONArray();

                fromdate.setText("");
                todate.setText("");

                fromdatefoll.setText("");
                todatefoll.setText("");
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        // put your code here...
        Application.getInstance().trackScreenView(activity,GlobalText.LeadfilterActivity);
    }

    @Override
    public void onBackPressed() {
        LeadsFragment.normalLead = true;
    }

    public void fragmentLoad(Fragment fragment) {

        FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fillLayout_main_filter, fragment);
        fragmentTransaction.commit();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                LeadsFragment.normalLead = true;
                PrivateLeadsFragment.mPrivate = true;

                String mJSon = mSharedPreferences.getString("leadfilter", "");
                if (mJSon != null && mJSon != "") {
                    LeadFilterFragment.mInstance = gson.fromJson(mJSon, FilterInstance.class);
                }
                if (LeadFilterFragment.mInstance.getLeadstatusJsonArray() != null) {
                    LeadFilterFragment.leadstatusJsonArray = LeadFilterFragment.mInstance.getLeadstatusJsonArray();
                }
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
