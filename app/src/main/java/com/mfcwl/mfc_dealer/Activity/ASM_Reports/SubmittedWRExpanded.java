package com.mfcwl.mfc_dealer.Activity.ASM_Reports;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.WeekReportSubmitModel;
import com.mfcwl.mfc_dealer.R;

import java.util.Calendar;


public class SubmittedWRExpanded extends AppCompatActivity {

    Calendar myCalendar;
    public TextView dealername_tv,cr_day,cr_date,cr_time,zone,zoneSelect,omsCode,dealerType,activation,dealerCategorySelect,stksevdaysSelect,
            week_title_tv,week_start_date,week_end_date,currentStock,conversion_IEP,totalAggregateCases,conversion_NCD,totalWalkins,total_procurement
            ,Num_Of_highestBids,total_sales,conversion_OMS,warrantyPen,actualActivationTime,royalty_select_tv,cr_omscodeet;

    public EditText state,city,acSapCode,amToken,amName,shTokenno,shName,osOfMonth,stkLtSixty,stkGtSixty,pAStock,
            cpt_procure,self_procure,iep_procure,ncd_procure,leadstIEP,leadstNCD,paidstock_ev,parksell_ev,
            xmart_nonIEP,retail_Sales,offload_sales,bookingInHand,
            omsLeads,walkinstOMS,DoOWalkins,numOfFinCases,warrantyNos,warrantyValue,
            openAggregateCases,closedAggregateCases,actualCollection,
            NumOfDealerVisits;
    LinearLayout dealername_ll;
    public Button week_save_btn,submitBtn;

    String json ="",dealer_code="";
    String createdOn;
    String TAG = getClass().getSimpleName();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_report);
        Log.i(TAG, "onCreate: ");
        cr_day=findViewById(R.id.cr_day);
        cr_date=findViewById(R.id.cr_date);
        cr_time=findViewById(R.id.cr_time);
        cr_day.setVisibility(View.GONE);
        cr_date.setHint("");
        cr_date.setBackgroundResource(0);
        cr_time.setVisibility(View.GONE);

        week_title_tv= findViewById(R.id.week_title_tv);
        dealername_tv= findViewById(R.id.dealername_tv);
        dealername_ll= findViewById(R.id.dealername_ll);
        week_start_date=findViewById(R.id.week_start_date);
        week_end_date= findViewById(R.id.week_end_date);
        week_save_btn= findViewById(R.id.week_save_btn);
        cr_omscodeet= findViewById(R.id.cr_omscodeet);
        paidstock_ev= findViewById(R.id.paidstock_ev);
        parksell_ev= findViewById(R.id.parksell_ev);
        royalty_select_tv= findViewById(R.id.royalty_select_tv);

        json=getIntent().getStringExtra("json");

       // Log.i("jsonobject", "onCreate: "+json);

        ImageView backIV=findViewById(R.id.createRepo_backbtn);

        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //Textviews
       // activationDate=findViewById(R.id.cr_activationet);
        actualActivationTime=findViewById(R.id.cr_dealercategorytv);

        //Textviews
        activation=findViewById(R.id.cr_activationet);
        dealerType=findViewById(R.id.cr_dealercategorytv);
        submitBtn=findViewById(R.id.submitBtn);

        //EditTexts
        state=findViewById(R.id.cr_stateet);
        city=findViewById(R.id.cr_cityet);
        omsCode=findViewById(R.id.cr_omscodeet);
        acSapCode=findViewById(R.id.ac_sapcodeet);
        amToken=findViewById(R.id.cr_amtokenet);
        amName=findViewById(R.id.cr_amnameet);
        shTokenno=findViewById(R.id.cr_smtokenet);
        shName=findViewById(R.id.cr_shnameet);
        osOfMonth=findViewById(R.id.cr_openingstocket);
        stkLtSixty=findViewById(R.id.cr_stocktillsixtyet);
        stkGtSixty=findViewById(R.id.cr_stockgtsixtyet);
        currentStock=findViewById(R.id.cr_current_stocket);
        pAStock=findViewById(R.id.cr_actualstocket);
        cpt_procure=findViewById(R.id.cr_cptet);
        self_procure=findViewById(R.id.cr_selfet);
        iep_procure=findViewById(R.id.cr_iepet);
        ncd_procure=findViewById(R.id.cr_ncdet);
        leadstIEP=findViewById(R.id.cr_leadsthiepet);
        leadstNCD=findViewById(R.id.cr_leadsthrncdet);
        conversion_IEP=findViewById(R.id.cr_coniepet);
        conversion_NCD=findViewById(R.id.cr_conncdet);
        Num_Of_highestBids=findViewById(R.id.cr_nohighestbidset);
        xmart_nonIEP=findViewById(R.id.cr_xmartet);
        total_procurement=findViewById(R.id.cr_procurementet);
        retail_Sales=findViewById(R.id.cr_retailsaleset);
        offload_sales=findViewById(R.id.cr_offloadsaleset);
        total_sales=findViewById(R.id.cr_totalet);
        bookingInHand=findViewById(R.id.cr_bookinginhandet);
        conversion_OMS=findViewById(R.id.cr_conomsset);
        omsLeads=findViewById(R.id.cr_omsleadset);
        walkinstOMS=findViewById(R.id.cr_walkinins);
        DoOWalkins=findViewById(R.id.cr_directorganicet);
        totalWalkins=findViewById(R.id.cr_totalnowalkinset);
        numOfFinCases=findViewById(R.id.cr_nofinancecaseset);
        warrantyNos=findViewById(R.id.cr_warrantyet);
        warrantyValue=findViewById(R.id.cr_warrantyvalet);
        warrantyPen=findViewById(R.id.cr_warrantypenet);
        openAggregateCases=findViewById(R.id.cr_openaggregateet);
        closedAggregateCases=findViewById(R.id.cr_closedaggregateet);
        totalAggregateCases=findViewById(R.id.cr_totalaggregateet);
        actualCollection=findViewById(R.id.cr_actualcollet);
        NumOfDealerVisits=findViewById(R.id.cr_nodealeret);

        //Select items
        zone= findViewById(R.id.zonelbl);
        zoneSelect= findViewById(R.id.zone_selectview);
        dealerCategorySelect=findViewById(R.id.dealer_category_selectview);
        stksevdaysSelect=findViewById(R.id.cr_stockrefreshedsv);

        LinearLayout l = findViewById(R.id.layout_WR);

        Button sbt,save;
        sbt = findViewById(R.id.submitBtn);
        save = findViewById(R.id.week_save_btn);
        sbt.setVisibility(View.GONE);
        save.setVisibility(View.GONE);

        setViewAndChildrenEnabled(l,false);

        //  cr_weektv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        zoneSelect.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        dealerCategorySelect.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        stksevdaysSelect.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        royalty_select_tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

        if(!json.equalsIgnoreCase("")){
            setData(json);
        }

    }
    private void setData(String editJson) {

        try{

            WeekReportSubmitModel setData = new WeekReportSubmitModel();
            Gson gson = new Gson();
            setData = gson.fromJson(editJson.toString(), WeekReportSubmitModel.class);

            zoneSelect.setText(setData.getZone());
            state.setText(setData.getState());
            city.setText(setData.getCity());
            dealer_code=setData.getDealerCode();
            acSapCode.setText(setData.getAccountSapCode());

            createdOn=setData.getCreatedOn();
            createdOn = createdOn.replace("T", " | ");
            createdOn = createdOn.substring(0, 18);
            String b = createdOn.substring(0, 10);
            Log.i("datetime", "onBindViewHolder: "+b);
            String[] values = b.split("-");
            int day = Integer.parseInt(values[2]);
            int month = Integer.parseInt(values[1]);
            int year = Integer.parseInt(values[0]);

            Log.i("datetime", "onBindViewHolder: "+values[1]);
            myCalendar = Calendar.getInstance();
            myCalendar.set(year, month - 1, day - 1);

            int dayof = myCalendar.get(myCalendar.DAY_OF_WEEK);
            String dayofweek = getDayOfWEek(dayof);
            Log.i(" dayofweek", "OnSuccess: " + dayofweek);
            createdOn = dayofweek + " | " + createdOn;

            cr_date.setText(createdOn);

            cr_omscodeet.setText(dealer_code);
            dealerCategorySelect.setText(setData.getDealerCategory());
            amToken.setText(setData.getAmTokenNo());
            amName.setText(setData.getAmName());
            shTokenno.setText(setData.getSmTokenNo());
            shName.setText(setData.getSmName());
            osOfMonth.setText(setData.getStcokOpeningMonth());
            stksevdaysSelect.setText(setData.getStockRefreshed());
            stkLtSixty.setText(setData.getStockLess60Days());
            stkGtSixty.setText(setData.getStockGrater60Days());
            currentStock.setText(setData.getCurrentStock());
            pAStock.setText(setData.getActualStock());
            cpt_procure.setText(setData.getCpt());
            self_procure.setText(setData.getSelfProcurement());
            iep_procure.setText(setData.getIep());
            ncd_procure.setText(setData.getNcd());
            leadstIEP.setText(setData.getIepLead());

            leadstNCD.setText(setData.getNcpLead());

            conversion_IEP.setText(setData.getIepConversion());
            conversion_NCD.setText(setData.getNcdConversionPer());
            Num_Of_highestBids.setText(setData.getNoOfBids());
            xmart_nonIEP.setText(setData.getXMart());
            total_procurement.setText(setData.getTotalProcurement());
            retail_Sales.setText(setData.getRetailSales());
            offload_sales.setText(setData.getOffloadSales());
            total_sales.setText(String.valueOf(setData.getTotalSales()));
            bookingInHand.setText(setData.getBookingInHand());
            conversion_OMS.setText(setData.getOmsLeadConversion());
            omsLeads.setText(setData.getOmsLeads());
            walkinstOMS.setText(setData.getOmsWalkinThrough());
            numOfFinCases.setText(setData.getNoOfFinanceCases());
            warrantyNos.setText(setData.getWarrantyNo());
            warrantyValue.setText(setData.getWarrantyValues());

            warrantyPen.setText(setData.getWarrantyPercentage());

            openAggregateCases.setText(setData.getOpenAggregateCases());
            closedAggregateCases.setText(setData.getClosedAggregateCases());
            totalAggregateCases.setText(setData.getTotalAggregateCases());

            actualCollection.setText(setData.getActualCollection());
            NumOfDealerVisits.setText(setData.getNoOfDealerVisits());

            //CreatedOn("2019-01-25");
            //  cr_weektv.setText(setData.getWeekForText());

            try {

                if(!setData.getFormDate().isEmpty()) {
                    String startDateVal = setData.getFormDate();
                    week_start_date.setText(startDateVal.substring(0, 10));
                }
                if(!setData.getToDate().isEmpty()) {
                    String endDateVal = setData.getToDate();
                    week_end_date.setText(endDateVal.substring(0, 10));
                }

                //new add
                if (setData.getPaidUpStock() != null) {
                    paidstock_ev.setText(setData.getPaidUpStock().toString());
                }

                if (setData.getParkAndSellStock() != null) {
                    parksell_ev.setText(setData.getParkAndSellStock().toString());
                }

                if (setData.getRoyaltyCollection() != null) {
                    royalty_select_tv.setText(setData.getRoyaltyCollection().toString());
                }

            }catch (Exception e){
                e.printStackTrace();
            }

            if(setData.getActualActivationTime()!=null) {

                String DateValue = setData.getActualActivationTime();
                String dateValue[] = DateValue.split("-");
                try {
                    String aactyear = dateValue[0];
                    String aactmonth = dateValue[1];
                    actualActivationTime.setText(aactmonth + " | " + aactyear);

                } catch (Exception e) {
                    actualActivationTime.setText(DateValue);
                }
            }
            DoOWalkins.setText(String.valueOf(setData.getDirectWalkin()));
            conversion_IEP.setText(setData.getIepConversion());
            week_title_tv.setText(setData.getDealerName());
            dealername_tv.setText(setData.getDealerName());

            int walkinthu=0,totalWalkin=0,walkin=0;

            walkinstOMS=findViewById(R.id.cr_walkinins);
            DoOWalkins=findViewById(R.id.cr_directorganicet);
            totalWalkins=findViewById(R.id.cr_totalnowalkinset);

            if (setData.getOmsWalkinThrough()!=null) {

                if(!walkinstOMS.getText().toString().trim().equalsIgnoreCase("")){
                    walkinthu=Integer.parseInt(walkinstOMS.getText().toString());
                }

            }

            if (setData.getDirectWalkin()!=null) {
                if(!DoOWalkins.getText().toString().trim().equalsIgnoreCase("")){
                    totalWalkin=Integer.parseInt(DoOWalkins.getText().toString());
                }
            }

            totalWalkin=walkinthu+walkin;

            totalWalkins.setText(String.valueOf(totalWalkin));

            serverDataCaluclation(setData);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void serverDataCaluclation(WeekReportSubmitModel resModel) {

        int stockTotal = 0, conversionIEP = 0, conversionNCD = 0, totalProc = 0, totalSales = 0, totalWalkin = 0, totalArgg = 0, warrantyP = 0;

        try {
            int stockData = 0, totalproc = 0, totalSale = 0;

            stockData = getvalue(resModel.getStcokOpeningMonth().toString());
            totalproc = getvalue(resModel.getTotalProcurement().toString());
            totalSale = getvalue(resModel.getTotalSales().toString());
            stockTotal = (stockData + totalproc) - totalSale;

        } catch (ArithmeticException e) {
            stockTotal = 0;
        } catch (Exception e) {
            stockTotal = 0;
        }
        currentStock.setText(String.valueOf(stockTotal));

        //

        try {

           /* int iepData = 0, leadiep = 0;

            iepData = getvalue(resModel.getIep().toString());
            leadiep = getvalue(resModel.getIepLead().toString());
            conversionIEP = iepData / leadiep;*/

            Double iepDatas, leadieps,conversionD;

            iepDatas = Double.valueOf(getvalue(resModel.getIep().toString()));
            leadieps = Double.valueOf(getvalue(resModel.getIepLead().toString()));

            conversionD = iepDatas / leadieps;

            conversionD = conversionD * 100;
            conversionIEP = (int) Math.round(conversionD);
            if(conversionIEP<0)conversionIEP=0;


        } catch (ArithmeticException e) {
            conversionIEP = 0;
        } catch (Exception e) {
            conversionIEP = 0;
        }
        conversion_IEP.setText(String.valueOf(conversionIEP));

        //

        try {

           /* int ncd = 0, leadncd = 0;
            ncd = getvalue(resModel.getNcd().toString());
            leadncd = getvalue(resModel.getNcpLead().toString());
            conversionNCD = ncd / leadncd;*/

            Double ncds, leadncds,conversionNCDD;

            ncds = Double.valueOf(getvalue(resModel.getNcd().toString()));
            leadncds = Double.valueOf(getvalue(resModel.getNcpLead().toString()));

            conversionNCDD = ncds / leadncds;

            conversionNCDD = conversionNCDD * 100;
            conversionNCD = (int) Math.round(conversionNCDD);
            if(conversionNCD<0)conversionNCD=0;

        } catch (ArithmeticException e) {
            conversionNCD = 0;
        } catch (Exception e) {
            conversionNCD = 0;
        }
        conversion_NCD.setText(String.valueOf(conversionNCD));
        //

        try {

            int cpt = 0, self = 0, iep = 0, ncd = 0, xmart = 0;
            cpt = getvalue(resModel.getCpt().toString());
            self = getvalue(resModel.getSelfProcurement().toString());
            iep = getvalue(resModel.getIep().toString());
            ncd = getvalue(resModel.getNcd().toString());
            xmart = getvalue(resModel.getXMart().toString());
            totalProc = cpt + self + iep + ncd + xmart;

        } catch (ArithmeticException e) {
            totalProc = 0;
        } catch (Exception e) {
            totalProc = 0;
        }
        total_procurement.setText(String.valueOf(totalProc));

        //

        try {

            int retails = 0, offload = 0;
            retails = getvalue(resModel.getRetailSales().toString());
            offload = getvalue(resModel.getOffloadSales().toString());
            totalSales = retails + offload;

        } catch (ArithmeticException e) {
            totalSales = 0;
        } catch (Exception e) {
            totalSales = 0;
        }
        total_sales.setText(String.valueOf(totalSales));

        //
        try {

            int walkin = 0, dwalkin = 0;
            walkin = getvalue(resModel.getOmsWalkinThrough().toString());
            dwalkin = getvalue(resModel.getDirectWalkin().toString());
            totalWalkin = dwalkin + walkin;

        } catch (ArithmeticException e) {
            totalWalkin = 0;
        } catch (Exception e) {
            totalWalkin = 0;
        }
        totalWalkins.setText(String.valueOf(totalWalkin));
        //

        try {

            int open = 0, close = 0;
            open = getvalue(resModel.getOpenAggregateCases().toString());
            close = getvalue(resModel.getClosedAggregateCases().toString());
            totalArgg = open + close;

        } catch (ArithmeticException e) {
            totalArgg = 0;
        } catch (Exception e) {
            totalArgg = 0;
        }
        totalAggregateCases.setText(String.valueOf(totalArgg));
        //

        Double warrantyPD;

        try {
            Double warrantyno, totalSale;
            warrantyno = Double.valueOf(getvalue(resModel.getWarrantyNo().toString()));
            totalSale = Double.valueOf(getvalue(resModel.getTotalSales().toString()));

            warrantyPD = warrantyno / totalSale;

            warrantyPD = warrantyPD * 100;
            warrantyP = (int) Math.round(warrantyPD);
            if(warrantyP<0)warrantyP=0;

        } catch (ArithmeticException e) {
            warrantyP = 0;
        } catch (Exception e) {
            warrantyP = 0;
        }
        warrantyPen.setText(String.valueOf(warrantyP));

    }

    private static void setViewAndChildrenEnabled(View view, boolean enabled) {
        view.setEnabled(enabled);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                setViewAndChildrenEnabled(child, enabled);
            }
        }
    }

    public static String getDayOfWEek(int a) {
        String str;
        switch (a) {
            case 1:
                str = "Mon";
                break;
            case 2:
                str = "Tue";
                break;
            case 3:
                str = "Wed";
                break;
            case 4:
                str = "Thu";
                break;
            case 5:
                str = "Fri";
                break;
            case 6:
                str = "Sat";
                break;
            case 7:
                str = "Sun";
                break;
            default:
                str = "mon";
        }
        return str;
    }

    public int getvalue(String values) {
        if (values != null && !values.isEmpty()) {
            return Integer.parseInt(values.toString());
        }
        return 0;
    }
}
