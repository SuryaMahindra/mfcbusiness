package com.mfcwl.mfc_dealer.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.mfc.stencilcamera.AIViewCamera.StencilCamera;
import com.mfc.stencilcamera.AIViewCamera.Utils.StencilCamConstant;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Fragment.StockFragment;
import com.mfcwl.mfc_dealer.InstanceCreate.ImageLibraryInstance;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.Global;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import at.grabner.circleprogress.CircleProgressView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.mfcwl.mfc_dealer.Utility.CommonMethods.GALLERY_RESCALE_VALUE;

public class ImageUploadActivity1 extends AppCompatActivity {

    @BindView(R.id.imagedonebtn)
    public Button imagedonebtn;

    @BindView(R.id.exteriorTab)
    public TextView exteriorTab;

    @BindView(R.id.interiorTab)
    public TextView interiorTab;

    @BindView(R.id.videos_upload)
    public TextView videos_upload;


    @BindView(R.id.exteriorItem)
    public LinearLayout exteriorItem;

    @BindView(R.id.interiorItem)
    public LinearLayout interiorItem;

    @BindView(R.id.samplesbtn)
    public Button samplesbtn;


    @BindView(R.id.coverimg)
    public ImageView coverimg;
    @BindView(R.id.coverimgTrans)
    public ImageView coverimgTrans;

    @BindView(R.id.frontview)
    public ImageView frontview;
    @BindView(R.id.frontviewTrans)
    public ImageView frontviewTrans;


    @BindView(R.id.rearview)
    public ImageView rearview;
    @BindView(R.id.rearviewTrans)
    public ImageView rearviewTrans;

    @BindView(R.id.leftsideview)
    public ImageView leftsideview;

    @BindView(R.id.rightsideview)
    public ImageView rightsideview;


    @BindView(R.id.rearleftview)
    public ImageView rearleftview;


    @BindView(R.id.frontright)
    public ImageView frontright;

    @BindView(R.id.rearright)
    public ImageView rearright;

    @BindView(R.id.wheelimg)
    public ImageView wheelimg;

    @BindView(R.id.frontseat)
    public ImageView frontseat;

    @BindView(R.id.backseat)
    public ImageView backseat;

    @BindView(R.id.odometer)
    public ImageView odometer;

    @BindView(R.id.dasborad)
    public ImageView dasborad;

    @BindView(R.id.steering)
    public ImageView steering;

    @BindView(R.id.frontrowseat)
    public ImageView frontrowseat;

    @BindView(R.id.backrowseat)
    public ImageView backrowseat;

    @BindView(R.id.doorimg)
    public ImageView doorimg;

    @BindView(R.id.backseatsideTrans)
    public ImageView backseatsideTrans;

    @BindView(R.id.frontseatsideTrans)
    public ImageView frontseatsideTrans;

    @BindView(R.id.frontseatTrans)
    public ImageView frontseatTrans;

    @BindView(R.id.steeringTrans)
    public ImageView steeringTrans;


    @BindView(R.id.dasboardTrans)
    public ImageView dasboardTrans;

    @BindView(R.id.odometerTrans)
    public ImageView odometerTrans;

    @BindView(R.id.backseatTrans)
    public ImageView backseatTrans;

    @BindView(R.id.doorTrans)
    public ImageView doorTrans;

    public ImageView coverimgClose;
    public ImageView frontviewClose;
    public ImageView rearviewClose;
    public ImageView leftsideview_close;
    public ImageView rightsideview_close;
    public ImageView rearleftview_close;
    public ImageView frontright_close;
    public ImageView rearright_close;
    public ImageView wheelimg_close;
    public ImageView frontseat_close;
    public ImageView backseat_close;
    public ImageView odometer_close;
    public ImageView dasborad_close;
    public ImageView steering_close;
    public ImageView frontrowseat_close;
    public ImageView backrowseat_close;
    public ImageView doorimg_close;
    public static TextView image_upload_count;


    static CircleProgressView mCircleView_front, mCircleView_rear, mCircleView_leftside, mCircleView_rightside, mCircleView_rearleft, mCircleView_frontright, mCircleView_rearright, mCircleView_wheels,
            mCircleView_frontseat,
            mCircleView_backseat,
            mCircleView_odameter,
            mCircleView_dasboard,
            mCircleView_steering,
            mCircleView_frontseatside,
            mCircleView_backseatside,
            mCircleView_door,
            mCircleView_cover;


    static String cover_image = "cover_image", front_image = "front_image", rear_image = "rear_image", leftside_image = "left_side_view", rightside_image = "rightside_image",
            rearleft_image = "rearleft_image", frontright_image = "frontright_image", rearright_image = "rear_right_view",
            front_seats_image = "front_seats",
            wheels_image = "wheels_tyres",
            back_seats_image = "back_seats",
            odometer_image = "odometer",
            dashboard_image = "dashboard",
            steering_image = "steering",
            front_row_side_image = "front_row_side",
            back_row_side_image = "back_row_side",
            trunk_door_open_view_image = "trunk_door_open_view";

    public boolean coverimgFlag,
            frontviewFlag,
            leftsideviewFlag,
            rightsideviewFlag,
            rearleftviewFlag,
            rearrightviewFlag,
            rearviewFlag,
            frontrightviewFlag,
            wheelstviewFlag,
            front_seatsFlag,
            back_seatsFlag,
            odometerFlag,
            dashboardFlag,
            steeringFlag,
            front_row_sideFlag,
            back_row_sideFlag,
            trunk_door_open_viewFlag;

    boolean front_image_flag = true;
    File file;
    Uri fileUri;

    int width_x = 700, height_y = 700;
    boolean whichSource = false;
    public static Activity activity;

    TextView coverimglbl,
            frontviewlbl,
            rearviewlbl,
            leftsidelbl,
            rightsidelbl,
            rearleftlbl,
            frontrightlbl,
            rearrightlbl,
            wheelslbl,
            frontseatlbl,
            backseatlbl,
            odameterlbl,
            dasboardlbl,
            steeringlbl,
            frontrowlbl,
            backrowlbl,
            doorlbl;



    public String[] imageurl;
    public String[] imagename;
    String stock_id,video_url="",regno="";


    public static String path = " ";
    public static ImageView uploadback;
    public static int resultCodeforImage;

    private static int RESULT_LOAD = 1;
    private static int CAMERA_LOAD = 2;
    protected static Uri image;
    private static ImageView imageView;
    private static final String fileName = "Aliens_Images";


    private static int RESULT_LOAD2 = 1;
    public static Uri image1;
    private static final int MY_PERMISSIONS_READ_PHONE_STATE = 0;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 0;

    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final String IMAGE_DIRECTORY_NAME = "MFC Business Images";

    public static boolean imageIsAvaliable = false;
    String TAG = getClass().getSimpleName();

   private StencilCamera stencilCamera;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate: ");
        setContentView(R.layout.activity_imageupload);
        ButterKnife.bind(this);
        registerReceiver();
        imageurl = new String[]{};
        activity = this;
        stencilCamera = new StencilCamera(ImageUploadActivity1.this);
        exteriorTab.setTextColor(getResources().getColor(R.color.yellow));
        interiorTab.setTextColor(getResources().getColor(R.color.White));

        exteriorItem.setVisibility(View.VISIBLE);
        interiorItem.setVisibility(View.GONE);


        MethodOfVisibleLayout();


        uploadback = (ImageView) findViewById(R.id.uploadback);

        image_upload_count = (TextView) findViewById(R.id.image_upload_count);


        coverimgClose = (ImageView) findViewById(R.id.coverimgClose);
        frontviewClose = (ImageView) findViewById(R.id.frontviewClose);
        rearviewClose = (ImageView) findViewById(R.id.rearviewClose);
        leftsideview_close = (ImageView) findViewById(R.id.leftsideview_close);
        rightsideview_close = (ImageView) findViewById(R.id.rightsideview_close);
        rearleftview_close = (ImageView) findViewById(R.id.rearleftview_close);
        frontright_close = (ImageView) findViewById(R.id.frontright_close);
        rearright_close = (ImageView) findViewById(R.id.rearright_close);
        wheelimg_close = (ImageView) findViewById(R.id.wheelimg_close);
        frontseat_close = (ImageView) findViewById(R.id.frontseat_close);
        backseat_close = (ImageView) findViewById(R.id.backseat_close);
        odometer_close = (ImageView) findViewById(R.id.odometer_close);
        dasborad_close = (ImageView) findViewById(R.id.dasborad_close);
        steering_close = (ImageView) findViewById(R.id.steering_close);
        frontrowseat_close = (ImageView) findViewById(R.id.frontrowseat_close);
        backrowseat_close = (ImageView) findViewById(R.id.backrowseat_close);
        doorimg_close = (ImageView) findViewById(R.id.doorimg_close);

        coverimgClose.setVisibility(View.INVISIBLE);
        frontviewClose.setVisibility(View.INVISIBLE);
        rearviewClose.setVisibility(View.INVISIBLE);
        leftsideview_close.setVisibility(View.INVISIBLE);
        rightsideview_close.setVisibility(View.INVISIBLE);
        rearleftview_close.setVisibility(View.INVISIBLE);
        frontright_close.setVisibility(View.INVISIBLE);
        rearright_close.setVisibility(View.INVISIBLE);
        wheelimg_close.setVisibility(View.INVISIBLE);
        frontseat_close.setVisibility(View.INVISIBLE);
        backseat_close.setVisibility(View.INVISIBLE);
        odometer_close.setVisibility(View.INVISIBLE);
        dasborad_close.setVisibility(View.INVISIBLE);
        steering_close.setVisibility(View.INVISIBLE);
        frontrowseat_close.setVisibility(View.INVISIBLE);
        backrowseat_close.setVisibility(View.INVISIBLE);
        doorimg_close.setVisibility(View.INVISIBLE);

        mCircleView_front = (CircleProgressView) findViewById(R.id.mCircleView_front);
        mCircleView_rear = (CircleProgressView) findViewById(R.id.mCircleView_rear);
        mCircleView_leftside = (CircleProgressView) findViewById(R.id.mCircleView_leftside);
        mCircleView_rightside = (CircleProgressView) findViewById(R.id.mCircleView_rightside);
        mCircleView_rearleft = (CircleProgressView) findViewById(R.id.mCircleView_rearleft);
        mCircleView_frontright = (CircleProgressView) findViewById(R.id.mCircleView_frontright);
        mCircleView_rearright = (CircleProgressView) findViewById(R.id.mCircleView_rearright);
        mCircleView_wheels = (CircleProgressView) findViewById(R.id.mCircleView_wheels);
        mCircleView_frontseat = (CircleProgressView) findViewById(R.id.mCircleView_frontseat);
        mCircleView_backseat = (CircleProgressView) findViewById(R.id.mCircleView_backseat);
        mCircleView_odameter = (CircleProgressView) findViewById(R.id.mCircleView_odameter);
        mCircleView_dasboard = (CircleProgressView) findViewById(R.id.mCircleView_dasboard);
        mCircleView_steering = (CircleProgressView) findViewById(R.id.mCircleView_steering);
        mCircleView_frontseatside = (CircleProgressView) findViewById(R.id.mCircleView_frontseatside);
        mCircleView_backseatside = (CircleProgressView) findViewById(R.id.mCircleView_backseatside);
        mCircleView_door = (CircleProgressView) findViewById(R.id.mCircleView_door);
        mCircleView_cover = (CircleProgressView) findViewById(R.id.mCircleView_cover);

        coverimglbl = (TextView) findViewById(R.id.coverimglbl);
        frontviewlbl = (TextView) findViewById(R.id.frontviewlbl);
        rearviewlbl = (TextView) findViewById(R.id.rearviewlbl);
        leftsidelbl = (TextView) findViewById(R.id.leftsidelbl);
        rightsidelbl = (TextView) findViewById(R.id.rightsidelbl);
        rearleftlbl = (TextView) findViewById(R.id.rearleftlbl);
        frontrightlbl = (TextView) findViewById(R.id.frontrightlbl);
        rearrightlbl = (TextView) findViewById(R.id.rearrightlbl);
        wheelslbl = (TextView) findViewById(R.id.wheelslbl);
        frontseatlbl = (TextView) findViewById(R.id.frontseatlbl);
        backseatlbl = (TextView) findViewById(R.id.backseatlbl);
        odameterlbl = (TextView) findViewById(R.id.odameterlbl);
        dasboardlbl = (TextView) findViewById(R.id.dasboardlbl);
        steeringlbl = (TextView) findViewById(R.id.steeringlbl);
        frontrowlbl = (TextView) findViewById(R.id.frontrowlbl);
        backrowlbl = (TextView) findViewById(R.id.backrowlbl);
        doorlbl = (TextView) findViewById(R.id.doorlbl);


        mCircleView_cover.setVisibility(View.GONE);
        mCircleView_front.setVisibility(View.GONE);
        mCircleView_rear.setVisibility(View.GONE);
        mCircleView_leftside.setVisibility(View.GONE);
        mCircleView_rightside.setVisibility(View.GONE);
        mCircleView_rearleft.setVisibility(View.GONE);
        mCircleView_frontright.setVisibility(View.GONE);
        mCircleView_rearright.setVisibility(View.GONE);
        mCircleView_wheels.setVisibility(View.GONE);
        mCircleView_frontseat.setVisibility(View.GONE);
        mCircleView_backseat.setVisibility(View.GONE);
        mCircleView_odameter.setVisibility(View.GONE);
        mCircleView_dasboard.setVisibility(View.GONE);
        mCircleView_steering.setVisibility(View.GONE);
        mCircleView_frontseatside.setVisibility(View.GONE);
        mCircleView_backseatside.setVisibility(View.GONE);
        mCircleView_door.setVisibility(View.GONE);

        if (ImageLibraryInstance.getInstance().getUploadstatus().equalsIgnoreCase("detailstatus")) {
            AutoFillMethods();
        }

            try {
                stock_id = getIntent().getStringExtra("stock_id");
            } catch (Exception e) {
            }
            try {
                regno = getIntent().getStringExtra("regno");
            } catch (Exception e) {
            }
            try {
                video_url = getIntent().getStringExtra("video_url");
            } catch (Exception e) {
            }





        uploadback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CommonMethods.getstringvaluefromkey(activity, "stockdetails").equalsIgnoreCase("stockdetails")) {

                    Intent intent = new Intent(ImageUploadActivity1.this, StockStoreInfoActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

                    // StockStoreInfoActivity.imageload(stock_id,activity);

                   /* StockStoreInfoActivity.imageload(stock_id,activity);
                    finish();*/

                    //vellaichamy
                    finish();

                } else {
                    CommonMethods.setvalueAgainstKey(activity, "stockcreated", "true");
                    Intent in_main = new Intent(ImageUploadActivity1.this, MainActivity.class);
                    startActivity(in_main);
                    finish();
                }
            }
        });


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onLowMemory() {
        System.gc();
        super.onLowMemory();
    }

    public void AutoFillMethods() {

        imageurl = getIntent().getStringArrayExtra("imageurl");
        imagename = getIntent().getStringArrayExtra("imagename");
        stock_id = getIntent().getStringExtra("stock_id");
        video_url = getIntent().getStringExtra("video_url");
        regno = getIntent().getStringExtra("regno");

        //  Log.e("imageurl ", "imageurl " + imageurl);

        String count = getIntent().getStringExtra("imageCount");

        image_upload_count.setText("Upload images (" + count + "/17)");

        String cover_image_url = "", front_view_url = "", rear_view_url = "", left_side_view_url = "", right_side_view_url = "",
                rear_left_view_url = "", front_right_view_url = "", rear_right_view_url = "", wheels_tyres_ur = "",
                front_seats_url = "", back_seats_url = "", odometer_url = "", dashboard_url = "", steering_url = "",
                front_row_side_url = "", back_row_side = "", trunk_door_open_view = "", back_row_side_url = "",
                trunk_door_open_view_url = "";


        for (int i = 0; i < imagename.length; i++) {


            if (imagename[i].equalsIgnoreCase("cover_image")) {
                cover_image_url = imageurl[i];
                coverimgFlag = true;
                coverimgClose.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("front_view")) {
                front_view_url = imageurl[i];
                frontviewFlag = true;
                frontviewClose.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("rear_view")) {
                rear_view_url = imageurl[i];
                rearviewFlag = true;
                rearviewClose.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("left_side_view")) {
                left_side_view_url = imageurl[i];
                leftsideviewFlag = true;
                leftsideview_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("right_side_view")) {
                right_side_view_url = imageurl[i];
                rightsideviewFlag = true;
                rightsideview_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("rear_left_view")) {
                rear_left_view_url = imageurl[i];
                rearleftviewFlag = true;
                rearleftview_close.setVisibility(View.VISIBLE);


            }
            if (imagename[i].equalsIgnoreCase("front_right_view")) {
                front_right_view_url = imageurl[i];
                frontrightviewFlag = true;
                frontright_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("rear_right_view")) {
                rear_right_view_url = imageurl[i];
                rearrightviewFlag = true;
                rearright_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("wheels_tyres")) {
                wheels_tyres_ur = imageurl[i];
                wheelstviewFlag = true;
                wheelimg_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("front_seats")) {
                front_seats_url = imageurl[i];
                front_seatsFlag = true;
                frontseat_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("back_seats")) {
                back_seats_url = imageurl[i];
                back_seatsFlag = true;
                backseat_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("odometer")) {
                odometer_url = imageurl[i];
                odometerFlag = true;
                odometer_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("dashboard")) {
                dashboard_url = imageurl[i];
                dashboardFlag = true;
                dasborad_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("steering")) {
                steering_url = imageurl[i];
                steeringFlag = true;
                steering_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("front_row_side")) {
                front_row_side_url = imageurl[i];
                front_row_sideFlag = true;
                frontrowseat_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("back_row_side")) {
                back_row_side_url = imageurl[i];
                back_row_sideFlag = true;
                backrowseat_close.setVisibility(View.VISIBLE);
            }
            if (imagename[i].equalsIgnoreCase("trunk_door_open_view")) {
                trunk_door_open_view_url = imageurl[i];
                trunk_door_open_viewFlag = true;
                doorimg_close.setVisibility(View.VISIBLE);
            }
        }

        if (!cover_image_url.equalsIgnoreCase("")) {
            try {
               /* Picasso.Builder builder = new Picasso.Builder(activity);
                builder.build()
                        .load(cover_image_url)
                        .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                        .placeholder(R.drawable.loading_big)
                        .noFade()
                        .into(coverimg);*/
                //setBitmapFromURL(cover_image_url,"cover_image_url");
                loadImageFromURL(this, cover_image_url, coverimg);
            } catch (Exception e) {
                Log.e("Exception ", "Exception " + e.toString());
            }
            //setBitmapFromURL(cover_image_url,"cover_image_url");


        }
        if (!front_view_url.equalsIgnoreCase("")) {
            try {
           /* Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build().load(front_view_url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .placeholder(R.drawable.loading_big)
                    .noFade()
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).into(frontview);
                //setBitmapFromURL(front_view_url,"front_view_url");*/


                loadImageFromURL(this, front_view_url, frontview);
            } catch (Exception e) {
                Log.e("Exception ", "Exception " + e.toString());
            }
            //setBitmapFromURL(front_view_url,"front_view_url");
        }
        if (!rear_view_url.equalsIgnoreCase("")) {
            try {
           /* Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build().load(rear_view_url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .placeholder(R.drawable.loading_big)
                    .noFade()
                    .into(rearview);
                //setBitmapFromURL(rear_view_url,"rear_view_url");*/
                loadImageFromURL(this, rear_view_url, rearview);

            } catch (Exception e) {
                Log.e("Exception ", "Exception " + e.toString());
            }
            //setBitmapFromURL(rear_view_url,"rear_view_url");
        }
        if (!left_side_view_url.equalsIgnoreCase("")) {
            try {
           /* Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build().load(left_side_view_url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .placeholder(R.drawable.loading_big)
                    .noFade()
                    .into(leftsideview);
                //setBitmapFromURL(left_side_view_url,"left_side_view_url");*/
                loadImageFromURL(this, left_side_view_url, leftsideview);

            } catch (Exception e) {
                Log.e("Exception ", "Exception " + e.toString());
            }
            //setBitmapFromURL(left_side_view_url,"left_side_view_url");
        }
        if (!right_side_view_url.equalsIgnoreCase("")) {
            try {
          /*  Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build().load(right_side_view_url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .placeholder(R.drawable.loading_big)
                    .noFade()
                    .into(rightsideview);
                //setBitmapFromURL(right_side_view_url,"right_side_view_url");*/
                loadImageFromURL(this, right_side_view_url, rightsideview);

            } catch (Exception e) {
                Log.e("Exception ", "Exception " + e.toString());
            }
            //setBitmapFromURL(right_side_view_url,"right_side_view_url");
        }

        ///


        if (!rear_left_view_url.equalsIgnoreCase("")) {
            try {
          /*  Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build().load(rear_left_view_url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .placeholder(R.drawable.loading_big)
                    .noFade()
                    .into(rearleftview);
                //setBitmapFromURL(rear_left_view_url,"rear_left_view_url");*/
                loadImageFromURL(this, rear_left_view_url, rearleftview);

            } catch (Exception e) {
                Log.e("Exception ", "Exception " + e.toString());
            }
            //setBitmapFromURL(rear_left_view_url,"rear_left_view_url");
        }

        if (!front_right_view_url.equalsIgnoreCase("")) {
            try {
          /*  Picasso.Builder builder = new Picasso.Builder(activity);
            builder.build().load(front_right_view_url)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .placeholder(R.drawable.loading_big)
                    .noFade()
                    .into(frontright);
                //setBitmapFromURL(front_right_view_url,"front_right_view_url");*/
                loadImageFromURL(this, front_right_view_url, frontright);

            } catch (Exception e) {
                Log.e("Exception ", "Exception " + e.toString());
            }
            //setBitmapFromURL(front_right_view_url,"front_right_view_url");
        }

        if (!rear_right_view_url.equalsIgnoreCase("")) {
            try {

                loadImageFromURL(this, rear_right_view_url, rearright);

              /*  Picasso.Builder builder = new Picasso.Builder(activity);
                builder.build().load(rear_right_view_url)
                        .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                        .placeholder(R.drawable.loading_big)
                        .noFade()
                        .into(rearright);*/
                // setBitmapFromURL(rear_right_view_url,"rear_right_view_url");
            } catch (Exception e) {
                Log.e("Exception ", "Exception " + e.toString());
            }
            //setBitmapFromURL(rear_right_view_url,"rear_right_view_url");

        }

        if (!wheels_tyres_ur.equalsIgnoreCase("")) {
            try {

                loadImageFromURL(this, wheels_tyres_ur, wheelimg);

              /*  Picasso.Builder builder = new Picasso.Builder(activity);
                builder.build().load(wheels_tyres_ur)
                        .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                        .placeholder(R.drawable.loading_big)
                        .noFade()
                        .into(wheelimg);*/
                // setBitmapFromURL(wheels_tyres_ur,"wheels_tyres_ur");
            } catch (Exception e) {
                Log.e("Exception ", "Exception " + e.toString());
            }
            //setBitmapFromURL(wheels_tyres_ur,"wheels_tyres_ur");
        }

        if (!front_seats_url.equalsIgnoreCase("")) {
            try {

                loadImageFromURL(this, front_seats_url, frontseat);

               /* Picasso.Builder builder = new Picasso.Builder(activity);
                builder.build().load(front_seats_url)
                        .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                        .placeholder(R.drawable.loading_big)
                        .noFade()
                        .into(frontseat);*/
                //setBitmapFromURL(front_seats_url,"front_seats_url");
            } catch (Exception e) {
                Log.e("Exception ", "Exception " + e.toString());
            }
            //setBitmapFromURL(front_seats_url,"front_seats_url");
        }

        if (!back_seats_url.equalsIgnoreCase("")) {
            try {

                loadImageFromURL(this, back_seats_url, backseat);

              /*  Picasso.Builder builder = new Picasso.Builder(activity);
                builder.build().load(back_seats_url)
                        .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                        .placeholder(R.drawable.loading_big)
                        .noFade()
                        .into(backseat);*/
                //setBitmapFromURL(back_seats_url,"back_seats_url");
            } catch (Exception e) {
                Log.e("Exception ", "Exception " + e.toString());
            }
            //setBitmapFromURL(back_seats_url,"back_seats_url");
        }


        if (!odometer_url.equalsIgnoreCase("")) {
            try {
                loadImageFromURL(this, odometer_url, odometer);

              /*  Picasso.Builder builder = new Picasso.Builder(activity);
                builder.build().load(odometer_url)
                        .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                        .placeholder(R.drawable.loading_big)
                        .noFade()
                        .into(odometer);*/
                //setBitmapFromURL(odometer_url,"odometer_url");
            } catch (Exception e) {
                Log.e("Exception ", "Exception " + e.toString());
            }
            //setBitmapFromURL(odometer_url,"odometer_url");
        }

        if (!dashboard_url.equalsIgnoreCase("")) {
            try {

                loadImageFromURL(this, dashboard_url, dasborad);


              /*  Picasso.Builder builder = new Picasso.Builder(activity);
                builder.build().load(dashboard_url)
                        .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                        .placeholder(R.drawable.loading_big)
                        .noFade()
                        .into(dasborad);*/
                //setBitmapFromURL(dashboard_url,"dashboard_url");
            } catch (Exception e) {
                Log.e("Exception ", "Exception " + e.toString());
            }
            //setBitmapFromURL(dashboard_url,"dashboard_url");
        }

        if (!steering_url.equalsIgnoreCase("")) {
            try {

                loadImageFromURL(this, steering_url, steering);

               /* Picasso.Builder builder = new Picasso.Builder(activity);
                builder.build().load(steering_url)
                        .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                        .placeholder(R.drawable.loading_big)
                        .noFade()
                        .into(steering);*/
                //  setBitmapFromURL(steering_url,"steering_url");
            } catch (Exception e) {
                Log.e("Exception ", "Exception " + e.toString());
            }
            //setBitmapFromURL(steering_url,"steering_url");
        }

        if (!front_row_side_url.equalsIgnoreCase("")) {
            try {

                loadImageFromURL(this, front_row_side_url, frontrowseat);


              /*  Picasso.Builder builder = new Picasso.Builder(activity);
                builder.build().load(front_row_side_url)
                        .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                        .placeholder(R.drawable.loading_big)
                        .noFade()
                        .into(frontrowseat);*/
                //setBitmapFromURL(front_row_side_url,"front_row_side_url");
            } catch (Exception e) {
                Log.e("Exception ", "Exception " + e.toString());
            }
            //setBitmapFromURL(front_row_side_url,"front_row_side_url");
        }

        if (!back_row_side_url.equalsIgnoreCase("")) {
            try {

                loadImageFromURL(this, back_row_side_url, backrowseat);

              /*  Picasso.Builder builder = new Picasso.Builder(activity);
                builder.build().load(back_row_side_url)
                        .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                        .placeholder(R.drawable.loading_big)
                        .noFade()
                        .into(backrowseat);*/
                //setBitmapFromURL(back_row_side_url,"back_row_side_url");
            } catch (Exception e) {
                Log.e("Exception ", "Exception " + e.toString());
            }
            //setBitmapFromURL(back_row_side_url,"back_row_side_url");
        }

        if (!trunk_door_open_view_url.equalsIgnoreCase("")) {
            try {
                loadImageFromURL(this, trunk_door_open_view_url, doorimg);

              /*  Picasso.Builder builder = new Picasso.Builder(activity);
                builder.build().load(trunk_door_open_view_url)
                        .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                        .placeholder(R.drawable.loading_big)
                        .noFade()
                        .into(doorimg);*/
                // setBitmapFromURL(trunk_door_open_view_url,"trunk_door_open_view_url");
            } catch (Exception e) {
                Log.e("Exception ", "Exception " + e.toString());
            }
            //setBitmapFromURL(trunk_door_open_view_url,"trunk_door_open_view_url");
        }


    }

    @OnClick(R.id.samplesbtn)
    public void samplesbtn(View view) {

        Intent in_main = new Intent(ImageUploadActivity1.this, ViewSamplesActivity.class);
        startActivity(in_main);

    }

    @OnClick(R.id.coverimgClose)
    public void coverimgClose(View view) {
        CloseAert("coverimgClose");

    }

    @OnClick(R.id.frontviewClose)
    public void frontviewClose(View view) {
        CloseAert("frontviewClose");

    }

    @OnClick(R.id.rearviewClose)
    public void rearviewClose(View view) {

        CloseAert("rearviewClose");

    }

    @OnClick(R.id.leftsideview_close)
    public void leftsideview_close(View view) {
        CloseAert("leftsideview_close");

    }

    @OnClick(R.id.frontright_close)
    public void frontright_close(View view) {
        CloseAert("frontright_close");

    }

    @OnClick(R.id.rightsideview_close)
    public void rightsideview_close(View view) {
        CloseAert("rightsideview_close");

    }

    @OnClick(R.id.rearleftview_close)
    public void rearleftview_close(View view) {
        CloseAert("rearleftview_close");

    }

    @OnClick(R.id.rearright_close)
    public void rearright_close(View view) {
        CloseAert("rearright_close");

    }

    @OnClick(R.id.wheelimg_close)
    public void wheelimg_close(View view) {
        CloseAert("wheelimg_close");

    }

    @OnClick(R.id.backseat_close)
    public void backseat_close(View view) {

        CloseAert("backseat_close");
    }

    @OnClick(R.id.odometer_close)
    public void odometer_close(View view) {

        CloseAert("odometer_close");
    }

    @OnClick(R.id.dasborad_close)
    public void dasborad_close(View view) {

        CloseAert("dasborad_close");
    }

    @OnClick(R.id.frontseat_close)
    public void frontseat_close(View view) {

        CloseAert("frontseat_close");

    }

    @OnClick(R.id.steering_close)
    public void steering_close(View view) {

        CloseAert("steering_close");
    }

    @OnClick(R.id.frontrowseat_close)
    public void frontrowseat_close(View view) {

        CloseAert("frontrowseat_close");
    }

    @OnClick(R.id.backrowseat_close)
    public void backrowseat_close(View view) {

        CloseAert("backrowseat_close");
    }

    @OnClick(R.id.doorimg_close)
    public void doorimg_close(View view) {

        CloseAert("doorimg_close");
    }

    @OnClick(R.id.coverimgTrans)
    public void coverimgTrans(View view) {

        selectImage("cover_image_request");


    }


    @OnClick(R.id.frontviewTrans)
    public void frontview(View view) {
        selectImage("front_image_request");
    }


    @OnClick(R.id.rearviewTrans)
    public void rearview(View view) {
        selectImage("rear_image_request");
    }


    @OnClick(R.id.leftsideviewTrans)
    public void leftsideviewTrans(View view) {
        selectImage("leftside_image_request");
    }

    @OnClick(R.id.rightsideviewTrans)
    public void rightsideviewTrans(View view) {
        selectImage("rightside_image_request");
    }

    @OnClick(R.id.rearleftviewTrans)
    public void rearleftviewTrans(View view) {

        selectImage("rearleft_image_request");
    }

    @OnClick(R.id.frontrightviewTrans)
    public void frontrightviewTrans(View view) {
        selectImage("frontright_image_request");
    }

    @OnClick(R.id.rearrightviewTrans)
    public void rearrightviewTrans(View view) {
        selectImage("rear_right_view");
    }

    @OnClick(R.id.wheelsTrans)
    public void wheelsTrans(View view) {
        selectImage("wheels_tyres");
    }

    @OnClick(R.id.frontseatTrans)
    public void frontseatTrans(View view) {
        selectImage("front_seats");
    }

    @OnClick(R.id.backseatTrans)
    public void backseatTrans(View view) {
        selectImage("back_seats");
    }

    @OnClick(R.id.backrowseat)
    public void backrowseat(View view) {
        selectImage("back_row_side");
    }


    @OnClick(R.id.odometerTrans)
    public void odometerTrans(View view) {
        selectImage("odometer");
    }


    @OnClick(R.id.dasboardTrans)
    public void dasboardTrans(View view) {
        selectImage("dashboard");
    }

    @OnClick(R.id.steeringTrans)
    public void steeringTrans(View view) {
        selectImage("steering");
    }

    @OnClick(R.id.frontseatsideTrans)
    public void frontseatsideTrans(View view) {
        selectImage("front_row_side");
    }


    @OnClick(R.id.doorTrans)
    public void doorTrans(View view) {
        selectImage("trunk_door_open_view");
    }

    @OnClick(R.id.imagedonebtn)
    public void imagedonebtn(View view) {
        if (CommonMethods.getstringvaluefromkey(this, "stockdetails").equalsIgnoreCase("stockdetails")) {
           /* Intent intent = new Intent(ImageUploadActivity1.this, StockStoreInfoActivity.class);
            startActivity(intent);*/
            // StockStoreInfoActivity.imageload(stock_id,activity);

            StockFragment.onBack = true;
            Intent intent = new Intent(ImageUploadActivity1.this, StockStoreInfoActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

        } else {
            StockFragment.onBack = false;
            CommonMethods.setvalueAgainstKey(this, "stockcreated", "true");
            Intent in_main = new Intent(ImageUploadActivity1.this, MainActivity.class);
            startActivity(in_main);
            finish();
        }
    }


    @OnClick(R.id.exteriorTab)
    public void exteriorTab(View view) {

        exteriorTab.setTextColor(getResources().getColor(R.color.yellow));
        interiorTab.setTextColor(getResources().getColor(R.color.White));

        exteriorItem.setVisibility(View.VISIBLE);
        interiorItem.setVisibility(View.GONE);
        CommonMethods.setvalueAgainstKey(this, "WhichTab", "exteriorTab");
    }

    @OnClick(R.id.interiorTab)
    public void interiorTab(View view) {

        exteriorTab.setTextColor(getResources().getColor(R.color.White));
        interiorTab.setTextColor(getResources().getColor(R.color.yellow));

        exteriorItem.setVisibility(View.GONE);
        interiorItem.setVisibility(View.VISIBLE);
        CommonMethods.setvalueAgainstKey(this, "WhichTab", "Interior");

    }

    @OnClick(R.id.videos_upload)
    public void videos_upload(View view) {

        Intent intent=new Intent(ImageUploadActivity1.this, VideoActivity.class);
        intent.putExtra("stock_id", stock_id);
        intent.putExtra("video_url", video_url);
        intent.putExtra("regno", regno);
        startActivity(intent);
    }


    private void captureImage(String stencilName,String lblname) {

        try {

            stencilCamera.openCustomCamera(ImageUploadActivity1.this, stencilName, lblname);

           /* Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            } else {
                File file = new File(fileUri.getPath());
                Uri photoUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", file);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            }
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            if (intent.resolveActivity(getApplicationContext().getPackageManager()) != null) {
                startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
            }*/

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e("fileUri ", "fileUri " + e.toString());
            Log.e("Error:get capture image", e.getMessage());

        }
    }

    public Uri  getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                // Log.e("fileUri ", "getOutputMediaFile ");
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }
        //   Log.e("fileUri ", "mediaFile " + mediaFile);
        return mediaFile;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("requestCode", "requestCode" + requestCode);
        Log.e("resultCode", "resultCode" + resultCode);
        Log.e("data", "data" + data);

        try {
            //   Log.e("onActivityResult", "onActivityResult-13");

            if (requestCode == RESULT_LOAD && resultCode == RESULT_OK && data != null) {
                //  Log.e("onActivityResult", "onActivityResult-14");
                imageIsAvaliable = true;
                image1 = data.getData();
                Intent intent = new Intent(this, EditImageActivity.class);
                // intent.putExtra("imageUri", image1);
                startActivity(intent);

            } else if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                try {
                    //   Log.e("onActivityResult", "onActivityResult-15");
                    if (resultCode != 0 /*& CommonMethods.getstringvaluefromkey(this, "imageIsAvaliable")
                            .equalsIgnoreCase("true")*/) {
                        imageIsAvaliable = true;
                        image1 = fileUri;
                        Intent intent = new Intent(this, EditImageActivity.class);
                        // intent.putExtra("imageUri", fileUri);
                        startActivity(intent);

                    } else {
                        imageIsAvaliable = false;
                        //finish();
                    }

                } catch (Exception e) {
                    imageIsAvaliable = false;
                    //     Log.e("onActivityResult", "onActivityResult-16=" + e);
                    e.printStackTrace();
                }
            } else {
                imageIsAvaliable = false;
               // finish();
            }

        } catch (Exception e) {
            //  Log.e("onActivityResult", "onActivityResult-17=" + e);
            e.printStackTrace();
            imageIsAvaliable = false;
        }

    }

    private void checkMPermissions() {

        ActivityCompat.requestPermissions(ImageUploadActivity1.this,
                new String[]{
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,

                }, MY_PERMISSIONS_REQUEST_READ_CONTACTS);


    }

    boolean checkedPermissionsmarshmellow = false;

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    checkedPermissionsmarshmellow = true;

                } else {
                    checkMPermissions();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    private void selectImage(String imageStatus) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.camera);
        dialog.setCancelable(true);

        TextView pictureType = dialog.findViewById(R.id.pictureType);
        LinearLayout select_camera = dialog.findViewById(R.id.select_camera);
        LinearLayout select_gallery = dialog.findViewById(R.id.select_gallery);

        imageIsAvaliable = false;
        CommonMethods.setvalueAgainstKey(activity, "imageIsAvaliable", "false");

        select_camera.setOnClickListener(view2 -> {

            whichSource = false;

            if (imageStatus.equalsIgnoreCase("cover_image_request")) {

                // openCamera(cover_camera_request);

                captureImage(StencilCamConstant.STENCIL_COVERIMAGE,StencilCamConstant.LBL_COVERIMAGE);

                resultCodeforImage = 2001;

                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("front_image_request")) {
                //   openCamera(front_camera_request);

                captureImage(StencilCamConstant.STENCIL_FRONT,StencilCamConstant.LBL_FRONT);

                resultCodeforImage = 2002;
                //CommonMethods.setvalueAgainstKey(this,"frontview","true");
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("rear_image_request")) {
                captureImage(StencilCamConstant.STENCIL_REARVIEW,StencilCamConstant.LBL_REARVIEW);
                resultCodeforImage = 2003;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("leftside_image_request")) {
                captureImage(StencilCamConstant.STENCIL_LEFTSIDEVIEW,StencilCamConstant.LBL_LEFTSIDEVIEW);
                resultCodeforImage = 2004;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("rightside_image_request")) {
                captureImage(StencilCamConstant.STENCIL_RIGHRSIDEVIEW,StencilCamConstant.LBL_RIGHRSIDEVIEW);
                resultCodeforImage = 2005;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("rearleft_image_request")) {
                captureImage(StencilCamConstant.STENCIL_REARLEFT,StencilCamConstant.LBL_REARLEFT);
                resultCodeforImage = 2006;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("frontright_image_request")) {
                captureImage(StencilCamConstant.STENCIL_FRONTRIGHT,StencilCamConstant.LBL_FRONTRIGHT);
                resultCodeforImage = 2007;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("rear_right_view")) {
                captureImage(StencilCamConstant.STENCIL_REARRIGHT,StencilCamConstant.LBL_REARRIGHT);
                resultCodeforImage = 2008;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("wheels_tyres")) {
                captureImage(StencilCamConstant.STENCIL_WHEEL,StencilCamConstant.LBL_WHEEL);
                resultCodeforImage = 2009;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("front_seats")) {
                captureImage(StencilCamConstant.STENCIL_BACKSEATS,StencilCamConstant.LBL_FRONTSEATS);
                resultCodeforImage = 2010;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("back_seats")) {
                captureImage(StencilCamConstant.STENCIL_FRONTSEATS,StencilCamConstant.LBL_BACKSEATS);
                resultCodeforImage = 2011;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("odometer")) {
                captureImage(StencilCamConstant.STENCIL_ODOMETER,StencilCamConstant.LBL_ODOMETER);
                resultCodeforImage = 2012;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("dashboard")) {
                captureImage(StencilCamConstant.STENCIL_DASHBOARD,StencilCamConstant.LBL_DASHBOARD);
                resultCodeforImage = 2013;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("steering")) {
                captureImage(StencilCamConstant.STENCIL_STEERING,StencilCamConstant.LBL_STEERING);
                resultCodeforImage = 2014;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("front_row_side")) {
                captureImage(StencilCamConstant.STENCIL_FRONTROW,StencilCamConstant.LBL_FRONTROW);
                resultCodeforImage = 2015;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("back_row_side")) {
                captureImage(StencilCamConstant.STENCIL_BACKROW,StencilCamConstant.LBL_BACKROW);
                resultCodeforImage = 2016;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("trunk_door_open_view")) {
                captureImage(StencilCamConstant.STENCIL_TRUNKDOOR,StencilCamConstant.LBL_TRUNKDOOR);
                resultCodeforImage = 2017;
                //startCameraActivity();
            }

            dialog.dismiss();

        });

        select_gallery.setOnClickListener(view2 -> {

            whichSource = false;
            if (imageStatus.equalsIgnoreCase("cover_image_request")) {

                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, RESULT_LOAD);

                resultCodeforImage = 2001;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("front_image_request")) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, RESULT_LOAD);

                resultCodeforImage = 2002;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("rear_image_request")) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, RESULT_LOAD);
                resultCodeforImage = 2003;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("leftside_image_request")) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, RESULT_LOAD);
                resultCodeforImage = 2004;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("rightside_image_request")) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, RESULT_LOAD);
                resultCodeforImage = 2005;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("rearleft_image_request")) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, RESULT_LOAD);
                resultCodeforImage = 2006;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("frontright_image_request")) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, RESULT_LOAD);
                resultCodeforImage = 2007;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("rear_right_view")) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, RESULT_LOAD);
                resultCodeforImage = 2008;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("wheels_tyres")) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, RESULT_LOAD);
                resultCodeforImage = 2009;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("front_seats")) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, RESULT_LOAD);
                resultCodeforImage = 2010;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("back_seats")) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, RESULT_LOAD);
                resultCodeforImage = 2011;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("odometer")) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, RESULT_LOAD);
                resultCodeforImage = 2012;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("dashboard")) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, RESULT_LOAD);
                resultCodeforImage = 2013;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("steering")) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, RESULT_LOAD);
                resultCodeforImage = 2014;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("front_row_side")) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, RESULT_LOAD);
                resultCodeforImage = 2015;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("back_row_side")) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, RESULT_LOAD);
                resultCodeforImage = 2016;
                //startCameraActivity();
            } else if (imageStatus.equalsIgnoreCase("trunk_door_open_view")) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery, RESULT_LOAD);
                resultCodeforImage = 2017;
                //startCameraActivity();
            }
            dialog.dismiss();

        });

        dialog.show();

    }

    private void openCamera(int requestType) {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (intent.resolveActivity(this.getPackageManager()) != null) {
            file = new File(getExternalCacheDir(),
                    String.valueOf(System.currentTimeMillis()) + ".jpg");
            fileUri = Uri.fromFile(file);
            fileUri = FileProvider.getUriForFile(this, this.getPackageName() + ".provider", file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            startActivityForResult(intent, requestType);
            overridePendingTransition(0, 0);

            cameraRequirement(intent, fileUri);
        }
    }

    private void startCameraActivity(int requestType) {

        whichSource = true;
        //  Log.e("requestType", "requestType=" + requestType);
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Choose File"), requestType);


    }

    public void CloseAert(String close) {


        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.common_cus_dialog);
        //dialog.setTitle("Custom Dialog");

        TextView Message, Message2;
        Button cancel, Confirm;
        Message2 = dialog.findViewById(R.id.Message2);
        Message = dialog.findViewById(R.id.Message);
        cancel = dialog.findViewById(R.id.cancel);
        Confirm = dialog.findViewById(R.id.Confirm);

        CommonMethods.MemoryClears();

            /* Window window = dialog.getWindow();
            window.setLayout(W, 1000);*/

        Message.setText("Are you sure you want to");
        Message2.setText("Delete this image?");

        Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  Log.e("leftsideview_close", close);
                if (close.equalsIgnoreCase("coverimgClose")) {
                    startSendingImageToServerCounter("", cover_image, "cover_image", "coverimgClose");
                } else if (close.equalsIgnoreCase("frontviewClose")) {
                    startSendingImageToServerCounter("", front_image, "front_view", "frontviewClose");
                } else if (close.equalsIgnoreCase("rearviewClose")) {
                    startSendingImageToServerCounter("", rear_image, "rear_view", "rearviewClose");
                } else if (close.equalsIgnoreCase("leftsideview_close")) {
                    startSendingImageToServerCounter("", leftside_image, "left_side_view", "leftsideview_close");
                } else if (close.equalsIgnoreCase("rightsideview_close")) {
                    startSendingImageToServerCounter("", rightside_image, "right_side_view", "rightsideview_close");
                } else if (close.equalsIgnoreCase("rearleftview_close")) {
                    startSendingImageToServerCounter("", rearleft_image, "rear_left_view", "rearleftview_close");
                } else if (close.equalsIgnoreCase("rearright_close")) {
                    startSendingImageToServerCounter("", rearright_image, "rear_right_view", "rearright_close");
                } else if (close.equalsIgnoreCase("wheelimg_close")) {
                    startSendingImageToServerCounter("", wheels_image, "wheels_tyres", "wheelimg_close");
                } else if (close.equalsIgnoreCase("frontright_close")) {
                    startSendingImageToServerCounter("", frontright_image, "front_right_view", "frontright_close");
                } else if (close.equalsIgnoreCase("frontseat_close")) {
                    startSendingImageToServerCounter("", front_seats_image, "front_seats", "frontseat_close");
                } else if (close.equalsIgnoreCase("backseat_close")) {
                    startSendingImageToServerCounter("", back_seats_image, "back_seats", "backseat_close");
                } else if (close.equalsIgnoreCase("odometer_close")) {
                    startSendingImageToServerCounter("", odometer_image, "odometer", "odometer_close");
                } else if (close.equalsIgnoreCase("dasborad_close")) {
                    startSendingImageToServerCounter("", dashboard_image, "dashboard", "dasborad_close");
                } else if (close.equalsIgnoreCase("steering_close")) {
                    startSendingImageToServerCounter("", steering_image, "steering", "steering_close");
                } else if (close.equalsIgnoreCase("frontrowseat_close")) {
                    startSendingImageToServerCounter("", front_row_side_image, "front_row_side", "frontrowseat_close");
                } else if (close.equalsIgnoreCase("backrowseat_close")) {
                    startSendingImageToServerCounter("", back_row_side_image, "back_row_side", "backrowseat_close");
                } else if (close.equalsIgnoreCase("doorimg_close")) {
                    startSendingImageToServerCounter("", trunk_door_open_view_image, "trunk_door_open_view", "doorimg_close");
                }


                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    void cameraRequirement(Intent intent, Uri fileUri) {
        List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo resolveInfo : resInfoList) {
            String packageName = resolveInfo.activityInfo.packageName;
            grantUriPermission(packageName, fileUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
    }

    private Bitmap loadImageFromStorage(File f) {
        Bitmap b = null;
        try {
            b = BitmapFactory.decodeStream(new FileInputStream(f));


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return b;
    }


    private void startSendingImageToServerCounter(String imageString, String imageType, String imageName, String deleteimg) {

        try {

            JSONObject jObj = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject1 = new JSONObject();

            jObj.put("stock_id", CommonMethods.getstringvaluefromkey(this, "stock_id"));
            jObj.put("dealer_code", CommonMethods.getstringvaluefromkey(this, "dealer_code"));
            jObj.put("IsNonMFC", "false");

            jObj.put("uploaded_from", "mobile-android");
            jObj.put("user_id", CommonMethods.getstringvaluefromkey(this, "user_id"));
            jObj.put("uploaded_loc", "unknow");

            jsonObject1.put("mdate", "2018-03-26 15:21:43");

            jsonObject1.put("type", "jpg");
            jsonObject1.put("category_identifier", imageName);
            if (deleteimg.equalsIgnoreCase("")) {
                jsonObject1.put("img", imageString);

            } else {
                jsonObject1.put("img", "");

            }

            imageUploadedProgressBar(imageType);

            jsonArray.put(jsonObject1);
            jObj.put("images", jsonArray);

            serverCallForImage(imageType, jObj, deleteimg);

            //  Log.e("serverCallForImage", "serverCallForImage" + imageType);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void serverCallForImage(String imageUpload, JSONObject jsonObject, String deleteimg) {

        //  Log.e("serverCallForImage", "serverCallForImage" + imageUpload);

        if (imageUpload.equals(cover_image)) {
            CoverImageWebServices parser = new CoverImageWebServices(activity, cover_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(front_image)) {
            FrontImageWebServices parser = new FrontImageWebServices(activity, front_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(rear_image)) {
            RearImageWebServices parser = new RearImageWebServices(activity, rear_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(leftside_image)) {
            LeftSideImageWebServices parser = new LeftSideImageWebServices(activity, leftside_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(rightside_image)) {
            RightSideImageWebServices parser = new RightSideImageWebServices(activity, rightside_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(rearleft_image)) {
            RearLeftImageWebServices parser = new RearLeftImageWebServices(activity, rearleft_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(frontright_image)) {
            FrontRightImageWebServices parser = new FrontRightImageWebServices(activity, frontright_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(rearright_image)) {
            RearRightImageWebServices parser = new RearRightImageWebServices(activity, rearright_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(wheels_image)) {
            WheelImageWebServices parser = new WheelImageWebServices(activity, wheels_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(front_seats_image)) {
            FrontSeatImageWebServices parser = new FrontSeatImageWebServices(activity, front_seats_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(back_seats_image)) {
            BackSeatImageWebServices parser = new BackSeatImageWebServices(activity, back_seats_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(odometer_image)) {
            OdometerImageWebServices parser = new OdometerImageWebServices(activity, odometer_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(dashboard_image)) {
            DasboardImageWebServices parser = new DasboardImageWebServices(activity, dashboard_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(steering_image)) {
            SteeringImageWebServices parser = new SteeringImageWebServices(activity, steering_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(front_row_side_image)) {
            FrontRowSideImageWebServices parser = new FrontRowSideImageWebServices(activity, front_row_side_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(back_row_side_image)) {
            BackRowSideImageWebServices parser = new BackRowSideImageWebServices(activity, back_row_side_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        } else if (imageUpload.equals(trunk_door_open_view_image)) {
            DoorImageWebServices parser = new DoorImageWebServices(activity, trunk_door_open_view_image, deleteimg);
            parser.execute(Global.stock_dealerURL + "dealer/stocks/image", jsonObject.toString());
        }

    }

    private void imageUploadedProgressBar(String imageName) {

        if (imageName.equals(cover_image)) {
            mCircleView_cover.setVisibility(View.VISIBLE);
            mCircleView_cover.spin();
        } else if (imageName.equals(front_image)) {
            mCircleView_front.setVisibility(View.VISIBLE);
            mCircleView_front.spin();
        } else if (imageName.equals(rear_image)) {
            mCircleView_rear.setVisibility(View.VISIBLE);
            mCircleView_rear.spin();
        } else if (imageName.equals(leftside_image)) {
            mCircleView_leftside.setVisibility(View.VISIBLE);
            mCircleView_leftside.spin();
        } else if (imageName.equals(rightside_image)) {
            mCircleView_rightside.setVisibility(View.VISIBLE);
            mCircleView_rightside.spin();
        } else if (imageName.equals(rearleft_image)) {
            mCircleView_rearleft.setVisibility(View.VISIBLE);
            mCircleView_rearleft.spin();
        } else if (imageName.equals(frontright_image)) {
            mCircleView_frontright.setVisibility(View.VISIBLE);
            mCircleView_frontright.spin();
        } else if (imageName.equals(rearright_image)) {
            mCircleView_rearright.setVisibility(View.VISIBLE);
            mCircleView_rearright.spin();
        } else if (imageName.equals(wheels_image)) {
            mCircleView_wheels.setVisibility(View.VISIBLE);
            mCircleView_wheels.spin();
        } else if (imageName.equals(front_seats_image)) {
            mCircleView_frontseat.setVisibility(View.VISIBLE);
            mCircleView_frontseat.spin();
        } else if (imageName.equals(back_seats_image)) {
            mCircleView_backseat.setVisibility(View.VISIBLE);
            mCircleView_backseat.spin();
        } else if (imageName.equals(odometer_image)) {
            mCircleView_odameter.setVisibility(View.VISIBLE);
            mCircleView_odameter.spin();
        } else if (imageName.equals(dashboard_image)) {
            mCircleView_dasboard.setVisibility(View.VISIBLE);
            mCircleView_dasboard.spin();
        } else if (imageName.equals(steering_image)) {
            mCircleView_steering.setVisibility(View.VISIBLE);
            mCircleView_steering.spin();
        } else if (imageName.equals(front_row_side_image)) {
            mCircleView_frontseatside.setVisibility(View.VISIBLE);
            mCircleView_frontseatside.spin();
        } else if (imageName.equals(back_row_side_image)) {
            mCircleView_backseatside.setVisibility(View.VISIBLE);
            mCircleView_backseatside.spin();
        } else if (imageName.equals(trunk_door_open_view_image)) {
            mCircleView_door.setVisibility(View.VISIBLE);
            mCircleView_door.spin();
        }
    }

    private static void imageUploadedProgressBarClosed(String imageName) {
        //  Log.e("mCircleView_cover", "mCircleView_cover-1");
        if (imageName.equals(cover_image)) {
            //     Log.e("mCircleView_cover", "mCircleView_cover-2");

            mCircleView_cover.stopSpinning();
            mCircleView_cover.setVisibility(View.GONE);
        } else if (imageName.equals(front_image)) {
            mCircleView_front.stopSpinning();
            mCircleView_front.setVisibility(View.GONE);
        } else if (imageName.equals(rear_image)) {
            mCircleView_rear.stopSpinning();
            mCircleView_rear.setVisibility(View.GONE);
        } else if (imageName.equals(leftside_image)) {
            mCircleView_leftside.stopSpinning();
            mCircleView_leftside.setVisibility(View.GONE);
        } else if (imageName.equals(rightside_image)) {
            mCircleView_rightside.stopSpinning();
            mCircleView_rightside.setVisibility(View.GONE);
        } else if (imageName.equals(rearleft_image)) {
            mCircleView_rearleft.stopSpinning();
            mCircleView_rearleft.setVisibility(View.GONE);
        } else if (imageName.equals(frontright_image)) {
            mCircleView_frontright.stopSpinning();
            mCircleView_frontright.setVisibility(View.GONE);
        } else if (imageName.equals(rearright_image)) {
            mCircleView_rearright.stopSpinning();
            mCircleView_rearright.setVisibility(View.GONE);
        } else if (imageName.equals(wheels_image)) {
            mCircleView_wheels.stopSpinning();
            mCircleView_wheels.setVisibility(View.GONE);
        } else if (imageName.equals(front_seats_image)) {
            mCircleView_frontseat.setVisibility(View.GONE);
            mCircleView_frontseat.spin();
        } else if (imageName.equals(back_seats_image)) {
            mCircleView_backseat.setVisibility(View.GONE);
            mCircleView_backseat.stopSpinning();
        } else if (imageName.equals(odometer_image)) {
            mCircleView_odameter.setVisibility(View.GONE);
            mCircleView_odameter.stopSpinning();
        } else if (imageName.equals(dashboard_image)) {
            mCircleView_dasboard.setVisibility(View.GONE);
            mCircleView_dasboard.stopSpinning();
        } else if (imageName.equals(steering_image)) {
            mCircleView_steering.setVisibility(View.GONE);
            mCircleView_steering.stopSpinning();
        } else if (imageName.equals(front_row_side_image)) {
            mCircleView_frontseatside.setVisibility(View.GONE);
            mCircleView_frontseatside.stopSpinning();
        } else if (imageName.equals(back_row_side_image)) {
            mCircleView_backseatside.setVisibility(View.GONE);
            mCircleView_backseatside.stopSpinning();
        } else if (imageName.equals(trunk_door_open_view_image)) {
            mCircleView_door.setVisibility(View.GONE);
            mCircleView_door.stopSpinning();
        }


    }

    private void imageUploadProgressBarClosedOnly(String imageName) {

        if (imageName.equals(front_image)) {
            mCircleView_front.stopSpinning();
            mCircleView_front.setVisibility(View.GONE);
            /*front_sent_indicator.setVisibility(View.INVISIBLE);
            Resend_Front.setVisibility(View.INVISIBLE);
            front_imageview.setImageDrawable(ContextCompat.getDrawable(activity, R.mipmap.failed_image));*/
        }
    }

    public String imageToString(String pictureOf) {

        Bitmap bm = null;

        //   Log.e("front_image", "front_image=3.0");

        if (pictureOf.equals(cover_image))
            bm = ((BitmapDrawable) coverimg.getDrawable()).getBitmap();
        else if (pictureOf.equals(front_image))
            bm = ((BitmapDrawable) frontview.getDrawable()).getBitmap();
        else if (pictureOf.equals(rear_image))
            bm = ((BitmapDrawable) rearview.getDrawable()).getBitmap();
        else if (pictureOf.equals(leftside_image))
            bm = ((BitmapDrawable) leftsideview.getDrawable()).getBitmap();
        else if (pictureOf.equals(rightside_image))
            bm = ((BitmapDrawable) rightsideview.getDrawable()).getBitmap();
        else if (pictureOf.equals(rearleft_image))
            bm = ((BitmapDrawable) rearleftview.getDrawable()).getBitmap();
        else if (pictureOf.equals(frontright_image))
            bm = ((BitmapDrawable) frontright.getDrawable()).getBitmap();
        else if (pictureOf.equals(rearright_image))
            bm = ((BitmapDrawable) rearright.getDrawable()).getBitmap();
        else if (pictureOf.equals(wheels_image))
            bm = ((BitmapDrawable) wheelimg.getDrawable()).getBitmap();
        else if (pictureOf.equals(front_seats_image))
            bm = ((BitmapDrawable) frontseat.getDrawable()).getBitmap();
        else if (pictureOf.equals(back_seats_image))
            bm = ((BitmapDrawable) backseat.getDrawable()).getBitmap();
        else if (pictureOf.equals(odometer_image))
            bm = ((BitmapDrawable) odometer.getDrawable()).getBitmap();
        else if (pictureOf.equals(dashboard_image))
            bm = ((BitmapDrawable) dasborad.getDrawable()).getBitmap();
        else if (pictureOf.equals(steering_image))
            bm = ((BitmapDrawable) steering.getDrawable()).getBitmap();
        else if (pictureOf.equals(front_row_side_image))
            bm = ((BitmapDrawable) frontrowseat.getDrawable()).getBitmap();
        else if (pictureOf.equals(back_row_side_image))
            bm = ((BitmapDrawable) backrowseat.getDrawable()).getBitmap();
        else if (pictureOf.equals(trunk_door_open_view_image))
            bm = ((BitmapDrawable) doorimg.getDrawable()).getBitmap();


        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 80, baos);
        byte[] b = null;
        b = baos.toByteArray();

        return globalImageCompression(b.length, bm);
    }

    private String globalImageCompression(int imageSize, Bitmap bm) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        if (imageSize <= (200 * 1024)) {                            // <= 200 KB
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] b = null;
            b = baos.toByteArray();
            String encodedImage = "";
            encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            return encodedImage;
        } else if (imageSize <= (500 * 1024)) {                      // <= 500 KB
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] b = null;
            b = baos.toByteArray();
            String encodedImage = "";
            encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            return encodedImage;
        } else if (imageSize <= (1 * 1024 * 1024)) {                 // <= 1 MB

            bm.compress(Bitmap.CompressFormat.JPEG, 90, baos);
            byte[] b = null;
            b = baos.toByteArray();
            String encodedImage = "";
            encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            return encodedImage;
        } else if (imageSize <= (2 * 1024 * 1024)) {                 // <= 2 MB

            bm.compress(Bitmap.CompressFormat.JPEG, 80, baos);
            byte[] b = null;
            b = baos.toByteArray();
            String encodedImage = "";
            encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            return encodedImage;
        } else if (imageSize > (2 * 1024 * 1024)) {                  //  >  2 MB

            Double w = bm.getWidth() * (GALLERY_RESCALE_VALUE);
            width_x = w.intValue();
            Double h = bm.getHeight() * (GALLERY_RESCALE_VALUE);
            height_y = h.intValue();

            Bitmap bitmapAfterRescaleImage = null;
            bitmapAfterRescaleImage = Bitmap.createScaledBitmap(bm, width_x, height_y, false);
            bitmapAfterRescaleImage.compress(Bitmap.CompressFormat.JPEG, 80, baos);

            /*try {
                if (bm != null) {
                    bm.recycle();
                    bm = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }*/
            byte[] b = null;
            b = baos.toByteArray();
            String encodedImage = "";
            encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            return encodedImage;
        }
        return "";
    }



    private class CoverImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String delete;

        public CoverImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            delete = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS-1");


                    if (delete.equalsIgnoreCase("coverimgClose")) {
                        coverimg.setImageResource(R.drawable.front_left_view);
                        coverimglbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        coverimgClose.setVisibility(View.INVISIBLE);
                    } else {
                        coverimglbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        coverimgClose.setVisibility(View.VISIBLE);
                    }


                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    coverimglbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);
                    coverimgClose.setVisibility(View.INVISIBLE);

                }
                //    Log.e("mCircleView_cover", "mCircleView_cover-0");

                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
                //Log.e("mCircleView_cover","mCircleView_cover-0");
                Log.e("mCircleView_cover", "mCircleView_cover-0" + e);


            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e("rse -> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // jsonObject = null;
                        try {
                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {
                                try {
                                    String res = new String(response.data,
                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                    // Now you can use any deserializer to make sense of data
                                    JSONObject obj = new JSONObject(res);
                                    jsonObject = obj;

                                } catch (UnsupportedEncodingException e1) {
                                    // Couldn't properly decode data to string
                                    e1.printStackTrace();
                                } catch (JSONException e2) {
                                    // returned data is not JSONObject?
                                    e2.printStackTrace();
                                }
                            }

                            try {
                                error_popup2(error.networkResponse.statusCode);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }


    private class FrontImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public FrontImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");
                    if (deleteimg.equalsIgnoreCase("frontviewClose")) {

                        frontview.setImageResource(R.drawable.front_view);
                        frontviewlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        frontviewClose.setVisibility(View.INVISIBLE);

                    } else {
                        frontviewlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        frontviewClose.setVisibility(View.VISIBLE);

                    }

                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    frontviewlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);
                    frontviewClose.setVisibility(View.INVISIBLE);

                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check ---> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // jsonObject = null;
                        try {
                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {
                                try {
                                    String res = new String(response.data,
                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                    // Now you can use any deserializer to make sense of data
                                    JSONObject obj = new JSONObject(res);
                                    jsonObject = obj;

                                } catch (UnsupportedEncodingException e1) {
                                    // Couldn't properly decode data to string
                                    e1.printStackTrace();
                                } catch (JSONException e2) {
                                    // returned data is not JSONObject?
                                    e2.printStackTrace();
                                }
                            }
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class RearImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public RearImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");

                    if (deleteimg.equalsIgnoreCase("rearviewClose")) {

                        rearview.setImageResource(R.drawable.rear_view);
                        rearviewlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        rearviewClose.setVisibility(View.INVISIBLE);

                    } else {
                        rearviewlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        rearviewClose.setVisibility(View.VISIBLE);
                    }


                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    rearviewlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);
                    rearviewClose.setVisibility(View.INVISIBLE);

                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here ---------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // jsonObject = null;
                        try {
                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {
                                try {
                                    String res = new String(response.data,
                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                    // Now you can use any deserializer to make sense of data
                                    JSONObject obj = new JSONObject(res);
                                    jsonObject = obj;

                                } catch (UnsupportedEncodingException e1) {
                                    // Couldn't properly decode data to string
                                    e1.printStackTrace();
                                } catch (JSONException e2) {
                                    // returned data is not JSONObject?
                                    e2.printStackTrace();
                                }
                            }
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class LeftSideImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public LeftSideImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            //   Log.e("delete leftsideview_close", result.toString());

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");
                    if (deleteimg.equalsIgnoreCase("leftsideview_close")) {
                        leftsideview.setImageResource(R.drawable.left_side_view);
                        leftsidelbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        leftsideview_close.setVisibility(View.INVISIBLE);
                    } else {
                        leftsidelbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        leftsideview_close.setVisibility(View.VISIBLE);
                    }

                } else {
                    Log.e("SUCCESS false -->> ", "SUCCESS-2");
                    leftsidelbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);
                    leftsideview_close.setVisibility(View.INVISIBLE);

                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here ---------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //jsonObject = null;
                        try {
                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {
                                try {
                                    String res = new String(response.data,
                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                    // Now you can use any deserializer to make sense of data
                                    JSONObject obj = new JSONObject(res);
                                    jsonObject = obj;

                                } catch (UnsupportedEncodingException e1) {
                                    // Couldn't properly decode data to string
                                    e1.printStackTrace();
                                } catch (JSONException e2) {
                                    // returned data is not JSONObject?
                                    e2.printStackTrace();
                                }
                            }
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class RightSideImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public RightSideImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    if (deleteimg.equalsIgnoreCase("rightsideview_close")) {
                        rightsideview.setImageResource(R.drawable.right_side_view);
                        rightsidelbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        rightsideview_close.setVisibility(View.INVISIBLE);

                    } else {
                        rightsidelbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        rightsideview_close.setVisibility(View.VISIBLE);

                    }


                } else {
                    // Log.e("SUCCESS -->> ", "SUCCESS-2");
                    rightsideview_close.setVisibility(View.INVISIBLE);
                    rightsidelbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);


                }

                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here ------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // jsonObject = null;
                        try {
                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {
                                try {
                                    String res = new String(response.data,
                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                    // Now you can use any deserializer to make sense of data
                                    JSONObject obj = new JSONObject(res);
                                    jsonObject = obj;

                                } catch (UnsupportedEncodingException e1) {
                                    // Couldn't properly decode data to string
                                    e1.printStackTrace();
                                } catch (JSONException e2) {
                                    // returned data is not JSONObject?
                                    e2.printStackTrace();
                                }
                            }
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class RearLeftImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public RearLeftImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {


                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");
                    if (deleteimg.equalsIgnoreCase("rearleftview_close")) {
                        rearleftview.setImageResource(R.drawable.rear_left_view);
                        rearleftlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        rearleftview_close.setVisibility(View.INVISIBLE);

                    } else {
                        rearleftlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        rearleftview_close.setVisibility(View.VISIBLE);

                    }


                } else {
                    rearleftview_close.setVisibility(View.VISIBLE);
                    rearleftlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);

                    Log.e("SUCCESS -->> ", "SUCCESS-2");

                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here ---------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  jsonObject = null;
                        try {
                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {
                                try {
                                    String res = new String(response.data,
                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                    // Now you can use any deserializer to make sense of data
                                    JSONObject obj = new JSONObject(res);
                                    jsonObject = obj;

                                } catch (UnsupportedEncodingException e1) {
                                    // Couldn't properly decode data to string
                                    e1.printStackTrace();
                                } catch (JSONException e2) {
                                    // returned data is not JSONObject?
                                    e2.printStackTrace();
                                }
                            }
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }


    private class FrontRightImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public FrontRightImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");

                    if (deleteimg.equalsIgnoreCase("frontright_close")) {
                        frontright.setImageResource(R.drawable.front_right_view);
                        frontrightlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        frontright_close.setVisibility(View.INVISIBLE);
                    } else {
                        frontrightlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        frontright_close.setVisibility(View.VISIBLE);
                    }

                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    frontright_close.setVisibility(View.INVISIBLE);
                    frontrightlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);
                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here ---------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // jsonObject = null;
                        try {
                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {
                                try {
                                    String res = new String(response.data,
                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                    // Now you can use any deserializer to make sense of data
                                    JSONObject obj = new JSONObject(res);
                                    jsonObject = obj;

                                } catch (UnsupportedEncodingException e1) {
                                    // Couldn't properly decode data to string
                                    e1.printStackTrace();
                                } catch (JSONException e2) {
                                    // returned data is not JSONObject?
                                    e2.printStackTrace();
                                }
                            }
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }
        }
    }


    private class RearRightImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public RearRightImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;
            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");

                    if (deleteimg.equalsIgnoreCase("rearright_close")) {
                        rearright.setImageResource(R.drawable.rear_right_view);
                        rearrightlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        rearright_close.setVisibility(View.INVISIBLE);
                    } else {
                        rearrightlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        rearright_close.setVisibility(View.VISIBLE);
                    }

                } else {
                    rearrightlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);
                    rearright_close.setVisibility(View.INVISIBLE);
                    Log.e("SUCCESS -->> ", "SUCCESS-2");

                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e("check the response here ---------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //jsonObject = null;
                        try {
                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {
                                try {
                                    String res = new String(response.data,
                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                    // Now you can use any deserializer to make sense of data
                                    JSONObject obj = new JSONObject(res);
                                    jsonObject = obj;

                                } catch (UnsupportedEncodingException e1) {
                                    // Couldn't properly decode data to string
                                    e1.printStackTrace();
                                } catch (JSONException e2) {
                                    // returned data is not JSONObject?
                                    e2.printStackTrace();
                                }
                            }
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class WheelImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public WheelImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");

                    if (deleteimg.equalsIgnoreCase("wheelimg_close")) {
                        wheelimg.setImageResource(R.drawable.wheel);
                        wheelslbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        wheelimg_close.setVisibility(View.INVISIBLE);
                    } else {
                        wheelslbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        wheelimg_close.setVisibility(View.VISIBLE);
                    }

                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    wheelslbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);

                    wheelimg_close.setVisibility(View.INVISIBLE);

                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here ---------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //jsonObject = null;
                        try {
                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {
                                try {
                                    String res = new String(response.data,
                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                    // Now you can use any deserializer to make sense of data
                                    JSONObject obj = new JSONObject(res);
                                    jsonObject = obj;

                                } catch (UnsupportedEncodingException e1) {
                                    // Couldn't properly decode data to string
                                    e1.printStackTrace();
                                } catch (JSONException e2) {
                                    // returned data is not JSONObject?
                                    e2.printStackTrace();
                                }
                            }
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }


    private class FrontSeatImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public FrontSeatImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");
                    if (deleteimg.equalsIgnoreCase("frontseat_close")) {

                        frontseat.setImageResource(R.drawable.front_seat);
                        frontseatlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        frontseat_close.setVisibility(View.INVISIBLE);
                    } else {
                        frontseatlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        frontseat_close.setVisibility(View.VISIBLE);
                    }

                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    frontseat_close.setVisibility(View.VISIBLE);
                    frontseatlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);


                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here ---------------->>>> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //jsonObject = null;
                        try {
                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {
                                try {
                                    String res = new String(response.data,
                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                    // Now you can use any deserializer to make sense of data
                                    JSONObject obj = new JSONObject(res);
                                    jsonObject = obj;

                                } catch (UnsupportedEncodingException e1) {
                                    // Couldn't properly decode data to string
                                    e1.printStackTrace();
                                } catch (JSONException e2) {
                                    // returned data is not JSONObject?
                                    e2.printStackTrace();
                                }
                            }
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class BackSeatImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public BackSeatImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");

                    if (deleteimg.equalsIgnoreCase("backseat_close")) {

                        backseat.setImageResource(R.drawable.back_seat);
                        backseatlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        backseat_close.setVisibility(View.INVISIBLE);
                    } else {
                        backseatlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        backseat_close.setVisibility(View.VISIBLE);
                    }

                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    backseat_close.setVisibility(View.INVISIBLE);
                    backseatlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);


                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" here --> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // jsonObject = null;
                        try {
                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {
                                try {
                                    String res = new String(response.data,
                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                    // Now you can use any deserializer to make sense of data
                                    JSONObject obj = new JSONObject(res);
                                    jsonObject = obj;

                                } catch (UnsupportedEncodingException e1) {
                                    // Couldn't properly decode data to string
                                    e1.printStackTrace();
                                } catch (JSONException e2) {
                                    // returned data is not JSONObject?
                                    e2.printStackTrace();
                                }
                            }
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class OdometerImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public OdometerImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");

                    if (deleteimg.equalsIgnoreCase("odometer_close")) {

                        odometer.setImageResource(R.drawable.odometer);
                        odameterlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        odometer_close.setVisibility(View.INVISIBLE);
                    } else {
                        odameterlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        odometer_close.setVisibility(View.VISIBLE);
                    }

                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    odometer_close.setVisibility(View.INVISIBLE);
                    odameterlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);


                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" here --> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // jsonObject = null;
                        try {
                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {
                                try {
                                    String res = new String(response.data,
                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                    // Now you can use any deserializer to make sense of data
                                    JSONObject obj = new JSONObject(res);
                                    jsonObject = obj;

                                } catch (UnsupportedEncodingException e1) {
                                    // Couldn't properly decode data to string
                                    e1.printStackTrace();
                                } catch (JSONException e2) {
                                    // returned data is not JSONObject?
                                    e2.printStackTrace();
                                }
                            }
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class DasboardImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public DasboardImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");
                    if (deleteimg.equalsIgnoreCase("dasborad_close")) {

                        dasborad.setImageResource(R.drawable.dashboard);
                        dasboardlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        dasborad_close.setVisibility(View.INVISIBLE);
                    } else {
                        dasboardlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        dasborad_close.setVisibility(View.VISIBLE);
                    }
                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    dasborad_close.setVisibility(View.INVISIBLE);
                    dasboardlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);


                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" here --> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // jsonObject = null;
                        try {
                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {
                                try {
                                    String res = new String(response.data,
                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                    // Now you can use any deserializer to make sense of data
                                    JSONObject obj = new JSONObject(res);
                                    jsonObject = obj;

                                } catch (UnsupportedEncodingException e1) {
                                    // Couldn't properly decode data to string
                                    e1.printStackTrace();
                                } catch (JSONException e2) {
                                    // returned data is not JSONObject?
                                    e2.printStackTrace();
                                }
                            }
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class SteeringImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public SteeringImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");

                    if (deleteimg.equalsIgnoreCase("steering_close")) {

                        steering.setImageResource(R.drawable.steering);
                        steeringlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        steering_close.setVisibility(View.INVISIBLE);
                    } else {
                        steeringlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        steering_close.setVisibility(View.VISIBLE);
                    }

                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    steering_close.setVisibility(View.INVISIBLE);
                    steeringlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);


                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" check the response here -->> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // jsonObject = null;
                        try {
                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {
                                try {
                                    String res = new String(response.data,
                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                    // Now you can use any deserializer to make sense of data
                                    JSONObject obj = new JSONObject(res);
                                    jsonObject = obj;

                                } catch (UnsupportedEncodingException e1) {
                                    // Couldn't properly decode data to string
                                    e1.printStackTrace();
                                } catch (JSONException e2) {
                                    // returned data is not JSONObject?
                                    e2.printStackTrace();
                                }
                            }
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class FrontRowSideImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public FrontRowSideImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");

                    if (deleteimg.equalsIgnoreCase("frontrowseat_close")) {

                        frontrowseat.setImageResource(R.drawable.front_row_from_side);
                        frontrowlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        frontrowseat_close.setVisibility(View.INVISIBLE);
                    } else {
                        frontrowlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        frontrowseat_close.setVisibility(View.VISIBLE);
                    }

                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    frontrowseat_close.setVisibility(View.INVISIBLE);
                    frontrowlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);


                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" here --> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //jsonObject = null;
                        try {
                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {
                                try {
                                    String res = new String(response.data,
                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                    // Now you can use any deserializer to make sense of data
                                    JSONObject obj = new JSONObject(res);
                                    jsonObject = obj;

                                } catch (UnsupportedEncodingException e1) {
                                    // Couldn't properly decode data to string
                                    e1.printStackTrace();
                                } catch (JSONException e2) {
                                    // returned data is not JSONObject?
                                    e2.printStackTrace();
                                }
                            }
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class BackRowSideImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public BackRowSideImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");

                    if (deleteimg.equalsIgnoreCase("backrowseat_close")) {

                        backrowseat.setImageResource(R.drawable.back_row_from_side);
                        backrowlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        backrowseat_close.setVisibility(View.INVISIBLE);
                    } else {
                        backrowlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        backrowseat_close.setVisibility(View.VISIBLE);
                    }

                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    backrowseat_close.setVisibility(View.INVISIBLE);
                    backrowlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);


                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e(" here --> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //jsonObject = null;
                        try {
                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {
                                try {
                                    String res = new String(response.data,
                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                    // Now you can use any deserializer to make sense of data
                                    JSONObject obj = new JSONObject(res);
                                    jsonObject = obj;

                                } catch (UnsupportedEncodingException e1) {
                                    // Couldn't properly decode data to string
                                    e1.printStackTrace();
                                } catch (JSONException e2) {
                                    // returned data is not JSONObject?
                                    e2.printStackTrace();
                                }
                            }
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }

    private class DoorImageWebServices extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject = null;
        Context mContext;
        String imageTitle;
        String deleteimg;

        public DoorImageWebServices(Context context, String imageType, String deletes) {

            mContext = context;
            imageTitle = imageType;
            deleteimg = deletes;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        protected JSONObject doInBackground(String... params) {

            front_image_flag = false;

            String strURL = params[0];
            String strParams = params[1];
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(strParams);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return getJSONFromURL(strURL, jObject);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            try {

                if (result.getString("status").equals("SUCCESS")) {

                    Log.e("SUCCESS -->> ", "SUCCESS");

                    if (deleteimg.equalsIgnoreCase("doorimg_close")) {

                        doorimg.setImageResource(R.drawable.trunk_door_open_view);
                        doorlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        doorimg_close.setVisibility(View.INVISIBLE);
                    } else {
                        doorlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick_svg, 0);
                        doorimg_close.setVisibility(View.VISIBLE);
                    }

                } else {
                    Log.e("SUCCESS -->> ", "SUCCESS-2");
                    doorlbl.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_outline_black_24dp, 0);

                    doorimg_close.setVisibility(View.INVISIBLE);

                }
                imageUploadedProgressBarClosed(imageTitle);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        private JSONObject getJSONFromURL(String strURL, final JSONObject jObj) {
            try {

                Log.e("URL is -->> ", strURL);
                Log.e("data sending is -->> ", jObj.toString());

                RequestQueue queue = Volley.newRequestQueue(mContext);

                final JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                        Request.Method.PUT, strURL, jObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                jsonObject = response;
                                Log.e("here --> ", jsonObject.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  jsonObject = null;
                        try {
                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {
                                try {
                                    String res = new String(response.data,
                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                    // Now you can use any deserializer to make sense of data
                                    JSONObject obj = new JSONObject(res);
                                    jsonObject = obj;

                                } catch (UnsupportedEncodingException e1) {
                                    // Couldn't properly decode data to string
                                    e1.printStackTrace();
                                } catch (JSONException e2) {
                                    // returned data is not JSONObject?
                                    e2.printStackTrace();
                                }
                            }
                            error_popup2(error.networkResponse.statusCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Accept", "application/json; charset=utf-8");
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("token", CommonMethods.getstringvaluefromkey(activity, "token"));

                        return headers;
                    }
                };


                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        20000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsObjRequest);
                int i = 0, n = 1;
                do {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        Log.d(" OS", "OS");
                    }
                    if (jsonObject != null) {
                        n = 0;
                    }
                } while (i != n);
                return jsonObject;

            } catch (Exception ex) {
                // Log.e(" EXCEPTION here -->> ", ex.toString());
                return jsonObject;
                //return getJSONFromURL(strURL, jObj.toString());
            }

        }

    }


    public static void error_popup2(int str) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        dialog.setContentView(R.layout.alert_error);
        TextView Message, Confirm;
        Message = (TextView) dialog.findViewById(R.id.Message);
        Confirm = (TextView) dialog.findViewById(R.id.ok_error);
        dialog.setCancelable(false);
        //String strI = Integer.toString(str);
        Message.setText("Error code :"+str +"\n"+"Please try again.");

        Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MainActivity.logOut();
                dialog.dismiss();

            }
        });

        dialog.show();
    }

    private void MethodOfGettingCarImage() {

        Log.e("resultCode ", "resultCode" + resultCodeforImage);
        /*Bundle b = null;
        b = getIntent().getExtras();

        if (SaveImageInstance.getInstance().getCoverimage() != null) {
            coverimg.setImageBitmap(SaveImageInstance.getInstance().getCoverimage());
            coverimgFlag = true;
            coverimgClose.setVisibility(View.VISIBLE);
        }


        if (SaveImageInstance.getInstance().getFrontview() != null) {
            frontview.setImageBitmap(SaveImageInstance.getInstance().getFrontview());
            frontviewFlag = true;
            frontviewClose.setVisibility(View.VISIBLE);
        }

        if (SaveImageInstance.getInstance().getRearview() != null) {
            rearview.setImageBitmap(SaveImageInstance.getInstance().getRearview());
            rearviewFlag = true;
            rearviewClose.setVisibility(View.VISIBLE);
        }

        if (SaveImageInstance.getInstance().getLeftsideview() != null) {
            leftsideview.setImageBitmap(SaveImageInstance.getInstance().getLeftsideview());
            leftsideviewFlag = true;
            leftsideview_close.setVisibility(View.VISIBLE);
        }


        if (SaveImageInstance.getInstance().getRightsideview() != null) {
            rightsideview.setImageBitmap(SaveImageInstance.getInstance().getRightsideview());
            rightsideviewFlag = true;

            rightsideview_close.setVisibility(View.VISIBLE);
        }

        //New Adding Code

        if (SaveImageInstance.getInstance().getRearleft() != null) {
            rearleftview.setImageBitmap(SaveImageInstance.getInstance().getRearleft());
            rearleftviewFlag = true;
            rearleftview_close.setVisibility(View.VISIBLE);
        }


        if (SaveImageInstance.getInstance().getFrontright() != null) {
            frontright.setImageBitmap(SaveImageInstance.getInstance().getFrontright());
            frontrightviewFlag = true;
            frontright_close.setVisibility(View.VISIBLE);
        }

        if (SaveImageInstance.getInstance().getRearright() != null) {
            rearright.setImageBitmap(SaveImageInstance.getInstance().getRearright());
            rearrightviewFlag = true;
            rearright_close.setVisibility(View.VISIBLE);
        }

        if (SaveImageInstance.getInstance().getWheel() != null) {
            wheelimg.setImageBitmap(SaveImageInstance.getInstance().getWheel());
            wheelstviewFlag = true;
            wheelimg_close.setVisibility(View.VISIBLE);
        }

        //Interior

        if (SaveImageInstance.getInstance().getFrontseats() != null) {
            frontseat.setImageBitmap(SaveImageInstance.getInstance().getFrontseats());
            front_seatsFlag = true;
            frontseat_close.setVisibility(View.VISIBLE);
        }


        if (SaveImageInstance.getInstance().getBackseats() != null) {
            backseat.setImageBitmap(SaveImageInstance.getInstance().getBackseats());
            back_seatsFlag = true;
            backseat_close.setVisibility(View.VISIBLE);
        }

        if (SaveImageInstance.getInstance().getOdometer() != null) {
            odometer.setImageBitmap(SaveImageInstance.getInstance().getOdometer());
            odometerFlag = true;
            odometer_close.setVisibility(View.VISIBLE);
        }


        if (SaveImageInstance.getInstance().getDashboard() != null) {
            dasborad.setImageBitmap(SaveImageInstance.getInstance().getDashboard());
            dashboardFlag = true;
            dasborad_close.setVisibility(View.VISIBLE);
        }


        if (SaveImageInstance.getInstance().getSteering() != null) {
            steering.setImageBitmap(SaveImageInstance.getInstance().getSteering());
            steeringFlag = true;
            steering_close.setVisibility(View.VISIBLE);
        }

        if (SaveImageInstance.getInstance().getFrontrowfromside() != null) {
            frontrowseat.setImageBitmap(SaveImageInstance.getInstance().getFrontrowfromside());
            front_row_sideFlag = true;
            frontrowseat_close.setVisibility(View.VISIBLE);
        }

        if (SaveImageInstance.getInstance().getBackrowfromside() != null) {
            backrowseat.setImageBitmap(SaveImageInstance.getInstance().getBackrowfromside());
            back_row_sideFlag = true;
            backrowseat_close.setVisibility(View.VISIBLE);
        }

        if (SaveImageInstance.getInstance().getTrunkdooropenview() != null) {
            doorimg.setImageBitmap(SaveImageInstance.getInstance().getTrunkdooropenview());
            trunk_door_open_viewFlag = true;
            doorimg_close.setVisibility(View.VISIBLE);
        }*/

        try {

            File imgFile = new File(path);
            if (imgFile.exists()) {
                    /*BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    options.inSampleSize = 8;*/
                Bitmap myBitmap = null;

                try {
                    //  Log.e("Read ", "Read-1 ");

                    myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    //   Log.e("Read ", "Read-2 ");

                } catch (OutOfMemoryError error) {
                    //  Log.e("OutOfMemoryError-1 ", "rotate ");

                    try {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 8;
                        myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
                    } catch (OutOfMemoryError errors) {
                        //   Log.e("OutOfMemoryError-2 ", "rotate ");
                        try {
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inJustDecodeBounds = false;
                            options.inPreferredConfig = Bitmap.Config.RGB_565;
                            options.inDither = false;                     //Disable Dithering mode
                            options.inPurgeable = true;                   //Tell to gc that whether it needs free memory, the Bitmap can be cleared
                            options.inInputShareable = true;
                            options.inSampleSize = 15;
                            myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);

                        } catch (OutOfMemoryError errorss) {
                            myBitmap = decodeFile(imgFile, 512, 512);
                        }
                    }
                }

                if (resultCodeforImage == 2001) {
                    coverimg.setImageBitmap(myBitmap);
                    // if (myBitmap != null) myBitmap = null;
                    startSendingImageToServerCounter(imageToString(cover_image), cover_image, "cover_image", "");
                    //  SaveImageString.getInstance().setCoverimage(path);

                }
                if (resultCodeforImage == 2002) {
                    frontview.setImageBitmap(myBitmap);
                    // if (myBitmap != null) myBitmap = null;
                    startSendingImageToServerCounter(imageToString(front_image), front_image, "front_view", "");
                    // SaveImageString.getInstance().setFrontview(path);

                }
                if (resultCodeforImage == 2003) {
                    rearview.setImageBitmap(myBitmap);
                    //  if (myBitmap != null) myBitmap = null;
                    startSendingImageToServerCounter(imageToString(rear_image), rear_image, "rear_view", "");
                    // SaveImageString.getInstance().setRearview(path);

                }
                if (resultCodeforImage == 2004) {
                    leftsideview.setImageBitmap(myBitmap);
                    //  if (myBitmap != null) myBitmap = null;
                    startSendingImageToServerCounter(imageToString(leftside_image), leftside_image, "left_side_view", "");
                    //  SaveImageString.getInstance().setLeftsideview(path);

                }
                if (resultCodeforImage == 2005) {
                    rightsideview.setImageBitmap(myBitmap);
                    //   if (myBitmap != null) myBitmap = null;
                    startSendingImageToServerCounter(imageToString(rightside_image), rightside_image, "right_side_view", "");
                    //  SaveImageString.getInstance().setRightsideview(path);

                }
                if (resultCodeforImage == 2006) {
                    rearleftview.setImageBitmap(myBitmap);
                    //   if (myBitmap != null) myBitmap = null;
                    startSendingImageToServerCounter(imageToString(rearleft_image), rearleft_image, "rear_left_view", "");
                    //   SaveImageString.getInstance().setRearleft(path);

                }
                if (resultCodeforImage == 2007) {
                    frontright.setImageBitmap(myBitmap);
                    //   if (myBitmap != null) myBitmap = null;
                    startSendingImageToServerCounter(imageToString(frontright_image), frontright_image, "front_right_view", "");
                    //   SaveImageString.getInstance().setFrontright(path);

                }
                if (resultCodeforImage == 2008) {
                    rearright.setImageBitmap(myBitmap);
                    //   if (myBitmap != null) myBitmap = null;
                    startSendingImageToServerCounter(imageToString(rearright_image), rearright_image, "rear_right_view", "");
                    //   SaveImageString.getInstance().setRearright(path);

                }
                if (resultCodeforImage == 2009) {
                    wheelimg.setImageBitmap(myBitmap);
                    //   if (myBitmap != null) myBitmap = null;
                    startSendingImageToServerCounter(imageToString(wheels_image), wheels_image, "wheels_tyres", "");
                    //   SaveImageString.getInstance().setWheel(path);

                }
                if (resultCodeforImage == 2010) {
                    frontseat.setImageBitmap(myBitmap);
                    //   if (myBitmap != null) myBitmap = null;
                    startSendingImageToServerCounter(imageToString(front_seats_image), front_seats_image, "front_seats", "");
                    //   SaveImageString.getInstance().setFrontseats(path);

                }
                if (resultCodeforImage == 2011) {
                    backseat.setImageBitmap(myBitmap);
                    //   if (myBitmap != null) myBitmap = null;
                    startSendingImageToServerCounter(imageToString(back_seats_image), back_seats_image, "back_seats", "");
                    //   SaveImageString.getInstance().setBackseats(path);

                }
                if (resultCodeforImage == 2012) {
                    odometer.setImageBitmap(myBitmap);
                    //   if (myBitmap != null) myBitmap = null;
                    startSendingImageToServerCounter(imageToString(odometer_image), odometer_image, "odometer", "");
                    //    SaveImageString.getInstance().setOdometer(path);

                }
                if (resultCodeforImage == 2013) {
                    dasborad.setImageBitmap(myBitmap);
                    //   if (myBitmap != null) myBitmap = null;
                    startSendingImageToServerCounter(imageToString(dashboard_image), dashboard_image, "dashboard", "");
                    //   SaveImageString.getInstance().setDashboard(path);

                }
                if (resultCodeforImage == 2014) {
                    steering.setImageBitmap(myBitmap);
                    //   if (myBitmap != null) myBitmap = null;
                    startSendingImageToServerCounter(imageToString(steering_image), steering_image, "steering", "");
                    //    SaveImageString.getInstance().setSteering(path);

                }
                if (resultCodeforImage == 2015) {
                    frontrowseat.setImageBitmap(myBitmap);
                    //    if (myBitmap != null) myBitmap = null;
                    startSendingImageToServerCounter(imageToString(front_row_side_image), front_row_side_image, "front_row_side", "");
                    //    SaveImageString.getInstance().setFrontrowfromside(path);

                }
                if (resultCodeforImage == 2016) {
                    backrowseat.setImageBitmap(myBitmap);
                    //    if (myBitmap != null) myBitmap = null;
                    startSendingImageToServerCounter(imageToString(back_row_side_image), back_row_side_image, "back_row_side", "");
                    //    SaveImageString.getInstance().setBackrowfromside(path);

                }
                if (resultCodeforImage == 2017) {
                    doorimg.setImageBitmap(myBitmap);
                    //   if (myBitmap != null) myBitmap = null;
                    startSendingImageToServerCounter(imageToString(trunk_door_open_view_image), trunk_door_open_view_image, "trunk_door_open_view", "");
                    //    SaveImageString.getInstance().setTrunkdooropenview(path);

                }

                if (myBitmap != null) myBitmap = null;

            }
        } catch (Exception e) {
        }
    }

    public static Bitmap decodeFile(File f, int WIDTH, int HIGHT) {
        try {
            //Decode image size
            //   Log.e("Read ", "Read-3");

            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            //The new size we want to scale to
            final int REQUIRED_WIDTH = WIDTH;
            final int REQUIRED_HIGHT = HIGHT;
            //Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_WIDTH && o.outHeight / scale / 2 >= REQUIRED_HIGHT)
                scale *= 2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
            //  Log.e("Read ", "Read-4 " + e);

        }
        return null;
    }

    private void MethodOfVisibleLayout() {


     /*   if (CommonMethods.getstringvaluefromkey(this, "WhichTab").equals("Interior")) {
            exteriorTab.setTextColor(getResources().getColor(R.color.White));
            interiorTab.setTextColor(getResources().getColor(R.color.yellow));

            exteriorItem.setVisibility(View.GONE);
            interiorItem.setVisibility(View.VISIBLE);

            CommonMethods.setvalueAgainstKey(this, "WhichTab", "Interior");

        } else {*/
            exteriorTab.setTextColor(getResources().getColor(R.color.yellow));
            interiorTab.setTextColor(getResources().getColor(R.color.White));

            exteriorItem.setVisibility(View.VISIBLE);
            interiorItem.setVisibility(View.GONE);
            CommonMethods.setvalueAgainstKey(this, "WhichTab", "Exterior");

       // }

    }


    @Override
    protected void onResume() {
        super.onResume();

        //  Log.e("onResume", "onResume==" + imageIsAvaliable);

        Application.getInstance().trackScreenView(this,GlobalText.ImageUpload_1_Activity);

        if (imageIsAvaliable && CommonMethods.getstringvaluefromkey(activity, "imageIsAvaliable").equalsIgnoreCase("true")) {
            ImageLibraryInstance.getInstance().setImagestatus("false");
            MethodOfGettingCarImage();
            imageIsAvaliable = false;
        }

    }

    @Override
    public void onBackPressed() {
        CommonMethods.setvalueAgainstKey(this, "stockcreated", "true");
        Intent in_main = new Intent(ImageUploadActivity1.this, MainActivity.class);
        startActivity(in_main);
        finish();
    }


    private void loadImageFromURL(final Context mContext, final String mURL, final ImageView mView) {
       /* Picasso
                .with(mContext)
                .load(Uri.parse(mURL))
                .placeholder(R.drawable.loading_big)
                .resize(250, 250)
                .onlyScaleDown()
                .into(mView);*/


        Glide.with(mContext)
                .load(mURL)
                .apply(new RequestOptions().placeholder(R.drawable.loading_big))
                .into(mView);
    }


    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent != null) {
                String action = intent.getAction();
                if (action != null && action.equalsIgnoreCase(StencilCamConstant.ACTION_IMAGE_DETAILS)) {
                    String path = intent.getStringExtra(StencilCamConstant.RESULT);
                    Log.e("image from broadcast"," " + path);
                    imageIsAvaliable = true;
                    image1 = Uri.fromFile(new File(path));
                    Intent editimage = new Intent(ImageUploadActivity1.this, EditImageActivity.class);
                    // intent.putExtra("imageUri", image1);
                    startActivity(editimage);

                }
            }
        }
    };


    private void registerReceiver(){
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(StencilCamConstant.ACTION_IMAGE_DETAILS);
        registerReceiver(broadcastReceiver, intentFilter);
    }
}


