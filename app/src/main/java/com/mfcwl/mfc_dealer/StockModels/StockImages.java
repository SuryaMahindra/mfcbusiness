package com.mfcwl.mfc_dealer.StockModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StockImages {

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("category_identifier")
    @Expose
    private String categoryIdentifier;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCategoryIdentifier() {
        return categoryIdentifier;
    }

    public void setCategoryIdentifier(String categoryIdentifier) {
        this.categoryIdentifier = categoryIdentifier;
    }

}