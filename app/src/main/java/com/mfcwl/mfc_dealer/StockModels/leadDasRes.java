package com.mfcwl.mfc_dealer.StockModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class leadDasRes {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("status_code")
    @Expose
    private Integer statusCode;
    @SerializedName("web_leads")
    @Expose
    private List<webleadsASM> webLeads = null;
    @SerializedName("lead_status")
    @Expose
    private List<leadstatusASM> leadStatus = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<webleadsASM> getWebLeads() {
        return webLeads;
    }

    public void setWebLeads(List<webleadsASM> webLeads) {
        this.webLeads = webLeads;
    }

    public List<leadstatusASM> getLeadStatus() {
        return leadStatus;
    }

    public void setLeadStatus(List<leadstatusASM> leadStatus) {
        this.leadStatus = leadStatus;
    }

}
