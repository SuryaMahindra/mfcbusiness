package com.mfcwl.mfc_dealer.StockModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StockData {

    @SerializedName("stock_id")
    @Expose
    private Integer stockId;
    @SerializedName("stock_source")
    @Expose
    private String stockSource;
    @SerializedName("posted_date")
    @Expose
    private String postedDate;
    @SerializedName("vehicle_make")
    @Expose
    private String vehicleMake;
    @SerializedName("vehicle_model")
    @Expose
    private String vehicleModel;
    @SerializedName("vehicle_variant")
    @Expose
    private String vehicleVariant;
    @SerializedName("reg_month")
    @Expose
    private String regMonth;
    @SerializedName("reg_year")
    @Expose
    private String regYear;
    @SerializedName("registraion_city")
    @Expose
    private String registraionCity;
    @SerializedName("registration_number")
    @Expose
    private String registrationNumber;
    @SerializedName("colour")
    @Expose
    private String colour;
    @SerializedName("kilometer")
    @Expose
    private Integer kilometer;
    @SerializedName("owner")
    @Expose
    private Integer owner;
    @SerializedName("insurance")
    @Expose
    private String insurance;
    @SerializedName("insurance_exp_date")
    @Expose
    private String insuranceExpDate;
    @SerializedName("selling_price")
    @Expose
    private Integer sellingPrice;
    @SerializedName("certified_text")
    @Expose
    private String certifiedText;
    @SerializedName("certification_number")
    @Expose
    private String certificationNumber;
    @SerializedName("warranty_recommended")
    @Expose
    private String warrantyRecommended;
    @SerializedName("photo_count")
    @Expose
    private Integer photoCount;
    @SerializedName("surveyor_code")
    @Expose
    private String surveyorCode;
    @SerializedName("surveyor_modified_date")
    @Expose
    private String surveyorModifiedDate;
    @SerializedName("surveyor_kilometer")
    @Expose
    private Object surveyorKilometer;
    @SerializedName("surveyor_remark")
    @Expose
    private String surveyorRemark;
    @SerializedName("dealer_code")
    @Expose
    private String dealerCode;
    @SerializedName("is_certified")
    @Expose
    private String isCertified;
    @SerializedName("is_featured_car")
    @Expose
    private String isFeaturedCar;
    @SerializedName("fuel_type")
    @Expose
    private String fuelType;
    @SerializedName("bought_price")
    @Expose
    private Integer boughtPrice;
    @SerializedName("refurbishment_cost")
    @Expose
    private Integer refurbishmentCost;
    @SerializedName("procurement_executive_id")
    @Expose
    private Integer procurementExecutiveId;
    @SerializedName("procurement_executive_name")
    @Expose
    private String procurementExecutiveName;
    @SerializedName("cng_kit")
    @Expose
    private String cngKit;
    @SerializedName("chassis_number")
    @Expose
    private String chassisNumber;
    @SerializedName("engine_number")
    @Expose
    private String engineNumber;
    @SerializedName("is_booked")
    @Expose
    private String isBooked;
    @SerializedName("private_vehicle")
    @Expose
    private String privateVehicle;
    @SerializedName("comments")
    @Expose
    private String comments;
    @SerializedName("finance_required")
    @Expose
    private String financeRequired;
    @SerializedName("manufacture_month")
    @Expose
    private String manufactureMonth;
    @SerializedName("dealer_price")
    @Expose
    private Integer dealerPrice;
    @SerializedName("sales_executive_id")
    @Expose
    private Integer salesExecutiveId;
    @SerializedName("sales_executive_name")
    @Expose
    private String salesExecutiveName;
    @SerializedName("is_offload")
    @Expose
    private String isOffload;
    @SerializedName("manufacture_year")
    @Expose
    private String manufactureYear;
    @SerializedName("actual_selling_price")
    @Expose
    private String actualSellingPrice;
    @SerializedName("images")
    @Expose
    private List<StockImages> images = null;
    @SerializedName("cover_image")
    @Expose
    private String coverImage;

    public Integer getStockId() {
        return stockId;
    }

    public void setStockId(Integer stockId) {
        this.stockId = stockId;
    }

    public String getStockSource() {
        return stockSource;
    }

    public void setStockSource(String stockSource) {
        this.stockSource = stockSource;
    }

    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getVehicleVariant() {
        return vehicleVariant;
    }

    public void setVehicleVariant(String vehicleVariant) {
        this.vehicleVariant = vehicleVariant;
    }

    public String getRegMonth() {
        return regMonth;
    }

    public void setRegMonth(String regMonth) {
        this.regMonth = regMonth;
    }

    public String getRegYear() {
        return regYear;
    }

    public void setRegYear(String regYear) {
        this.regYear = regYear;
    }

    public String getRegistraionCity() {
        return registraionCity;
    }

    public void setRegistraionCity(String registraionCity) {
        this.registraionCity = registraionCity;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public Integer getKilometer() {
        return kilometer;
    }

    public void setKilometer(Integer kilometer) {
        this.kilometer = kilometer;
    }

    public Integer getOwner() {
        return owner;
    }

    public void setOwner(Integer owner) {
        this.owner = owner;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getInsuranceExpDate() {
        return insuranceExpDate;
    }

    public void setInsuranceExpDate(String insuranceExpDate) {
        this.insuranceExpDate = insuranceExpDate;
    }

    public Integer getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(Integer sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public String getCertifiedText() {
        return certifiedText;
    }

    public void setCertifiedText(String certifiedText) {
        this.certifiedText = certifiedText;
    }

    public String getCertificationNumber() {
        return certificationNumber;
    }

    public void setCertificationNumber(String certificationNumber) {
        this.certificationNumber = certificationNumber;
    }

    public String getWarrantyRecommended() {
        return warrantyRecommended;
    }

    public void setWarrantyRecommended(String warrantyRecommended) {
        this.warrantyRecommended = warrantyRecommended;
    }

    public Integer getPhotoCount() {
        return photoCount;
    }

    public void setPhotoCount(Integer photoCount) {
        this.photoCount = photoCount;
    }

    public String getSurveyorCode() {
        return surveyorCode;
    }

    public void setSurveyorCode(String surveyorCode) {
        this.surveyorCode = surveyorCode;
    }

    public String getSurveyorModifiedDate() {
        return surveyorModifiedDate;
    }

    public void setSurveyorModifiedDate(String surveyorModifiedDate) {
        this.surveyorModifiedDate = surveyorModifiedDate;
    }

    public Object getSurveyorKilometer() {
        return surveyorKilometer;
    }

    public void setSurveyorKilometer(Object surveyorKilometer) {
        this.surveyorKilometer = surveyorKilometer;
    }

    public String getSurveyorRemark() {
        return surveyorRemark;
    }

    public void setSurveyorRemark(String surveyorRemark) {
        this.surveyorRemark = surveyorRemark;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getIsCertified() {
        return isCertified;
    }

    public void setIsCertified(String isCertified) {
        this.isCertified = isCertified;
    }

    public String getIsFeaturedCar() {
        return isFeaturedCar;
    }

    public void setIsFeaturedCar(String isFeaturedCar) {
        this.isFeaturedCar = isFeaturedCar;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public Integer getBoughtPrice() {
        return boughtPrice;
    }

    public void setBoughtPrice(Integer boughtPrice) {
        this.boughtPrice = boughtPrice;
    }

    public Integer getRefurbishmentCost() {
        return refurbishmentCost;
    }

    public void setRefurbishmentCost(Integer refurbishmentCost) {
        this.refurbishmentCost = refurbishmentCost;
    }

    public Integer getProcurementExecutiveId() {
        return procurementExecutiveId;
    }

    public void setProcurementExecutiveId(Integer procurementExecutiveId) {
        this.procurementExecutiveId = procurementExecutiveId;
    }

    public String getProcurementExecutiveName() {
        return procurementExecutiveName;
    }

    public void setProcurementExecutiveName(String procurementExecutiveName) {
        this.procurementExecutiveName = procurementExecutiveName;
    }

    public String getCngKit() {
        return cngKit;
    }

    public void setCngKit(String cngKit) {
        this.cngKit = cngKit;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public String getEngineNumber() {
        return engineNumber;
    }

    public void setEngineNumber(String engineNumber) {
        this.engineNumber = engineNumber;
    }

    public String getIsBooked() {
        return isBooked;
    }

    public void setIsBooked(String isBooked) {
        this.isBooked = isBooked;
    }

    public String getPrivateVehicle() {
        return privateVehicle;
    }

    public void setPrivateVehicle(String privateVehicle) {
        this.privateVehicle = privateVehicle;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getFinanceRequired() {
        return financeRequired;
    }

    public void setFinanceRequired(String financeRequired) {
        this.financeRequired = financeRequired;
    }

    public String getManufactureMonth() {
        return manufactureMonth;
    }

    public void setManufactureMonth(String manufactureMonth) {
        this.manufactureMonth = manufactureMonth;
    }

    public Integer getDealerPrice() {
        return dealerPrice;
    }

    public void setDealerPrice(Integer dealerPrice) {
        this.dealerPrice = dealerPrice;
    }

    public Integer getSalesExecutiveId() {
        return salesExecutiveId;
    }

    public void setSalesExecutiveId(Integer salesExecutiveId) {
        this.salesExecutiveId = salesExecutiveId;
    }

    public String getSalesExecutiveName() {
        return salesExecutiveName;
    }

    public void setSalesExecutiveName(String salesExecutiveName) {
        this.salesExecutiveName = salesExecutiveName;
    }

    public String getIsOffload() {
        return isOffload;
    }

    public void setIsOffload(String isOffload) {
        this.isOffload = isOffload;
    }

    public String getManufactureYear() {
        return manufactureYear;
    }

    public void setManufactureYear(String manufactureYear) {
        this.manufactureYear = manufactureYear;
    }

    public String getActualSellingPrice() {
        return actualSellingPrice;
    }

    public void setActualSellingPrice(String actualSellingPrice) {
        this.actualSellingPrice = actualSellingPrice;
    }

    public List<StockImages> getImages() {
        return images;
    }

    public void setImages(List<StockImages> images) {
        this.images = images;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

}