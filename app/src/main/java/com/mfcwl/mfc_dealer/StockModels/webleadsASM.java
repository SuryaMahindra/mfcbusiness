package com.mfcwl.mfc_dealer.StockModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class webleadsASM {
    @SerializedName("staus")
    @Expose
    private String staus;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @SerializedName("filter")
    @Expose
    private List<leadFilterASM> filter = null;

    public String getStaus() {
        return staus;
    }

    public void setStaus(String staus) {
        this.staus = staus;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public List<leadFilterASM> getFilter() {
        return filter;
    }

    public void setFilter(List<leadFilterASM> filter) {
        this.filter = filter;
    }

}
