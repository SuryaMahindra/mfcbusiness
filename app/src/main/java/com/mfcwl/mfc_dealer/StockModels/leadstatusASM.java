package com.mfcwl.mfc_dealer.StockModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class leadstatusASM {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("filter")
    @Expose
    private List<leadFilterASM> filter = null;
    @SerializedName("display_name")
    @Expose
    private String displayName;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<leadFilterASM> getFilter() {
        return filter;
    }

    public void setFilter(List<leadFilterASM> filter) {
        this.filter = filter;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

}
