package com.mfcwl.mfc_dealer.RetrofitServices;

import android.content.Context;

import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.RequestModel.MarTraDealerDetailReq;
import com.mfcwl.mfc_dealer.RequestModel.MarketTradeRequest;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.ResponseModel.MarTraDealerDetailResp;
import com.mfcwl.mfc_dealer.StockModels.StockResponse;
import com.mfcwl.mfc_dealer.retrofitconfig.BaseService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class MarketTradeStockService extends BaseService {


    public static void fetchMarketStock(Context mContext, final MarketTradeRequest mMarketTradeRequest, HttpCallResponse mHttpCallResponse) {

        MarketTradeInterface mInterface = retrofit.create(MarketTradeInterface.class);
        Call<StockResponse> mCall = mInterface.getMarketTradeStock(mMarketTradeRequest);
        mCall.enqueue(new Callback<StockResponse>() {
            @Override
            public void onResponse(Call<StockResponse> call, Response<StockResponse> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }

                if (response.code() == 400) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 401) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 500) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 404) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 304) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 503) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 405) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                }
            }

            @Override
            public void onFailure(Call<StockResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });
    }

    public static void fetchMarTraDealerDetail(Context mContext, MarTraDealerDetailReq mMarTraDealerDetailReq, HttpCallResponse mHttpCallResponse) {
        MarketTradeInterface mInterface = retrofit.create(MarketTradeInterface.class);
        Call<MarTraDealerDetailResp> mCall = mInterface.getMarTraDealerDetail(mMarTraDealerDetailReq);
        mCall.enqueue(new Callback<MarTraDealerDetailResp>() {
            @Override
            public void onResponse(Call<MarTraDealerDetailResp> call, Response<MarTraDealerDetailResp> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
                if (response.code() == 400) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 401) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 500) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 404) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 304) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 503) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 405) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                }
            }

            @Override
            public void onFailure(Call<MarTraDealerDetailResp> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });

    }

    public interface MarketTradeInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("dealer/stocks/store")
        Call<StockResponse> getMarketTradeStock(@Body MarketTradeRequest mMarketTradeRequest);

        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("dealer/get-details")
        Call<MarTraDealerDetailResp> getMarTraDealerDetail(@Body MarTraDealerDetailReq mMarTraDealerDetailReq);

    }
}
