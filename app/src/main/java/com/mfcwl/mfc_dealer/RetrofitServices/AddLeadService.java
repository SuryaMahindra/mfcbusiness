package com.mfcwl.mfc_dealer.RetrofitServices;

import android.content.Context;

import com.mfcwl.mfc_dealer.AddStockModel.Procurement;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.RequestModel.ReqAddLead;
import com.mfcwl.mfc_dealer.RequestModel.ReqPrivateLeadFollowUp;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.ResponseModel.ResAddLead;
import com.mfcwl.mfc_dealer.ResponseModel.ResPrivateLeadFollowUp;
import com.mfcwl.mfc_dealer.retrofitconfig.BaseService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public class AddLeadService extends BaseService {

    //sales
    public static void getSalesFromServer(Context mContext, final HttpCallResponse mHttpCallResponse) {

        AddLeadInterface mAddLeadService = retrofit.create(AddLeadInterface.class);

        Call<List<Procurement>> mCall = mAddLeadService.getSales();
        mCall.enqueue(new Callback<List<Procurement>>() {
            @Override
            public void onResponse(Call<List<Procurement>> call, Response<List<Procurement>> response) {

                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
                if (response.code() == 400) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 401) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 500) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 404) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 304) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 503) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 405) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                }
            }

            @Override
            public void onFailure(Call<List<Procurement>> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });
    }

    //add lead
    public static void addLeadFromServer(ReqAddLead reqAddLead, Context mContext, HttpCallResponse mHttpCallResponse) {

        AddLeadInterface mAddLeadService = retrofit.create(AddLeadInterface.class);

        Call<ResAddLead> mCall = mAddLeadService.createLead(reqAddLead);
        mCall.enqueue(new Callback<ResAddLead>() {
            @Override
            public void onResponse(Call<ResAddLead> call, Response<ResAddLead> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {
                    if (response.code() == 400) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 401) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 500) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 404) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 304) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 503) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 405) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResAddLead> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });
    }

    //private followup lead
    public static void privateFollowUpLeadFromServer(ReqPrivateLeadFollowUp mReqPrivateLeadFollowUp, Context mContext, HttpCallResponse mHttpCallResponse) {
        AddLeadInterface mAddLeadInterface = retrofit.create(AddLeadInterface.class);
        Call<ResPrivateLeadFollowUp> mCall = mAddLeadInterface.privateFollowUpLead(mReqPrivateLeadFollowUp);
        mCall.enqueue(new Callback<ResPrivateLeadFollowUp>() {
            @Override
            public void onResponse(Call<ResPrivateLeadFollowUp> call, Response<ResPrivateLeadFollowUp> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
                if (response.code() == 400) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 401) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 500) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 404) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 304) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 503) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 405) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                }
            }

            @Override
            public void onFailure(Call<ResPrivateLeadFollowUp> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });
    }

    public interface AddLeadInterface {

        @Headers("Content-Type: application/json; charset=utf-8")
        @GET("dealer/active-executive-list/sales")
        Call<List<Procurement>> getSales();

        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("dealer/private-leads/create")
        Call<ResAddLead> createLead(@Body ReqAddLead mReqAddLead);

        //private lead followup update basePrivateLeadURL
        @Headers("Content-Type: application/json; charset=utf-8")
        @PUT("dealer/private-leads/update")
        Call<ResPrivateLeadFollowUp> privateFollowUpLead(@Body ReqPrivateLeadFollowUp mReqPrivateLeadFollowUp);

    }

}
