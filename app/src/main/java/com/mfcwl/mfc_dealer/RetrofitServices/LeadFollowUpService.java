package com.mfcwl.mfc_dealer.RetrofitServices;

import android.content.Context;

import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.RequestModel.ReqLeadFollowUp;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.ResponseModel.ResLeadFollowUp;
import com.mfcwl.mfc_dealer.retrofitconfig.WebLeadBaseService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class LeadFollowUpService extends WebLeadBaseService {


    //followup lead
    public static void leadFollowUpFromServer(ReqLeadFollowUp mReqLeadFollowUp, Context mContext, HttpCallResponse mHttpCallResponse) {
        LeadFollowUpInterface mInFollowUpInterface = retrofit.create(LeadFollowUpInterface.class);
        Call<ResLeadFollowUp> mCall = mInFollowUpInterface.leadFollowUp(mReqLeadFollowUp);
        mCall.enqueue(new Callback<ResLeadFollowUp>() {
            @Override
            public void onResponse(Call<ResLeadFollowUp> call, Response<ResLeadFollowUp> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
                if (response.code() == 400) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 401) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 500) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 404) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 304) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 503) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                } else if (response.code() == 405) {
                    WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                }
            }

            @Override
            public void onFailure(Call<ResLeadFollowUp> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });

    }

    public interface LeadFollowUpInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("leadupdate")
        Call<ResLeadFollowUp> leadFollowUp(@Body ReqLeadFollowUp mReqLeadFollowUp);

    }
}