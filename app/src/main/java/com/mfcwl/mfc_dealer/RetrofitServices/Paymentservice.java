package com.mfcwl.mfc_dealer.RetrofitServices;


import android.util.Log;

import com.mfcwl.mfc_dealer.ASMModel.paymentDetails;
import com.mfcwl.mfc_dealer.AddStockModel.paymentdetailsRes;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.retrofitconfig.BaseService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class Paymentservice extends BaseService {


    public void pushMultipleData(List<paymentDetails> paymentDetails , HttpCallResponse mCallResponse){
        WRSubmitInterface wrSubmitInterface = retrofit.create(WRSubmitInterface.class);
        Call<paymentdetailsRes> call = wrSubmitInterface.pushLsitofData(paymentDetails);
        call.enqueue(new Callback<paymentdetailsRes>() {
            @Override
            public void onResponse(Call<paymentdetailsRes> call, Response<paymentdetailsRes> response) {
                if(response.isSuccessful()){
                    mCallResponse.OnSuccess(response);
                    Log.d("Success Message"," "+response);
                }
            }

            @Override
            public void onFailure(Call<paymentdetailsRes> call, Throwable throwable) {
                mCallResponse.OnFailure(throwable);
                Log.d("Failure Message"," "+throwable);
            }
        });
    }

    public interface WRSubmitInterface
    {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("Dealer/SaveTransactionDetails")
        Call <paymentdetailsRes> pushLsitofData(@Body List<paymentDetails> paymentDetails);

    }

}
