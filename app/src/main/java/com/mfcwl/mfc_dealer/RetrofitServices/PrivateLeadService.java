package com.mfcwl.mfc_dealer.RetrofitServices;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.Procurement.ReqResModel.PWLRequest;
import com.mfcwl.mfc_dealer.RequestModel.PrivateLeadRequest;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.ResponseModel.PrivateLeadResponse;
import com.mfcwl.mfc_dealer.retrofitconfig.BaseService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class PrivateLeadService extends BaseService {

    public static void fetchPrivateLeads(Context mContext, PrivateLeadRequest mPrivateLeadRequest, HttpCallResponse mHttpCallResponse) {
        PrivateLeadInterface mInterface = retrofit.create(PrivateLeadInterface.class);
        Call<PrivateLeadResponse> mCall = mInterface.getPrivateLeads(mPrivateLeadRequest);
        Log.i("TAG", "URL "+mCall.request().url().toString());
        String str = new Gson().toJson(mPrivateLeadRequest, PrivateLeadRequest.class);
        Log.i("TAG", "Request "+str);

        mCall.enqueue(new Callback<PrivateLeadResponse>() {
            @Override
            public void onResponse(Call<PrivateLeadResponse> call, Response<PrivateLeadResponse> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                } else {
                    if (response.code() == 400) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 401) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 500) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 404) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 304) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 503) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 405) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    }
                }
            }

            @Override
            public void onFailure(Call<PrivateLeadResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);

            }
        });

    }

    public interface PrivateLeadInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("dealer/private-leads/getlead")
        Call<PrivateLeadResponse> getPrivateLeads(@Body PrivateLeadRequest mPrivateLeadRequest);
    }
}