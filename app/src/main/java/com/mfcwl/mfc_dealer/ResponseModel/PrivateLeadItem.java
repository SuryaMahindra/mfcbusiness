package com.mfcwl.mfc_dealer.ResponseModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrivateLeadItem {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("lead_type")
    @Expose
    private String leadType;
    @SerializedName("lead_title")
    @Expose
    private String leadTitle;
    @SerializedName("lead_date")
    @Expose
    private String leadDate;
    @SerializedName("lead_category")
    @Expose
    private String leadCategory;
    @SerializedName("lead_tags")
    @Expose
    private String leadTags;
    @SerializedName("lead_flag")
    @Expose
    private String leadFlag;
    @SerializedName("customer_id")
    @Expose
    private int customerId;
    @SerializedName("customer_prefix")
    @Expose
    private String customerPrefix;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("customer_mobile")
    @Expose
    private String customerMobile;
    @SerializedName("customer_email")
    @Expose
    private String customerEmail;
    @SerializedName("customer_comments")
    @Expose
    private String customerComments;
    @SerializedName("customer_xtras")
    @Expose
    private String customerXtras;
    @SerializedName("customer_prefs_communication")
    @Expose
    private String customerPrefsCommunication;
    @SerializedName("area_code")
    @Expose
    private String areaCode;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("area_mfc_branch")
    @Expose
    private String areaMfcBranch;
    @SerializedName("area_pincode")
    @Expose
    private String areaPincode;
    @SerializedName("stock_id")
    @Expose
    private int stockId;
    @SerializedName("stock_type")
    @Expose
    private String stockType;
    @SerializedName("make")
    @Expose
    private String make;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("variant")
    @Expose
    private String variant;
    @SerializedName("stock_model_year")
    @Expose
    private String stockModelYear;
    @SerializedName("register_month")
    @Expose
    private String registerMonth;
    @SerializedName("register_year")
    @Expose
    private String registerYear;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("kms")
    @Expose
    private int kms;
    @SerializedName("owner")
    @Expose
    private int owner;
    @SerializedName("register_no")
    @Expose
    private String registerNo;
    @SerializedName("register_city")
    @Expose
    private String registerCity;
    @SerializedName("stock_vinno")
    @Expose
    private String stockVinno;
    @SerializedName("insurance_type")
    @Expose
    private String insuranceType;
    @SerializedName("stock_insurance")
    @Expose
    private String stockInsurance;
    @SerializedName("stock_expected_price")
    @Expose
    private float stockExpectedPrice;
    @SerializedName("dealer_id")
    @Expose
    private String dealerId;
    @SerializedName("demand_for_dealer")
    @Expose
    private String demandForDealer;
    @SerializedName("dealer_quote")
    @Expose
    private Object dealerQuote;
    @SerializedName("dealer_comments")
    @Expose
    private String dealerComments;
    @SerializedName("lead_stage")
    @Expose
    private String leadStage;
    @SerializedName("lead_priority")
    @Expose
    private String leadPriority;
    @SerializedName("lead_status")
    @Expose
    private String leadStatus;
    @SerializedName("lead_source")
    @Expose
    private String leadSource;
    @SerializedName("marketting_source")
    @Expose
    private String markettingSource;
    @SerializedName("marketting_campaign")
    @Expose
    private String markettingCampaign;
    @SerializedName("marketting_device")
    @Expose
    private String markettingDevice;
    @SerializedName("client_ip")
    @Expose
    private String clientIp;
    @SerializedName("follow_date")
    @Expose
    private String followDate;
    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("associated_lead")
    @Expose
    private String associatedLead;
    @SerializedName("ref_id")
    @Expose
    private String refId;
    @SerializedName("ref_src")
    @Expose
    private String refSrc;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("comments")
    @Expose
    private String comments;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("updated_by")
    @Expose
    private String updatedBy;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("manufacturing_month")
    @Expose
    private int manufacturingMonth;
    @SerializedName("executive_name")
    @Expose
    private String executiveName;
    @SerializedName("executive_id")
    @Expose
    private int executiveId;
    @SerializedName("Vehicle_Type")
    @Expose
    private String vehicleType;
    @SerializedName("lead_date2")
    @Expose
    private String leadDate2;
    @SerializedName("created_by_device")
    @Expose
    private String createdByDevice;
    @SerializedName("updated_by_device")
    @Expose
    private String updatedByDevice;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLeadType() {
        return leadType;
    }

    public void setLeadType(String leadType) {
        this.leadType = leadType;
    }

    public String getLeadTitle() {
        return leadTitle;
    }

    public void setLeadTitle(String leadTitle) {
        this.leadTitle = leadTitle;
    }

    public String getLeadDate() {
        return leadDate;
    }

    public void setLeadDate(String leadDate) {
        this.leadDate = leadDate;
    }

    public String getLeadCategory() {
        return leadCategory;
    }

    public void setLeadCategory(String leadCategory) {
        this.leadCategory = leadCategory;
    }

    public String getLeadTags() {
        return leadTags;
    }

    public void setLeadTags(String leadTags) {
        this.leadTags = leadTags;
    }

    public String getLeadFlag() {
        return leadFlag;
    }

    public void setLeadFlag(String leadFlag) {
        this.leadFlag = leadFlag;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getCustomerPrefix() {
        return customerPrefix;
    }

    public void setCustomerPrefix(String customerPrefix) {
        this.customerPrefix = customerPrefix;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerComments() {
        return customerComments;
    }

    public void setCustomerComments(String customerComments) {
        this.customerComments = customerComments;
    }

    public String getCustomerXtras() {
        return customerXtras;
    }

    public void setCustomerXtras(String customerXtras) {
        this.customerXtras = customerXtras;
    }

    public String getCustomerPrefsCommunication() {
        return customerPrefsCommunication;
    }

    public void setCustomerPrefsCommunication(String customerPrefsCommunication) {
        this.customerPrefsCommunication = customerPrefsCommunication;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAreaMfcBranch() {
        return areaMfcBranch;
    }

    public void setAreaMfcBranch(String areaMfcBranch) {
        this.areaMfcBranch = areaMfcBranch;
    }

    public String getAreaPincode() {
        return areaPincode;
    }

    public void setAreaPincode(String areaPincode) {
        this.areaPincode = areaPincode;
    }

    public int getStockId() {
        return stockId;
    }

    public void setStockId(int stockId) {
        this.stockId = stockId;
    }

    public String getStockType() {
        return stockType;
    }

    public void setStockType(String stockType) {
        this.stockType = stockType;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public String getStockModelYear() {
        return stockModelYear;
    }

    public void setStockModelYear(String stockModelYear) {
        this.stockModelYear = stockModelYear;
    }

    public String getRegisterMonth() {
        return registerMonth;
    }

    public void setRegisterMonth(String registerMonth) {
        this.registerMonth = registerMonth;
    }

    public String getRegisterYear() {
        return registerYear;
    }

    public void setRegisterYear(String registerYear) {
        this.registerYear = registerYear;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getKms() {
        return kms;
    }

    public void setKms(int kms) {
        this.kms = kms;
    }

    public int getOwner() {
        return owner;
    }

    public void setOwner(int owner) {
        this.owner = owner;
    }

    public String getRegisterNo() {
        return registerNo;
    }

    public void setRegisterNo(String registerNo) {
        this.registerNo = registerNo;
    }

    public String getRegisterCity() {
        return registerCity;
    }

    public void setRegisterCity(String registerCity) {
        this.registerCity = registerCity;
    }

    public String getStockVinno() {
        return stockVinno;
    }

    public void setStockVinno(String stockVinno) {
        this.stockVinno = stockVinno;
    }

    public String getInsuranceType() {
        return insuranceType;
    }

    public void setInsuranceType(String insuranceType) {
        this.insuranceType = insuranceType;
    }

    public String getStockInsurance() {
        return stockInsurance;
    }

    public void setStockInsurance(String stockInsurance) {
        this.stockInsurance = stockInsurance;
    }

    public float getStockExpectedPrice() {
        return stockExpectedPrice;
    }

    public void setStockExpectedPrice(float stockExpectedPrice) {
        this.stockExpectedPrice = stockExpectedPrice;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getDemandForDealer() {
        return demandForDealer;
    }

    public void setDemandForDealer(String demandForDealer) {
        this.demandForDealer = demandForDealer;
    }

    public Object getDealerQuote() {
        return dealerQuote;
    }

    public void setDealerQuote(Object dealerQuote) {
        this.dealerQuote = dealerQuote;
    }

    public String getDealerComments() {
        return dealerComments;
    }

    public void setDealerComments(String dealerComments) {
        this.dealerComments = dealerComments;
    }

    public String getLeadStage() {
        return leadStage;
    }

    public void setLeadStage(String leadStage) {
        this.leadStage = leadStage;
    }

    public String getLeadPriority() {
        return leadPriority;
    }

    public void setLeadPriority(String leadPriority) {
        this.leadPriority = leadPriority;
    }

    public String getLeadStatus() {
        return leadStatus;
    }

    public void setLeadStatus(String leadStatus) {
        this.leadStatus = leadStatus;
    }

    public String getLeadSource() {
        return leadSource;
    }

    public void setLeadSource(String leadSource) {
        this.leadSource = leadSource;
    }

    public String getMarkettingSource() {
        return markettingSource;
    }

    public void setMarkettingSource(String markettingSource) {
        this.markettingSource = markettingSource;
    }

    public String getMarkettingCampaign() {
        return markettingCampaign;
    }

    public void setMarkettingCampaign(String markettingCampaign) {
        this.markettingCampaign = markettingCampaign;
    }

    public String getMarkettingDevice() {
        return markettingDevice;
    }

    public void setMarkettingDevice(String markettingDevice) {
        this.markettingDevice = markettingDevice;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public String getFollowDate() {
        return followDate;
    }

    public void setFollowDate(String followDate) {
        this.followDate = followDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAssociatedLead() {
        return associatedLead;
    }

    public void setAssociatedLead(String associatedLead) {
        this.associatedLead = associatedLead;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getRefSrc() {
        return refSrc;
    }

    public void setRefSrc(String refSrc) {
        this.refSrc = refSrc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getManufacturingMonth() {
        return manufacturingMonth;
    }

    public void setManufacturingMonth(int manufacturingMonth) {
        this.manufacturingMonth = manufacturingMonth;
    }

    public String getExecutiveName() {
        return executiveName;
    }

    public void setExecutiveName(String executiveName) {
        this.executiveName = executiveName;
    }

    public int getExecutiveId() {
        return executiveId;
    }

    public void setExecutiveId(int executiveId) {
        this.executiveId = executiveId;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getLeadDate2() {
        return leadDate2;
    }

    public void setLeadDate2(String leadDate2) {
        this.leadDate2 = leadDate2;
    }

    public String getCreatedByDevice() {
        return createdByDevice;
    }

    public void setCreatedByDevice(String createdByDevice) {
        this.createdByDevice = createdByDevice;
    }

    public String getUpdatedByDevice() {
        return updatedByDevice;
    }

    public void setUpdatedByDevice(String updatedByDevice) {
        this.updatedByDevice = updatedByDevice;
    }
}
