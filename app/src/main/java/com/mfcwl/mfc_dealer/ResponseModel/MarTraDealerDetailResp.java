package com.mfcwl.mfc_dealer.ResponseModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MarTraDealerDetailResp {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("dealer")
    @Expose
    private Dealer dealer;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Dealer getDealer() {
        return dealer;
    }

    public void setDealer(Dealer dealer) {
        this.dealer = dealer;
    }
}
