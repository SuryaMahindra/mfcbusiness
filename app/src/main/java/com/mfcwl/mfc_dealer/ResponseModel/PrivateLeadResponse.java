package com.mfcwl.mfc_dealer.ResponseModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PrivateLeadResponse {
    @SerializedName("current_page")
    @Expose
    private int currentPage;
    @SerializedName("per_page")
    @Expose
    private int perPage;
    @SerializedName("total")
    @Expose
    private int total;
    @SerializedName("data")
    @Expose
    private List<PrivateLeadItem> data = null;

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<PrivateLeadItem> getData() {
        return data;
    }

    public void setData(List<PrivateLeadItem> data) {
        this.data = data;
    }

}
