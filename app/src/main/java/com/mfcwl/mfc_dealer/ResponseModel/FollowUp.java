package com.mfcwl.mfc_dealer.ResponseModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FollowUp {
    @SerializedName("Follow_up")
    @Expose
    private String followUp;
    @SerializedName("Lead_Status")
    @Expose
    private String leadStatus;
    @SerializedName("Lead_remark")
    @Expose
    private String leadRemark;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("executive_name")
    @Expose
    private String executiveName;

    public String getFollowUp() {
        return followUp;
    }

    public void setFollowUp(String followUp) {
        this.followUp = followUp;
    }

    public String getLeadStatus() {
        return leadStatus;
    }

    public void setLeadStatus(String leadStatus) {
        this.leadStatus = leadStatus;
    }

    public String getLeadRemark() {
        return leadRemark;
    }

    public void setLeadRemark(String leadRemark) {
        this.leadRemark = leadRemark;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getExecutiveName() {
        return executiveName;
    }

    public void setExecutiveName(String executiveName) {
        this.executiveName = executiveName;
    }

}
