package com.mfcwl.mfc_dealer.Utility;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.SalesStocks.Instances.SalesCalendarFilterDialog;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicReference;

import static java.util.ResourceBundle.clearCache;

/**
 * Created by Shashir on 3/31/2016.
 */
public class CommonMethods {

    public static Context con;
    private static Activity activity;

    final public static double GALLERY_RESCALE_VALUE = .50;
    final public static double CAMERA_RESCALE_VALUE = .85;

    public static String getStringFromInputStream(InputStream stream) throws IOException {
        int n = 0;
        char[] buffer = new char[1024 * 4];
        InputStreamReader reader = new InputStreamReader(stream, "UTF8");
        StringWriter writer = new StringWriter();
        while (-1 != (n = reader.read(buffer))) writer.write(buffer, 0, n);
        return writer.toString();
    }

    public static boolean isInternetWorking(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }


    public static void alertMessage(final Activity activity, final String strMsg) {

        // custom dialog

        try {
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            // dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            dialog.setContentView(R.layout.common_alert);
            //dialog.setTitle("Custom Dialog");
            dialog.setCancelable(false);

            TextView Message, cancel;
            Button Confirm;
            Message = dialog.findViewById(R.id.Message);
            Confirm = dialog.findViewById(R.id.Confirm);

            Message.setText(strMsg);

            Confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();

                }
            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getTodayDate(Activity activity) {
        Date today = Calendar.getInstance().getTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String dateToStr = format.format(today);
        System.out.println(dateToStr);
        Log.i(activity.getClass().getSimpleName(), "getTodayDate" + dateToStr);
        return dateToStr;
    }

    public static String getTomorrowDateAndTime(Activity activity) {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        Date tomorrowDate = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String tomorrowDateTime = sdf.format(tomorrowDate);
        return tomorrowDateTime + " 23:59:59";
    }

    public static String getTodayAndTime(Activity activity) {
        final Calendar cal = Calendar.getInstance();
        Date todayDate = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String todayDateTime = sdf.format(todayDate);
        return todayDateTime;
    }

    public static boolean isPastDate(String date, String dateFormat) throws ParseException {


        Date parsedDate = new SimpleDateFormat(dateFormat, Locale.ENGLISH).parse(date);
        Date currentDate = new Date();

        if (parsedDate != null)
            return parsedDate.before(currentDate);
        return false;

    }

    public static String getMonthNameAndYear(Activity activity, String strDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = Calendar.getInstance().getTime();
        try {
            date = format.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String monthName = new SimpleDateFormat("MMMM", Locale.ENGLISH).format(date);
        String year = new SimpleDateFormat("yyyy").format(date);

        return monthName + " " + year;

    }

    public static String getFirstDay(Date d) {
        String formatedDate = "";
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(d);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            Date mdate = calendar.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            formatedDate = sdf.format(mdate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formatedDate;
    }

    public static void alertMessage2(final Activity activity, final String strMsg) {

        // custom dialog

        try {
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            // dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            dialog.setContentView(R.layout.common_alert);
            //dialog.setTitle("Custom Dialog");
            dialog.setCancelable(false);

            TextView Message, cancel;
            Button Confirm;
            Message = dialog.findViewById(R.id.Message);
            Confirm = dialog.findViewById(R.id.Confirm);

            Message.setText(strMsg);

            Confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                    activity.finish();

                }
            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void alertMessage(final Activity activity, final String strMsg, final String strMsg2) {

        // custom dialog

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        dialog.setContentView(R.layout.common_alert);
        //dialog.setTitle("Custom Dialog");
        dialog.setCancelable(false);

        TextView Message, Message2, cancel;
        Button Confirm;
        Message = dialog.findViewById(R.id.Message);
        Message2 = dialog.findViewById(R.id.Message2);
        Confirm = dialog.findViewById(R.id.Confirm);

        Message.setText(strMsg);
        Message2.setText(strMsg2);

        Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });

        dialog.show();
    }

    public static String replaceStringURL(String str) {
        String[] words = str.split(" ");
        StringBuilder sentence = new StringBuilder(words[0]);

        for (int i = 1; i < words.length; ++i) {
            sentence.append("%20");
            sentence.append(words[i]);
        }

        return sentence.toString();
    }

    public static void setvalueAgainstKey(Activity activity, String Key,
                                          String value) {

        try {
            SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(activity);
            SharedPreferences.Editor editor = preference.edit();
            editor.putString(Key, value);

            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void setDb(String Key, String value) {
        try {
            SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(Application.getAppContext());
            SharedPreferences.Editor editor = preference.edit();
            editor.putString(Key, value);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getDb(String key) {
        SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(Application.getAppContext());
        return preference.getString(key, "");
    }


    public static String getstringvaluefromkey(Activity activity, String key) {

        SharedPreferences preference = PreferenceManager
                .getDefaultSharedPreferences(Application.getAppContext());

        return preference.getString(key, "");

    }

    public static Set<String> getstringvaluefromkeyArray(Activity activity, String key) {

        SharedPreferences preference = PreferenceManager
                .getDefaultSharedPreferences(Application.getAppContext());

        return preference.getStringSet(key, null);

    }

    public static String getToken(String key) {

        SharedPreferences preference = PreferenceManager
                .getDefaultSharedPreferences(Application.getAppContext());

        return preference.getString(key, "");

    }


    public static String getTime() {

        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
        Date currentLocalTime = cal.getTime();
        DateFormat date = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        date.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
        String localTime = date.format(currentLocalTime);
        return localTime;

    }


    // Clear Memory
    public static void MemoryClears() {
        clearCache();
        System.gc();

    }

    public static void deleteCache(Activity activity) {
        try {
            File dir = activity.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    //VersionUpgrade Pop Up

    public static void error_popup(Context context, String url) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.common_alert);
        TextView Message;
        Button Confirm;
        Message = dialog.findViewById(R.id.Message);
        Confirm = dialog.findViewById(R.id.Confirm);
        dialog.setCancelable(false);

        Message.setText(GlobalText.APP_VERSION);

        Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                playstore(context, url);

            }
        });

        dialog.show();

    }

    public static void playstore(Context context, String url) {

        try {
            Intent viewIntent =
                    new Intent("android.intent.action.VIEW",
                            Uri.parse(url));
            context.startActivity(viewIntent);
        } catch (Exception e) {
            Toast.makeText(activity, "Unable to Connect Try Again.",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    @SuppressLint("NewApi")
    public static boolean isConnectingToInternet(Activity activity) {
        ConnectivityManager connectivity;
        if (null == activity) {
            connectivity = (ConnectivityManager) Application.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        } else {
            connectivity = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        }
        if (null != connectivity) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (NetworkInfo anInfo : info)
                    if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                        StrictMode.setThreadPolicy(policy);
                        NetworkInfo activeNetwork = connectivity.getActiveNetworkInfo();
                        return activeNetwork != null
                                && activeNetwork.isConnectedOrConnecting();
                    }

        }
        return false;
    }

    public void error_popup(int str, String error, Context mContext) {
        final Dialog dialog = new Dialog(mContext, R.style.Theme_AppCompat_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.common_alert);
        TextView Message = dialog.findViewById(R.id.Message);
        Button Confirm = dialog.findViewById(R.id.Confirm);
        dialog.setCancelable(false);

        if (str == 400) {
            Message.setText("400 Bad Request Error.");
        } else if (str == 401) {
            Message.setText("401 Unauthorized Error.");
        } else if (str == 500) {
            Message.setText("500 Internal Server Error.");
        } else if (str == 404) {
            Message.setText("Not found 404 Error.");
        } else if (str == 304) {
            Message.setText("Not Modified 304 Error.");
        } else if (str == 503) {
            Message.setText("Gateway timeout 503 Error.");
        } else if (str == 405) {
            Message.setText("405 Server Error(In Request Parameter)");
        } else {
            Message.setText(error);
        }

        Confirm.setOnClickListener(v -> {

            if (str == 401) {
                CommonMethods.setvalueAgainstKey(MainActivity.activity, "appstatus", "0");
                MainActivity.logOut();
                dialog.dismiss();
            } else {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static String getTodayDate() {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return df.format(c);
    }

    public static boolean selectedTimeIsValid(String currentTime, String selectedTime) {

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
        Date stTime = Calendar.getInstance().getTime(), edTime = Calendar.getInstance().getTime();
        try {
            stTime = sdf.parse(currentTime);
            edTime = sdf.parse(selectedTime);
            Log.i("CommonMethods", "isValidEndTime: " + stTime + "----->" + edTime);
        } catch (ParseException exception) {
            exception.printStackTrace();
        }
        if (edTime.after(stTime) || edTime.equals(stTime)) {
            Log.i("CommonMethods", "isValidEndTime: " + edTime.before(stTime));
            return true;
        } else {
            Log.i("CommonMethods", "isValidEndTime: " + edTime.before(stTime));
            return false;
        }
    }

    public static String getLastSevenDays() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -7);

        Calendar cal1 = Calendar.getInstance();
        /*cal1.add(Calendar.DATE, 0);*/

        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

        String startDate = s.format(new Date(cal.getTimeInMillis()));
        String endDate = s.format(new Date(cal1.getTimeInMillis()));

        return startDate + "-" + endDate;
    }

    public static String getLastFifteenDays() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -15);

        Calendar cal1 = Calendar.getInstance();
        /*cal1.add(Calendar.DATE, 0);*/

        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

        String startDate = s.format(new Date(cal.getTimeInMillis()));
        String endDate = s.format(new Date(cal1.getTimeInMillis()));

        return startDate + "-" + endDate;
    }

    public static void insertToSP(Activity activity, HashMap<String, Integer> jsonMap) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences("LeadStatus", activity.MODE_PRIVATE);
        String jsonString = new Gson().toJson(jsonMap);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("map", jsonString);
        editor.apply();
    }

    public static HashMap<String, Integer> readFromSP(Activity activity) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences("LeadStatus", activity.MODE_PRIVATE);
        String defValue = new Gson().toJson(new HashMap<String, Integer>());
        String json = sharedPreferences.getString("map", "");
        TypeToken<HashMap<String, Integer>> token = new TypeToken<HashMap<String, Integer>>() {
        };
        HashMap<String, Integer> retrievedMap = new Gson().fromJson(json, token.getType());
        return retrievedMap;
    }

    public static void clearLeadStatusMap(Activity activity) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences("LeadStatus", activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove("map");
        editor.commit();
    }


    public static String getLastThirtyDays() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -30);

        Calendar cal1 = Calendar.getInstance();
        /*cal1.add(Calendar.DATE, 0);*/

        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

        String startDate = s.format(new Date(cal.getTimeInMillis()));
        String endDate = s.format(new Date(cal1.getTimeInMillis()));

        return startDate + "-" + endDate;
    }
}

