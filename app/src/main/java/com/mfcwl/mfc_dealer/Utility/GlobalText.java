package com.mfcwl.mfc_dealer.Utility;

/**
 * Created by HP 240 G4 on 13-11-2017.
 */

public interface GlobalText {

    int GET = 0;
    int POST = 1;
    int PUT = 2;
    int DELETE = 3;
    int HEAD = 4;
    // int HEAD = 4;

    String AUTH_ERROR = "Please Login again to continue";
    String QUOTE_SUBMIT_SUCCESS = "Your Quote Is Submitted Successfully";
    String LEAD_REMOVE_SUCCESS = "Lead removed successfully.";
    String SERVER_ERROR = "Server error.";
    String USER_NAME_INVALID = "User Name or Password is Invalid";
    String HIGHER_QUOTE = "Cannot submit an amount lower than previous quote.";
    String TRY_AGAIN = "Try Again.";
    String NETWORK_ERROR = "Network or Server Problem.";
    String CHECK_NETWORK_CONNECTION = "Your internet connection appears to be offline. Please retry later.";
    String ENTER_QUOTE = "Please Enter Quote.";
    String HIGHER_THAN_5000 = "Quote amount should be atleast Rs. 5000 higher than previous bid.";
    String LEAD_REMOVE_TEXT = "This will remove the car from your My Leads section.";
    String APP_VERSION = "A newer version of this App is now available. Please upgrade to the latest version.";

    // Google Analytics
    String Splash = "Launcher Screen - Android";
    String login = "Login Screen - Android";
    String Mainactivity = "Dashboard Screen - Android";
    String Home = "Home Screen - Android";
    String LeadDetailsActivity = "Lead Detail Screen - Android";
    String LeadfilterActivity = "Lead Filter Screen - Android";
    String AddleadActivity = "Add Lead Screen - Android";
    String AddstockActivity = "Add Stock Screen - Android";
    String AddstocksuccessActivity = "Add Stock success Screen - Android";
    String booknowActivity = "Book Now Screen - Android";
    String EditImageActivity = "Edit Image Screen - Android";
    String FilterPriceActivity = "Filter Price Screen - Android";
    String FilterYearActivity = "Filter Year Screen - Android";
    String IbbPriceActivity = "Ibb Price Screen - Android";
    String ImageUpload_Activity = "Image Upload Screen - Android";
    String ImageUpload_1_Activity = "Image Upload_1 Screen - Android";
    String Maps_Activity = "Map Screen - Android";
    String PhotoEditor_Activity = "Text Editor Screen - Android";
    String SimpleSample_Activity = "Simple Sample Screen - Android";
    String StockFeaturedCar_Activity = "Stock Featured Screen - Android";
    String StockFilter_Activity = "Stock Filter Screen - Android";
    String StockStoreInfo_Activity = "Stock StoreInfo Screen - Android";
    String ToolsFilter_Activity = "Tools Filter Screen - Android";
    String ViewSamples_Activity = "Stock Image Sample Screen - Android";
    String Notification_list_Activity = "Notification Screen - Android";
    String EditStockActivity = "Edit Stock Screen - Android";
    String GalleryActivity = "Gallery Screen - Android";
    String FollowUpHistory_Fragment = "Followup Lead Screen - Android";
    String LeadDetails_Fragment = "Lead Details Screen - Android";
    String LeadFilter_Fragment = "Lead Filter Screen - Android";
    String LeadFragment_Fragment = "Lead Screen - Android";
    String NormalLeadDetails_Fragment = "Lead Details Screen - Android";
    String NormalLead_Fragment = "Web Lead Screen - Android";
    String PrivateLead_Fragment = "Private Lead Screen - Android";
    String Gallery_Fragment = "Image Editor Screen - Android";
    String Home_Fragment = "Home Screen - Android";
    String Stock_Fragment = "Stock Screen - Android";
    String StockStore_Fragment = "Store Stocks Screen - Android";
    String BookedStock_Fragment = "Booked Stocks Screen - Android";
    String ToolFragment_Fragment = "Market Trade Screen - Android";
    String ToolIbbCpo_Fragment = "Ibb Cpo Screen - Android";
    String ToolIbbPrice_Fragment = "Ibb Price Screen - Android";
    String ToolIbbPrivate_Fragment = "Ibb Private Screen - Android";
    String ToolIbbRetail_Fragment = "Ibb Retail Screen - Android";
    String ToolTradeInPrice_Fragment = "Trade-in Price Screen - Android";
    String proc_leads = "proc leads Screen - Android";

    // Google Analytics >>> OnClick Events
    String android = "Android";
    String home = "Home Tab - Android";
    String stocks = "Stocks Tab - Android";
    String add_stock = "Add Stock Tab - Android";
    String add_lead_tab = "Add Lead Tab - Android";
    String leads = "Leads Tab - Android";
    String market_trade = "Market Trade Tab - Android";
    String view_ibb_price = "View IBB Price Tab - Android";
    String stock_filter = "Stock Filter - Android";
    String stock_short = "Stock Short - Android";
    String add_image = "Add image stock - Android";
    String share_image = "Share image - Android";
    String share_details = "Share details - Android";
    String edit_stocks = "Edit stocks - Android";
    String edit_primary_stocks = "Edit primary details stocks - Android";
    String edit_additional_stocks = "Edit additional details stocks - Android";
    String book_stocks = "Book stocks - Android";
    String book_now_stock = "Book now stocks - Android";
    String exit_book_now = "Book now stocks - Android";
    String store_stock = "Store Stocks - Android";
    String booked_stock = "Booked Stocks - Android";
    String add_primary_stoke = "Add Primary Stock - Android";
    String add_additional_stoke = "Add Additional Stock - Android";
    String c2c_ibb_stock = "Add as C2C/IBB Stock - Android";
    String add_regular_stock = "Add as Regular Stock - Android";
    String add_lead = "Add Lead - Android";
    String web_leads = "Web Leads - Android";
    String web_all_leads = "Web All Leads - Android";
    String web_follow_leads = "Web Follow-up Leads - Android";
    String private_leads = "Private Leads - Android";
    String private_all_leads = "Private All Leads - Android";
    String private_follow_leads = "Private Follow-up Leads - Android";
    String lead_sort_by = "Leads Sort By - Android";
    String lead_filter = "Leads filter - Android";
    String lead_filter_apply = "Leads Filter Apply - Android";
    String lead_filter_clear = "Leads Filter Clear All - Android";
    String lead_sortby_apply = "Leads Sort By Apply - Android";
    String web_leads_whatsapp = "Web Leads WhatsApp - Android";
    String web_leads_message = "Web Leads Message - Android";
    String web_leads_call = "Web Leads Call - Android";
    String private_leads_call = "Private Leads Call - Android";
    String private_leads_message = "Private Leads Message - Android";
    String private_leads_whatsapp = "Private Leads WhatsApp - Android";
    String leads_details_whatsapp = "Lead Details WhatsApp - Android";
    String leads_details_message = "Lead Details Message - Android";
    String leads_details_call = "Lead Details Call - Android";
    String leads_details_booknow = "Lead Details Book Now - Android";
    String leads_details_update = "Lead Details Update Follow-up - Android";
    String tool_shortby = "Tool Short By - Android";
    String tool_filter = "Tool Filter - Android";
    String tool_filter_clear = "Tool Filter Clear - Android";
    String tool_apply_shortby = "Tool Apply Short By - Android";
    String tool_dealer_details = "Tool Dealer Details - Android";
    String ibb_check_price = "Check IBB Price - Android";
    String nav_my_profile = "My Profile - Android";
    String nav_my_profile_share = "My Profile Share - Android";
    String nav_logout = "Logout - Android";
    String sales_leads = "Sales Lead Tab - Android";
    // String proc_leads = "Proc.Lead Tab - Android";

    //========================== Procurement ===============================
    String add_proc_leads = "Add Proc.Lead - Android";
    String proc_privateLeads = "Proc. Private Lead - Android";
    String proc_webLeads = "Proc. Web Lead - Android";
    String proc_sortby = "Proc. SortBy - Android";
    String proc_filter = "Proc. Filter - Android";
    String proc_private_allLeads = "Proc. Private All Lead - Android";
    String proc_private_followLeads = "Proc. Private Follow-up Lead - Android";
    String proc_web_allLeads = "Proc. Web All Lead - Android";
    String proc_web_followLeads = "Proc. Web Follow-up Lead - Android";
    String proc_web_whatsapp = "Proc. Web Lead WhatsApp - Android";
    String proc_web_message = "Proc. Web Lead Message - Android";
    String proc_web_call = "Proc. Web Lead Phone Call - Android";
    String proc_private_whatsapp = "Proc. Private Lead WhatsApp - Android";
    String proc_private_message = "Proc. Private Lead Message - Android";
    String proc_private_call = "Proc. Private Lead Call - Android";
    String proc_followupHistory = "Proc. Follow-up History - Android";
    String proc_Aboutlead = "Proc. About Lead - Android";
    String proc_Aboutlead_call = "Proc. About Lead Call - Android";
    String proc_Aboutlead_message = "Proc. About Lead Message - Android";
    String proc_Aboutlead_whatsApp = "Proc. About Lead WhatsApp - Android";
    String proc_convertTostock = "Proc. Convert Stock - Android";

    //ASM logins
    String asm_dashboard = "Asm Dashboard - Android";
    String asm_message_center = "Asm Message Center - Android";
    String asm_stocks_dashboard = "Asm Stock Dashboard - Android";
    String asm_stock_listing = "Asm Stock Listing - Android";
    String asm_booked_listing = "Asm Booked Listing - Android";
    String asm_stock_filter = "Asm stock Filter - Android";
    String asm_stock_sortby = "Asm Stock SortBy - Android";
    String asm_stock_details = "Asm Stock Details - Android";

    String asm_leads_dashboard = "Asm Leads Dashboard - Android";
    String asm_privateLeads = "Asm Private Lead - Android";
    String asm_webLeads = "Asm Web Lead - Android";
    String asm_private_allLeads = "Asm Private All Lead - Android";
    String asm_private_followLeads = "Asm Private Follow-up Lead - Android";
    String asm_web_allLeads = "Asm Web All Lead - Android";
    String asm_web_followLeads = "Asm Web Follow-up Lead - Android";
    String asm_followupHistory = "Asm Follow-up History - Android";
    String asm_Aboutlead = "Asm About Lead - Android";
    String asm_lead_filter = "Asm Leads Filter - Android";
    String asm_leads_sorts = "Asm Leads Sorts - Android";

    String asm_sales_dashboard = "Asm Sales Dashboard - Android";
    String asm_total_sales_listing = "Asm Total Sales Listing - Android";
    String asm_warranty_listing = "Asm Warranty Listing - Android";
    String asm_sales_filter = "Asm Sales Filter - Android";
    String asm_sales_sortby = "Asm Sales SortBy - Android";

    String asm_report_dashboard = "Asm Report Dashboard - Android";
    String dashboard = "Dashboard";
    String stateHeadDashboard = "State Head Dashboard";
    String zonalHeadDashboard = "Zonal Head Dashboard";

    String USERTYPE_ASM = "AM";
    String USERTYPE_ZONALHEAD = "ZM";
    String USERTYPE_STATEHEAD = "SM";

    String USER_TYPE_DEALER_CRE = "dealer cre";
    String USER_TYPE_SALES = "sales";
    String USER_TYPE_PROCUREMENT = "procurement";

    String dealerTypeLablel = "Dealer Type : ";
    String warrentyBalLabel = "Warranty Bal: ";
    String warrantyPunchLabel = "T.W.S.R.(MTD): ";

    String label4Wheeler = "4 Wheeler";
    String HEADING = "HEADING";

    String are_you_sure_to_send_mail = "Are you sure you want to send mail?";
    String are_you_sure_to_delete = "Do you want to remove attachment?";

    String paymentModeTitle = "Mode of Payment";
    String PrefillTitle = "Prefill from existing data";

    String BUCKET_KEY = "accessKey";
    String BUCKET_SECRET = "secretKey";



}
