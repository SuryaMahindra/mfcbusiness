package com.mfcwl.mfc_dealer.Utility;


public class Global_Urls {

    private String baseURL;
    private String baseWebURL;
    private String whichType;


    // RDM Event BaseURL

    public String getRdmEventBaseURL() {
        return rdmEventBaseURL;
    }

    public void setRdmEventBaseURL(String rdmEventBaseURL) {
        this.rdmEventBaseURL = rdmEventBaseURL;
    }

    private String rdmEventBaseURL;

    public String getLprBucketName() {
        return lprBucketName;
    }

    public void setLprBucketName(String lprBucketName) {
        this.lprBucketName = lprBucketName;
    }

    private String lprBucketName;

    private String IbbAccessToken;
    public static String ibb_uname, ibb_password;


    //ibb
    public String getIbbAccessToken() {
        return IbbAccessToken;
    }

    public void setIbbAccessToken(String ibbAccessToken) {
        IbbAccessToken = ibbAccessToken;
    }

    //LeadAPI
    private String leadAccessToken;

    private String leadBaseUrl;

    public String getImageBucketName() {
        return imageBucketName;
    }

    public void setImageBucketName(String imageBucketName) {
        this.imageBucketName = imageBucketName;
    }

    private String imageBucketName;

    public String getVideoBucketName() {
        return videoBucketName;
    }

    public void setVideoBucketName(String videoBucketName) {
        this.videoBucketName = videoBucketName;
    }

    private String videoBucketName;

    public String getMainUrl() {
        return mainUrl;
    }

    public void setMainUrl(String mainUrl) {
        this.mainUrl = mainUrl;
    }

    private String mainUrl;

    public static String loginUrl;

    public static String getAddstock() {
        return addstock;
    }

    public static String getStock_dealer() {
        return stock_dealer;
    }

    public static String getIbbMFC() {
        return ibbMFC;
    }

    public static String addstock;
    public static String stock_dealer;

    public static String getWedleads() {
        return wedleads;
    }

    public static String wedleads;
    public static String ibbMFC;

    private String cityUrl;

    public String getAddproc() {
        return addproc;
    }

    public void setAddproc(String addproc) {
        this.addproc = addproc;
    }

    String addproc;

    public String getProclist() {
        return proclist;
    }

    public void setProclist(String proclist) {
        this.proclist = proclist;
    }

    String proclist;

    public String getLeadstatus() {
        return leadstatus;
    }

    public void setLeadstatus(String leadstatus) {
        this.leadstatus = leadstatus;
    }

    String leadstatus;

    public String getDasborad() {
        return dasborad;
    }

    public void setDasborad(String dasborad) {
        this.dasborad = dasborad;
    }

    String dasborad, stock_dasborad;

    public String getDealerParkAndSellDeleteURL() {
        return dealerParkAndSellDeleteURL;
    }

    public String dealerParkAndSellDeleteURL;

    public Global_Urls(String whichTypeCheck) {
        this.whichType = whichTypeCheck;
        if (this.whichType.equalsIgnoreCase("Production")) {

            loginUrl = "https://newadmin.emfcwl.com/mfc_apis/v3/api/login/dealer";
            this.cityUrl = "https://newadmin.emfcwl.com/mfc_apis/v3/api/master/citylist";
            this.mainUrl = "https://newadmin.emfcwl.com/mfc_apis/v3/api/dealer/stocks/store";
            addstock = "https://newadmin.emfcwl.com/mfc_apis/v3/api/master/";
            stock_dealer = "https://newadmin.emfcwl.com/mfc_apis/v3/api/";


            this.leadAccessToken = "http://lms-api.prodibb.com/public/api/mfcwapp/generateAccessToken";
            this.leadBaseUrl = "http://lms-api.prodibb.com/public/api/";
            wedleads = "http://lms-api.prodibb.com/public/api/mfcwapp/";

            //ibb
            // https://system.indianbluebook.com/api/
            ibbMFC = "https://system.indianbluebook.com/api/";
            this.IbbAccessToken = "https://system.indianbluebook.com/api/get_access_token";
            ibb_uname = "mfc@ibb.com";
            ibb_password = "LVUM2gHfiDrJ";
            //dHk69ffu7ebP

            //Procurement
            //v2
            /*addproc = "https://newadmin.emfcwl.com/mfc_apis/v2/api/dealer/procurement/";
            proclist = "https://newadmin.emfcwl.com/mfc_apis/v2/api/dealer/";
            leadstatus = "https://newadmin.emfcwl.com/mfc_apis/v2/api/dealer/lead/";*/

            //Procurement
            addproc = "https://newadmin.emfcwl.com/mfc_apis/v3/api/dealer/procurement/";
            proclist = "https://newadmin.emfcwl.com/mfc_apis/v3/api/dealer/";
            leadstatus = "https://newadmin.emfcwl.com/mfc_apis/v3/api/dealer/lead/";


            dasborad = "https://newadmin.emfcwl.com/mfc_apis/v3/api/";

            String mainURL = "https://newadmin.emfcwl.com/mfc_apis/v3/api/";

            dealerParkAndSellDeleteURL = mainURL + "dealer/parkandsellstock/delete/";

            //For PayU Payment
            merchantkey = "UH5Vlw";
            salt = "zj1nZIE5";
            paymentbaseurl = "https://secure.payu.in";

            imageBucketName="mfc-offline-payment";
            videoBucketName="mfcwl-vehicle-videos-live-1"; //"mfcwl-vehicle-videos-live";
            lprBucketName = "lpr-live";


            //leadBaseURL
            rdmEventBaseURL="https://newadmin.emfcwl.com/mfc_apis/v3/api/franchise/event/";
        }
        if (this.whichType.equalsIgnoreCase("Staging")) {

            loginUrl = "https://newadmin.stagemfc.com/mfc_apis/v2/api/login/dealer";
            this.cityUrl = "https://newadmin.stagemfc.com/mfc_apis/v2/api/master/citylist";
            this.mainUrl = "https://newadmin.stagemfc.com/mfc_apis/v2/api/dealer/stocks/store";
            addstock = "https://newadmin.stagemfc.com/mfc_apis/v2/api/master/";
            stock_dealer = "https://newadmin.stagemfc.com/mfc_apis/v2/api/";
            ibbMFC = "https://api2.stageibb.com/api/";

            wedleads = "http://lms.stageibb.com/leads_microservice/public/api/mfcwapp/";

            //LMS
            this.leadAccessToken = "http://lms.stageibb.com/leads_microservice/public/api/mfcwapp/generateAccessToken";
            this.leadBaseUrl = "http://lms.stageibb.com/leads_microservice/public/api/";

            //ibb
            this.IbbAccessToken = "https://api2.stageibb.com/api/get_access_token";
            ibb_uname = "mfc@ibb.com";
            ibb_password = "dHk69ffu7ebP";
            //dHk69ffu7ebP

            //Procurement
            addproc = "https://newadmin.stagemfc.com/mfc_apis/v2/api/dealer/procurement/";
            proclist = "https://newadmin.stagemfc.com/mfc_apis/v2/api/dealer/";

            leadstatus = "https://newadmin.stagemfc.com/mfc_apis/v2/api/dealer/lead/";

            dasborad = "https://newadmin.stagemfc.com/mfc_apis/v2/api/";

            String mainURL = "https://newadmin.stagemfc.com/mfc_apis/v2/api/";

            dealerParkAndSellDeleteURL = mainURL + "dealer/parkandsellstock/delete/";

            //For PayU Payment
            merchantkey = "cO7sIO";
            salt = "DKeZBOnf";
            paymentbaseurl = "https://test.payu.in";

            imageBucketName="mfcwl-dealer-payment-images";
            videoBucketName="mfcwl-vehicle-videos-live-1";
            lprBucketName = "lpr-stage";
            //leadBaseURL
            rdmEventBaseURL="https://newadmin.stagemfc.com/mfc_apis/v2/api/franchise/event/";
        }

        // Pre Production

        if (this.whichType.equalsIgnoreCase("PreProduction")) {

            // V1
            /*loginUrl = "https://newadmin.emfcwl.com/mfc_apis/v1/api/login/dealer";
            this.cityUrl = "https://newadmin.emfcwl.com/mfc_apis/v1/api/master/citylist";
            this.mainUrl = "https://newadmin.emfcwl.com/mfc_apis/v1/api/dealer/stocks/store";
            addstock = "https://newadmin.emfcwl.com/mfc_apis/v1/api/master/";
            stock_dealer = "https://newadmin.emfcwl.com/mfc_apis/v1/api/";*/

            //V2
            /*loginUrl = "https://newadmin.emfcwl.com/mfc_apis/v2/api/login/dealer";
            this.cityUrl = "https://newadmin.emfcwl.com/mfc_apis/v2/api/master/citylist";
            this.mainUrl = "https://newadmin.emfcwl.com/mfc_apis/v2/api/dealer/stocks/store";
            addstock = "https://newadmin.emfcwl.com/mfc_apis/v2/api/master/";
            stock_dealer = "https://newadmin.emfcwl.com/mfc_apis/v2/api/";*/

            //V3
            loginUrl = "https://newadmin.emfcwl.com/mfc_apis/preprod/rdmr3/api/login/dealer";
            this.cityUrl = "https://newadmin.emfcwl.com/mfc_apis/preprod/rdmr3/api/master/citylist";
            this.mainUrl = "https://newadmin.emfcwl.com/mfc_apis/preprod/rdmr3/api/dealer/stocks/store";
            addstock = "https://newadmin.emfcwl.com/mfc_apis/preprod/rdmr3/api/master/";
            stock_dealer = "https://newadmin.emfcwl.com/mfc_apis/v3/api/"; //"https://newadmin.emfcwl.com/mfc_apis/preprod/rdmr3/api/";

            //LMS
            // https://lms-api.prodibb.com/public/api/mfcwapp/

            this.leadAccessToken = "http://lms-api.prodibb.com/public/api/mfcwapp/generateAccessToken";
            this.leadBaseUrl = "http://lms-api.prodibb.com/public/api/";
            wedleads = "http://lms-api.prodibb.com/public/api/mfcwapp/";

            //ibb
            // https://system.indianbluebook.com/api/
            ibbMFC = "https://system.indianbluebook.com/api/";
            this.IbbAccessToken = "https://system.indianbluebook.com/api/get_access_token";
            ibb_uname = "mfc@ibb.com";
            ibb_password = "LVUM2gHfiDrJ";
            //dHk69ffu7ebP

            //Procurement
            //v2
            /*addproc = "https://newadmin.emfcwl.com/mfc_apis/v2/api/dealer/procurement/";
            proclist = "https://newadmin.emfcwl.com/mfc_apis/v2/api/dealer/";
            leadstatus = "https://newadmin.emfcwl.com/mfc_apis/v2/api/dealer/lead/";*/

            //Procurement
            addproc = "https://newadmin.emfcwl.com/mfc_apis/preprod/rdmr3/api/dealer/procurement/";
            proclist = "https://newadmin.emfcwl.com/mfc_apis/preprod/rdmr3/api/dealer/";
            leadstatus = "https://newadmin.emfcwl.com/mfc_apis/preprod/rdmr3/api/dealer/lead/";


            dasborad = "https://newadmin.emfcwl.com/mfc_apis/preprod/rdmr3/api/";

            String mainURL = "https://newadmin.emfcwl.com/mfc_apis/preprod/rdmr3/api/";

            dealerParkAndSellDeleteURL = mainURL + "dealer/parkandsellstock/delete/";

            //For PayU Payment
            merchantkey = "UH5Vlw";
            salt = "zj1nZIE5";
            paymentbaseurl = "https://secure.payu.in";

            imageBucketName="mfc-offline-payment";
            videoBucketName="mfcwl-vehicle-videos-live-1";
            lprBucketName = "lpr-live";

            // RDM Event URL

            // rdmEventBaseURL="https://newadmin.emfcwl.com/mfc_apis/preprod/rdmr3/api/franchise/event/";
            rdmEventBaseURL="https://newadmin.emfcwl.com/mfc_apis/v3/api/franchise/event/";

        }

    }

    public String getBaseURL() {
        return baseURL;
    }

    public String getLoginUrl() {
        return loginUrl;
    }

    public void setLoginUrl(String loginUrl) {
        Global_Urls.loginUrl = loginUrl;
    }

    public String getBaseWebURL() {
        return baseWebURL;
    }

    public String getCityUrl() {
        return cityUrl;
    }

    public void setCityUrl(String cityUrl) {
        this.cityUrl = cityUrl;
    }

    //LeadAPI

    public String getLeadAccessToken() {
        return leadAccessToken;
    }

    public void setLeadAccessToken(String leadAccessToken) {
        this.leadAccessToken = leadAccessToken;
    }

    public String getLeadBaseUrl() {
        return leadBaseUrl;
    }

    public void setLeadBaseUrl(String leadBaseUrl) {
        this.leadBaseUrl = leadBaseUrl;
    }

    public String getMerchantkey() {
        return merchantkey;
    }

    public void setMerchantkey(String merchantkey) {
        this.merchantkey = merchantkey;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getPaymentbaseurl() {
        return paymentbaseurl;
    }

    public void setPaymentbaseurl(String paymentbaseurl) {
        this.paymentbaseurl = paymentbaseurl;
    }

    // For PayU Payment
    private String merchantkey;
    private String salt;
    private String paymentbaseurl;

}
