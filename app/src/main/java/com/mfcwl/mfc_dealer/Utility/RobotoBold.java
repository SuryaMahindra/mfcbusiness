package com.mfcwl.mfc_dealer.Utility;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

public class RobotoBold extends AppCompatTextView {
    public RobotoBold(Context context) {
        super(context);
    }


    public RobotoBold(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RobotoBold(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/roboto_bold.ttf");
        setTypeface(tf ,Typeface.NORMAL);

    }
}
