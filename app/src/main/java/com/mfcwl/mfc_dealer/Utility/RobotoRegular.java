package com.mfcwl.mfc_dealer.Utility;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatTextView;

import androidx.annotation.Nullable;

public class RobotoRegular extends AppCompatTextView {
    public RobotoRegular(Context context) {
        super(context);
    }


    public RobotoRegular(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RobotoRegular(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

   public void init() {
       Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/roboto_regular.ttf");
       setTypeface(tf ,Typeface.NORMAL);

   }
}
