package com.mfcwl.mfc_dealer.Utility;

import android.content.Context;
import android.graphics.Typeface;

public class CustomFonts
{

    public static Typeface getRobotoBlackTF(Context context) {
        Typeface robotoBlackTF = Typeface.createFromAsset(context.getAssets(), "roboto_black_italic.ttf");
        return robotoBlackTF;
    }

    public static Typeface getRobotoBold(Context context) {
        Typeface robotoBoldTF = Typeface.createFromAsset(context.getAssets(), "roboto_bold.ttf");
        return robotoBoldTF;
    }

    public static Typeface getRobotoBoldItalicTF(Context context) {
        Typeface robotoBoldItalicTF = Typeface.createFromAsset(context.getAssets(), "roboto_bold_italic.ttf");
        return robotoBoldItalicTF;
    }

    public static Typeface getRobotoRegularTF(Context context) {
        Typeface robotoRegularTF = Typeface.createFromAsset(context.getAssets(), "roboto_regular.ttf");
        return robotoRegularTF;
    }


    public static Typeface getRobotoMedium(Context context)
    {
        Typeface robotoMediumTF = Typeface.createFromAsset(context.getAssets(), "roboto_medium.ttf");
        return robotoMediumTF;
    }


    public static Typeface getRobotoLight(Context context)
    {
        Typeface robotoLight = Typeface.createFromAsset(context.getAssets(), "roboto_light.ttf");
        return robotoLight;
    }



}
