package com.mfcwl.mfc_dealer.Utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class MonthUtility {
    public static String[] Month = {"", "Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"};
    public static String[] monthlist = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

    public static String getMonthNumber(String month) {
        return Integer.toString(Arrays.asList(MonthUtility.monthlist).indexOf(month) + 1);
    }

    public static String[] showMonthsTillDate(int num) {
        String[] strMonth = {};

        switch (num) {
            case 0:
                strMonth = new String[]{"January"};
                break;
            case 1:
                strMonth = new String[]{"January", "February"};
                break;
            case 2:
                strMonth = new String[]{"January", "February", "March"};
                break;
            case 3:
                strMonth = new String[]{"January", "February", "March", "April"};
                break;
            case 4:
                strMonth = new String[]{"January", "February", "March", "April", "May"};
                break;
            case 5:
                strMonth = new String[]{"January", "February", "March", "April", "May", "June"};
                break;
            case 6:
                strMonth = new String[]{"January", "February", "March", "April", "May", "June", "July"};
                break;
            case 7:
                strMonth = new String[]{"January", "February", "March", "April", "May", "June", "July", "August"};
                break;
            case 8:
                strMonth = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September"};
                break;
            case 9:
                strMonth = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October"};
                break;
            case 10:
                strMonth = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October"
                        , "November"};
                break;
            case 11:
                strMonth = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October"
                        , "November", "December"};
                break;
        }


        return strMonth;
    }

    public static String getCurrentDate() {
        DateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        return simpleDateFormat.format(date);
    }

}

