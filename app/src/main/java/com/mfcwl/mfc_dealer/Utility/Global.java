package com.mfcwl.mfc_dealer.Utility;

public class Global {

    public static Global_Urls global_urls = new Global_Urls("Production");  // Production,Staging,PreProduction

    public static String commonBaseURL = global_urls.getBaseURL();

    public static String imageBucket = global_urls.getImageBucketName();
    public static String videoBucket = global_urls.getVideoBucketName();
    public static String lprBucket = global_urls.getLprBucketName();

    public static String loginURL = global_urls.getLoginUrl();

    public static String cityUrl = global_urls.getCityUrl();

    public static String mainURL = global_urls.getMainUrl();

    public static String stock_dealerURL = global_urls.getStock_dealer();
    public static String addstockURL = global_urls.getAddstock();
    public static String webleads = global_urls.getWedleads();

    //Lead API Micro Services
    public static String leadAccessToken = global_urls.getLeadAccessToken();
    public static String leadeadBaseUrl = global_urls.getLeadBaseUrl();

    // IBB
    public static String IbbAccessToken = global_urls.getIbbAccessToken();
    public static String ibbMFCURL = global_urls.getIbbMFC();

    //Procurement
    public static String addProcurement = global_urls.getAddproc();
    public static String procList = global_urls.getProclist();

    public static String Leadstatus = global_urls.getLeadstatus();
    public static String dasboard = global_urls.getDasborad();

    // For PayU Paymnet
    public static final String merchantkey = global_urls.getMerchantkey();
    public static final String salt = global_urls.getSalt();
    public static final String paymentbaseurl = global_urls.getPaymentbaseurl();

    public static final String parkAndSellDelete = global_urls.getDealerParkAndSellDeleteURL();

}
