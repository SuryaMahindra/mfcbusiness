package com.mfcwl.mfc_dealer.Utility;

public class GAConstants
{
    public static String LOGIN_DETAILS = "AM_Login_Android";
    public static String LOGOUT_DETAILS="AM_Logout_Android";
    public static String AM_DASHBOARD="AM_Dashboard_Android";
    public static String AM_DEALER_LISTING="AM_Dealer_Listing_Android";
    public static String AM_DEALER_ID_DEALER_DETAILS="AM_DealerID_DealerDetails_Android";
    public static String AM_DEALER_ID_STOCK_LISTING="AM_Dealer ID_Stock_Listing_Android";
    public static String AM_DEALER_ID_SALES_LEADS="AM_DealerID_Salesleads_Android";
    public static String AM_DEALER_ID_PROCUREMENT_LEAD="AM_DealerID_Procurement_Lead_Android";
    public static String AM_DEALER_ID_SALES="AM_DealerID_Sales_Android";
    public static String AM_RDR_SUMMARY="AM_RDR_Summary_Android";
    public static String AM_DEALER_ID_ACTION_ITEM="AM_DealerID_Action_Item_Android";
    public static String AM_RDR_ID_START="AM_RDRID_Start_Android";
    public static String AM_RDR_ID_END="AM_RDRID_End_Android";
    public static String AM_RDR_ID_MEETING_LINK="AM_RDRID_Meeting_Link_Android";
    public static String AM_NOTIFICATION_RECEIVED="Notification_Received_Android";
    public static String AM_NOTIFICATION_OPENED="Notification_Opened_Android";

}
