package com.mfcwl.mfc_dealer.Utility;

public class AppConstants {

    public static final String ASM_LEAD_SORT = "asm_sort_lead";
    public static final String POSTED_OTL = "posted_date_old_to_latest";
    public static final String POSTED_LTO ="posted_date_latest_to_oldest" ;
    public static final String FOLLOW_OTL = "follow_date_oldest_to_latest";
    public static final String FOLLOW_LTO = "follow_date_latest_to_oldest";
    public static final String TODAY ="TODAY" ;
    public static final String LAST_SEVEN_DAYS = "LAST_SEVEN_DAYS";
    public static final String LAST_FIFTEEN_DAYS = "LAST_FIFTEEN_DAYS";
    public static final String LAST_THIRTY_DAYS ="LAST_THIRTY_DAYS" ;
    public static final String POSTED_DATE ="POSTED_DATE";
    public static final String FOLLOW_DATE ="FOLLOW_DATE";
    public static final String LEAD_FILTER_CATEGORY = "field_category";
    public static final String LEAD_FILTER_CATEGORY_VALUE = "field_category_value";
    public static final String LEAD_STATUS_COUNT = "lead_status_count";
    public static final String LEAD_FILTER_CUSTOM_DATE = "lead_filter_custom_date";
    public static final String LEAD_STATUS ="LEAD STAUS" ;

    public static String LOGINTYPE_DEALER = "dealer";
    public static String LOGINTYPE_DEALERCRE = "dealer cre";
    public static String LOGINTYPE_SLAES = "sales";
    public static String LOGINTYPE_PROCUREMENT = "procurement";
    public static String DATE_KEY = "DateKey";
    public static String MEETING_TYPE = "meetingtype";
    public static String RDR_STATUS = "rdrstatus";
    public static String IS_OP = "isop";
    public static String DEALER_CODE = "dealercode";
    public static String DEALER_NAME = "dealername";
    public static String MEETING_LINK = "meetinglink";
    public static String TIME_ZONE = "India Standard Time";
    public static String WEB_LEADS="webLeads";
    public static String PRIVATE_LEADS="privateLeads";

    public static String LEAD_START_DATE="leadStartDate";
    public static String LEAD_END_DATE="leadEndDate";




}
