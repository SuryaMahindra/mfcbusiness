package com.mfcwl.mfc_dealer.Utility;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.archit.calendardaterangepicker.customviews.DateRangeCalendarView;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.SalesStocks.Instances.SalesCalendarFilterDialog;
import com.mfcwl.mfc_dealer.SalesStocks.SalesInterface.SalesFilterSelected;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class EventDialog extends Dialog implements View.OnClickListener {


    Activity activity;
    private DateRangeCalendarView mCalendar;
    private String startDateString = "", endDateString = "";
    private String dateType;
    private Button mYes, mCancel;
    private SalesFilterSelected mFilterSelected;
    private String TAG = SalesCalendarFilterDialog.class.getSimpleName();
    public static EventDialog mFilterCon;
    private Date startDt,endDt;
    Context mContext;


    public EventDialog(Activity activity, View.OnClickListener onClickListener, String dateType, SalesFilterSelected salesFilterSelected) {
        super(activity);
        this.activity = activity;
        this.dateType = dateType;
        startDateString = "";
        endDateString = "";
        mFilterSelected = salesFilterSelected;
        mFilterCon = EventDialog.this;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_date_picker_dialog);
        Log.i(TAG, "onCreate: ");
        mContext = getContext();
        startDateString = "";
        endDateString = "";
        initView();
    }

    private void initView() {

        mCalendar = (DateRangeCalendarView) findViewById(R.id.calendar);
        mCalendar.setCalendarListener(mListener);
        mYes = (Button) findViewById(R.id.btnDone);
        mCancel = (Button) findViewById(R.id.btnCancel);
        mYes.setOnClickListener(this);
        mCancel.setOnClickListener(this);
    }

    private DateRangeCalendarView.CalendarListener mListener = new DateRangeCalendarView.CalendarListener() {

        @Override
        public void onFirstDateSelected(Calendar startDate) {
            Date dateStart = new Date(startDate.getTimeInMillis());
            startDt=dateStart;
            endDt=startDt;
            SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
            if (validDate(dateStart))
            {startDateString = dateformat.format(dateStart);
            endDateString=startDateString;
            mYes.setBackground(activity.getResources().getDrawable(R.drawable.done_blue_button));
            }
            else {
                Toast.makeText(activity, "Please select Date from today", Toast.LENGTH_SHORT).show();
                mYes.setBackground(activity.getResources().getDrawable(R.drawable.event_button_bg));
            }
        }

        @Override
        public void onDateRangeSelected(Calendar startDate, Calendar endDate) {
            Date dateStart = new Date(startDate.getTimeInMillis());
            Date dateEnd = new Date(endDate.getTimeInMillis());
            startDt=dateStart;
            endDt=dateEnd;
            if(validDate(dateStart,dateEnd))
            {
                SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
                startDateString = dateformat.format(dateStart);
                endDateString = dateformat.format(dateEnd);
                mYes.setBackground(activity.getResources().getDrawable(R.drawable.done_blue_button));

                Log.e("DateRange ", "dateStart " + dateformat.format(dateStart) + " dateEnd " + dateformat.format(dateEnd));
            }
            else
            {
                Toast.makeText(activity,"Please valid range of dates",Toast.LENGTH_SHORT);
                mYes.setBackground(activity.getResources().getDrawable(R.drawable.event_button_bg));

            }

        }
    };

    private boolean validDate(Date givenDate) {

        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        Date today = c.getTime();
        if (givenDate.before(today))
            return false;
        else
            return true;
    }

    private boolean validDate(Date givenStartDate,Date givenEndDate) {

        if (validDate(givenStartDate) && (givenStartDate.equals(givenEndDate) || givenEndDate.after(givenStartDate)))
            return true;
        else
            return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnDone:
                if(!startDateString.equals(""))
                {
                if(startDt.equals(endDt))
                {
                    if(validDate(startDt))
                    {
                        mFilterSelected.onSalesFilterSelection(startDateString, endDateString, dateType);
                        dismiss();
                    }
                    else
                    {
                        showDialog();
                    }
                }
                else{
                    if(validDate(startDt,endDt))
                    {
                        mFilterSelected.onSalesFilterSelection(startDateString, endDateString, dateType);
                        dismiss();
                    }
                    else
                    {
                        showDialog();
                    }
                }}
                else
                {
                    showDialog();
                }
                break;


            case R.id.btnCancel:
                dismiss();
                break;

            default:
                break;
        }
    }

    private void showDialog()
    {
        Toast.makeText(activity,"Please select valid Date",Toast.LENGTH_SHORT).show();
    }
}
