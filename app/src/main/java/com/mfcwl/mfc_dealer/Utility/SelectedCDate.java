package com.mfcwl.mfc_dealer.Utility;

public class SelectedCDate
{
    public SelectedCDate(String startDate, String endDate)
    {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    private String startDate="", endDate="";

}
