package com.mfcwl.mfc_dealer.Utility;

import com.mfcwl.mfc_dealer.Activity.Leads.FilterInstance;
import com.mfcwl.mfc_dealer.Activity.Leads.NotificationInstance;

import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chfollowlast7days;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chfollowtmrw;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chfollowtoday;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chfollowyester;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chpost15days;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chpost7days;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chposttoday;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.chpostyester;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.followflag;
import static com.mfcwl.mfc_dealer.Fragment.Leads.LeadFilterFragment.postflag;

public class LeadsUtils {

    public static void ClearPostDataFilter() {
        FilterInstance.getInstance().setPtoday("");
        FilterInstance.getInstance().setPyesterday("");
        FilterInstance.getInstance().setPlast7days("");
        FilterInstance.getInstance().setPlast15days("");
        FilterInstance.getInstance().setPchoosecustomdatefrom("");
        FilterInstance.getInstance().setPchoosecustomdateto("");
    }

    public static void ClearFollowDataFilter() {
        FilterInstance.getInstance().setFtomorrow("");
        FilterInstance.getInstance().setFtoday("");
        FilterInstance.getInstance().setFyesterday("");
        FilterInstance.getInstance().setFlast7days("");
        FilterInstance.getInstance().setFchoosecustomdatefrom("");
        FilterInstance.getInstance().setFchoosecustomdateto("");
        NotificationInstance.getInstance().setTodayfollowdate("");
    }
    
    public static void ClearLeadStatusFilter() {
        FilterInstance.getInstance().setLsopen("");
        FilterInstance.getInstance().setLshot("");
        FilterInstance.getInstance().setLswarm("");
        FilterInstance.getInstance().setLscold("");
        FilterInstance.getInstance().setLslost("");
        FilterInstance.getInstance().setLssold("");
    }

    public static void ClearPostCheckBoxFilter_vvv() {
        if (postflag == 1) {
            chpostyester.setChecked(false);
            chpost7days.setChecked(false);
            chpost15days.setChecked(false);
        } else if (postflag == 2) {
            chposttoday.setChecked(false);
            chpost7days.setChecked(false);
            chpost15days.setChecked(false);
        } else if (postflag == 3) {
            chposttoday.setChecked(false);
            chpostyester.setChecked(false);
            chpost15days.setChecked(false);
        } else if (postflag == 4) {
            chposttoday.setChecked(false);
            chpostyester.setChecked(false);
            chpost7days.setChecked(false);
        } else if (postflag == 5) {
            chposttoday.setChecked(false);
            chpostyester.setChecked(false);
            chpost7days.setChecked(false);
            chpost15days.setChecked(false);
        }
    }

    public static void ClearPostCheckBoxFilter() {
        if (postflag == 1) {
            chposttoday.setChecked(true);
            chpostyester.setChecked(false);
            chpost7days.setChecked(false);
            chpost15days.setChecked(false);
        } else if (postflag == 2) {
            chposttoday.setChecked(false);
            chpostyester.setChecked(true);
            chpost7days.setChecked(false);
            chpost15days.setChecked(false);
        } else if (postflag == 3) {
            chposttoday.setChecked(false);
            chpostyester.setChecked(false);
            chpost7days.setChecked(true);
            chpost15days.setChecked(false);
        } else if (postflag == 4) {
            chposttoday.setChecked(false);
            chpostyester.setChecked(false);
            chpost7days.setChecked(false);
            chpost15days.setChecked(true);
        } else if (postflag == 5) {
            chposttoday.setChecked(false);
            chpostyester.setChecked(false);
            chpost7days.setChecked(false);
            chpost15days.setChecked(false);
        }
    }

    public static void ClearFollowCheckBoxFilter_vvv() {
        if (followflag == 1) {
            chfollowtoday.setChecked(false);
            chfollowyester.setChecked(false);
            chfollowlast7days.setChecked(false);
        } else if (followflag == 2) {
            chfollowtmrw.setChecked(false);
            chfollowyester.setChecked(false);
            chfollowlast7days.setChecked(false);
        } else if (followflag == 3) {
            chfollowtmrw.setChecked(false);
            chfollowtoday.setChecked(false);
            chfollowlast7days.setChecked(false);
        } else if (followflag == 4) {
            chfollowtmrw.setChecked(false);
            chfollowtoday.setChecked(false);
            chfollowyester.setChecked(false);
        } else if (followflag == 5) {
            chfollowtmrw.setChecked(false);
            chfollowtoday.setChecked(false);
            chfollowyester.setChecked(false);
            chfollowlast7days.setChecked(false);
        }
    }

    public static void ClearFollowCheckBoxFilter() {
        if (followflag == 1) {
            chfollowtmrw.setChecked(true);
            chfollowtoday.setChecked(false);
            chfollowyester.setChecked(false);
            chfollowlast7days.setChecked(false);
        } else if (followflag == 2) {
            chfollowtmrw.setChecked(false);
            chfollowtoday.setChecked(true);
            chfollowyester.setChecked(false);
            chfollowlast7days.setChecked(false);
        } else if (followflag == 3) {
            chfollowtmrw.setChecked(false);
            chfollowtoday.setChecked(false);
            chfollowyester.setChecked(true);
            chfollowlast7days.setChecked(false);
        } else if (followflag == 4) {
            chfollowtmrw.setChecked(false);
            chfollowtoday.setChecked(false);
            chfollowyester.setChecked(false);
            chfollowlast7days.setChecked(true);
        } else if (followflag == 5) {
            chfollowtmrw.setChecked(false);
            chfollowtoday.setChecked(false);
            chfollowyester.setChecked(false);
            chfollowlast7days.setChecked(false);
        }
    }

}
