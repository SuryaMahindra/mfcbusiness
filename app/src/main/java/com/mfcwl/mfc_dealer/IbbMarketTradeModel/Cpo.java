package com.mfcwl.mfc_dealer.IbbMarketTradeModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Cpo implements Serializable {
    @SerializedName("fairprice")
    @Expose
    private int fairprice;
    @SerializedName("marketprice")
    @Expose
    private int marketprice;
    @SerializedName("bestprice")
    @Expose
    private int bestprice;

    public int getFairprice() {
        return fairprice;
    }

    public void setFairprice(int fairprice) {
        this.fairprice = fairprice;
    }

    public int getMarketprice() {
        return marketprice;
    }

    public void setMarketprice(int marketprice) {
        this.marketprice = marketprice;
    }

    public int getBestprice() {
        return bestprice;
    }

    public void setBestprice(int bestprice) {
        this.bestprice = bestprice;
    }
}
