package com.mfcwl.mfc_dealer.IbbMarketTradeModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseIbbTradeData {
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("year")
    @Expose
    private List<String> year = null;
    @SerializedName("month")
    @Expose
    private List<String> month = null;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("make")
    @Expose
    private List<String> make = null;
    @SerializedName("model")
    @Expose
    private List<String> model = null;
    @SerializedName("variant")
    @Expose
    private List<String> variant = null;
    @SerializedName("color")
    @Expose
    private List<String> color = null;

    @SerializedName("tradein")
    @Expose
    private Tradein tradein;
    @SerializedName("private")
    @Expose
    private Private _private;
    @SerializedName("retail")
    @Expose
    private Retail retail;
    @SerializedName("cpo")
    @Expose
    private Cpo cpo;

    public Tradein getTradein() {
        return tradein;
    }

    public void setTradein(Tradein tradein) {
        this.tradein = tradein;
    }

    public Private get_private() {
        return _private;
    }

    public void set_private(Private _private) {
        this._private = _private;
    }

    public Retail getRetail() {
        return retail;
    }

    public void setRetail(Retail retail) {
        this.retail = retail;
    }

    public Cpo getCpo() {
        return cpo;
    }

    public void setCpo(Cpo cpo) {
        this.cpo = cpo;
    }

    public List<Integer> getComPriceID() {
        return comPriceID;
    }

    public void setComPriceID(List<Integer> comPriceID) {
        this.comPriceID = comPriceID;
    }

    @SerializedName("ComPriceID")
    @Expose
    private List<Integer> comPriceID = null;

    public List<String> getColor() {
        return color;
    }

    public void setColor(List<String> color) {
        this.color = color;
    }

    public List<String> getOwner() {
        return owner;
    }

    public void setOwner(List<String> owner) {
        this.owner = owner;
    }

    @SerializedName("city")
    @Expose

    private List<String> city = null;
    @SerializedName("owner")
    @Expose
    private List<String> owner = null;
    public List<String> getCity() {
        return city;
    }

    public void setCity(List<String> city) {
        this.city = city;
    }

    public List<String> getVariant() {
        return variant;
    }

    public void setVariant(List<String> variant) {
        this.variant = variant;
    }

    public List<String> getModel() {
        return model;
    }

    public void setModel(List<String> model) {
        this.model = model;
    }

    public List<String> getMake() {
        return make;
    }

    public void setMake(List<String> make) {
        this.make = make;
    }

    public List<String> getMonth() {
        return month;
    }

    public void setMonth(List<String> month) {
        this.month = month;
    }




    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<String> getYear() {
        return year;
    }

    public void setYear(List<String> year) {
        this.year = year;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
