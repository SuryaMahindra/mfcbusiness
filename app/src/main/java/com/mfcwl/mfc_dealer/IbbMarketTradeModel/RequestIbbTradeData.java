package com.mfcwl.mfc_dealer.IbbMarketTradeModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class RequestIbbTradeData {
    @SerializedName("for")
    @Expose
    private String _for;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("month")
    @Expose
    private String month;
    @SerializedName("make")
    @Expose
    private String make;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("variant")
    @Expose
    private String variant;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("owner")
    @Expose
    private String owner;
    @SerializedName("kilometer")
    @Expose
    private String kilometer;
    @SerializedName("tag")
    @Expose
    private String tag;
    @SerializedName("pricefor")
    @Expose
    private int pricefor;
    @SerializedName("partuserid")
    @Expose
    private String partuserid;
    @SerializedName("access_token")
    @Expose
    private String accessToken;

    public String getFor() {
        return _for;
    }

    public void setFor(String _for) {
        this._for = _for;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getKilometer() {
        return kilometer;
    }

    public void setKilometer(String kilometer) {
        this.kilometer = kilometer;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getPricefor() {
        return pricefor;
    }

    public void setPricefor(int pricefor) {
        this.pricefor = pricefor;
    }

    public String getPartuserid() {
        return partuserid;
    }

    public void setPartuserid(String partuserid) {
        this.partuserid = partuserid;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

}
