package com.mfcwl.mfc_dealer.ASMAdapter.ASMReportAdapter;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.WeekReportSubmitModel;
import com.mfcwl.mfc_dealer.ASMModel.amsCreateRes;
import com.mfcwl.mfc_dealer.ASMReportsServices.WRSubmitService;
import com.mfcwl.mfc_dealer.Activity.ASM_Reports.CreateReportActivity;
import com.mfcwl.mfc_dealer.Model.weekResData;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Utility.Helper.dealerNameList;

public class WeeklyReportAdapter extends RecyclerView.Adapter<WeeklyReportAdapter.ViewHolder> {

    private ArrayList<weekResData> lists;
    public String TAG = getClass().getSimpleName();
    Activity activity;

    public WeeklyReportAdapter(Activity act, ArrayList<weekResData> lists) {
        this.activity = act;
        this.lists = lists;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = (View) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_weeklyreport_asm, viewGroup, false);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int postion) {
        weekResData weekResData = lists.get(postion);

        viewHolder.companyNametv.setText(weekResData.getDealerName());
        viewHolder.daytv.setText(weekResData.getDay());
        viewHolder.datetv.setText(weekResData.getDate());
        viewHolder.timetv.setText(weekResData.getHour());


        ((Activity) activity).runOnUiThread(new Runnable() {
            public void run() {
                //Code goes here

        for (Map.Entry<String, String> entry : dealerNameList.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();

           // System.out.println("value -out = " + value + ", week = -out" + weekResData.getToDate());
            if(value.equalsIgnoreCase(weekResData.getToDate())) {
              //  System.out.println("value = -in" + value + ", week = in" + weekResData.getToDate());
                viewHolder.week_name.setVisibility(View.VISIBLE);
                viewHolder.week_name.setText(weekResData.getFormDate() +" To "+value);
                dealerNameList.remove(entry.getKey());
                return;

            }else{
                viewHolder.week_name.setVisibility(View.GONE);
            }

        }

            }
        });


        viewHolder.editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                week_report_edit_data(postion ,weekResData.getDealerCode());

            }
        });
        viewHolder.submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < lists.size(); i++) {

                    if (lists.get(i).getDealerCode() == weekResData.getDealerCode()) {

                        week_report_delete_data(i, weekResData.getDealerCode(), weekResData.getTempdealer_code());

                       /* lists.remove(i);
                        notifyItemRemoved(i);*/

                    }
                }


            }
        });

        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < lists.size(); i++) {

                    if (lists.get(i).getDealerCode() == weekResData.getDealerCode()) {

                        deletePopup(i, weekResData.getDealerCode(), weekResData.getTempdealer_code(),weekResData.getCreatedforKey());
                    }
                }





            }
        });
    }

    private void deletePopup(int i, String dealerCode, String tempdealer_code,String refKey) {

        // custom dialog
        Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.common_cus_dialog);
        //dialog.setTitle("Custom Dialog");

        TextView Message, Message2;
        Button cancel, Confirm;
        Message = dialog.findViewById(R.id.Message);
        Message2 = dialog.findViewById(R.id.Message2);
        cancel = dialog.findViewById(R.id.cancel);
        Confirm = dialog.findViewById(R.id.Confirm);
        Message.setText("Are you sure you want to ");
        Message2.setText("Delete ?");


        Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                week_report_delete_data_Current(i, dealerCode, tempdealer_code);
                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();


    }

    @Override
    public int getItemCount() {
        if (lists != null) {
            return lists.size();
        } else {
            return 0;
        }
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        public final View view;
        public final TextView companyNametv, daytv, datetv, timetv,week_name;

        public final Button editBtn, submitBtn,delete;


        public ViewHolder(View view) {
            super(view);
            this.view = view;
            companyNametv = view.findViewById(R.id.wr_cname_tv);
            daytv = view.findViewById(R.id.wr_day_tv);
            datetv = view.findViewById(R.id.wr_date_tv);
            timetv = view.findViewById(R.id.wr_time_tv);
            editBtn = view.findViewById(R.id.wr_edit_btn);
            submitBtn = view.findViewById(R.id.wr_submit_btn);
            delete = view.findViewById(R.id.delete);
            week_name = view.findViewById(R.id.week_name);
        }

    }


    private void week_report_edit_data(int postions, String code) {


        String commonJson = CommonMethods.getstringvaluefromkey(activity, "jsonweek");
        JSONArray new_array = new JSONArray();
        JSONObject jsonObjMain = new JSONObject();

        try {

            if (!commonJson.equalsIgnoreCase("")) {
                jsonObjMain = new JSONObject(commonJson);

                new_array = jsonObjMain.getJSONArray("data");
                JSONObject editJson = new JSONObject();
                editJson = new_array.getJSONObject(postions);

                Log.i(TAG, "week_report_edit_data: "+editJson.toString());

                Intent intent = new Intent(activity, CreateReportActivity.class);


                intent.putExtra("dealername", "");
                intent.putExtra("dealer_code",code);
                intent.putExtra("tempdealer_code","");

                intent.putExtra("editJson", editJson.toString());

                activity.startActivity(intent);
                activity.finish();

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    private void week_report_delete_data(int postions, String code, String dealerCode) {


        String commonJson = CommonMethods.getstringvaluefromkey(activity, "jsonweek");
        JSONArray new_array = new JSONArray();
        JSONObject jsonObjMain = new JSONObject();

        try {

            if (!commonJson.equalsIgnoreCase("")) {
                jsonObjMain = new JSONObject(commonJson);

                new_array = jsonObjMain.getJSONArray("data");

                for (int i = 0; i < new_array.length(); i++) {

                    JSONObject updateJson = new JSONObject();
                    updateJson = new_array.getJSONObject(i);

                    if (updateJson.get("tempdealer_code").equals(dealerCode)) {

                        Log.i(TAG, "week_report_delete_data: " + updateJson.toString());

                        if (ValitationCheck(updateJson)) {

                           /* lists.remove(postions);
                            notifyItemRemoved(postions);

                            new_array.remove(i);*/

                            if (CommonMethods.isInternetWorking(activity)) {
                                submitReportDetails(updateJson, postions, new_array, i,jsonObjMain);
                            } else {
                                CommonMethods.alertMessage(activity, GlobalText.CHECK_NETWORK_CONNECTION);
                            }


                        } else {
                            Toast.makeText(activity, "some field data missing.", Toast.LENGTH_LONG).show();
                            return;
                        }

                    } else {
                    }

                }

            }

            //jsonObjMain.put("data", new_array);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*String saveJsonNew = jsonObjMain.toString(); //http request

        CommonMethods.setvalueAgainstKey(activity, "jsonweek", saveJsonNew);

        Log.d("saveJsonNew", "saveJsonNew= " + saveJsonNew.toString());*/

    }


    private void week_report_delete_data_Current(int postions, String code, String dealerCode) {


        String commonJson = CommonMethods.getstringvaluefromkey(activity, "jsonweek");
        JSONArray new_array = new JSONArray();
        JSONObject jsonObjMain = new JSONObject();

        try {

            if (!commonJson.equalsIgnoreCase("")) {
                jsonObjMain = new JSONObject(commonJson);

                new_array = jsonObjMain.getJSONArray("data");

                for (int i = 0; i < new_array.length(); i++) {

                    JSONObject updateJson = new JSONObject();
                    updateJson = new_array.getJSONObject(i);

                    if (updateJson.get("tempdealer_code").equals(dealerCode)) {

                        Log.i(TAG, "week_report_delete_data: " + updateJson.toString());

                String key =  updateJson.get("createdforKey").toString();

                        lists.remove(postions);
                        notifyItemRemoved(postions);
                        new_array.remove(i);
                        CommonMethods.setvalueAgainstKey(activity, key, "");


                    } else {
                    }
                }
            }

            jsonObjMain.put("data", new_array);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String saveJsonNew = jsonObjMain.toString(); //http request

        CommonMethods.setvalueAgainstKey(activity, "jsonweek", saveJsonNew);
        CommonMethods.setvalueAgainstKey(activity, "jsonweek", saveJsonNew);

        Log.d("saveJsonNew", "saveJsonNew= " + saveJsonNew.toString());

    }


    private Boolean ValitationCheck(JSONObject updateJson) {


        Iterator<String> keys = updateJson.keys();
        for (int i = 0; i < updateJson.length(); i++) {
            try {
                if (updateJson.get(keys.next()).equals("")) {



                    return false;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    private void submitReportDetails(JSONObject updateJson, int postions, JSONArray new_array, int i,JSONObject jsonObjMain) {

        SpinnerManager.showSpinner(activity);

        WeekReportSubmitModel data = new WeekReportSubmitModel();
        Gson gson = new Gson();
        data = gson.fromJson(updateJson.toString(), WeekReportSubmitModel.class);

        data.setTempdealer_code(null);
        data.setDate(null);
        data.setDay(null);
        data.setHour(null);

        ArrayList<WeekReportSubmitModel> rq = new ArrayList<>();

        rq.add(data);

        WRSubmitService wrSubmitService = new WRSubmitService();
        wrSubmitService.pushMultipleData(rq, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {

                Response<amsCreateRes> mRes = (Response <amsCreateRes>) obj;
                amsCreateRes mData = mRes.body();

                if(mData.getStatus().equalsIgnoreCase("SUCCESS")) {
                    Toast.makeText(activity, "Successfully Submited", Toast.LENGTH_LONG).show();

                    lists.remove(postions);
                    notifyItemRemoved(postions);
                    new_array.remove(i);

                    // remove json
                    try {
                        jsonObjMain.put("data", new_array);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String saveJsonNew = jsonObjMain.toString(); //http request
                    CommonMethods.setvalueAgainstKey(activity, "jsonweek", saveJsonNew);

                    Log.d("saveJsonNew", "saveJsonNew= " + saveJsonNew.toString());

                }else{
                    Toast.makeText(activity, mData.getMessage().toString(), Toast.LENGTH_LONG).show();
                }

                SpinnerManager.hideSpinner(activity);
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(activity);
                Toast.makeText(activity, "Something went wrong Please check your code", Toast.LENGTH_LONG).show();
            }
        });

    }


}
