package com.mfcwl.mfc_dealer.ASMAdapter.ASMReportAdapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.SRLastTtyDaysModel;

import com.mfcwl.mfc_dealer.R;

import java.util.ArrayList;

public class SRLastTtyDaysAdapter extends RecyclerView.Adapter<SRLastTtyDaysAdapter.ViewHolder> {

    private ArrayList<SRLastTtyDaysModel> lists;

    public SRLastTtyDaysAdapter(ArrayList<SRLastTtyDaysModel> lists)
    {
        this.lists=lists;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View v = (View) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_submittedreport_lasttydays, viewGroup, false);
        return new ViewHolder(v);


    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i)
    {
        SRLastTtyDaysModel srLastTtyDaysModel=lists.get(i);

        viewHolder.companyNametv.setText(srLastTtyDaysModel.getCompanyName());
        viewHolder.daytv.setText(srLastTtyDaysModel.getDaytv());
        viewHolder.datetv.setText(srLastTtyDaysModel.getDatetv());
        viewHolder.timetv.setText(srLastTtyDaysModel.getTimetv());
        viewHolder.viewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        viewHolder.emailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override
    public int getItemCount() {

        if (lists != null) {
            return lists.size();
        } else {
            return 0;
        }
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        public final View view;
        public TextView companyNametv,daytv,datetv,timetv;

        public  Button viewBtn,emailBtn;




        public ViewHolder(View view) {
            super(view);
            this.view = view;
            companyNametv = view.findViewById(R.id.sr_cname_tv);
            daytv = view.findViewById(R.id.sr_day_tv);
            datetv = view.findViewById(R.id.sr_date_tv);
            timetv = view.findViewById(R.id.sr_time_tv);
            viewBtn = view.findViewById(R.id.sr_view_btn);
            emailBtn = view.findViewById(R.id.sr_email_btn);
        }

    }



}
