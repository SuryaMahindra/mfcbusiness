package com.mfcwl.mfc_dealer.ASMAdapter.ASMReportAdapter;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.WeekReportSubmitModel;
import com.mfcwl.mfc_dealer.ASMReportsServices.DvReport;
import com.mfcwl.mfc_dealer.Activity.ASM_Reports.SubmittedWRExpanded;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class SubmittedRptAdapterWR extends RecyclerView.Adapter<SubmittedRptAdapterWR.ViewHolder> {

    private ArrayList<WeekReportSubmitModel> lists;
    ProgressBar pgs;
    public int position;
    private Context mContext;
    public String TAG=getClass().getSimpleName();
    Calendar myCalendar;

    private static Context c;

    public SubmittedRptAdapterWR(final Context mContext, ArrayList<WeekReportSubmitModel> lists, final Context c) {
        this.lists = lists;
        this.mContext = mContext;
        this.c = c;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = (View) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_submittedreport, viewGroup, false);
        return new ViewHolder(v);


    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        WeekReportSubmitModel submittedReportModel = lists.get(i);

       // viewHolder.companyNametv.setText(submittedReportModel.getDealerName());
        viewHolder.wr_cname_tv.setText(submittedReportModel.getDealerName());


        try {

            String a = submittedReportModel.getCreatedOn();

            if(a!=null && !a.isEmpty()) {

                Log.i(TAG, "onBindViewHolder: " + a);

                a = a.replace("T", " | ");
                a = a.substring(0, 18);
                String b = a.substring(0, 10);
                Log.i("datetime", "onBindViewHolder: " + b);
                String[] values = b.split("-");
                int day = Integer.parseInt(values[2]);
                int month = Integer.parseInt(values[1]);
                int year = Integer.parseInt(values[0]);

                Log.i("datetime", "onBindViewHolder: " + values[1]);
                myCalendar = Calendar.getInstance();
                myCalendar.set(year, month - 1, day - 1);

                int dayof = myCalendar.get(myCalendar.DAY_OF_WEEK);
                String dayofweek = getDayOfWEek(dayof);
                Log.i(" dayofweek", "OnSuccess: " + dayofweek);
                a = dayofweek + " | " + a;

                viewHolder.daytv.setText(a);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
//        viewHolder.datetv.setText(submittedReportModel.getDatetv());
//        viewHolder.timetv.setText(submittedReportModel.getTimetv());
        viewHolder.viewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //    ArrayList<WeekReportSubmitModel> json =new ArrayList<>();


                WeekReportSubmitModel nn = new WeekReportSubmitModel();

                //   nn =  json.get(i);

                Gson gson = new Gson();

                String json = gson.toJson(lists.get(i));


                Log.i("jsonobject1", "onClick: " + json);

                Intent intent = new Intent(mContext, SubmittedWRExpanded.class);

                intent.putExtra("json", json);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                mContext.startActivity(intent);


            }
        });
        viewHolder.emailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //pgs.setVisibility(View.VISIBLE);


                List<WeekReportSubmitModel> slist = new ArrayList<WeekReportSubmitModel>();
                slist.add(submittedReportModel);

                DvReport dvr = new DvReport();


                // custom dialog
                Dialog dialog = new Dialog(c);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setGravity(Gravity.CENTER|Gravity.CENTER);
                dialog.setContentView(R.layout.common_cus_dialog);
                TextView Message, Message2;
                Button cancel, Confirm;
                Message = dialog.findViewById(R.id.Message);
                Message2 = dialog.findViewById(R.id.Message2);
                cancel = dialog.findViewById(R.id.cancel);
                Confirm = dialog.findViewById(R.id.Confirm);
                Message.setText(GlobalText.are_you_sure_to_send_mail);
                //Message2.setText("Go ahead and Submit ?");
                Confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        SpinnerManager.showSpinner(c);
                        dvr.pushEMailWR(slist, submittedReportModel.getId(), new HttpCallResponse() {
                            @Override
                            public void OnSuccess(Object obj) {
                                SpinnerManager.hideSpinner(c);
                                confiirmAlert();
                            }

                            @Override
                            public void OnFailure(Throwable mThrowable) {
                                SpinnerManager.hideSpinner(c);
                                Toast.makeText(mContext, "Failed to send Email", Toast.LENGTH_SHORT).show();
                            }
                        });

                        dialog.dismiss();
                    }
                });
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();




            }
        });


    }

    public void confiirmAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(c);

        builder.setMessage("Email sent succesfully").setTitle("success");


        builder.setMessage("Email sent Sucessfully")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();

                    }
                });
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Done");
        alert.show();
        //Toast.makeText(mContext,"workig",Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {

        if (lists != null) {
            return lists.size();
        } else {
            return 0;
        }
    }

    public int getPosition() {
        return position;
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        public final View view;
        public final TextView daytv,wr_cname_tv;//datetv,timetv;

        public final Button viewBtn, emailBtn;


        public ViewHolder(View view) {
            super(view);
            this.view = view;
           // companyNametv = view.findViewById(R.id.companyName);
            daytv = view.findViewById(R.id.daytv);
            //datetv = view.findViewById(R.id.sr_datetv);
            //timetv = view.findViewById(R.id.srtimetv);
            viewBtn = view.findViewById(R.id.srviewbtn);
            emailBtn = view.findViewById(R.id.sremailtv);
            wr_cname_tv = view.findViewById(R.id.wr_cname_tv);
        }

    }

    public static String getDayOfWEek(int a) {
        String str;
        switch (a) {
            case 1:
                str = "Mon";
                break;
            case 2:
                str = "Tue";
                break;
            case 3:
                str = "Wed";
                break;
            case 4:
                str = "Thu";
                break;
            case 5:
                str = "Fri";
                break;
            case 6:
                str = "Sat";
                break;
            case 7:
                str = "Sun";
                break;
            default:
                str = "mon";
        }
        return str;
    }


}
