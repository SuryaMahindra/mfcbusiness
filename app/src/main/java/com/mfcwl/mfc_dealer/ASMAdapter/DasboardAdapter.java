package com.mfcwl.mfc_dealer.ASMAdapter;

import android.app.Activity;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.FragmentActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.ASMModel.dasboardRes;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Model.DashboardModels;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DasboardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private Activity activity;
    private List<dasboardRes> mlivelist;
    private List<dasboardRes> mSearch;
    private Context context;
    int fav = 0;
    private DashboardModels dashboardSwtich;

    private boolean isLoadingAdded = false;

    public DasboardAdapter(Context context) {
        this.context = context;
        activity = (Activity) context;
        mlivelist = new ArrayList<>();
        //all_map.clear();
        mSearch = new ArrayList<dasboardRes>();
        mSearch.addAll(mlivelist);

        /*Helper.dealerName.clear();
        Helper.dealerCode.clear();*/


    }

    public List<dasboardRes> getmlivelist() {
        return mlivelist;
    }

    public void setmlivelist(List<dasboardRes> mlivelist) {
        this.mlivelist = mlivelist;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());


        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new DasboardAdapter.LoadingVH(v2);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.row_all_live_auction, parent, false);
        viewHolder = new DasboardAdapter.MyLiveholder(v1);

        dashboardSwtich = ViewModelProviders.of((FragmentActivity) activity).get(DashboardModels.class);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        dasboardRes data = mlivelist.get(position);
        try {

        switch (getItemViewType(position)) {
            case ITEM:


                MyLiveholder Aucholder = (MyLiveholder) holder;

                Aucholder.dealearName.setText(data.getDealerName());
                Aucholder.sectionHeading.setVisibility(View.GONE);
               /* Helper.dealerName.add(position,data.getDealerName());

                Helper.dealerCode.add(position,data.getDealerCode());*/


                if (data.getType() != null) {
                    if (data.getType().equalsIgnoreCase("4 Wheeler")) {
                        Aucholder.fwDealer_tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_f_wheeler, 0, 0, 0);
                    } else {
                        Aucholder.fwDealer_tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_t_wheeler, 0, 0, 0);
                    }

                    Aucholder.fwDealer_tv.setText("" + data.getType());
                } else {
                    Aucholder.fwDealer_tv.setText("" + "");
                }

                if (data.getDealerType() != null) {
                    Aucholder.dealer_type_tv.setText("Dealer Type : " + data.getDealerType());
                } else {
                    Aucholder.dealer_type_tv.setText("Dealer Type : " + "");
                }

                if (data.getBalance() != null) {
                    Aucholder.warranty_bal.setText("Warranty Bal: " + data.getBalance());
                } else {
                    Aucholder.warranty_bal.setText("Warranty Bal: " + "0");
                }

                if (data.getWarrantypunch() != null) {
                    Aucholder.twsr_tv.setText("T.W.S.R.(MTD): " + data.getWarrantypunch());
                } else {
                    Aucholder.twsr_tv.setText("T.W.S.R.(MTD): " + "0");
                }

                break;
            case LOADING:
//                Do nothing
                break;

        }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);


    }

    @Override
    public int getItemCount() {
        return mlivelist == null ? 0 : mlivelist.size();
    }

    @Override
    public int getItemViewType(int position) {

        /*  Log.i("position", "position==: " + position);
          Log.i("getItemCount", "getItemCount==: " + (getItemCount()-1));*/

        return (position == mlivelist.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    /*
   Helpers
   _________________________________________________________________________________________________
    */

    public void add(dasboardRes mc) {
        mlivelist.add(mc);
        notifyItemInserted(mlivelist.size() - 1);
    }

    public void addAll(List<dasboardRes> mcList) {
        for (dasboardRes mc : mcList) {
            add(mc);
        }

        mSearch.clear();
        mSearch.addAll(mlivelist);
    }


    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new dasboardRes());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = mlivelist.size() - 1;
        dasboardRes item = getItem(position);

        if (item != null) {
            mlivelist.remove(position);
            notifyItemRemoved(position);
        }
    }

    public dasboardRes getItem(int position) {


        position = position == -1 ? 0 : position;
        return mlivelist.get(position);
    }

    /**
     * Main list's content ViewHolder
     */
    protected class MyLiveholder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView dealearName,dealer_type_tv,fwDealer_tv,warranty_bal,twsr_tv;
        public TextView sectionHeading;
        public CardView card_view;

        public BottomNavigationView mNavigationView;


        public MyLiveholder(View itemView) {
            super(itemView);
            sectionHeading = itemView.findViewById(R.id.sectionHeading);
            dealearName = itemView.findViewById(R.id.dealearName);
            dealer_type_tv = itemView.findViewById(R.id.dealer_type_tv);
            fwDealer_tv = itemView.findViewById(R.id.fwDealer_tv);
            warranty_bal = itemView.findViewById(R.id.warranty_bal);
            twsr_tv = itemView.findViewById(R.id.twsr_tv);

            card_view = itemView.findViewById(R.id.card_view);
            mNavigationView = itemView.findViewById(R.id.navigation);

            dealearName.setOnClickListener(this::onClick);
            card_view.setOnClickListener(this::onClick);
        }

        @Override
        public void onClick(View view) {


            int position = getAdapterPosition();
            dasboardRes data = mlivelist.get(position);

            switch (view.getId()) {
                case R.id.card_view:

                    try {

                        MainActivity.searchClose();

                        CommonMethods.setvalueAgainstKey(activity,"status","");

                        CommonMethods.setvalueAgainstKey(activity, "dcode", data.getDealerCode().toString());

                        if (data.getWarrantypunch() != null) {
                            CommonMethods.setvalueAgainstKey(activity, "getWarrantypunch", data.getWarrantypunch().toString());
                        } else {
                            CommonMethods.setvalueAgainstKey(activity, "getWarrantypunch", "0");
                        }
                        if (data.getBalance() != null) {

                          //  String val =  BigDecimal(data.getBalance().toString()).stripTrailingZeros().toPlainString();

                            CommonMethods.setvalueAgainstKey(activity, "getBalance", String.valueOf(data.getBalance()));
                        } else {
                            CommonMethods.setvalueAgainstKey(activity, "getBalance", "0");
                        }

                        CommonMethods.setvalueAgainstKey(activity, "dealer_types", data.getType().toString());
                        CommonMethods.setvalueAgainstKey(activity,"dealerName",data.getDealerName().toString());

                        if (data.getType().equalsIgnoreCase("4 Wheeler")) {
                            MainActivity.navigation.getMenu().clear();
                            MainActivity.navigation.inflateMenu(R.menu.navigation);
                            MainActivity.navigation.setVisibility(View.VISIBLE);
                            CommonMethods.setvalueAgainstKey(activity, "dealer_code", data.getDealerCode());
                            dashboardSwtich.getdashboardSwtich().setValue("MessageCenter");
                        } else {
                            //Toast.makeText(activity, "This 2 Wheeler Dealer  ", Toast.LENGTH_LONG).show();
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    break;

                case R.id.dealearName:

                    try {

                        MainActivity.searchClose();
                        CommonMethods.setvalueAgainstKey(activity, "dcode", data.getDealerCode().toString());

                        if (data.getWarrantypunch() != null) {
                            CommonMethods.setvalueAgainstKey(activity, "getWarrantypunch", data.getWarrantypunch().toString());
                        } else {
                            CommonMethods.setvalueAgainstKey(activity, "getWarrantypunch", "0");
                        }
                        if (data.getBalance() != null) {
                            CommonMethods.setvalueAgainstKey(activity, "getBalance", String.valueOf(data.getBalance()));
                        } else {
                            CommonMethods.setvalueAgainstKey(activity, "getBalance", "0");
                        }

                        CommonMethods.setvalueAgainstKey(activity, "dealer_types", data.getType().toString());
                        CommonMethods.setvalueAgainstKey(activity,"dealerName",data.getDealerName().toString());

                        if (data.getType().equalsIgnoreCase("4 Wheeler")) {
                            MainActivity.navigation.getMenu().clear();
                            MainActivity.navigation.inflateMenu(R.menu.navigation);
                            MainActivity.navigation.setVisibility(View.VISIBLE);
                            CommonMethods.setvalueAgainstKey(activity, "dealer_code", data.getDealerCode());
                            dashboardSwtich.getdashboardSwtich().setValue("MessageCenter");
                        } else {
                            //Toast.makeText(activity, "This 2 Wheeler Dealer  ", Toast.LENGTH_LONG).show();
                        }

                    } catch (Exception e){
                        e.printStackTrace();
                    }

                    break;

            }
        }
    }

    public void remove(dasboardRes data) {
        int position = mlivelist.indexOf(data);
        if (position > -1) {
            mlivelist.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }
    protected class LoadingVH extends RecyclerView.ViewHolder {

        public LoadingVH(View itemView) {
            super(itemView);
        }

    }


    // Search Filter
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        mlivelist.clear();
        if (charText.length() == 0) {
            mlivelist.addAll(mSearch);
        } else {

            for (dasboardRes it : mSearch) {
                if (it.getDealerName().toLowerCase(Locale.getDefault()).contains(charText)
                        /*|| it.getType().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getDealerType().toLowerCase(Locale.getDefault()).contains(charText)*/) {

                    mlivelist.add(it);
                }
            }
        }
        notifyDataSetChanged();
    }


}
