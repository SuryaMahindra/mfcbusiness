package com.mfcwl.mfc_dealer.ASMAdapter.ASMReportAdapter;



import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.DVEmail;
import com.mfcwl.mfc_dealer.ASMModel.ASM_Report_Model.SubmittedReportModel;
import com.mfcwl.mfc_dealer.ASMReportsServices.DvReport;
import com.mfcwl.mfc_dealer.Activity.ASM_Reports.SubmittedDVRExpanded;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import java.util.ArrayList;
import java.util.List;

public class SubmittedRptAdapter extends RecyclerView.Adapter<SubmittedRptAdapter.ViewHolder> {

    private ArrayList<SubmittedReportModel> lists;
    ProgressBar pgs;
    public int position;
    private Context mContext;
public String TAG=getClass().getSimpleName();
    private static Context c;

    public SubmittedRptAdapter(final Context mContext,ArrayList<SubmittedReportModel> lists,final Context c)
    {
        this.lists=lists;
        this.mContext = mContext;
        this.c = c;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View v = (View) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_submittedreport, viewGroup, false);
        return new ViewHolder(v);


    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i)
    {
        SubmittedReportModel submittedReportModel=lists.get(i);

        viewHolder.companyNametv.setText(submittedReportModel.getCompanyName());
        viewHolder.daytv.setText(submittedReportModel.getDaytv());
        //viewHolder.datetv.setText(submittedReportModel.getDatetv());
        //viewHolder.timetv.setText(submittedReportModel.getTimetv());
        viewHolder.viewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,SubmittedDVRExpanded.class);

                SubmittedReportModel adata = new SubmittedReportModel();

                adata=  submittedReportModel;

                Gson gson = new Gson();
                String Json = gson.toJson(adata);  //see firstly above
                Log.i(TAG, "getAllData: " + Json);

                intent.putExtra("Json",Json);
                intent.putExtra("dealer",submittedReportModel.getCompanyName());

               /* DVDisplay.name = lists.get(i).getCompanyName();
                DVDisplay.points = lists.get(i).getDatetv();
                DVDisplay.action = lists.get(i).getTimetv();
                DVDisplay.created = lists.get(i).getDaytv();*/

                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);

            }
        });
        viewHolder.emailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //pgs.setVisibility(View.VISIBLE);
               // Toast.makeText(mContext, "Sending mail........", Toast.LENGTH_LONG).show();
                 String id = submittedReportModel.getId();
                String created_on = submittedReportModel.getCreated();
                String action_plan = submittedReportModel.getTimetv();
                String points_discussed = submittedReportModel.getDatetv();
                String latitude = submittedReportModel.getLatitude();
                String longitude = submittedReportModel.getLongitude();
                String dealer_code = submittedReportModel.getDealer_code();

                int code = Integer.parseInt(id);
                DvReport dvReport = new DvReport();
                List<DVEmail> dv = new ArrayList<DVEmail>() ;
                DVEmail dvEmail = new DVEmail(id,dealer_code,created_on,points_discussed,action_plan,latitude,longitude);
                dv.add(dvEmail);


                // custom dialog
                Dialog dialog = new Dialog(c);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setGravity(Gravity.CENTER|Gravity.CENTER);
                dialog.setContentView(R.layout.common_cus_dialog);
                TextView Message, Message2;
                Button cancel, Confirm;
                Message = dialog.findViewById(R.id.Message);
                Message2 = dialog.findViewById(R.id.Message2);
                cancel = dialog.findViewById(R.id.cancel);
                Confirm = dialog.findViewById(R.id.Confirm);
                Message.setText(GlobalText.are_you_sure_to_send_mail);
                //Message2.setText("Go ahead and Submit ?");
                Confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SpinnerManager.showSpinner(c);
                        dvReport.pushEMail(dv,id, new HttpCallResponse() {
                            @Override
                            public void OnSuccess(Object obj) {

                                SpinnerManager.hideSpinner(c);


                                Log.d("successmsg", "success sent");
                                confiirmAlert();

                            }

                            @Override
                            public void OnFailure(Throwable mThrowable) {
                                //  pgs.setVisibility(View.GONE);
                                Log.i("error", "OnFailure: "+mThrowable);
                                Toast.makeText(mContext, "cannot submit Please check your connection or Host is down ", Toast.LENGTH_LONG).show();
                            }
                        });

                        dialog.dismiss();
                    }
                });
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();





            }
        });


    }
    public void confiirmAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(c);

        builder.setMessage("Email sent succesfully") .setTitle("success");

        //Setting message manually and performing action on button click
        builder.setMessage("Email sent Sucessfully")
                .setCancelable(false)
//                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        //finish();
//                        Toast.makeText(mContext,"you choose yes action for alertbox",
//                                Toast.LENGTH_SHORT).show();
//                    }
//                })
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();

                    }
                });
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Done");
        alert.show();
        //Toast.makeText(mContext,"workig",Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {

        if (lists != null) {
            return lists.size();
        } else {
            return 0;
        }
    }

    public int getPosition(){
        return position;
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        public final View view;
        public final TextView companyNametv,daytv;//datetv,timetv;

        public final Button viewBtn,emailBtn;




        public ViewHolder(View view) {
            super(view);
            this.view = view;
            companyNametv = view.findViewById(R.id.wr_cname_tv);
            daytv = view.findViewById(R.id.daytv);
            //datetv = view.findViewById(R.id.sr_datetv);
            //timetv = view.findViewById(R.id.srtimetv);
            viewBtn = view.findViewById(R.id.srviewbtn);
            emailBtn = view.findViewById(R.id.sremailtv);
        }

    }


}


