package com.mfcwl.mfc_dealer.ResponseHandler;

/**
 * Created by Surya on 11/06/18.
 */
public interface HttpCallResponse {

    void OnSuccess(Object obj);

    void OnFailure(Throwable mThrowable);

}
