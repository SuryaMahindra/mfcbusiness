package com.mfcwl.mfc_dealer.IbbMarketTradeService;

import android.app.Activity;
import android.content.Context;

import com.mfcwl.mfc_dealer.IbbMarketTradeModel.RequestAccessTokenIbbTrade;
import com.mfcwl.mfc_dealer.IbbMarketTradeModel.RequestIbbTradeData;
import com.mfcwl.mfc_dealer.IbbMarketTradeModel.ResponseAccessTokenIbbTrade;
import com.mfcwl.mfc_dealer.IbbMarketTradeModel.ResponseIbbTradeData;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

import static com.mfcwl.mfc_dealer.retrofitconfig.IbbBaseService.retrofit;

public class MarketTradeService {

    private static Activity context;

    public static void IddMrkTradegetAccessToken(Context mContext,final String name, final String password, final HttpCallResponse mHttpCallResponse) {
        AccessTokenIbbInterface mInterface = retrofit.create(AccessTokenIbbInterface.class);

        RequestAccessTokenIbbTrade requestAccessTokenIbbTrade = new RequestAccessTokenIbbTrade();

        requestAccessTokenIbbTrade.setUsername(name);
        requestAccessTokenIbbTrade.setPassword(password);
        Call<ResponseAccessTokenIbbTrade> mCall = mInterface.getIbbAccessToken(requestAccessTokenIbbTrade);
        mCall.enqueue(new Callback<ResponseAccessTokenIbbTrade>() {
            @Override
            public void onResponse(Call<ResponseAccessTokenIbbTrade> call, Response<ResponseAccessTokenIbbTrade> response) {

                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }else{
                    if (response.code() == 400) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 401) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 500) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 404) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 304) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 503) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 405) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseAccessTokenIbbTrade> call, Throwable t) {

                mHttpCallResponse.OnFailure(t);

            }
        });

    }

    //for year data

    public static void IddMrkTradeData(Context mContext,final String year, final String app, String access_token, final HttpCallResponse mHttpCallResponse) {

        AccessTokenIbbInterface mInterface = retrofit.create(AccessTokenIbbInterface.class);

        RequestIbbTradeData req = new RequestIbbTradeData();
        req.setAccessToken(access_token);
        req.setTag(app);
        req.setFor(year);
        Call<ResponseIbbTradeData> mCall = mInterface.getIbbTradeData(req);
        mCall.enqueue(new Callback<ResponseIbbTradeData>() {
            @Override
            public void onResponse(Call<ResponseIbbTradeData> call, Response<ResponseIbbTradeData> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }else{
                    if (response.code() == 400) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 401) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 500) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 404) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 304) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 503) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    } else if (response.code() == 405) {
                        WebServicesCall.error_popup_retrofit(response.code(), response.message(), mContext);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseIbbTradeData> call, Throwable t) {

            }
        });
    }

    //for city
    public static void IddMrkTradeDataCity(String forvalue, final String year, final String app, String access_token, final HttpCallResponse mHttpCallResponse) {

        AccessTokenIbbInterface mInterface = retrofit.create(AccessTokenIbbInterface.class);

        RequestIbbTradeData req = new RequestIbbTradeData();
        req.setAccessToken(access_token);
        req.setTag(app);
        req.setFor(forvalue);
        req.setYear(year);
        Call<ResponseIbbTradeData> mCall = mInterface.getIbbTradeData(req);
        mCall.enqueue(new Callback<ResponseIbbTradeData>() {
            @Override
            public void onResponse(Call<ResponseIbbTradeData> call, Response<ResponseIbbTradeData> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<ResponseIbbTradeData> call, Throwable t) {

            }
        });
    }

    //ibb make
    public static void IddMrkTradeDataMake(String forvalue, final String year, String month,
                                           final String app, String access_token, final HttpCallResponse mHttpCallResponse) {

        AccessTokenIbbInterface mInterface = retrofit.create(AccessTokenIbbInterface.class);

        RequestIbbTradeData req = new RequestIbbTradeData();
        req.setAccessToken(access_token);
        req.setTag(app);
        req.setFor(forvalue);
        req.setYear(year);
        req.setMonth(month);
        Call<ResponseIbbTradeData> mCall = mInterface.getIbbTradeData(req);
        mCall.enqueue(new Callback<ResponseIbbTradeData>() {
            @Override
            public void onResponse(Call<ResponseIbbTradeData> call, Response<ResponseIbbTradeData> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<ResponseIbbTradeData> call, Throwable t) {

            }
        });
    }

    //ibb model
    public static void IddMrkTradeDataModel(String forvalue, final String make, String year, String month,
                                            final String app, String access_token, final HttpCallResponse mHttpCallResponse) {

        AccessTokenIbbInterface mInterface = retrofit.create(AccessTokenIbbInterface.class);

        RequestIbbTradeData req = new RequestIbbTradeData();
        req.setAccessToken(access_token);
        req.setTag(app);
        req.setFor(forvalue);
        req.setMonth(month);
        req.setYear(year);
        req.setMake(make);
        Call<ResponseIbbTradeData> mCall = mInterface.getIbbTradeData(req);


        mCall.enqueue(new Callback<ResponseIbbTradeData>() {
            @Override
            public void onResponse(Call<ResponseIbbTradeData> call, Response<ResponseIbbTradeData> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<ResponseIbbTradeData> call, Throwable t) {

            }
        });
    }

    //ibb variant
    public static void IddMrkTradeDataVariant(String forvalue, final String year, String month, String make, String model, final String app, String access_token, final HttpCallResponse mHttpCallResponse) {

        AccessTokenIbbInterface mInterface = retrofit.create(AccessTokenIbbInterface.class);

        RequestIbbTradeData req = new RequestIbbTradeData();
        req.setAccessToken(access_token);
        req.setTag(app);
        req.setFor(forvalue);
        req.setMake(make);
        req.setMonth(month);
        req.setYear(year);
        req.setModel(model);
        Call<ResponseIbbTradeData> mCall = mInterface.getIbbTradeData(req);
        mCall.enqueue(new Callback<ResponseIbbTradeData>() {
            @Override
            public void onResponse(Call<ResponseIbbTradeData> call, Response<ResponseIbbTradeData> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<ResponseIbbTradeData> call, Throwable t) {

            }
        });
    }

    //ibb city
    public static void IddMrkTradeDataCity(String forvalue, final String app, String access_token, final HttpCallResponse mHttpCallResponse) {

        AccessTokenIbbInterface mInterface = retrofit.create(AccessTokenIbbInterface.class);

        RequestIbbTradeData req = new RequestIbbTradeData();
        req.setAccessToken(access_token);
        req.setTag(app);
        req.setFor(forvalue);
        Call<ResponseIbbTradeData> mCall = mInterface.getIbbTradeData(req);
        mCall.enqueue(new Callback<ResponseIbbTradeData>() {
            @Override
            public void onResponse(Call<ResponseIbbTradeData> call, Response<ResponseIbbTradeData> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<ResponseIbbTradeData> call, Throwable t) {

            }
        });
    }

    //ibb color
    public static void IddMrkTradeDataColor(String forvalue, final String app, String access_token, final HttpCallResponse mHttpCallResponse) {

        AccessTokenIbbInterface mInterface = retrofit.create(AccessTokenIbbInterface.class);

        RequestIbbTradeData req = new RequestIbbTradeData();
        req.setAccessToken(access_token);
        req.setTag(app);
        req.setFor(forvalue);
        Call<ResponseIbbTradeData> mCall = mInterface.getIbbTradeData(req);
        mCall.enqueue(new Callback<ResponseIbbTradeData>() {
            @Override
            public void onResponse(Call<ResponseIbbTradeData> call, Response<ResponseIbbTradeData> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<ResponseIbbTradeData> call, Throwable t) {

            }
        });
    }

    //ibb Owner
    public static void IddMrkTradeDataOwner(String forvalue, final String app, String access_token, final HttpCallResponse mHttpCallResponse) {

        AccessTokenIbbInterface mInterface = retrofit.create(AccessTokenIbbInterface.class);

        RequestIbbTradeData req = new RequestIbbTradeData();
        req.setAccessToken(access_token);
        req.setTag(app);
        req.setFor(forvalue);
        Call<ResponseIbbTradeData> mCall = mInterface.getIbbTradeData(req);
        mCall.enqueue(new Callback<ResponseIbbTradeData>() {
            @Override
            public void onResponse(Call<ResponseIbbTradeData> call, Response<ResponseIbbTradeData> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<ResponseIbbTradeData> call, Throwable t) {

            }
        });
    }

    public static void IddMrkTradeDataComprehensivePrice(String forvalue, String year, String month, String make, String model, String variant, String location, String color, String owner, String kilometer, int priceFor, String partUserId, final String app, String access_token, final HttpCallResponse mHttpCallResponse) {

        AccessTokenIbbInterface mInterface = retrofit.create(AccessTokenIbbInterface.class);

        RequestIbbTradeData req = new RequestIbbTradeData();
        req.setAccessToken(access_token);
        req.setTag(app);
        req.setFor(forvalue);
        req.setYear(year);
        req.setMonth(month);
        req.setMake(make);
        req.setModel(model);
        req.setVariant(variant);
        req.setLocation(location);
        req.setColor(color);
        req.setOwner(owner);
        req.setKilometer(kilometer);
        req.setPricefor(priceFor);
        req.setPartuserid(partUserId);
        Call<ResponseIbbTradeData> mCall = mInterface.getIbbTradeData(req);
        mCall.enqueue(new Callback<ResponseIbbTradeData>() {
            @Override
            public void onResponse(Call<ResponseIbbTradeData> call, Response<ResponseIbbTradeData> response) {
                if (response.isSuccessful()) {
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<ResponseIbbTradeData> call, Throwable t) {

            }
        });
    }

    public interface AccessTokenIbbInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("get_access_token")
        Call<ResponseAccessTokenIbbTrade> getIbbAccessToken(@Body RequestAccessTokenIbbTrade requestAccessTokenIbbTrade);


        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("MFC")
        Call<ResponseIbbTradeData> getIbbTradeData(@Body RequestIbbTradeData requestIbbTradeComprehensivePrice);


    }

}
