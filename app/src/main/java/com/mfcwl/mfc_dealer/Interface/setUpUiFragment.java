package com.mfcwl.mfc_dealer.Interface;

import android.view.View;

import org.json.JSONException;

public  interface setUpUiFragment {

    void intialGUI(View v) throws JSONException;

    void setData();

}
