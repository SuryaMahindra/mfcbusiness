package com.mfcwl.mfc_dealer.Interface;

import android.view.View;

/**
 * Created by sowmya on 13/7/16.
 */

public interface ClickListener {
    void onClick(View view, int position);

    void onLongClick(View view, int position);
}
