package com.mfcwl.mfc_dealer.Interface.LeadSection;

public interface FilterSelected {
    void onFilterSelection(String startDateString, String endDateString, String dateType);
}
