package com.mfcwl.mfc_dealer.ASMSalesServices;

import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.retrofitconfig.DasboardBaseService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class ASMSalesService extends DasboardBaseService {

    public static void fetchWarrantySalesCount(WarrantySalesCountReq mReq, final HttpCallResponse mResponse){
        ASMSalesInterface mInterface = retrofit.create(ASMSalesInterface.class);

        Call<List<WarrantySalesCountRes>> mCall =  mInterface.getWarrantySalesCount(mReq);

        mCall.enqueue(new Callback<List<WarrantySalesCountRes>>() {
            @Override
            public void onResponse(Call<List<WarrantySalesCountRes>> call, Response<List<WarrantySalesCountRes>> response) {
                if(response.isSuccessful()){
                    mResponse.OnSuccess(response);
                }else {
                    Throwable t=new Throwable ();
                    mResponse.OnFailure(t);
                }
            }

            @Override
            public void onFailure(Call<List<WarrantySalesCountRes>> call, Throwable t) {
                mResponse.OnFailure(t);
            }
        });


    }

    public static void fetchTotalSalesCount(WarrantySalesCountReq mReq , HttpCallResponse mCallResponse){
        ASMSalesInterface mInterface = retrofit.create(ASMSalesInterface.class);

        Call<TotalSalesCountRes> mCall = mInterface.getTotalSalesCount(mReq);
        mCall.enqueue(new Callback<TotalSalesCountRes>() {
            @Override
            public void onResponse(Call<TotalSalesCountRes> call, Response<TotalSalesCountRes> response) {
                if(response.isSuccessful()){
                    mCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<TotalSalesCountRes> call, Throwable t) {
                mCallResponse.OnFailure(t);
            }
        });
    }



    public interface ASMSalesInterface{
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("franchise/warrantysalescount")
        Call<List<WarrantySalesCountRes>> getWarrantySalesCount(@Body WarrantySalesCountReq mReq);

        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("franchise/TotalSalescount")
        Call<TotalSalesCountRes> getTotalSalesCount(@Body WarrantySalesCountReq mReq);
    }
}
