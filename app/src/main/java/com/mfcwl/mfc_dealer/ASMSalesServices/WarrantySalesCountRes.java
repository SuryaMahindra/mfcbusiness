package com.mfcwl.mfc_dealer.ASMSalesServices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WarrantySalesCountRes {

    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("amount")
    @Expose
    private Double amount;
    @SerializedName("warranty_type")
    @Expose
    private String warrantyType;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getWarrantyType() {
        return warrantyType;
    }

    public void setWarrantyType(String warrantyType) {
        this.warrantyType = warrantyType;
    }
}