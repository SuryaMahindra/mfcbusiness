package com.mfcwl.mfc_dealer.SalesStocks.Fragment;

public class WarrantyConstant {
    private static WarrantyConstant mInstance = null;


    public static WarrantyConstant getmInstance() {
        return mInstance;
    }

    public static void setmInstance(WarrantyConstant mInstance) {
        WarrantyConstant.mInstance = mInstance;
    }

    public String getWhichWarranty() {
        return whichWarranty;
    }

    public void setWhichWarranty(String whichWarranty) {
        this.whichWarranty = whichWarranty;
    }

    private String whichWarranty;

    private WarrantyConstant() {
        whichWarranty = "";

    }

    public static WarrantyConstant getInstance() {
        if (mInstance == null) {
            mInstance = new WarrantyConstant();
        }
        return mInstance;
    }
}
