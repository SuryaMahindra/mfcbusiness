package com.mfcwl.mfc_dealer.SalesStocks.SalesInterface;

public interface SalesFilterSelected {
    void onSalesFilterSelection(String startDateString, String endDateString, String dateType);
}
