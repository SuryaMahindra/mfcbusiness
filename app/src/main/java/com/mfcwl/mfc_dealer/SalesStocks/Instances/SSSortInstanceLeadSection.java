package com.mfcwl.mfc_dealer.SalesStocks.Instances;

public class SSSortInstanceLeadSection {
    private static SSSortInstanceLeadSection mInstance = null;
    private String whichsort;
    private String sortby;

    private SSSortInstanceLeadSection() {
        whichsort = "";
        sortby = "";

    }

    public static SSSortInstanceLeadSection getInstance() {
        if (mInstance == null) {
            mInstance = new SSSortInstanceLeadSection();
        }
        return mInstance;
    }

    public String getWhichsort() {
        return whichsort;
    }

    public void setWhichsort(String whichsort) {
        this.whichsort = whichsort;
    }

    public String getSortby() {
        return sortby;
    }

    public void setSortby(String sortby) {
        this.sortby = sortby;
    }
}
