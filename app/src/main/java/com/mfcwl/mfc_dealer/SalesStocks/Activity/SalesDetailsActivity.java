package com.mfcwl.mfc_dealer.SalesStocks.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.RequiresApi;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Adapter.CustomPagerAdapter;
import com.mfcwl.mfc_dealer.NetworkConnect.WebServicesCall;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.MonthUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SalesDetailsActivity extends AppCompatActivity {


    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    public static TextView carName, days, modelVariant, stock_registration_no, sellingprice, ownertv,
            yeartv, kmstv, fueltypetv, regcitytv, stock_bought_price_val, stock_registration_no_val, stock_refurbishment_cost_val,
            stock_certification_no_val, stock_dealer_price_val, stock_insurance_val, stock_procurement_executive, stock_insurance_expiry_date,
            stock_cng_kit, stock_chassis_number, stock_enginenotv, stock_source, stock_warrantytv, stock_vehicletype, stock_cattv,
            stock_comments, stock_img_count, post_on_lab, postOn;

    public static ImageView iv_price_image;

    public static LinearLayout buttom_ll;

    public static ViewPager viewPager;
    @BindView(R.id.left_nav)
    public ImageView leftNav;
    ArrayList<String> salesexecIdList;
    ArrayList<String> salesexecList;
    public static Activity activity;
    public static String isCertifiedWhatsupdata;
    ImageView rightNav;
    @BindView(R.id.collapsing_toolbar)
    public CollapsingToolbarLayout collapsingToolbarLayout;

    public static String make, variantModel, variant, regno, selling_price, owner, Regyear, kms, registraionCity, certifiedNo,
            insurance = "", insuranceExp_date, source = "", warranty, surveyor_remark, photo_count, reg_month, fuel_type,
            bought, refurbishment_cost, dealer_price, procurement_executive_id, cng_kit, chassis_number, engine_number,
            is_display, is_certified, is_featured_car_admin, procurement_executive_name, private_vehicle, comments,
            finance_required, manufacture_month, sales_executive_id, sales_executive_name, manufacture_year,
            actual_selling_price, CertifiedText, IsDisplay, TransmissionType, colour, dealer_code, is_offload,
            posted_date, stock_id = "", is_booked, coverimage;

    public static String is_featured_car;
    public static TextView stock_Certified_lbl;
    public static String[] imageurl;
    public static String[] imagename;
    public static String share_Content = "";
    public static String imageCount = "";
    public static int imageLenght = 0;
    public static MenuItem item;
    public static Menu menus;
    public int current = 0, last = 0;
    public static ProgressDialog progress;

    private ArrayList<Uri> imageUriArray;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_store_info);
        Log.i(TAG, "onCreate: ");
        CommonMethods.setvalueAgainstKey(SalesDetailsActivity.this, "issalesDetails", "true");

        ButterKnife.bind(this);
        activity = this;
        salesexecIdList = new ArrayList<String>();
        salesexecList = new ArrayList<String>();
        progress = new ProgressDialog(activity);

        imageurl = new String[]{};
        imagename = new String[]{};

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        ViewCompat.setTransitionName(findViewById(R.id.app_bar_layout), null);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");


        collapsingToolbarLayout.setTitle("");
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        //slider image
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        buttom_ll = (LinearLayout) findViewById(R.id.buttom_ll);

        buttom_ll.setVisibility(View.GONE);


        leftNav = (ImageView) findViewById(R.id.left_nav);
        rightNav = (ImageView) findViewById(R.id.right_nav);
        stock_source = (TextView) findViewById(R.id.stock_source);
        stock_comments = (TextView) findViewById(R.id.stock_comments);

        stock_vehicletype = (TextView) findViewById(R.id.stock_vehicletype);
        stock_img_count = (TextView) findViewById(R.id.stock_img_count);
        postOn = (TextView) findViewById(R.id.post_on);
        stock_Certified_lbl = (TextView) findViewById(R.id.stock_Certified_lbl);
        days = (TextView) findViewById(R.id.days);
        post_on_lab = (TextView) findViewById(R.id.post_on_lab);
        stock_chassis_number = (TextView) findViewById(R.id.stock_chassis_number);
        stock_enginenotv = (TextView) findViewById(R.id.stock_enginenotv);
        carName = (TextView) findViewById(R.id.car_name);
        stock_procurement_executive = (TextView) findViewById(R.id.stock_procurement_executive);
        modelVariant = (TextView) findViewById(R.id.expedition);
        sellingprice = (TextView) findViewById(R.id.sellingprice);
        iv_price_image = (ImageView) findViewById(R.id.iv_price_image);
        ownertv = (TextView) findViewById(R.id.ownertv);
        stock_refurbishment_cost_val = (TextView) findViewById(R.id.stock_refurbishment_cost_val);
        yeartv = (TextView) findViewById(R.id.yeartv);
        kmstv = (TextView) findViewById(R.id.kmstv);
        fueltypetv = (TextView) findViewById(R.id.fueltypetv);
        regcitytv = (TextView) findViewById(R.id.regcitytv);
        stock_bought_price_val = (TextView) findViewById(R.id.stock_bought_price_val);
        stock_certification_no_val = (TextView) findViewById(R.id.stock_certification_no_val);
        stock_insurance_val = (TextView) findViewById(R.id.stock_insurance_val);
        stock_insurance_expiry_date = (TextView) findViewById(R.id.stock_insurance_expiry_date);
        stock_cng_kit = (TextView) findViewById(R.id.stock_cng_kit);
        stock_warrantytv = (TextView) findViewById(R.id.stock_warrantytv);
        stock_registration_no_val = (TextView) findViewById(R.id.stock_registration_no_val);
        stock_registration_no = (TextView) findViewById(R.id.stock_registration_no);
        stock_dealer_price_val = (TextView) findViewById(R.id.stock_dealer_price_val);
        stock_cattv = (TextView) findViewById(R.id.stock_cattv);


        Window window = activity.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(activity, R.color.Black));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        leftNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewPager.getCurrentItem();
                viewPager.arrowScroll(View.FOCUS_LEFT);
            }
        });

        // Images right navigatin
        rightNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewPager.getCurrentItem();
                viewPager.arrowScroll(View.FOCUS_RIGHT);
            }
        });

        // Images right navigatin
        viewPager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewPager.getCurrentItem();
            }
        });

        viewPager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                stock_img_count.setText(arg0 + 1 + "/" + imageLenght);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int num) {
            }
        });


        updateFontUI(this);


        progress = new ProgressDialog(activity);
        imageurl = new String[]{};
        imagename = new String[]{};

        WebServicesCall.webCall(activity, activity, jsonMakestock(), "SalesDetails", GlobalText.POST);

    }


    public static void stockdetailspage(JSONObject jObj, String strMethod, Activity activity) {

        try {


            JSONArray arrayList = jObj.getJSONArray("data");

            for (int i = 0; i < arrayList.length(); i++) {
                JSONObject jsonobject = new JSONObject(
                        arrayList.getString(i));

                posted_date = jsonobject.getString("posted_date");

                String createdDate = posted_date.toString();
                // Log.e("getPostedDate ", "getPostedDate " + createdDate);
                String[] splitDateTime = createdDate.split(" ", 10);
                String[] splitDate = splitDateTime[0].split("-", 10);
                String finalDate = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");
                Calendar c = Calendar.getInstance();
                String formattedDate = simpleDateFormat.format(c.getTime());
                String[] splitSpace = formattedDate.split(" ", 10);


                try {
                    Date date1 = simpleDateFormat.parse(finalDate + " 00:00:00");
                    Date date2 = simpleDateFormat.parse(splitSpace[0] + " 00:00:00");

                    Log.e("diff ", "diff " + date1 + " " + date2 + "==" + printDifference(date1, date2));

                    String day = "" + printDifference(date1, date2);

                    days.setVisibility(View.VISIBLE);
                    if (day.equalsIgnoreCase("0")) {
                        days.setText("" + "Today");
                    } else if (day.equalsIgnoreCase("1")) {
                        days.setText("" + "Yesterday");

                    } else {
                        days.setText("" + printDifference(date1, date2) + " D ago");

                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                regno = jsonobject.getString("registration_number");
                stock_registration_no.setText(regno);
                make = jsonobject.getString("vehicle_make");
                carName.setText(make);
                variantModel = jsonobject.getString("vehicle_model");
                variant = jsonobject.getString("vehicle_variant");
                modelVariant.setText(variantModel + " " + variant);
                selling_price = String.valueOf(jsonobject.getInt("selling_price"));
                iv_price_image.setVisibility(View.VISIBLE);
                sellingprice.setText(selling_price);
                owner = String.valueOf(jsonobject.getInt("owner"));
                ownertv.setText(owner);
                Regyear = String.valueOf(jsonobject.getInt("reg_year"));
                reg_month = jsonobject.getString("reg_month");
                stock_registration_no_val.setText(reg_month + " " + Regyear);
                kms = String.valueOf(jsonobject.getInt("kilometer"));
                kmstv.setText(kms);
                registraionCity = jsonobject.getString("registraion_city");
                regcitytv.setText(registraionCity);
                fuel_type = jsonobject.getString("fuel_type");
                if (fuel_type == null || fuel_type.equalsIgnoreCase("")
                        || fuel_type.equalsIgnoreCase("null")) {
                    fuel_type = "NA";
                }
                fueltypetv.setText(fuel_type);

                //------------------------------------

                final JSONArray images = jsonobject.getJSONArray("images");

                ArrayList<String> imageurls;
                imageurls = new ArrayList<String>();
                ArrayList<String> imagenames;

                imagenames = new ArrayList<String>();


                if (images != null && images.length() > 0) {
                    for (int j = 0; j < images.length(); j++) {
                        try {
                            JSONObject data = images.getJSONObject(j);
                            imageurls.add(data.getString("url"));
                            imagenames.add(data.getString("category_identifier"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    imageurls.add("url");
                    imagenames.add("category_identifier");

                }


                Object[] objectList_imagenames = imagenames.toArray();
                imagename = Arrays.copyOf(objectList_imagenames, objectList_imagenames.length, String[].class);


                Object[] objectList = imageurls.toArray();

                imageurl = Arrays.copyOf(objectList, objectList.length, String[].class);

                if (imageurl[0].equalsIgnoreCase("url")) {
                    imageLenght = 0;
                } else {
                    imageLenght = imageurl.length;
                }
                stock_img_count.setVisibility(View.VISIBLE);
                if (imageLenght == 0) {
                    stock_img_count.setText("0" + "/" + imageLenght);
                } else {
                    stock_img_count.setText("1" + "/" + imageLenght);
                }


                CustomPagerAdapter mCustomPagerAdapter = new CustomPagerAdapter(activity, imageurl);
                viewPager.setAdapter(mCustomPagerAdapter);

                if (!jsonobject.getString("cover_image").equalsIgnoreCase("")) {
                    coverimage = jsonobject.getString("cover_image").toString();
                } else {
                    coverimage = imageurl[0];
                }

                imageCount = String.valueOf(jsonobject.getInt("photo_count"));


                certifiedNo = jsonobject.getString("certification_number");
                if (certifiedNo == null || certifiedNo.equalsIgnoreCase("")
                        || certifiedNo.equalsIgnoreCase("null")) {
                    certifiedNo = "NA";
                }

                insurance = jsonobject.getString("insurance");

                if (insurance == null || insurance.equalsIgnoreCase("")
                        || insurance.equalsIgnoreCase("null")) {
                    insurance = "NA";
                }
                insuranceExp_date = jsonobject.getString("insurance_exp_date");

                if (insuranceExp_date == null || insuranceExp_date.equalsIgnoreCase("")
                        || insuranceExp_date.equalsIgnoreCase("null")) {
                    insuranceExp_date = "NA";
                }

                source = jsonobject.getString("stock_source");
                if (source == null || source.equalsIgnoreCase("")
                        || source.equalsIgnoreCase("null")) {
                    source = "NA";
                }

                warranty = jsonobject.getString("warranty_recommended");

                if (warranty == null || warranty.equalsIgnoreCase("")
                        || warranty.equalsIgnoreCase("null")) {
                    warranty = "NA";
                }
                Log.e("warranty", "warranty=" + warranty);


                surveyor_remark = jsonobject.getString("surveyor_remark");

                if (surveyor_remark == null || surveyor_remark.equalsIgnoreCase("")
                        || surveyor_remark.equalsIgnoreCase("null")) {
                    surveyor_remark = "NA";
                }

                photo_count = String.valueOf(jsonobject.getInt("photo_count"));


                Log.e("reg_month", "reg_month->>" + jsonobject.getString("reg_month"));
                Log.e("reg_month", "reg_month->=" + reg_month);


                bought = jsonobject.getString("bought_price");
                refurbishment_cost = jsonobject.getString("refurbishment_cost");
                dealer_price = jsonobject.getString("dealer_price");
                procurement_executive_id = jsonobject.getString("procurement_executive_id");

                if (procurement_executive_id == null || procurement_executive_id.equalsIgnoreCase("")) {
                    procurement_executive_id = "0";
                }

                cng_kit = jsonobject.getString("cng_kit");

                if (cng_kit == null || cng_kit.equalsIgnoreCase("")
                        || cng_kit.equalsIgnoreCase("null")) {
                    cng_kit = "NA";
                }

                chassis_number = jsonobject.getString("chassis_number");
                engine_number = jsonobject.getString("engine_number");
                is_certified = jsonobject.getString("is_certified");
                is_featured_car = jsonobject.getString("is_featured_car");


                if (is_featured_car.equalsIgnoreCase("true") || is_featured_car.equalsIgnoreCase("1")) {
                    postOn.setBackgroundResource(R.drawable.star_y);
                    post_on_lab.setText("Featured");
                } else {
                    postOn.setBackgroundResource(R.drawable.star);
                }
                if (source.toString().equalsIgnoreCase("mfc")) {
                    postOn.setVisibility(View.VISIBLE);
                } else {
                    postOn.setVisibility(View.INVISIBLE);
                }

                is_featured_car_admin = "";
                procurement_executive_name = jsonobject.getString("procurement_executive_name");
                private_vehicle = jsonobject.getString("private_vehicle");

                comments = jsonobject.getString("comments");

                if (comments == null || comments.equalsIgnoreCase("")
                        || comments.equalsIgnoreCase("null")) {
                    stock_comments.setText("NA");
                } else {
                    stock_comments.setText(comments);

                }

                finance_required = jsonobject.getString("finance_required");
                manufacture_month = jsonobject.getString("manufacture_month");
                sales_executive_id = jsonobject.getString("sales_executive_id");
                sales_executive_name = jsonobject.getString("sales_executive_name");
                manufacture_year = jsonobject.getString("manufacture_year");
                actual_selling_price = jsonobject.getString("actual_selling_price");
                CertifiedText = jsonobject.getString("CertifiedText");
                IsDisplay = "";
                TransmissionType = "";
                colour = jsonobject.getString("colour");
                dealer_code = jsonobject.getString("dealer_code");
                is_offload = jsonobject.getString("is_offload");

                stock_id = String.valueOf(jsonobject.getInt("stock_id"));

                CommonMethods.setvalueAgainstKey(activity, "stock_id", stock_id);
                is_booked = jsonobject.getString("is_booked");


                //reena
                if (is_certified == null) {
                    stock_Certified_lbl.setVisibility(View.GONE);
                    isCertifiedWhatsupdata = "NO";
                } else {
                    if (is_certified.equals("1") || is_certified.equals("true")) {
                        stock_Certified_lbl.setText("Certified");
                        stock_Certified_lbl.setVisibility(View.VISIBLE);
                        isCertifiedWhatsupdata = "YES";
                    } else {
                        stock_Certified_lbl.setVisibility(View.GONE);
                        isCertifiedWhatsupdata = "NO";
                    }
                }

                if (chassis_number == null || chassis_number.equalsIgnoreCase("")
                        || chassis_number.equalsIgnoreCase("null")) {
                    chassis_number = "NA";

                }
                if (engine_number == null || engine_number.equalsIgnoreCase("")
                        || engine_number.equalsIgnoreCase("null")) {
                    engine_number = "NA";

                }

                stock_chassis_number.setText(chassis_number);
                stock_enginenotv.setText(engine_number);

                if (procurement_executive_name == null || procurement_executive_name.equalsIgnoreCase("")) {
                    procurement_executive_name = "";
                }
                stock_procurement_executive.setText(procurement_executive_name);

                if (refurbishment_cost == null || refurbishment_cost.equalsIgnoreCase("")) {
                    refurbishment_cost = "0";
                }

                stock_refurbishment_cost_val.setText(refurbishment_cost);
                yeartv.setText(manufacture_month.substring(0, 3) + " " + manufacture_year);

                if (bought == null || bought.equalsIgnoreCase("")) {
                    bought = "0";
                }
                stock_bought_price_val.setText(bought);

                stock_certification_no_val.setText(certifiedNo);
                stock_insurance_val.setText(insurance);


                if (insuranceExp_date == null || insuranceExp_date.equalsIgnoreCase("")
                        || insuranceExp_date.equalsIgnoreCase("null")) {
                    stock_insurance_expiry_date.setText("NA");

                } else {

                    if (insuranceExp_date.length() < 10) {
                        stock_insurance_expiry_date.setText(insuranceExp_date);

                    } else {

                        //reena
                        String insuranceExp_date1 = insuranceExp_date.substring(0, 10);
                        String CurrentString = insuranceExp_date1;
                        String[] separated = CurrentString.split("-");
                        String s1 = separated[0];
                        String s2 = separated[1];
                        String s3 = separated[2];

                        Log.e("test split", s1 + " " + s2 + " " + s3);

                        String insuranceExp_date = s3 + " " + MonthUtility.Month[Integer.parseInt(s2)] + " " + s1;

                        stock_insurance_expiry_date.setText(insuranceExp_date);
                    }
                }

                stock_cng_kit.setText(cng_kit);
                stock_source.setText(source);

                if (source.equalsIgnoreCase("mfc")) {
                    item = menus.findItem(R.id.add_image);
                    item.setVisible(true);
                } else {
                    item = menus.findItem(R.id.add_image);
                    item.setVisible(false);
                }

                if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {

                } else {
                    item = menus.findItem(R.id.add_image);
                    item.setVisible(false);

                    item = menus.findItem(R.id.share);
                    item.setVisible(true);

                    item = menus.findItem(R.id.asmHome);
                    item.setVisible(false);

                    item = menus.findItem(R.id.asm_mail);
                    item.setVisible(false);
                }

                stock_warrantytv.setText(warranty);
                //stock_img_count.setText(photo_count + "/20");


                if (dealer_price == null || dealer_price.equalsIgnoreCase("")) {
                    dealer_price = "0";
                }

                stock_dealer_price_val.setText(dealer_price);

                if (is_offload.equalsIgnoreCase("0") || is_offload.equalsIgnoreCase("false")) {
                    stock_cattv.setText("Retail");
                } else {
                    stock_cattv.setText("OffLoad Vehicle");
                }
                if (private_vehicle.equalsIgnoreCase("1") || private_vehicle.equalsIgnoreCase("true")) {
                    stock_vehicletype.setText("Private");
                    post_on_lab.setVisibility(View.VISIBLE);

                } else {
                    post_on_lab.setVisibility(View.INVISIBLE);
                    stock_vehicletype.setText("Commercial");
                }


                String CERTIFIED = "", ADDRESS = "", URL = "";
                Double dealer_latitude = null, dealer_longitude = null;
                String newline = "\n";
                //val
                ADDRESS = CommonMethods.getstringvaluefromkey(activity, "dealer_address");

                try {
                    dealer_latitude = Double.parseDouble(CommonMethods.getstringvaluefromkey(activity, "dealer_latitude").toString());
                    dealer_longitude = Double.parseDouble(CommonMethods.getstringvaluefromkey(activity, "dealer_longitude").toString());
                } catch (Exception e) {

                }

                if (dealer_latitude != null
                        && dealer_longitude != null
                        && !dealer_latitude.isNaN()
                        && !dealer_longitude.isNaN()
                ) {

                    URL = "http://maps.google.com/maps?q=" + String.format("%f,%f", dealer_latitude, dealer_longitude);
                }

                if (is_certified.equals("1") || is_certified.equals("true")) {
                    CERTIFIED = "_Certified by Mahindra First Choice_";
                } else {
                    CERTIFIED = "";
                    newline = "";
                }

                share_Content =
                        "*" + make + " " + variantModel + " " + variant + "*" + "\n" +
                                "REG.CITY : " + registraionCity + "\n" +
                                "SELLING PRICE : " + activity.getResources().getString(R.string.rs) + selling_price + "\n" +
                                "FUEL TYPE : " + fuel_type + "\n" +
                                "KMS : " + kms + "\n" +
                                "OWNER : " + owner + "\n" +
                                "MFG. DATE : " + manufacture_month + " " + manufacture_year + newline +
                                CERTIFIED + "\n" +
                                "AVAILABLE AT : " + ADDRESS + "\n" +
                                URL;

                Log.e("test share content", share_Content);

            }


        } catch (Exception ex) {
            Log.e("Parsestockstore-Ex", ex.toString());
        }


    }


    public static JSONObject jsonMakestock() {
        JSONObject jObj = new JSONObject();
        JSONObject jObj1 = new JSONObject();
        JSONObject jObj2 = new JSONObject();

        JSONObject jObjvalues = new JSONObject();


        JSONArray jsonArray = new JSONArray();
        JSONArray jsonArrayValues = new JSONArray();
        JSONArray jObjdealer = new JSONArray();

        try {

            jObj.put("Page", "1");
            jObj.put("PageItems", "10");

            jObj1.put("column", "stock_id");
            jObj1.put("operator", "=");
            jObj1.put("value", "260047" /*CommonMethods.getstringvaluefromkey(activity, "stock_id")*/);

            jObjdealer.put(CommonMethods.getstringvaluefromkey(activity, "dealer_code"));

            jObjvalues.put("column", "dealer_code");
            jObjvalues.put("operator", "=");
            jObjvalues.put("values", jObjdealer);

            jsonArrayValues.put(jObjvalues);

            jObj.put("wherein", jsonArrayValues);


            jsonArray.put(jObj1);


            jObj.put("where", jsonArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("jsonMake1 ", "jsonMake1" + jObj.toString());

        return jObj;
    }


    public static JSONObject jsonMake1() {
        JSONObject jObj = new JSONObject();


        Log.e("jsonMake1 ", "jsonMake1" + jObj.toString());

        return jObj;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        menus = menu;
        getMenuInflater().inflate(R.menu.menu_item, menu);

        item = menu.findItem(R.id.share);
        item.setVisible(true);

        item = menu.findItem(R.id.asmHome);
        item.setVisible(false);

        item = menu.findItem(R.id.asm_mail);
        item.setVisible(false);

        item = menu.findItem(R.id.delete_fea);
        item.setVisible(false);
        item = menu.findItem(R.id.stock_fil_clear);
        item.setVisible(false);
        item = menu.findItem(R.id.notification_bell);
        item.setVisible(false);
        item = menu.findItem(R.id.notification_cancel);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button

            case R.id.share:

                try {
                    listpopupWhatup();
                } catch (Exception e) {

                }

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public void multipleImageshare_wait() {

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                if (!progress.isShowing()) {
                    progress.setTitle("Loading...");
                    progress.setMessage("Please Wait...");
                    progress.setCancelable(false);
                    progress.show();
                }
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(final Void... params) {
                // something you know that will take a few seconds

                try {


                    multipleImageshare();


                } catch (Exception e) {
                    e.printStackTrace();
                    progress.dismiss();
                }
                return null;
            }

            @Override
            protected void onPostExecute(final Void result) {

                if (progress.isShowing()) {
                    progress.dismiss();
                }

                try {
                    Intent intents = new Intent();
                    intents.setAction(Intent.ACTION_SEND_MULTIPLE);
                    intents.setType("image/jpeg");
                    // intents.putExtra(Intent.EXTRA_TEXT,share_Content);
                    intents.setPackage("com.whatsapp");
                    intents.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imageUriArray);
                    startActivity(intents);
                } catch (Exception e) {
                    if (progress.isShowing()) {
                        progress.dismiss();
                    }
                }

            }
        }.execute();
    }


    public void multipleImageshare() {

        imageUriArray = new ArrayList<Uri>();

        for (int i = 0; i < imagename.length; i++) {
            Bitmap mBitmap = null;
            try {
                mBitmap = getBitmapFromUrl(imageurl[i]);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                mBitmap.compress(Bitmap.CompressFormat.JPEG, 70, bytes);
                File f = new File(Environment.getExternalStorageDirectory() + File.separator + imagename[i] + ".jpg");
                try {
                    // deleteImageFile(f);
                    f.createNewFile();
                    new FileOutputStream(f).write(bytes.toByteArray());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Uri uri1 = Uri.parse(Environment.getExternalStorageDirectory() + File.separator + imagename[i] + ".jpg");
                imageUriArray.add(uri1);


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }


    public void listpopupWhatup() {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Share");
        String[] animals;
        // add a list
        if (photo_count.equalsIgnoreCase("0")
                || photo_count.equalsIgnoreCase("-1")
                || photo_count == null
                || photo_count.equalsIgnoreCase("null")) {
            animals = new String[]{"Share Details"};

        } else {
            animals = new String[]{"Share Images", "Share Details"};

        }
        builder.setItems(animals, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: // horse


                        if (photo_count.equalsIgnoreCase("0")
                                || photo_count.equalsIgnoreCase("-1")
                                || photo_count == null
                                || photo_count.equalsIgnoreCase("null")) {

                            shareImageWhatsApp();
                        } else {

                            //imageload();
                            multipleImageshare_wait();

                        }


                        break;
                    case 1: // cow

                        shareImageWhatsApp();
                        //multipleImageshare();
                        break;
                }
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();

        dialog.getWindow().setGravity(Gravity.CENTER | Gravity.LEFT);

        dialog.show();
    }


    public static Bitmap getBitmapFromUrl(String urls) {
        Bitmap bitmap = null;

        try {
            URL url = new URL(urls);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream inputStream1 = connection.getInputStream();

            try {
                bitmap = BitmapFactory.decodeStream(inputStream1);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return bitmap;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;

    }


    public void shareImageWhatsApp() {

        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");

        Log.e("test share content", share_Content);

        share.putExtra(Intent.EXTRA_TEXT, share_Content);
        //sharingIntent.setType("*/*")
        share.setPackage("com.whatsapp");

        if (isPackageInstalled("com.whatsapp", this)) {
            share.setPackage("com.whatsapp");
            startActivity(Intent.createChooser(share, "Share Details"));

        } else {

            Toast.makeText(getApplicationContext(), "Please Install Whatsapp", Toast.LENGTH_LONG).show();
        }

    }

    private boolean isPackageInstalled(String packagename, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }


    //for stock_fragment font
    public void updateFontUI(Context context) {
        ArrayList<Integer> arl = new ArrayList<Integer>();
        arl.add(R.id.car_name);
        arl.add(R.id.expedition);
      /*  arl.add(R.id.stock_registration);
        arl.add(R.id.stock_certified);*/


        setFontStyle(arl, context);
    }

    private void setFontStyle(ArrayList<Integer> tv_list, Context context) {
        for (int i = 0; i < tv_list.size(); i++) {
            Typeface myTypeface = Typeface.createFromAsset(this.getAssets(), "lato_semibold.ttf");
            TextView myTextView = (TextView) findViewById(tv_list.get(i));
            myTextView.setTypeface(myTypeface);
        }
    }


    public static long printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();


        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;


        return elapsedDays;
    }


    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            MainActivity.getSSFragmentRefreshListener().onRefresh();
            //  }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finish();
    }
}
