package com.mfcwl.mfc_dealer.SalesStocks.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Procurement.Activity.ProcurementDetails;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.SalesStocks.Adapter.SalesStockAllAdapter;
import com.mfcwl.mfc_dealer.SalesStocks.Instances.SSConstantsSection;
import com.mfcwl.mfc_dealer.SalesStocks.Instances.SSFilterSaveInstance;
import com.mfcwl.mfc_dealer.SalesStocks.Instances.SSSortInstanceLeadSection;
import com.mfcwl.mfc_dealer.SalesStocks.RetrofitServicesLeadSection.SalesStockService;
import com.mfcwl.mfc_dealer.SalesStocks.SalesInterface.SSFilterApply;
import com.mfcwl.mfc_dealer.SalesStocks.SalesInterface.SSLazyloaderSalesStocks;
import com.mfcwl.mfc_dealer.SalesStocks.SalesModels.SSCustomWhere;
import com.mfcwl.mfc_dealer.SalesStocks.SalesModels.SSWhereIn;
import com.mfcwl.mfc_dealer.SalesStocks.SalesModels.SalesStockAllResponse;
import com.mfcwl.mfc_dealer.SalesStocks.SalesModels.SalesStockReq;
import com.mfcwl.mfc_dealer.SalesStocks.SalesModels.SalesStockResData;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GAConstants;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import rdm.NotificationActivity;
import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.activity;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.linearLayoutVisible;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.navigation;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchImage;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchVal;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchicon;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toggle;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toolbar;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.tv_header_title;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ASM;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_STATEHEAD;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ZONALHEAD;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_DEALER_CRE;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_PROCUREMENT;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USER_TYPE_SALES;

public class SalesStocksAll extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    SwipeRefreshLayout ss_swipeRefresh;
    RecyclerView ss_recyclerView;
    TextView ss_noResults;
    private List<SalesStockResData> mLeadsDatumList;
    private int pageItem = 10;
    private String page_no = "1";
    private int counts = 1;
    private int salesStocklistTotalCount = 0;
    private boolean isLoadmore;
    private String searchQuery = "";
    public  static  SalesStockAllAdapter madapter;
    private HashMap<String, String> postedfollDateHashMap = new HashMap<String, String>();
    private JSONArray leadstatusJsonArray = new JSONArray();
    public boolean flag = false;
    int i = 0;
    public List<SSWhereIn> mwhereinList;
    public List<SSCustomWhere> MfcWhereList;
    public List<SSCustomWhere> salesWhereList;
    public String TAG =getClass().getSimpleName();

    SalesStockReq salesRequest;

    public SalesStocksAll() {


        setHasOptionsMenu(true);
    }

  /*  @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        menu.clear();
        inflater.inflate(R.menu.menu_item, menu);*/



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.fragment_sales_stock_all, container, false);
        setHasOptionsMenu(true);
        if(CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(USERTYPE_ASM))
        {
            linearLayoutVisible.setVisibility(View.VISIBLE);
            searchicon.setVisibility(View.VISIBLE);
            searchImage.setVisibility(View.VISIBLE);
            toggle.setDrawerIndicatorEnabled(false);
            toggle.setHomeAsUpIndicator(R.drawable.ic_new_hamburger_menu);
        }
        if(tv_header_title!=null)
        {
            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText("Sales List");
            tv_header_title.setTextColor(getResources().getColor(R.color.white));
        }
        InitUI(view);

        return view;

    }

    private void InitUI(View view) {

        ss_swipeRefresh = view.findViewById(R.id.proc_swipeRefresh);
        ss_recyclerView = view.findViewById(R.id.proc_recyclerView);
        ss_noResults = view.findViewById(R.id.proc_noResults);

        ss_swipeRefresh.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.CYAN);
        ss_swipeRefresh.setOnRefreshListener(this);

        isLoadmore = false;




        if (CommonMethods.getstringvaluefromkey(getActivity(), "SalesStockLeads").equalsIgnoreCase("true")) {
            try {
                i = Integer.parseInt(ProcurementDetails.position);
            } catch (Exception e) {
                i = 0;
                // e.printStackTrace();
            }
        } else {
            i = 0;
        }

        if (CommonMethods.isInternetWorking(getActivity())) {
            prepareSalesStockRequest();
        } else {
            CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);
        }


        onCallbackEvents();
    }

    private void onCallbackEvents() {

        ((MainActivity) getActivity()).setPFragmentSortListener(new MainActivity.PFragmentSortListener() {

            @Override
            public void onSortPWeb() {
                if (CommonMethods.getstringvaluefromkey(getActivity(), "SalesStockLeads").equalsIgnoreCase("false")) {
                    i = 0;
                }
                restoreInputValues();

                if (CommonMethods.isInternetWorking(getActivity())) {
                    prepareSalesStockRequest();
                } else {
                    CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);
                }

            }
        });

/*        ((MainActivity) getActivity()).SSupdateSearchSales(new SSSearchSales() {
            @Override
            public void onSearchSales(String query) {
                searchQuery = query;
                if (mLeadsDatumList.isEmpty()) {
                    restoreInputValues();
                    i = 0;

                    if (CommonMethods.isInternetWorking(getActivity())) {
                        prepareSalesStockRequest();
                    } else {
                        CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);
                    }

                }

                madapter.filter(query);

               *//* if (madapter != null) {
                    madapter.filter(query);
                }*//*
            }
        });*/

        ((MainActivity) getActivity()).setSSLazyloaderSalesStocksListener(new SSLazyloaderSalesStocks() {
            @Override
            public void Salesloadmore(int ItemCount) {


                counts = counts + 1;
                double maxPageNumber = Math.ceil((double) (salesStocklistTotalCount) / (double) pageItem);
                if (counts > maxPageNumber) {
                    return;
                }
                page_no = Integer.toString(counts);

                isLoadmore = true;


                if (CommonMethods.isInternetWorking(getActivity())) {
                    prepareSalesStockRequest();
                } else {
                    CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);
                }

            }

            @Override
            public void SalesUpdatecount(int count) {
                //  Log.e("updatecount ", "size " + count);
                if (count <= 3) {
                    // mLeadsDatumList.clear();
                    if (CommonMethods.getstringvaluefromkey(getActivity(), "SalesStockLeads").equalsIgnoreCase("false")) {
                        i = 0;
                    }
                    restoreInputValues();

                    if (CommonMethods.isInternetWorking(getActivity())) {
                        prepareSalesStockRequest();
                    } else {
                        CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);
                    }

                }
            }
        });

        //Apply Filter

        ((MainActivity) getActivity()).setSSFilterApplyFilterListener(new SSFilterApply() {
            @Override
            public void applySSFilter(String typeLead) {
                //Posted Date
                if (CommonMethods.getstringvaluefromkey(getActivity(), "SalesStockLeads").equalsIgnoreCase("false")) {
                    i = 0;
                }
                restoreInputValues();


                postedfollDateHashMap = SSFilterSaveInstance.getInstance().getSavedatahashmap();
                leadstatusJsonArray = SSFilterSaveInstance.getInstance().getStatusarray();

                if (CommonMethods.isInternetWorking(getActivity())) {
                    prepareSalesStockRequest();
                } else {
                    CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);
                }

            }
        });

    }



    private void attachtoAdapter(List<SalesStockResData> mlist) {
        madapter = new SalesStockAllAdapter(getContext(), getActivity(), mlist);
        ss_recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getContext());
        ss_recyclerView.setLayoutManager(mLinearLayoutManager);
        ss_recyclerView.setAdapter(madapter);

      //  ss_recyclerView.scrollToPosition(i);

    }

    private void restoreInputValues() {
        page_no = "1";
        counts = 1;
        salesStocklistTotalCount = 0;
        isLoadmore = false;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        searchImage.setImageResource(R.drawable.search);
        // menu.clear();
        MenuItem asm_mail = menu.findItem(R.id.asm_mail);

        //new ASM
        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase("dealer")) {
            asm_mail.setVisible(false);
        }else if(CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_DEALER_CRE) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_SALES) ||
                CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USER_TYPE_PROCUREMENT)){
            asm_mail.setVisible(false);
        }else{
            asm_mail.setVisible(true);
        }
        MenuItem asmHome = menu.findItem(R.id.asmHome);
        MenuItem add_image = menu.findItem(R.id.add_image);
        MenuItem share = menu.findItem(R.id.share);
        MenuItem delete_fea = menu.findItem(R.id.delete_fea);
        MenuItem stock_fil_clear = menu.findItem(R.id.stock_fil_clear);
        MenuItem notification_bell = menu.findItem(R.id.notification_bell);
        MenuItem notification_cancel = menu.findItem(R.id.notification_cancel);

        asmHome.setVisible(false);
        add_image.setVisible(false);
        share.setVisible(false);
        delete_fea.setVisible(false);
        stock_fil_clear.setVisible(false);
        notification_bell.setVisible(false);
        notification_cancel.setVisible(false);



        asm_mail.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // Toast.makeText(getActivity(),"total stocks sending email...",Toast.LENGTH_LONG).show();
                if (CommonMethods.isInternetWorking(getActivity())) {


                    String isEmail="";

                    /*SSCustomWhere where=new SSCustomWhere();
                    where.setColumn("warranty_type");
                    where.setOperator("=");
                    where.setValue("false");
                    ArrayList<SSCustomWhere> whereArrayList=new ArrayList<>();
                    whereArrayList.add(where);
                    salesRequest.setWhere(whereArrayList);*/

                    SSWhereIn email =new SSWhereIn();
                    ArrayList<String> emailArray = new ArrayList<String>();
                    emailArray.add("true");
                    email.setColumn("isEmail");
                    email.setValues(emailArray);
                    email.setOperator("=");


                    for(int i =0;i<mwhereinList.size();i++){

                        if(mwhereinList.get(i).getColumn().equalsIgnoreCase("isEmail")){
                            isEmail="isEmail";
                        }
                    }

                    if(isEmail.equalsIgnoreCase("")) {
                        mwhereinList.add(email);
                        salesRequest.setWhereIn(mwhereinList);
                    }


                    // custom dialog
                    Dialog dialog = new Dialog(activity);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setGravity(Gravity.TOP|Gravity.RIGHT);
                    dialog.setContentView(R.layout.common_cus_dialog);
                    TextView Message, Message2;
                    Button cancel, Confirm;
                    Message = dialog.findViewById(R.id.Message);
                    Message2 = dialog.findViewById(R.id.Message2);
                    cancel = dialog.findViewById(R.id.cancel);
                    Confirm = dialog.findViewById(R.id.Confirm);
                    Message.setText(GlobalText.are_you_sure_to_send_mail);
                    ///Message2.setText("Go ahead and Submit ?");
                    Confirm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            sendEmailsSalesStockDetails(getActivity(),salesRequest);

                            dialog.dismiss();
                        }
                    });
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                } else {
                    CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);

                }
                return true;
            }
        });
    }


    @Override
    public void onRefresh() {

        restoreInputValues();
        i = 0;
        ss_swipeRefresh.setRefreshing(true);

        if (CommonMethods.isInternetWorking(getActivity())) {
            prepareSalesStockRequest();
        } else {
            CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);
        }

    }

    public static void onSerarchResultUpdate(String query) {
        searchVal = query;

        if (madapter == null) {

        } else {
            madapter.filter(query);
        }
        //System.out.println("Search Values>>>>>>>>>> "+query);

    }

    private void prepareSalesStockRequest() {

        Log.i(TAG, "prepareSalesStockRequest: ");


        try {
            MainActivity.searchClose();
        }catch (Exception e){
            e.printStackTrace();
        }

        salesRequest = new SalesStockReq();

        ss_swipeRefresh.setRefreshing(false);
        mwhereinList = new ArrayList<>();
        MfcWhereList = new ArrayList<>();
        salesWhereList = new ArrayList<>();


        salesRequest.setPageItems(Integer.toString(pageItem));
        salesRequest.setPage(page_no);

        sortbySalesLeads(salesRequest);

        //salesRequest.setOrderByMultipleFields("sold_date desc,selling_price desc");

        //dealer_code
        ArrayList<String> code = new ArrayList<String>();
        SSWhereIn wherein = new SSWhereIn();
        wherein.setColumn("dealer_code");
        code.add(CommonMethods.getstringvaluefromkey(getActivity(), "dealer_code"));
        wherein.setValues(code);
        wherein.setOperator("=");
        mwhereinList.add(wherein);
        salesRequest.setWhereIn(mwhereinList);




        //posted followed
        setPostedFollowupDateInfo(salesRequest);

       /* salesRequest.setWhere(salesWhereList);
*/

        //warranty certified stage

        if (CommonMethods.getstringvaluefromkey(getActivity(), "SalesWarranty").equalsIgnoreCase("SalesWarranty")) {

        }else {
            ArrayList<String> warranty = new ArrayList<String>();
            if (leadstatusJsonArray.length() != 0) {
                for (int i = 0; i < leadstatusJsonArray.length(); i++) {
                    try {
                        warranty.add(leadstatusJsonArray.getString(i));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                SSWhereIn wherein2 = new SSWhereIn();
                wherein2.setColumn("warranty_type");
                wherein2.setValues(warranty);
                wherein2.setOperator("=");
                mwhereinList.add(wherein2);
                salesRequest.setWhereIn(mwhereinList);
            }
        }


        //Source MFC
        if (CommonMethods.getstringvaluefromkey(getActivity(), "salesmfc").equalsIgnoreCase("mfc")) {

            SSCustomWhere where = new SSCustomWhere();
            where.setColumn("source");
            where.setOperator("=");
            where.setValue("MFC");
            salesWhereList.add(where);
            //salesRequest.setWhere(salesWhereList);
        }

        //Certified MFC
        if (CommonMethods.getstringvaluefromkey(getActivity(), "sales_certified").equalsIgnoreCase("certified")) {

            SSCustomWhere where = new SSCustomWhere();
            where.setColumn("certified");
            where.setOperator("=");
            where.setValue("1");
            salesWhereList.add(where);
            //salesRequest.setWhere(salesWhereList);
        }

        if (!CommonMethods.getstringvaluefromkey(getActivity(), "sales_startpricedata").equalsIgnoreCase("")) {

            String price1=CommonMethods.getstringvaluefromkey(getActivity(), "sales_startpricedata");
            String price2=CommonMethods.getstringvaluefromkey(getActivity(), "sales_endpricedata");

            SSCustomWhere selling_price = new SSCustomWhere();
            selling_price.setColumn("selling_price");
            selling_price.setOperator(">=");
            selling_price.setValue(price1);
            salesWhereList.add(selling_price);

            SSCustomWhere selling_price1 = new SSCustomWhere();
            selling_price1.setColumn("selling_price");
            selling_price1.setOperator("<=");
            selling_price1.setValue(price2);
            salesWhereList.add(selling_price1);
        }

        salesRequest.setWhere(salesWhereList);


        sendSalesStockDetails(getContext(), salesRequest);

    }

    private void sendSalesStockDetails(final Context mContext, SalesStockReq request) {

        SpinnerManager.showSpinner(mContext);

        SalesStockService.postSalesStock(mContext, request, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);

                Response<SalesStockAllResponse> mRes = (Response<SalesStockAllResponse>) obj;
                SalesStockAllResponse mData = mRes.body();

                salesStocklistTotalCount = mData.getTotal();

                List<SalesStockResData> mlist = mData.getData();

                //Toast.makeText(mContext, "success", Toast.LENGTH_LONG).show();

                if (isLoadmore) {
                    madapter.notifyItemInserted(mLeadsDatumList.size() - 1);
                    mLeadsDatumList.addAll(mlist);
                    madapter.notifyDataSetChanged();
                    madapter.searchDataAdd();
                } else {
                    mLeadsDatumList = new ArrayList<>();
                    mLeadsDatumList.addAll(mlist);
                    attachtoAdapter(mLeadsDatumList);
                }
                isLeadsAvailable(salesStocklistTotalCount);
            }

            @Override
            public void OnFailure(Throwable t) {
                SpinnerManager.hideSpinner(mContext);
                t.printStackTrace();
            }
        });

    }


    private void sendEmailsSalesStockDetails(final Context mContext, SalesStockReq request) {

        SpinnerManager.showSpinner(mContext);

        SalesStockService.sendEmailSalesStock(mContext, request, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(mContext);

                Response<String> mRes = (Response<String>) obj;
                String mData = mRes.body();
                CommonMethods.alertMessage(getActivity(),"Mail sent"+"\n"+mData.toString());

            }

            @Override
            public void OnFailure(Throwable t) {
                SpinnerManager.hideSpinner(mContext);
                t.printStackTrace();
            }
        });

    }

    private void isLeadsAvailable(int TotalCount) {
        if (TotalCount == 0) {
            ss_noResults.setVisibility(View.VISIBLE);
            ss_swipeRefresh.setVisibility(View.GONE);
        } else {
            ss_noResults.setVisibility(View.GONE);
            ss_swipeRefresh.setVisibility(View.VISIBLE);
        }
    }

    private void setPostedFollowupDateInfo(SalesStockReq salesRequest) {

        postedfollDateHashMap = SSFilterSaveInstance.getInstance().getSavedatahashmap();
        String postdateinfo = "";

        if (postedfollDateHashMap.get(SSConstantsSection.LEAD_POST_TODAY) != null) {
            postdateinfo = postedfollDateHashMap.get(SSConstantsSection.LEAD_POST_TODAY);
        }
        if (postedfollDateHashMap.get(SSConstantsSection.LEAD_POST_YESTER) != null) {
            postdateinfo = postedfollDateHashMap.get(SSConstantsSection.LEAD_POST_YESTER);
        }
        if (postedfollDateHashMap.get(SSConstantsSection.LEAD_POST_7DAYS) != null) {
            postdateinfo = postedfollDateHashMap.get(SSConstantsSection.LEAD_POST_7DAYS);
        }
        if (postedfollDateHashMap.get(SSConstantsSection.LEAD_POST_15DAYS) != null) {
            postdateinfo = postedfollDateHashMap.get(SSConstantsSection.LEAD_POST_15DAYS);
        }
        if (postedfollDateHashMap.get(SSConstantsSection.LEAD_POST_CUSTDATE) != null) {
            postdateinfo = postedfollDateHashMap.get(SSConstantsSection.LEAD_POST_CUSTDATE);
        }

        SSCustomWhere postDateCustomWhereStart = new SSCustomWhere();
        SSCustomWhere postDateCustomWhereEnd = new SSCustomWhere();

            if (postdateinfo != null && postdateinfo.contains("@")) {
                //split the date and atach to query
                String[] createddates = postdateinfo.split("@");

                try {
                    postDateCustomWhereStart.setColumn("date_of_booking");
                    postDateCustomWhereEnd.setColumn("date_of_booking");

                    if (postedfollDateHashMap.get(SSConstantsSection.LEAD_POST_CUSTDATE) != null) {
                        postDateCustomWhereStart.setOperator(">=");
                        postDateCustomWhereEnd.setOperator("<=");

                    } else {
                        postDateCustomWhereStart.setOperator(">=");
                        postDateCustomWhereEnd.setOperator("<");
                    }

                    postDateCustomWhereStart.setValue(createddates[0]);
                    postDateCustomWhereEnd.setValue(createddates[1]);
                    salesWhereList.add(postDateCustomWhereStart);
                    salesWhereList.add(postDateCustomWhereEnd);

                    salesRequest.setWhere(salesWhereList);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        String followdateinfo = "";

        if (postedfollDateHashMap.get(SSConstantsSection.LEAD_FOLUP_TMRW) != null) {
            followdateinfo = postedfollDateHashMap.get(SSConstantsSection.LEAD_FOLUP_TMRW);
        }
        if (postedfollDateHashMap.get(SSConstantsSection.LEAD_FOLUP_TODAY) != null) {
            followdateinfo = postedfollDateHashMap.get(SSConstantsSection.LEAD_FOLUP_TODAY);
        }
        if (postedfollDateHashMap.get(SSConstantsSection.LEAD_FOLUP_YESTER) != null) {
            followdateinfo = postedfollDateHashMap.get(SSConstantsSection.LEAD_FOLUP_YESTER);
        }
        if (postedfollDateHashMap.get(SSConstantsSection.LEAD_FOLUP_7DAYS) != null) {
            followdateinfo = postedfollDateHashMap.get(SSConstantsSection.LEAD_FOLUP_7DAYS);
        }
        if (postedfollDateHashMap.get(SSConstantsSection.LEAD_FOLUP_CUSTDATE) != null) {
            followdateinfo = postedfollDateHashMap.get(SSConstantsSection.LEAD_FOLUP_CUSTDATE);
        }

        SSCustomWhere followDateCustomWhereStart = new SSCustomWhere();
        SSCustomWhere followDateCustomWhereEnd = new SSCustomWhere();
        if (followdateinfo != null && followdateinfo.contains("@")) {

            String[] follow = followdateinfo.split("@");
            try {
                followDateCustomWhereStart.setColumn("sold_date");
                followDateCustomWhereEnd.setColumn("sold_date");

                if (postedfollDateHashMap.get(SSConstantsSection.LEAD_FOLUP_TMRW) != null) {
                    followDateCustomWhereStart.setOperator(">");
                    followDateCustomWhereEnd.setOperator("<=");
                    followDateCustomWhereStart.setValue(follow[1]);
                    followDateCustomWhereEnd.setValue(follow[0]);

                }
                if (postedfollDateHashMap.get(SSConstantsSection.LEAD_FOLUP_TODAY) != null || postedfollDateHashMap.get(SSConstantsSection.LEAD_FOLUP_YESTER) != null
                        || postedfollDateHashMap.get(SSConstantsSection.LEAD_FOLUP_7DAYS) != null) {
                    followDateCustomWhereStart.setOperator(">=");
                    followDateCustomWhereEnd.setOperator("<");
                    followDateCustomWhereStart.setValue(follow[0]);
                    followDateCustomWhereEnd.setValue(follow[1]);
                }
                if (postedfollDateHashMap.get(SSConstantsSection.LEAD_FOLUP_CUSTDATE) != null) {
                    followDateCustomWhereStart.setOperator(">=");
                    followDateCustomWhereEnd.setOperator("<=");
                    followDateCustomWhereStart.setValue(follow[0]);
                    followDateCustomWhereEnd.setValue(follow[1]);
                }

                salesWhereList.add(followDateCustomWhereStart);
                salesWhereList.add(followDateCustomWhereEnd);

                salesRequest.setWhere(salesWhereList);


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void sortbySalesLeads(SalesStockReq salesRequest) {

        if (SSSortInstanceLeadSection.getInstance().getSortby().equals("soldout_date_l_o")) {
            salesRequest.setOrderBy("sold_date");
            salesRequest.setOrderByReverse(true);
        } else if (SSSortInstanceLeadSection.getInstance().getSortby().equals("soldout_date_o_l")) {
            salesRequest.setOrderBy("sold_date");
            salesRequest.setOrderByReverse(false);
        } else if (SSSortInstanceLeadSection.getInstance().getSortby().equals("sellingprice_l_o")) {
            salesRequest.setOrderBy("selling_price");
            salesRequest.setOrderByReverse(true);
        } else if (SSSortInstanceLeadSection.getInstance().getSortby().equals("sellingprice_o_l")) {
            salesRequest.setOrderBy("selling_price");
            salesRequest.setOrderByReverse(false);
        }else if (SSSortInstanceLeadSection.getInstance().getSortby().equals("year_1_o")) {
            salesRequest.setOrderBy("model_year");
            salesRequest.setOrderByReverse(true);
        }else if (SSSortInstanceLeadSection.getInstance().getSortby().equals("year_o_l")) {
            salesRequest.setOrderBy("model_year");
            salesRequest.setOrderByReverse(false);
        } else {
            salesRequest.setOrderBy("sold_date");
            salesRequest.setOrderByReverse(false);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        //new ASM
        if(CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(USERTYPE_ASM))
        {
            toggle.setDrawerIndicatorEnabled(false);
            toggle.setHomeAsUpIndicator(R.drawable.ic_new_hamburger_menu);
            navigation.setVisibility(View.GONE);
            Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_SALES,CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));

        }
        if (tv_header_title != null) {
            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText("Sales Lists");
        }


        if(CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(USERTYPE_ASM))
        {
            linearLayoutVisible.setVisibility(View.VISIBLE);
            searchicon.setVisibility(View.VISIBLE);
            searchImage.setVisibility(View.VISIBLE);
            toolbar.setBackgroundColor(getResources().getColor(R.color.black));
            if (tv_header_title != null) {
                tv_header_title.setVisibility(View.VISIBLE);
                tv_header_title.setText("Sales Lists");
            }


        }



        if (CommonMethods.getstringvaluefromkey(getActivity(), "user_type").equalsIgnoreCase("dealer")) {
        }else{
            Application.getInstance().trackScreenView(getActivity(),GlobalText.asm_total_sales_listing);
        }

    }

}
