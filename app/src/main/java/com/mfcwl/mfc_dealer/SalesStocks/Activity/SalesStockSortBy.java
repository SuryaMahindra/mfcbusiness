package com.mfcwl.mfc_dealer.SalesStocks.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Procurement.ProcInterface.PSortedL;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.SalesStocks.Instances.SSSortInstanceLeadSection;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

public class SalesStockSortBy extends Dialog implements View.OnClickListener {

    private Context mContext;
    private Activity mActivity;

    private TextView leads_posted_date_l_to_o, leads_followup_date_l_to_o,year;
    private ImageView posteddateimg, folowimg,yearArrow;
    private RelativeLayout mRelativeLayoutLeadPostedDate, mRelativeLayoutLeadFollowupDate,yearClick;
    private Button mApply;

    private boolean postedFlag = false;
    private boolean followFlag = false;
    private boolean yearFlag = false;
    private String leadsortbystatus = "";

    private PSortedL mSortedbyLead;

    public SalesStockSortBy(@NonNull Context mContext, Activity mActivity) {
        super(mContext);
        this.mContext = mContext;
        this.mActivity = mActivity;
    }
    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.salesstock_sort_dialog);
        Log.i(TAG, "onCreate: ");
        mSortedbyLead = (PSortedL) mActivity;
        InitUI();
    }

    private void InitUI() {
        leads_posted_date_l_to_o = findViewById(R.id.leads_posted_date_l_to_o);
        leads_followup_date_l_to_o = findViewById(R.id.leads_followup_date_l_to_o);
        year = findViewById(R.id.year);

        mApply = findViewById(R.id.stock_sort_apply);

        posteddateimg = findViewById(R.id.posteddateimg);
        folowimg = findViewById(R.id.folowimg);
        yearArrow = findViewById(R.id.yearArrow);

        mRelativeLayoutLeadPostedDate = findViewById(R.id.lsrl1);
        mRelativeLayoutLeadFollowupDate = findViewById(R.id.lsrl2);
        yearClick = findViewById(R.id.lsrl3);

        posteddateimg.setVisibility(View.VISIBLE);
        posteddateimg.animate().rotation(180).setDuration(180).start();
        folowimg.setVisibility(View.INVISIBLE);
        yearArrow.setVisibility(View.INVISIBLE);

        mRelativeLayoutLeadPostedDate.setOnClickListener(this);
        mRelativeLayoutLeadFollowupDate.setOnClickListener(this);
        yearClick.setOnClickListener(this);
        mApply.setOnClickListener(this);

        isSortingApplied();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lsrl1:
                posteddateimg.setVisibility(View.VISIBLE);
                folowimg.setVisibility(View.INVISIBLE);
                yearArrow.setVisibility(View.INVISIBLE);
                if (postedFlag) {
                    postedFlag = false;
                    leadsortbystatus = "soldout_date_l_o";
                    posteddateimg.animate().rotation(180).setDuration(180).start();
                    leads_posted_date_l_to_o.setText("Soldout date(Latest to Oldest)");

                } else{
                    postedFlag = true;
                    leadsortbystatus = "soldout_date_o_l";
                    posteddateimg.animate().rotation(0).setDuration(180).start();
                    leads_posted_date_l_to_o.setText("Soldout date(Oldest to Latest)");

                }
                break;
            case R.id.lsrl2:
                folowimg.setVisibility(View.VISIBLE);
                posteddateimg.setVisibility(View.INVISIBLE);
                yearArrow.setVisibility(View.INVISIBLE);
                if (followFlag) {
                    followFlag = false;
                    leadsortbystatus = "sellingprice_l_o";
                    folowimg.animate().rotation(180).setDuration(180).start();
                    leads_followup_date_l_to_o.setText("Selling price(High to low)");
                } else {
                    followFlag = true;
                    leadsortbystatus = "sellingprice_o_l";
                    folowimg.animate().rotation(0).setDuration(180).start();
                    leads_followup_date_l_to_o.setText("Selling price(Low to High)");

                }
                break;
                case R.id.lsrl3:

                folowimg.setVisibility(View.INVISIBLE);
                posteddateimg.setVisibility(View.INVISIBLE);
                yearArrow.setVisibility(View.VISIBLE);

                if (yearFlag) {
                    yearFlag = false;
                    leadsortbystatus = "year_1_o";
                    yearArrow.animate().rotation(180).setDuration(180).start();
                    year.setText("Year(Latest to Oldest)");
                } else {
                    yearFlag = true;
                    leadsortbystatus = "year_o_l";
                    yearArrow.animate().rotation(0).setDuration(180).start();
                    year.setText("Year(Oldest to Latest)");

                }

                break;

            case R.id.stock_sort_apply:
                dismiss();
                SSSortInstanceLeadSection.getInstance().setSortby(leadsortbystatus);
                mSortedbyLead.onPSortSelection();

                if (CommonMethods.getstringvaluefromkey(mActivity, "user_type").equalsIgnoreCase("dealer")) {
                }else{
                    Application.getInstance().trackScreenView(mActivity,GlobalText.asm_sales_sortby);
                }



                break;
            default:
                break;
        }
    }

    private void isSortingApplied() {

        if (SSSortInstanceLeadSection.getInstance().getSortby().equals("soldout_date_l_o")) {
            posteddateimg.setVisibility(View.VISIBLE);
            yearArrow.setVisibility(View.INVISIBLE);
            folowimg.setVisibility(View.INVISIBLE);
            posteddateimg.animate().rotation(180).setDuration(180).start();
            leads_posted_date_l_to_o.setText("Soldout date(Latest to Oldest)");
            leadsortbystatus = "soldout_date_l_o";
        } else if (SSSortInstanceLeadSection.getInstance().getSortby().equals("soldout_date_o_l")) {
            posteddateimg.setVisibility(View.VISIBLE);
            yearArrow.setVisibility(View.INVISIBLE);
            folowimg.setVisibility(View.INVISIBLE);
            posteddateimg.animate().rotation(0).setDuration(180).start();
            leads_posted_date_l_to_o.setText("Soldout date(Oldest to Latest)");
            leadsortbystatus = "soldout_date_o_l";
        } else if (SSSortInstanceLeadSection.getInstance().getSortby().equals("sellingprice_l_o")) {
            posteddateimg.setVisibility(View.INVISIBLE);
            yearArrow.setVisibility(View.INVISIBLE);
            folowimg.setVisibility(View.VISIBLE);
            folowimg.animate().rotation(180).setDuration(180).start();
            leads_followup_date_l_to_o.setText("Selling price(High to Low)");
            leadsortbystatus = "sellingprice_l_o";
        } else if (SSSortInstanceLeadSection.getInstance().getSortby().equals("sellingprice_o_l")) {
            posteddateimg.setVisibility(View.INVISIBLE);
            yearArrow.setVisibility(View.INVISIBLE);
            folowimg.setVisibility(View.VISIBLE);
            folowimg.animate().rotation(0).setDuration(180).start();
            leads_followup_date_l_to_o.setText("Selling price(Low to High)");
            leadsortbystatus = "sellingprice_o_l";
        } else if (SSSortInstanceLeadSection.getInstance().getSortby().equals("year_1_o")) {
            posteddateimg.setVisibility(View.INVISIBLE);
            folowimg.setVisibility(View.INVISIBLE);
            yearArrow.setVisibility(View.VISIBLE);

            yearArrow.animate().rotation(180).setDuration(180).start();

            year.setText("Year(Latest to Oldest)");
            leadsortbystatus = "year_l_o";

        } else if (SSSortInstanceLeadSection.getInstance().getSortby().equals("year_o_l")) {
            posteddateimg.setVisibility(View.INVISIBLE);
            folowimg.setVisibility(View.INVISIBLE);
            yearArrow.setVisibility(View.VISIBLE);

            yearArrow.animate().rotation(0).setDuration(180).start();
            year.setText("Year(Oldest to Latest)");
            leadsortbystatus = "year_o_l";
        }
    }


}
