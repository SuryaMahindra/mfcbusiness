package com.mfcwl.mfc_dealer.SalesStocks.SalesModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SalesStockReq {

    @SerializedName("Page")
    @Expose
    private String page;
    @SerializedName("PageItems")
    @Expose
    private String pageItems;
    @SerializedName("order_by_multiple_fields")
    @Expose
    private String orderByMultipleFields;
    @SerializedName("WhereIn")
    @Expose
    private List<SSWhereIn> whereIn = null;
    @SerializedName("where")
    @Expose
    private List<SSCustomWhere> where = null;
    @SerializedName("DateFrom")
    @Expose
    private String dateFrom;
    @SerializedName("DateTo")
    @Expose
    private String dateTo;
    @SerializedName("DateField")
    @Expose
    private String dateField;

    @SerializedName("OrderBy")
    @Expose
    private String orderBy;
    @SerializedName("OrderByReverse")
    @Expose
    private Boolean orderByReverse;

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public Boolean getOrderByReverse() {
        return orderByReverse;
    }

    public void setOrderByReverse(Boolean orderByReverse) {
        this.orderByReverse = orderByReverse;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getPageItems() {
        return pageItems;
    }

    public void setPageItems(String pageItems) {
        this.pageItems = pageItems;
    }

    public String getOrderByMultipleFields() {
        return orderByMultipleFields;
    }

    public void setOrderByMultipleFields(String orderByMultipleFields) {
        this.orderByMultipleFields = orderByMultipleFields;
    }

    public List<SSWhereIn> getWhereIn() {
        return whereIn;
    }

    public void setWhereIn(List<SSWhereIn> whereIn) {
        this.whereIn = whereIn;
    }

    public List<SSCustomWhere> getWhere() {
        return where;
    }

    public void setWhere(List<SSCustomWhere> where) {
        this.where = where;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getDateField() {
        return dateField;
    }

    public void setDateField(String dateField) {
        this.dateField = dateField;
    }

}
