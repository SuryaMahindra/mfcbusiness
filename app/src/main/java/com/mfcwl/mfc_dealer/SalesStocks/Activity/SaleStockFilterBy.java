package com.mfcwl.mfc_dealer.SalesStocks.Activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.TextView;

import com.appyvet.materialrangebar.RangeBar;
import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Model.LeadstatusModel;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.SalesStocks.Adapter.SalesStatusAdapter;
import com.mfcwl.mfc_dealer.SalesStocks.Instances.SSConstantsSection;
import com.mfcwl.mfc_dealer.SalesStocks.Instances.SSFilterSaveInstance;
import com.mfcwl.mfc_dealer.SalesStocks.Instances.SalesCalendarFilterDialog;
import com.mfcwl.mfc_dealer.SalesStocks.SalesInterface.SalesFilterSelected;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GlobalText;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

public class SaleStockFilterBy extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, SalesFilterSelected {

    private FrameLayout mApplyFrameLayout, mCloseFrameLayout;
    private TextView clearFilter, startprice, endprice;
    private TextView mPostdate, mFollowup, mLeadstatus;
    private TextView mPostcalendardate, mFollowupcalendardate, mPostfromdate, mPosttodate, mFollfromdate, mFolltodate;

    private CardView mCardpostdate, mCardfolldate, mCardleadstatus;

    private CheckBox chposttoday, chpostyester, chpost7days, chpost15days;
    private CheckBox chfollowtmrw, chfollowtoday, chfollowyester, chfollowlast7days;
    private CheckBox chsource;

    private HashMap<String, String> postedfollDateHashMap;

    public static JSONArray leadstatusJsonArray;
    //close option use this values

    SSConstantsSection salesConstantsSection;

    private String typeLead = "";

    private boolean flagpostdate = true, flagfollowup = true, flagstatus = true;
    private String customPostedstartDateString = "", customPostedendDateString = "";
    private String customFollowstartDateString = "", customFollowendDateString = "";

    private SharedPreferences mSharedPreferences = null;

    public Button filter_all_cars, filter_cetified_car;

    public String certifiedStatus="";

    SalesStatusAdapter adapter;

    //price vales send
    public static String startPriceValues = "0";
    public static String endPriceValues = "10000000";
    RangeBar salestwoseekar;
    float start = 0, end = 100;
    int progval = 0;
    String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate: ");
        setContentView(R.layout.sales_stocks_filter);

        Log.i("onCreate", "SaleStockFilterBy=" + CommonMethods.getstringvaluefromkey(this, "Pstatus"));



        InitUI();
    }

    private void InitUI() {
        mSharedPreferences = getSharedPreferences("MFCP", Context.MODE_PRIVATE);
        mApplyFrameLayout = (FrameLayout) findViewById(R.id.mApplyFrameLayout);
        mCloseFrameLayout = (FrameLayout) findViewById(R.id.mCloseFrameLayout);



        clearFilter = (TextView) findViewById(R.id.clearFilter);

        mPostdate = (TextView) findViewById(R.id.postdate);
        mFollowup = (TextView) findViewById(R.id.followup);
        mLeadstatus = (TextView) findViewById(R.id.leadstatus);

        mPostcalendardate = (TextView) findViewById(R.id.postcalendardate);
        mFollowupcalendardate = (TextView) findViewById(R.id.followupcalendardate);
        mFollfromdate = (TextView) findViewById(R.id.fromdatefoll);
        mFolltodate = (TextView) findViewById(R.id.todatefoll);
        mPostfromdate = (TextView) findViewById(R.id.postfromdate);
        mPosttodate = (TextView) findViewById(R.id.posttodate);

        //CardView
        mCardpostdate = (CardView) findViewById(R.id.mCardpostdate);
        mCardfolldate = (CardView) findViewById(R.id.mCardfolldate);
        mCardleadstatus = (CardView) findViewById(R.id.mCardleadstatus);

        startprice = (TextView) findViewById(R.id.startprice);
        endprice = (TextView) findViewById(R.id.endprice);

        cardVisibiltyCheck();

        //CheckBox for Posted Date
        chposttoday = (CheckBox) findViewById(R.id.chposttoday);
        chpostyester = (CheckBox) findViewById(R.id.chpostyester);
        chpost7days = (CheckBox) findViewById(R.id.chpost7days);
        chpost15days = (CheckBox) findViewById(R.id.chpost15days);

        //source asm
        chsource = (CheckBox) findViewById(R.id.chsource);
        filter_all_cars = (Button) findViewById(R.id.filter_all_cars);
        filter_cetified_car = (Button) findViewById(R.id.filter_cetified_car);
        salestwoseekar = (RangeBar) findViewById(R.id.salestwoseekar);


        //CheckBox for Followup Date
        chfollowtmrw = (CheckBox) findViewById(R.id.chfollowtmrw);
        chfollowtoday = (CheckBox) findViewById(R.id.chfollowtoday);
        chfollowyester = (CheckBox) findViewById(R.id.chfollowyester);
        chfollowlast7days = (CheckBox) findViewById(R.id.chfollowlast7days);

        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Initialize HashMap
        postedfollDateHashMap = new HashMap<String, String>();
        leadstatusJsonArray = new JSONArray();

        salesConstantsSection = new SSConstantsSection();

        //old values use close time
        certifiedStatus =CommonMethods.getstringvaluefromkey(SaleStockFilterBy.this, "sales_certified");

        restoreSelection();
        priceRefill();
        CommonMethods.setvalueAgainstKey(this, "SalesWarranty", "");


        clearFilter.setOnClickListener(this);
        mApplyFrameLayout.setOnClickListener(this);
        mCloseFrameLayout.setOnClickListener(this);

        //PostedDate Callback Listener
        chposttoday.setOnCheckedChangeListener(this);
        chpostyester.setOnCheckedChangeListener(this);
        chpost7days.setOnCheckedChangeListener(this);
        chpost15days.setOnCheckedChangeListener(this);

        chsource.setOnCheckedChangeListener(this);
        filter_all_cars.setOnClickListener(this);
        filter_cetified_car.setOnClickListener(this);


        //Followup Callback Listener
        chfollowtmrw.setOnCheckedChangeListener(this);
        chfollowtoday.setOnCheckedChangeListener(this);
        chfollowyester.setOnCheckedChangeListener(this);
        chfollowlast7days.setOnCheckedChangeListener(this);

        //Calendar Callback Listener
        mPostcalendardate.setOnClickListener(this);
        mFollowupcalendardate.setOnClickListener(this);

        mPostdate.setOnClickListener(this);
        mFollowup.setOnClickListener(this);
        mLeadstatus.setOnClickListener(this);

        GridView status_grid = (GridView) findViewById(R.id.status_grid);
        ArrayList dataModels = new ArrayList();

        try {
            for (int i = 0; i < MainActivity.warrantyStatus.size(); i++) {
                String data = MainActivity.warrantyStatus.get(i);
                if (leadstatusJsonArray != null) {
                    String data1 = "";
                    try {
                        for (int j = 0; j < leadstatusJsonArray.length(); j++) {
                            String item = leadstatusJsonArray.get(j).toString();
                            if (data.equalsIgnoreCase(item)) {
                                data1 = data;
                            }
                        }
                        if (data1.equalsIgnoreCase(data)) {
                            dataModels.add(new LeadstatusModel(data, true));
                        } else {
                            dataModels.add(new LeadstatusModel(data, false));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    dataModels.add(new LeadstatusModel(MainActivity.warrantyStatus.get(i).toString(), false));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        adapter = new SalesStatusAdapter(dataModels, getApplicationContext());
        status_grid.setAdapter(adapter);

        status_grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {

                try {
                    LeadstatusModel dataModel = (LeadstatusModel) dataModels.get(position);
                    dataModel.checked = !dataModel.checked;
                    adapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        salestwoseekar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                //code reena
                CommonMethods.setvalueAgainstKey(SaleStockFilterBy.this, "price_valuesval", "true");

                setStartEndPrice(leftPinIndex, rightPinIndex);
            }
        });

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.postdate:
                if (flagpostdate) {
                    mCardpostdate.setVisibility(View.VISIBLE);
                    mPostdate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                    flagpostdate = false;
                } else {
                    mCardpostdate.setVisibility(View.GONE);
                    mPostdate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
                    flagpostdate = true;
                }
                break;
            case R.id.followup:
                if (flagfollowup) {
                    mCardfolldate.setVisibility(View.VISIBLE);
                    mFollowup.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                    flagfollowup = false;
                } else {
                    mCardfolldate.setVisibility(View.GONE);
                    mFollowup.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
                    flagfollowup = true;
                }

                break;
            case R.id.leadstatus:
                if (flagstatus) {
                    mCardleadstatus.setVisibility(View.VISIBLE);
                    mLeadstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                    flagstatus = false;
                } else {
                    mCardleadstatus.setVisibility(View.GONE);
                    mLeadstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
                    flagstatus = true;
                }

                break;

            case R.id.clearFilter:

                //ASM
                CommonMethods.setvalueAgainstKey(SaleStockFilterBy.this, "leadStatus", "");
                CommonMethods.setvalueAgainstKey(SaleStockFilterBy.this, "salesmfc", "");
                CommonMethods.setvalueAgainstKey(this, "sales_certified", "");
                certifiedStatus="";
                CommonMethods.setvalueAgainstKey(this, "sales_startpricedata", "");
                CommonMethods.setvalueAgainstKey(this, "sales_endpricedata", "");

                certified();
                priceRangeReset();

                SharedPreferences.Editor mEditor = mSharedPreferences.edit();

                mEditor.putString(salesConstantsSection.LEAD_SAVE_DATE, "");
                mEditor.putString(salesConstantsSection.LEAD_SAVE_STATUS, "");
                SSFilterSaveInstance.getInstance().setSavedatahashmap(new HashMap<>());
                SSFilterSaveInstance.getInstance().setStatusarray(new JSONArray());
                mEditor.clear();
                mEditor.commit();
                setResult(500);
                finish();
                break;

            case R.id.mApplyFrameLayout:
                Log.i("mApplyFrameLayout", "Pstatus=" + CommonMethods.getstringvaluefromkey(this, "Pstatus"));

                isPostedCheckBox();
                isFollowCheckBox();
                // isStatusCheckBox();
                SSFilterSaveInstance.getInstance().setSavedatahashmap(postedfollDateHashMap);


                SSFilterSaveInstance.getInstance().setStatusarray(leadstatusJsonArray);

                CommonMethods.setvalueAgainstKey(this, "sales_certified", certifiedStatus);


                CommonMethods.setvalueAgainstKey(this, "sales_startpricedata", startPriceValues);
                CommonMethods.setvalueAgainstKey(this, "sales_endpricedata", endPriceValues);


                SaveDevice();
                setResult(500);
                finish();
                break;

            case R.id.mCloseFrameLayout:
                finish();
                break;

            case R.id.postcalendardate:
                mPostcalendardate.setEnabled(false);
                SalesCalendarFilterDialog postcalendarFilterDialog = new SalesCalendarFilterDialog(this, "CustomPostDate");
                postcalendarFilterDialog.setCancelable(false);
                postcalendarFilterDialog.show();
                break;

            case R.id.followupcalendardate:
                mFollowupcalendardate.setEnabled(false);
                SalesCalendarFilterDialog followcalendarFilterDialog = new SalesCalendarFilterDialog(this, "CustomFollowDate");
                followcalendarFilterDialog.setCancelable(false);
                followcalendarFilterDialog.show();
                break;

            case R.id.filter_all_cars:
                certifiedStatus="allcars";
                certified();
                break;

            case R.id.filter_cetified_car:
                certifiedStatus="certified";
                certified();
                break;

            default:
                break;
        }
    }

    private void priceRangeReset() {
        //twoseekbar
        start = 0;
        end = 100;
        salestwoseekar.setRangePinsByValue(start, end);

    }

    private void certified() {


        if (certifiedStatus.equalsIgnoreCase("allcars")) {

            filter_all_cars.setBackgroundResource(R.drawable.my_button);
            filter_all_cars.setTextColor(getResources().getColor(R.color.Black));

            filter_cetified_car.setBackgroundResource(R.drawable.my_buttom_gray_light);
            filter_cetified_car.setTextColor(getResources().getColor(R.color.lgray));

        } else if (certifiedStatus.equalsIgnoreCase("certified")) {

            filter_cetified_car.setBackgroundResource(R.drawable.my_button);
            filter_cetified_car.setTextColor(getResources().getColor(R.color.Black));

            filter_all_cars.setBackgroundResource(R.drawable.my_buttom_gray_light);
            filter_all_cars.setTextColor(getResources().getColor(R.color.lgray));

        } else {

            filter_all_cars.setBackgroundResource(R.drawable.my_button);
            filter_all_cars.setTextColor(getResources().getColor(R.color.Black));

            filter_cetified_car.setBackgroundResource(R.drawable.my_buttom_gray_light);
            filter_cetified_car.setTextColor(getResources().getColor(R.color.lgray));
        }

    }

    private void SaveDevice() {
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();
        Gson gson = new Gson();
        String postfollDate = gson.toJson(postedfollDateHashMap);
        String status = gson.toJson(leadstatusJsonArray);
        mEditor.putString(salesConstantsSection.LEAD_SAVE_DATE, postfollDate);
        mEditor.putString(salesConstantsSection.LEAD_SAVE_STATUS, status);
        mEditor.commit();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.chposttoday || buttonView.getId() == R.id.chpostyester ||
                buttonView.getId() == R.id.chpost7days || buttonView.getId() == R.id.chpost15days) {
            unCheckPostedCheckBox();
        } else if (buttonView.getId() == R.id.chfollowtmrw || buttonView.getId() == R.id.chfollowtoday ||
                buttonView.getId() == R.id.chfollowyester || buttonView.getId() == R.id.chfollowlast7days) {
            unCheckFollowupCheckBox();
        } else if (buttonView.getId() == R.id.chsource) {

        }
        switch (buttonView.getId()) {
            //source
            case R.id.chsource:
                if (isChecked) {
                    chsource.setChecked(true);
                } else {
                    chsource.setChecked(false);
                }
                break;
            //Posted Date
            case R.id.chposttoday:
                if (isChecked) {
                    chposttoday.setChecked(true);
                } else {
                    chposttoday.setChecked(false);
                }
                break;
            case R.id.chpostyester:
                if (isChecked) {
                    chpostyester.setChecked(true);
                } else {
                    chpostyester.setChecked(false);
                }
                break;
            case R.id.chpost7days:
                if (isChecked) {
                    chpost7days.setChecked(true);
                } else {
                    chpost7days.setChecked(false);
                }
                break;
            case R.id.chpost15days:
                if (isChecked) {
                    chpost15days.setChecked(true);
                } else {
                    chpost15days.setChecked(false);
                }
                break;
            //End


            //Followup Date
            case R.id.chfollowtmrw:
                if (isChecked) {
                    chfollowtmrw.setChecked(true);
                } else {
                    chfollowtmrw.setChecked(false);
                }
                break;
            case R.id.chfollowtoday:
                if (isChecked) {
                    chfollowtoday.setChecked(true);
                } else {
                    chfollowtoday.setChecked(false);
                }
                break;
            case R.id.chfollowyester:
                if (isChecked) {
                    chfollowyester.setChecked(true);
                } else {
                    chfollowyester.setChecked(false);
                }
                break;
            case R.id.chfollowlast7days:
                if (isChecked) {
                    chfollowlast7days.setChecked(true);
                } else {
                    chfollowlast7days.setChecked(false);
                }
                break;

        }

    }


    private void unCheckPostedCheckBox() {
        chposttoday.setChecked(false);
        chpostyester.setChecked(false);
        chpost7days.setChecked(false);
        chpost15days.setChecked(false);
        mPostfromdate.setText("");
        mPosttodate.setText("");
    }

    private void unCheckFollowupCheckBox() {
        chfollowtmrw.setChecked(false);
        chfollowtoday.setChecked(false);
        chfollowyester.setChecked(false);
        chfollowlast7days.setChecked(false);
        mFollfromdate.setText("");
        mFolltodate.setText("");
    }


    private void cardVisibiltyCheck() {
        mCardpostdate.setVisibility(View.GONE);
        mCardfolldate.setVisibility(View.GONE);
        mCardleadstatus.setVisibility(View.GONE);
    }


    @Override
    public void onSalesFilterSelection(String startDateString, String endDateString, String dateType) {


        Log.i("coming", "coming....: ");

        mPostcalendardate.setEnabled(true);
        mFollowupcalendardate.setEnabled(true);

        if (startDateString.equals("") || endDateString.equals("")) {
            customPostedstartDateString = startDateString;
            customPostedendDateString = endDateString;
            customFollowstartDateString = startDateString;
            customFollowendDateString = endDateString;
            if (dateType.equals("CustomPostDate")) {
                mPostfromdate.setText("");
                mPosttodate.setText("");
                //postedfollDateHashMap.remove(LeadConstantsSection.LEAD_POST_CUSTDATE);
            } else {
                mFollfromdate.setText("");
                mFolltodate.setText("");
                //postedfollDateHashMap.remove(LeadConstantsSection.LEAD_FOLUP_CUSTDATE);
            }
            return;
        }
        if (dateType.equals("CustomPostDate")) {
            unCheckPostedCheckBox();
            customPostedstartDateString = startDateString;
            customPostedendDateString = endDateString;
            showCustomPostedDate(startDateString, endDateString);
        } else {
            unCheckFollowupCheckBox();
            customFollowstartDateString = startDateString;
            customFollowendDateString = endDateString;
            showCustomFollowupDate(startDateString, endDateString);
        }
    }


    private void showCustomPostedDate(String startDateString, String endDateString) {

        String[] splitStartDate = startDateString.split("-", 10);
        String[] splitEndDate = endDateString.split("-", 10);
        mPostfromdate.setText(splitStartDate[2] + " " + salesConstantsSection.MethodofMonth(splitStartDate[1]) + " " + splitStartDate[0] + " To ");
        mPosttodate.setText(splitEndDate[2] + " " + salesConstantsSection.MethodofMonth(splitEndDate[1]) + " " + splitEndDate[0]);
    }

    private void showCustomFollowupDate(String startDateString, String endDateString) {
        String[] splitStartDate = startDateString.split("-", 10);
        String[] splitEndDate = endDateString.split("-", 10);
        mFollfromdate.setText(splitStartDate[2] + " " + salesConstantsSection.MethodofMonth(splitStartDate[1]) + " " + splitStartDate[0] + " To ");
        mFolltodate.setText(splitEndDate[2] + " " + salesConstantsSection.MethodofMonth(splitEndDate[1]) + " " + splitEndDate[0]);
    }


    private void restoreSelection() {

        if (CommonMethods.getstringvaluefromkey(SaleStockFilterBy.this, "salesmfc").equalsIgnoreCase("mfc")) {
            chsource.setChecked(true);
        } else {
            chsource.setChecked(false);
        }



        certified();

        postedfollDateHashMap = SSFilterSaveInstance.getInstance().getSavedatahashmap();

        if (CommonMethods.getstringvaluefromkey(this, "SalesWarranty")
                .equalsIgnoreCase("SalesWarranty")
        && CommonMethods.getstringvaluefromkey(this, "Pstatus").equalsIgnoreCase("salesstock")
        ) {

        }else {
            leadstatusJsonArray = SSFilterSaveInstance.getInstance().getStatusarray();
        }



        //restore Posted Date
        if (postedfollDateHashMap.get(salesConstantsSection.LEAD_POST_TODAY) != null) {
            mCardpostdate.setVisibility(View.VISIBLE);
            chposttoday.setChecked(true);
        }
        if (postedfollDateHashMap.get(salesConstantsSection.LEAD_POST_YESTER) != null) {
            mCardpostdate.setVisibility(View.VISIBLE);
            chpostyester.setChecked(true);
        }
        if (postedfollDateHashMap.get(salesConstantsSection.LEAD_POST_7DAYS) != null) {
            mCardpostdate.setVisibility(View.VISIBLE);
            chpost7days.setChecked(true);
        }
        if (postedfollDateHashMap.get(salesConstantsSection.LEAD_POST_15DAYS) != null) {
            mCardpostdate.setVisibility(View.VISIBLE);
            chpost15days.setChecked(true);
        }

        if (postedfollDateHashMap.get(salesConstantsSection.LEAD_POST_CUSTDATE) != null) {
            mCardpostdate.setVisibility(View.VISIBLE);
            String[] splitPostDate = postedfollDateHashMap.get(salesConstantsSection.LEAD_POST_CUSTDATE).split("@", 10);
            customPostedstartDateString = splitPostDate[0];
            customPostedendDateString = splitPostDate[1];
            showCustomPostedDate(customPostedstartDateString, customPostedendDateString);
        }

        //restore Follow-Up Date

        if (postedfollDateHashMap.get(salesConstantsSection.LEAD_FOLUP_TMRW) != null) {
            mCardfolldate.setVisibility(View.VISIBLE);
            chfollowtmrw.setChecked(true);
        }
        if (postedfollDateHashMap.get(salesConstantsSection.LEAD_FOLUP_TODAY) != null) {
            mCardfolldate.setVisibility(View.VISIBLE);
            chfollowtoday.setChecked(true);
        }
        if (postedfollDateHashMap.get(salesConstantsSection.LEAD_FOLUP_YESTER) != null) {
            mCardfolldate.setVisibility(View.VISIBLE);
            chfollowyester.setChecked(true);
        }
        if (postedfollDateHashMap.get(salesConstantsSection.LEAD_FOLUP_7DAYS) != null) {
            mCardfolldate.setVisibility(View.VISIBLE);
            chfollowlast7days.setChecked(true);
        }

        if (postedfollDateHashMap.get(salesConstantsSection.LEAD_FOLUP_CUSTDATE) != null) {
            mCardfolldate.setVisibility(View.VISIBLE);
            String[] splitFollowDate = postedfollDateHashMap.get(salesConstantsSection.LEAD_FOLUP_CUSTDATE).split("@", 10);
            customFollowstartDateString = splitFollowDate[0];
            customFollowendDateString = splitFollowDate[1];
            showCustomFollowupDate(customFollowstartDateString, customFollowendDateString);
        }

        //restore Lead Status

        if (leadstatusJsonArray.length() == 0) {
            mCardleadstatus.setVisibility(View.GONE);

        } else {
            mCardleadstatus.setVisibility(View.VISIBLE);

        }
    }

    private void isPostedCheckBox() {

        if (!customPostedstartDateString.equals("") && !customPostedendDateString.equals("")) {
            postedfollDateHashMap.put(salesConstantsSection.LEAD_POST_CUSTDATE, customPostedstartDateString + "@" + customPostedendDateString);
        } else {
            postedfollDateHashMap.remove(salesConstantsSection.LEAD_POST_CUSTDATE);
        }

        if (chposttoday.isChecked()) {
            postedfollDateHashMap.put(salesConstantsSection.LEAD_POST_TODAY, salesConstantsSection.getTodayDate());
            postedfollDateHashMap.remove(salesConstantsSection.LEAD_POST_CUSTDATE);
        } else {
            postedfollDateHashMap.remove(salesConstantsSection.LEAD_POST_TODAY);
        }
        if (chpostyester.isChecked()) {
            postedfollDateHashMap.put(salesConstantsSection.LEAD_POST_YESTER, salesConstantsSection.getYesterdayDate());
            postedfollDateHashMap.remove(salesConstantsSection.LEAD_POST_CUSTDATE);
        } else {
            postedfollDateHashMap.remove(salesConstantsSection.LEAD_POST_YESTER);
        }
        if (chpost7days.isChecked()) {
            postedfollDateHashMap.put(salesConstantsSection.LEAD_POST_7DAYS, salesConstantsSection.get7days());
            postedfollDateHashMap.remove(salesConstantsSection.LEAD_POST_CUSTDATE);
        } else {
            postedfollDateHashMap.remove(salesConstantsSection.LEAD_POST_7DAYS);
        }
        if (chpost15days.isChecked()) {
            postedfollDateHashMap.put(salesConstantsSection.LEAD_POST_15DAYS, salesConstantsSection.get15days());
            postedfollDateHashMap.remove(salesConstantsSection.LEAD_POST_CUSTDATE);
        } else {
            postedfollDateHashMap.remove(salesConstantsSection.LEAD_POST_15DAYS);
        }

        if (chsource.isChecked()) {
            CommonMethods.setvalueAgainstKey(SaleStockFilterBy.this, "salesmfc", "mfc");

        } else {
            CommonMethods.setvalueAgainstKey(SaleStockFilterBy.this, "salesmfc", "");

        }


    }

    private void isFollowCheckBox() {

        // Log.e("isFollowCheckBox ", "start " + customFollowstartDateString);
        //  Log.e("isFollowCheckBox ", "end " + customFollowendDateString);

        if (!customFollowstartDateString.equals("") && !customFollowendDateString.equals("")) {
            postedfollDateHashMap.put(salesConstantsSection.LEAD_FOLUP_CUSTDATE, customFollowstartDateString + "@" + customFollowendDateString);
        } else {
            postedfollDateHashMap.remove(salesConstantsSection.LEAD_FOLUP_CUSTDATE);
        }


        //Followup Date
        if (chfollowtmrw.isChecked()) {
            postedfollDateHashMap.put(salesConstantsSection.LEAD_FOLUP_TMRW, salesConstantsSection.getFollowTomorrowDate());
            postedfollDateHashMap.remove(salesConstantsSection.LEAD_FOLUP_CUSTDATE);
        } else {
            postedfollDateHashMap.remove(salesConstantsSection.LEAD_FOLUP_TMRW);
        }
        if (chfollowtoday.isChecked()) {
            postedfollDateHashMap.put(salesConstantsSection.LEAD_FOLUP_TODAY, salesConstantsSection.getFollowtodayDate());
            postedfollDateHashMap.remove(salesConstantsSection.LEAD_FOLUP_CUSTDATE);
        } else {
            postedfollDateHashMap.remove(salesConstantsSection.LEAD_FOLUP_TODAY);
        }

        if (chfollowyester.isChecked()) {
            postedfollDateHashMap.put(salesConstantsSection.LEAD_FOLUP_YESTER, salesConstantsSection.getFollowyesterDate());
            postedfollDateHashMap.remove(salesConstantsSection.LEAD_FOLUP_CUSTDATE);
        } else {
            postedfollDateHashMap.remove(salesConstantsSection.LEAD_FOLUP_YESTER);
        }

        if (chfollowlast7days.isChecked()) {
            postedfollDateHashMap.put(salesConstantsSection.LEAD_FOLUP_7DAYS, salesConstantsSection.getFollowlast7Date());
            postedfollDateHashMap.remove(salesConstantsSection.LEAD_FOLUP_CUSTDATE);
        } else {
            postedfollDateHashMap.remove(salesConstantsSection.LEAD_FOLUP_7DAYS);
        }

    }

    private void priceRefill() {


        salestwoseekar.setRangePinsByValue(start, end);

        if (CommonMethods.getstringvaluefromkey(this, "sales_startpricedata").equalsIgnoreCase("")) {
            CommonMethods.setvalueAgainstKey(this, "sales_startpricedata", startPriceValues);
            priceRangeReset();
        }
        if (CommonMethods.getstringvaluefromkey(this, "sales_endpricedata").equalsIgnoreCase("")) {
            CommonMethods.setvalueAgainstKey(this, "sales_endpricedata", endPriceValues);
            priceRangeReset();
        }

        startPriceValues = CommonMethods.getstringvaluefromkey(this, "sales_startpricedata").toString();
        endPriceValues = CommonMethods.getstringvaluefromkey(this, "sales_endpricedata").toString();


        if (startPriceValues.equalsIgnoreCase("100000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "1L");
            start = 5;
        } else if (startPriceValues.equalsIgnoreCase("200000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "2L");
            start = 10;
        } else if (startPriceValues.equalsIgnoreCase("300000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "3L");
            start = 15;
        } else if (startPriceValues.equalsIgnoreCase("400000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "4L");
            start = 20;
        } else if (startPriceValues.equalsIgnoreCase("500000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "5L");
            start = 25;
        } else if (startPriceValues.equalsIgnoreCase("600000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "6L");
            start = 30;
        } else if (startPriceValues.equalsIgnoreCase("700000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "7L");
            start = 35;
        } else if (startPriceValues.equalsIgnoreCase("800000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "8L");
            start = 40;
        } else if (startPriceValues.equalsIgnoreCase("900000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "9L");
            start = 45;
        } else if (startPriceValues.equalsIgnoreCase("1000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "10L");
            start = 50;
        } else if (startPriceValues.equalsIgnoreCase("2000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "20L");
            start = 55;
        } else if (startPriceValues.equalsIgnoreCase("3000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "30L");
            start = 60;
        } else if (startPriceValues.equalsIgnoreCase("4000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "40L");
            start = 65;
        } else if (startPriceValues.equalsIgnoreCase("5000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "50L");
            start = 70;
        } else if (startPriceValues.equalsIgnoreCase("6000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "60L");
            start = 75;
        } else if (startPriceValues.equalsIgnoreCase("7000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "70L");
            start = 80;
        } else if (startPriceValues.equalsIgnoreCase("8000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "80L");
            start = 85;
        } else if (startPriceValues.equalsIgnoreCase("9000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "90L");
            start = 90;
        } else if (startPriceValues.equalsIgnoreCase("10000000")) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "1 Cr");
            start = 100;
        } else {
            startprice.setText(getResources().getString(R.string.rs) + " " + "0");
            start = 0;
        }

        if (endPriceValues.equalsIgnoreCase("100000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "1L");
            end = 5;
        } else if (endPriceValues.equalsIgnoreCase("200000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "2L");
            end = 10;
        } else if (endPriceValues.equalsIgnoreCase("300000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "3L");
            end = 15;
        } else if (endPriceValues.equalsIgnoreCase("400000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "4L");
            end = 20;
        } else if (endPriceValues.equalsIgnoreCase("500000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "5L");
            end = 25;
        } else if (endPriceValues.equalsIgnoreCase("600000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "6L");
            end = 30;
        } else if (endPriceValues.equalsIgnoreCase("700000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "7L");
            end = 35;
        } else if (endPriceValues.equalsIgnoreCase("800000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "8L");
            end = 40;
        } else if (endPriceValues.equalsIgnoreCase("900000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "9L");
            end = 45;
        } else if (endPriceValues.equalsIgnoreCase("1000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "10L");
            end = 50;
        } else if (endPriceValues.equalsIgnoreCase("2000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "20L");
            end = 55;
        } else if (endPriceValues.equalsIgnoreCase("3000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "30L");
            end = 60;
        } else if (endPriceValues.equalsIgnoreCase("4000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "40L");
            end = 65;
        } else if (endPriceValues.equalsIgnoreCase("5000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "50L");
            end = 70;
        } else if (endPriceValues.equalsIgnoreCase("6000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "60L");
            end = 75;
        } else if (endPriceValues.equalsIgnoreCase("7000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "70L");
            end = 80;
        } else if (endPriceValues.equalsIgnoreCase("8000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "80L");
            end = 85;
        } else if (endPriceValues.equalsIgnoreCase("9000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "90L");
            end = 90;
        } else if (endPriceValues.equalsIgnoreCase("10000000")) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "1 Cr");
            end = 100;
        } else {
            endprice.setText(getResources().getString(R.string.rs) + " " + "0");
            end = 0;
        }

        salestwoseekar.setRangePinsByValue(start, end);


    }


    public void setStartEndPrice(int leftPinIndex, int rightPinIndex) {
        if (leftPinIndex == 0) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "0");
            startPriceValues = "0";
        } else if (leftPinIndex == 1) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "1L");
            startPriceValues = "100000";
        } else if (leftPinIndex == 2) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "2L");
            startPriceValues = "200000";
        } else if (leftPinIndex == 3) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "3L");
            startPriceValues = "300000";
        } else if (leftPinIndex == 4) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "4L");
            startPriceValues = "400000";
        } else if (leftPinIndex == 5) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "5L");
            startPriceValues = "500000";
        } else if (leftPinIndex == 6) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "6L");
            startPriceValues = "600000";
        } else if (leftPinIndex == 7) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "7L");
            startPriceValues = "700000";
        } else if (leftPinIndex == 8) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "8L");
            startPriceValues = "800000";
        } else if (leftPinIndex == 9) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "9L");
            startPriceValues = "900000";
        } else if (leftPinIndex == 10) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "10L");
            startPriceValues = "1000000";
        } else if (leftPinIndex == 12) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "20L");
            startPriceValues = "2000000";
        } else if (leftPinIndex == 13) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "30L"); //change 1300000 to 30000000 reena
            startPriceValues = "3000000";
        } else if (leftPinIndex == 14) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "40L");
            startPriceValues = "4000000";
        } else if (leftPinIndex == 15) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "50L");
            startPriceValues = "5000000";
        } else if (leftPinIndex == 16) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "60L");
            startPriceValues = "6000000";
        } else if (leftPinIndex == 17) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "70L");
            startPriceValues = "7000000";
        } else if (leftPinIndex == 18) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "80L");
            startPriceValues = "8000000";
        } else if (leftPinIndex == 19) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "90L");
            startPriceValues = "9000000";
        } else if (leftPinIndex == 20) {
            startprice.setText(getResources().getString(R.string.rs) + " " + "1 Cr");
            startPriceValues = "10000000";
        }


        if (rightPinIndex == 0) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "0");
            endPriceValues = "0";
        } else if (rightPinIndex == 1) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "1L");
            endPriceValues = "100000";
        } else if (rightPinIndex == 2) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "2L");
            endPriceValues = "200000";
        } else if (rightPinIndex == 3) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "3L");
            endPriceValues = "300000";
        } else if (rightPinIndex == 4) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "4L");
            endPriceValues = "400000";
        } else if (rightPinIndex == 5) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "5L");
            endPriceValues = "500000";
        } else if (rightPinIndex == 6) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "6L");
            endPriceValues = "600000";
        } else if (rightPinIndex == 7) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "7L");
            endPriceValues = "700000";
        } else if (rightPinIndex == 8) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "8L");
            endPriceValues = "800000";
        } else if (rightPinIndex == 9) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "9L");
            endPriceValues = "900000";
        } else if (rightPinIndex == 10) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "10L");
            endPriceValues = "1000000";
        } else if (rightPinIndex == 12) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "20L");
            endPriceValues = "2000000";
        } else if (rightPinIndex == 13) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "30L");
            endPriceValues = "3000000";//
        } else if (rightPinIndex == 14) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "40L");
            endPriceValues = "4000000";
        } else if (rightPinIndex == 15) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "50L");
            endPriceValues = "5000000";
        } else if (rightPinIndex == 16) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "60L");
            endPriceValues = "6000000";
        } else if (rightPinIndex == 17) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "70L");
            endPriceValues = "7000000";
        } else if (rightPinIndex == 18) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "80L");
            endPriceValues = "8000000";
        } else if (rightPinIndex == 19) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "90L");
            endPriceValues = "9000000";
        } else if (rightPinIndex == 20) {
            endprice.setText(getResources().getString(R.string.rs) + " " + "1 Cr");
            endPriceValues = "10000000";
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        //new ASM
        if (CommonMethods.getstringvaluefromkey(this, "user_type").equalsIgnoreCase("dealer")) {
        }else{
            Application.getInstance().trackScreenView(this,GlobalText.asm_sales_filter);
        }

    }

}
