package com.mfcwl.mfc_dealer.SalesStocks.Fragment;

public class SalesStockConstant {

    private static SalesStockConstant mInstance = null;
    
    public static SalesStockConstant getmInstance() {
        return mInstance;
    }

    public static void setmInstance(SalesStockConstant mInstance) {
        SalesStockConstant.mInstance = mInstance;
    }

    public String getWhichStock() {
        return whichStock;
    }

    public void setWhichStock(String whichStock) {
        this.whichStock = whichStock;
    }

    private String whichStock;

    private SalesStockConstant() {
        whichStock = "";

    }

    public static SalesStockConstant getInstance() {
        if (mInstance == null) {
            mInstance = new SalesStockConstant();
        }
        return mInstance;
    }
}
