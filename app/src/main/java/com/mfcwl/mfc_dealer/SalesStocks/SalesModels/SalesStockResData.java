package com.mfcwl.mfc_dealer.SalesStocks.SalesModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalesStockResData {

    @SerializedName("stock_id")
    @Expose
    private Integer stockId;
    @SerializedName("dealer_code")
    @Expose
    private String dealerCode;
    @SerializedName("make")
    @Expose
    private String make;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("variant")
    @Expose
    private String variant;
    @SerializedName("kilometer")
    @Expose
    private Integer kilometer;
    @SerializedName("owner")
    @Expose
    private Integer owner;
    @SerializedName("registration_number")
    @Expose
    private String registrationNumber;
    @SerializedName("selling_price")
    @Expose
    private Integer sellingPrice;
    @SerializedName("certified")
    @Expose
    private Object certified;
    @SerializedName("fuel_type")
    @Expose
    private String fuelType;
    @SerializedName("warranty_type")
    @Expose
    private String warrantyType;
    @SerializedName("warranty_number")
    @Expose
    private String warrantyNumber;
    @SerializedName("warranty_amount")
    @Expose
    private Float warrantyAmount;
    @SerializedName("sold_date")
    @Expose
    private String soldDate;
    @SerializedName("stock_type")
    @Expose
    private String stockType;
    @SerializedName("model_year")
    @Expose
    private Integer modelYear;
    @SerializedName("sold_days")
    @Expose
    private Integer soldDays;
    @SerializedName("photo_count")
    @Expose
    private Integer photoCount;
    @SerializedName("cover_image")
    @Expose
    private Object coverImage;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("sold")
    @Expose
    private Integer sold;
    @SerializedName("date_of_booking")
    @Expose
    private String dateOfBooking;

    public Integer getStockId() {
        return stockId;
    }

    public void setStockId(Integer stockId) {
        this.stockId = stockId;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public Integer getKilometer() {
        return kilometer;
    }

    public void setKilometer(Integer kilometer) {
        this.kilometer = kilometer;
    }

    public Integer getOwner() {
        return owner;
    }

    public void setOwner(Integer owner) {
        this.owner = owner;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public Integer getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(Integer sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public Object getCertified() {
        return certified;
    }

    public void setCertified(Object certified) {
        this.certified = certified;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getWarrantyType() {
        return warrantyType;
    }

    public void setWarrantyType(String warrantyType) {
        this.warrantyType = warrantyType;
    }

    public String getWarrantyNumber() {
        return warrantyNumber;
    }

    public void setWarrantyNumber(String warrantyNumber) {
        this.warrantyNumber = warrantyNumber;
    }

    public Float getWarrantyAmount() {
        return warrantyAmount;
    }

    public void setWarrantyAmount(Float warrantyAmount) {
        this.warrantyAmount = warrantyAmount;
    }

    public String getSoldDate() {
        return soldDate;
    }

    public void setSoldDate(String soldDate) {
        this.soldDate = soldDate;
    }

    public String getStockType() {
        return stockType;
    }

    public void setStockType(String stockType) {
        this.stockType = stockType;
    }

    public Integer getModelYear() {
        return modelYear;
    }

    public void setModelYear(Integer modelYear) {
        this.modelYear = modelYear;
    }

    public Integer getSoldDays() {
        return soldDays;
    }

    public void setSoldDays(Integer soldDays) {
        this.soldDays = soldDays;
    }

    public Integer getPhotoCount() {
        return photoCount;
    }

    public void setPhotoCount(Integer photoCount) {
        this.photoCount = photoCount;
    }

    public Object getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(Object coverImage) {
        this.coverImage = coverImage;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Integer getSold() {
        return sold;
    }

    public void setSold(Integer sold) {
        this.sold = sold;
    }

    public String getDateOfBooking() {
        return dateOfBooking;
    }

    public void setDateOfBooking(String dateOfBooking) {
        this.dateOfBooking = dateOfBooking;
    }

}
