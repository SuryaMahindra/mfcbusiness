package com.mfcwl.mfc_dealer.SalesStocks.RetrofitServicesLeadSection;

import android.content.Context;

import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.RetrofitConfigLeadSection.PrivateLeadBaseService;
import com.mfcwl.mfc_dealer.SalesStocks.SalesModels.SalesStockAllResponse;
import com.mfcwl.mfc_dealer.SalesStocks.SalesModels.SalesStockReq;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class SalesStockService extends PrivateLeadBaseService {

    public static void postSalesStock(Context context, final SalesStockReq salesRequest, final HttpCallResponse mHttpCallResponse){
        salesStockReqInterFace mInterFace = retrofit.create(salesStockReqInterFace.class);
        Call<SalesStockAllResponse> mCall = mInterFace.salesStockReq(salesRequest);
        mCall.enqueue(new Callback<SalesStockAllResponse>() {
            @Override
            public void onResponse(Call<SalesStockAllResponse> call, Response<SalesStockAllResponse> response) {
                if(response.isSuccessful()){
                    mHttpCallResponse.OnSuccess(response);
                }else {
                    Throwable t=new Throwable ();
                    mHttpCallResponse.OnFailure(t);
                }
            }

            @Override
            public void onFailure(Call<SalesStockAllResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });


    }

    public static void sendEmailSalesStock(Context context, final SalesStockReq salesRequest, final HttpCallResponse mHttpCallResponse){
        sendEmailsalesStockReqInterFace mInterFace = retrofit.create(sendEmailsalesStockReqInterFace.class);
        Call<String> mCall = mInterFace.salesStockReq(salesRequest);
        mCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    mHttpCallResponse.OnSuccess(response);
                }else {
                    Throwable t=new Throwable ();
                    mHttpCallResponse.OnFailure(t);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });


    }


    public interface sendEmailsalesStockReqInterFace{
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("franchise/WSRTSR")
        Call<String> salesStockReq(@Body SalesStockReq salesStockReq);
    }

    public interface salesStockReqInterFace{
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("franchise/WSRTSR")
        Call<SalesStockAllResponse> salesStockReq(@Body SalesStockReq salesStockReq);
    }
}
