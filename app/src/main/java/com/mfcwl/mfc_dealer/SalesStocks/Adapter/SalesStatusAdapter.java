package com.mfcwl.mfc_dealer.SalesStocks.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.mfcwl.mfc_dealer.Model.LeadstatusModel;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.SalesStocks.Activity.SaleStockFilterBy;

import org.json.JSONException;

import java.util.ArrayList;

public class SalesStatusAdapter extends ArrayAdapter {

    private ArrayList dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        CheckBox checkBox;
    }

    public SalesStatusAdapter(ArrayList data, Context context) {
        super(context, R.layout.row_checkbox, data);
        this.dataSet = data;
        this.mContext = context;

    }

    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public LeadstatusModel getItem(int position) {
        return (LeadstatusModel) dataSet.get(position);
    }


    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        SalesStatusAdapter.ViewHolder viewHolder;
        final View result;

        if (convertView == null) {
            viewHolder = new SalesStatusAdapter.ViewHolder();
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_checkbox, parent, false);
            viewHolder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);

            result = convertView;
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (SalesStatusAdapter.ViewHolder) convertView.getTag();
            result = convertView;
        }

        LeadstatusModel item = getItem(position);
        viewHolder.checkBox.setChecked(item.checked);

        viewHolder.checkBox.setText(item.name);
        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                item.checked = isChecked;
                if (isChecked) {
                    SaleStockFilterBy.leadstatusJsonArray.put(item.name);
                } else {
                    LeadStatusArray(item.name);
                }
            }
        });

        return result;
    }

    private void LeadStatusArray(String status) {
        try {
            for (int i = 0; i < SaleStockFilterBy.leadstatusJsonArray.length(); i++) {
                String mStatus = SaleStockFilterBy.leadstatusJsonArray.getString(i);
                if (mStatus.equalsIgnoreCase(status)) {
                    SaleStockFilterBy.leadstatusJsonArray.remove(i);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}