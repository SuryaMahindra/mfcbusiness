package com.mfcwl.mfc_dealer.SalesStocks.Instances;

import org.json.JSONArray;

import java.util.HashMap;

public class SSFilterSaveInstance {

    private static SSFilterSaveInstance mInstance = null;

    HashMap<String, String> savedatahashmap;
    JSONArray statusarray = null;

    private SSFilterSaveInstance() {
        savedatahashmap = new HashMap<String, String>();
        statusarray = new JSONArray();
    }

    public static SSFilterSaveInstance getInstance() {
        if (mInstance == null) {
            mInstance = new SSFilterSaveInstance();
        }
        return mInstance;
    }

    public HashMap<String, String> getSavedatahashmap() {
        return savedatahashmap;
    }

    public void setSavedatahashmap(HashMap<String, String> savedatahashmap) {
        this.savedatahashmap = savedatahashmap;
    }

    public JSONArray getStatusarray() {
        return statusarray;
    }

    public void setStatusarray(JSONArray statusarray) {
        this.statusarray = statusarray;
    }
}
