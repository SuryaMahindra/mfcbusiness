package com.mfcwl.mfc_dealer.SalesStocks.Fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.SalesStocks.Activity.SaleStockFilterBy;
import com.mfcwl.mfc_dealer.SalesStocks.Activity.SalesStockSortBy;
import com.mfcwl.mfc_dealer.SalesStocks.Instances.SSFilterSaveInstance;
import com.mfcwl.mfc_dealer.SalesStocks.SalesInterface.SSFilterApply;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.GAConstants;

import java.lang.ref.WeakReference;

import rdm.PowerBIFragment;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.activity;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.linearLayoutVisible;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.navigation;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchImage;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchicon;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toggle;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toolbar;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.tv_header_title;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ASM;

public class SalesStockTAB extends Fragment implements TabLayout.OnTabSelectedListener,
        View.OnClickListener {

    LinearLayout ll_asm_sales_back_btn,ll_headerdata_ss, ll_ss_sortBy, ll_ss_filter;
    ImageView iv_back_asm_stock_tab,iv_ss_sortBy, iv_ss_filter;
    TabLayout ssTab;
    ViewPager ss_viewpager;

    private final SparseArray<WeakReference<Fragment>> instantiatedFragments = new SparseArray<>();
    private SharedPreferences mSharedPreferences = null;

    private SSFilterApply filterApply;

    public String TAG=getClass().getSimpleName();
    private String dealerName, dealerCode;

    public SalesStockTAB() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments()!=null)
        {
            dealerCode = getArguments().getString("DCODE");
            dealerName = getArguments().getString("DNAME");
        }

    }



    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.fragment_sales_stock_tab, container, false);
        toolbar.setBackgroundColor(getResources().getColor(R.color.black));

        if(CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(USERTYPE_ASM))
        {

            linearLayoutVisible.setVisibility(View.VISIBLE);
            searchicon.setVisibility(View.VISIBLE);
            searchImage.setVisibility(View.VISIBLE);
            toggle.setDrawerIndicatorEnabled(false);
            toggle.setHomeAsUpIndicator(R.drawable.ic_new_hamburger_menu);
           // navigation.setVisibility(View.GONE);
        }

        InitUI(view);

        if (searchicon != null) {
            searchicon.setVisibility(View.VISIBLE);
        }

        if (linearLayoutVisible != null) {
            linearLayoutVisible.setVisibility(View.VISIBLE);
        }

        if (tv_header_title != null) {
            tv_header_title.setVisibility(View.VISIBLE);
            tv_header_title.setText("Sales Lists");
        }

        onClickListener();
        return view;
    }

    private void InitUI(View view) {

        setHasOptionsMenu(true);
        filterApply = (SSFilterApply) getActivity();

        CommonMethods.setvalueAgainstKey(getActivity(), "asm_pages", "sales_list");
        ll_asm_sales_back_btn=view.findViewById(R.id.ll_asm_sales_back_btn);
        ll_headerdata_ss = view.findViewById(R.id.ll_headerdata_ss);
        ll_ss_sortBy = view.findViewById(R.id.ll_ss_sortBy);
        ll_ss_filter = view.findViewById(R.id.ll_ss_filter);
        iv_ss_filter = view.findViewById(R.id.iv_ss_filter);
        iv_ss_sortBy = view.findViewById(R.id.iv_ss_sortBy);
        iv_back_asm_stock_tab=view.findViewById(R.id.iv_back_asm_stock_tab);
        ssTab = view.findViewById(R.id.ssTab);
        ss_viewpager = view.findViewById(R.id.ss_viewpager);
        ss_viewpager.setAdapter(new SalesStockTAB.LeadsPagerAdapter(getFragmentManager(), ssTab.getTabCount()));
        ss_viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(ssTab));
        ssTab.setupWithViewPager(ss_viewpager);
        ssTab.setTabTextColors(Color.parseColor("#aaaaaa"), Color.parseColor("#2d2d2d"));
        ssTab.setSelectedTabIndicatorColor(Color.parseColor("#e5e5e5"));

        ssTab.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(ss_viewpager));

        SalesStockConstant.getInstance().setWhichStock("AllStock");
        WarrantyConstant.getInstance().setWhichWarranty("AllStock");

        CommonMethods.setvalueAgainstKey(getActivity(), "SalesStockLeads", "false");
        CommonMethods.setvalueAgainstKey(getActivity(), "WarrantyLeads", "false");

        CommonMethods.setvalueAgainstKey(getActivity(), "status", "");
        CommonMethods.setvalueAgainstKey(getActivity(), "Pstatus", "salesstock");

        if (SalesInstance.getInstance().getSalesdirection().equals("SalesWarranty")) {
            SalesInstance.getInstance().setSalesdirection("");
            CommonMethods.setvalueAgainstKey(getActivity(), "Pstatus", "warranty");
            ss_viewpager.setCurrentItem(1);
        }

        if(CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(USERTYPE_ASM))
        {
            ll_asm_sales_back_btn.setVisibility(View.VISIBLE);
            iv_back_asm_stock_tab.setVisibility(View.VISIBLE);
        }
        else
        {
            ll_asm_sales_back_btn.setVisibility(View.GONE);
            iv_back_asm_stock_tab.setVisibility(View.GONE);
        }

        if(ll_asm_sales_back_btn.getVisibility()==View.VISIBLE && iv_back_asm_stock_tab.getVisibility()==View.VISIBLE)
        {
            iv_back_asm_stock_tab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loadFragment(new PowerBIFragment(activity));
                }
            });
        }

        try {
            ((MainActivity) getActivity()).setSSFragmentRefreshListener(new MainActivity.SSFragmentRefreshListener() {
                @Override
                public void onRefresh() {
                    // Refresh Your Fragment
                    try {
                        if (CommonMethods.getstringvaluefromkey(getActivity(), "issalesDetails").equalsIgnoreCase("true")) {
                            CommonMethods.setvalueAgainstKey(getActivity(), "issalesDetails", "false");

                            if (CommonMethods.getstringvaluefromkey(getActivity(), "salesleads").equalsIgnoreCase("sales")) {
                                CommonMethods.setvalueAgainstKey(getActivity(), "SalesStockLeads", "true");
                            }
                            if (CommonMethods.getstringvaluefromkey(getActivity(), "salesleads").equalsIgnoreCase("warrantylead")) {
                                CommonMethods.setvalueAgainstKey(getActivity(), "WarrantyLeads", "true");
                            }

                            final ViewPager viewPager = view.findViewById(R.id.ss_viewpager);
                            viewPager.setAdapter(new LeadsPagerAdapter(getFragmentManager(), ssTab.getTabCount()));
                            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(ssTab));

                            if (CommonMethods.getstringvaluefromkey(getActivity(), "salesleads").equalsIgnoreCase("sales")) {
                                viewPager.setCurrentItem(0);
                            }
                            if (CommonMethods.getstringvaluefromkey(getActivity(), "salesleads").equalsIgnoreCase("warranty")) {
                                viewPager.setCurrentItem(1);
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void onClickListener() {
        ssTab.addOnTabSelectedListener(this);
        ll_ss_sortBy.setOnClickListener(this::onClick);
        ll_ss_filter.setOnClickListener(this::onClick);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuItem item = menu.findItem(R.id.notification_bell);
        item.setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        switch (tab.getPosition()) {
            case 0:
                CommonMethods.setvalueAgainstKey(getActivity(), "Pstatus", "salesstock");
                filterApply.applySSFilter("salesstock");
                //  ss_viewpager.setCurrentItem(0);
                break;

            case 1:
                CommonMethods.setvalueAgainstKey(getActivity(), "Pstatus", "warranty");
                filterApply.applySSFilter("warranty");
                //  ss_viewpager.setCurrentItem(1);
                break;

        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_ss_sortBy:

                if (CommonMethods.getstringvaluefromkey(getActivity(), "salesleads").equalsIgnoreCase("sales")) {
                    CommonMethods.setvalueAgainstKey(getActivity(), "SalesStockLeads", "false");
                }
                if (CommonMethods.getstringvaluefromkey(getActivity(), "salesleads").equalsIgnoreCase("warranty")) {
                    CommonMethods.setvalueAgainstKey(getActivity(), "WarrantyLeads", "false");
                }

                SalesStockSortBy sortBy = new SalesStockSortBy(getContext(), getActivity());
                sortBy.show();

                break;
            case R.id.ll_ss_filter:

                if (CommonMethods.getstringvaluefromkey(getActivity(), "salesleads").equalsIgnoreCase("sales")) {
                    CommonMethods.setvalueAgainstKey(getActivity(), "SalesStockLeads", "false");
                }
                if (CommonMethods.getstringvaluefromkey(getActivity(), "salesleads").equalsIgnoreCase("warranty")) {
                    CommonMethods.setvalueAgainstKey(getActivity(), "WarrantyLeads", "false");
                }

                Intent intent = new Intent(getActivity(), SaleStockFilterBy.class);
                startActivityForResult(intent, 400);
                break;
        }
    }

    public class LeadsPagerAdapter extends FragmentStatePagerAdapter {
        private String[] tabTitles = new String[]{" Total Sales ", "Warranty Sales "};

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }

        public LeadsPagerAdapter(FragmentManager fm, int mNumOfTabs) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new SalesStocksAll();
                case 1:
                    return new WarrantyStocks();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return tabTitles.length;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            final Fragment fragment = (Fragment) super.instantiateItem(container, position);
            instantiatedFragments.put(position, new WeakReference<>(fragment));
            return fragment;
        }

        @Nullable
        public Fragment getFragment(final int position) {
            final WeakReference<Fragment> wr = instantiatedFragments.get(position);
            if (wr != null) {
                return wr.get();
            } else {
                return null;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (SSFilterSaveInstance.getInstance().getSavedatahashmap().size() != 0
                ||SSFilterSaveInstance.getInstance().getStatusarray().length() != 0
                ||CommonMethods.getstringvaluefromkey(getActivity(), "sales_certified").equalsIgnoreCase("certified")
                ||CommonMethods.getstringvaluefromkey(getActivity(), "salesmfc").equalsIgnoreCase("mfc")

        ) {
            iv_ss_filter.setBackgroundResource(R.drawable.filter_icon_yellow);
        } else {
            iv_ss_filter.setBackgroundResource(R.drawable.filter_48x18);
        }

        if(CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(USERTYPE_ASM))
        {
            Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_SALES,CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));
        }
    }

    private void loadFragment(Fragment mFragment) {
        Bundle mBundle = new Bundle();
        mBundle.putString("DNAME", dealerName);
        mBundle.putString("DCODE", dealerCode);
        mFragment.setArguments(mBundle);
        switchContent(R.id.fillLayout, mFragment);
    }

    public void switchContent(int id, Fragment fragment) {
        if (activity == null)
            return;
        if (activity instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) activity;
            Fragment frag = fragment;
            mainActivity.switchContent(id, frag);
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

            Log.i("resultCode", "resultCode="+resultCode);
            Log.i("Pstatus", "Pstatus="+CommonMethods.getstringvaluefromkey(getActivity(), "Pstatus"));

            if (resultCode == 500) {
                if (!CommonMethods.getstringvaluefromkey(getActivity(), "Pstatus").equalsIgnoreCase("")) {

                   // if (CommonMethods.getstringvaluefromkey(getActivity(), "Pstatus").equalsIgnoreCase("salesstock")) {
                        filterApply.applySSFilter(CommonMethods.getstringvaluefromkey(getActivity(), "Pstatus"));
                   /* }else{
                        filterApplyWarranty.applyWarrantyFilter(CommonMethods.getstringvaluefromkey(getActivity(), "Pstatus"));
                    }*/

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
