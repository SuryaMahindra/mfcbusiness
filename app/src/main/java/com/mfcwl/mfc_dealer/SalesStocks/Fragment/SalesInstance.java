package com.mfcwl.mfc_dealer.SalesStocks.Fragment;

public class SalesInstance {


    private String whichsales;
    private String salesdirection;

    private SalesInstance() {
        whichsales = "";
        salesdirection = "";
    }

    public static SalesInstance getInstance() {
        if (mInstance == null) {
            mInstance = new SalesInstance();
        }
        return mInstance;
    }

    private static SalesInstance mInstance = null;

    public static SalesInstance getmInstance() {
        return mInstance;
    }

    public static void setmInstance(SalesInstance mInstance) {
        SalesInstance.mInstance = mInstance;
    }

    public String getWhichsales() {
        return whichsales;
    }

    public void setWhichsales(String whichsales) {
        this.whichsales = whichsales;
    }

    public String getSalesdirection() {
        return salesdirection;
    }

    public void setSalesdirection(String salesdirection) {
        this.salesdirection = salesdirection;
    }

}
