package com.mfcwl.mfc_dealer.SalesStocks.RetrofitServicesLeadSection;

import com.mfcwl.mfc_dealer.Model.LeadSection.PrivateLeadsModelRequest;
import com.mfcwl.mfc_dealer.Model.LeadSection.PrivateLeadsModelResponse;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.RetrofitConfigLeadSection.PrivateLeadBaseService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class PrivateLeadService extends PrivateLeadBaseService {

    public static void postPrivateLead(final PrivateLeadsModelRequest privateLeadRequest, final HttpCallResponse mHttpCallResponse){
        PrivateLeadsInterFace mInterFace = retrofit.create(PrivateLeadsInterFace.class);
        Call<PrivateLeadsModelResponse> mCall = mInterFace.privateleads(privateLeadRequest);
        mCall.enqueue(new Callback<PrivateLeadsModelResponse>() {
            @Override
            public void onResponse(Call<PrivateLeadsModelResponse> call, Response<PrivateLeadsModelResponse> response) {
                if(response.isSuccessful()){
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<PrivateLeadsModelResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });


    }




    public interface PrivateLeadsInterFace{
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("dealer/private-leads/getlead")
        Call<PrivateLeadsModelResponse> privateleads(@Body PrivateLeadsModelRequest privateLeadRequest);
    }
}
