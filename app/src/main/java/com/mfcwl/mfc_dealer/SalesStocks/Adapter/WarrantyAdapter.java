package com.mfcwl.mfc_dealer.SalesStocks.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.SalesStocks.SalesInterface.SSLazyloaderWarranty;
import com.mfcwl.mfc_dealer.SalesStocks.SalesModels.SalesStockResData;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchVal;

public class WarrantyAdapter extends RecyclerView.Adapter<WarrantyAdapter.MyViewHolder> implements View.OnClickListener {

    private List<SalesStockResData> warrantyList;
    LayoutInflater layoutInflater;
    public Context mContext;
    Activity a;
    int i;
    private List<SalesStockResData> itemListSearch;
    private SSLazyloaderWarranty lazyloaderWarranty;

    public WarrantyAdapter(Activity a, Context mContext, List<SalesStockResData> procList) {
        this.mContext = mContext;
        this.warrantyList = procList;
        layoutInflater = LayoutInflater.from(mContext);
        itemListSearch = new ArrayList<SalesStockResData>();
        itemListSearch.addAll(procList);
        this.a = a;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView make_tv, sales_modelvariant_tv, sales_registration, day_text, sales_year, sales_kms, sales_owner, sales_warranty, image_number, price_tv, sales_certifieds;
        public CircleImageView car_image;

        public MyViewHolder(View view) {
            super(view);

            make_tv = (TextView) view.findViewById(R.id.make_tv);
            sales_modelvariant_tv = (TextView) view.findViewById(R.id.sales_modelvariant_tv);
            sales_registration = (TextView) view.findViewById(R.id.sales_registration);
            day_text = (TextView) view.findViewById(R.id.day_text);
            sales_year = (TextView) view.findViewById(R.id.sales_year);
            sales_kms = (TextView) view.findViewById(R.id.sales_kms);
            sales_owner = (TextView) view.findViewById(R.id.sales_owner);
            sales_warranty = (TextView) view.findViewById(R.id.sales_warranty);
            image_number = (TextView) view.findViewById(R.id.image_number);
            price_tv = (TextView) view.findViewById(R.id.price_tv);
            car_image = view.findViewById(R.id.car_image);
            sales_certifieds = view.findViewById(R.id.sales_certifieds);


        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        lazyloaderWarranty = (SSLazyloaderWarranty) a;
        View itemView = layoutInflater.from(parent.getContext())
                .inflate(R.layout.row_sales_stock, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        i = position;

        try {
            if (position >= getItemCount() - 1 && getItemCount() > 0 || warrantyList.size() <= 3) {
                if(searchVal.equalsIgnoreCase("")) {
                    lazyloaderWarranty.loadmoreWarranty(getItemCount());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        final SalesStockResData salesData = warrantyList.get(position);

        //ValidateDate

        if (salesData.getDateOfBooking() == null || salesData.getDateOfBooking().equalsIgnoreCase("")) {
            holder.day_text.setText("NA");
        } else {


            holder.day_text.setText(String.valueOf(salesData.getSoldDays())+ " D ago");

   /*         String createdDate = salesData.getDateOfBooking();
            String[] splitDateTime = createdDate.split(" ", 10);
            String[] splitDate = splitDateTime[0].split("-", 10);
            String finalDate = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");
            Calendar c = Calendar.getInstance();
            String formattedDate = simpleDateFormat.format(c.getTime());
            String[] splitSpace = formattedDate.split(" ", 10);

            try {
                Date date1 = simpleDateFormat.parse(finalDate + " 00:00:00");
                Date date2 = simpleDateFormat.parse(splitSpace[0] + " 00:00:00");

                if (printDifference(date1, date2) == 0) {
                    holder.day_text.setText("Today");
                } else if (printDifference(date1, date2) == 1) {
                    holder.day_text.setText("Yesterday");
                } else if (printDifference(date1, date2) > 364) {
                    holder.day_text.setText("" + printDifference(date1, date2) / 365 + " Y ago");
                } else {
                    holder.day_text.setText("" + printDifference(date1, date2) + " D ago");
                }
*/
          /*  } catch (ParseException e) {
                //  e.printStackTrace();
                Log.e("ParseException ", "Normal " + e.toString());
            }*/
        }

        //Update Lead Details
        String make = ((salesData.getMake().trim() == null || salesData.getMake().trim().equalsIgnoreCase("NA") || salesData.getMake().trim().equalsIgnoreCase("")) ? "NA" : salesData.getMake().trim());
        holder.make_tv.setText((make));
        String regNo = ((salesData.getRegistrationNumber().trim() == null || salesData.getRegistrationNumber().trim().equalsIgnoreCase("NA") || salesData.getRegistrationNumber().trim().equalsIgnoreCase("")) ? "NA" : salesData.getRegistrationNumber().trim());
        holder.sales_registration.setText((regNo));
        String leadModVar = ((salesData.getModel().trim() == null || salesData.getModel().trim().equalsIgnoreCase("NA") || salesData.getModel().trim().equalsIgnoreCase("") || salesData.getModel().trim() == null || salesData.getModel().trim().equalsIgnoreCase("NA") || salesData.getModel().trim().equalsIgnoreCase("")) ? "NA" : salesData.getModel().trim() + " " + salesData.getVariant().trim());
        holder.sales_modelvariant_tv.setText((leadModVar));

        //holder.image_number.setText(String.valueOf(salesData.getPhotoCount()));

        if(salesData.getPhotoCount()==null) {
            holder.image_number.setText("0");
        } else {
            holder.image_number.setText(String.valueOf(salesData.getPhotoCount()));
        }

        holder.sales_year.setText(String.valueOf(salesData.getModelYear()));
        holder.sales_kms.setText(String.valueOf(salesData.getKilometer()) + " Kms");
        holder.sales_owner.setText(String.valueOf(salesData.getOwner()) + " Owners");
        holder.price_tv.setText(String.valueOf(salesData.getSellingPrice()));

        if (salesData.getCertified() == null) {
            holder.sales_certifieds.setBackgroundResource(R.drawable.warm_28);
            holder.sales_certifieds.setText("  " + "NA");
        } else {
            try {


                if( salesData.getCertified().equals("1")) {
                    holder.sales_certifieds.setBackgroundResource(R.drawable.warm_28);
                    holder.sales_certifieds.setText("Certified");
                }else{
                    holder.sales_certifieds.setBackgroundResource(R.drawable.warm_28);
                    holder.sales_certifieds.setText("  " + salesData.getCertified());
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        }



        if (salesData.getWarrantyType() == null || salesData.getWarrantyType().equalsIgnoreCase("")) {
            holder.sales_warranty.setText("NA");
        } else {
            try {
                holder.sales_warranty.setText("  " + salesData.getWarrantyType());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (salesData.getCoverImage() == null || salesData.getCoverImage().equals("")) {
            Picasso.get().cancelRequest(holder.car_image);
            holder.car_image.setImageResource(R.drawable.no_car_small);

        } else {

            Target target = new Target() {

                @Override
                public void onPrepareLoad(Drawable arg0) {

                }

                @Override
                public void onBitmapLoaded(Bitmap arg0, Picasso.LoadedFrom arg1) {
                    holder.car_image.setImageBitmap(arg0);
                }

                @Override
                public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                }

            };

            Picasso.get()
                    .load((String) salesData.getCoverImage())
                    .placeholder(R.drawable.small_loading)
                    .resize(200, 200)
                    .noFade()
                    .into(target);

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*CommonMethods.setvalueAgainstKey(a, "salesleads", "warranty");
                Intent in = new Intent(a, SalesDetailsActivity.class);
                a.startActivity(in);*/
            }
        });

    }

    @Override
    public int getItemCount() {
        return warrantyList.size();
    }


    private String capitalize(String capString) {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()) {
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }

    private long printDifference(Date startDate, Date endDate) {
        long different = endDate.getTime() - startDate.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long elapsedDays = different / daysInMilli;
        return elapsedDays;
    }


    //searchfunctionaly WebLeads

    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());
        warrantyList.clear();
        if (charText.length() == 0) {
            warrantyList.addAll(itemListSearch);
        } else {
            for (SalesStockResData it : itemListSearch) {
                if (it.getMake().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getModel().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getVariant().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getWarrantyType().toLowerCase(Locale.getDefault()).contains(charText)
                        || it.getRegistrationNumber().toLowerCase(Locale.getDefault()).contains(charText)
                ) {
                    warrantyList.add(it);
                }
            }
        }
       /* try {
            lazyloaderWarranty.updatecountWarranty(warrantyList.size());
        } catch (Exception e) {

        }*/
        notifyDataSetChanged();

    }

    public void searchDataAdd() {

        itemListSearch.clear();
        itemListSearch.addAll(warrantyList);
    }
}
