package com.mfcwl.mfc_dealer.SalesStocks.Instances;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.archit.calendardaterangepicker.customviews.DateRangeCalendarView;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.SalesStocks.SalesInterface.SalesFilterSelected;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class SalesCalendarFilterDialog extends Dialog implements View.OnClickListener{


    Activity activity;
    private DateRangeCalendarView mCalendar;
    private String startDateString="",endDateString="";
    private String dateType;
    private Button mYes,mCancel;
    private SalesFilterSelected mFilterSelected;
    private String TAG=SalesCalendarFilterDialog.class.getSimpleName();
    Context mContext;
    public static SalesCalendarFilterDialog mFilterCon;
    public SalesCalendarFilterDialog(Activity activity, String dateType) {
        super(activity);
        this.activity= activity;
        mFilterSelected =(SalesFilterSelected) activity;
        this.dateType = dateType;
    }

    public SalesCalendarFilterDialog(Activity activity, String dateType, SalesFilterSelected salesFilterSelected) {
        super(activity);
        this.activity= activity;
        mFilterSelected = salesFilterSelected;
        this.dateType = dateType;
        startDateString="";
        endDateString="";
    }

    public SalesCalendarFilterDialog(Activity activity,View.OnClickListener onClickListener, String dateType, SalesFilterSelected salesFilterSelected) {
        super(activity);
        this.activity=activity;
        mFilterSelected = salesFilterSelected;
        this.dateType = dateType;
        startDateString="";
        endDateString="";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_date_picker_dialog);
        Log.i(TAG, "onCreate: ");
        mContext = getContext();
        mFilterCon = SalesCalendarFilterDialog.this;
        startDateString="";
        endDateString="";
        initView();

    }

    private void initView() {

        mCalendar = (DateRangeCalendarView) findViewById(R.id.calendar);
        mCalendar.setCalendarListener(mListener);
        //mCalendar.setFonts(CustomFonts.getNexaBold(activity));
        mYes = (Button)findViewById(R.id.btnDone);
        mCancel = (Button)findViewById(R.id.btnCancel);
        mYes.setOnClickListener(this);
        mCancel.setOnClickListener(this);
    }

    private DateRangeCalendarView.CalendarListener mListener = new DateRangeCalendarView.CalendarListener() {
        @Override
        public void onFirstDateSelected(Calendar startDate) {
            Log.i(TAG, "onFirstDateSelected: "+startDate);
            Date dateStart = new Date(startDate.getTimeInMillis());
            SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
            startDateString = dateformat.format(dateStart);
            endDateString = startDateString;
            Log.e(TAG,"dateStart "+dateformat.format(dateStart)+" dateEnd "+endDateString);
            mYes.setBackground(activity.getDrawable(R.drawable.rounded_button_background_blue));
            //Toast.makeText(getActivity(), "Start Date: " + startDate.getTime().toString(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onDateRangeSelected(Calendar startDate, Calendar endDate) {

            Date dateStart = new Date(startDate.getTimeInMillis());
            Date dateEnd = new Date(endDate.getTimeInMillis());
            SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
            startDateString = dateformat.format(dateStart);
            endDateString = dateformat.format(dateEnd);
            Log.e(TAG,"dateStart "+dateformat.format(dateStart)+" dateEnd "+dateformat.format(dateEnd));
            mYes.setBackground(activity.getDrawable(R.drawable.rounded_button_background_blue));
            // Toast.makeText(getActivity(), "Start Date: " + startDate.getTime().toString() + " End date: " + endDate.getTime().toString(), Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnDone:
                if(!startDateString.isEmpty())
                { mFilterSelected.onSalesFilterSelection(startDateString,endDateString,dateType);
                dismiss();
                }else{
                    Toast.makeText(activity,"Please select Date",Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.btnCancel:
                mYes.setBackground(activity.getDrawable(R.drawable.event_button_bg));
                dismiss();
                break;

            default:
                break;
        }
        //dismiss();
    }

}
