package com.mfcwl.mfc_dealer.SalesStocks.SalesModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SSWhereIn {
    @SerializedName("column")
    @Expose
    private String column;
    @SerializedName("operator")
    @Expose
    private String operator;
    @SerializedName("values")
    @Expose
    private List<String> values = null;

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }
}
