package com.mfcwl.mfc_dealer.RequestModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MarTraDealerDetailReq {
    @SerializedName("dealercode")
    @Expose
    private String dealercode;

    public String getDealercode() {
        return dealercode;
    }

    public void setDealercode(String dealercode) {
        this.dealercode = dealercode;
    }
}
