package com.mfcwl.mfc_dealer.RequestModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mfcwl.mfc_dealer.DasBoardModels.CustomWhere;

import java.util.List;

public class WebLeadReq1 {
    @SerializedName("filter_by_fields")
    @Expose
    private String filterByFields;
    @SerializedName("per_page")
    @Expose
    private String perPage;
    @SerializedName("page")
    @Expose
    private String page;
    @SerializedName("order_by")
    @Expose
    private String orderBy;
    @SerializedName("order_by_reverse")
    @Expose
    private String orderByReverse;
    @SerializedName("alias_fields")
    @Expose
    private String aliasFields;
    @SerializedName("where_or")
    @Expose
    private List<CustomWhere> whereOr = null;
    @SerializedName("custom_where")
    @Expose
    private List<CustomWhere> customWhere = null;
    @SerializedName("wherenotin")
    @Expose
    private List<CustomWhere> wherenotin = null;
    @SerializedName("report_prefix")
    @Expose
    private String reportPrefix;
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("where_in")
    @Expose
    private List<WhereIn> whereIn = null;
    @SerializedName("tag")
    @Expose
    private String tag;

    public String getFilterByFields() {
        return filterByFields;
    }

    public void setFilterByFields(String filterByFields) {
        this.filterByFields = filterByFields;
    }

    public String getPerPage() {
        return perPage;
    }

    public void setPerPage(String perPage) {
        this.perPage = perPage;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getOrderByReverse() {
        return orderByReverse;
    }

    public void setOrderByReverse(String orderByReverse) {
        this.orderByReverse = orderByReverse;
    }

    public String getAliasFields() {
        return aliasFields;
    }

    public void setAliasFields(String aliasFields) {
        this.aliasFields = aliasFields;
    }


    public List<CustomWhere> getCustomWhere() {
        return customWhere;
    }

    public void setCustomWhere(List<CustomWhere> customWhere) {
        this.customWhere = customWhere;
    }


    public String getReportPrefix() {
        return reportPrefix;
    }

    public void setReportPrefix(String reportPrefix) {
        this.reportPrefix = reportPrefix;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public List<WhereIn> getWhereIn() {
        return whereIn;
    }

    public void setWhereIn(List<WhereIn> whereIn) {
        this.whereIn = whereIn;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public List<CustomWhere> getWhereOr() {
        return whereOr;
    }

    public void setWhereOr(List<CustomWhere> whereOr) {
        this.whereOr = whereOr;
    }

    public List<CustomWhere> getWherenotin() {
        return wherenotin;
    }

    public void setWherenotin(List<CustomWhere> wherenotin) {
        this.wherenotin = wherenotin;
    }
}
