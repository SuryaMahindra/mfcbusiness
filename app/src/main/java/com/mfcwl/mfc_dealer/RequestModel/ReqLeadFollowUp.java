package com.mfcwl.mfc_dealer.RequestModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReqLeadFollowUp {

    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("dispatch_id")
    @Expose
    private String dispatchId;
    @SerializedName("employee_id")
    @Expose
    private String employeeId;
    @SerializedName("employee_name")
    @Expose
    private String employeeName;
    @SerializedName("employee_type")
    @Expose
    private String employeeType;
    @SerializedName("tag")
    @Expose
    private String tag;
    @SerializedName("followup_date")
    @Expose
    private String followupDate;
    @SerializedName("followup_status")
    @Expose
    private String followupStatus;
    @SerializedName("followup_comments")
    @Expose
    private String followupComments;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getDispatchId() {
        return dispatchId;
    }

    public void setDispatchId(String dispatchId) {
        this.dispatchId = dispatchId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(String employeeType) {
        this.employeeType = employeeType;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getFollowupDate() {
        return followupDate;
    }

    public void setFollowupDate(String followupDate) {
        this.followupDate = followupDate;
    }

    public String getFollowupStatus() {
        return followupStatus;
    }

    public void setFollowupStatus(String followupStatus) {
        this.followupStatus = followupStatus;
    }

    public String getFollowupComments() {
        return followupComments;
    }

    public void setFollowupComments(String followupComments) {
        this.followupComments = followupComments;
    }
}
