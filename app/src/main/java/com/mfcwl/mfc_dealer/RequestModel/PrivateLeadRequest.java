package com.mfcwl.mfc_dealer.RequestModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mfcwl.mfc_dealer.DasBoardModels.CustomWhere;

import java.util.List;

public class PrivateLeadRequest {
    @SerializedName("PageItems")
    @Expose
    private String pageItems;
    @SerializedName("Page")
    @Expose
    private String page;
    @SerializedName("wherein")
    @Expose
    private List<WhereinPrivateLeadReq> wherein = null;

    public List<WhereinPrivateLeadReq> getWherein() {
        return wherein;
    }

    public void setWherein(List<WhereinPrivateLeadReq> wherein) {
        this.wherein = wherein;
    }

    @SerializedName("OrderBy")
    @Expose

    private String orderBy;
    @SerializedName("OrderByReverse")
    @Expose
    private String orderByReverse;
    @SerializedName("where_or")
    @Expose
    private List<CustomWhere> whereOr = null;
    @SerializedName("where")
    @Expose
    private List<CustomWhere> where = null;

    public String getPageItems() {
        return pageItems;
    }

    public void setPageItems(String pageItems) {
        this.pageItems = pageItems;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }


    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getOrderByReverse() {
        return orderByReverse;
    }

    public void setOrderByReverse(String orderByReverse) {
        this.orderByReverse = orderByReverse;
    }

    @SerializedName("column")
    @Expose
    private String column;
    @SerializedName("values")
    @Expose
    private List<String> values = null;

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }


    public List<CustomWhere> getWhereOr() {
        return whereOr;
    }

    public void setWhereOr(List<CustomWhere> whereOr) {
        this.whereOr = whereOr;
    }

    public List<CustomWhere> getWhere() {
        return where;
    }

    public void setWhere(List<CustomWhere> where) {
        this.where = where;
    }
}
