package com.mfcwl.mfc_dealer.RequestModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReqPrivateLeadFollowUp {
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("LeadId")
    @Expose
    private String leadId;
    @SerializedName("LeadType")
    @Expose
    private String leadType;
    @SerializedName("LeadStatus")
    @Expose
    private String leadStatus;
    @SerializedName("LeadRemark")
    @Expose
    private String leadRemark;
    @SerializedName("Followup")
    @Expose
    private String followup;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getLeadType() {
        return leadType;
    }

    public void setLeadType(String leadType) {
        this.leadType = leadType;
    }

    public String getLeadStatus() {
        return leadStatus;
    }

    public void setLeadStatus(String leadStatus) {
        this.leadStatus = leadStatus;
    }

    public String getLeadRemark() {
        return leadRemark;
    }

    public void setLeadRemark(String leadRemark) {
        this.leadRemark = leadRemark;
    }

    public String getFollowup() {
        return followup;
    }

    public void setFollowup(String followup) {
        this.followup = followup;
    }
}
