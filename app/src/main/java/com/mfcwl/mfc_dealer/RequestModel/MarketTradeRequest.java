package com.mfcwl.mfc_dealer.RequestModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mfcwl.mfc_dealer.StockModels.Where;

import java.util.List;

public class MarketTradeRequest {

    @SerializedName("Page")
    @Expose
    private String page;
    @SerializedName("PageItems")
    @Expose
    private String pageItems;
    @SerializedName("OrderBy")
    @Expose
    private String orderBy;
    @SerializedName("OrderByReverse")
    @Expose
    private String orderByReverse;
    @SerializedName("wherein")
    @Expose
    private List<WhereIn> wherein = null;
    @SerializedName("where_or")
    @Expose
    private List<Where> whereOr = null;
    @SerializedName("where")
    @Expose
    private List<Where> where = null;

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getPageItems() {
        return pageItems;
    }

    public void setPageItems(String pageItems) {
        this.pageItems = pageItems;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getOrderByReverse() {
        return orderByReverse;
    }

    public void setOrderByReverse(String orderByReverse) {
        this.orderByReverse = orderByReverse;
    }

    public List<WhereIn> getWherein() {
        return wherein;
    }

    public void setWherein(List<WhereIn> wherein) {
        this.wherein = wherein;
    }

    public List<Where> getWhereOr() {
        return whereOr;
    }

    public void setWhereOr(List<Where> whereOr) {
        this.whereOr = whereOr;
    }

    public List<Where> getWhere() {
        return where;
    }

    public void setWhere(List<Where> where) {
        this.where = where;
    }
}
