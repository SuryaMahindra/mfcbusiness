package com.mfcwl.mfc_dealer.RequestModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReqAddLead {
    @SerializedName("lead_type")
    @Expose
    private String leadType;
    @SerializedName("vehicle_type")
    @Expose
    private String vehicleType;
    @SerializedName("lead_date")
    @Expose
    private String leadDate;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("customer_mobile")
    @Expose
    private String customerMobile;
    @SerializedName("customer_email")
    @Expose
    private String customerEmail;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("customer_address")
    @Expose
    private String customerAddress;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("make")
    @Expose
    private String make;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("colour")
    @Expose
    private String colour;
    @SerializedName("customer_budget")
    @Expose
    private String customerBudget;
    @SerializedName("lead_status")
    @Expose
    private String leadStatus;
    @SerializedName("lead_source")
    @Expose
    private String leadSource;
    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("follow_date")
    @Expose
    private String followDate;
    @SerializedName("sales_executive_id")
    @Expose
    private String salesExecutiveId;
    @SerializedName("created_by_device")
    @Expose
    private String createdByDevice;

    public String getSalesExecutivename() {
        return salesExecutivename;
    }

    public void setSalesExecutivename(String salesExecutivename) {
        this.salesExecutivename = salesExecutivename;
    }

    @SerializedName("executive_name")
    @Expose
    private String salesExecutivename;

    public String getLeadType() {
        return leadType;
    }

    public void setLeadType(String leadType) {
        this.leadType = leadType;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getLeadDate() {
        return leadDate;
    }

    public void setLeadDate(String leadDate) {
        this.leadDate = leadDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getCustomerBudget() {
        return customerBudget;
    }

    public void setCustomerBudget(String customerBudget) {
        this.customerBudget = customerBudget;
    }

    public String getLeadStatus() {
        return leadStatus;
    }

    public void setLeadStatus(String leadStatus) {
        this.leadStatus = leadStatus;
    }

    public String getLeadSource() {
        return leadSource;
    }

    public void setLeadSource(String leadSource) {
        this.leadSource = leadSource;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getFollowDate() {
        return followDate;
    }

    public void setFollowDate(String followDate) {
        this.followDate = followDate;
    }

    public String getSalesExecutiveId() {
        return salesExecutiveId;
    }

    public void setSalesExecutiveId(String salesExecutiveId) {
        this.salesExecutiveId = salesExecutiveId;
    }

    public String getCreatedByDevice() {
        return createdByDevice;
    }

    public void setCreatedByDevice(String createdByDevice) {
        this.createdByDevice = createdByDevice;
    }

}
