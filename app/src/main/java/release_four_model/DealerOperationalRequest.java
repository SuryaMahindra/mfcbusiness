package release_four_model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class DealerOperationalRequest
{
        @SerializedName("dealer_code")
        @Expose
        private String dealerCode;
        @SerializedName("follow_up_date")
        @Expose
        private String followUpDate;
        @SerializedName("points")
        @Expose
        private List<Object> points = null;
        @SerializedName("video_url")
        @Expose
        private String videoUrl;
        @SerializedName("is_operational")
        @Expose
        private String isOperational;

        public String getDealerCode() {
            return dealerCode;
        }

        public void setDealerCode(String dealerCode) {
            this.dealerCode = dealerCode;
        }

        public String getFollowUpDate() {
            return followUpDate;
        }

        public void setFollowUpDate(String followUpDate) {
            this.followUpDate = followUpDate;
        }

        public List<Object> getPoints() {
            return points;
        }

        public void setPoints(List<Object> points) {
            this.points = points;
        }

        public String getVideoUrl() {
            return videoUrl;
        }

        public void setVideoUrl(String videoUrl) {
            this.videoUrl = videoUrl;
        }

        public String getIsOperational() {
            return isOperational;
        }

        public void setIsOperational(String isOperational) {
            this.isOperational = isOperational;
        }

    }

