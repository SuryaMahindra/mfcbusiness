package rdm;

import android.app.Activity;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.CustomFonts;
import com.mfcwl.mfc_dealer.Utility.GAConstants;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import imageeditor.base.BaseFragment;

import rdm.model.DealerResponseData;
import rdm.services.LandingRequest;
import rdm.services.RDMService;
import remotedealer.rdrform.ui.EventListFragment;
import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.hideOption;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.navigation;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchImage;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchicon;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toolbar;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.tv_header_title;

public class RDMDealerListFragment extends BaseFragment {

    Activity activity;

    TextView tvAllDealers, tvRed, tvYellow, tvGreen;
    ImageView ivAllDealerNotification;
    RecyclerView recyclerView;
    MenuItem asm_mail;
    private String mASMId = "";

    private static String TAG = EventListFragment.class.getSimpleName();

/*
    public RDMDealerListFragment() {
    }
*/

    public RDMDealerListFragment(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_r_d_m_dealer_list, container, false);
        setHasOptionsMenu(true);
        Log.i(TAG, "onCreateView: ");
        tvAllDealers = view.findViewById(R.id.tvAllDealers);
        tvRed = view.findViewById(R.id.tvRed);
        tvYellow = view.findViewById(R.id.tvyellow);
        tvGreen = view.findViewById(R.id.tvgreen);
        ivAllDealerNotification = view.findViewById(R.id.ivAllDealerNotification);
        setCustomTypeFace(tvAllDealers);
        setCustomTypeFace(tvRed);
        setCustomTypeFace(tvYellow);
        setCustomTypeFace(tvGreen);
        recyclerView = view.findViewById(R.id.all_dealers_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutManager);
        mASMId = CommonMethods.getstringvaluefromkey(activity, "user_id");
        LandingRequest req = new LandingRequest();
        req.setAreaManagerId("");
        req.setUserId(mASMId);
        try {
            req.setStartDate(CommonMethods.getTodayDate(activity));
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        fetchData(req);
        searchicon.setVisibility(View.VISIBLE);
        searchImage.setVisibility(View.GONE);

        searchImage.setImageResource(0);
        tv_header_title.setVisibility(View.VISIBLE);
        tv_header_title.setTypeface(CustomFonts.getRobotoRegularTF(activity.getApplicationContext()));
        CommonMethods.setvalueAgainstKey(activity, "status", "dealers");
        tv_header_title.setText("DEALERS");


        return view;
    }


    private void setCustomTypeFace(TextView textView) {

        try {
            textView.setTypeface(CustomFonts.getRobotoBold(activity));
        } catch (Exception exception) {
            exception.printStackTrace();

        }

    }

    private void fetchData(LandingRequest mLandingRequest) {

        SpinnerManager.showSpinner(getActivity());
        RDMService.getDealerData(mLandingRequest, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(getActivity());
                retrofit2.Response<DealerResponseData> mRes = (Response<DealerResponseData>) obj;
                DealerResponseData mData = mRes.body();
                tvRed.setText("" + mData.getTotalRed());
                tvYellow.setText("" + mData.getTotalAmber());
                tvGreen.setText("" + mData.getTotalGreen());
                setToAdapter(mData);
            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(getActivity());
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem asm_mail = menu.findItem(R.id.asm_mail);
        asm_mail.setVisible(false);
        MenuItem notification = menu.findItem(R.id.notification_bell);
        notification.setVisible(true);
        hideOption(R.id.notification_bell);

        super.onPrepareOptionsMenu(menu);

        super.onPrepareOptionsMenu(menu);
    }

    private void setToAdapter(DealerResponseData mData) {

        try {
            RDMDealerListAdapter rdmDealerListAdapter = new RDMDealerListAdapter(activity, mData);
            recyclerView.setAdapter(rdmDealerListAdapter);
        } catch (NullPointerException nullPointerExc) {
            CommonMethods.alertMessage(activity, getResources().getString(R.string.no_data_found));
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

/*    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }*/

    @Override
    public void onResume() {
        tv_header_title.setText("DEALERS");
        tv_header_title.setTextColor(getResources().getColor(R.color.black));
        searchImage.setVisibility(View.GONE);
        /*navigation.setVisibility(View.VISIBLE);*/
        toolbar.setBackgroundColor(getResources().getColor(R.color.all_dealers_fragment_bg));
        Application.getInstance().logASMEvent(GAConstants.AM_DEALER_LISTING, CommonMethods.getstringvaluefromkey(activity, "user_id") + System.currentTimeMillis() + "" + CommonMethods.getstringvaluefromkey(activity, "device_id"));

        if (asm_mail != null) {
            asm_mail.setVisible(false);
        }
        super.onResume();
    }
}