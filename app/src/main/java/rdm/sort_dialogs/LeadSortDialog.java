package rdm.sort_dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.AppConstants;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;

public class LeadSortDialog extends Dialog implements View.OnClickListener {

    private TextView tvOldestToLatest, tvPostedDateLTO, tvFollowDateOTL, tvFollowDateLeadLTO, tvClearLeadsSort;
    private ImageView ivSortByClose, ivLeadsOldestToLatest, ivPostedDateLTO, ivFollowDateLTO, ivFollowUpLeadsOTL;
    private String selectedSort = "";
    private Activity activity;
    private LeadSortCallBack leadSortCallBack;

    public LeadSortDialog(Activity activity, LeadSortCallBack leadSortCallBack) {
        super(activity);
        this.activity = activity;
        this.leadSortCallBack = leadSortCallBack;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_leads_sort_dialog);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tvOldestToLatest = findViewById(R.id.tvOldestToLatest);
        tvPostedDateLTO = findViewById(R.id.tvPostedDateLTO);
        tvFollowDateOTL = findViewById(R.id.tvFollowDateOTL);
        tvFollowDateLeadLTO = findViewById(R.id.tvFollowDateLeadLTO);
        tvClearLeadsSort = findViewById(R.id.tvClearLeadsSort);
        ivSortByClose = findViewById(R.id.ivSortByClose);
        ivLeadsOldestToLatest = findViewById(R.id.ivLeadsOldestToLatest);
        ivPostedDateLTO = findViewById(R.id.ivPostedDateLTO);
        ivFollowDateLTO = findViewById(R.id.ivFollowDateLTO);
        ivFollowUpLeadsOTL = findViewById(R.id.ivFollowUpLeadsOTL);

        tvOldestToLatest.setOnClickListener(this);
        tvPostedDateLTO.setOnClickListener(this);
        tvFollowDateOTL.setOnClickListener(this);
        tvFollowDateLeadLTO.setOnClickListener(this);
        tvClearLeadsSort.setOnClickListener(this);
        ivSortByClose.setOnClickListener(this);
        ivLeadsOldestToLatest.setOnClickListener(this);
        ivPostedDateLTO.setOnClickListener(this);
        ivFollowDateLTO.setOnClickListener(this);
        ivFollowUpLeadsOTL.setOnClickListener(this);

        if (!CommonMethods.getstringvaluefromkey(activity, AppConstants.ASM_LEAD_SORT).isEmpty()) {
            selectedSort = CommonMethods.getstringvaluefromkey(activity, AppConstants.ASM_LEAD_SORT);
            tvClearLeadsSort.setVisibility(View.VISIBLE);
            if (selectedSort.equalsIgnoreCase(AppConstants.POSTED_OTL)) {
                tvOldestToLatest.setTextColor(activity.getColor(R.color.edit_text_blue));
                ivLeadsOldestToLatest.setImageResource(R.drawable.ic_new_active_rounded_cb);
            } else if (selectedSort.equalsIgnoreCase(AppConstants.POSTED_LTO)) {
                tvPostedDateLTO.setTextColor(activity.getColor(R.color.edit_text_blue));
                ivPostedDateLTO.setImageResource(R.drawable.ic_new_active_rounded_cb);
            } else if (selectedSort.equalsIgnoreCase(AppConstants.FOLLOW_OTL)) {
                tvFollowDateOTL.setTextColor(activity.getColor(R.color.edit_text_blue));
                ivFollowUpLeadsOTL.setImageResource(R.drawable.ic_new_active_rounded_cb);
            } else if (selectedSort.equalsIgnoreCase(AppConstants.FOLLOW_LTO)) {
                tvFollowDateLeadLTO.setTextColor(activity.getColor(R.color.edit_text_blue));
                ivFollowDateLTO.setImageResource(R.drawable.ic_new_active_rounded_cb);
            }

        }

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.tvOldestToLatest)
        {
            selectedSort = AppConstants.POSTED_OTL;
            updateSelectedSort(tvOldestToLatest, ivLeadsOldestToLatest);
        }
        else if (v.getId() == R.id.tvPostedDateLTO) {
            selectedSort = AppConstants.POSTED_LTO;
            updateSelectedSort(tvPostedDateLTO, ivPostedDateLTO);
        }
        else if (v.getId() == R.id.tvFollowDateOTL)
        {
            selectedSort = AppConstants.FOLLOW_OTL;
            updateSelectedSort(tvFollowDateOTL, ivFollowUpLeadsOTL);
        }
        else if (v.getId() == R.id.tvFollowDateLeadLTO)
        {
            selectedSort = AppConstants.FOLLOW_LTO;
            updateSelectedSort(tvFollowDateLeadLTO, ivFollowDateLTO);
        }
        else if (v.getId() == R.id.tvClearLeadsSort)
        {
            selectedSort = "";
            tvOldestToLatest.setTextColor(activity.getColor(R.color.tone));
            tvPostedDateLTO.setTextColor(activity.getColor(R.color.tone));
            tvFollowDateOTL.setTextColor(activity.getColor(R.color.tone));
            tvFollowDateLeadLTO.setTextColor(activity.getColor(R.color.tone));
            CommonMethods.setvalueAgainstKey(activity, AppConstants.ASM_LEAD_SORT, selectedSort);
            leadSortCallBack.sortLeadBy(selectedSort);
            tvClearLeadsSort.setVisibility(View.GONE);
            dismiss();
        }
        else if (v.getId() == R.id.ivSortByClose)
        {
            leadSortCallBack.sortLeadBy(selectedSort);
            dismiss();
        }
    }

    private void updateSelectedSort(TextView textView, ImageView imageView) {
        textView.setTextColor(activity.getColor(R.color.edit_text_blue));
        CommonMethods.setvalueAgainstKey(activity, AppConstants.ASM_LEAD_SORT, selectedSort);
        leadSortCallBack.sortLeadBy(selectedSort);
        imageView.setImageResource(R.drawable.ic_new_active_rounded_cb);
        dismiss();
    }

    public interface LeadSortCallBack {
        void sortLeadBy(String selectedSort);
    }

}
