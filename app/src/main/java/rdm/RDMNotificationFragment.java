package rdm;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mfcwl.mfc_dealer.R;

public class RDMNotificationFragment extends Fragment {

    private RecyclerView rv_asmLanding_notification;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_notification, container, false);
        rv_asmLanding_notification = view.findViewById(R.id.rv_asmLanding_notification);
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getActivity());
        rv_asmLanding_notification.setLayoutManager(layoutManager);

        return view;
    }
}
