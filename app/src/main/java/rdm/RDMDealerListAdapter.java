package rdm;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.AppConstants;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.CustomFonts;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mteams.MTeamsLogin;
import rdm.model.Dealer;
import rdm.model.DealerResponseData;
import rdm.model.Parameter;

import static java.util.Objects.*;

public class RDMDealerListAdapter extends RecyclerView.Adapter<RDMDealerListAdapter.ViewHolder> {
    DealerResponseData dealerResponseData;
    Activity activity;
    List<Dealer> allDealers;

    private String formater = "yyyy-MM-dd";
    List<Parameter> parameterList;
    final Calendar cal = Calendar.getInstance();
    private SimpleDateFormat sdf = new SimpleDateFormat(formater);
    private String TAG = RDMDealerListAdapter.class.getSimpleName();
    private Context context;


    public RDMDealerListAdapter(Activity activity, DealerResponseData dealerResponseData) {
        this.dealerResponseData = dealerResponseData;
        this.activity = activity;
        context = activity.getApplicationContext();
        this.allDealers = dealerResponseData.getDealers();
        removeNullElement(allDealers);

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_item_all_dealer, parent, false);
        Log.i(TAG, "onCreateViewHolder: ");
        return new ViewHolder(listItem);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        try {

            final Dealer dealer = allDealers.get(position);
            holder.tvAllDealersDealerName.setText(dealer.getDealerName());
            String TheDateString = sdf.format(cal.getTime());

            holder.tvAllDealersToday.setText(getRequiredDate(TheDateString, formater, 0));
            holder.tvAllDealersYesterday.setText(getRequiredDate(TheDateString, formater, -1));
            holder.tvAllDealers4July.setText(getRequiredDate(TheDateString, formater, -2));
            holder.tvAllDealers5July.setText(getRequiredDate(TheDateString, formater, -3));


            setImageColorBackground(holder.ivDealerStatusToday, dealer.getStatusDay0());
            setImageColorBackground(holder.ivAllDealersYesterday, dealer.getStatusDay1());
            setImageColorBackground(holder.ivAllDealers5July, dealer.getStatusDay2());
            setImageColorBackground(holder.ivAllDealers4July, dealer.getStatusDay3());

            if (dealer.getWarrantyBalance() != null) {
                String strWB = String.valueOf(dealer.getWarrantyBalance());
                strWB = strWB.substring(0, strWB.indexOf('.'));
                holder.tvWarrantyBalanceVal.setText(strWB);
            } else {
                holder.tvWarrantyBalanceVal.setText("0");
            }

            if (dealer.getRoyalty() != null) {
                holder.tvRoyaltyValue.setText(String.valueOf(dealer.getRoyalty()));
            } else {
                holder.tvRoyaltyValue.setText("NA");
            }

            parameterList = dealer.getParameters();
            setColumnAdapter(parameterList, holder.all_dealers_row_item_recyclerview);


            holder.mDetailedview.setOnClickListener(v -> {
           /* Intent chatview = new Intent(activity, PowerBIChartsActivity.class);
            chatview.putExtra("DNAME", dealerResponseData.getDealers().get(position).getDealerName());
            chatview.putExtra("DCODE", dealerResponseData.getDealers().get(position).getDealerCode());
            activity.startActivity(chatview);*/

                loadFragment(dealerResponseData.getDealers().get(position).getDealerName(), dealerResponseData.getDealers().get(position).getDealerCode());


            });

            holder.tvSetRDR.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Intent intent=new Intent(activity,NotificationActivity.class);
                    Intent intent = new Intent(activity, MTeamsLogin.class);
                    intent.putExtra(AppConstants.DEALER_CODE, dealer.getDealerCode());
                    intent.putExtra(AppConstants.DEALER_NAME, dealer.getDealerName());
                    intent.putExtra(AppConstants.MEETING_TYPE, "setRDR");
                    activity.startActivity(intent);
                }
            });

            holder.tvStartRDR.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, MTeamsLogin.class);
                    intent.putExtra(AppConstants.DEALER_CODE, dealer.getDealerCode());
                    intent.putExtra(AppConstants.DEALER_NAME, dealer.getDealerName());
                    intent.putExtra(AppConstants.MEETING_TYPE, "startRDR");
                    activity.startActivity(intent);
                }
            });

        } catch (Exception exception) {

        }

    }

    private void setColumnAdapter(List<Parameter> parameterList, RecyclerView recyclerView) {

        try {
            if (parameterList.isEmpty()) {
                CommonMethods.alertMessage(activity, activity.getResources().getString(R.string.no_data_found));
            } else {
                RDMColumnListAdapter rdmColumnListAdapter = new RDMColumnListAdapter(activity, parameterList);
                recyclerView.setAdapter(rdmColumnListAdapter);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return dealerResponseData.getDealers().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvDealerName, tvSetRDR, tvStartRDR;
        RecyclerView all_dealers_row_item_recyclerview;
        TextView tvAllDealersDealerName, tvAllDealersToday, tvAllDealersYesterday, tvAllDealers5July, tvAllDealers4July, tvWarrantyBalanceVal, tvRoyaltyValue;
        LinearLayout mDetailedview;
        ImageView ivDealerStatusToday, ivAllDealersYesterday, ivAllDealers5July, ivAllDealers4July;


        public ViewHolder(View itemView) {
            super(itemView);
            tvSetRDR = itemView.findViewById(R.id.tvSetRDR);
            tvStartRDR = itemView.findViewById(R.id.tvStartRDR);
            SpannableString setRDR = new SpannableString("Set RDR");
            setRDR.setSpan(new UnderlineSpan(), 0, setRDR.length(), 0);
            tvSetRDR.setText(setRDR);
            SpannableString startRDR = new SpannableString("Start RDR");
            startRDR.setSpan(new UnderlineSpan(), 0, startRDR.length(), 0);
            tvStartRDR.setText(startRDR);
            tvAllDealersDealerName = itemView.findViewById(R.id.tvAllDealersDealerName);
            tvAllDealersToday = itemView.findViewById(R.id.tvAllDealersToday);
            tvAllDealersYesterday = itemView.findViewById(R.id.tvAllDealersYesterday);
            tvAllDealers5July = itemView.findViewById(R.id.tvAllDealers5July);
            tvAllDealers4July = itemView.findViewById(R.id.tvAllDealers4July);
            tvWarrantyBalanceVal = itemView.findViewById(R.id.tvWarrantyBalanceVal);
            tvRoyaltyValue = itemView.findViewById(R.id.tvRoyaltyValue);
            mDetailedview = itemView.findViewById(R.id.detailedview);
            ivDealerStatusToday = itemView.findViewById(R.id.ivDealerStatusToday);
            ivAllDealersYesterday = itemView.findViewById(R.id.ivAllDealersYesterday);
            ivAllDealers5July = itemView.findViewById(R.id.ivAllDealers5July);
            ivAllDealers4July = itemView.findViewById(R.id.ivAllDealers4July);
            tvSetRDR.setTypeface(CustomFonts.getRobotoMedium(activity.getApplicationContext()));
            tvStartRDR.setTypeface(CustomFonts.getRobotoMedium(activity.getApplicationContext()));
            all_dealers_row_item_recyclerview = itemView.findViewById(R.id.all_dealers_row_item_recyclerview);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
            all_dealers_row_item_recyclerview.setLayoutManager(layoutManager);

        }

    }

    @SuppressLint("SimpleDateFormat")
    public String getRequiredDate(String date, String format, int days) {
        try {
            final Calendar cal = Calendar.getInstance();
            cal.setTime(requireNonNull(new SimpleDateFormat(format).parse(date)));
            cal.add(Calendar.DATE, days);
            String formatExpected = "dd MMM";
            SimpleDateFormat sdf = new SimpleDateFormat(formatExpected);
            date = sdf.format(cal.getTime());
            if (days == 0) {
                date = "Today";
            } else if (days == -1) {
                date = "Yesterday";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return date;
    }

    private void loadFragment(String name, String code) {
        PowerBIFragment mFragment = new PowerBIFragment(activity);
        Bundle mBundle = new Bundle();
        mBundle.putString("DNAME", name);
        mBundle.putString("DCODE", code);
        mFragment.setArguments(mBundle);
        switchContent(R.id.fillLayout, mFragment);
    }

    public void switchContent(int id, Fragment fragment) {
        if (activity == null)
            return;
        if (activity instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) activity;
            Fragment frag = fragment;
            mainActivity.switchContent(id, frag);
        }

    }

    public void setImageColorBackground(ImageView imageView, String colorName) {
        if (colorName == null || colorName.equalsIgnoreCase("")) {
            imageView.setImageResource(android.R.color.transparent);
            imageView.setBackground(null);
            return;
        }
        if (colorName.equalsIgnoreCase("Red")) {
            imageView.setImageResource(android.R.color.transparent);
            imageView.setBackground(activity.getResources().getDrawable(R.drawable.all_dealer_pink_bar));
        } else if (colorName.equalsIgnoreCase("Green")) {
            imageView.setImageResource(android.R.color.transparent);
            imageView.setBackground(activity.getResources().getDrawable(R.drawable.all_dealer_green_bar));
        } else if (colorName.equalsIgnoreCase("Amber")) {
            imageView.setImageResource(android.R.color.transparent);
            imageView.setBackground(activity.getResources().getDrawable(R.drawable.all_dealer_yellow_bar));
        }

    }

    private void removeNullElement(List<Dealer> dealers) {
        while (dealers.remove(null)) {
        }
    }

}
