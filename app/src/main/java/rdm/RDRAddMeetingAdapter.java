package rdm;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mfcwl.mfc_dealer.Activity.RDR.DiscussionPoint10;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.AppConstants;
import com.mfcwl.mfc_dealer.Utility.CustomFonts;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import rdm.model.EventData;
import rdm.model.Participant;

public class RDRAddMeetingAdapter extends RecyclerView.Adapter<RDRAddMeetingAdapter.ViewHolder> {
    private static final String TAG = DiscussionPoint10.class.getSimpleName();
    Activity activity;
    List<EventData> addMeetingList;
    private RadioButton lastCheckedRB = null;
    private int lastCheckedPosition = -1;

    public RDRAddMeetingAdapter(Activity activity, List<EventData> addMeetingList) {
        this.addMeetingList = addMeetingList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.rdm_row_item_add_meeting, parent, false);
        Log.i(TAG, "onCreateViewHolder: ");
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String dealerCode;
        String dealerName, eventTitle;
        String strEventDescription, strEventType, strStartDate, strEndDate, strLocation, strParticipants, strIsAllDay, strEventId,strMeetingId="";


        try {
            if (addMeetingList.get(position).getDealerCode() != null && !addMeetingList.get(position).getDealerCode().isEmpty()) {
                dealerCode = addMeetingList.get(position).getDealerCode();
            } else {
                dealerCode = activity.getResources().getString(R.string.str_NA);
            }

            if (addMeetingList.get(position).getDealerName() != null && !addMeetingList.get(position).getDealerName().isEmpty()) {
                dealerName = addMeetingList.get(position).getDealerName();
            } else {
                dealerName = activity.getResources().getString(R.string.str_NA);
            }

            if (addMeetingList.get(position).getEventTitle() != null && !addMeetingList.get(position).getEventTitle().isEmpty()) {
                eventTitle = addMeetingList.get(position).getEventTitle();
            } else {
                eventTitle = activity.getResources().getString(R.string.str_NA);
            }

            strEventDescription = addMeetingList.get(position).getEventDescription();
            strEndDate = addMeetingList.get(position).getEventEndDateTime();
            strStartDate = addMeetingList.get(position).getEventStartDateTime();
            strLocation = addMeetingList.get(position).getEventLocation();
            strEventType = addMeetingList.get(position).getEventType();
            strIsAllDay = addMeetingList.get(position).getIsAllDayEvent();
            strParticipants = getParticipants(addMeetingList.get(position).getParticipants());
            strEventId = String.valueOf(addMeetingList.get(position).getEventId());
            if(addMeetingList.get(position).getMteamsEventId()!=null)
            {
                strMeetingId=addMeetingList.get(position).getMteamsEventId();
            }
            else
            {
                strMeetingId="";
            }

            holder.tv_rdr_DealerName.setText(eventTitle);



            holder.rb_addMeeting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int copyOfLastCheckedPosition = lastCheckedPosition;
                    lastCheckedPosition = position;
                    notifyItemChanged(copyOfLastCheckedPosition);
                    notifyItemChanged(lastCheckedPosition);

                }
            });
            String finalStrMeetingId = strMeetingId;
            holder.btnAddMeetingRowModify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.rb_addMeeting.performClick();

                    Intent intent = new Intent(activity, EventEditActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("dealer_code", dealerCode);
                    bundle.putString("dealer_name", dealerName);
                    bundle.putString("event_type", strEventType);
                    bundle.putString("event_title", eventTitle);
                    bundle.putString("event_description", strEventDescription);
                    bundle.putString("event_location", strLocation);
                    bundle.putString("event_participants", strParticipants);
                    bundle.putString("is_all_day_event", strIsAllDay);
                    bundle.putString("event_start", strStartDate);
                    bundle.putString("event_end", strEndDate);
                    bundle.putString("event_id", strEventId);
                    bundle.putString("meeting_id", finalStrMeetingId);
                    bundle.putString(AppConstants.MEETING_TYPE, "startRDR");
                    intent.putExtra("intent_data", bundle);
                    activity.startActivity(intent);
                }
            });

            holder.rb_addMeeting.setChecked(position == lastCheckedPosition);
            if (addMeetingList.get(position).getIsAllDayEvent().equalsIgnoreCase("false")) {
                String startDate = getStartDate(addMeetingList.get(position).getEventStartDateTime());
                holder.tv_rdr_date_and_day.setText(startDate);
                getTimeInFormat(addMeetingList.get(position).getEventStartDateTime(), holder.tv_rdr_time);
                getTimeInFormat(addMeetingList.get(position).getEventEndDateTime(), holder.tv_addEvent_rdr_end_time);
            } else {
                String allDay = " All Day";
                String startDate = getStartDate(addMeetingList.get(position).getEventStartDateTime());
                holder.tv_rdr_date_and_day.setText(startDate + allDay);
                holder.tv_addEvent_to_lbl.setVisibility(View.GONE);
                holder.tv_addEvent_rdr_end_time.setVisibility(View.GONE);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private String getParticipants(List<Participant> participants) {

        ArrayList<Participant> alParticipantList = new ArrayList<>();
        alParticipantList.addAll(participants);
        String strParticipant = "";
        for (int i = 0; i < alParticipantList.size(); i++) {
            strParticipant = strParticipant + " | " + alParticipantList.get(i).getEmail();

        }
        return strParticipant;
    }

    @Override
    public int getItemCount() {
        return addMeetingList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_rdr_DealerName, tv_rdr_date_and_day, tv_rdr_time, tv_addEvent_to_lbl, tv_addEvent_rdr_end_time;
        public RadioGroup rg_addMeeting;
        RadioButton rb_addMeeting;
        Button btnAddMeetingRowModify;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_rdr_DealerName = (TextView) itemView.findViewById(R.id.tv_rdr_DealerName);
            rb_addMeeting = (RadioButton) itemView.findViewById(R.id.rb_addMeeting);
            tv_rdr_date_and_day = (TextView) itemView.findViewById(R.id.tv_addEvent_rdr_date_and_day);
            tv_rdr_time = (TextView) itemView.findViewById(R.id.tv_addEvent_rdr_start_time);
            tv_addEvent_to_lbl = (TextView) itemView.findViewById(R.id.tv_addEvent_to_lbl);
            tv_addEvent_rdr_end_time = (TextView) itemView.findViewById(R.id.tv_addEvent_rdr_end_time);
            //rg_addMeeting = (RadioGroup) itemView.findViewById(R.id.rg_addMeeting);
            btnAddMeetingRowModify = (Button) itemView.findViewById(R.id.btnAddMeetingRowModify);

            tv_rdr_DealerName.setTypeface(CustomFonts.getRobotoLight(activity));
            tv_addEvent_to_lbl.setTypeface(CustomFonts.getRobotoMedium(activity));
            tv_addEvent_rdr_end_time.setTypeface(CustomFonts.getRobotoMedium(activity));

            tv_rdr_date_and_day.setTypeface(CustomFonts.getRobotoMedium(activity));
            tv_rdr_time.setTypeface(CustomFonts.getRobotoMedium(activity));
            btnAddMeetingRowModify.setTypeface(CustomFonts.getRobotoRegularTF(activity));

        }
    }


    @SuppressLint("SimpleDateFormat")
    private String getStartDate(String startDate) {
        String[] dateArr = startDate.split(" ");
        String date = dateArr[0];
        String dayOfWeek = "";
        String suffix = "";
        Date startDt = new Date();
        SimpleDateFormat strDateToDate = new SimpleDateFormat("yyyy-MM-dd");
        try {
            startDt = strDateToDate.parse(date);
            date = strDateToDate.format(startDt);
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        dayOfWeek = new SimpleDateFormat("EE").format(startDt);

        String formattedDate[] = date.split("-");
        suffix = getDayOfMonthSuffix(Integer.parseInt(formattedDate[2]));
        String result = formattedDate[2] + suffix + " " + dayOfWeek;
        return result;
    }

    private String getDayOfMonthSuffix(final int n) {
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    private void getTimeInFormat(String dateTime, TextView textView) {
        String textViewText = textView.getText().toString();
        String[] givenDateTime = dateTime.split(" ");
        String time = givenDateTime[1];
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:", Locale.getDefault());
        SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a", Locale.getDefault());
        Date dt;
        String formattedTime = "";
        try {
            dt = sdf.parse(time);
            if (dt != null) {
                formattedTime = sdfs.format(dt);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        textView.setText(formattedTime);
    }
}
