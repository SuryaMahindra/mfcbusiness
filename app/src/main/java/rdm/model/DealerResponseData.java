package rdm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DealerResponseData {

    @SerializedName("TotalDealers")
    @Expose
    private Integer totalDealers;
    @SerializedName("TotalRed")
    @Expose
    private Integer totalRed;
    @SerializedName("TotalAmber")
    @Expose
    private Integer totalAmber;
    @SerializedName("TotalGreen")
    @Expose
    private Integer totalGreen;
    @SerializedName("Dealers")
    @Expose
    private List<Dealer> dealers = null;
    @SerializedName("PendingTask")
    @Expose
    private Integer pendingTask;
    @SerializedName("MeetingsToday")
    @Expose
    private Integer meetingsToday;
    @SerializedName("WRPending")
    @Expose
    private Integer wRPending;
    @SerializedName("Training")
    @Expose
    private Integer training;
    @SerializedName("TaskAssigned")
    @Expose
    private Integer taskAssigned;

    public Integer getTotalDealers() {
        return totalDealers;
    }

    public void setTotalDealers(Integer totalDealers) {
        this.totalDealers = totalDealers;
    }

    public Integer getTotalRed() {
        return totalRed;
    }

    public void setTotalRed(Integer totalRed) {
        this.totalRed = totalRed;
    }

    public Integer getTotalAmber() {
        return totalAmber;
    }

    public void setTotalAmber(Integer totalAmber) {
        this.totalAmber = totalAmber;
    }

    public Integer getTotalGreen() {
        return totalGreen;
    }

    public void setTotalGreen(Integer totalGreen) {
        this.totalGreen = totalGreen;
    }

    public List<Dealer> getDealers() {
        return dealers;
    }

    public void setDealers(List<Dealer> dealers) {
        this.dealers = dealers;
    }

    public Integer getPendingTask() {
        return pendingTask;
    }

    public void setPendingTask(Integer pendingTask) {
        this.pendingTask = pendingTask;
    }

    public Integer getMeetingsToday() {
        return meetingsToday;
    }

    public void setMeetingsToday(Integer meetingsToday) {
        this.meetingsToday = meetingsToday;
    }

    public Integer getWRPending() {
        return wRPending;
    }

    public void setWRPending(Integer wRPending) {
        this.wRPending = wRPending;
    }

    public Integer getTraining() {
        return training;
    }

    public void setTraining(Integer training) {
        this.training = training;
    }

    public Integer getTaskAssigned() {
        return taskAssigned;
    }

    public void setTaskAssigned(Integer taskAssigned) {
        this.taskAssigned = taskAssigned;
    }

}