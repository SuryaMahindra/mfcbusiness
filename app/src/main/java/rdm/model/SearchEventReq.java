package rdm.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchEventReq {

        @SerializedName("Page")
        @Expose
        private String page;
        @SerializedName("PageItems")
        @Expose
        private String pageItems;
        @SerializedName("FilterByFields")
        @Expose
        private String filterByFields;
        @SerializedName("OrderBy")
        @Expose
        private String orderBy;
        @SerializedName("OrderByReverse")
        @Expose
        private String orderByReverse;
        @SerializedName("where")
        @Expose
        private List<EventWhere> where = null;

        public String getPage() {
            return page;
        }

        public void setPage(String page) {
            this.page = page;
        }

        public String getPageItems() {
            return pageItems;
        }

        public void setPageItems(String pageItems) {
            this.pageItems = pageItems;
        }

        public String getFilterByFields() {
            return filterByFields;
        }

        public void setFilterByFields(String filterByFields) {
            this.filterByFields = filterByFields;
        }

        public String getOrderBy() {
            return orderBy;
        }

        public void setOrderBy(String orderBy) {
            this.orderBy = orderBy;
        }

        public String getOrderByReverse() {
            return orderByReverse;
        }

        public void setOrderByReverse(String orderByReverse) {
            this.orderByReverse = orderByReverse;
        }

        public List<EventWhere> getWhere() {
            return where;
        }

        public void setWhere(List<EventWhere> where) {
            this.where = where;
        }

    }
