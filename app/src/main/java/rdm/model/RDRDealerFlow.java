package rdm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RDRDealerFlow {

    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("sent")
    @Expose
    private String sent;
    @SerializedName("SentTime")
    @Expose
    private String sentTime;
    @SerializedName("dealer_code")
    @Expose
    private Object dealerCode;
    @SerializedName("dealer_name")
    @Expose
    private Object dealerName;
    @SerializedName("meeting_url")
    @Expose
    private Object meetingUrl;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSent() {
        return sent;
    }

    public void setSent(String sent) {
        this.sent = sent;
    }

    public String getSentTime() {
        return sentTime;
    }

    public void setSentTime(String sentTime) {
        this.sentTime = sentTime;
    }

    public Object getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(Object dealerCode) {
        this.dealerCode = dealerCode;
    }

    public Object getDealerName() {
        return dealerName;
    }

    public void setDealerName(Object dealerName) {
        this.dealerName = dealerName;
    }

    public Object getMeetingUrl() {
        return meetingUrl;
    }

    public void setMeetingUrl(Object meetingUrl) {
        this.meetingUrl = meetingUrl;
    }

}