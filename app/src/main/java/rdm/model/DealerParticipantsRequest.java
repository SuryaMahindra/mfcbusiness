package rdm.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class DealerParticipantsRequest
{

        @SerializedName("dealer_code")
        @Expose
        private String dealerCode;

        public String getDealerCode() {
            return dealerCode;
        }

        public void setDealerCode(String dealerCode) {
            this.dealerCode = dealerCode;
        }


    }
