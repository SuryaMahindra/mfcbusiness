package rdm.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EventEditRequest {

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    @SerializedName("dealer_code")
    @Expose
    private String dealerCode = "";

    @SerializedName("event_title")
    @Expose
    private String eventTitle;
    @SerializedName("event_description")
    @Expose
    private String eventDescription;
    @SerializedName("event_location")
    @Expose
    private String eventLocation;
    @SerializedName("is_all_day_event")
    @Expose
    private String isAllDayEvent;
    @SerializedName("event_start_date_time")
    @Expose
    private String eventStartDateTime;
    @SerializedName("event_end_date_time")
    @Expose
    private String eventEndDateTime;
    @SerializedName("Event_type")
    @Expose
    private String eventType;
    @SerializedName("event_id")
    @Expose
    private String eventId;
    @SerializedName("participants")
    @Expose
    private List<Participant> participants = null;
    @SerializedName("mteams_event_id")
    @Expose
    private String mteamsEventId;

    public String getMteamsEventId() {
        return mteamsEventId;
    }

    public void setMteamsEventId(String mteamsEventId) {
        this.mteamsEventId = mteamsEventId;
    }


    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getEventLocation() {
        return eventLocation;
    }

    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }

    public String getIsAllDayEvent() {
        return isAllDayEvent;
    }

    public void setIsAllDayEvent(String isAllDayEvent) {
        this.isAllDayEvent = isAllDayEvent;
    }

    public String getEventStartDateTime() {
        return eventStartDateTime;
    }

    public void setEventStartDateTime(String eventStartDateTime) {
        this.eventStartDateTime = eventStartDateTime;
    }

    public String getEventEndDateTime() {
        return eventEndDateTime;
    }

    public void setEventEndDateTime(String eventEndDateTime) {
        this.eventEndDateTime = eventEndDateTime;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }


}
