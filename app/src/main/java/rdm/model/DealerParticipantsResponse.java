package rdm.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DealerParticipantsResponse {

    @SerializedName("participants")
    @Expose
    private List<Participant> participants = null;
    @SerializedName("dealer_code")
    @Expose
    private String dealerCode;

    public List<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

}