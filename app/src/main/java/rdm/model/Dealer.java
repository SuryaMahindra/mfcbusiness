package rdm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Dealer {

    @SerializedName("dealerCode")
    @Expose
    private String dealerCode;
    @SerializedName("dealer_name")
    @Expose
    private String dealerName;
    @SerializedName("dealer_email")
    @Expose
    private String dealerEmail;
    @SerializedName("DealerType")
    @Expose
    private String dealerType;
    @SerializedName("WarrantyBalance")
    @Expose
    private Double warrantyBalance;
    @SerializedName("Royalty")
    @Expose
    private String royalty;
    @SerializedName("WarrantyAmount")
    @Expose
    private String warrantyAmount;
    @SerializedName("status_day0")
    @Expose
    private String statusDay0;
    @SerializedName("status_day1")
    @Expose
    private String statusDay1;
    @SerializedName("status_day2")
    @Expose
    private String statusDay2;
    @SerializedName("status_day3")
    @Expose
    private String statusDay3;
    @SerializedName("paramaters")
    @Expose
    private List<Parameter> parameters = null;

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDealerEmail() {
        return dealerEmail;
    }

    public void setDealerEmail(String dealerEmail) {
        this.dealerEmail = dealerEmail;
    }

    public String getDealerType() {
        return dealerType;
    }

    public void setDealerType(String dealerType) {
        this.dealerType = dealerType;
    }

    public Double getWarrantyBalance() {
        return warrantyBalance;
    }

    public void setWarrantyBalance(Double warrantyBalance) {
        this.warrantyBalance = warrantyBalance;
    }

    public String getWarrantyAmount() {
        return warrantyAmount;
    }

    public void setWarrantyAmount(String warrantyAmount) {
        this.warrantyAmount = warrantyAmount;
    }
    public String getRoyalty() {
        return royalty;
    }

    public void setRoyalty(String royalty) {
        this.royalty = royalty;
    }


    public String getStatusDay0() {
        return statusDay0;
    }

    public void setStatusDay0(String statusDay0) {
        this.statusDay0 = statusDay0;
    }

    public String getStatusDay1() {
        return statusDay1;
    }

    public void setStatusDay1(String statusDay1) {
        this.statusDay1 = statusDay1;
    }

    public String getStatusDay2() {
        return statusDay2;
    }

    public void setStatusDay2(String statusDay2) {
        this.statusDay2 = statusDay2;
    }

    public String getStatusDay3() {
        return statusDay3;
    }

    public void setStatusDay3(String statusDay3) {
        this.statusDay3 = statusDay3;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

}
