package rdm;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.CustomFonts;

import java.util.List;

import rdm.model.Parameter;

public class RDMColumnListAdapter extends RecyclerView.Adapter<RDMColumnListAdapter.ViewHolder>
{

    List<Parameter> parameter;
    Activity activity;
    private String TAG=RDMColumnListAdapter.class.getSimpleName();

    public RDMColumnListAdapter(Activity activity,List<Parameter> parameter) {
        this.parameter = parameter;
        this.activity = activity;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.column_item_rdm_dealer_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        Log.i(TAG, "onCreateView: ");
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvParameterTitle.setText(parameter.get(position).getName());
        String actual = parameter.get(position).getActualValue();
        String target = parameter.get(position).getTargetValue();

        if(actual == null || actual.equalsIgnoreCase("")){
            actual = "0";
        }
        if(target == null || target.equalsIgnoreCase("")){
            target = "0";
        }

        holder.tvActualTargetValue.setText(actual+" / "+ target);
        setImageColorBackground(holder.ivParamSmallTab,parameter.get(position).getStatus());
    }

    @Override
    public int getItemCount() {
        return parameter.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvParameterTitle,tvActualTargetValue;
        ImageView ivParamSmallTab;

        public ViewHolder(View itemView) {
            super(itemView);
            tvParameterTitle=itemView.findViewById(R.id.tvParameterTitle);
            tvParameterTitle.setTypeface(CustomFonts.getRobotoMedium(activity.getApplicationContext()));
            tvActualTargetValue=itemView.findViewById(R.id.tvActualTargetValue);
            ivParamSmallTab=itemView.findViewById(R.id.ivParamSmallTab);
        }
    }

    public void setImageColorBackground(ImageView imageView, String colorName) {
        if(colorName == null || colorName.equalsIgnoreCase("")){
            imageView.setImageResource(android.R.color.transparent);
            imageView.setBackground(null);
            return;
        }
        if (colorName.equalsIgnoreCase("Red")) {
            imageView.setImageResource(android.R.color.transparent);
            imageView.setBackground(activity.getResources().getDrawable(R.drawable.all_dealer_pink_bar));
        } else if (colorName.equalsIgnoreCase("Green")) {
            imageView.setImageResource(android.R.color.transparent);
            imageView.setBackground(activity.getResources().getDrawable(R.drawable.all_dealer_green_bar));
        } else if (colorName.equalsIgnoreCase("Amber")) {
            imageView.setImageResource(android.R.color.transparent);
            imageView.setBackground(activity.getResources().getDrawable(R.drawable.all_dealer_yellow_bar));
        }

    }
}
