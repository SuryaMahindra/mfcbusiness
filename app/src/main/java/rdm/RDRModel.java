package rdm;

class RDRModel
{

    public RDRModel(Boolean isCheckBoxChecked, String strDealerName, String strRDR, String strMeetingDate, String strDateAndTime, String strDateDay)
    {
        this.isCheckBoxChecked=isCheckBoxChecked;
        this.strDealerName=strDealerName;
        this.strRDR=strRDR;
        this.strMeetingDate=strMeetingDate;
        this.strDateAndTime=strDateAndTime;
        this.strDateDay=strDateDay;
    }

    public Boolean getCheckBoxChecked() {
        return isCheckBoxChecked;
    }

    public void setCheckBoxChecked(Boolean checkBoxChecked) {
        isCheckBoxChecked = checkBoxChecked;
    }

    Boolean isCheckBoxChecked;
    String strDealerName="";
    String strRDR="";
    String strMeetingDate="";
    String strDateAndTime="";
    String strDateDay="";

    public String getStrDealerName() {
        return strDealerName;
    }

    public void setStrDealerName(String strDealerName) {
        this.strDealerName = strDealerName;
    }

    public String getStrRDR() {
        return strRDR;
    }

    public void setStrRDR(String strRDR) {
        this.strRDR = strRDR;
    }

    public String getStrMeetingDate() {
        return strMeetingDate;
    }

    public void setStrMeetingDate(String strMeetingDate) {
        this.strMeetingDate = strMeetingDate;
    }

    public String getStrDateAndTime() {
        return strDateAndTime;
    }

    public void setStrDateAndTime(String strDateAndTime) {
        this.strDateAndTime = strDateAndTime;
    }

    public String getStrDateDay() {
        return strDateDay;
    }

    public void setStrDateDay(String strDateDay) {
        this.strDateDay = strDateDay;
    }

    public String getStrMeetingTime() {
        return strMeetingTime;
    }

    public void setStrMeetingTime(String strMeetingTime) {
        this.strMeetingTime = strMeetingTime;
    }

    String strMeetingTime="";


}
