package rdm;

import android.app.Activity;
import android.content.Intent;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.AppConstants;
import com.mfcwl.mfc_dealer.Utility.CustomFonts;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import mteams.MTeamsLogin;
import rdm.model.Dealer;

class ASMLandingAdapter extends RecyclerView.Adapter<ASMLandingAdapter.ASMViewHolder> {

    List<Dealer> dealers;
    Activity activity;

    private String strFormatter = "yyyy-MM-dd";

    final Calendar cal = Calendar.getInstance();
    private SimpleDateFormat sdf = new SimpleDateFormat(strFormatter);
    String TAG = ASMLandingAdapter.class.getSimpleName();


    public ASMLandingAdapter(List<Dealer> dealers, Activity activity) {
        this.dealers = dealers;
        this.activity = activity;

    }


    @NonNull
    @Override
    public ASMViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_item_dealer_status, parent, false);
        ASMViewHolder viewHolder = new ASMViewHolder(listItem);
        Log.i(TAG, "onCreateViewHolder: ");
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ASMViewHolder holder, int position) {
        String theDateString = sdf.format(cal.getTime());
        // Log.e("the date","" +TheDateString);
        try {
            if (dealers.get(position) != null) {
                Dealer mData = dealers.get(position);


                holder.tvDealerName.setText(mData.getDealerName());


                holder.tvDealerStatusToday.setText(getRequiredDate(theDateString, strFormatter, 0));
                holder.tvDealerStatusYesterday.setText(getRequiredDate(theDateString, strFormatter, -1));
                holder.tvDealerStatus5July.setText(getRequiredDate(theDateString, strFormatter, -2));
                holder.tvDealerStatus4July.setText(getRequiredDate(theDateString, strFormatter, -3));

                setImageColorBackground(holder.ivDealerStatusToday, mData.getStatusDay0());
                setImageColorBackground(holder.ivDealerStatusYesterday, mData.getStatusDay1());
                setImageColorBackground(holder.ivDealerStatus5July, mData.getStatusDay2());
                setImageColorBackground(holder.ivDealerStatus4July, mData.getStatusDay3());


                Dealer finalMData = mData;
                holder.tvSetRDR.setOnClickListener(v -> setAndStartRDR(finalMData.getDealerCode(), finalMData.getDealerName(), "setRDR"));

                holder.tvStartRDR.setOnClickListener(v -> setAndStartRDR(finalMData.getDealerCode(), finalMData.getDealerName(), "startRDR"));
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return dealers.size();
    }


    public void setImageColorBackground(ImageView imageView, String colorName) {
        if (colorName == null || colorName.equalsIgnoreCase("")) {
            imageView.setImageResource(android.R.color.transparent);
            imageView.setBackground(null);
            return;
        }
        if (colorName.equalsIgnoreCase("Red")) {
            imageView.setImageResource(android.R.color.transparent);
            imageView.setBackground(activity.getResources().getDrawable(R.drawable.all_dealer_pink_bar));
        } else if (colorName.equalsIgnoreCase("Green")) {
            imageView.setImageResource(android.R.color.transparent);
            imageView.setBackground(activity.getResources().getDrawable(R.drawable.all_dealer_green_bar));
        } else if (colorName.equalsIgnoreCase("Amber")) {
            imageView.setImageResource(android.R.color.transparent);
            imageView.setBackground(activity.getResources().getDrawable(R.drawable.all_dealer_yellow_bar));
        }

    }

    public class ASMViewHolder extends RecyclerView.ViewHolder {

        public TextView tvDealerName, tvSetRDR, tvStartRDR, tvDealerStatusToday, tvDealerStatusYesterday, tvDealerStatus5July, tvDealerStatus4July;
        ImageView ivDealerStatusToday, ivDealerStatusYesterday, ivDealerStatus5July, ivDealerStatus4July;
        LinearLayout llCallDealerList;

        public ASMViewHolder(View itemView) {
            super(itemView);
            this.tvDealerName = (TextView) itemView.findViewById(R.id.tvDealersStatusDealerName);
            tvSetRDR = (TextView) itemView.findViewById(R.id.tvDealerStatusSetRDR);
            tvStartRDR = (TextView) itemView.findViewById(R.id.tvDealerStatusStartRDR);
            tvDealerStatusToday = (TextView) itemView.findViewById(R.id.tvDealerStatusToday);
            tvDealerStatusYesterday = (TextView) itemView.findViewById(R.id.tvDealerStatusYesterday);
            tvDealerStatus5July = (TextView) itemView.findViewById(R.id.tvDealerStatus5July);
            tvDealerStatus4July = (TextView) itemView.findViewById(R.id.tvDealerStatus4July);

            llCallDealerList = (LinearLayout) itemView.findViewById(R.id.llCallDealerList);

            tvSetRDR.setTypeface(CustomFonts.getRobotoMedium(activity.getApplicationContext()));
            tvStartRDR.setTypeface(CustomFonts.getRobotoMedium(activity.getApplicationContext()));

            ivDealerStatusToday = (ImageView) itemView.findViewById(R.id.ivDealerStatusToday);
            ivDealerStatusYesterday = (ImageView) itemView.findViewById(R.id.ivDealerStatusYesterday);
            ivDealerStatus5July = (ImageView) itemView.findViewById(R.id.ivDealerStatus5July);
            ivDealerStatus4July = (ImageView) itemView.findViewById(R.id.ivDealerStatus4July);


            SpannableString setRDR = new SpannableString("Set RDR");
            setRDR.setSpan(new UnderlineSpan(), 0, setRDR.length(), 0);
            tvSetRDR.setText(setRDR);
            SpannableString startRDR = new SpannableString("Start RDR");
            startRDR.setSpan(new UnderlineSpan(), 0, startRDR.length(), 0);
            tvStartRDR.setText(startRDR);
        }
    }

    public String getRequiredDate(String date, String format, int days) {
        try {
            final Calendar cal = Calendar.getInstance();
            cal.setTime(new SimpleDateFormat(format).parse(date));
            cal.add(Calendar.DATE, days);
            String formaterExpected = "dd MMM";
            SimpleDateFormat sdf = new SimpleDateFormat(formaterExpected);
            date = sdf.format(cal.getTime());
            if (days == 0) {
                date = "Today";
            } else if (days == -1) {
                date = "Yesterday";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return date;
    }

    private void setAndStartRDR(String dealerCode, String dealerName, String meetingType) {
        Intent intent = new Intent(activity, MTeamsLogin.class);
        intent.putExtra(AppConstants.DEALER_CODE, dealerCode);
        intent.putExtra(AppConstants.DEALER_NAME, dealerName);
        intent.putExtra(AppConstants.MEETING_TYPE, meetingType);
        activity.startActivity(intent);
    }
}



