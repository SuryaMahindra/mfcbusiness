package rdm;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import imageeditor.base.BaseActivity;
import rdm.model.NotificationRequest;
import rdm.model.RDRNotificationResponse;
import rdm.services.RDMService;
import retrofit2.Response;

public class NotificationActivity extends BaseActivity {

    LinearLayout iv_notification_back;
    RecyclerView rv_asmLanding_notification;
    ASMNotificationAdapter asmNotificationAdapter;
    SearchView searchView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        Log.i(TAG, "onCreate: ");
        initViews();
        NotificationRequest req = new NotificationRequest();
        req.setFromdate(CalendarTodayDate());
       // req.setFromdate("2020-10-21");
        fetchNotifications(req);
    }


    public String lastmonth = "";

    public String CalendarTodayDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -30);
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        lastmonth = s.format(new Date(cal.getTimeInMillis()));
        return lastmonth;
    }

    private void initViews() {
        iv_notification_back = findViewById(R.id.iv_notification_back);
        rv_asmLanding_notification = findViewById(R.id.rv_asmLanding_notification);
       // iv_notification_search = findViewById(R.id.iv_notification_search);
/*
        searchView = findViewById(R.id.searchView);
*/
        iv_notification_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rv_asmLanding_notification.setLayoutManager(layoutManager);

      /*  iv_notification_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setVisibility(View.VISIBLE);
            }
        });


        int searchCloseButtonId = searchView.getContext().getResources()
                .getIdentifier("android:id/search_close_btn", null, null);

        ImageView closeButton = (ImageView) this.searchView.findViewById(searchCloseButtonId);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                searchView.setQuery("", false);
                searchView.clearFocus();
                searchView.setVisibility(View.GONE);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                asmNotificationAdapter.filter(query);
                return false;
            }


            @Override
            public boolean onQueryTextChange(String newText) {
                asmNotificationAdapter.filter(newText);
                return false;
            }
        });*/
    }


    private void fetchNotifications(NotificationRequest mRequest) {
        SpinnerManager.showSpinner(this);
        RDMService.getNotificationsFromServer(mRequest, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(NotificationActivity.this);
                Response<RDRNotificationResponse> mData = (Response<RDRNotificationResponse>) obj;
                RDRNotificationResponse data = mData.body();

                try {
                    if (data.getData().size() > 0) {
                        asmNotificationAdapter = new ASMNotificationAdapter(data, NotificationActivity.this);
                        rv_asmLanding_notification.setAdapter(asmNotificationAdapter);
                    } else {
                        Toast.makeText(NotificationActivity.this,"No data found",Toast.LENGTH_LONG).show();
                         //CommonMethods.alertMessage(NotificationActivity.this,getResources().getString(R.string.no_data_found));
                    }
                    }
                catch(Exception exception){
                        exception.printStackTrace();
                    }

                }

                @Override
                public void OnFailure (Throwable mThrowable){
                    SpinnerManager.hideSpinner(NotificationActivity.this);
                    mThrowable.printStackTrace();
                    CommonMethods.alertMessage(NotificationActivity.this, getResources().getString(R.string.no_data_found));
                }
            });

        }

        @Override
        public void onBackPressed () {
            finish();
            super.onBackPressed();
        }

    }