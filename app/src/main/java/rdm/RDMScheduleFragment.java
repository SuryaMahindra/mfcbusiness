package rdm;

import android.app.Activity;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.CustomFonts;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import imageeditor.base.BaseFragment;
import rdm.model.EventData;
import rdm.model.EventResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.hideOption;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.navigation;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchImage;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchicon;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toolbar;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.tv_header_title;
import static rdm.retrofit_services.RetroBaseService.URL_END_SCHEDULE_EVENT;
import static rdm.retrofit_services.RetroBaseService.retrofitInterface;

public class RDMScheduleFragment extends BaseFragment implements Callback<Object> {


    Activity activity;
    String startDate = "", endDate = "";
    private String startDateVal = "2020-07-13";
    private String format = "yyyy-MM-dd hh:mm:ss";
    private String formatExpected = "dd MMM";
    String TAG = RDMScheduleFragment.class.getSimpleName();

    final Calendar cal = Calendar.getInstance();
    Date todayDate=cal.getTime();

    private SimpleDateFormat sdf = new SimpleDateFormat(format);

    TextView tvScheduleEventLbl;
    ImageView iv_schedule_event_search;
    RecyclerView rv_schedule_events_list;
    SearchView searchView;
    MenuItem asm_mail;
    ScheduleEventAdapter scheduleEventAdapter;

    public RDMScheduleFragment(Activity activity) {
        this.activity = activity;
        startDateVal = sdf.format(todayDate);
        Log.i(TAG, "RDMScheduleFragment: "+startDateVal);
        //startDate = startDateVal + " 05:10:39";
        startDate= CommonMethods.getTodayAndTime(activity);
        endDate = "2024-08-07 01:10:39";
        retrofitInterface.getFromWeb(URL_END_SCHEDULE_EVENT, startDate, endDate).enqueue(this);
        SpinnerManager.showSpinner(activity);

        if(MainActivity.menu != null) {
            MenuItem item = MainActivity.menu.findItem(R.id.asm_mail);
            item.setVisible(false);
            MenuItem item1=MainActivity.menu.findItem(R.id.notification_bell);
            searchicon.setVisibility(View.GONE);
            item1.setVisible(true);
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_r_d_m_schedule, container, false);
        setHasOptionsMenu(true);
       /* if (navigation.getVisibility() == View.GONE || navigation.getVisibility() == View.INVISIBLE)
        {
            navigation.setVisibility(View.VISIBLE);
        }
       */ rv_schedule_events_list = view.findViewById(R.id.rv_schedule_events_list);
       // tvScheduleEventLbl=view.findViewById(R.id.tvScheduleEventLbl);
        //tvScheduleEventLbl.setTypeface(CustomFonts.getRobotoBlackTF(activity));
        //iv_schedule_event_search=view.findViewById(R.id.iv_schedule_event_search);
        //searchView=view.findViewById(R.id.se_searchView);
        //tvScheduleEventLbl.setText(CommonMethods.getMonthNameAndYear(activity,CommonMethods.getTodayDate(activity)));
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        rv_schedule_events_list.setLayoutManager(layoutManager);
        Log.i(TAG, "onCreateView: ");

        /*searchicon.setVisibility(View.VISIBLE);
        searchImage.setImageResource(0);
        tv_header_title.setVisibility(View.VISIBLE);

        tv_header_title.setTypeface(CustomFonts.getRobotoRegularTF(activity.getApplicationContext()));
        tv_header_title.setText("SCHEDULE");

        iv_schedule_event_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setVisibility(View.VISIBLE);
            }
        });

        int searchCloseButtonId = searchView.getContext().getResources()
                .getIdentifier("android:id/search_close_btn", null, null);

        ImageView closeButton = (ImageView) this.searchView.findViewById(searchCloseButtonId);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                searchView.setQuery("", false);
                searchView.clearFocus();
                searchView.setVisibility(View.GONE);
            }
        });

        searchView.setOnQueryTextListener(new  SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                scheduleEventAdapter.filter(query);
                return false;
            }


            @Override
            public boolean onQueryTextChange(String newText) {
                scheduleEventAdapter.filter(newText);
                return false;
            }
        });
        */return view;
    }

    @Override
    public void onResponse(Call<Object> call, Response<Object> response) {

        SpinnerManager.hideSpinner(activity);
        try {
            String strRes = new Gson().toJson(response.body());
            Log.i("RDMScheduleFragment", "onResponse: " + strRes);
            EventResponse eventResponse = new Gson().fromJson(strRes, EventResponse.class);
            if (eventResponse.getIsSuccess()) {
                Log.i(TAG, "onResponse: " + eventResponse.getIsSuccess());
                setToScheduleAdapter(eventResponse.getData());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onFailure(Call<Object> call, Throwable t) {

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        asm_mail = menu.findItem(R.id.asm_mail);
        asm_mail.setVisible(false);
        MenuItem notification=menu.findItem(R.id.notification_bell);
        notification.setVisible(true);
        hideOption(R.id.notification_bell);

        super.onPrepareOptionsMenu(menu);

    }
    public void setToScheduleAdapter(List<EventData> eventDataList) {

        try
        {
            scheduleEventAdapter = new ScheduleEventAdapter(eventDataList, getActivity());
            rv_schedule_events_list.setAdapter(scheduleEventAdapter);
        }
        catch(NullPointerException nullPointerExc)
        {
            CommonMethods.alertMessage(activity,getResources().getString(R.string.no_data_found));
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
        }

    }

    @Override
    public void onResume() {
        tv_header_title.setText("SCHEDULE");
        tv_header_title.setTextColor(getResources().getColor(R.color.black));
        searchImage.setVisibility(View.GONE);
        /*navigation.setVisibility(View.VISIBLE);*/
        toolbar.setBackgroundColor(getResources().getColor(R.color.all_dealers_fragment_bg));
        super.onResume();
    }
}