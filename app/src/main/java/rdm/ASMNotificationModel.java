package rdm;

class ASMNotificationModel
{
    String strEventTitle;
    String strNotificationRDR;
    String strNDate;
    String strEventStartTime;

    public ASMNotificationModel(String strEventTitle, String strNotificationRDR, String strNDate, String strEventStartTime) {
        this.strEventTitle = strEventTitle;
        this.strNotificationRDR = strNotificationRDR;
        this.strNDate = strNDate;
        this.strEventStartTime = strEventStartTime;
    }

    public String getStrEventTitle() {
        return strEventTitle;
    }

    public void setStrEventTitle(String strEventTitle) {
        this.strEventTitle = strEventTitle;
    }

    public String getStrNotificationRDR() {
        return strNotificationRDR;
    }

    public void setStrNotificationRDR(String strNotificationRDR) {
        this.strNotificationRDR = strNotificationRDR;
    }

    public String getStrNDate() {
        return strNDate;
    }

    public void setStrNDate(String strNDate) {
        this.strNDate = strNDate;
    }

    public String getStrEventStartTime() {
        return strEventStartTime;
    }

    public void setStrEventStartTime(String strEventStartTime) {
        this.strEventStartTime = strEventStartTime;
    }


}
