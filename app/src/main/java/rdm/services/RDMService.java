package rdm.services;

import com.mfcwl.mfc_dealer.Activity.RDR.Model.DashboardResponse;
import com.mfcwl.mfc_dealer.Activity.RDR.Model.EscalateRequest;
import com.mfcwl.mfc_dealer.Activity.RDR.Services.RDRDashboardService;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.retrofitconfig.DasboardBaseService;

import rdm.model.DealerResponseData;
import rdm.model.NotificationRequest;
import rdm.model.RDRNotificationResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class RDMService extends DasboardBaseService {

    public static void getDealerData(final  LandingRequest mRequest, final HttpCallResponse mCallResponse){
        RDMInterFace mInterFace =  retrofit.create(RDMInterFace.class);
        Call<DealerResponseData> mCall = mInterFace.fetchDealers(mRequest);

        mCall.enqueue(new Callback<DealerResponseData>() {

            @Override
            public void onResponse(Call<DealerResponseData> call, Response<DealerResponseData> response) {
                if(response.isSuccessful()){
                    mCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<DealerResponseData> call, Throwable t) {
                mCallResponse.OnFailure(t);
            }
        });
    }

    public static void getNotificationsFromServer(NotificationRequest mRequest,HttpCallResponse mHttpCallResponse){

        RDMInterFace mInterFace =  retrofit.create(RDMInterFace.class);
        Call<RDRNotificationResponse> mCall = mInterFace.fetchNotification(mRequest);

        mCall.enqueue(new Callback<RDRNotificationResponse>() {
            @Override
            public void onFailure(Call<RDRNotificationResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }

            @Override
            public void onResponse(Call<RDRNotificationResponse> call, Response<RDRNotificationResponse> response) {
                if(response.isSuccessful()){
                    mHttpCallResponse.OnSuccess(response);
                }
            }
        });
    }


    public interface RDMInterFace {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("rd-dealer/dealers")
        Call<DealerResponseData> fetchDealers(@Body LandingRequest mRequest);

        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("asm/pull-notifications")
        Call<RDRNotificationResponse> fetchNotification(@Body NotificationRequest mRequest);

    }
}
