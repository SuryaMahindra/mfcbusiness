package rdm;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mfcwl.mfc_dealer.Activity.RDR.DiscussionPoint1;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.AppConstants;

import java.util.ArrayList;
import java.util.Locale;

import mteams.MTeamsLogin;
import rdm.model.NotificationData;
import rdm.model.RDRNotificationResponse;

class ASMNotificationAdapter extends RecyclerView.Adapter<ASMNotificationAdapter.ViewHolder> {

    RDRNotificationResponse mData;
    Activity activity;
    ArrayList<NotificationData> notificationList = new ArrayList<>();
    ArrayList<NotificationData> filteredItems = new ArrayList<>();
    private String TAG=ASMNotificationAdapter.class.getSimpleName();

    public ASMNotificationAdapter(RDRNotificationResponse mData, Activity activity) {
        this.mData = mData;
        this.activity = activity;
        filteredItems.addAll(mData.getData());
        notificationList.addAll(filteredItems);

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_item_asm_notification, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        NotificationData actualData = filteredItems.get(position);
        holder.tvEventTitle.setText(actualData.getMessage());
        /*holder.llMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (actualData.getDealerCode() != null) {
                    if (actualData.getMeetingUrl() == null) {

                        setRDR(actualData.getDealerCode().toString(), actualData.getDealerName().toString());
                    } else {
                        startRDR(actualData.getDealerCode().toString(), actualData.getDealerName().toString(),actualData.getMeetingUrl().toString());
                    }
                } else {
                    Toast.makeText(activity, "Dealer code is empty", Toast.LENGTH_SHORT).show();
                }
            }
        });*/
    }


    @Override
    public int getItemCount() {
        return filteredItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvEventTitle, tvNotificationRDR, tvDate, tvStartTime;
        LinearLayout llMessage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvEventTitle = itemView.findViewById(R.id.tvEventTit);
            tvNotificationRDR = itemView.findViewById(R.id.tvNotificationRDR);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvStartTime = itemView.findViewById(R.id.tvStartTime);
            llMessage = itemView.findViewById(R.id.llmessage);
        }
    }

    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());
        filteredItems.clear();
        if (charText.length() == 0) {
            filteredItems.addAll(notificationList);
        } else {
            for (NotificationData it : notificationList) {
                if (it.getMessage().toLowerCase(Locale.getDefault()).contains(charText)) {
                    filteredItems.add(it);
                }
            }
        }
        notifyDataSetChanged();
    }

    private void startRDR(String dealerCode, String dealerName, String meetingLink) {
        Intent RDRIntent = new Intent(activity, DiscussionPoint1.class);
        RDRIntent.putExtra(AppConstants.DEALER_CODE, dealerCode);
        RDRIntent.putExtra(AppConstants.MEETING_LINK, meetingLink);
        RDRIntent.putExtra(AppConstants.DEALER_NAME, dealerName);
        activity.startActivity(RDRIntent);
    }

    private void setRDR(String dealerCode, String dealerName) {
        Intent intent = new Intent(activity, MTeamsLogin.class);
        intent.putExtra(AppConstants.DEALER_CODE, dealerCode);
        intent.putExtra(AppConstants.DEALER_NAME, dealerName);
        intent.putExtra(AppConstants.MEETING_TYPE, "setRDR");
        activity.startActivity(intent);
    }
}
