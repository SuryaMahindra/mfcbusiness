package rdm;

class DealerStatusModel
{
    public DealerStatusModel(String dealerName,String todayColor,String yesterdayColor,String fivethJulyColor,String fourthJulyColor) {
        this.dealerName = dealerName;
        this.todayColor = todayColor;
        this.yesterdayColor = yesterdayColor;
        this.fivethJulyColor = fivethJulyColor;
        this.fourthJulyColor = fourthJulyColor;

    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName,String todayColor,String yesterdayColor,String fivethJulyColor,String fourthJulyColor)
    {
        this.dealerName = dealerName;

    }

    private String dealerName="";
    private String todayColor="";
    private String yesterdayColor="";
    private String fivethJulyColor="";
    private String fourthJulyColor="";

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getTodayColor() {
        return todayColor;
    }

    public void setTodayColor(String todayColor) {
        this.todayColor = todayColor;
    }

    public String getYesterdayColor() {
        return yesterdayColor;
    }

    public void setYesterdayColor(String yesterdayColor) {
        this.yesterdayColor = yesterdayColor;
    }

    public String getFivethJulyColor() {
        return fivethJulyColor;
    }

    public void setFivethJulyColor(String fivethJulyColor) {
        this.fivethJulyColor = fivethJulyColor;
    }

    public String getFourthJulyColor() {
        return fourthJulyColor;
    }

    public void setFourthJulyColor(String fourthJulyColor) {
        this.fourthJulyColor = fourthJulyColor;
    }
}
