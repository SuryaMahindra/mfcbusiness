package rdm.retrofit_services;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;
public interface RetrofitInterface
{
    @Headers("Content-Type: application/json; charset=utf-8")
    @POST
    Call<Object> getFromWeb(@Body Object request, @Url String url);

    @Headers("Content-Type: application/json; charset=utf-8")
    @GET
    Call<Object> getFromWeb(@Url String url);
}
