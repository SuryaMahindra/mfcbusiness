package rdm.retrofit_services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.Global_Urls;
import com.mfcwl.mfc_dealer.retrofitconfig.BaseService;
import com.mfcwl.mfc_dealer.retrofitconfig.RetroBase;
import com.mfcwl.mfc_dealer.retrofitconfig.RetroInterface;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroBaseService
{


    public static Global_Urls global_urls = new Global_Urls("Production");  // Production,Staging,PreProduction

    public static String eventBaseURL = global_urls.getRdmEventBaseURL();
    private static String BASE_URL = eventBaseURL;


    public static final String URL_END_CREATE_EVENT = "creates";
    public static final String URL_END_EDIT_EVENT = "edit";
    public static final String URL_END_DEALER_PARTICIPANTS="fetchparticipants";
    public static final String URL_END_RESET_PASSWORD="fetchparticipants";
    public static final String URL_END_SCHEDULE_EVENT="fetch";
    public static final String URL_END_SEARCH_EVENT="search";


    public static final String POWER_BI_STAGE_URL="https://newadmin.stagemfc.com/powerbi/home/embedstockreport";
    public static final String POWER_BI_PROD_URL="https://newadmin.emfcwl.com/powerbi/home/embedstockreport";

    public static final String ASM_POWER_BI_STAGE_URL="https://newadmin.stagemfc.com/powerbi";
    public static final String ASM_POWER_BI_PROD_URL="http://newadmin.emfcwl.com/powerbi";



    protected static TokenInterceptor tokenInterceptor = new TokenInterceptor();

    private static Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create();
    private static HttpLoggingInterceptor logging = new HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BODY);
    private static OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(logging)
            .addInterceptor(tokenInterceptor)
            .build();
    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client)
            .build();

    public static RetroInterface retrofitInterface = retrofit.create(RetroInterface.class);

    private static class TokenInterceptor implements Interceptor {
        private TokenInterceptor() {

        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request initialRequest = chain.request();

            String sToken = "";
            if (sToken != null) {
                initialRequest = initialRequest.newBuilder()
                        // .addHeader("Accept", "application/json; charset=utf-8")
                        .addHeader("token", CommonMethods.getToken("token"))
                        .build();
            }

            Response response = chain.proceed(initialRequest);
            return response;
        }
    }


}
