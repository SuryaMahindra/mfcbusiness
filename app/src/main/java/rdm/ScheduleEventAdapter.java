package rdm;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mfcwl.mfc_dealer.Activity.RDR.DiscussionPoint1;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.AppConstants;
import com.mfcwl.mfc_dealer.Utility.CustomFonts;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import rdm.model.EventData;
import rdm.model.Participant;


public class ScheduleEventAdapter extends RecyclerView.Adapter<ScheduleEventAdapter.ViewHolder> {


    Activity activity;
    String TAG = ScheduleEventAdapter.class.getSimpleName();
    ArrayList<EventData> filteredEvent = new ArrayList<>();
    ArrayList<EventData> eventDataList = new ArrayList<>();

    public ScheduleEventAdapter(List<EventData> eventDataList, Activity activity) {
        this.filteredEvent.addAll(eventDataList);
        this.eventDataList.addAll(filteredEvent);
        this.activity = activity;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_item_schecdule_event, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        Log.i(TAG, "onCreateViewHolder: ");
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return filteredEvent.size();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        try {
            EventData actualData = filteredEvent.get(position);

            String eventType = actualData.getEventType();
            String dealerCode = actualData.getDealerCode();
            String dealerName = actualData.getDealerName();
            String meetingLink = actualData.getEventLocation();
            String strParticipants = "";

           /* holder.ll_eventLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (dealerCode == null || meetingLink == null) {
                        Toast.makeText(activity, "Expected action could not be done due to missing data", Toast.LENGTH_LONG).show();
                        return;

                    }
                    if (eventType != null && eventType.equalsIgnoreCase("Remote Dealer Report")) {
                        if (!dealerCode.isEmpty() && !dealerName.isEmpty() && !meetingLink.isEmpty()) {
                            Intent intent = new Intent(activity, DiscussionPoint1.class);
                            intent.putExtra(AppConstants.DEALER_CODE, dealerCode);
                            intent.putExtra(AppConstants.DEALER_NAME, dealerName);
                            intent.putExtra(AppConstants.MEETING_LINK, meetingLink);
                            activity.startActivity(intent);
                        } else {
                            Toast.makeText(activity, "Expected action could not be done due to missing data", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(activity, "Not RDR event", Toast.LENGTH_LONG).show();

                    }

                }
            });*/

            String eventTitle = actualData.getEventTitle();
            holder.tv_schedule_event_dealer_title.setText(eventTitle);

            if (actualData.getIsAllDayEvent().equalsIgnoreCase("false")) {
                String startDate = getStartDate(actualData.getEventStartDateTime());
                holder.tvStartDate.setText(startDate);
                getTimeInFormat(actualData.getEventStartDateTime(), holder.tvSEStartsTime);
                getTimeInFormat(actualData.getEventEndDateTime(), holder.tvSEEndsDate);
            } else {
                String allDay = " All Day";
                String startDate = getStartDate(actualData.getEventStartDateTime());
                holder.tvStartDate.setText(startDate + allDay);
                holder.tv_scheduleEvent_to_lbl.setVisibility(View.GONE);
                holder.tvSEEndsDate.setVisibility(View.GONE);
            }


            holder.ivEventEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, EventEditActivity.class);
                    Bundle bundle = new Bundle();
                    if (isValid(dealerCode))
                        bundle.putString("dealer_code", dealerCode);
                    else
                        bundle.putString("dealer_code", "");

                    if (isValid(dealerName))
                        bundle.putString("dealer_name", dealerName);
                    else
                        bundle.putString("dealer_name", "");

                    if (isValid(actualData.getEventType()))
                        bundle.putString("event_type", actualData.getEventType());
                    else
                        bundle.putString("event_type", "");


                    if (isValid(eventTitle))
                        bundle.putString("event_title", eventTitle);
                    else
                        bundle.putString("event_title", "");

                    if (isValid(actualData.getEventDescription()))
                        bundle.putString("event_description", actualData.getEventDescription());
                    else
                        bundle.putString("event_description", "");


                    if (isValid(actualData.getEventLocation()))
                        bundle.putString("event_location", actualData.getEventLocation());
                    else
                        bundle.putString("event_location", "");

                    if (actualData.getParticipants() != null && !actualData.getParticipants().isEmpty()) {
                        ArrayList<String> arrayList = getParticipants(actualData.getParticipants());
                        bundle.putStringArrayList("event_participants", arrayList);
                    } else
                        bundle.putString("event_participants", "");

                    if (isValid(actualData.getIsAllDayEvent()))
                        bundle.putString("is_all_day_event", actualData.getIsAllDayEvent());
                    else
                        bundle.putString("is_all_day_event", "");


                    if (isValid(actualData.getEventStartDateTime()))
                        bundle.putString("event_start", actualData.getEventStartDateTime());
                    else
                        bundle.putString("event_start", "");


                    if (isValid(actualData.getEventEndDateTime()))
                        bundle.putString("event_end", actualData.getEventEndDateTime());
                    else
                        bundle.putString("event_end", "");

                    if (isValid(String.valueOf(actualData.getEventId()))) {
                        bundle.putString("event_id", String.valueOf(actualData.getEventId()));
                    }
                    else
                        bundle.putString("event_id", "");


                    if (isValid(String.valueOf(actualData.getMteamsEventId()))) {
                        bundle.putString("meeting_id", String.valueOf(actualData.getMteamsEventId()));
                        Log.i(TAG, "onClick: "+actualData.getMteamsEventId());
                    }
                    else
                        bundle.putString("meeting_id", "");

                    bundle.putString(AppConstants.MEETING_TYPE, "startRDR");
                    intent.putExtra("intent_data", bundle);
                    activity.startActivity(intent);

                }
            });
        }
         catch (Exception exception) {
            exception.printStackTrace();
        }
    }


    private boolean isValid(String str)
    {
        if(str!=null && !str.isEmpty())
            return true;
            else
                return false;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout ll_eventLayout;
        TextView tv_schedule_event_dealer_title, tvStartDate, tvSEEndsDate, tvSEStartsTime, tv_scheduleEvent_to_lbl;
        ImageView ivEventEdit;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ll_eventLayout = itemView.findViewById(R.id.ll_schedule_event_dealer_name);
            tv_schedule_event_dealer_title = itemView.findViewById(R.id.tv_schedule_event_dealer_title);
            tvStartDate = itemView.findViewById(R.id.tv_scheduleEvent_rdr_date_and_day);
            tvSEStartsTime = itemView.findViewById(R.id.tv_scheduleEvent_rdr_start_time);
            tv_scheduleEvent_to_lbl = itemView.findViewById(R.id.tv_scheduleEvent_to_lbl);
            tvSEEndsDate = itemView.findViewById(R.id.tv_scheduleEvent_rdr_end_time);
            ivEventEdit=itemView.findViewById(R.id.ivEventEdit);
            tvStartDate.setTypeface(CustomFonts.getRobotoMedium(activity));
            tvSEStartsTime.setTypeface(CustomFonts.getRobotoMedium(activity));
            tv_scheduleEvent_to_lbl.setTypeface(CustomFonts.getRobotoMedium(activity));
            tvSEEndsDate.setTypeface(CustomFonts.getRobotoMedium(activity));

        }
    }


    @SuppressLint("SimpleDateFormat")
    private String getStartDate(String startDate) {
        String[] dateArr = startDate.split(" ");
        String date = dateArr[0];
        String dayOfWeek = "";
        String suffix = "";
        Date startDt = new Date();
        SimpleDateFormat strDateToDate = new SimpleDateFormat("yyyy-MM-dd");
        try {
            startDt = strDateToDate.parse(date);
            date = strDateToDate.format(startDt);
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        dayOfWeek = new SimpleDateFormat("EE").format(startDt);

        String formattedDate[] = date.split("-");
        suffix = getDayOfMonthSuffix(Integer.parseInt(formattedDate[2]));
        String result = formattedDate[2] + suffix + " " + dayOfWeek;
        return result;
    }

    private String getDayOfMonthSuffix(final int n) {
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    private void getTimeInFormat(String dateTime, TextView textView) {
        String textViewText = textView.getText().toString();
        String[] givenDateTime = dateTime.split(" ");
        String time = givenDateTime[1];
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:", Locale.getDefault());
        SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a", Locale.getDefault());
        Date dt;
        String formattedTime = "";
        try {
            dt = sdf.parse(time);
            if (dt != null) {
                formattedTime = sdfs.format(dt);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        textView.setText(formattedTime);
    }

    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());
        filteredEvent.clear();
        if (charText.length() == 0) {
            filteredEvent.addAll(eventDataList);
        } else {
            for (EventData event : eventDataList) {
                if (event.getDealerName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    filteredEvent.add(event);
                }
            }
        }
        notifyDataSetChanged();
    }

    private ArrayList<String> getParticipants(List<Participant> participants) {

        ArrayList<Participant> alParticipantList = new ArrayList<>();
        alParticipantList.addAll(participants);
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 0; i < alParticipantList.size(); i++) {
            arrayList.add(alParticipantList.get(i).getEmail());

        }
        return arrayList;
    }

}