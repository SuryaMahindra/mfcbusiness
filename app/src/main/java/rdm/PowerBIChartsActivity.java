package rdm;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.AppConstants;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.CustomFonts;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import im.delight.android.webview.AdvancedWebView;
import mteams.MTeamsLogin;

public class PowerBIChartsActivity extends AppCompatActivity implements AdvancedWebView.Listener, PopupMenu.OnMenuItemClickListener {

    private AdvancedWebView mWebView;
     private TextView mDealerName,tvSetRDR,tvStartRDR,tv_powerBi_rd ;
     private String dealerName,dealerCode;

    private String POWERBI_URL = "https://newadmin.stagemfc.com/powerbi/home/embedstockreport";
    private ImageView mBack,ivPowerBiList;
    public static Menu menu;
    private String TAG=PowerBIChartsActivity.class.getSimpleName();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.powerbi_charts);
        Log.i(TAG, "onCreate: ");
        mWebView = findViewById(R.id.webview);
        mDealerName =  findViewById(R.id.tvAllDealersDealerName);
        mBack = findViewById(R.id.im_backbtn);
        ivPowerBiList=findViewById(R.id.iv_list_menu);
        tvSetRDR=findViewById(R.id.tvSetRDR);
        tvStartRDR=findViewById(R.id.tvStartRDR);
        tv_powerBi_rd=findViewById(R.id.tv_powerBi_rd);
        dealerName = getIntent().getExtras().getString("DNAME");
        dealerCode = getIntent().getExtras().getString("DCODE");
        mDealerName.setText(dealerName);
        mDealerName.setTypeface(CustomFonts.getRobotoMedium(this));
        tvSetRDR.setTypeface(CustomFonts.getRobotoMedium(this));
        tvStartRDR.setTypeface(CustomFonts.getRobotoMedium(this));
        setmWebView();

        tvSetRDR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PowerBIChartsActivity.this, MTeamsLogin.class);
                intent.putExtra(AppConstants.DEALER_CODE,dealerCode);
                Log.i(TAG, "onClick: dealerCode"+dealerCode);
                intent.putExtra(AppConstants.DEALER_NAME,dealerName);
                intent.putExtra(AppConstants.MEETING_TYPE,"setRDR");
                startActivity(intent);
            }
        });

        tvStartRDR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PowerBIChartsActivity.this, MTeamsLogin.class);
                intent.putExtra(AppConstants.DEALER_CODE,dealerCode);
                intent.putExtra(AppConstants.DEALER_NAME,dealerName);
                intent.putExtra(AppConstants.MEETING_TYPE,"startRDR");
                startActivity(intent);
            }
        });
        ivPowerBiList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(PowerBIChartsActivity.this, R.style.MyPopupMenu);
                PopupMenu popupMenu = new PopupMenu(wrapper,v);
                MenuInflater inflater = popupMenu.getMenuInflater();
                inflater.inflate(R.menu.rdr_menu, popupMenu.getMenu());
                popupMenu.show();

            }
        });
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void setmWebView() {
        mWebView.setListener(this,this);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.setFocusable(true);
        mWebView.setFocusableInTouchMode(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setDatabaseEnabled(true);
        mWebView.getSettings().setAppCacheEnabled(true);
        mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        mWebView.setInitialScale(1);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);

        Log.e("url",""+ POWERBI_URL + "?Date="+ getTodayDate() + "&AMId=" + CommonMethods.getstringvaluefromkey(this, "user_id"));

        mWebView.loadUrl(POWERBI_URL + "?Date="+ getTodayDate() + "&AMId=" + CommonMethods.getstringvaluefromkey(this, "user_id"));
    }


    public String getTodayDate() {
        String lastmonth = "";
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        lastmonth = s.format(new Date(cal.getTimeInMillis()));
        return lastmonth;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.rdr_menu,menu);
        return true;
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        SpinnerManager.showSpinner(PowerBIChartsActivity.this);
       // Log.e("url", "" + url);
    }

    @Override
    public void onPageFinished(String url) {
     SpinnerManager.hideSpinner(PowerBIChartsActivity.this);
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {

    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {

    }

    @Override
    public void onExternalPageRequest(String url) {

    }

    @SuppressLint("NewApi")
    @Override
    public void onResume() {
        super.onResume();
        mWebView.onResume();

    }

    @SuppressLint("NewApi")
    @Override
    public void onPause() {
        mWebView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mWebView.onDestroy();

        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        mWebView.onActivityResult(requestCode, resultCode, intent);

    }

    @Override
    public boolean onMenuItemClick(MenuItem item)
    {
        switch  (item.getItemId()) {
            case R.id.rdr_power_bi_link_one:
                Toast.makeText(this, "List One Clicked", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.rdr_power_bi_link_two:
                Toast.makeText(this, "List Two Clicked", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.rdr_power_bi_link_three:
                Toast.makeText(this, "List Three Clicked", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.rdr_power_bi_link_four:
                Toast.makeText(this, "List Four Clicked", Toast.LENGTH_SHORT).show();
                return true;
        }

        return true;
    }
}
