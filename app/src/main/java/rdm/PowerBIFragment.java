package rdm;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.Fragment.LeadSection.LeadsFragmentSection;
import com.mfcwl.mfc_dealer.Fragment.StockFragment;
import com.mfcwl.mfc_dealer.Procurement.Fragment.ProcurementTAB;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.SalesStocks.Fragment.SalesStockTAB;
import com.mfcwl.mfc_dealer.Utility.AppConstants;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.CustomFonts;
import com.mfcwl.mfc_dealer.Utility.GAConstants;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import im.delight.android.webview.AdvancedWebView;
import mteams.MTeamsLogin;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.activity;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.iv_toggle_back_btn;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.layout_back_btn;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.navigation;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchImage;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchicon;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toggle;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toolbar;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.tv_header_title;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ASM;
import static rdm.retrofit_services.RetroBaseService.POWER_BI_PROD_URL;
import static rdm.retrofit_services.RetroBaseService.POWER_BI_STAGE_URL;


public class PowerBIFragment extends Fragment implements AdvancedWebView.Listener {

    private AdvancedWebView mWebView;
    private TextView mDealerName, tvSetRDR, tvStartRDR, tv_powerBi_rd;
    private String dealerName, dealerCode;

    //private String POWER_BI_URL = POWER_BI_STAGE_URL;//stage
    private String POWER_BI_URL = POWER_BI_PROD_URL; //prod
    private ImageView mBack, ivPowerBiList;
    public static Menu menu;
    private String TAG = PowerBIChartsActivity.class.getSimpleName();

    private Activity activity;

    public PowerBIFragment() {
        // Required empty public constructor
    }

    public PowerBIFragment(Activity activity) {
        this.activity = activity;
    }


    public static PowerBIFragment newInstance(String param1, String param2) {
        PowerBIFragment fragment = new PowerBIFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dealerName = getArguments().getString("DNAME");
            dealerCode = getArguments().getString("DCODE");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_power_b_i, container, false);
        setHasOptionsMenu(true);
        Log.i(TAG, "onCreateView: ");
        /*if (navigation.getVisibility() == View.GONE || navigation.getVisibility() == View.INVISIBLE)
        {
            navigation.setVisibility(View.VISIBLE);
        }*/
        mWebView = view.findViewById(R.id.webview);
        mDealerName = view.findViewById(R.id.tvAllDealersDealerName);
        mBack = view.findViewById(R.id.im_backbtn);
        ivPowerBiList = view.findViewById(R.id.iv_list_menu);
        tvSetRDR = view.findViewById(R.id.tvSetRDR);
        tvStartRDR = view.findViewById(R.id.tvStartRDR);
        tv_powerBi_rd = view.findViewById(R.id.tv_powerBi_rd);
        mDealerName.setText(dealerName);
        mDealerName.setTypeface(CustomFonts.getRobotoMedium(activity));
        tvSetRDR.setTypeface(CustomFonts.getRobotoMedium(activity));
        tvStartRDR.setTypeface(CustomFonts.getRobotoMedium(activity));
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setHomeAsUpIndicator(R.drawable.ic_new_hamburger_menu);
        setmWebView();

        tvSetRDR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, MTeamsLogin.class);
                intent.putExtra(AppConstants.DEALER_CODE, dealerCode);
                Log.i(TAG, "onClick: dealerCode" + dealerCode);
                intent.putExtra(AppConstants.DEALER_NAME, dealerName);
                intent.putExtra(AppConstants.MEETING_TYPE, "setRDR");
                startActivity(intent);
            }
        });

        tvStartRDR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, MTeamsLogin.class);
                intent.putExtra(AppConstants.DEALER_CODE, dealerCode);
                intent.putExtra(AppConstants.DEALER_NAME, dealerName);
                intent.putExtra(AppConstants.MEETING_TYPE, "startRDR");
                startActivity(intent);
            }
        });
        ivPowerBiList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(activity, R.style.MyPopupMenu);
                PopupMenu popupMenu = new PopupMenu(wrapper, v);
                MenuInflater inflater = popupMenu.getMenuInflater();
                inflater.inflate(R.menu.rdr_menu, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        CommonMethods.setvalueAgainstKey(activity,"dealer_code",dealerCode);
                        switch (item.getItemId()) {
                            case R.id.rdr_power_bi_link_one:
                                loadFragment(new StockFragment());
                                //loadFragment(new ASMStockFragment());
                                return true;
                            case R.id.rdr_power_bi_link_two:
                                loadFragment(new ProcurementTAB());
                                return true;
                            case R.id.rdr_power_bi_link_three:
                                loadFragment(new LeadsFragmentSection());
                                return true;
                            case R.id.rdr_power_bi_link_four:
                                loadFragment(new SalesStockTAB());
                                return true;
                        }
                        return true;
                    }
                });

                popupMenu.show();

            }
        });
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.finish();
            }
        });

        mWebView.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        searchicon.setVisibility(View.VISIBLE);
        searchImage.setImageResource(0);
        searchImage.setVisibility(View.GONE);
        tv_header_title.setVisibility(View.VISIBLE);
        tv_header_title.setTypeface(CustomFonts.getRobotoRegularTF(activity.getApplicationContext()));
        CommonMethods.setvalueAgainstKey(activity, "status","dealer_details");
        tv_header_title.setText("DEALER DETAILS");

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        MenuItem asmHome = menu.findItem(R.id.asmHome);
        MenuItem add_image = menu.findItem(R.id.add_image);
        MenuItem share = menu.findItem(R.id.share);
        MenuItem delete_fea = menu.findItem(R.id.delete_fea);
        MenuItem stock_fil_clear = menu.findItem(R.id.stock_fil_clear);
        MenuItem notification_bell = menu.findItem(R.id.notification_bell);
        MenuItem notification_cancel = menu.findItem(R.id.notification_cancel);


        asmHome.setVisible(false);
        add_image.setVisible(false);
        share.setVisible(false);
        delete_fea.setVisible(false);
        stock_fil_clear.setVisible(false);
        notification_bell.setVisible(true);
        notification_cancel.setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void setmWebView() {
        mWebView.setListener(activity, this);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.setFocusable(true);
        mWebView.setFocusableInTouchMode(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setDatabaseEnabled(true);
        mWebView.getSettings().setAppCacheEnabled(true);
        mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        mWebView.setInitialScale(1);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);

        mWebView.loadUrl(POWER_BI_URL + "?Date="+getTodayDate() + "&AMId=" + CommonMethods.getstringvaluefromkey(activity, "user_id")+"&dealercode="+dealerCode);
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {

       // Log.e("url", "" + url);

    }

    @Override
    public void onPageFinished(String url) {

    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {

    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {

    }

    @Override
    public void onExternalPageRequest(String url) {

    }

    @Override
    public void onResume() {
        super.onResume();

        if(CommonMethods.getstringvaluefromkey(activity,"user_type").equalsIgnoreCase(USERTYPE_ASM))
        {
           /* toggle.setDrawerIndicatorEnabled(false);
            toggle.setHomeAsUpIndicator(R.drawable.ic_new_hamburger_menu);
            toolbar.setBackgroundColor(getResources().getColor(R.color.all_dealers_fragment_bg));*/
            tv_header_title.setText("DEALER DETAILS");
            toolbar.setBackgroundColor(getResources().getColor(R.color.all_dealers_fragment_bg));
            tv_header_title.setTextColor(getResources().getColor(R.color.black));
            layout_back_btn.setVisibility(View.GONE);
            iv_toggle_back_btn.setVisibility(View.GONE);
            /*navigation.setVisibility(View.VISIBLE);*/
            searchImage.setVisibility(View.GONE);
            Application.getInstance().logASMEvent(GAConstants.AM_DEALER_ID_DEALER_DETAILS,CommonMethods.getstringvaluefromkey(activity,"user_id")+System.currentTimeMillis()+""+CommonMethods.getstringvaluefromkey(activity, "device_id"));
        }
        mWebView.onResume();

    }



    public String getTodayDate() {
        String lastmonth = "";
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        lastmonth = s.format(new Date(cal.getTimeInMillis()));
        return lastmonth;
    }



    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem asm_mail = menu.findItem(R.id.asm_mail);

        MenuItem asmHome = menu.findItem(R.id.asmHome);
        MenuItem add_image = menu.findItem(R.id.add_image);
        MenuItem share = menu.findItem(R.id.share);
        MenuItem delete_fea = menu.findItem(R.id.delete_fea);
        MenuItem stock_fil_clear = menu.findItem(R.id.stock_fil_clear);
        MenuItem notification_bell = menu.findItem(R.id.notification_bell);
        MenuItem notification_cancel = menu.findItem(R.id.notification_cancel);

        asm_mail.setVisible(false);
        asmHome.setVisible(false);
        add_image.setVisible(false);
        share.setVisible(false);
        delete_fea.setVisible(false);
        stock_fil_clear.setVisible(false);
        notification_bell.setVisible(true);
        notification_cancel.setVisible(false);


        super.onPrepareOptionsMenu(menu);
    }

    @SuppressLint("NewApi")
    @Override
    public void onPause() {
        mWebView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mWebView.onDestroy();

        super.onDestroy();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        mWebView.onActivityResult(requestCode, resultCode, intent);

    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }


    private void loadFragment(Fragment mFragment) {
        Bundle mBundle = new Bundle();
        mBundle.putString("DNAME", dealerName);
        mBundle.putString("DCODE", dealerCode);
        mFragment.setArguments(mBundle);
        switchContent(R.id.fillLayout, mFragment);
    }

    public void switchContent(int id, Fragment fragment) {
        if (activity == null)
            return;
        if (activity instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) activity;
            Fragment frag = fragment;
            mainActivity.switchContent(id, frag);
        }

    }
}