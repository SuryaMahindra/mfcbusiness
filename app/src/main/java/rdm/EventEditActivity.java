package rdm;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.Activity.RDR.DiscussionPoint1;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.AppConstants;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.CustomFonts;
import com.mfcwl.mfc_dealer.Utility.EventDialog;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.retrofitconfig.dealerreport.DealerReportRequest;
import com.microsoft.identity.client.AuthenticationCallback;
import com.microsoft.identity.client.IAuthenticationResult;
import com.microsoft.identity.client.exception.MsalClientException;
import com.microsoft.identity.client.exception.MsalException;
import com.microsoft.identity.client.exception.MsalServiceException;
import com.microsoft.identity.client.exception.MsalUiRequiredException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import imageeditor.base.BaseActivity;
import mteams.Attendee;
import mteams.AuthenticationHelper;
import mteams.Body;
import mteams.CreateEventRequest;
import mteams.CreateEventService;
import mteams.EmailAddress;
import mteams.End;
import mteams.Location;
import mteams.MTeamsLogin;
import mteams.MTeamsResponse;
import mteams.OnlineMeeting;
import mteams.Start;
import rdm.model.CreateEventResponse;
import rdm.model.DealerParticipantData;
import rdm.model.DealerParticipantsRequest;
import rdm.model.EventEditRequest;
import rdm.model.Participant;
import remotedealer.adapter.ParticipantAdapter;
import remotedealer.rdrform.ui.EventListFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static rdm.retrofit_services.RetroBaseService.URL_END_DEALER_PARTICIPANTS;
import static rdm.retrofit_services.RetroBaseService.URL_END_EDIT_EVENT;
import static rdm.retrofit_services.RetroBaseService.retrofitInterface;

public class EventEditActivity extends BaseActivity implements Callback<Object>, ParticipantAdapter.ParticipantCallBack {

    private TextView tvEventType;
    private TextView tvStartDate;
    private TextView tvEndsDate;
    private EditText etEEParticipants, etEditEventDuration;
    private ImageButton ivEEParticipants;
    LinearLayout layoutStarts, layoutEnds;
    EditText etTitle, etLocation;
    int selectedHour, selectedMinutes;
    String strTitle = "", strEDescription = "", strEventID = "", strLocation = "", strEventType = "", strParticipants = "", strAllDay = "", strStarts = "", strEnds = "";
    ImageView iv_editEvent_back;
    private ArrayList<Participant> participantArrayList = new ArrayList<>();
    String dealerCode = "", dealerName = "", meetingType = "", mTeamsMeetingId = "";
    RecyclerView rvEEParticipants;
    private ArrayList<String> selectedParticipantList = new ArrayList<>();
    String startData = "", endData = "", startDate = "", endDate = "", startTime = "", endTime = "";

    final Calendar cal = Calendar.getInstance();
    private String format = "yyyy-MM-dd";
    private String serverDateFormat = "yyyy-MM-dd'T'HH:mm:ss";
    private SimpleDateFormat sdf = new SimpleDateFormat(format);
    private SimpleDateFormat serverFormat = new SimpleDateFormat(serverDateFormat);
    private Button btnEditLead, btnEditEventQuickAdd;
    private AuthenticationHelper mAuthHelper = null;
    private String strStartDate = "", strEndDate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_edit);
        Log.i(TAG, "onCreate: ");
        initMTeams();
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("intent_data");
        dealerCode = bundle.getString("dealer_code");
        dealerName = bundle.getString("dealer_name");
        strTitle = bundle.getString("event_title");
        strEDescription = bundle.getString("event_description");
        strLocation = bundle.getString("event_location");
        strEventType = bundle.getString("event_type");
        strEventID = bundle.getString("event_id");
        strAllDay = bundle.getString("is_all_day_event");
        strStarts = bundle.getString("event_start");
        Log.i(TAG, "onCreate: " + strStarts);
        strEnds = bundle.getString("event_end");
        Log.i(TAG, "onCreate: " + strEnds);
        meetingType = bundle.getString(AppConstants.MEETING_TYPE);
        mTeamsMeetingId = bundle.getString("meeting_id");
        Log.i(TAG, "onCreate: MeetingId" + mTeamsMeetingId);

        try {
            if (bundle.getStringArrayList("event_participants") != null && !bundle.getStringArrayList("event_participants").isEmpty()) {
                selectedParticipantList.addAll(bundle.getStringArrayList("event_participants"));
            }
        } catch (Exception exception) {
            selectedParticipantList.addAll(new ArrayList<>());
            exception.printStackTrace();
        }

        initViews();

        if (meetingType.equalsIgnoreCase("setRDR") || meetingType.equalsIgnoreCase("startRDR")) {

            //etTitle.setText("" + dealerName + " | " + " RDR " + " | " + sdf.format(cal.getTime()));
            etTitle.setFocusable(false);
            etTitle.setFocusableInTouchMode(false);
            etLocation.setFocusableInTouchMode(false);
            etLocation.setFocusable(false);

        }
        if (strParticipants.isEmpty()) {
            getParticipantFromServer();
        } else {
            strParticipants = strParticipants.substring(3);
            Log.i(TAG, "onCreate: " + strParticipants);

        }

    }

    private void getParticipantFromServer() {
        SpinnerManager.showSpinner(EventEditActivity.this);
        retrofitInterface.getFromWeb(getDealerCode(), URL_END_DEALER_PARTICIPANTS).enqueue(this);
    }

    private DealerParticipantsRequest getDealerCode() {
        DealerParticipantsRequest request = new DealerParticipantsRequest();
        request.setDealerCode(dealerCode);
        Log.i(TAG, "getDealerCode: " + request.getDealerCode());
        return request;
    }


    private void setDefaultDateTime() {
        cal.add(Calendar.MINUTE, 10);
        String startTime = serverFormat.format(cal.getTime());
        cal.add(Calendar.MINUTE, 60);
        String endTime = serverFormat.format(cal.getTime());

    }

    private void initViews() {
        tvEventType = findViewById(R.id.tvEditEventType);
        iv_editEvent_back = findViewById(R.id.iv_edit_Event_back);
        tvStartDate = findViewById(R.id.tvEditEventStartTimeVal);
        tvEndsDate = findViewById(R.id.tvEditEventToTimeVal);
        etEEParticipants = findViewById(R.id.etEEParticipants);
        ivEEParticipants = findViewById(R.id.iv_addEEParticipants);
        etTitle = findViewById(R.id.etEditEventTitle);
        layoutStarts = findViewById(R.id.llEditEventStartTime);
        layoutEnds = findViewById(R.id.llEditEventEndTime);
        etLocation = findViewById(R.id.etEELocation);
        rvEEParticipants = findViewById(R.id.rvEEParticipants);
        etEditEventDuration = findViewById(R.id.etEditEventDuration);
        btnEditEventQuickAdd = findViewById(R.id.btnEditEventQuickAdd);
        btnEditLead = findViewById(R.id.btnEditLead);
        etTitle.setTypeface(CustomFonts.getRobotoRegularTF(this));
        etLocation.setTypeface(CustomFonts.getRobotoRegularTF(this));
        tvStartDate.setTypeface(CustomFonts.getRobotoMedium(this));
        tvEndsDate.setTypeface(CustomFonts.getRobotoMedium(this));
        etTitle.setText(strTitle);
        tvEventType.setText(strEventType);
        Log.i(TAG, "initViews: " + strStarts);
        if (!strStarts.equals("") && strStarts != null) {
            String arr[] = strStarts.split(" ");
            tvStartDate.setText(arr[1].substring(0, 5));
            startDate = arr[0];
            startTime = arr[1];
            startData = startDate + "T" + startTime;
            Log.i(TAG, "initViews: " + startData);

            if (strEnds.equals("")) {
                etEditEventDuration.setText(startDate);
                Log.i(TAG, "initViews: " + arr[1].substring(0, 5));
                strEnds = arr[1].substring(0, 5);
                endData = startData;
                Log.i(TAG, "initViews: " + endData);
            } else {
                String arr1[] = strEnds.split(" ");
                endDate = arr1[0];
                endTime = arr1[1].substring(0, 5);
                if (startDate.equals(endDate)) {
                    etEditEventDuration.setText(startDate);
                    tvEndsDate.setText(endTime);
                    endData = endDate + "T" + arr1[1];
                    Log.i(TAG, "initViews: " + endData);

                } else {
                    etEditEventDuration.setText(startDate + " To " + endDate);
                    tvEndsDate.setText(endTime);
                    endData = endDate + "T" + arr1[1];
                    Log.i(TAG, "initViews: " + endData);
                }
            }
        }
        etLocation.setText(strLocation);

        if (!selectedParticipantList.isEmpty()) {
            addParticipantsToString(selectedParticipantList);
        }
        iv_editEvent_back.setOnClickListener(this);
        btnEditEventQuickAdd.setOnClickListener(this);
        btnEditLead.setOnClickListener(this);
        etEditEventDuration.setOnClickListener(this);
        ivEEParticipants.setOnClickListener(this);
        etEEParticipants.setOnClickListener(this);
        layoutStarts.setOnClickListener(this);
        layoutEnds.setOnClickListener(this);


    }

    private void addParticipantsToString(ArrayList<String> selectedParticipantList) {
        for (int i = 0; i < selectedParticipantList.size(); i++) {
            if (selectedParticipantList.get(i) != null && !selectedParticipantList.get(i).isEmpty()) {
                strParticipants = strParticipants + " | " + selectedParticipantList.get(i);
            }
        }
        attachToAdapter();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_edit_Event_back:
                finish();
                break;
            case R.id.btnEditEventQuickAdd:

                if (!strParticipants.isEmpty()) {

                    if (validate()) {

                        if (!strEventType.equalsIgnoreCase("Dealer Visit") || !strEventType.equalsIgnoreCase("Training") || !tvEventType.getText().toString().equalsIgnoreCase("Others")) {
                            String mteamstoken = CommonMethods.getstringvaluefromkey(EventEditActivity.this, "mteamstoken");
                            if (mteamstoken != null && !mteamstoken.equalsIgnoreCase("")) {
                                editMTeamsEvent(strTitle, strParticipants);
                            } else {
                                Toast.makeText(EventEditActivity.this, "Please login with MTeams", Toast.LENGTH_LONG).show();
                                doSilentSignIn();
                            }
                        } else {
                            callRetroService();
                        }
                    }
                } else {
                    Toast.makeText(this, "Please add participants", Toast.LENGTH_LONG).show();
                }
                break;
                case R.id.btnEditLead:
                    if (!strParticipants.isEmpty()) {

                        if (validate()) {

                            if (strEventType.equalsIgnoreCase("Dealer Visit") || strEventType.equalsIgnoreCase("Training") || strEventType.equalsIgnoreCase("Others")) {
                                if (strEDescription.equalsIgnoreCase("Offline Meeting")) {
                                    callRetroService();
                                } else {
                                    invokeMTeamsEvent();
                                }
                            } else {
                                invokeMTeamsEvent();
                            }
                        }
                    } else {
                        Toast.makeText(this, "Please add participants", Toast.LENGTH_LONG).show();
                    }
                    break;
                case R.id.etEditEventDuration:
                    try {
                        EventDialog postCalendarDialog = new EventDialog(
                                EventEditActivity.this, this,
                                "",
                                (startDateString, endDateString, dateType) -> {
                                    if (!startDateString.equals(endDateString)) {
                                        etEditEventDuration.setText(startDateString + " To " + endDateString);
                                        strStartDate = startDateString;
                                        startDate = startDateString;
                                        endDate = endDateString;
                                        strEndDate = endDateString;
                                    } else {
                                        etEditEventDuration.setText(startDateString);
                                        strStartDate = startDateString;
                                        startDate = startDateString;
                                        endDate = endDateString;
                                        strEndDate = endDateString;
                                    }
                                });
                        postCalendarDialog.setCancelable(false);
                        postCalendarDialog.show();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                    break;
                case R.id.iv_addEEParticipants:
                    if (!etEEParticipants.getText().toString().isEmpty()) {
                        String participantVal = etEEParticipants.getText().toString();
                        if (isValidEmail(participantVal)) {
                            selectedParticipantList.add(participantVal);
                            attachToAdapter();
                            updateStrParticipants();
                            etEEParticipants.setText("");
                        } else {
                            Toast.makeText(EventEditActivity.this, "Please enter valid EmailId", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(EventEditActivity.this, "Please enter EmailId", Toast.LENGTH_LONG).show();
                    }

                    break;

                case R.id.etEEParticipants:
                    generateParticipantDialog(participantArrayList);
                    break;

                case R.id.llEditEventStartTime:
                    setTimeValue(tvStartDate);
                    break;

                case R.id.llEditEventEndTime:
                    setTimeValue(tvEndsDate);
                    break;
            }
        }

        private void invokeMTeamsEvent () {
            if (EventListFragment.preferenceEManager != null && EventListFragment.preferenceEManager.readString("mteamstoken", "") != null) {

                String mteamstoken = EventListFragment.preferenceEManager.readString("mteamstoken", "");

                if (mteamstoken != null && !mteamstoken.equalsIgnoreCase("")) {
                    editMTeamsEvent(strTitle, strParticipants);
                } else {
                    Toast.makeText(EventEditActivity.this, "Please login with MTeams", Toast.LENGTH_LONG).show();
                    doSilentSignIn();
                }
            } else {
                Toast.makeText(EventEditActivity.this, "Please login with MTeams", Toast.LENGTH_LONG).show();
                doSilentSignIn();
            }
        }

        private void callRetroService () {
            retrofitInterface.getEventFromWeb(getEditEventRequest(), URL_END_EDIT_EVENT).enqueue(EventEditActivity.this);
        }

        private DealerReportRequest getRequest () {
            DealerReportRequest request = new DealerReportRequest();
            request.dealerCode = dealerCode;
            request.followUpDate = getTimeMMddyyy();
            if (DiscussionPoint1.meetinglink.isEmpty()) {
                request.videoUrl = "";
            } else {
                request.videoUrl = DiscussionPoint1.meetinglink;
            }

            request.points = discussionPoints;
            request.isOperational = "Yes";

            return request;
        }

        private EventEditRequest getEditEventRequest () {
            EventEditRequest rdmEventEditReq = new EventEditRequest();
            rdmEventEditReq.setDealerCode(dealerCode);
            rdmEventEditReq.setEventTitle(strTitle);
            rdmEventEditReq.setEventDescription(strEDescription);
            rdmEventEditReq.setEventLocation(strLocation);
            rdmEventEditReq.setIsAllDayEvent(strAllDay);
            Log.i(TAG, "getCreateEventRequest: AllDay" + strAllDay);
            rdmEventEditReq.setEventStartDateTime(startData.replace("T", " "));
            rdmEventEditReq.setEventEndDateTime(endData.replace("T", " "));
            rdmEventEditReq.setEventType(strEventType);
            rdmEventEditReq.setEventId(strEventID);
            rdmEventEditReq.setMteamsEventId(mTeamsMeetingId);
            rdmEventEditReq.setParticipants(getParticipants());
            return rdmEventEditReq;

        }

        private List<Participant> getParticipants () {


            // need to add selectedParticipantList here

        /*Log.i(TAG, "getParticipants: " + strParticipants);
        String[] partNameArr = strParticipants.split("\\|");
        Log.i(TAG, "getParticipants: " + partNameArr.length);*/
            ArrayList<Participant> arrayList = new ArrayList<>();
            for (int i = 0; i < selectedParticipantList.size(); i++) {
                Participant participant = new Participant();
                participant.setEmail(selectedParticipantList.get(i));
                arrayList.add(participant);
            }

            return arrayList;

        }

        private boolean validate () {
            boolean result = true;
            String dateVal=etEditEventDuration.getText().toString();
            strEventType = tvEventType.getText().toString();
            strLocation = etLocation.getText().toString();
            startData = startDate + "T" + tvStartDate.getText().toString() + ":00";
            endData = endDate + "T" + tvEndsDate.getText().toString() + ":00";
            if ((strParticipants.equals("")) || dateVal.equals("")|| startData.equals("") || endData.equals("") || tvStartDate.getText().toString().equals("") || tvEndsDate.getText().toString().equals("")) {
                result = false;
                Toast.makeText(EventEditActivity.this, "Please fill all fields", Toast.LENGTH_LONG).show();
            }

            return result;
        }

        private void setTimeValue (TextView textView){
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            String getCurrentTime = sdf.format(cal.getTime());


            TimePickerDialog timePickerDialog = new TimePickerDialog(this, R.style.EventDatePicker,
                    new TimePickerDialog.OnTimeSetListener() {

                        @SuppressLint("NewApi")
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            Calendar dateTime = Calendar.getInstance();
                            dateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            dateTime.set(Calendar.MINUTE, minute);

                            int hourOfDayVal = hourOfDay;
                            String hourOfDayValLen = String.valueOf(hourOfDayVal);
                            int minuteVal = minute;
                            Log.i(TAG, "onTimeSet: minute" + minute);
                            String minuteValLen = String.valueOf(minuteVal);
                            Log.i(TAG, "onTimeSet: minuteValLen" + minuteValLen);


                            if (hourOfDayValLen.length() == 1) {
                                hourOfDayValLen = "0" + hourOfDayValLen;
                            }
                            if (minuteValLen.length() == 1) {
                                minuteValLen = "0" + minuteValLen;
                                Log.i(TAG, "onTimeSet: minuteValLen after adding 0" + minuteValLen);
                            }

                            boolean res = isTodayDate(startDate) && isTodayDate(endDate);
                            String currentTime = hourOfDayValLen + ":" + minuteValLen;

                            if (res && (!CommonMethods.selectedTimeIsValid(getCurrentTime, currentTime))) {
                                Toast.makeText(EventEditActivity.this, "Please select valid time", Toast.LENGTH_LONG).show();
                                textView.setText("");
                            } else {

                                if (textView.getId() == R.id.tvEditEventStartTimeVal) {

                                    if (!hourOfDayValLen.equals("")) {
                                        if (isTodayDate(startDate) && !CommonMethods.selectedTimeIsValid(getCurrentTime, currentTime)) {

                                            Toast.makeText(EventEditActivity.this, "Please select valid time", Toast.LENGTH_LONG).show();
                                            textView.setText("");
                                        } else {
                                            strStarts = strStartDate + " " + hourOfDayValLen + ":" + minuteValLen + ":00";
                                            startTime = hourOfDayValLen + ":" + minuteValLen + ":00";
                                            startData = startDate + "T" + startTime;
                                            textView.setText(hourOfDayValLen + ":" + minuteValLen);
                                        }

                                    } else
                                        textView.setText("");


                                } else if (textView.getId() == R.id.tvEditEventToTimeVal) {


                                    if (isValidEndTime(hourOfDayValLen + ":" + minuteValLen)) {
                                        Log.i(TAG, "onTimeSet: " + isValidEndTime(hourOfDayValLen + ":" + minuteValLen));
                                        if (!hourOfDayValLen.equals("")) {
                                            strEnds = strEndDate + " " + hourOfDayValLen + ":" + minuteValLen + ":00";
                                            endTime = hourOfDayValLen + ":" + minuteValLen + ":00";
                                            endData = endDate + "T" + endTime;
                                            textView.setText(hourOfDayValLen + ":" + minuteValLen);
                                        } else
                                            textView.setText("");
                                    } else {
                                        hourOfDayValLen = "";
                                        minuteValLen = "";
                                        Toast.makeText(EventEditActivity.this, "Please select valid time", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            }
                        }
                    }, selectedHour, selectedMinutes, false);
            timePickerDialog.show();
        }


        private boolean isTodayDate (String dateData){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                return sdf.parse(dateData).equals(sdf.parse(CommonMethods.getTodayDate(EventEditActivity.this)));
            } catch (ParseException e) {

                e.printStackTrace();
                return false;
            }
        }

        private void generateParticipantDialog (List < Participant > participantArray) {
            Log.i(TAG, "generateParticipantDialog: list Size " + participantArray.size());
            if (participantArray.size() > 0) {
                String[] strArray = new String[participantArray.size()];
                for (int i = 0; i < strArray.length; i++) {
                    strArray[i] = participantArray.get(i).getEmail();
                    Log.i(TAG, "generateParticipantDialog: " + strArray[i]);
                }
                Log.i(TAG, "generateParticipantDialog: Array length --> " + strArray.length);

                setDialogMultiSelectionCheck(etEEParticipants.getId(), "Participants", strArray);
            }
        }


        private void initMTeams () {
            mAuthHelper = AuthenticationHelper.getInstance(getApplicationContext());
            doSilentSignIn();
            //  preferenceManager = new PreferenceManager(this);
        }


        private void doInteractiveSignIn () {
            mAuthHelper.acquireTokenInteractively(this, getAuthCallback());
        }

        private boolean isValidEndTime (String endTime){
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
            Date stTime = Calendar.getInstance().getTime(), edTime = Calendar.getInstance().getTime();
            try {
                stTime = sdf.parse(tvStartDate.getText().toString());
                edTime = sdf.parse(endTime);
                Log.i(TAG, "isValidEndTime: " + stTime + "----->" + edTime);
            } catch (ParseException exception) {
                exception.printStackTrace();
            }
            if (edTime.before(stTime)) {
                Log.i(TAG, "isValidEndTime: " + edTime.before(stTime));
                return false;
            } else {
                Log.i(TAG, "isValidEndTime: " + edTime.before(stTime));
                return true;
            }
        }
        private void doSilentSignIn () {
            mAuthHelper.acquireTokenSilently(getAuthCallback());
        }


        public AuthenticationCallback getAuthCallback () {
            return new AuthenticationCallback() {

                @Override
                public void onSuccess(IAuthenticationResult authenticationResult) {
                    // Log the token for debug purposes
                    String accessToken = authenticationResult.getAccessToken();
                    Log.d("AUTH", String.format("Access token: %s", accessToken));

                    EventListFragment.preferenceEManager.writeString("mteamstoken", accessToken);

                }

                @Override
                public void onError(MsalException exception) {
                    // Check the type of exception and handle appropriately
                    if (exception instanceof MsalUiRequiredException) {
                        Log.d("AUTH", "Interactive login required");
                        doInteractiveSignIn();

                    } else if (exception instanceof MsalClientException) {
                        if (exception.getErrorCode() == "no_current_account") {
                            Log.d("AUTH", "No current account, interactive login required");
                            doInteractiveSignIn();
                        } else {
                            // Exception inside MSAL, more info inside MsalError.java
                            Log.e("AUTH", "Client error authenticating", exception);
                        }
                    } else if (exception instanceof MsalServiceException) {
                        // Exception when communicating with the auth server, likely config issue
                        Log.e("AUTH", "Service error authenticating", exception);

                    }

                }

                @Override
                public void onCancel() {
                    // User canceled the authentication
                    Log.d("AUTH", "Authentication canceled");

                }
            };
        }


        private void editMTeamsEvent (String title, String participants){

            CreateEventRequest request = new CreateEventRequest();
            request.setSubject(title);
            request.setIsOnlineMeeting(true);
            request.setOnlineMeetingProvider("teamsForBusiness");

            Body b = new Body();
            b.setContent(title);
            b.setContentType("HTML");

            request.setBody(b);

            Start st = new Start();
            st.setDateTime(startData);
            st.setTimeZone(AppConstants.TIME_ZONE);
            request.setStart(st);

            End et = new End();
            et.setDateTime(endData);
            et.setTimeZone(AppConstants.TIME_ZONE);
            request.setEnd(et);

            Location l = new Location();
            l.setDisplayName("Virtual location ");

            request.setLocation(l);

            List<Attendee> at = new ArrayList<>();
            if (strParticipants.contains("|")) {
                String[] partNameArr = strParticipants.split("\\|");

                for (int i = 0; i < partNameArr.length; i++) {
                    Attendee at1 = new Attendee();
                    EmailAddress em = new EmailAddress();
                    em.setAddress(partNameArr[i]);
                    at1.setEmailAddress(em);
                    at1.setType("required");
                    at.add(at1);
                }
            } else {
                Attendee at1 = new Attendee();
                EmailAddress em = new EmailAddress();
                em.setAddress(participants);
                at1.setEmailAddress(em);
                at1.setType("required");
                at.add(at1);
            }
            Log.i(TAG, "createMteamsEvent: " + at.size());
            for (int i = 0; i < at.size(); i++) {
                Log.i(TAG, "createMteamsEvent: " + at.get(i).toString());
            }
            request.setAttendees(at);

            if (mTeamsMeetingId != null && !mTeamsMeetingId.isEmpty()) {
                sendEventRequest(request);
            } else {
                Toast.makeText(EventEditActivity.this, "Event Id not found. So you cannot edit this event!", Toast.LENGTH_SHORT).show();
            }

        }


        private void sendEventRequest (CreateEventRequest mRequest){
            SpinnerManager.showSpinner(EventEditActivity.this);
            CreateEventService.updateEvent(mRequest, mTeamsMeetingId, new HttpCallResponse() {
                @Override
                public void OnSuccess(Object obj) {
                    SpinnerManager.hideSpinner(EventEditActivity.this);
                    try {
                        Response<MTeamsResponse> mData = (Response<MTeamsResponse>) obj;
                        MTeamsResponse teamsResponse = mData.body();
                        OnlineMeeting meeting = teamsResponse.getOnlineMeeting();

                        if (teamsResponse != null && meeting != null) {
                            strLocation = meeting.getJoinUrl();
                            etLocation.setText(strLocation);
                            strEDescription = "Created with MTeams";
                            strAllDay = "false";
                            callRetroService();
                        } else {
                            doInteractiveSignIn();
                        }

                    } catch (Exception e) {
                        Toast.makeText(EventEditActivity.this, "You cannot edit past event!", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }

                @Override
                public void OnFailure(Throwable mThrowable) {
                    SpinnerManager.hideSpinner(EventEditActivity.this);
                    mThrowable.printStackTrace();

                }
            });


        }


        @Override
        public void onResponse (Call < Object > call, Response < Object > response){
            SpinnerManager.hideSpinner(EventEditActivity.this);
            try {

                String url = call.request().url().toString();

                if (url.contains(URL_END_DEALER_PARTICIPANTS)) {

                    String strRes = new Gson().toJson(response.body());
                    Log.i(TAG, "onResponse: " + strRes);

                    try {
                        DealerParticipantData dealerParticipantData = new Gson().fromJson(strRes, DealerParticipantData.class);
                        if (dealerParticipantData.getParticipants() != null || dealerParticipantData.getParticipants().size() > 0) {
                            addParticipantsToString(dealerParticipantData.getParticipants());
                        } else {
                            etEEParticipants.setText("No Participants available");
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                } else if (url.contains(URL_END_EDIT_EVENT)) {
                    String strRes = new Gson().toJson(response.body());
                    Log.i(TAG, "onResponse: " + strRes);
                    CreateEventResponse createEventResponse = new Gson().fromJson(strRes, CreateEventResponse.class);
                    try {
                        if (createEventResponse.getIsSuccess()) {
                            Toast.makeText(EventEditActivity.this, createEventResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            finish();
                            getFragmentManager().findFragmentById(R.id.eventListFragment);

                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }

                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }

        private void addParticipantsToString (List < Participant > participants) {
            for (int i = 0; i < participants.size(); i++) {
                if (participants.get(i).getEmail() != null && !participants.get(i).getEmail().isEmpty()) {
                    selectedParticipantList.add(participants.get(i).getEmail());
                    strParticipants = strParticipants + " | " + selectedParticipantList.get(i);
                } else
                    participants.remove(i);
            }
            attachToAdapter();

        }

        @Override
        public void onFailure (Call < Object > call, Throwable t){

        }

        private boolean isValidEmail (String participantVal){
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
            if (participantVal.matches(emailPattern)) {
                return true;
            } else {
                return false;
            }
        }

        private void attachToAdapter () {
            ParticipantAdapter participantAdapter = new ParticipantAdapter(selectedParticipantList, this, this);
            rvEEParticipants.setLayoutManager(
                    new LinearLayoutManager(this));
            rvEEParticipants.setAdapter(participantAdapter);
        }

        @Override
        public void updateParticipantList (ArrayList < String > eventParticipants) {
            selectedParticipantList.clear();
            selectedParticipantList = eventParticipants;
            attachToAdapter();
            updateStrParticipants();

        }

        private void updateStrParticipants () {
            strParticipants = "";
            for (int i = 0; i < selectedParticipantList.size(); i++) {
                if (i == 0) {
                    strParticipants = selectedParticipantList.get(i);
                } else {
                    strParticipants = strParticipants + "|" + selectedParticipantList.get(i);
                }
            }
        }


        @Override
        protected void onResume () {

            super.onResume();
            SpinnerManager.hideSpinner(this);

        }
    }