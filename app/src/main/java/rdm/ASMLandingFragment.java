package rdm;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mfcwl.mfc_dealer.ASMModel.dasboardRes;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Controller.Application;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.CustomFonts;
import com.mfcwl.mfc_dealer.Utility.GAConstants;
import com.mfcwl.mfc_dealer.Utility.GlobalText;
import com.mfcwl.mfc_dealer.Utility.Helper;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.retrofitconfig.DasboardStatusServices;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import im.delight.android.webview.AdvancedWebView;
import rdm.model.Dealer;
import rdm.model.DealerResponseData;
import rdm.services.LandingRequest;
import rdm.services.RDMService;
import retrofit2.Response;

import static com.mfcwl.mfc_dealer.Activity.MainActivity.hideOption;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.navigation;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchImage;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.searchicon;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.toolbar;
import static com.mfcwl.mfc_dealer.Activity.MainActivity.tv_header_title;
import static com.mfcwl.mfc_dealer.Utility.GlobalText.USERTYPE_ASM;
import static rdm.retrofit_services.RetroBaseService.ASM_POWER_BI_PROD_URL;
import static rdm.retrofit_services.RetroBaseService.ASM_POWER_BI_STAGE_URL;

public class ASMLandingFragment extends Fragment implements AdvancedWebView.Listener {

    Activity activity;
    TextView tvPendingTaskCount, tvMeetingsTodayCount, tvWRPendingCount, tvDealerStatusLbl;

    private RecyclerView rvDealerStatus;

    private AdvancedWebView mWebView;

    //private String POWER_BI_URL = ASM_POWER_BI_STAGE_URL; //stage
    private String POWER_BI_URL = ASM_POWER_BI_PROD_URL; //prod

    private String TAG = ASMLandingFragment.class.getSimpleName();
    List<dasboardRes> liveList;
    MenuItem asm_mail;


    public ASMLandingFragment() {

    }


    public ASMLandingFragment(Activity activity) {
        this.activity = activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_a_s_m_landing, container, false);
        Log.i(TAG, "onCreateView: ");
        setHasOptionsMenu(true);
        String mASMId = CommonMethods.getstringvaluefromkey(activity, "user_id");
        tvDealerStatusLbl = view.findViewById(R.id.tvDealerStatusLbl);
        tvPendingTaskCount = view.findViewById(R.id.tvPendingTaskCount);
        tvMeetingsTodayCount = view.findViewById(R.id.tvMeetingsTodayCount);
        tvWRPendingCount = view.findViewById(R.id.tvWRPendingCount);
        if (CommonMethods.getstringvaluefromkey(activity, "user_type").equalsIgnoreCase(USERTYPE_ASM)) {
            MainActivity.toolbar.setBackgroundColor(getResources().getColor(R.color.all_dealers_fragment_bg));
        }
        liveList = new ArrayList<>();
        if(activity!=null)
        {
            tvDealerStatusLbl.setTypeface(CustomFonts.getRobotoBold(activity.getApplicationContext()));
            tvPendingTaskCount.setTypeface(CustomFonts.getRobotoMedium(activity.getApplicationContext()));
            tvMeetingsTodayCount.setTypeface(CustomFonts.getRobotoMedium(activity.getApplicationContext()));
            tvWRPendingCount.setTypeface(CustomFonts.getRobotoMedium(activity.getApplicationContext()));
        }


        rvDealerStatus = view.findViewById(R.id.asm_dealer_status_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        rvDealerStatus.setLayoutManager(layoutManager);


        if (CommonMethods.isInternetWorking(getActivity())) {
            dealerNameReq();
        } else {
            CommonMethods.alertMessage(getActivity(), GlobalText.CHECK_NETWORK_CONNECTION);
        }


        setmWebView(view);
        LandingRequest req = new LandingRequest();
        req.setAreaManagerId("");
        req.setUserId(mASMId);
        req.setStartDate(CommonMethods.getTodayDate(activity));
        fetchData(req);

        try {

            searchicon.setVisibility(View.VISIBLE);
            searchImage.setVisibility(View.GONE);
            MainActivity.navigation.getMenu().clear();
            navigation.setItemBackground(getResources().getDrawable(R.drawable.asm_bottom_navigation_view_bg));
            MainActivity.navigation.inflateMenu(R.menu.asm_btmnavigation);
            MainActivity.navigation.getMenu().findItem(R.id.navigation_createWR).setCheckable(false);

        } catch (Exception e) {
            e.printStackTrace();
        }

        mWebView.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        searchicon.setVisibility(View.VISIBLE);
        searchImage.setVisibility(View.GONE);
        tv_header_title.setVisibility(View.VISIBLE);
        tv_header_title.setTypeface(CustomFonts.getRobotoRegularTF(activity.getApplicationContext()));
        tv_header_title.setText("DASHBOARD");
        return view;
    }

    public String getTodayDate() {
        String lastmonth = "";
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        lastmonth = s.format(new Date(cal.getTimeInMillis()));
        return lastmonth;
    }

    private void setmWebView(View view) {
        mWebView = view.findViewById(R.id.webview);
        mWebView.setListener(getActivity(), this);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.setFocusable(true);
        mWebView.setFocusableInTouchMode(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setDatabaseEnabled(true);
        mWebView.getSettings().setAppCacheEnabled(true);
        mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        mWebView.setInitialScale(1);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);

        mWebView.loadUrl(POWER_BI_URL + "?Date=" + getTodayDate() + "&AMId=" + CommonMethods.getstringvaluefromkey(getActivity(), "user_id"));
    }

    private void fetchData(LandingRequest mLandingRequest) {

        SpinnerManager.showSpinner(getActivity());
        RDMService.getDealerData(mLandingRequest, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(getActivity());
                Response<DealerResponseData> mRes = (Response<DealerResponseData>) obj;
                DealerResponseData mData = mRes.body();



                try {
                    if (mData != null) {

                        List<Dealer> dealers = mData.getDealers();
                        removeNullElement(dealers);
                        if (dealers.size() > 0) {
                            for (int i = 0; i < dealers.size(); i++) {
                                try {
                                    if (dealers.get(i) != null) {
                                        Dealer dealerdaat = dealers.get(i);
                                        if (dealerdaat.getStatusDay0() != null) {
                                            if (dealerdaat.getStatusDay0().equalsIgnoreCase("Green") || (dealerdaat.getStatusDay0().isEmpty() && dealerdaat.getStatusDay1().isEmpty() && dealerdaat.getStatusDay2().isEmpty() && dealerdaat.getStatusDay3().isEmpty())) {
                                                dealers.remove(i);
                                            }
                                        }
                                    }
                                } catch (Exception exception) {
                                    exception.printStackTrace();
                                }

                            }
                            ASMLandingAdapter asmLandingAdapter = new ASMLandingAdapter(dealers, activity);
                            rvDealerStatus.setAdapter(asmLandingAdapter);
                            tvPendingTaskCount.setText("" + mData.getPendingTask());
                            tvMeetingsTodayCount.setText("" + mData.getMeetingsToday());
                            tvWRPendingCount.setText(""+mData.getWRPending());
                        }

                    } else {
                        CommonMethods.alertMessage(activity, getResources().getString(R.string.no_data_found));
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }



            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(getActivity());
            }
        });
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
       // Log.e("url", "" + url);
    }

    @Override
    public void onPageFinished(String url) {

    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {

    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {

    }

    @Override
    public void onExternalPageRequest(String url) {

    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @SuppressLint("NewApi")
    @Override
    public void onResume() {
        super.onResume();

        toolbar.setBackgroundColor(getResources().getColor(R.color.all_dealers_fragment_bg));
        CommonMethods.setvalueAgainstKey(activity, "status", "dashboard");
        tv_header_title.setText("DASHBOARD");
        tv_header_title.setTextColor(getResources().getColor(R.color.black));
        searchImage.setVisibility(View.GONE);

        //  navigation.setVisibility(View.VISIBLE);
        if (asm_mail != null) {
            asm_mail.setVisible(false);
        }

        Application.getInstance().logASMEvent(GAConstants.AM_DASHBOARD, CommonMethods.getstringvaluefromkey(activity, "user_id") + System.currentTimeMillis() + "" + CommonMethods.getstringvaluefromkey(activity, "device_id"));
        mWebView.onResume();


    }

    @SuppressLint("NewApi")
    @Override
    public void onPause() {
        mWebView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mWebView.onDestroy();

        super.onDestroy();
    }

   /* @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
*/

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        asm_mail = menu.findItem(R.id.asm_mail);
        asm_mail.setVisible(false);
        searchImage.setVisibility(View.GONE);
        MenuItem notification = menu.findItem(R.id.notification_bell);
        notification.setVisible(true);
        hideOption(R.id.notification_bell);

        super.onPrepareOptionsMenu(menu);

    }

    private void dealerNameReq() {

        //SpinnerManager.showSpinner(getActivity());

        DasboardStatusServices.getLeadStatus(getActivity(), new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                // SpinnerManager.hideSpinner(getActivity());
                Response<List<dasboardRes>> mRes = (Response<List<dasboardRes>>) obj;
                List<dasboardRes> mData = mRes.body();

                try {
                    String str = new Gson().toJson(mData, new TypeToken<List<dasboardRes>>() {
                    }.getType());
                    Log.i(TAG, "OnSuccess: " + str);
                } catch (Exception e) {
                    Log.e(TAG, "Exception : " + e.getMessage());
                }

                liveList.clear();
                liveList = mData;

                Helper.dealerName.clear();
                Helper.dealerCode.clear();

                if (!liveList.isEmpty()) {

                    for (int i = 0; i <= liveList.size() - 1; i++) {
                        Helper.dealerName.add(i, liveList.get(i).getDealerName());
                        Helper.dealerCode.add(i, liveList.get(i).getDealerCode());
                    }

                } else {
                    Log.i(TAG, "OnSuccess: No data found");
                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                //SpinnerManager.hideSpinner(getActivity());
                try {
                    Helper.dealerName.clear();
                    Helper.dealerCode.clear();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void removeNullElement(List<Dealer> dealers) {
        while (dealers.remove(null)) {
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        mWebView.onActivityResult(requestCode, resultCode, intent);

    }
}