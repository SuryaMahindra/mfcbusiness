package rdm;


import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import imageeditor.base.BaseActivity;
import mteams.MTeamsLogin;
import rdm.model.EventData;
import rdm.model.EventResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static rdm.retrofit_services.RetroBaseService.URL_END_SCHEDULE_EVENT;
import static rdm.retrofit_services.RetroBaseService.retrofitInterface;

public class RDRAddMeetingActivity extends BaseActivity implements Callback<Object> {

    TextView tvMonthAndYearLbl;
    Button btnScheduleAnotherRDR,btnAddMeetingSubmit;
    RecyclerView rv_rgm_add_meeting;
    String startDate="",endDate="";
    private String startDateVal = "2020-07-13";
    private String format =  "yyyy-MM-dd";
    private String formatExpected =  "dd MMM";

    final Calendar cal = Calendar.getInstance();
    private SimpleDateFormat sdf = new SimpleDateFormat(format);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_r_d_r_add_meeting);
        Log.i(TAG, "onCreate: ");
        //initViews();
        tvMonthAndYearLbl=findViewById(R.id.tv_add_meeting_monthAndYearLbl);
        btnScheduleAnotherRDR=findViewById(R.id.btnScheduleRDR);
        btnScheduleAnotherRDR.setOnClickListener(this);
        btnAddMeetingSubmit=findViewById(R.id.btnAddMeetingSubmit);
        btnAddMeetingSubmit.setOnClickListener(this);
        rv_rgm_add_meeting=findViewById(R.id.rgm_add_meeting);
        tvMonthAndYearLbl.setText(CommonMethods.getMonthNameAndYear(this,CommonMethods.getTodayDate(this)));
        startDateVal = sdf.format(cal.getTime());
        startDate= CommonMethods.getTodayAndTime(this);
        endDate="2024-08-07 01:10:39";
        retrofitInterface.getFromWeb(URL_END_SCHEDULE_EVENT,startDate,endDate).enqueue(this);
        SpinnerManager.showSpinner(this);
        LinearLayoutManager layoutManager=new LinearLayoutManager(getApplicationContext());
        rv_rgm_add_meeting.setLayoutManager(layoutManager);
    }

    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btnScheduleRDR:
                navigateToMTeamsLogin();
                break;
            case R.id.btnAddMeetingSubmit:
                Toast.makeText(this,"Clicked on Submit",Toast.LENGTH_LONG).show();
                break;
        }
    }

    private void navigateToMTeamsLogin()
    {
        Intent intent=new Intent(RDRAddMeetingActivity.this, MTeamsLogin.class);
        intent.putExtra("MeetingType","ScheduleAnotherRDR");
        startActivity(intent);
    }

    @Override
    public void onResponse(Call<Object> call, Response<Object> response)
    {
        try
        {
            SpinnerManager.hideSpinner(this);
            String strRes = new Gson().toJson(response.body());
            Log.i(TAG, "onResponse: " + strRes);
            EventResponse eventResponse = new Gson().fromJson(strRes, EventResponse.class);
            if(eventResponse.getIsSuccess())
            {
                Log.i(TAG, "onResponse: "+eventResponse.getIsSuccess());
                if(eventResponse.getData().size()>0)
                {
                    btnScheduleAnotherRDR.setText(getResources().getString(R.string.schedule_another_rdr));
                }else
                    {
                        btnScheduleAnotherRDR.setText(getResources().getString(R.string.schedule_rdr));
                    }
                setToAddMeetingAdapter(eventResponse.getData());
            }
        }
        catch(Exception ex){ex.printStackTrace();}
    }

    @Override
    public void onFailure(Call<Object> call, Throwable t) {
        t.printStackTrace();
        btnScheduleAnotherRDR.setText(getResources().getString(R.string.schedule_rdr));
    }

    private void setToAddMeetingAdapter(List<EventData> data)
    {
        RDRAddMeetingAdapter addMeetingAdapter=new RDRAddMeetingAdapter(RDRAddMeetingActivity.this,data);
        rv_rgm_add_meeting.setAdapter(addMeetingAdapter);
    }

}