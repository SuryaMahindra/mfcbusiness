package mteams;

import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

public class CreateEventService extends MTeamsBaseService {


  public static void postEvent(CreateEventRequest mRequest, HttpCallResponse mHttpCallResponse){
      MTeamsInterface teamsInterface = retrofit.create(MTeamsInterface.class);

      Call<MTeamsResponse> mCall = teamsInterface.createOnlineEvent(mRequest);
      mCall.enqueue(new Callback<MTeamsResponse>() {
          @Override
          public void onFailure(Call<MTeamsResponse> call, Throwable t) {
              mHttpCallResponse.OnFailure(t);
          }

          @Override
          public void onResponse(Call<MTeamsResponse> call, Response<MTeamsResponse> response) {
              mHttpCallResponse.OnSuccess(response);

          }
      });
  }

    public static void updateEvent(CreateEventRequest mRequest, String id,HttpCallResponse mHttpCallResponse){
        MTeamsInterface teamsInterface = retrofit.create(MTeamsInterface.class);

        Call<MTeamsResponse> mCall = teamsInterface.updateMTeamsEvent(mRequest,id);
        mCall.enqueue(new Callback<MTeamsResponse>() {
            @Override
            public void onFailure(Call<MTeamsResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }

            @Override
            public void onResponse(Call<MTeamsResponse> call, Response<MTeamsResponse> response) {
                mHttpCallResponse.OnSuccess(response);

            }
        });
    }

    public interface MTeamsInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("events")
        Call<MTeamsResponse> createOnlineEvent(@Body CreateEventRequest mRequest);


        @Headers("Content-Type: application/json; charset=utf-8")
        @PATCH("events/{id}")
        Call<MTeamsResponse> updateMTeamsEvent(@Body CreateEventRequest mRequest, @Path("id") String path);
    }


}
