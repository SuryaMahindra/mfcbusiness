package mteams;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MTeamsResponse {


    @SerializedName("onlineMeeting")
    @Expose
    private OnlineMeeting onlineMeeting;


    @SerializedName("id")
    @Expose
    private String id;

    public OnlineMeeting getOnlineMeeting() {
        return onlineMeeting;
    }

    public void setOnlineMeeting(OnlineMeeting onlineMeeting) {
        this.onlineMeeting = onlineMeeting;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



}
