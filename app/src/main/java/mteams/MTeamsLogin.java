package mteams;

import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.mfcwl.mfc_dealer.Activity.RDR.DiscussionPoint1;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.AppConstants;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.CustomFonts;
import com.mfcwl.mfc_dealer.Utility.EventDialog;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.microsoft.identity.client.AuthenticationCallback;
import com.microsoft.identity.client.IAuthenticationResult;
import com.microsoft.identity.client.exception.MsalClientException;
import com.microsoft.identity.client.exception.MsalException;
import com.microsoft.identity.client.exception.MsalServiceException;
import com.microsoft.identity.client.exception.MsalUiRequiredException;

import java.nio.file.ClosedFileSystemException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import imageeditor.base.BaseActivity;
import rdm.model.CreateEventResponse;
import rdm.model.DealerParticipantData;
import rdm.model.DealerParticipantsRequest;
import rdm.model.Participant;
import rdm.model.RDMCreateEventRequest;
import remotedealer.adapter.ParticipantAdapter;
import remotedealer.rdrform.ui.EventListFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sidekicklpr.PreferenceManager;

import static rdm.retrofit_services.RetroBaseService.URL_END_CREATE_EVENT;
import static rdm.retrofit_services.RetroBaseService.URL_END_DEALER_PARTICIPANTS;
import static rdm.retrofit_services.RetroBaseService.retrofitInterface;


public class MTeamsLogin extends BaseActivity implements Callback<Object>, ParticipantAdapter.ParticipantCallBack {
    private static final String TAG = "MTeamsLogin";
    private TextView tvEventType;
    private TextView tvEventDuration;
    private TextView tvStartTimeVal, tvToTimeVal;
    private EditText etParticipants;
    private ImageView iv_addParticipants;
    private RecyclerView rvParticipants;
    private Button btnQuickAdd, btnAddLead;
    LinearLayout llStartTime, llEndTime;
    EditText etTitle, etLocation;
    int selectedHour, selectedMinutes;
    private boolean isAllDayYes = false;
    String strTitle = "", strDescription = "", strLocation = "", strEventType = "", strParticipants = "", strStartDate = "", strEndDateText = "", strStartsText = "", strEndsText = "", strStartsData = "", strEndsData = "";
    ImageView iv_asm_close;
    private ArrayList<Participant> participantArrayList = new ArrayList<>();
    private ArrayList<String> selectedParticipantList = new ArrayList<>();
    String dealerCode = "", dealerName = "", meetingtype = "";
    String mTeamMeetingID = "";
    final Calendar cal = Calendar.getInstance();
    private String formater = "yyyy-MM-dd";
    private String serverDateFromat = "yyyy-MM-dd HH:mm:ss";
    private SimpleDateFormat sdf = new SimpleDateFormat(formater);
    private SimpleDateFormat serverFormater = new SimpleDateFormat(serverDateFromat);
    //MTeams stuff

    private AuthenticationHelper mAuthHelper = null;
    private ArrayList<String> eventTypeList = new ArrayList<>();
    public static PreferenceManager preferenceManager;
    private LinearLayout llEventDurationTime;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);
        Log.i(TAG, "onCreate: ");

        initMteams();

        Log.i(TAG, "onCreate: Dealer Code from Intent" + getIntent().getExtras().getString(AppConstants.DEALER_CODE));

        getIntentValues();
        initViews();
    }

    private void getIntentValues() {
        try {
            if (getIntent() != null && getIntent().getExtras() != null) {
                dealerCode = getIntent().getExtras().getString(AppConstants.DEALER_CODE);
                Log.i(TAG, "onCreate: dealerCode" + dealerCode);
                dealerName = getIntent().getExtras().getString(AppConstants.DEALER_NAME);
                meetingtype = getIntent().getExtras().getString(AppConstants.MEETING_TYPE);
            } else {
                dealerCode = "";
                dealerName = "";
                meetingtype = "";
            }
        } catch (NullPointerException exception) {
            exception.printStackTrace();
        }

    }

    private void addEventTypes() {
        eventTypeList.add("Remote Dealer Report");
        eventTypeList.add("Dealer Visit");
        eventTypeList.add("Virtual Meeting");
        eventTypeList.add("Training");
        eventTypeList.add("Others");
    }

    private void openEventTypeDialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(MTeamsLogin.this);
        builderSingle.setIcon(R.drawable.ic_launcher);
        builderSingle.setTitle("Event Type");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MTeamsLogin.this, android.R.layout.simple_list_item_1, eventTypeList);

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                tvEventType.setText(strName);
            }
        });
        builderSingle.show();
    }

    private void getParticipantFromServer() {
        SpinnerManager.showSpinner(MTeamsLogin.this);
        retrofitInterface.getFromWeb(getDealerCode(), URL_END_DEALER_PARTICIPANTS).enqueue(this);
    }


    private DealerParticipantsRequest getDealerCode() {

        DealerParticipantsRequest request = new DealerParticipantsRequest();
        //request.setDealerCode(CommonMethods.getstringvaluefromkey(CreateEventActivity.this,"dealer_code"));
        request.setDealerCode(dealerCode);
        Log.i(TAG, "getDealerCode: " + request.getDealerCode());
        return request;
    }

    private void setDefaultTime() {
        cal.add(Calendar.MINUTE, 10);
        strStartsData = serverFormater.format(cal.getTime());
        Log.i(TAG, "setDefaultTime: 1 : " + strStartsData);
        cal.add(Calendar.MINUTE, 60);
        strEndsData = serverFormater.format(cal.getTime());
        Log.i(TAG, "setDefaultTime: 2 : " + strEndsData);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Calendar calendar = Calendar.getInstance();
        calendar.getTime();
        calendar.add(Calendar.MINUTE, 10);
        calendar.add(Calendar.HOUR, 2);
        strStartDate = CommonMethods.getTodayDate(MTeamsLogin.this);
        strEndDateText = strStartDate;
        tvEventDuration.setText(strStartDate);

        strStartsText = sdf.format(cal.getTime());
        strEndsText = sdf.format(calendar.getTime());

        tvStartTimeVal.setText(strStartsText);
        tvToTimeVal.setText(strEndsText);
    }

    private void initViews() {

        tvEventType = findViewById(R.id.tvEventType);
        iv_asm_close = findViewById(R.id.iv_asm_close);
        etParticipants = findViewById(R.id.etParticipants);
        etTitle = findViewById(R.id.etTitle);
        tvStartTimeVal = findViewById(R.id.tvStartTimeVal);
        tvToTimeVal = findViewById(R.id.tvToTimeVal);
        llEventDurationTime = findViewById(R.id.llEventDurationTime);
        llStartTime = findViewById(R.id.llStartTime);
        llEndTime = findViewById(R.id.llEndTime);
        etLocation = findViewById(R.id.etLocation);
        rvParticipants = findViewById(R.id.rvParticipants);
        tvEventDuration = findViewById(R.id.tvEventDuration);
        btnQuickAdd = findViewById(R.id.btnQuickAdd);
        btnAddLead = findViewById(R.id.btnAddLead);
        iv_addParticipants = findViewById(R.id.iv_addParticipants);
        etTitle.setTypeface(CustomFonts.getRobotoRegularTF(this));
        etLocation.setTypeface(CustomFonts.getRobotoRegularTF(this));
        addEventTypes();
        getParticipantFromServer();
        setDefaultTime();
        if (meetingtype.equalsIgnoreCase("setRDR") || meetingtype.equalsIgnoreCase("startRDR") || meetingtype.equalsIgnoreCase("ScheduleAnotherRDR")) {

            tvEventType.setText("Remote Dealer Report");
            etTitle.setText("" + dealerName + " | " + " RDR " + " | " + sdf.format(cal.getTime()));
            etTitle.setFocusable(false);
            etTitle.setFocusableInTouchMode(false);
            etLocation.setFocusableInTouchMode(false);
            etLocation.setFocusable(false);
        }
        tvEventType.setOnClickListener(this);
        iv_addParticipants.setOnClickListener(this);
        llStartTime.setOnClickListener(this);
        llEndTime.setOnClickListener(this);
        iv_asm_close.setOnClickListener(this);
        btnQuickAdd.setOnClickListener(this);
        btnAddLead.setOnClickListener(this);
        if (meetingtype.equalsIgnoreCase("startRDR")) {
            tvEventDuration.setClickable(false);
            tvEventDuration.setFocusable(false);
            llStartTime.setClickable(false);
            llEndTime.setClickable(false);
            llEventDurationTime.setFocusable(false);
        } else {
            tvEventDuration.setOnClickListener(this);
            llStartTime.setOnClickListener(this);
            llEndTime.setOnClickListener(this);
            llEventDurationTime.setFocusable(true);
        }

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.tvEventType) {
            openEventTypeDialog();
        } else if (v.getId() == R.id.iv_asm_close) {
            finish();
        } else if (v.getId() == R.id.btnAddLead) {
            try {
                if (validate()) {
                    Log.i(TAG, "onClick: validate TRUE");
                    if (!strParticipants.isEmpty()) {

                        String mteamstoken = CommonMethods.getstringvaluefromkey(MTeamsLogin.this, "mteamstoken");
                        if (strEventType.equalsIgnoreCase("Remote Dealer Report") || strEventType.equalsIgnoreCase("Virtual Meeting")) {
                            if (mteamstoken != null && !mteamstoken.equalsIgnoreCase("")) {
                                createMteamsEvent(strTitle, strParticipants);
                            } else {
                                Toast.makeText(MTeamsLogin.this, "Please login with MTeams", Toast.LENGTH_LONG).show();
                                doSilentSignIn();
                            }
                        } else {
                            if (strDescription.isEmpty() || !strDescription.equalsIgnoreCase("Created with MTeams")) {
                                strDescription = "Offline Meeting";
                            }
                            SpinnerManager.showSpinner(MTeamsLogin.this);
                            callRetroService();
                        }
                    }

                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }

        } else if (v.getId() == R.id.iv_addParticipants) {
            if (!etParticipants.getText().toString().isEmpty()) {
                String participantVal = etParticipants.getText().toString();
                if (isValidEmail(participantVal)) {
                    selectedParticipantList.add(participantVal);
                    attachToAdapter();
                    updateStrParticipants();
                    etParticipants.setText("");
                } else {
                    Toast.makeText(MTeamsLogin.this, "Please enter valid EmailId", Toast.LENGTH_LONG).show();
                }

            } else {
                Toast.makeText(MTeamsLogin.this, "Please enter EmailId", Toast.LENGTH_LONG).show();
            }

        } else if (v.getId() == R.id.tvEventDuration) {
            try {
                EventDialog postCalendarDialog = new EventDialog(
                        MTeamsLogin.this, this,
                        "",
                        (startDateString, endDateString, dateType) -> {
                            if (!startDateString.equals(endDateString)) {
                                tvEventDuration.setText(startDateString + " To " + endDateString);
                                strStartDate = startDateString;
                                Log.i("StartDate", "onClick: " + strStartDate);
                                strEndDateText = endDateString;
                            } else {
                                tvEventDuration.setText(startDateString);
                                strStartDate = startDateString;
                                strEndDateText = strStartDate;
                                Log.i("StartDate", "onClick: " + strEndDateText);
                            }
                        });
                postCalendarDialog.setCancelable(false);
                postCalendarDialog.show();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        } else if (v.getId() == R.id.btnQuickAdd) {
            if (quickAddValidate()) {
                if (strEventType.equalsIgnoreCase("Dealer Visit") || strEventType.equalsIgnoreCase("Training") || tvEventType.getText().toString().equalsIgnoreCase("Others")) {
                    String mteamstoken = CommonMethods.getstringvaluefromkey(MTeamsLogin.this, "mteamstoken");
                    if (mteamstoken != null && !mteamstoken.equalsIgnoreCase("")) {
                        createMteamsEvent(strTitle, strParticipants);
                    } else {
                        Toast.makeText(MTeamsLogin.this, "Please login with MTeams", Toast.LENGTH_LONG).show();
                        doSilentSignIn();
                    }
                }
            }
        } else if (v.getId() == R.id.llStartTime) {
            if (!tvEventDuration.getText().toString().isEmpty())
                setTimeValue(tvStartTimeVal);
            else
                Toast.makeText(MTeamsLogin.this, "Please Event Date", Toast.LENGTH_SHORT).show();
        } else if (v.getId() == R.id.llEndTime) {
            if (!tvEventDuration.getText().toString().isEmpty()) {
                if (!tvStartTimeVal.getText().toString().isEmpty())
                    setTimeValue(tvToTimeVal);
                else
                    Toast.makeText(MTeamsLogin.this, "Please Event Start Time", Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(MTeamsLogin.this, "Please Event Date", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean quickAddValidate() {
        strEventType = tvEventType.getText().toString();
        strTitle = etTitle.getText().toString();
        strStartsData = strStartDate + " " + tvStartTimeVal.getText().toString() + ":00";
        strEndsData = strEndDateText + " " + tvToTimeVal.getText().toString() + ":00";
        if (strEventType.equals("Event Type") || (strTitle.equals("")) || (selectedParticipantList.isEmpty()) ||
                (tvEventDuration.getText().toString().equals("")) || (strStartsText.equals("")) || (tvStartTimeVal.getText().toString().equals("")) || (strEndsText.equals("")) || (tvToTimeVal.getText().toString().equals("")) || (strStartsData.equals("")) || (strEndsData.equals(""))) {
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_LONG).show();
            return false;
        } else if ((strEventType.equals("Event Type")) || (!strEventType.equals("")) && (!strTitle.equals("")) && (selectedParticipantList.isEmpty()) &&
                (!tvEventDuration.getText().toString().equals("")) && (!strStartsText.equals("")) && (!strEndsText.equals("")) && (!tvStartTimeVal.getText().toString().equals("")) && (!tvToTimeVal.getText().toString().equals("")) && (!strStartsData.equals("")) && (!strEndsData.equals(""))) {
            Toast.makeText(this, "Please select Participants", Toast.LENGTH_LONG).show();
            return false;
        } else {
            return true;
        }
    }

    private boolean isValidEmail(String participantVal) {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (participantVal.matches(emailPattern)) {
            return true;
        } else {
            return false;
        }
    }

    private void callRetroService() {
        retrofitInterface.getFromWeb(getCreateEventRequest(), URL_END_CREATE_EVENT).enqueue(this);
    }

    private RDMCreateEventRequest getCreateEventRequest() {

        RDMCreateEventRequest rdmCreateEventRequest = new RDMCreateEventRequest();
        rdmCreateEventRequest.setEventTitle(strTitle);
        rdmCreateEventRequest.setEventDescription(strDescription);
        rdmCreateEventRequest.setEventLocation(strLocation);

        if (strDescription.equalsIgnoreCase("Created with MTeams"))
            rdmCreateEventRequest.setMteamsEventId(mTeamMeetingID);
        else
            rdmCreateEventRequest.setMteamsEventId("Offline Meeting");

        if (isAllDayYes) {
            rdmCreateEventRequest.setIsAllDayEvent("true");
        } else {
            rdmCreateEventRequest.setIsAllDayEvent("false");
        }
        Log.i(TAG, "getCreateEventRequest: AllDay" + rdmCreateEventRequest.getIsAllDayEvent());
        rdmCreateEventRequest.setEventStartDateTime(strStartsData);
        rdmCreateEventRequest.setEventEndDateTime(strEndsData);
        rdmCreateEventRequest.setEventType(strEventType);
        rdmCreateEventRequest.setDealerCode(dealerCode);
        rdmCreateEventRequest.setParticipants(getParticipants());
        return rdmCreateEventRequest;
    }

    private List<Participant> getParticipants() {
        Log.i(TAG, "getParticipants: " + strParticipants);
        String[] partNameArr = strParticipants.split("\\|");
        Log.i(TAG, "getParticipants: " + partNameArr.length);
        ArrayList<Participant> arrayList = new ArrayList<>();
        for (int i = 0; i < selectedParticipantList.size(); i++) {
            try {
                Participant participant = new Participant();
                participant.setEmail(selectedParticipantList.get(i));
                Log.i(TAG, "getParticipants: " + selectedParticipantList.get(i));
                arrayList.add(participant);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }

        return arrayList;

        /*int partNameArrIndex = 0;
        for (int i = 0; i < alParticipant.size(); i++) {
            if (alParticipant.get(i).getName().equalsIgnoreCase(partNameArr[partNameArrIndex])) {
                Log.i(TAG, "getParticipants: if" + alParticipant.get(i).getName() + "===" + partNameArr[partNameArrIndex]);
               Participant participant1 = new Participant();
                participant1.setType(participantArrayList.get(i).getType());
                participant1.setName(participantArrayList.get(i).getName());
                participant1.setEmail(participantArrayList.get(i).getEmail());

                  addParticipantList(participant1);

                if (partNameArr.length!=0 && partNameArrIndex == partNameArr.length - 1) {
                    i = participantArrayList.size() - 1;
                } else {
                    partNameArrIndex++;
                }

            }
        }*/
    }


    private boolean validate() {
        strEventType = tvEventType.getText().toString();
        strTitle = etTitle.getText().toString();
        strLocation = etLocation.getText().toString();
        strStartsData = strStartDate + " " + tvStartTimeVal.getText().toString() + ":00";
        strEndsData = strEndDateText + " " + tvToTimeVal.getText().toString() + ":00";
        if (strEventType.equals("Event Type") || (strTitle.equals("")) || (selectedParticipantList.isEmpty()) ||
                (tvEventDuration.getText().toString().equals("")) || (strStartsText.equals("")) || (tvStartTimeVal.getText().toString().equals("")) || (strEndsText.equals("")) || (tvToTimeVal.getText().toString().equals("")) || (strStartsData.equals("")) || (strEndsData.equals(""))) {
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_LONG).show();
            return false;
        } else if ((strEventType.equals("Event Type")) || (!strEventType.equals("")) && (!strTitle.equals("")) && (selectedParticipantList.isEmpty()) &&
                (!tvEventDuration.getText().toString().equals("")) && (!strStartsText.equals("")) && (!strEndsText.equals("")) && (!tvStartTimeVal.getText().toString().equals("")) && (!tvToTimeVal.getText().toString().equals("")) && (!strStartsData.equals("")) && (!strEndsData.equals(""))) {
            Toast.makeText(this, "Please select Participants", Toast.LENGTH_LONG).show();
            return false;
        } else if ((strEventType.equalsIgnoreCase("Dealer Visit") || strEventType.equalsIgnoreCase("Training") || tvEventType.getText().toString().equalsIgnoreCase("Others")) && (strLocation.equals(""))) {
            Toast.makeText(this, "Please provide Location, For Non MTeams events Location is mandatory", Toast.LENGTH_LONG).show();
            return false;
        } else {
            return true;

        }
    }

    private void addParticipantsToString(List<Participant> participant) {
        for (int i = 0; i < participant.size(); i++) {
            if (participant.get(i).getEmail() != null && !participant.get(i).getEmail().isEmpty()) {
                selectedParticipantList.add(participant.get(i).getEmail());
                strParticipants = strParticipants + " | " + selectedParticipantList.get(i);
            } else
                participant.remove(i);
        }
        attachToAdapter();
    }


    private void setAlertDialog(final String strTitle,
                                final String[] arrVal, TextView textview) {

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(strTitle);
        alert.setItems(arrVal, (dialog, which) -> {
            textview.setText(arrVal[which]);
        });
        alert.create();
        alert.show();
    }


    private void setTimeValue(TextView textView) {

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        String getCurrentTime = sdf.format(cal.getTime());

        TimePickerDialog timePickerDialog = new TimePickerDialog(this, R.style.EventDatePicker,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        Calendar dateTime = Calendar.getInstance();
                        dateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        dateTime.set(Calendar.MINUTE, minute);
                        String currentTime = "";
                        int hourOfDayVal = hourOfDay;
                        String hourOfDayValLen = String.valueOf(hourOfDayVal);
                        int minuteVal = minute;

                        String minuteValLen = String.valueOf(minuteVal);

                        currentTime = hourOfDayValLen + ":" + minuteValLen;

                        if (hourOfDayValLen.length() == 1) {
                            hourOfDayValLen = "0" + hourOfDayValLen;
                        }
                        if (minuteValLen.length() == 1) {
                            minuteValLen = "0" + minuteValLen;

                        }

                        boolean res = isTodayDate(strStartDate) && isTodayDate(strEndDateText);

                        if (res && (!CommonMethods.selectedTimeIsValid(getCurrentTime, currentTime))) {
                            Toast.makeText(MTeamsLogin.this, "Please select valid time", Toast.LENGTH_LONG).show();
                            textView.setText("");
                        } else {

                            if (textView.getId() == R.id.tvStartTimeVal) {
                                Log.i("StartDate", "onClick: " + strEndsText);

                                if (!hourOfDayValLen.equals("")) {
                                    currentTime = hourOfDayValLen + ":" + minuteValLen;
                                    if (isTodayDate(strStartDate) && !CommonMethods.selectedTimeIsValid(getCurrentTime, currentTime)) {
                                        Toast.makeText(MTeamsLogin.this, "Please select valid time", Toast.LENGTH_LONG).show();
                                        textView.setText("");
                                    } else {
                                        textView.setText(hourOfDayValLen + ":" + minuteValLen);
                                    }
                                } else {
                                    textView.setText("");
                                }


                            } else if (textView.getId() == R.id.tvToTimeVal) {


                                if (isValidEndTime(hourOfDayValLen + ":" + minuteValLen)) {
                                    Log.i(TAG, "onTimeSet: " + isValidEndTime(hourOfDayValLen + ":" + minuteValLen));
                                    if (!hourOfDayValLen.equals("")) {

                                        textView.setText(hourOfDayValLen + ":" + minuteValLen);

                                    } else {
                                        textView.setText("");
                                    }
                                } else {
                                    hourOfDayValLen = "";
                                    minuteValLen = "";
                                    Toast.makeText(MTeamsLogin.this, "Please select valid time", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                }, selectedHour, selectedMinutes, false);
        timePickerDialog.show();
    }


    private boolean isValidEndTime(String endTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
        Date stTime = Calendar.getInstance().getTime(), edTime = Calendar.getInstance().getTime();
        try {
            stTime = sdf.parse(tvStartTimeVal.getText().toString());
            edTime = sdf.parse(endTime);
            Log.i(TAG, "isValidEndTime: " + stTime + "----->" + edTime);
        } catch (ParseException exception) {
            exception.printStackTrace();
        }
        if (edTime.before(stTime)) {
            Log.i(TAG, "isValidEndTime: " + edTime.before(stTime));
            return false;
        } else {
            Log.i(TAG, "isValidEndTime: " + edTime.before(stTime));
            return true;
        }
    }


    private boolean isTodayDate(String dateData) {
        Log.i("StartDate", "onClick: " + dateData);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return sdf.parse(dateData).equals(sdf.parse(CommonMethods.getTodayDate(MTeamsLogin.this)));
        } catch (ParseException e) {

            e.printStackTrace();
            return false;
        }
    }


    private void generateParticipantDialog(List<Participant> participantArray) {
        Log.i(TAG, "generateParticipantDialog: list Size " + participantArray.size());
        String[] strArray = new String[participantArray.size()];
        for (int i = 0; i < strArray.length; i++) {
            try {
                if (participantArray.get(i).getEmail() != null && !participantArray.get(i).getEmail().isEmpty()) {
                    strArray[i] = participantArray.get(i).getEmail();
                    Log.i(TAG, "generateParticipantDialog: " + strArray[i]);
                } else {
                    Log.i(TAG, "data null");
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }

        }
        Log.i(TAG, "generateParticipantDialog: Array length --> " + strArray.length);
    }


    private void initMteams() {
        mAuthHelper = AuthenticationHelper.getInstance(getApplicationContext());
        doSilentSignIn();
        /* preferenceManager = new PreferenceManager(this);*/
    }

    private void doInteractiveSignIn() {
        mAuthHelper.acquireTokenInteractively(this, getAuthCallback());
    }

    private void doSilentSignIn() {
        mAuthHelper.acquireTokenSilently(getAuthCallback());
    }

    public AuthenticationCallback getAuthCallback() {
        return new AuthenticationCallback() {

            @Override
            public void onSuccess(IAuthenticationResult authenticationResult) {
                // Log the token for debug purposes
                String accessToken = authenticationResult.getAccessToken();
                Log.d("AUTH", String.format("Access token: %s", accessToken));

                EventListFragment.preferenceEManager.writeString("mteamstoken", accessToken);
                CommonMethods.setvalueAgainstKey(MTeamsLogin.this, "mteamstoken", accessToken);
            }

            @Override
            public void onError(MsalException exception) {
                // Check the type of exception and handle appropriately
                if (exception instanceof MsalUiRequiredException) {
                    Log.d("AUTH", "Interactive login required");
                    doInteractiveSignIn();

                } else if (exception instanceof MsalClientException) {
                    if (exception.getErrorCode() == "no_current_account") {
                        Log.d("AUTH", "No current account, interactive login required");
                        doInteractiveSignIn();
                    } else {
                        // Exception inside MSAL, more info inside MsalError.java
                        Log.e("AUTH", "Client error authenticating", exception);
                    }
                } else if (exception instanceof MsalServiceException) {
                    // Exception when communicating with the auth server, likely config issue
                    Log.e("AUTH", "Service error authenticating", exception);

                }

            }

            @Override
            public void onCancel() {
                // User canceled the authentication
                Log.d("AUTH", "Authentication canceled");

            }
        };
    }


    private void createMteamsEvent(String title, String participants) {
        CreateEventRequest request = new CreateEventRequest();
        request.setSubject(title);
        request.setIsOnlineMeeting(true);
        request.setOnlineMeetingProvider("teamsForBusiness");

        Body b = new Body();
        b.setContent(title);
        b.setContentType("HTML");

        request.setBody(b);

        Start st = new Start();
        st.setDateTime(strStartsData);
        st.setTimeZone(AppConstants.TIME_ZONE);

        request.setStart(st);
        End et = new End();
        et.setDateTime(strEndsData);
        et.setTimeZone(AppConstants.TIME_ZONE);
        request.setEnd(et);

        Location l = new Location();
        l.setDisplayName("Virtual location ");

        request.setLocation(l);

        List<Attendee> at = new ArrayList<>();
        if (participants.trim().contains("|")) {
            String[] partNameArr = participants.trim().split("\\|");

            for (int i = 0; i < partNameArr.length; i++) {
                Attendee at1 = new Attendee();
                EmailAddress em = new EmailAddress();
                em.setAddress(partNameArr[i]);
                at1.setEmailAddress(em);
                at1.setType("required");
                at.add(at1);
            }
        } else {
            Attendee at1 = new Attendee();
            EmailAddress em = new EmailAddress();
            em.setAddress(participants);
            at1.setEmailAddress(em);
            at1.setType("required");
            at.add(at1);
        }
        Log.i(TAG, "createMteamsEvent: " + at.size());
        for (int i = 0; i < at.size(); i++) {
            Log.i(TAG, "createMteamsEvent: " + at.get(i).toString());
        }
        request.setAttendees(at);
        sendEventRequest(request);
    }


    private void sendEventRequest(CreateEventRequest mRequest) {
        SpinnerManager.showSpinner(MTeamsLogin.this);
        CreateEventService.postEvent(mRequest, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(MTeamsLogin.this);
                try {
                    Response<MTeamsResponse> mData = (Response<MTeamsResponse>) obj;
                    MTeamsResponse teamsResponse = mData.body();
                    OnlineMeeting meeting = teamsResponse.getOnlineMeeting();
                    mTeamMeetingID = teamsResponse.getId();
                    Log.i(TAG, "OnSuccess: " + mTeamMeetingID);
                    if (teamsResponse != null) {
                        strLocation = meeting.getJoinUrl();
                        etLocation.setText(strLocation);
                        strDescription = "Created with MTeams";
                        callRetroService();
                    } else {
                        doInteractiveSignIn();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(MTeamsLogin.this);
                Toast.makeText(MTeamsLogin.this, "Please try again", Toast.LENGTH_LONG).show();
                mThrowable.printStackTrace();

            }
        });


    }

    @Override
    public void onResponse(Call<Object> call, Response<Object> response) {
        SpinnerManager.hideSpinner(MTeamsLogin.this);
        try {

            String url = call.request().url().toString();

            if (url.contains(URL_END_DEALER_PARTICIPANTS)) {

                String strRes = new Gson().toJson(response.body());
                Log.i(TAG, "onResponse: Participants  " + strRes);

                DealerParticipantData dealerParticipantData = new Gson().fromJson(strRes, DealerParticipantData.class);
                if (dealerParticipantData.getParticipants() != null)
                    addParticipantsToString(dealerParticipantData.getParticipants());
                Log.i(TAG, "participantArrayList------> " + participantArrayList.size());

            } else if (url.contains(URL_END_CREATE_EVENT)) {
                String strRes = new Gson().toJson(response.body());
                Log.i(TAG, "onResponse: " + strRes);
                CreateEventResponse createEventResponse = new Gson().fromJson(strRes, CreateEventResponse.class);
                try {
                    if (createEventResponse.getIsSuccess()) {
                        Toast.makeText(MTeamsLogin.this, createEventResponse.getMessage(), Toast.LENGTH_SHORT).show();

                        if (meetingtype.equals("startRDR") || meetingtype.equals("ScheduleAnotherRDR")) {
                            /*Intent RDRIntent = new Intent(MTeamsLogin.this, DiscussionPoint1.class);
                            RDRIntent.putExtra(AppConstants.DEALER_CODE, dealerCode);
                            RDRIntent.putExtra(AppConstants.MEETING_LINK, strLocation);
                            RDRIntent.putExtra(AppConstants.DEALER_NAME, dealerName);
                            startActivity(RDRIntent);
                            */

                            finish();
                            getFragmentManager().findFragmentById(R.id.eventListFragment);


                        } else {

                            finish();
                            getFragmentManager().findFragmentById(R.id.eventListFragment);


                        }
                    }

                } catch (Exception exception) {
                    exception.printStackTrace();
                }

            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void onFailure(Call<Object> call, Throwable t) {
        SpinnerManager.hideSpinner(MTeamsLogin.this);
        t.printStackTrace();
    }

    private void attachToAdapter() {
        ParticipantAdapter participantAdapter = new ParticipantAdapter(selectedParticipantList, this, this);
        rvParticipants.setLayoutManager(
                new LinearLayoutManager(this));
        rvParticipants.setAdapter(participantAdapter);
    }

    @Override
    public void updateParticipantList(ArrayList<String> eventParticipants) {
        selectedParticipantList.clear();
        selectedParticipantList = eventParticipants;
        attachToAdapter();
        updateStrParticipants();

    }

    private void updateStrParticipants() {
        strParticipants = "";
        for (int i = 0; i < selectedParticipantList.size(); i++) {
            if(i==0)
            {
                strParticipants =  selectedParticipantList.get(i);
            }
            else
            {
                strParticipants = strParticipants + "|" + selectedParticipantList.get(i);
            }
        }
    }

    @Override
    protected void onResume() {

        super.onResume();
        SpinnerManager.hideSpinner(this);

    }
}
/*
                DealerParticipantsResponse dealerParticipantsResponse = new Gson().fromJson(strRes, DealerParticipantsResponse.class);
                Log.i(TAG, "onResponse: Participant Size" + dealerParticipantsResponse.getData().size());
                //addParticipantList(dealerParticipantsResponse.getData());*/
