package mteams;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;

import java.io.IOException;

import lprServices.LPRBaseService;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import remotedealer.activity.RDMLandingPage;
import remotedealer.rdrform.ui.EventListFragment;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MTeamsBaseService   {
    public static final String BASE_URL = "https://graph.microsoft.com/v1.0/me/";
    protected static HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

    protected static TokenInterceptor tokenInterceptor = new TokenInterceptor();
    protected static OkHttpClient client = new OkHttpClient
            .Builder()
            .addInterceptor(tokenInterceptor)
            .addInterceptor(interceptor)
            .build();
    protected static Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create();

    protected static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();

    private static class TokenInterceptor implements Interceptor {

        private TokenInterceptor() {

        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request initialRequest = chain.request();

            String sToken = "";
            if (sToken != null) {
                initialRequest = initialRequest.newBuilder()
                        .addHeader("Accept", "application/json; charset=utf-8")
                        .addHeader("Authorization", "Bearer "+ EventListFragment.preferenceEManager.readString("mteamstoken",""))
                        .build();
            }

            Response response = chain.proceed(initialRequest);
            return response;
        }
    }


}