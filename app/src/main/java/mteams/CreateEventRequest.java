package mteams;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CreateEventRequest {

    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("body")
    @Expose
    private Body body;
    @SerializedName("start")
    @Expose
    private Start start;
    @SerializedName("end")
    @Expose
    private End end;
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("attendees")
    @Expose
    private List<Attendee> attendees = null;
    @SerializedName("isOnlineMeeting")
    @Expose
    private Boolean isOnlineMeeting;
    @SerializedName("onlineMeetingProvider")
    @Expose
    private String onlineMeetingProvider;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Start getStart() {
        return start;
    }

    public void setStart(Start start) {
        this.start = start;
    }

    public End getEnd() {
        return end;
    }

    public void setEnd(End end) {
        this.end = end;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<Attendee> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<Attendee> attendees) {
        this.attendees = attendees;
    }

    public Boolean getIsOnlineMeeting() {
        return isOnlineMeeting;
    }

    public void setIsOnlineMeeting(Boolean isOnlineMeeting) {
        this.isOnlineMeeting = isOnlineMeeting;
    }

    public String getOnlineMeetingProvider() {
        return onlineMeetingProvider;
    }

    public void setOnlineMeetingProvider(String onlineMeetingProvider) {
        this.onlineMeetingProvider = onlineMeetingProvider;
    }

}


