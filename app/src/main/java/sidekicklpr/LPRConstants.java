package sidekicklpr;

public class LPRConstants {

    public static final String   LPR_DIR = "lpr_dir";

    public static final String   LPR_FILENAME = "lpr_filename";

    public static final int    REQUEST_CODE_LPR_SINGLE_SCAN = 1001;

    public static final int  REQUEST_CODE_LPR_MULTIPLE_SCAN = 1002;

    public static final int  REQUEST_CODE_LPR_STOCK_LIST = 1003;


    public static String  TABLE_LPR_LIST = "table_lpr_list";

    public static String LPR_ITEM_ID = "lpr_item_id",
            CAR_NUMBER = "car_number",
            EVENT_ID = "EventID",
            DATE_TIME = "DateTime",
            CAR_IMAGE_PATH = "car_image_path",
            PLATE_IMAGE_PATH = "plate_image_path",
            LATITUDE = "Latitude",
            LONGITUDE = "Longitude",
            CAR_IMAGE_URL = "car_image_url",
            SELECTED_DATA = "selected_data",
            OUTPUT_VALUE = "output_value",IS_SYNCED = "is_synced";

    public static String CREATE_TABLE_LPR_LIST = "Create table IF NOT EXISTS " + TABLE_LPR_LIST + "(" +
            LPR_ITEM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            EVENT_ID + " TEXT," +
            DATE_TIME + " TEXT," +
            CAR_IMAGE_PATH + " TEXT," +
            PLATE_IMAGE_PATH + " TEXT," +
            LATITUDE + " TEXT ," +
            LONGITUDE + " TEXT," +
            CAR_IMAGE_URL + " TEXT," +
            SELECTED_DATA + " TEXT," +
            OUTPUT_VALUE + " TEXT," +
            IS_SYNCED + " INTEGER DEFAULT 0" +
            ")";




}
