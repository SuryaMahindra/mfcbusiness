package sidekicklpr;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.videoAppSpecific.SqlAdapterForDML;

import java.util.List;

import aws.AwsImageSender;

public class MediaUploadManager extends AsyncTask<Void, Void, Void> {
    private SqlAdapterForDML mSqlAdapterForDML = SqlAdapterForDML.getInstance();

    private List<StockItem> data;
    private Activity mContext;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        SpinnerManager.showSpinner(mContext);
    }

    public MediaUploadManager( Activity mContext) {
        this.mContext = mContext;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        data = mSqlAdapterForDML.getLPRList();
        for (StockItem entry : data) {
            int stock_Id = entry.getStockId();
            String key = entry.getStockValue();
            String carplate = entry.getCarplateimage();
            String carImage = entry.getCarImagePath();
            int type = entry.getIsSync();
          String dealercode = CommonMethods.getstringvaluefromkey((Activity) mContext,"user_id");
          Log.e("dealer code","code" + dealercode);

            if (type == 0) {{
            String    carPlateUrl = AwsImageSender.getImageUrl(carplate,dealercode,key);
            String    carImageurl = AwsImageSender.getImageUrl(carImage,dealercode,key);
                mSqlAdapterForDML.updateLPRItemStatus(stock_Id,carPlateUrl,carImageurl);
                Log.e("Plate image url"," "+ carPlateUrl);
            }
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        SpinnerManager.hideSpinner(mContext);
        LPRDataUploadManager manager = new LPRDataUploadManager(mContext);
        manager.execute();
    }
}
