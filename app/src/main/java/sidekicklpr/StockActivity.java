package sidekicklpr;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.encrypt.AWSSecretsManager;
import com.mfcwl.mfc_dealer.encrypt.s3Res;
import com.mfcwl.mfc_dealer.retrofitconfig.S3DetailsServices;
import com.mfcwl.mfc_dealer.videoAppSpecific.SqlAdapterForDML;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Response;

public class StockActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private ImageView ivCaptureStock;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Button btnSubmit;
    private RecyclerView recyclerView;
    private StockAdapter adapter;
    private TextView emptyView, txtCount;
    protected List<StockItem> stockList;
    private LinearLayout BackNavigation;
    private LinearLayout addlprstock;
    private PreferenceManager preferenceManager;

    private SqlAdapterForDML mSqlAdapterForDML = SqlAdapterForDML.getInstance();

    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock);
        Log.i(TAG, "onCreate: ");
        initView();

        ivCaptureStock.setOnClickListener(this);
        swipeRefreshLayout.setOnRefreshListener(this);
        btnSubmit.setOnClickListener(this);
        BackNavigation.setOnClickListener(this);
        addlprstock.setOnClickListener(this);
        preferenceManager = new PreferenceManager(this);

        loadLPRData();
        sendS3Details(this);

    }

    private void initView() {
        ivCaptureStock = (ImageView) findViewById(R.id.iv_stockcapture);
        txtCount = (TextView) findViewById(R.id.txt_count);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        emptyView = (TextView) findViewById(R.id.empty_view);
        BackNavigation = findViewById(R.id.backnavigation);
        addlprstock = findViewById(R.id.addlprstock);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        btnSubmit = (Button) findViewById(R.id.btn_submit);


    }

    private void loadLPRData() {
        new DataFetch().execute();
    }

    private class DataFetch extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            stockList = mSqlAdapterForDML.getLPRList();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Collections.reverse(stockList);
            setAdapter(stockList);
        }
    }

    private void setAdapter(final List<StockItem> stockList) {
        try {
            if (stockList.size() > 0) {
                hideEmptyView();
                String count = "- " + String.valueOf(stockList.size());
                txtCount.setText(count);
                adapter = new StockAdapter(StockActivity.this, stockList, txtCount, true);
                LinearLayoutManager layoutManager = new LinearLayoutManager(StockActivity.this);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setItemViewCacheSize(stockList.size());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();


            } else
                showEmptyView();

        } catch (Exception e) {
            e.printStackTrace();
            showEmptyView();
        }
    }


    private void hideEmptyView() {
        recyclerView.setVisibility(View.VISIBLE);
        emptyView.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
    }

    private void showEmptyView() {
        recyclerView.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setRefreshing(false);
        //txtCount.setText(R.string.zero);
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
        loadLPRData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.app_logo:
            case R.id.backnavigation:
                finish();
                break;
            case R.id.addlprstock:
                LPRWarningDialog mDialog = new LPRWarningDialog(StockActivity.this, MainActivity.isLPRstartedfromMenu);
                mDialog.show();
                break;

            case R.id.btn_submit:
                if (CommonMethods.isConnectingToInternet(StockActivity.this)) {
                    new MediaUploadManager(StockActivity.this).execute();

                } else {
                    Toast.makeText(StockActivity.this, "Please check your network connection", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LPRConstants.REQUEST_CODE_LPR_MULTIPLE_SCAN) {
            Log.e("Onactivity called", "onActivity called");
            String value = readLPRFile(this, LPRConstants.REQUEST_CODE_LPR_MULTIPLE_SCAN);
            Log.e("LPR Data", "" + value);
            parseLPRData(value);
        }

    }

    private void sendS3Details(final Context mContext) {


        // SpinnerManager.showSpinner(mContext);
        S3DetailsServices.getDetails(StockActivity.this, "", new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                //SpinnerManager.hideSpinner(StockActivity.this);
                Response<s3Res> mRes = (Response<s3Res>) obj;
                s3Res mData = mRes.body();

                try {
                    AWSSecretsManager.getInstance().parseAWSSecret(mData);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void OnFailure(Throwable t) {
                //   SpinnerManager.hideSpinner(StockActivity.this);
                t.printStackTrace();
            }
        });

    }


    private void parseLPRData(String value) {
        try {
            Type empTypeList = new TypeToken<ArrayList<LPRResponse>>() {
            }.getType();
            List<LPRResponse> mData = new Gson().fromJson(value, empTypeList);
            if (mData != null && mData.size() > 0) {
                Log.i("LPR", "Result: ");
                for (int i = 0; i < mData.size(); i++) {
                    LPRResponse response = mData.get(i);
                    mSqlAdapterForDML.insertLPRItem(response);
                }

            }

            if (mSqlAdapterForDML.getLPRListCount() > 0) {
                loadLPRData();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String readLPRFile(Context context, int type) { // reading file..for single scan..jSON filw is deleted after reading image..
        String value = "";
        final String lprDir, lprFileName;
        lprDir = preferenceManager.readString(LPRConstants.LPR_DIR, "");
        lprFileName = preferenceManager.readString(LPRConstants.LPR_FILENAME + type, "");
        try {

            int fileCount = 0;
            File directory;
            File[] allFiles;

            FilenameFilter fileFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return (name.startsWith(lprFileName) && name.endsWith(".json"));
                }
            };

            directory = new File(lprDir + "/" + lprFileName);
            allFiles = directory.listFiles(fileFilter);
            Log.e("LPR", "file lenght: " + allFiles.length);
            if (allFiles.length == 1) {

                File file = allFiles[0];/*LPR required file..*/
                if (file.exists()) {
                    FileInputStream fis = new FileInputStream(file);
                    DataInputStream in = new DataInputStream(fis);
                    BufferedReader br = new BufferedReader(new InputStreamReader(in));
                    String strLine;
                    while ((strLine = br.readLine()) != null) {
                        value = value + strLine;
                    }
                    in.close();
                    if (type == LPRConstants.REQUEST_CODE_LPR_SINGLE_SCAN && file.delete()) {
                        fileCount++;
                        Log.e("LPR", "file Deleted");
                    } else {
                        Log.e("LPR", "file not Deleted");
                    }
                }
            } else {
                for (File f : allFiles) {
                    if (f.delete()) {
                        fileCount++;
                        Log.e("LPR", "file Deleted");
                    } else {
                        Log.e("LPR", "file not Deleted");
                    }
                }
            }
            Log.e("LPR", "deleted file count: " + fileCount);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return value;
    }


}
