package sidekicklpr;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.videoAppSpecific.SqlAdapterForDML;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author MAUTO0c332 on 11-12-2018.
 */
public class StockAdapter extends RecyclerView.Adapter<StockAdapter.ViewHolder> {
    private List<StockItem> cardList, filterList;
    private boolean isLPRFlow;
    private Activity context;
    private TextView txtCount;
    private onItemClickListener listener;
    private Intent intent;
    private SqlAdapterForDML mSqlAdapterForDML = SqlAdapterForDML.getInstance();


    public StockAdapter(Activity context, List<StockItem> cardList, TextView txtCount, boolean isLPRFlow) {
        this.context = context;
        this.cardList = cardList;
        filterList = new ArrayList<>();
        this.filterList.addAll(cardList);
        this.txtCount = txtCount;
        this.isLPRFlow = isLPRFlow;
    }

    public interface onItemClickListener {
        void onViewImage(LPRResponse item);

        void onChangeImage(LPRResponse item);

        void onChangeRegNo(LPRResponse item);

        void onDeleteStockItem(LPRResponse item);
    }

    public void setOnItemClickListener(onItemClickListener listener) {
        this.listener = listener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.stock_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        String conNo, ymsId, imageUrl;
        StockItem item = filterList.get(position);
        viewHolder.mainLayout.setId(position);
        viewHolder.etRegistration.setText(item.getStockValue());
        viewHolder.mLabelName.setText(item.getStockDataType());
           String imagePath = item.getCarImagePath();
           if(imagePath.startsWith("https") ||imagePath.startsWith("http") ){
                Picasso.get()
                        .load(imagePath)
                        .placeholder(R.drawable.no_image)
                        .into(viewHolder.thumbImage);
           }else {
               Bitmap bitmap = decodeFile(100, 100, imagePath);
               if (bitmap != null)
                   viewHolder.thumbImage.setImageBitmap(bitmap);
               else
                   viewHolder.thumbImage.setImageResource(R.drawable.no_image);

           }

        viewHolder.thumbImage.setId(position);
        viewHolder.thumbImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FileItem fileItem1 = new FileItem(3, filterList.get(view.getId()).getCarImagePath());

                ArrayList<FileItem> files = new ArrayList<>();
                files.add(fileItem1);

                intent = new Intent(context, GalleryDialogActivity.class);
                intent.putParcelableArrayListExtra("files", files);
                context.startActivity(intent);
            }
        });

        viewHolder.editImage.setId(position);
        viewHolder.editImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });



        viewHolder.editTextWatcherListener.addListener(viewHolder.etRegistration, position);
        viewHolder.textEditorDoneListener.addListener(viewHolder.editRegistration_iv, viewHolder.etRegistration, position, viewHolder);
        viewHolder.editTextImageClickListener.addListener(viewHolder.editRegistration_iv, viewHolder.etRegistration, position, viewHolder);

        viewHolder.btnDelete.setId(position);
        viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {


                showConfirmAlert(context,view.getId());

            }
        });
    }

    private int getSpinnerPosition(String value) {
        int selection = 0;
        if (value.toLowerCase().contains("engine"))
            selection = 1;
        else if (value.toLowerCase().contains("chassis"))
            selection = 2;
        else if (value.toLowerCase().contains("contract"))
            selection = 3;

        return selection;
    }

    private String getSpinnerValue(String type) {
        String str = "Registration Number";
        if (type.toLowerCase().contains("engine"))
            str = "Engine Number";
        else if (type.toLowerCase().contains("chassis"))
            str = "Chassis Number";
        else if (type.toLowerCase().contains("contract"))
            str = "Contract Number";

        return str;
    }

    public void deleteStockItem(int position) {
        StockItem item = filterList.get(position);
        if (isLPRFlow)
            mSqlAdapterForDML.deleteLPRItem(item.getStockId(), item.getStockValue());

        filterList.remove(position);
        notifyDataSetChanged();
        String count = "- " + String.valueOf(filterList.size());
        txtCount.setText(count);
    }

    public void updateVehicleImage(int position, String filePath) {
        StockItem item = filterList.get(position);
        if (isLPRFlow)
            mSqlAdapterForDML.updateLPRItemImage(item.getStockId(), item.getStockValue(), filePath);

        filterList.get(position).setCarImagePath(filePath);
        filterList.get(position).setCarImageUrl("");
        filterList.get(position).setIsSync(0);

        notifyDataSetChanged();
        String count = "- " + String.valueOf(filterList.size());
        txtCount.setText(count);
    }


    public List<StockItem> getFilterList() {
        return this.filterList;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return (null != filterList ? filterList.size() : 0);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView thumbImage, editImage, editRegistration_iv;
        private EditText etRegistration;
        private TextView btnDelete;
        private LinearLayout mainLayout;
        private TextView mLabelName;
        private EditTextWatcherListener editTextWatcherListener;
        private TextEditorDoneListener textEditorDoneListener;
        private EditTextImageClickListener editTextImageClickListener;
        private SpinnerListener spinnerListener;

        public ViewHolder(View rootView) {
            super(rootView);
            thumbImage = (ImageView) rootView.findViewById(R.id.thumbImage);
            editImage = (ImageView) rootView.findViewById(R.id.iv_edit_image);
            editRegistration_iv = (ImageView) rootView.findViewById(R.id.iv_edit_reg);
            etRegistration = (EditText) rootView.findViewById(R.id.edtxt_reg_number);
            btnDelete = (TextView) rootView.findViewById(R.id.txt_delete);
            mainLayout =  rootView.findViewById(R.id.main_layout);
            mLabelName = rootView.findViewById(R.id.labelname);

            editTextWatcherListener = new EditTextWatcherListener();
            textEditorDoneListener = new TextEditorDoneListener();
            editTextImageClickListener = new EditTextImageClickListener();
            spinnerListener = new SpinnerListener();

            etRegistration.addTextChangedListener(editTextWatcherListener);
            etRegistration.setOnEditorActionListener(textEditorDoneListener);
            editRegistration_iv.setOnClickListener(editTextImageClickListener);
        }
    }

    private class EditTextImageClickListener implements View.OnClickListener {
        private ImageView imageView;
        private EditText editText;
        private int position;
        private ViewHolder viewHolder;

        private EditTextImageClickListener() {
        }

        private void addListener(ImageView imageView, EditText editText, int position, ViewHolder viewHolder) {
            this.imageView = imageView;
            this.editText = editText;
            this.position = position;
            this.viewHolder = viewHolder;
        }

        @Override
        public void onClick(View v) {
            if (imageView.getDrawable().getConstantState() == ContextCompat.getDrawable(context, R.drawable.ic_edit).getConstantState()) {
                imageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.tick));

                editText.setEnabled(true);
                editText.requestFocus();
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                viewHolder.btnDelete.setVisibility(View.GONE);
            }

        }
    }

    private class TextEditorDoneListener implements TextView.OnEditorActionListener {

        private ImageView imageView;
        private EditText editText;
        private int position;
        private ViewHolder viewHolder;

        private TextEditorDoneListener() {
        }

        private void addListener(ImageView imageView, EditText editText, int position, ViewHolder viewHolder) {
            this.imageView = imageView;
            this.editText = editText;
            this.position = position;
            this.viewHolder = viewHolder;
        }

        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

            if (editText != null) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    String regNo = editText.getText().toString().trim();
                    if (regNo.length() > 0) {
                        return true;
                    } else {
                        Toast.makeText(context, "Please Enter Registration No", Toast.LENGTH_SHORT).show();
                        return true;
                    }
                }
            }
            return false;
        }
    }

    private class EditTextWatcherListener implements TextWatcher {

        private EditText editText;
        private int position;

        private EditTextWatcherListener() {
        }

        private void addListener(EditText editText, int position) {
            this.editText = editText;
            this.position = position;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        public void onTextChanged(CharSequence s, int i, int before, int mCount) {
            if (editText != null) {
                String str, value;
                Pattern pattern;
                if (s.hashCode() == editText.getText().hashCode()) {
                    str = editText.getText().toString();
                    if (before < mCount) {
                        pattern = Pattern.compile("[A-Za-z0-9]");
                        for (int count = 0; count < str.length(); count++) {
                            value = String.valueOf(s.charAt(count));
                            if (!pattern.matcher(value).matches()) {
                                editText.removeTextChangedListener(this);
                                editText.setText(str.substring(0, str.length() - 1));
                                editText.addTextChangedListener(this);
                                editText.setSelection(editText.getText().length());
                            }
                        }
                    }
                }

            }

        }

        public void afterTextChanged(Editable s) {
            if (editText != null) {
                if (s.hashCode() == editText.getText().hashCode()) {
                    if (!s.toString().equals(s.toString().toUpperCase())) {
                        editText.setText(s.toString().toUpperCase());
                    }
                    editText.setSelection(editText.getText().length());
                }
            }
        }
    }

    private class SpinnerListener implements AdapterView.OnItemSelectedListener {

        private ViewHolder viewHolder;
        private int position, spinnerPosition;

        private SpinnerListener() {
        }

        private void addListener(ViewHolder viewHolder, int position, int spinnerPos) {
            this.viewHolder = viewHolder;
            this.position = position;
            this.spinnerPosition = spinnerPos;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            if (position != spinnerPosition) {



                if (viewHolder.editRegistration_iv.getDrawable().getConstantState() == ContextCompat.getDrawable(context, R.drawable.ic_edit).getConstantState())
                    viewHolder.editTextImageClickListener.onClick(viewHolder.editRegistration_iv);


            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void filter(final String text) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                filterList.clear();
                if (TextUtils.isEmpty(text))
                    filterList.addAll(cardList);
                else {
                    String data = text.toLowerCase();
                    for (StockItem item : cardList) {
                        String regNo = item.getStockValue().toLowerCase();
                        if (regNo.contains(data))
                            filterList.add(item);
                    }
                }
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        notifyDataSetChanged();
                        String count = "- " + String.valueOf(filterList.size());
                        txtCount.setText(count);
                    }
                });

            }
        }).start();
    }


    public  Bitmap decodeFile(int WIDTH, int HEIGHT, String Path) {
        try {
            File f = new File(Path);
            if (f.exists()) {
                //Decode image size
                BitmapFactory.Options o = new BitmapFactory.Options();
                o.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(new FileInputStream(f), null, o);
                //Find the correct scale value. It should be the power of 2.
                int scale = 1;
                while (o.outWidth / scale / 2 >= WIDTH && o.outHeight / scale / 2 >= HEIGHT)
                    scale *= 2;

                //Decode with inSampleSize
                BitmapFactory.Options o2 = new BitmapFactory.Options();
                o2.inSampleSize = scale;
                Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
                ExifInterface ei = null;
                try {
                    ei = new ExifInterface(Path);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        bmp = rotateImage(bmp, 90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        bmp = rotateImage(bmp, 180);
                        break;
                }
                return bmp;
            } else
                return null;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private  Bitmap rotateImage(Bitmap source, float angle) {
        Bitmap retVal;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        retVal = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
        return retVal;
    }

    private void showConfirmAlert(Context mContext,int position){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);

        // set title
        alertDialogBuilder.setTitle("MFCBuniness");

        // set dialog message
        alertDialogBuilder
                .setMessage("Are you sure you want to delete it ?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteStockItem(position);
                    }
                })
                .setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }
}