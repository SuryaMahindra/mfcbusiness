package sidekicklpr;

/**
 * @author MAUTO0c332 on 12-11-2018.
 */
public class StockItem {

    private int stockId;
    private String carImagePath;
    private String carImageUrl;
    private String stockDataType;
    private String stockValue;
    private int isSync;

    private String eventid;
    private String datetime;
    private String latitude;
    private String longitude;

    public String getCarplateimage() {
        return carplateimage;
    }

    public void setCarplateimage(String carplateimage) {
        this.carplateimage = carplateimage;
    }

    private String carplateimage;

    public String getEventid() {
        return eventid;
    }

    public void setEventid(String eventid) {
        this.eventid = eventid;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }



    public int getStockId() {
        return this.stockId;
    }

    public void setStockId(int stockId) {
        this.stockId = stockId;
    }




    public String getCarImagePath() {
        return carImagePath;
    }

    public void setCarImagePath(String path) {
        this.carImagePath = path;
    }




    public String getCarImageUrl() {
        return carImageUrl;
    }

    public void setCarImageUrl(String url) {
        this.carImageUrl = url;
    }



    public String getStockDataType() {
        return stockDataType;
    }

    public void setStockDataType(String stockDataType) {
        this.stockDataType = stockDataType;
    }



    public String getStockValue() {
        return stockValue;
    }

    public void setStockValue(String reg) {
        this.stockValue = reg;
    }



    public int getIsSync() {
        return this.isSync;
    }

    public void setIsSync(int status) {
        this.isSync = status;
    }


}
