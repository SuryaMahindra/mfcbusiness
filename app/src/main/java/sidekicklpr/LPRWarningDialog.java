package sidekicklpr;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.R;
import com.vigilant.mcsidekicksdk.Screens.ScreenOnboardCamera;
import com.vigilant.mcsidekicksdk.app.VKey;

import java.io.File;
import java.util.UUID;

public class LPRWarningDialog extends Dialog implements
        android.view.View.OnClickListener {

    public Activity c;
    public Dialog d;
    public Button cancel, ok;
    private PreferenceManager preferenceManager;
    private boolean fromMainMenu;


    public LPRWarningDialog(Activity a,boolean frommainmenu) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.fromMainMenu = frommainmenu;
        preferenceManager = new PreferenceManager(c);

    }
    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog);
        Log.i(TAG, "onCreate: ");
        cancel = (Button) findViewById(R.id.btn_cancel);
        ok = (Button) findViewById(R.id.btn_ok);
        cancel.setOnClickListener(this);
        ok.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
               dismiss();
                break;
            case R.id.btn_ok:
                dismiss();
                MainActivity.isLPRstartedfromMenu = fromMainMenu;
                startLPRScan(c,LPRConstants.REQUEST_CODE_LPR_MULTIPLE_SCAN);
                break;
            default:
                break;
        }

    }


    public void startLPRScan(Activity context, int type) { // type 1001(sinlge scan), 1002(multiple)
        String lprDir = preferenceManager.readString(LPRConstants.LPR_DIR, ""); //this will be YMS / LPR

        if (lprDir.isEmpty()) {
            File sdCard = Environment.getExternalStorageDirectory();
            lprDir = sdCard.getAbsolutePath() + "/" + context.getResources().getString(R.string.app_name) + "/" + "LPR";
            File dir = new File(lprDir);
            if (!dir.exists())
                dir.mkdirs();

            lprDir = dir.getPath();
            preferenceManager.writeString(LPRConstants.LPR_DIR, lprDir);
        }

        /*LPR FILENAME will be created for every call of this method..*/
        String lprFileName = UUID.randomUUID().toString() + "_" + System.currentTimeMillis(); // unique id creating..
        /*LPR_FILENAME1 for SINGLE SCAN and LPR_FILENAME2 for MULTIPLE SCAN*/
        preferenceManager.writeString(LPRConstants.LPR_FILENAME + type, lprFileName); //this is unique id only..now storing bsed on type of scan..

        Intent intent = new Intent(context, ScreenOnboardCamera.class);
       // intent.putExtra(VKey.ShowListScans, false);
        intent.putExtra(VKey.IconTakePhoto, R.drawable.edit_text_trans_bg);
        Log.e("LPR Path", lprDir + "/" + lprFileName);
        intent.putExtra(VKey.PathFolder, lprDir + "/" + lprFileName); // path folder setting..
        if (type == LPRConstants.REQUEST_CODE_LPR_SINGLE_SCAN) {
            intent.putExtra(VKey.LimitScans, 1);
            intent.putExtra(VKey.ShowListScans, false);
        }
        context.startActivityForResult(intent, type);
    }

}