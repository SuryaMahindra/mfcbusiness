package sidekicklpr;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;

import java.util.List;

import lprServices.LPRModel;
import lprServices.LPRService;
import lprServices.ReconcialationRequest;
import lprServices.ReconcialationResponse;
import retrofit2.Response;

public class StockReconciliationReport extends AppCompatActivity {

    private LinearLayout mDataContainer;
    private ImageView mBack;
    private TextView mAll, mAtDealership, mInOMS,lbldealership,lblinoms;
    private String dealercode = "";

    String TAG = getClass().getSimpleName();
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stockreconciliationreport);
        Log.i(TAG, "onCreate: ");
        mDataContainer = findViewById(R.id.datacontainer);
        mBack = findViewById(R.id.stock_backbtn);
        mAll = findViewById(R.id.alldata);
        mAtDealership = findViewById(R.id.atdealership);
        mInOMS = findViewById(R.id.inoms);
        lbldealership = findViewById(R.id.lbldealership);
        lblinoms = findViewById(R.id.lblinoms);

        mAll.setTextColor(getResources().getColor(R.color.orange));
        String userType = CommonMethods.getstringvaluefromkey(StockReconciliationReport.this,"user_type");
        if(userType.equalsIgnoreCase("dealer")){
            dealercode = CommonMethods.getstringvaluefromkey(StockReconciliationReport.this, "dealer_code");

        }else {
            dealercode = CommonMethods.getstringvaluefromkey(StockReconciliationReport.this, "dcode");
        }

        fetchReconcialationData("all");

        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mAll.setTextColor(getResources().getColor(R.color.orange));
                mAtDealership.setTextColor(getResources().getColor(R.color.white));
                mInOMS.setTextColor(getResources().getColor(R.color.white));
                mDataContainer.removeAllViews();
                fetchReconcialationData("all");

            }
        });

        mAtDealership.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mDataContainer.removeAllViews();
                mAll.setTextColor(getResources().getColor(R.color.white));
                mAtDealership.setTextColor(getResources().getColor(R.color.orange));
                mInOMS.setTextColor(getResources().getColor(R.color.white));
                fetchReconcialationData("atdealership");
            }
        });

        mInOMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mDataContainer.removeAllViews();
                mAll.setTextColor(getResources().getColor(R.color.white));
                mAtDealership.setTextColor(getResources().getColor(R.color.white));
                mInOMS.setTextColor(getResources().getColor(R.color.orange));
                fetchReconcialationData("inoms");
            }
        });


    }


    private void getReconcialationData(final ReconcialationRequest request) {
        SpinnerManager.showSpinner(StockReconciliationReport.this);
        LPRService.getLPRData(request, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                SpinnerManager.hideSpinner(StockReconciliationReport.this);
                Response<ReconcialationResponse> mRes = (Response<ReconcialationResponse>) obj;
                ReconcialationResponse mData = mRes.body();
                List<LPRModel> lprModelList = mData.getLPR();
                if (lprModelList.size() > 0) {
                    showReconcialationData(lprModelList);
                } else {
                    Toast.makeText(StockReconciliationReport.this, "No data found", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void OnFailure(Throwable mThrowable) {
                SpinnerManager.hideSpinner(StockReconciliationReport.this);
                mThrowable.printStackTrace();

            }
        });

    }

    private void showReconcialationData(List<LPRModel> mData) {
        for (int i = 0; i < mData.size(); i++) {
            LPRModel data = mData.get(i);
            View view = getLayoutInflater().inflate(R.layout.reconcialation_design, null);
            TextView txt = view.findViewById(R.id.regnumber);
            ImageView atdealer = view.findViewById(R.id.imgatdealer);
            ImageView inoms = view.findViewById(R.id.imginoms);
            txt.setText(data.getRegistrationNo());
            if (data.getAtDealership()) {
                atdealer.setImageResource(R.drawable.ic_green);
            } else {
                atdealer.setImageResource(R.drawable.ic_rose);

            }
            if (data.getInOMS()) {
                inoms.setImageResource(R.drawable.ic_green);
            } else {
                inoms.setImageResource(R.drawable.ic_rose);

            }

            mDataContainer.addView(view);
        }


    }

    private void fetchReconcialationData(String filter) {
        ReconcialationRequest request = new ReconcialationRequest();
        request.setFilter(filter);
        request.setDealerCode(dealercode);
        getReconcialationData(request);
    }
}
