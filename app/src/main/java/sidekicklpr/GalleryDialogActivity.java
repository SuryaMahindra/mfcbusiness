package sidekicklpr;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.mfcwl.mfc_dealer.R;

import java.util.ArrayList;

import gallerylib.ScrollGalleryView;
import gallerylib.utils.MediaInfo;
import gallerylib.utils.PicassoImageLoader;

/**
 * @author MAUTO0c332 on 10-04-2019.
 */
public class GalleryDialogActivity extends AppCompatActivity {

    protected ScrollGalleryView scrollGalleryView;
    private LinearLayout editImage;

    private ArrayList<FileItem> files = new ArrayList<>();

    private int editStatus;
    private boolean editAvailable = false;

    private Intent returnIntent;
    String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        Log.i(TAG, "onCreate: ");
        scrollGalleryView = (ScrollGalleryView) findViewById(R.id.scroll_gallery_view);
        scrollGalleryView
                .setThumbnailSize(200)
                .setZoom(true)
                .setFragmentManager(getSupportFragmentManager());

        returnIntent = getIntent();
        files = returnIntent.getParcelableArrayListExtra("files");

        FileItem fileItem;
        for (int i = 0; i < files.size(); i++) {
            fileItem = files.get(i);
            scrollGalleryView.addMedia(
                   MediaInfo.mediaLoader(new PicassoImageLoader(this, fileItem.getType(), fileItem.getValue(), i))
            );
        }

        editStatus = getIntent().getIntExtra("editStatus", 0);
        editAvailable = editStatus != 0;
        if (editAvailable) {

            editImage = (LinearLayout) findViewById(R.id.layout_edit_image);
            editImage.setVisibility(View.VISIBLE);

            editImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    returnIntent.putExtra("editStatus", editStatus);
                    setResult(RESULT_OK, returnIntent);
                    finish();
                }
            });
        }
    }
}
