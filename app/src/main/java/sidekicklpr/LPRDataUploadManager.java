package sidekicklpr;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;

import com.mfcwl.mfc_dealer.Activity.MainActivity;
import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.SpinnerManager;
import com.mfcwl.mfc_dealer.videoAppSpecific.SqlAdapterForDML;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import lprServices.AsyncResponse;
import lprServices.LPRData;
import lprServices.LPRRequest;
import lprServices.LPRServerResponse;
import lprServices.LPRService;
import retrofit2.Response;

public class LPRDataUploadManager extends AsyncTask<Void, Void, Void> {
    private SqlAdapterForDML mSqlAdapterForDML = SqlAdapterForDML.getInstance();
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private Activity mContext;
    private  LPRServerResponse mData;
    public static AsyncResponse delegate = null;
    private  String dealercode = "";

    public LPRDataUploadManager(final Activity mContext) {
        this.mContext = mContext;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        SpinnerManager.showSpinner(mContext);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        List<LPRData> mData = mSqlAdapterForDML.getLPRRequestData();
        LPRRequest mRequest = new LPRRequest();
        String userType = CommonMethods.getstringvaluefromkey(mContext,"user_type");
        if(userType.equalsIgnoreCase("dealer")){
            dealercode = CommonMethods.getstringvaluefromkey((Activity) mContext, "user_id");

        }else {
            dealercode = CommonMethods.getstringvaluefromkey((Activity) mContext, "dcode");
        }
        mRequest.setDealerCode(dealercode);
        mRequest.setDataSentOn(dateFormat.format(new Date()));
        if(MainActivity.isLPRstartedfromMenu){
            mRequest.setSentBy("Dealer");
        }else {
            mRequest.setSentBy("AreaManger");
        }
        mRequest.setData(mData);
        sendLPRData(mRequest);

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        SpinnerManager.hideSpinner(mContext);



    }

    private void sendLPRData(final LPRRequest mLprRequest) {


        LPRService.sendLPRDataToServer(mLprRequest, new HttpCallResponse() {
            @Override
            public void OnSuccess(Object obj) {
                Response<LPRServerResponse> mRes = (Response<LPRServerResponse>) obj;
                 mData = mRes.body();
                 mSqlAdapterForDML.deleteLPRData();
                showLPRStatus(mData);
            }

            @Override
            public void OnFailure(Throwable mThrowable) {

            }
        });

    }

    private void pushToReconciliationScreen() {
        Intent mIntent = new Intent(mContext, StockReconciliationReport.class);
        mContext.startActivity(mIntent);
        this.mContext.finish();
    }


    private void showLPRStatus(LPRServerResponse mData) {
        AlertDialog alertDialog = new AlertDialog.Builder(mContext)
                .setTitle("LPR")
                .setMessage(mData.getMessage())
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mContext.finish();

                        if (MainActivity.isLPRstartedfromMenu) {
                            pushToReconciliationScreen();
                        } else {
                            if (delegate != null) {
                                delegate.processFinish(mData);
                            }
                        }
                    }
                })
                .show();
    }
}
