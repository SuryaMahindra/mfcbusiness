package sidekicklpr;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LPRResponse {

    @SerializedName("EventID")
    @Expose
    private String eventID;
    @SerializedName("DateTime")
    @Expose
    private String dateTime;
    @SerializedName("PlateImage")
    @Expose
    private String plateImage;
    @SerializedName("CarImage")
    @Expose
    private String carImage;
    @SerializedName("Longitude")
    @Expose
    private String longitude;
    @SerializedName("Latitude")
    @Expose
    private String latitude;
    @SerializedName("SelectedData")
    @Expose
    private String selectedData;
    @SerializedName("OutputValue")
    @Expose
    private String outputValue;
    @SerializedName("Type")
    @Expose
    private Integer type;

    public String getEventID() {
        return eventID;
    }

    public void setEventID(String eventID) {
        this.eventID = eventID;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getPlateImage() {
        return plateImage;
    }

    public void setPlateImage(String plateImage) {
        this.plateImage = plateImage;
    }

    public String getCarImage() {
        return carImage;
    }

    public void setCarImage(String carImage) {
        this.carImage = carImage;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getSelectedData() {
        return selectedData;
    }

    public void setSelectedData(String selectedData) {
        this.selectedData = selectedData;
    }

    public String getOutputValue() {
        return outputValue;
    }

    public void setOutputValue(String outputValue) {
        this.outputValue = outputValue;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

}
