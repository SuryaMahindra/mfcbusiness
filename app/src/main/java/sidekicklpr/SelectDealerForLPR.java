package sidekicklpr;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mfcwl.mfc_dealer.R;
import com.mfcwl.mfc_dealer.Utility.CommonMethods;
import com.mfcwl.mfc_dealer.Utility.Helper;
import com.mfcwl.mfc_dealer.videoAppSpecific.SqlAdapterForDML;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SelectDealerForLPR extends AppCompatActivity {



    TextView select_dealer_tv;
    String cNameValue;
    String dealerCatVal = "select";
    public String TAG = getClass().getSimpleName();
    private Button startLPR ;

    private sidekicklpr.PreferenceManager preferenceManager;
    private SqlAdapterForDML mSqlAdapterForDML = SqlAdapterForDML.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selectdealerforlpr);
        Log.i(TAG, "onCreate: ");
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ImageView wrBackIV = findViewById(R.id.createRepo_backbtn);
        startLPR = findViewById(R.id.startLPR);
        preferenceManager = new sidekicklpr.PreferenceManager(this);


        Log.i(TAG, "onCreate: ");

         startLPR.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 LPRWarningDialog mDialog = new LPRWarningDialog(SelectDealerForLPR.this,true);
                 mDialog.show();
             }
         });
        wrBackIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });
        select_dealer_tv = findViewById(R.id.select_dealer_tv);


        select_dealer_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<String> dealerName = new ArrayList<String>();
                dealerName.clear();

                dealerName = Helper.dealerName;

               /* if (!dealerName.isEmpty()) {

                    if (!dealerName.get(dealerName.size() - 1).equalsIgnoreCase("Others")) {
                        dealerName.add(dealerName.size(), "Others");
                    }
                } else {
                    dealerName.add(dealerName.size(), "Others");
                }
*/
                List<String> dealerCode = new ArrayList<String>();
                dealerCode.clear();
                dealerCode = Helper.dealerCode;

              /*  if (!dealerCode.isEmpty()) {
                    if (!dealerCode.get(dealerCode.size() - 1).equalsIgnoreCase("Others")) {
                        dealerCode.add(dealerCode.size(), "Others");
                    }
                } else {
                    dealerCode.add(dealerCode.size(), "Others");
                }*/

                String[] namesArr = dealerName.toArray(new String[dealerName.size()]);

                String[] codeArr = dealerCode.toArray(new String[dealerCode.size()]);

                setAlertDialog(v, SelectDealerForLPR.this, "Dealer Category", namesArr, codeArr);

            }
        });
        cNameValue = (String) select_dealer_tv.getText();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LPRConstants.REQUEST_CODE_LPR_MULTIPLE_SCAN) {
            Log.e("Onactivity called", "onActivity called");
            String value = readLPRFile(this, LPRConstants.REQUEST_CODE_LPR_MULTIPLE_SCAN);
            Log.e("LPR Data", "" + value);
            parseLPRData(value);
        }

    }


    private void parseLPRData(String value) {
        try {
            Type empTypeList = new TypeToken<ArrayList<LPRResponse>>() {
            }.getType();
            List<LPRResponse> mData = new Gson().fromJson(value, empTypeList);
            if (mData != null && mData.size() > 0) {
                Log.i("LPR", "Result: ");
                for (int i = 0; i < mData.size(); i++) {
                    LPRResponse response = mData.get(i);
                    String outputValue = response.getOutputValue();
                    if (outputValue.toLowerCase().equals("unreg/na"))
                        outputValue = "";
                    mSqlAdapterForDML.insertLPRItem(response);
                }

            }
            if (mSqlAdapterForDML.getLPRListCount() > 0) {
                showStockListingScreen();
            }
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showStockListingScreen() {
        Intent intent = new Intent(SelectDealerForLPR.this, StockActivity.class);
        startActivityForResult(intent, LPRConstants.REQUEST_CODE_LPR_STOCK_LIST);
    }

    private void setAlertDialog(View v, Activity a, final String strTitle,
                                final String[] arrVal, final String[] codeVal) {

        AlertDialog.Builder alert = new AlertDialog.Builder(a);
        alert.setTitle(strTitle);
        alert.setItems(arrVal, (dialog, which) -> {

            select_dealer_tv.setText(arrVal[which]);
            dealerCatVal = codeVal[which];

            CommonMethods.setvalueAgainstKey(SelectDealerForLPR.this, "dcode", dealerCatVal);
            CommonMethods.setvalueAgainstKey(SelectDealerForLPR.this, "dname", select_dealer_tv.getText().toString());



        });
        alert.create();
        alert.show();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

       finish();
    }


    public String readLPRFile(Context context, int type) { // reading file..for single scan..jSON filw is deleted after reading image..
        String value = "";
        final String lprDir, lprFileName;
        lprDir = preferenceManager.readString(LPRConstants.LPR_DIR, "");
        lprFileName = preferenceManager.readString(LPRConstants.LPR_FILENAME + type, "");
        try {

            int fileCount = 0;
            File directory;
            File[] allFiles;

            FilenameFilter fileFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return (name.startsWith(lprFileName) && name.endsWith(".json"));
                }
            };

            directory = new File(lprDir + "/" + lprFileName);
            allFiles = directory.listFiles(fileFilter);
            Log.e("LPR", "file lenght: " + allFiles.length);
            if (allFiles.length == 1) {

                File file = allFiles[0];/*LPR required file..*/
                if (file.exists()) {
                    FileInputStream fis = new FileInputStream(file);
                    DataInputStream in = new DataInputStream(fis);
                    BufferedReader br = new BufferedReader(new InputStreamReader(in));
                    String strLine;
                    while ((strLine = br.readLine()) != null) {
                        value = value + strLine;
                    }
                    in.close();
                    if (type == LPRConstants.REQUEST_CODE_LPR_SINGLE_SCAN && file.delete()) {
                        fileCount++;
                        Log.e("LPR", "file Deleted");
                    } else {
                        Log.e("LPR", "file not Deleted");
                    }
                }
            } else {
                for (File f : allFiles) {
                    if (f.delete()) {
                        fileCount++;
                        Log.e("LPR", "file Deleted");
                    } else {
                        Log.e("LPR", "file not Deleted");
                    }
                }
            }
            Log.e("LPR", "deleted file count: " + fileCount);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return value;
    }

}