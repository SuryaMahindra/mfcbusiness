package sidekicklpr;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author MAUTO0c332 on 15-11-2018.
 */
public class FileItem implements Parcelable{
    private int type; /*o- path, 1- url, 2-drawable*/
    private String value;

    public FileItem(int type, String value) {
        this.type = type;
        this.value = value;
    }

    protected FileItem(Parcel in) {
        type = in.readInt();
        value = in.readString();
    }

    public static final Creator<FileItem> CREATOR = new Creator<FileItem>() {
        @Override
        public FileItem createFromParcel(Parcel in) {
            return new FileItem(in);
        }

        @Override
        public FileItem[] newArray(int size) {
            return new FileItem[size];
        }
    };

    public void setType(int type){this.type = type;}
    public int getType(){return this.type;}
    public void setValue(String value){this.value = value;}
    public String getValue() { return value; }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(type);
        dest.writeString(value);
    }
}
