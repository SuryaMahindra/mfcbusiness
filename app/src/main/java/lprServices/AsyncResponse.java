package lprServices;

public interface AsyncResponse {
    void processFinish(LPRServerResponse output);
}
