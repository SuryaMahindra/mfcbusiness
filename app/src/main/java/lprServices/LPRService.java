package lprServices;

import com.mfcwl.mfc_dealer.ResponseHandler.HttpCallResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class LPRService extends LPRBaseService {


    public static void sendLPRDataToServer(final LPRRequest mRequest, final HttpCallResponse mCallResponse){
        LPRInterface mInterface = retrofit.create(LPRInterface.class);
        Call<LPRServerResponse> mCall = mInterface.postLPRData(mRequest);

        mCall.enqueue(new Callback<LPRServerResponse>() {
            @Override
            public void onResponse(Call<LPRServerResponse> call, Response<LPRServerResponse> response) {
                if(response.isSuccessful()){
                    mCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<LPRServerResponse> call, Throwable t) {
                mCallResponse.OnFailure(t);
            }
        });
    }


    public static void getLPRData(final ReconcialationRequest mRequest , final HttpCallResponse mHttpCallResponse){
        LPRInterface mInterface = retrofit.create(LPRInterface.class);
        Call<ReconcialationResponse> mCall = mInterface.getLPRData(mRequest);

        mCall.enqueue(new Callback<ReconcialationResponse>() {
            @Override
            public void onResponse(Call<ReconcialationResponse> call, Response<ReconcialationResponse> response) {
                if(response.isSuccessful()){
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<ReconcialationResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });
    }

    public static void getLPRDashBoardDateFromServer(final ReconcialationRequest mRequest , final HttpCallResponse mHttpCallResponse){

        LPRInterface mInterface = retrofit.create(LPRInterface.class);
        Call<LPRDashboardResponse> mCall = mInterface.getLPRDashboardData(mRequest);

        mCall.enqueue(new Callback<LPRDashboardResponse>() {
            @Override
            public void onResponse(Call<LPRDashboardResponse> call, Response<LPRDashboardResponse> response) {
                if(response.isSuccessful()){
                    mHttpCallResponse.OnSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<LPRDashboardResponse> call, Throwable t) {
                mHttpCallResponse.OnFailure(t);
            }
        });

    }



    public interface LPRInterface {
        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("LPR/AddLPR")
        Call<LPRServerResponse> postLPRData(@Body LPRRequest mRequest);

        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("lpr/FilterLPR")
        Call<ReconcialationResponse> getLPRData(@Body ReconcialationRequest mRequest);

        @Headers("Content-Type: application/json; charset=utf-8")
        @POST("lpr/LPRDashboard")
        Call<LPRDashboardResponse> getLPRDashboardData(@Body ReconcialationRequest mRequest);
    }
}
