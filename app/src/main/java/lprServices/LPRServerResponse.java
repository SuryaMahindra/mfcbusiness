package lprServices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LPRServerResponse {

    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("Message")
    @Expose
    private String message;

    @SerializedName("SuccessEventCount")
    @Expose
    private Integer successEventCount;
    @SerializedName("FaileEventsCount")
    @Expose
    private Integer faileEventsCount;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getSuccessEventCount() {
        return successEventCount;
    }

    public void setSuccessEventCount(Integer successEventCount) {
        this.successEventCount = successEventCount;
    }

    public Integer getFaileEventsCount() {
        return faileEventsCount;
    }

    public void setFaileEventsCount(Integer faileEventsCount) {
        this.faileEventsCount = faileEventsCount;
    }

}