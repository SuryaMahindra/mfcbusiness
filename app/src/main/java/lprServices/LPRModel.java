package lprServices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LPRModel {

    @SerializedName("RegistrationNo")
    @Expose
    private String registrationNo;
    @SerializedName("AtDealership")
    @Expose
    private Boolean atDealership;
    @SerializedName("EventDateTime")
    @Expose
    private String eventDateTime;
    @SerializedName("InOMS")
    @Expose
    private Boolean inOMS;

    public String getRegistrationNo() {
        return registrationNo;
    }

    public void setRegistrationNo(String registrationNo) {
        this.registrationNo = registrationNo;
    }

    public Boolean getAtDealership() {
        return atDealership;
    }

    public void setAtDealership(Boolean atDealership) {
        this.atDealership = atDealership;
    }

    public String getEventDateTime() {
        return eventDateTime;
    }

    public void setEventDateTime(String eventDateTime) {
        this.eventDateTime = eventDateTime;
    }

    public Boolean getInOMS() {
        return inOMS;
    }

    public void setInOMS(Boolean inOMS) {
        this.inOMS = inOMS;
    }

}
