package lprServices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LPRRequest {

    @SerializedName("DealerCode")
    @Expose
    private String dealerCode;
    @SerializedName("SentBy")
    @Expose
    private String sentBy;
    @SerializedName("DataSentOn")
    @Expose
    private String dataSentOn;
    @SerializedName("Data")
    @Expose
    private List<LPRData> data = null;

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getSentBy() {
        return sentBy;
    }

    public void setSentBy(String sentBy) {
        this.sentBy = sentBy;
    }

    public String getDataSentOn() {
        return dataSentOn;
    }

    public void setDataSentOn(String dataSentOn) {
        this.dataSentOn = dataSentOn;
    }

    public List<LPRData> getData() {
        return data;
    }

    public void setData(List<LPRData> data) {
        this.data = data;
    }

}