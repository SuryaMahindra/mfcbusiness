package lprServices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LPRDashboardResponse {

    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("ErrorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("atDealershipcount")
    @Expose
    private Integer atDealershipcount;
    @SerializedName("inoms")
    @Expose
    private Integer inoms;
    @SerializedName("dispcrepancy")
    @Expose
    private Integer dispcrepancy;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Integer getAtDealershipcount() {
        return atDealershipcount;
    }

    public void setAtDealershipcount(Integer atDealershipcount) {
        this.atDealershipcount = atDealershipcount;
    }

    public Integer getInoms() {
        return inoms;
    }

    public void setInoms(Integer inoms) {
        this.inoms = inoms;
    }

    public Integer getDispcrepancy() {
        return dispcrepancy;
    }

    public void setDispcrepancy(Integer dispcrepancy) {
        this.dispcrepancy = dispcrepancy;
    }

}